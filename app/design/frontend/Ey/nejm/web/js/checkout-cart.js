require([
	'jquery'
], function($) {
	let afterCartSectionElement = '.after-cart-section-1',
		copyrightLogoElement = '.copyright-logo';

	function moveCartElements() {
   		if ($(window).width() <= 767) {
        	$(afterCartSectionElement).insertAfter(".cart.main.actions");
        	$(copyrightLogoElement).insertAfter(".cart-footer .pagebuilder-column-group");
   		} else {
    		$(afterCartSectionElement).insertAfter(".after-cart-section-0");
    		$(copyrightLogoElement).insertBefore(".cart-footer .pagebuilder-column-group");
    	}
	}

	$(document).ready(function() {
		moveCartElements();
	});

	$(window).resize(function() {
		moveCartElements();
	});
});
