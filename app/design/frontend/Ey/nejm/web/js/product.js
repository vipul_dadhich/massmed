require(['jquery'], function ($) {	
	$(document).ready(function(){
		readMore();
	});
	function readMore(){
     	var carLmt = 230;
     	var readMoreTxt = " <span class='readMoreHolder'>... <span class='readMore'>Read More</span></span>";
     	var readLessTxt = " <span class='readLessHolder' style='display:none'><span class='readLess'>Read Less</span></span>";
		$(".product-info-main .product.attribute.overview .value").each(function() {
		    var allstr = $(this).text();
		    if (allstr.length > carLmt) {
		        var firstSet = allstr.substring(0, carLmt);
		        var secdHalf = allstr.substring(carLmt, allstr.length);
		        var strtoadd = firstSet + "<span class='SecSec' style='display:none'>" + secdHalf + "</span>" + readMoreTxt + readLessTxt;
		        $(this).html(strtoadd);
		        
		        $('.readMore').click(function(){
		        	$('.readLessHolder').show();
		        	$('.readMoreHolder').hide();
		        	$('.SecSec').show();
		        });
		        $('.readLess').click(function(){
		        	$('.readMoreHolder').show();
		        	$('.readLessHolder').hide();
		        	$('.SecSec').hide();
		        });
		    }
		
		});
		
    }
    
    
});
