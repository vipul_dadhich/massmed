require(['jquery'], function ($) {
	$(document).ready(function(){
		loginFix();
	});
	function loginFix(){

            var get_url = '/loginstate/ajax/loginstate/';
            $.ajax({
                type: 'GET',
                url: get_url,
                dataType: 'json',
                success: function(data) {
                    if(data['login']){
                        var customerName = data['name'];
                        $('.loggedin').show();
                        $('.customername').text(customerName);
                    }else{
                        $('.notloggedin').show();
                    }
                }, error: function(xhr, ajaxOptions, thrownError){
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
     }
     $("body").delegate(".loggedin","click",function(){
      $('.account-drop-down').toggle();
     });
     $("body").delegate(".ey_personal_information_step input","keyup",function(){
            var messageId = $(this).attr('id')+'-error';
            $(this).removeClass('mage-error');
            $(this).parent('div').find('#'+messageId).remove();
     });
     $("body").delegate(".ey_personal_information_step select","change",function(){
            var messageId = $(this).attr('id')+'-error';
            $(this).removeClass('mage-error');
            $(this).parent('div').find('#'+messageId).remove();
     });


     /*$("body").delegate(".billing-address-form input[name='postcode']","blur",function(){

        if($(this).val() !=="" ){
            var isValid = true;
            if($(".billing-address-form select[name='country_id']").val()==="US"){

                isValid = /(^\d{5}$)|(^\d{5}-\d{4}$)/.test($(this).val());

              }else if($(".billing-address-form select[name='country_id']").val()==="CA"){
                isValid = /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/.test($(this).val());

              }else {
                isValid = ( $(this).val().trim().length <= 9 ) ? true : false;
              }

              if(!isValid && !$(".zip-code-error").length){
                $(this).after("<div class='field-error zip-code-error' generated='true'\> <span>Please enter a valid Zip/Postal Code for your region.</span> </div>");
              }
        }
     });*/

  $("body").delegate(".billing-address-form input[name='postcode']","keyup",function(){
	$(".zip-code-error").remove();
});
});
