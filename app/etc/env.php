<?php
return [
    'cache' => [
        'frontend' => [
            'default' => [
                'backend' => 'Cm_Cache_Backend_Redis',
                'backend_options' => [
                    'server' => 'redis',
                    'port' => '6379',
                    'database' => 1
                ]
            ],
            'page_cache' => [
                'backend' => 'Cm_Cache_Backend_Redis',
                'backend_options' => [
                    'server' => 'redis',
                    'port' => '6379',
                    'database' => 2
                ]
            ]
        ]
    ],
    'MAGE_MODE' => 'developer',
    'cron' => [
        'enabled' => 0
    ],
    'backend' => [
        'frontName' => 'admin'
    ],
    'db' => [
        'connection' => [
            'default' => [
                'username' => 'magento2',
                'host' => 'db',
                'dbname' => 'magento2',
                'password' => 'magento2'
            ],
            'indexer' => [
                'username' => 'magento2',
                'host' => 'db',
                'dbname' => 'magento2',
                'password' => 'magento2'
            ]
        ]
    ],
    'crypt' => [
        'key' => '30e96b1d2b5a3df22d6f1179f33f0f40'
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'x-frame-options' => 'SAMEORIGIN',
    'session' => [
        'save' => 'redis',
        'redis' => [
            'host' => 'redis',
            'port' => '6379',
            'database' => 0,
            'disable_locking' => 1
        ]
    ],
    'lock' => [
        'provider' => 'db',
        'config' => [
            'prefix' => null
        ]
    ],
    'cache_types' => [
        'config' => 1,
        'layout' => 1,
        'block_html' => 1,
        'collections' => 1,
        'reflection' => 1,
        'db_ddl' => 1,
        'compiled_config' => 1,
        'eav' => 1,
        'customer_notification' => 1,
        'config_integration' => 1,
        'config_integration_api' => 1,
        'target_rule' => 1,
        'google_product' => 1,
        'full_page' => 1,
        'config_webservice' => 1,
        'translate' => 1,
        'vertex' => 1,
        'mms_price_engine_cache' => 1
    ],
    'downloadable_domains' => [
        'magento2.docker',
        'ey-studio-integration-ewu5rqi-5xvvdonptouwe.us-3.magentosite.cloud'
    ],
    'install' => [
        'date' => 'Tue, 22 Oct 2019 10:44:58 +0000'
    ],
    'static_content_on_demand_in_production' => 0,
    'force_html_minification' => 1,
    'cron_consumers_runner' => [
        'cron_run' => false,
        'max_messages' => 10000,
        'consumers' => [

        ]
    ],
    'queue' => [
        'consumers_wait_for_messages' => 0
    ],
    'system' => [
        'default' => [
            'catalog' => [
                'search' => [
                    'engine' => 'mysql'
                ]
            ],
            'dev' => [
                'js' => [
                    'minify_files' => '1',
                    'merge_files' => '1'
                ],
                'css' => [
                    'minify_files' => '1'
                ]
            ]
        ]
    ],
    'directories' => [
        'document_root_is_pub' => true
    ]
];
