<?php

namespace Ey\PromoCode\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
    
        $installer = $setup;
        $installer->startSetup();

		
		// Add pi_firstname field to quote, sales_order tables        
        
        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'ucc_promocode',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Promo Code',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'ucc_promocode',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Promo Code',
            ]
        );

        $setup->endSetup();
    }
}