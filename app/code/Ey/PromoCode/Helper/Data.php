<?php

/**
* Connect SignUp Widget
*
* @package Ey_PromoCode
* @author Himanshu Tongya
* @copyright Copyright (c) 2019 Massachusetts Medical Society. All rights reserved
* @license Commercial
*/

namespace Ey\PromoCode\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	/**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
    */
	public function __construct(
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
		\Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
		\Magento\Framework\Session\SessionManagerInterface $sessionManager,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Cms\Model\Page $cmsPage
    )
    {
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
        $this->scopeConfig = $scopeConfig;
        $this->cmsPage = $cmsPage;
        $this->urlInterface = $urlInterface;
        return parent::__construct($context);
    }
    
    
    public function setCustomCookie($cookiename,$value, $duration = 86400)
    {
        $metadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration($duration)
            ->setPath($this->sessionManager->getCookiePath())
            ->setDomain($this->sessionManager->getCookieDomain())
            ->setHttpOnly(true)
            ->setSecure(true);

        $this->cookieManager->setPublicCookie(
            $cookiename,
            $value,
            $metadata
        );
    }
    
    public function getCustomCookie($cookiename)
    {
        return $this->cookieManager->getCookie($cookiename);
    }
    
    public function deleteCustomCookie($cookiename)
    {
        return $this->cookieManager->deleteCookie($cookiename);
    }

	public function getPromoCodeUrlParam(){
		return  $this->scopeConfig->getValue('promocode/settings/url_parameter',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * ...
     * @return string
     */

    public function getPromoCodeExceptionPages(){
		return  $this->scopeConfig->getValue('promocode/settings/promocode_exception',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPromoCodeExceptionRegex(){
        return  $this->scopeConfig->getValue('promocode/settings/promocode_regex',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * ...
     * @return int
     */
    
    public function getPageID(){
        return $this->cmsPage->getId();
    }
}