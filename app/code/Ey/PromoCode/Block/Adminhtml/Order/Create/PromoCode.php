<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ey\PromoCode\Block\Adminhtml\Order\Create;

/**
 * Create order comment form
 *
 * @api
 * @author      Magento Core Team <core@magentocommerce.com>
 * @since 100.0.2
 */
class PromoCode extends \Magento\Sales\Block\Adminhtml\Order\Create\AbstractCreate
{
    /**
     * Data Form object
     *
     * @var \Magento\Framework\Data\Form
     */
    protected $_form;

    /**
     * Get header css class
     *
     * @return string
     */
    public function getHeaderCssClass()
    {
        return 'head-promocode';
    }

    /**
     * Get header text
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        return __('Promo Code');
    }

    /**
     * Get comment note
     *
     * @return string
     */
    public function getPromoCode()
    {
        return $this->escapeHtml($this->getQuote()->getData('ucc_promocode'));
    }
}
