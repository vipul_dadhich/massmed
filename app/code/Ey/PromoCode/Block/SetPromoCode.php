<?php
/**
 * Copyright © MassMed, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ey\PromoCode\Block;

/**
 * Create order comment form
 *
 * @api
 * @author      Vipul Dadhich <vipul@mms.org>
 * @since 100.0.2
 */

class SetPromoCode extends \Magento\Framework\View\Element\Template
{
    
	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Ey\PromoCode\Helper\Data $helper,
        array $data = []
    ) {
        $this->_helper = $helper;
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
    }

    public function getPromoCodeUrlParam(){
    	return $this->_helper->getPromoCodeUrlParam();
    }

    public function getPromoCode(){
    	$urlParam = $this->getPromoCodeUrlParam();
    	return $this->escapeHtml($this->getRequest()->getParam($urlParam));
    }

    public function getPageId(){
    	return $this->_helper->getPageID();
    }

    public function getExceptionPages(){
    	return $this->_helper->getPromoCodeExceptionPages();
    }

    public function getPromoCodeExceptionRegex(){
    	return $this->_helper->getPromoCodeExceptionRegex();
    }
}
