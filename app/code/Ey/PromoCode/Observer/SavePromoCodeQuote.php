<?php
namespace Ey\PromoCode\Observer;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class SavePromoCodeQuote
 * @package Ey\PromoCode\Observer
 */
class SavePromoCodeQuote implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * @var \Ey\PromoCode\Helper\Data
     */
    protected $helper;
    /**
     * @var \MMS\Logger\Model\Logger
     */
    protected $_logger;
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * SavePromoCodeQuote constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Ey\PromoCode\Helper\Data $helper
     * @param \MMS\Logger\Model\Logger $logger
     * @param RequestInterface $request
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Ey\PromoCode\Helper\Data $helper,
        \MMS\Logger\Model\Logger $logger,
        RequestInterface $request
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->helper = $helper;
        $this->_logger = $logger;
        $this->_request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_logger->debug("************ Started Calling SavePromocodeQuote *****************");
        try {
            $quote = $this->checkoutSession->getQuote();
            if ($quote && $quote->getId() && $quote->getIsActive() && $quote->hasItems()) {
                $promoCode = $this->_request->getParam($this->helper->getPromoCodeUrlParam(), false);
                if (!$promoCode && strval($this->getCookiePromoCode()) != '') {
                    $promoCode = $this->getCookiePromoCode();
                }

                if ((!$promoCode || $promoCode == '') && $this->getPromoCodeFromItemBuyRequest($quote)) {
                    $promoCode = $this->getPromoCodeFromItemBuyRequest($quote);
                }

                if (strval($promoCode) != "") {
                    $quote->setData('ucc_promocode', $promoCode);
                    $quote->save();

                    $this->helper->deleteCustomCookie('PromoCode');

                    $this->_logger->debug("Saved Promocode => " . $quote->getData('ucc_promocode'));
                }
            }
        } catch (NoSuchEntityException $e) {
        } catch (LocalizedException $e) {
        }

        $this->_logger->debug("************ Ended Calling SavePromocodeQuote *****************");

        return $this;
    }

    /**
     * @return string|null
     */
    protected function getCookiePromoCode()
    {
        $promoCode = $this->helper->getCustomCookie('PromoCode');
        if (!$promoCode && isset($_COOKIE['PromoCode']) && $_COOKIE['PromoCode'] != '') {
            $promoCode = $_COOKIE['PromoCode'];
        }
        return $promoCode;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return false|mixed
     */
    protected function getPromoCodeFromItemBuyRequest(\Magento\Quote\Model\Quote $quote)
    {
        foreach ($quote->getAllVisibleItems() as $item) {
            $option = $item->getOptionByCode('info_buyRequest');
            if ($option) {
                $buyRequest = \json_decode($option->getValue(), true);
                if (is_null($buyRequest)) {
                    $buyRequest = \unserialize($option->getValue());
                }

                if (isset($buyRequest['promocode']) && $buyRequest['promocode'] != '') {
                    return $buyRequest['promocode'];
                }
            }
        }
    }
}
