<?php

namespace Ey\PromoCode\Observer;

class SavePromoCodeOrder implements \Magento\Framework\Event\ObserverInterface
{
    protected $_request;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();
        $order = $observer->getOrder();
        $code = '';
        if($quote->getData('ucc_promocode') === NULL) {
            $code = $this->_request->getPost()['order']['ucc_promocode'];
        } else {
            $code = $quote->getData('ucc_promocode');
        }
        $order->setData('ucc_promocode', $code);

        return $this;
    }
}
