<?php

namespace Ey\PromoCode\Plugin;

class PromoCode extends \Magento\Framework\Model\AbstractModel
{
    
    public function afterCreateOrder(\Magento\Sales\Model\AdminOrder\Create $subject, $result)
    {

        $ucc_promocode = $subject->getData('ucc_promocode');
        $result->setData('ucc_promocode',$ucc_promocode);
        return $result;
    }

}