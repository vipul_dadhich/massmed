<?php 
namespace Ey\PromoCode\Plugin;

class PromoPlugin
{
    public function afterToHtml(\Magento\Sales\Block\Adminhtml\Order\Create\Form\Account $subject, $html)
    {
        $newBlockHtml = $subject->getLayout()->createBlock('\Ey\PromoCode\Block\Adminhtml\Order\Create\PromoCode')->setTemplate('Ey_PromoCode::ucc_promocode.phtml')->toHtml();

        return $html.$newBlockHtml;
    }
}