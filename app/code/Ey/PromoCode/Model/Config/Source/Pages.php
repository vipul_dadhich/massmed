<?php

/**
* Connect SignUp Widget
*
* @package Ey_PromoCode
* @author Himanshu Tongya
* @copyright Copyright (c) 2019 Massachusetts Medical Society. All rights reserved
* @license Commercial
*/

namespace Ey\PromoCode\Model\Config\Source;

class Pages implements \Magento\Framework\Option\ArrayInterface
{ 
    public function __construct(
        \Magento\Cms\Api\PageRepositoryInterface $pageRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
        ) {
            $this->_cmsPage = $pageRepository;
            $this->_search = $searchCriteriaBuilder;
        }

        /**
         * ...
         *
         * @return array
         */

        
        public function toOptionArray()
        {
            $pages = [];
            foreach($this->_cmsPage->getList($this->_getSearchCriteria())->getItems() as $page) {
                $pages[] = [
                'value' => $page->getId(),
                'label' => $page->getTitle()
                ];
            }
            return $pages;
        }
        
        protected function _getSearchCriteria()
        {
            return $this->_search->addFilter('is_active', '1')->create();
        }
}