/**
 * Copyright © 2018 MassMed. All rights reserved.
 * See LICENSE file for license details.
 */
define([
    'jquery',
    'jquery/jquery.cookie',
    'mage/url',
    'domReady!'
], function ($) {
    'use strict';

    return {
        options : {
            PageId: '',
            exceptionPages: '',
            PromoCode: '',
            exceptionPagesRegex: ''
        },
        _init: function(options){
            let pageurl =  $(location).attr('href');
            let pageUrlParts = pageurl.split('/');
            let excludePromoCode = false;
            this.options = options;
            this.options.exceptionPagesRegex = this.options.exceptionPagesRegex.length ? JSON.parse(this.options.exceptionPagesRegex): '';
            this.options.exceptionPages = this.options.exceptionPages.length ? JSON.parse(this.options.exceptionPages) : '';
            this.options.PageId = this.options.PageId.toString();

            // To exclude promocode for exception pages provided into admin text area
            for(let i=0;i<this.options.exceptionPagesRegex.length;i++){
                if($.inArray( this.options.exceptionPagesRegex[i], pageUrlParts )>=0){
                    excludePromoCode = true;
                }
            }
            if(!excludePromoCode){
                if(this.options.PageId!='' && this.options.exceptionPages.length>1){
                    if( $.inArray( this.options.PageId, this.options.exceptionPages ) == -1){
                        if(this.options.PromoCode.length > 0 ){
                            this._setPromoCodeCookie();
                            }
                        }
                    }else{
                    if(this.options.PromoCode.length >0 ){
                            this._setPromoCodeCookie();
                    }
                }
            }
        },
        _setPromoCodeCookie: function(){
            var baseurl =  $(location).attr('hostname');
            $.cookie("PromoCode", this.options.PromoCode, 
            { path: '/', 
            domain:  "."+baseurl ,
            secure: true,
            expires: 1 });
        }
    }
});