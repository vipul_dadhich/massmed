/**
 * Copyright © 2018 Scommerce Mage. All rights reserved.
 * See COPYING.txt for license details.
 */


var config = {
    map: {
        '*': {
            'promocode': 'Ey_PromoCode/js/promo-code'
        }
    }
};
