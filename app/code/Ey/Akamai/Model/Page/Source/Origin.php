<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ey\Akamai\Model\Page\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Origin
 */
class Origin implements OptionSourceInterface
{

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = array(
            'Store' => 'Store',
            'NEJM' => 'NEJM',
            'Catalyst' => 'Catalyst',
            'MyAccount' => 'MyAccount',
    );
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
