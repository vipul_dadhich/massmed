<?php

namespace Ey\Akamai\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	/**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
    */
	public function __construct(
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Customer\Model\CustomerFactory $customerFactory,
		\Magento\Customer\Model\Customer $customer,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Cms\Model\Page $page
    )
    {
    	$this->customerSession = $customerSession;
    	$this->scopeConfig = $scopeConfig;
    	$this->customerFactory = $customerFactory;
        $this->customer = $customer;
        $this->context = $context;
        $this->_page = $page;
        return parent::__construct($context);
    }
    
    
    public function getAkamaiClientId()
    {
        return $this->scopeConfig->getValue('akamai/api/client_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

	public function getCustomerSession(){
    	return $this->customerSession;
    }
	
	public function createCustomer($email,$firstName,$lastName,$password){
        try{
            $customer  = $this->customerFactory->create();
            $customer->setWebsiteId(1);
            $customer->setEmail($email); 
            $customer->setFirstname($firstName);
            $customer->setLastname($lastName);
            $customer->setPassword($password);
            $customer->save();
           
           	$customer->setConfirmation(null);
            $customer->save();
            
            return $customer;
        }catch(Exception $e){
            throw new \Magento\Framework\Exception\LocalizedException($e->getMessage());
        }
    }
 	
 	public function saveUcid($customer_id,$ucid){
        try{
        	$customer = $this->customer->load($customer_id);
        	$customerData = $customer->getDataModel();
        	$customerData->setCustomAttribute('ucid',$ucid);
    		$customer->updateData($customerData);
        	$customer->save();
        }catch(Exception $e){
            throw new \Magento\Framework\Exception\LocalizedException($e->getMessage());
        }
    }
    
    public function getWidgetURL()
    {
        return $this->scopeConfig->getValue('akamai/page_urls/widget_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    public function getWidgetOrigin()
    {
        $pageOrigin = 'Store';
        $currentFullAction = $this->context->getRequest()->getFullActionName();
        $cmspages = array('cms_index_index','cms_page_view');

        if(in_array($currentFullAction, $cmspages)){
            if ($this->_page->getMmswidgetOrigin()) {
                $pageOrigin = $this->_page->getMmswidgetOrigin();
            }
        }
        return $pageOrigin;
    }
}