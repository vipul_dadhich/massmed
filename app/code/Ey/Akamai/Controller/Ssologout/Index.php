<?php
/**
 * Magento logout
 *
 * @package     Ey_Akamai
 * @author      Dinesh Kaswan
 * @copyright   Copyright (c) 2019 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace Ey\Akamai\Controller\Ssologout;

class Index extends \Magento\Framework\App\Action\Action{
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->resultJsonFactory = $resultJsonFactory;
    }
    /**
     * Logout Customer event ajax call
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
    	$response = ['logout' => false];
        $customerId = $this->customerSession->getId();
        if($customerId) {
        	$this->customerSession->logout()->setLastCustomerId($customerId);
        	$response = [
        		'logout' => true
            ];
	    }
	    
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
