<?php
/**
 * Magento login
 *
 * @package     Ey_Akamai
 * @author      Dinesh Kaswan
 * @copyright   Copyright (c) 2019 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */

namespace Ey\Akamai\Controller\Ssologin;

use Ey\MuleSoft\Helper\CustomerHelper;
use Ey\MuleSoft\Model\Catalyst\Api\GetCustomerData;
use Ey\MuleSoft\Model\Core\Logger\Monolog;
use GuzzleHttp\ClientFactory;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\Helper\Data;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\StoreResolver;

/**
 * Class Index
 * @package Ey\Akamai\Controller\Ssologin
 */
class Index extends Action
{
    /**
     * @var Customer
     */
    protected $customerRepo;
    /**
     * @var Session
     */
    protected $customerSession;
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var StoreResolver
     */
    protected $storeResolver;
    /**
     * @var \Ey\MuleSoft\Helper\Data
     */
    protected $helper;
    /**
     * @var mixed
     */
    protected $isDebugMode;
    /**
     * @var bool
     */
    protected $isDebugModeType;
    /**
     * @var Monolog
     */
    protected $mulesoftLogger;
    /**
     * @var GetCustomerData
     */
    protected $getCustomerData;
    /**
     * @var Data
     */
    protected $jsonHelper;
    /**
     * @var CustomerHelper
     */
    protected $_customerHelper;
    /**
     * @var ClientFactory
     */
    protected $_clientFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param Customer $customerRepo
     * @param Session $customerSession
     * @param JsonFactory $resultJsonFactory
     * @param StoreManagerInterface $storeManager
     * @param StoreResolver $storeResolver
     * @param \Ey\MuleSoft\Helper\Data $helper
     * @param Monolog $mulesoftLogger
     * @param GetCustomerData $getCustomerData
     * @param Data $jsonHelper
     * @param CustomerHelper $customerHelper
     * @param ClientFactory $clientFactory
     */
    public function __construct(
        Context $context,
        Customer $customerRepo,
        Session $customerSession,
        JsonFactory $resultJsonFactory,
        StoreManagerInterface $storeManager,
        StoreResolver $storeResolver,
        \Ey\MuleSoft\Helper\Data $helper,
        Monolog $mulesoftLogger,
        GetCustomerData $getCustomerData,
        Data $jsonHelper,
        CustomerHelper $customerHelper,
        ClientFactory $clientFactory
    ) {
        parent::__construct($context);
        $this->customerRepo = $customerRepo;
        $this->customerSession = $customerSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->storeManager = $storeManager;
        $this->storeResolver = $storeResolver;
        $this->helper = $helper;
        $this->isDebugMode = $this->helper->getMuleSoftDebug();
        $this->isDebugModeType = $this->helper->getMuleSoftDebugMode();
        $this->mulesoftLogger = $mulesoftLogger;
        $this->getCustomerData = $getCustomerData;
        $this->jsonHelper = $jsonHelper;
        $this->_customerHelper = $customerHelper;
        $this->_clientFactory = $clientFactory;
    }


    /**
     * Login event ajax call
     * @return json object
     */
    public function execute()
    {
        $response = ['login' => false, 'reloadCustomerData' => false];
        $customerEmail = '';
        if ($this->customerSession->isLoggedIn()) {
            $customerEmail = $this->customerSession->getCustomer()->getEmail();
        }
        $postParams = $this->getRequest()->getPostValue();
        $email = $postParams['email'] ?? '';
        if (!$this->customerSession->isLoggedIn() || ($customerEmail && $customerEmail != $email)) {
            $accessToken = $postParams['accessToken'];
            $ucid = $postParams['ucid'];
            $storeId=$this->storeResolver->getCurrentStoreId();
            $store = $this->storeManager->getStore($storeId);
            $websiteId = $store->getWebsiteId();
            $customerRepo = $this->customerRepo->get($email);
            $customerRepo = $this->customerRepo;
            $customerRepo->setWebsiteId($websiteId);
            $customerRepo->loadByEmail($email);
            $this->customerSession->setAkamaiAccessToken($accessToken);
            if ($ucid) {
                if ($this->isDebugMode) {
                    $this->mulesoftLogger->mulesoftLog(" ***************************** SSO Login Start Here **************");
                    $this->mulesoftLogger->mulesoftLog(" ***************************** Get Customer API START **************");
                }
                $customerResponse = $this->getCustomerData->execute($ucid);
                if ($this->isDebugMode) {
                    $this->mulesoftLogger->mulesoftLog("Get Customer API Response Code: ".$customerResponse->getStatusCode());
                }
                if ($customerResponse->getStatusCode() == 200) {
                    $response = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                    if ($this->isDebugMode && $this->isDebugModeType) {
                        $this->mulesoftLogger->mulesoftLog("Get Customer API Response Body: ".json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($this->isDebugMode) {
                        $this->mulesoftLogger->mulesoftLog(" ***************************** Get Customer API END **************");
                    }
                    if ($customerRepo->getId()) {
                        $customerRepo = $this->helper->updateCustomer($response, $customerRepo);
                    } else {
                        $customerRepo = $this->helper->createCustomerLoginTime($response);
                    }
                }
            }

            if ($customerRepo->getId() && $accessToken && $ucid) {
                $responseData = $this->validateToken($accessToken, $ucid);
                if (isset($responseData['valid']) && $responseData['valid'] == true) {
                    $this->customerSession->setCustomerAsLoggedIn($customerRepo);
                    $this->triggerEvent($customerRepo);
                    $response = array('login' => true);
                } else {
                    $this->customerSession->logout()->setLastCustomerId($customerRepo->getId());
                }
            }
        } else if ($this->customerSession->isLoggedIn() && isset($postParams['ucid']) && $postParams['ucid'] != '') {
            $storeId = $this->storeResolver->getCurrentStoreId();
            $store = $this->storeManager->getStore($storeId);
            $websiteId = $store->getWebsiteId();
            $customerRepo = $this->customerRepo->setWebsiteId($websiteId)->loadByEmail($this->customerSession->getCustomer()->getEmail());
            if ($customerRepo->getId()) {
                $this->customerSession->setAkamaiAccessToken($postParams['accessToken']);
                $customerResponse = $this->getCustomerData->execute($postParams['ucid']);
                if ($customerResponse->getStatusCode() == 200) {
                    $_customerResponse = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                    $this->mulesoftLogger->mulesoftLog("Get Customer API Response Body: ".json_encode($_customerResponse, JSON_PRETTY_PRINT));
                    if (is_array($_customerResponse) && isset($_customerResponse['Customer']) && !empty($_customerResponse['Customer'])) {
                        $isCustomerUpdated = $this->_customerHelper->compareCustomerData($customerRepo->getData(), $_customerResponse['Customer']);
                        if ($isCustomerUpdated) {
                            $customerRepo = $this->helper->updateCustomer($_customerResponse, $customerRepo);
                            $this->mulesoftLogger->mulesoftLog("Get Customer ".json_encode($customerRepo->getData(), JSON_PRETTY_PRINT));
                            $this->triggerEvent($customerRepo);
                            $response = ['reloadCustomerData' => true];
                        }
                    }
                }
            }
        }

        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }

    /**
     * Validating UCID and Access Token from akamai.
     *
     * @param String $accessToken | $ucid
     * @return Object | bool
     * @throws LocalizedException
     */
    public function validateToken($accessToken, $ucid, array $headers = []){
        try {
            $apiBaseUrl = $this->helper->getMuleSoftURI();
            $accessTokenEndpoint = $this->helper->getMuleSoftTokenValidationURI();

            $headers = $this->prepareHeader($headers);
            $client = $this->_clientFactory->create(['config' => [
                'base_uri' => $apiBaseUrl,
                'headers' => $headers
            ]]);

            $customerData = array('token' => $accessToken, 'ucid' => $ucid);
            if($this->isDebugMode){
                $this->mulesoftLogger->mulesoftLog("Token Validation API Start: ");
                $this->mulesoftLogger->mulesoftLog("Token Validation Request Headers: ". json_encode($headers, JSON_PRETTY_PRINT));
                $this->mulesoftLogger->mulesoftLog("Token Validation Request URL: ".$apiBaseUrl.$accessTokenEndpoint);
                if ($this->isDebugModeType) {
                    $this->mulesoftLogger->mulesoftLog("Token Validation Request Payload: " . json_encode($customerData, JSON_PRETTY_PRINT));
                }
            }
            $response = $client->request(
                'POST',
                $accessTokenEndpoint,
                ['json' => $customerData]
            );
            if ($response->getStatusCode() == 200 ){
                $tokenValidationResponse = $this->jsonHelper->jsonDecode($response->getBody()->getContents());
                if ($this->isDebugMode) {
                    $this->mulesoftLogger->mulesoftLog("Token Validation Response Code: ".$response->getStatusCode());
                    if ($this->isDebugModeType) {
                        $this->mulesoftLogger->mulesoftLog("Token Validation Response ". \json_encode($tokenValidationResponse, JSON_PRETTY_PRINT));
                    }
                    $this->mulesoftLogger->mulesoftLog(" ***************************** Validate Token API END **************");
                    $this->mulesoftLogger->mulesoftLog(" ***************************** SSO Login Ends Here **************");
                }
                return $tokenValidationResponse;
            } else {
                return false;
            }
        } catch(\Exception $e){
            throw new LocalizedException(__($e->getMessage()));
        }
    }

    /**
     * @param $customerRepo
     */
    private function triggerEvent($customerRepo)
    {
        $this->_eventManager->dispatch('sso_login_after', [
            'customer' => $customerRepo,
            'session' => $this->customerSession
        ]);
    }

    /**
     * prepare header for request
     **/
    private function prepareHeader($headers){
        $headers['Content-Type'] = 'application/json';
        $headers['Accept'] = 'application/json';
        $headers['client_id'] = $this->helper->getMuleSoftClientId();
        $headers['client_secret'] = $this->helper->getMuleSoftClientSecret();
        return $headers;
    }
}
