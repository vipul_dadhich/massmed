<?php
/**
 * Magento Register
 *
 * @package     Ey_Akamai
 * @author      Dinesh Kaswan
 * @copyright   Copyright (c) 2019 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace Ey\Akamai\Controller\Ssoregister;

class Index extends \Magento\Framework\App\Action\Action{
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Customer $customerRepo,
        \Ey\MuleSoft\Helper\Data $MuleSoftHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\StoreResolver $storeResolver,
        \Ey\Akamai\Helper\Data $helper
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->customerRepo = $customerRepo;
        $this->helper = $helper;
        $this->mulesoft_helper = $MuleSoftHelper;
        $this->storeManager = $storeManager;
        $this->storeResolver = $storeResolver;
    }
    /**
     * Register Customer event ajax call
     * @return json object
     */
    public function execute()
    {
        $response = array('login' => false);
        if(!$this->customerSession->isLoggedIn()){
            $postParams = $this->getRequest()->getPostValue();
            $firstName = $postParams['firstName'];
            $lastName = $postParams['lastName'];
            $email = $postParams['email'];
            $password = $postParams['password'];
            $ucid = $postParams['ucid'];
            $storeId=$this->storeResolver->getCurrentStoreId();
            $store = $this->storeManager->getStore($storeId);
            $websiteId = $store->getWebsiteId();
            if(!$this->mulesoft_helper->customerExists($email, $websiteId)){
                $customerNew = $this->helper->createCustomer($email,$firstName,$lastName,$password);
                $this->helper->saveUcid($customerNew->getId(),$ucid);
            }
            $response = array('login' => true);
        } 

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
