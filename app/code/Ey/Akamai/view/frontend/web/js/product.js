require(['jquery'], function($){ 
	$(document).ready(function(){
		if (getUrlVar("url") != null) {
			var rurl = getUrlVar("url");
			var decodedUri = decodeURIComponent(rurl);
			if(validURL(decodedUri)){
				var html = "<a href='"+decodedUri+"' class='gobackurl' style='position: absolute;top: 5px;left: 5px;color: #000;text-decoration: underline;font-weight: 700;'><< Go Back</a>";
				$('.header.content').prepend(html);
			}
		}
	});
	function getUrlVars(){
		var vars = [], hash;
	  	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	  	for(var i = 0; i < hashes.length; i++){
	    	hash = hashes[i].split('=');
	    	vars.push(hash[0]);
	    	vars[hash[0]] = hash[1];
	  	}
	  	return vars;
	}
	function getUrlVar(name){
		return getUrlVars()[name];
	}
	function validURL(str) {
		var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
		  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
		  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
		  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
		  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
		  '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
		return !!pattern.test(str);
	}
});