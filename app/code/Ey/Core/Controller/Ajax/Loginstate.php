<?php
namespace Ey\Core\Controller\Ajax;
use Magento\Customer\Model\Session;
class Loginstate extends \Magento\Framework\App\Action\Action{    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        Session $customerSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->resultJsonFactory = $resultJsonFactory;
    }
    /**
     * Login state
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        if($this->customerSession->isLoggedIn()){
            $response = [
                'login' => true,
                'name' => $this->customerSession->getCustomer()->getName()
            ];
        } else{
            $response = [
                'login' => false,
            ];
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
