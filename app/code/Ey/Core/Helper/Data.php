<?php

namespace Ey\Core\Helper;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Sales\Model\OrderFactory;

/**
 * Class Data
 * @package Ey\Core\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Data constructor.
     * @param Context $request
     * @param OrderFactory $orderFactory
     * @param Configurable $configurable
     * @param ProductRepository $productRepository
     * @param Renderer $addressRenderer
     * @param Session $customerSession
     * @param Product $product
     * @param ScopeConfigInterface $scopeConfig
     * @param Registry $coreRegistry
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param CookieManagerInterface $cookieManager
     */
	public function __construct(
		Context $request,
		OrderFactory $orderFactory,
		Configurable $configurable,
		ProductRepository $productRepository,
		Renderer $addressRenderer,
        Session $customerSession,
        Product $product,
        ScopeConfigInterface $scopeConfig,
        Registry $coreRegistry,
        CookieMetadataFactory $cookieMetadataFactory,
        CookieManagerInterface $cookieManager
    ){
    	$this->_request = $request;
    	$this->_orderFactory = $orderFactory;
    	$this->_configurable = $configurable;
	    $this->_productRepository = $productRepository;
	    $this->_addressRenderer = $addressRenderer;
        $this->customerSession = $customerSession;
        $this->product = $product;
        $this->scopeConfig = $scopeConfig;
        $this->coreRegistry = $coreRegistry;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->cookieManager = $cookieManager;
    }

    /**
     * @return int
     */
    public function getIsCustomerLoggedIn(){
        if($this->customerSession->isLoggedIn()){
        	return 1;
        }
        return 0;
    }

    /**
     * @param $id
     * @return Product
     */
    public function loadProductById($id){
        $product = $this->product->load($id);
        return $product;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getConfigValue($id){
        return $this->scopeConfig->getValue($id, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $id
     * @return \Magento\Sales\Model\Order
     */
	public function getOrder($id)
	{
		$order = $this->_orderFactory->create()->loadByIncrementId($id);
		 return $order;
	}

    /**
     * @param $id
     * @return mixed
     */
	public function getIncrementOrder($id)
	{
		$order = $this->getOrder($id);
		 return $order->getEntityId();
	}

    /**
     * @param $productId
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductInfo($productId)
    {
    	$id = $productId;
		$parentConfigObject = $this->_configurable->getParentIdsByChild($productId);
	    if($parentConfigObject) {
			$id = $parentConfigObject[0];
	    }
	    $product = $this->_productRepository->getById($id);
	    $months = $product->getResource()->getAttribute('months')->getFrontend()->getValue($product);
	    $pdp_title = $product->getResource()->getAttribute('pdp_title')->getFrontend()->getValue($product);
		return [
			'sku' => $product->getSku(),
			'name' => $product->getName(),
			'regular_price' => $product->getPrice(),
			'special_price' => $product->getSpecialPrice(),
			'final_price' => $product->getFinalPrice(),
			'months' => $months,
			'pdp_title' => $pdp_title
		];
	}

    /**
     * @param $id
     * @return array
     */
	public function getOrderAmount($id)
	{
		$order = $this->getOrder($id);
		return [
			'subtotal' => $order->getSubtotal(),
			'tax' => $order->getTaxAmount(),
			'amount-paid' => $order->getGrandTotal()
		];
	}

    /**
     * @param $id
     * @return string|null
     */
	public function getBillingInformation($id)
	{
		$order = $this->getOrder($id);
		return  $this->_addressRenderer->format($order->getBillingAddress(), 'html');
	}

    /**
     * @param $id
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
	public function getPaymentTitle($id)
	{
		$order = $this->getOrder($id);
		if($order->getPayment()){
			return  $order->getPayment()->getMethodInstance()->getTitle();
		}else{
			return "";
		}

	}

	public function setCurrentOrder($order){
		if(!$this->getIsCustomerLoggedIn()){
			$this->coreRegistry->register('current_order', $order);
			$cookieValue = base64_encode($order->getProtectCode() . ':' . $order->getIncrementId());
			$metadata = $this->cookieMetadataFactory->createPublicCookieMetadata()->setPath('/')->setHttpOnly(true);
        	$this->cookieManager->setPublicCookie('guest-view', $cookieValue, $metadata);
			return 1;
		}
		return 0;
	}
}
