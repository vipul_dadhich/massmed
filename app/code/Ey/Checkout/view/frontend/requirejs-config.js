var config = {
    map: {
        '*': {
	        'Magento_Checkout/template/summary/item/details.html': 'Ey_Checkout/template/summary/item/details.html',
           	'Magento_Checkout/template/billing-address/details.html': 'Ey_Checkout/template/billing-address/details.html',
           	'Magento_Checkout/template/billing-address/form.html': 'Ey_Checkout/template/billing-address/form.html',
           	'Magento_Checkout/template/billing-address/actions.html': 'Ey_Checkout/template/core/billing-address/actions.html',
            'Magento_Checkout/template/payment.html': 'Ey_Checkout/template/core/payment.html',
            'Magento_Checkout/template/progress-bar.html': 'Ey_Checkout/template/core/progress-bar.html',
            'Magento_Checkout/template/sidebar.html': 'Ey_Checkout/template/core/sidebar.html',
            'Magento_Checkout/template/form/element/email.html': 'Ey_Checkout/template/core/form/element/email.html',
            'Magento_Checkout/template/minicart/item/default.html': 'Ey_Checkout/template/core/minicart/item/default.html',
            'Magento_Checkout/js/view/billing-address': 'Ey_Checkout/js/view/billing-address',
            'Magento_Checkout/js/view/form/element/email': 'Ey_Checkout/js/view/form/element/email',
            'Magento_Checkout/js/view/billing-address/list': 'Ey_Checkout/js/view/billing-address/list',
            'Magento_Checkout/js/view/summary/item/details/thumbnail': 'Ey_Checkout/js/view/summary/item/details/thumbnail',
            'Magento_Checkout/template/shipping-address/address-renderer/default.html':
                'Ey_Checkout/template/shipping-address/address-renderer/default.html',
        }
  },
  config: {
        mixins: {
            'Magento_Checkout/js/action/set-billing-address': {
                'Ey_Checkout/js/action/set-billing-address-mixin': true
            },
            'Magento_Checkout/js/action/set-payment-information-extended': {
                'Ey_Checkout/js/action/set-payment-information-extended-mixin': true
            },
            'Magento_Checkout/js/model/checkout-data-resolver': {
                'Ey_Checkout/js/model/checkout-data-resolver': true
            },
            'Magento_Checkout/js/action/set-shipping-information': {
                'Ey_Checkout/js/action/set-shipping-information-mixin': true
            },
            'Magento_Checkout/js/sidebar': {
                'Ey_Checkout/js/sidebar-mixin': true
            },
            'Magento_Checkout/js/action/create-shipping-address': {
                'Ey_Checkout/js/action/create-shipping-address-mixin': true
            },
            'Magento_Checkout/js/action/create-billing-address': {
                'Ey_Checkout/js/action/set-billing-address-mixin': true
            }
        }
    }
};
