define([
	'jquery',
    'Magento_Ui/js/form/element/abstract'
], function($,Component) {
    'use strict';

    return Component.extend({
		defaults: {
			template: 'Ey_Checkout/role',
            skipValidation: false,
            imports: {
                update: '${ $.parentName }.pi_professional_category:value'
            }
		},

        initialize: function () {
            this._super();
            return this;
        },
        /**
         * @param {String} value
         */
        update: function (value) {
            if (value === "STU" || value === "RES") {
                this.visible(false);
                this.value('RSD');
                if (value == 'STU') {
                    this.value('STU');
                }
            } else {
                this.visible(true);
                this.value('');
            }
        },
        /**
         * Form submit handler
         *
         * This method can have any name.
         */
        onSubmit: function() {

        }
    });
});
