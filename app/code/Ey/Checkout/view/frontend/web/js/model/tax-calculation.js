/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'jquery',
    'ko',
    'underscore',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/action/select-billing-address',
    'Magento_Checkout/js/model/postcode-validator',
    'Ey_Checkout/js/model/postcode-validator',
    'mage/translate',
    'uiRegistry',
    'mageUtils',
    'MMS_Tax/js/vat-validation',
    'Magento_Checkout/js/model/quote'
], function (
    $,
    ko,
    _,
    addressConverter,
    selectBillingAddress,
    postcodeValidator,
    eyPostcodeValidator,
    $t,
    uiRegistry,
    utils,
    vatTax
) {
    'use strict';

    var checkoutConfig = window.checkoutConfig,
        observedElements = [],
        postcodeElements = [],
        postcodeElementName = 'postcode',
        vatIdErrorSelector = '.vat-id-error',
        vatIdErrorBillingSelector = '.vat-id-error-billing',
        vatIdErrorShippingSelector = '.vat-id-error-shipping',
        billingAddressSharedVatIdElement = $('div[name="billingAddressshared\\.vat_id"] input'),
        shippingAddressVatIdElement = 'div[name="shippingAddress\\.vat_id"] input';


    return {
        validateAddressTimeout: 0,
        validateZipCodeTimeout: 0,
        validateDelay: 2000,
        validationErrors: [],
        billingFormPath: 'checkout.steps.billing-step.payment.afterMethods.billing-address-form.form-fields',
        mailingFormPath: 'checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset',

        /**
         * @param {Object} address
         * @return {Boolean}
         */
        validateAddressData: function (address) {
            return this.validate(address);
        },

        /**
         * @param {Object} address
         * @return {Boolean}
         */
        validate: function (address, isMailingAddress = false) {
            var self = this,
                rules = this.getRules(),
                vatCountries = ["AT","BE","DK","FR","DE","GR","IE","IT","NL","PT","ES","SE","GB"];

            this.validationErrors = [];
            $.each(rules, function (field, rule) {
                var message;
                if (field === 'vat_id' && !utils.isEmpty(address[field])
                    && $.inArray(address['country_id'], vatCountries) >= 0) {
                    let vatIdValidate = vatTax.checkVat(address[field], address['country_id']);
                    if (!vatIdValidate) {
                        if (!$(vatIdErrorBillingSelector).length > 0) {
                            $(billingAddressSharedVatIdElement).after(
                                "<div class='field-error vat-id-error vat-id-error-billing' generated='true'>" +
                                "<span>Please enter a valid VAT Number.</span>" +
                                "</div>"
                            );
                        }

                        if (!$(vatIdErrorShippingSelector).length > 0 && isMailingAddress) {
                            $(shippingAddressVatIdElement).after(
                                "<div class='field-error vat-id-error vat-id-error-shipping' generated='true'>" +
                                "<span>Please enter a valid VAT Number.</span>" +
                                "</div>"
                            );
                        }

                        message = $t('Please enter a valid VAT Number.');
                        self.validationErrors.push(message);
                    }
                }

                let validatePostCode = true;
                if (field === 'postcode') {
                    validatePostCode = self.isValidatePostCode(isMailingAddress, address['country_id']);
                    if (!validatePostCode) {
                        rule.required = false;
                    }
                }

                if (field === 'postcode' && !utils.isEmpty(address[field]) && validatePostCode) {
                    if (!eyPostcodeValidator.validate(address[field], address['country_id'])) {
                        message = $t('Provided Zip/Postal Code seems to be invalid.');
                        self.validationErrors.push(message);
                    }

                    /*if (!postcodeValidator.validate(address[field], address['country_id'])) {
                        message = $t('Provided Zip/Postal Code seems to be invalid.');
                        self.validationErrors.push(message);
                    }*/
                }

                if (rule.required && utils.isEmpty(address[field])) {
                    message = $t('Field ') + field + $t(' is required.');
                    self.validationErrors.push(message);
                }
            });

            return !this.validationErrors.length;
        },

        /**
         * Perform postponed binding for fieldset elements
         *
         * @param {String} formPath
         */
        initFields: function (formPath) {
            var self = this,
                elements = this.getObservableFields();

            if ($.inArray(postcodeElementName, elements) === -1) {
                // Add postcode field to observables if not exist for zip code validation support
                elements.push(postcodeElementName);
            }

            $.each(elements, function (index, field) {
                uiRegistry.async(formPath + '.' + field)(self.doElementBinding.bind(self));
            });

            if (formPath) {
                this.billingFormPath = formPath;
            }
        },

        getRules: function() {
            let elements = {},
            observableFields = this.getObservableFields();
            $.each(observableFields, function (index, field) {
                elements[field] = [];
                if (field !== 'vat_id' && field !== 'region_id') {
                    elements[field]['required'] = true;
                }
            });

            return elements;
        },

        getObservableFields: function() {
            let elements = [];
            elements.push('country_id');
            elements.push('postcode');
            elements.push('city');
            elements.push('region_id');
            elements.push('vat_id');
            return elements;
        },

        /**
         * Bind Tax rate request to form element
         *
         * @param {Object} element
         * @param {Boolean} force
         * @param {Number} delay
         */
        doElementBinding: function (element, force, delay) {
            var observableFields = this.getObservableFields();

            if (element && (observableFields.indexOf(element.index) !== -1 || force)) {
                if (element.index !== postcodeElementName) {
                    this.bindHandler(element, delay);
                }
            }

            if (element.index === postcodeElementName) {
                this.bindHandler(element, delay);
                postcodeElements.push(element);
            }
        },

        /**
         * @param {*} elements
         * @param {Boolean} force
         * @param {Number} delay
         */
        bindChangeHandlers: function (elements, force, delay) {
            var self = this;

            $.each(elements, function (index, elem) {
                self.doElementBinding(elem, force, delay);
            });
        },

        /**
         * @param {Object} element
         * @param {Number} delay
         */
        bindHandler: function (element, delay) {
            var self = this;

            delay = typeof delay === 'undefined' ? self.validateDelay : delay;

            element.on('value', function () {
                clearTimeout(self.validateZipCodeTimeout);
                self.validateZipCodeTimeout = setTimeout(function () {
                    if (element.index === postcodeElementName) {
                        self.postcodeValidation(element);
                    } else {
                        $.each(postcodeElements, function (index, elem) {
                            self.postcodeValidation(elem);
                        });
                    }
                }, delay);

                clearTimeout(self.validateAddressTimeout);
                self.validateAddressTimeout = setTimeout(function () {
                    self.validateFields();
                }, delay);
            });
            observedElements.push(element);
        },

        /**
         * @return {*}
         */
        postcodeValidation: function (postcodeElement) {
            var countryId = $('div[name="billingAddressshared\\.country_id"] select[name="country_id"]:visible').val(),
                validationResult,
                errorMessage,
                warnMessage;

            if (postcodeElement == null || postcodeElement.value() == null) {
                return true;
            }

            postcodeElement.error(null);
            postcodeElement.warn(null);

            validationResult = eyPostcodeValidator.validate(postcodeElement.value(), countryId);
            if (!validationResult) {
                errorMessage = $t('Please enter a valid Zip/Postal Code for your region.');
                postcodeElement.error(errorMessage);
            }

            /*if (validationResult) {
                validationResult = postcodeValidator.validate(postcodeElement.value(), countryId);
                if (!validationResult) {
                    warnMessage = $t('Provided Zip/Postal Code seems to be invalid.');
                    if (postcodeValidator.validatedPostCodeExample.length) {
                        warnMessage += $t(' Example: ') + postcodeValidator.validatedPostCodeExample.join('; ') + '. ';
                    }

                    warnMessage += $t('If you believe it is the right one you can ignore this notice.');
                    postcodeElement.error(null);
                    postcodeElement.warn(null);
                    postcodeElement.warn(warnMessage);
                }
            }*/

            return validationResult;
        },

        /**
         * Convert form data to quote address and validate fields for tax rates
         */
        validateFields: function () {
            var addressFlat = addressConverter.formDataProviderToFlatData(
                this.collectObservedData(),
                'billingAddressshared'
                ),
                address;

            if (this.validateAddressData(addressFlat)) {
                addressFlat = uiRegistry.get('checkoutProvider').billingAddressshared;
                address = addressConverter.formAddressDataToQuoteAddress(addressFlat);
                address.trigger_tax = true;
                selectBillingAddress(address);
            }
        },

        /**
         * Collect observed fields data to object
         *
         * @returns {*}
         */
        collectObservedData: function () {
            var observedValues = {};

            $.each(observedElements, function (index, field) {
                observedValues[field.dataScope] = field.value();
            });

            return observedValues;
        },

        isValidatePostCode: function (isMailingAddress, value) {
            let parentName = this.billingFormPath;
            if (isMailingAddress) {
                parentName = this.mailingFormPath;
            }

            var country = uiRegistry.get(parentName + '.' + 'country_id'),
                options = country.indexedOptions,
                option = null;

            if (!value) {
                return true;
            }

            option = options[value];

            if (!option) {
                return true;
            }

            if (option['is_zipcode_optional']) {
                return false;
            }

            return true;
        }
    };
});
