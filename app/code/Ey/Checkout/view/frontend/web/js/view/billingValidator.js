define([
        'uiComponent',
        'Magento_Checkout/js/model/default-validation-rules',
        'Ey_Checkout/js/model/billingValidator'
    ],
    function (Component, additionalValidators, billingValidator) {
        'use strict';
        additionalValidators.registerValidator(billingValidator);

        return Component.extend({});
    }
);
