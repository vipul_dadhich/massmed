/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'ko',
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'underscore'
], function (ko, Component, customerData, _) {
    'use strict';

    var imageData = window.checkoutConfig.imageData;
    var imageDataConfig = window.checkoutConfig.imageData;
    var imageDataArr = ko.observable(window.checkoutConfig.imageData);

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/summary/item/details/thumbnail'
        },
        displayArea: 'before_details',
        imageDataConfig: imageDataConfig,
        imageDataArr: imageDataArr,
        imageData: imageData,

        /**
         * @override
         */
        initialize: function () {
            this._super();
            this.cart = customerData.get('cart');
            this.cart.subscribe(function (updatedCart) {
                this.setImagesData(this.isImageDataApplied(updatedCart.items));
            }, this);
        },

        /**
         * Determine if subtotal should be hidden.
         * @param {Array} cartItems
         * @return {Boolean}
         */
        isImageDataApplied: function (cartItems) {
            var self = this;
            var images = _.map(cartItems, function(item, obj) {
                if (_.has(item, 'product_image')) {
                    var imageDataEle = {};
                    imageDataEle[item['item_id']] = item['product_image'];
                    self.imageDataConfig[item['item_id']] = item['product_image'];
                    return imageDataEle;
                }
            });
            return self.imageDataConfig;
        },

        /**
         * Set image items to observable field
         *
         * @param {Object} items
         */
        setImagesData: function (items) {
            this.imageDataArr(items);
        },

        /**
         * Get image items to observable field
         * @return {Array}
         */
        getImagesData: function () {
            return this.imageDataArr();
        },

        /**
         * @param {Object} item
         * @return {Array}
         */
        getImageItem: function (item) {
            let imageData = this.getImagesData();
            if (imageData[item['item_id']]) {
                return imageData[item['item_id']];
            }

            return [];
        },

        /**
         * @param {Object} item
         * @return {null}
         */
        getSrc: function (item) {
            let imageData = this.getImagesData();
            if (imageData[item['item_id']]) {
                return imageData[item['item_id']].src;
            }

            return null;
        },

        /**
         * @param {Object} item
         * @return {null}
         */
        getWidth: function (item) {
            let imageData = this.getImagesData();
            if (imageData[item['item_id']]) {
                return imageData[item['item_id']].width;
            }

            return null;
        },

        /**
         * @param {Object} item
         * @return {null}
         */
        getHeight: function (item) {
            let imageData = this.getImagesData();
            if (imageData[item['item_id']]) {
                return imageData[item['item_id']].height;
            }

            return null;
        },

        /**
         * @param {Object} item
         * @return {null}
         */
        getAlt: function (item) {
            let imageData = this.getImagesData();
            if (imageData[item['item_id']]) {
                return imageData[item['item_id']].alt;
            }

            return null;
        }
    });
});
