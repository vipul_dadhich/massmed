define([
        'jquery',
        'Magento_Checkout/js/model/quote',
        'mage/storage',
        'mage/url',
        'mage/cookies',
        'mage/validation'
    ],
    function ($, quote, storage, urlBuilder) {
        'use strict';

        return {
            /**
             * Validate checkout billing address
             *
             * @returns {Boolean}
             */
            validate: function () {
                var self = this,
                    billingAddressIdSelector = 'select[name="billing_address_id"]',
                    billingAddressFormErrorSelector = 'billing-address-form .field-error',
                    catalystSoiSelector = '#catalyst_soi',
                    catalystConnectSelector = '#catalyst_connect',
                    piFirstNameElement = $('#pi_firstname'),
                    piLastNameElement = $('#pi_lastname'),
                    piOrganizationElement = $('#pi_organization'),
                    piRoleElement = $('#pi_role'),
                    piSuffixElement = $('#pi_suffix'),
                    piPrimarySpecialtyElement = $('#pi_primaryspecialty'),
                    piProfessionalCategoryElement = $('#pi_professional_category'),
                    piStudentTypeElement = $('#pi_student_type'),
                    piProfessionElement = $('#pi_profession'),
                    errorElementSelector = 'form.form.form-login .mage-error:visible',
                    validationResult = true,
                    catalyst_soi = 1,
                    catalyst_connect = 1;

                if ($(billingAddressIdSelector).length) {
                    if (strtolower($(billingAddressIdSelector + ' option:selected').text()) === "new address"
                        && $(billingAddressFormErrorSelector).length > 0) {
                        $('html, body').animate({
                            scrollTop: $(billingAddressFormErrorSelector).first().offset().top - 200
                        }, 1000);
                        return false;
                    } else {
                        if (catalystSoiSelector.length) {
                            catalyst_soi = $(catalystSoiSelector).is(':checked') ? 1 : 0;
                        }

                        if ($(catalystConnectSelector).length) {
                            catalyst_connect = $(catalystConnectSelector).is(':checked') ? 1 : 0;
                        }

                        return true;
                    }
                } else {
                    if (piFirstNameElement.length > 0) {
                        if (!$.validator.validateSingleElement(piFirstNameElement)) {
                            validationResult = false;
                        }
                    }

                    if (piLastNameElement.length > 0) {
                        if (!$.validator.validateSingleElement(piLastNameElement)) {
                            validationResult = false;
                        }
                    }

                    if (piOrganizationElement.length) {
                        if (!$.validator.validateSingleElement(piOrganizationElement)) {
                            validationResult = false;
                        }
                    }

                    if (piRoleElement.length) {
                        if (!$.validator.validateSingleElement(piRoleElement)) {
                            validationResult = false;
                        }
                    }

                    if (piSuffixElement.length) {
                        if(!$.validator.validateSingleElement(piSuffixElement)){
                            validationResult = false;
                        }
                    }
                    if (piProfessionalCategoryElement.length) {
                        if(!$.validator.validateSingleElement(piProfessionalCategoryElement)){
                            validationResult = false;
                        } else {
                            
                            if(piProfessionalCategoryElement.val() === "PHY" ||
                                piProfessionalCategoryElement.val() === "RES"){
                                if(piPrimarySpecialtyElement.length){
                                    if (!$.validator.validateSingleElement(piPrimarySpecialtyElement)) {
                                        validationResult = false;
                                    }
                                }
                            }
                            if(piProfessionalCategoryElement.val() === "STU"){
                                if(piStudentTypeElement.length){
                                    if (!$.validator.validateSingleElement(piStudentTypeElement)) {
                                        validationResult = false;
                                    }
                                } else {
                                    if(piStudentTypeElement.val() === "OTH"){
                                        if(piProfessionElement.length){
                                            if (!$.validator.validateSingleElement(piProfessionElement)) {
                                                validationResult = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!validationResult) {
                        $('html, body').animate({
                            scrollTop: $(errorElementSelector).first().offset().top - 100
                        }, 1000);
                    } else {
                        var billingAddress = quote.billingAddress();
                        var ba_password = "";

                        if (billingAddress['customAttributes'] !== undefined) {
                            var customAttributes = billingAddress['customAttributes'];
                            if (customAttributes.length > 0) {
                                for (var i=0;i<customAttributes.length;i++) {
                                    if (customAttributes[i]['attribute_code'] === "ba_password") {
                                        ba_password = customAttributes[i]['value'];
                                        break;
                                    }
                                }
                            }
                        }

                        if (catalystSoiSelector.length) {
                            catalyst_soi = $(catalystSoiSelector).is(':checked') ? 1 : 0;
                        }

                        if ($(catalystConnectSelector).length) {
                            catalyst_connect = $(catalystConnectSelector).is(':checked') ? 1 : 0;
                        }

                        return true;
                    }
                }
            },

            storagePost: function (url, data) {
                return true;
            }
        };
    }
);
