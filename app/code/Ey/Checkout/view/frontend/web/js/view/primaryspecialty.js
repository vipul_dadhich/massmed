define([
	'jquery',
    'Magento_Ui/js/form/element/abstract'
], function($,Component) {
    'use strict';

    return Component.extend({
		defaults: {
			template: 'Ey_Checkout/primaryspecialty',
            skipValidation: false,
            imports: {
                update: '${ $.parentName }.pi_professional_category:value'
            }
		},

        initialize: function () {
            this._super();
            return this;
        },

        /**
         * @param {String} value
         */
        update: function (value) {
            if (value === "PHY" || value === "RES") {
                this.visible(true);
                this.required(true);
            } else {
                this.visible(false);
                this.value('');
                this.validation['required-entry'] = false;
                this.required(false);
            }
        }
    });
});
