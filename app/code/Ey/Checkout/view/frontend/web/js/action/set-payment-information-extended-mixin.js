define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, customer, quote) {
    'use strict';

    return function (setPaymentInformationExtended) {
        return wrapper.wrap(setPaymentInformationExtended, function (originalAction, messageContainer, paymentData, skipBilling) {

            var billingAddress = quote.billingAddress(),
                catalystConnectElement = $('#catalyst_connect'),
                catalystSoiElement = $('#catalyst_soi'),
                shipAddrSameAsBillElement = $('#shipping-address-same-as-billing');

            if (billingAddress !== undefined) {
                var mailingAddressCode = 'C1';
                if (shipAddrSameAsBillElement.length > 0 && shipAddrSameAsBillElement.is(":checked")) {
                    mailingAddressCode = 'C1';
                } else if (shipAddrSameAsBillElement.length > 0 && !shipAddrSameAsBillElement.is(":checked")) {
                    mailingAddressCode = 'C2';
                }
                if (billingAddress.customAttributes !== undefined) {
                    $.each(billingAddress.customAttributes, function (key, value) {
                        if(value['attribute_code'] == 'customer_catalyst_connect') {
                            value['value'] = (catalystConnectElement.is(':checked')) ? 1 : '';
                            billingAddress.customAttributes[key] = value;
                        }
                        if(value['attribute_code'] == 'customer_catalyst_soi') {
                            value['value'] = (catalystSoiElement.is(':checked')) ? 1 : '';
                            billingAddress.customAttributes[key] = value;
                        }
                        if (billingAddress.extension_attributes !== undefined) {
                            billingAddress['extension_attributes'][value['attribute_code']] = value['value'];
                            if(value['attribute_code'] === 'customer_catalyst_connect'){
                                billingAddress['extension_attributes'][value['attribute_code']] = (catalystConnectElement.is(':checked')) ? 1 : '';
                            }
                            if(value['attribute_code'] === 'customer_catalyst_soi'){
                                billingAddress['extension_attributes'][value['attribute_code']] = (catalystSoiElement.is(':checked')) ? 1 : '';
                            }
                        }
                    });
                }

                if (billingAddress.customAttributes === undefined) {
                    billingAddress.customAttributes = {};
                }

                billingAddress.customAttributes['billing_address_code'] = 'C1';
                billingAddress.customAttributes['mailing_address_code'] = mailingAddressCode;
                if (billingAddress.extension_attributes !== undefined) {
                    billingAddress['extension_attributes']['billing_address_code'] = 'C1';
                    billingAddress['extension_attributes']['mailing_address_code'] = mailingAddressCode;
                }
            }

            return originalAction(messageContainer, paymentData, skipBilling);
        });
    };
});
