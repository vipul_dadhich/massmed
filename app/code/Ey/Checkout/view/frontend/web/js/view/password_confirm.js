define([
	'jquery',
    'Magento_Ui/js/form/form'
], function($,Component) {
    'use strict';

    return Component.extend({
		defaults: {
			template: 'Ey_Checkout/password_confirm'
		},

        initialize: function () {
            this._super();
            return this;
        }
    });
});
