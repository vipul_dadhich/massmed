define([
        'jquery',
        'Magento_Checkout/js/model/quote',
        'mage/storage',
        'MMS_Tax/js/vat-validation',
        'mage/validation'
    ],
    function ($,quote,storage, vatTax) {
        'use strict';
        return {
            validate: function () {
                var validationResult = true,
                    zipValidationResult = true,
                    customerEmailElement = $('#customer-email'),
                    billingPostcodeElement = $('.billing-address-form input[name="postcode"]'),
                    billingCountryIdElement = $('.billing-address-form select[name="country_id"]'),
                    billingAddressSharedVatIdElement = $('div[name="billingAddressshared\\.vat_id"] input'),
                    billingAddressSharedCountryIdElement = $('div[name="billingAddressshared\\.country_id"] select'),
                    errorElementSelector = 'form.form.form-login .mage-error:visible',
                    FormLoginFieldErrorSelector = '.form-login .field-error',
                    zipCodeErrorSelector = '.zip-code-error',
                    vatIdErrorSelector = '.vat-id-error',
                    inputPostcodeSelector = 'input[name="postcode"]';
					
                if (billingPostcodeElement.length > 0 && billingPostcodeElement.val() !== "") {
                    var piPostcodeValidate = true;

                    if (billingCountryIdElement.val() === "US") {
                        piPostcodeValidate = /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(billingPostcodeElement.val());

                    } else if(billingCountryIdElement.val() === "CA") {
                        piPostcodeValidate = /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/.test(billingPostcodeElement.val());
                    } else {
                        piPostcodeValidate = billingPostcodeElement.val().trim().length <=9 ? true : false;
                    }

                    if (!piPostcodeValidate) {
                        zipValidationResult = false;
                    } else {
                        zipValidationResult = true;
                    }
                }

                var vatIdValidate = true;
                var countryId = billingAddressSharedCountryIdElement.val();
                var VATNumber = billingAddressSharedVatIdElement.val().toUpperCase();
                if (billingAddressSharedVatIdElement.length > 0 && billingAddressSharedVatIdElement.val() !== "") {
                    vatIdValidate = vatTax.checkVat(VATNumber, countryId);
                }

                if (!validationResult) {
                    $('html, body').animate({
                        scrollTop: $(errorElementSelector).first().offset().top - 100
                    }, 1000);
                } else if ($(FormLoginFieldErrorSelector).length > 0) {
                    $('html, body').animate({
                        scrollTop: $(FormLoginFieldErrorSelector).first().offset().top - 100
                    }, 1000);
                } else if (!zipValidationResult) {
                    if (!$(zipCodeErrorSelector).length > 0) {
                        let alertDiv = $(inputPostcodeSelector).find('.field-error');
                        if (alertDiv.length > 0) {
                            alertDiv.remove();
                        }
                        $(inputPostcodeSelector).after(
                            "<div class='field-error zip-code-error' generated='true'>" +
                            "<span>Please enter a valid Zip/Postal Code for your region.</span>" +
                            "</div>"
                        );
                    }
                    $('html, body').animate({
                        scrollTop: $(zipCodeErrorSelector).first().offset().top - 100
                    }, 1000);
                } else if ($(zipCodeErrorSelector).length > 0) {
                    $('html, body').animate({
                        scrollTop: $(zipCodeErrorSelector).first().offset().top - 100
                    }, 1000);
                } else if (!vatIdValidate) {
                    if(!$(vatIdErrorSelector).length > 0){
                        $(billingAddressSharedVatIdElement).after(
                            "<div class='field-error vat-id-error' generated='true'>" +
                            "<span>Please enter a valid VAT Number.</span>" +
                            "</div>"
                        );
                    }
                    $('html, body').animate({
                        scrollTop: $(vatIdErrorSelector).first().offset().top - 100
                    }, 1000);
                } else if ($(vatIdErrorSelector).length > 0) {
                    $('html, body').animate({
                        scrollTop: $(vatIdErrorSelector).first().offset().top - 100
                    }, 1000);
                } else {
                    return true;
                }
            }
        };
    }
);
