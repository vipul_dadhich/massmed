define([
	'jquery',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'underscore'
], function($, registry, Component, _) {
    'use strict';

    return Component.extend({
		defaults: {
			template: 'Ey_Checkout/profession',
            skipValidation: false,
            imports: {
                update: '${ $.parentName }.pi_professional_category:value'
            }
		},
        /**
         * @param {String} value
         */
        update: function (value) {
            if (value === "OTH") {
                this.visible(true);
                this.validation['required-entry'] = true;
                this.required(true);
            } else {
                this.visible(false);
                this.value('');
                this.validation['required-entry'] = false;
                this.required(false);
            }
        },
        getProfessionOptions: function () {
            var result = [];
            var professionalData = window.checkoutConfig.customerData.profession;
            _.each(professionalData, function (pData) {
                result.push({
                    'pCode': pData.value,
                    'pName': pData.label
                });
            });

            return result;
        },
        initialize: function () {
            this._super();
            return this;
        }
    });
});
