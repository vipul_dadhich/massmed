define([
	'jquery',
    'Magento_Ui/js/form/form'
], function($,Component) {
    'use strict';

    return Component.extend({
		defaults: {
			template: 'Ey_Checkout/password'
		},

        initialize: function () {
            this._super();
            return this;
        }
    });
});
