define([
	'jquery',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'underscore'
], function($, registry, Component, _) {
    'use strict';

    return Component.extend({
		defaults: {
			template: 'Ey_Checkout/studenttype',
            skipValidation: false,
            imports: {
                update: '${ $.parentName }.pi_professional_category:value'
            }
		},
        /**
         * @param {String} value
         */
        update: function (value) {
            if (value === "STU") {
                this.visible(true);
                this.validation['required-entry'] = true;
                this.required(true);
            } else {
                this.visible(false);
                this.value('');
                this.validation['required-entry'] = false;
                this.required(false);
            }
        },
        getStudentTypeOptions: function () {
            var result = [];
            var studentTypeData = window.checkoutConfig.customerData.student_type_options;
            _.each(studentTypeData, function (studentOption) {
                result.push({
                    'studentCode': studentOption.value,
                    'studentName': studentOption.label
                });
            });

            return result;
        },
        initialize: function () {
            this._super();
            return this;
        }
    });
});
