define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, customer, quote) {
    'use strict';

    return function (setBillingAddressAction) {
        return wrapper.wrap(setBillingAddressAction, function (originalAction, messageContainer) {

            var billingAddress = quote.billingAddress(),
                shippingAddress = quote.billingAddress(),
                piFirstNameElement = $('#pi_firstname'),
                piLastNameElement = $('#pi_lastname'),
                piCustomerEmailElement = $('#customer-email'),
                catalystConnectElement = $('#catalyst_connect'),
                nejmSoiElement = $('#nejm_soi'),
                nejmEtocElement = $('#nejm_etoc'),
                catalystSoiElement = $('#catalyst_soi'),
                shipAddrSameAsBillElement = $('#shipping-address-same-as-billing');

            if (billingAddress !== undefined && billingAddress !== '' && billingAddress !== null) {
                if (piFirstNameElement.val() !== undefined) {
                    billingAddress['firstname']= piFirstNameElement.val();
                }

                if (piLastNameElement.val() !== undefined) {
                    billingAddress['lastname']= piLastNameElement.val();
                }

                //For non-logged in customers
                if (!customer.isLoggedIn()) {
                    billingAddress['email'] = piCustomerEmailElement.val();
                }

                if (billingAddress['extension_attributes'] === undefined) {
                    billingAddress['extension_attributes'] = {};
                }

                if (billingAddress.customAttributes === undefined) {
                    billingAddress.customAttributes = {};
                }

                let billingAddressCode = 'C1', mailingAddressCode = 'C2';
                $.each(billingAddress.customAttributes, function (key, value) {
                    if (value['attribute_code'] === 'addresscode') {
                        if($.isPlainObject(value)){
                            value = value['value'];
                        }
                        billingAddressCode = value;
                        return false;
                    }
                });
                if (shippingAddress.customAttributes !== undefined) {
                    $.each(shippingAddress.customAttributes, function (key, value) {
                        if (value['attribute_code'] === 'addresscode') {
                            if($.isPlainObject(value)){
                                value = value['value'];
                            }
                            mailingAddressCode = value;
                            return false;
                        }
                    });
                }

                if (shipAddrSameAsBillElement.length > 0 && shipAddrSameAsBillElement.is(":checked")) {
                    mailingAddressCode = billingAddressCode;
                }

                billingAddress.customAttributes['billing_address_code'] = {'attribute_code': 'billing_address_code','value': billingAddressCode};
                billingAddress.customAttributes['mailing_address_code'] = {'attribute_code': 'mailing_address_code','value': mailingAddressCode};

                if (billingAddress.customAttributes !== undefined) {
                    $.each(billingAddress.customAttributes, function (key, value) {
                        if (value['attribute_code'] === 'customer_catalyst_soi') {
                            value['value'] = (catalystSoiElement.is(':checked')) ? 1 : '';
                            billingAddress.customAttributes[key] = value;
                        } else if (value['attribute_code'] === 'customer_catalyst_connect') {
                            value['value'] = (catalystConnectElement.is(':checked')) ? 1 : '';
                            billingAddress.customAttributes[key] = value;
                        } else if (value['attribute_code'] === 'nejm_soi') {
                            value['value'] = (nejmSoiElement.is(':checked')) ? 1 : '';
                            billingAddress.customAttributes[key] = value;
                        } else if (value['attribute_code'] === 'nejm_etoc') {
                            value['value'] = (nejmEtocElement.is(':checked')) ? 1 : '';
                            billingAddress.customAttributes[key] = value;
                        }

                        let formatValue = value
                        if($.isPlainObject(value)){
                            formatValue = value['value'];
                        }
                        billingAddress['extension_attributes'][value['attribute_code']] = formatValue;
                        if(value['attribute_code'] === 'customer_catalyst_connect'){
                            billingAddress['extension_attributes'][value['attribute_code']] = (catalystConnectElement.is(':checked')) ? 1 : '';
                        } else if (value['attribute_code'] === 'customer_catalyst_soi') {
                            billingAddress['extension_attributes'][value['attribute_code']] = (catalystSoiElement.is(':checked')) ? 1 : '';
                        } else if (value['attribute_code'] === 'nejm_soi') {
                            billingAddress['extension_attributes'][value['attribute_code']] = (nejmSoiElement.is(':checked')) ? 1 : '';
                        } else if (value['attribute_code'] === 'nejm_etoc') {
                            billingAddress['extension_attributes'][value['attribute_code']] = (nejmEtocElement.is(':checked')) ? 1 : '';
                        }
                    });
                }

            }
            return originalAction(messageContainer);
        });
    };
});
