/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'mageUtils',
    'jquery'
], function (utils, $) {
    'use strict';

    return {

        /**
         * @param {*} postCode
         * @param {*} countryId
         * @return {Boolean}
         */
        validate: function (postCode, countryId ) {
            if (!utils.isEmpty(postCode)) {
                let piPostcodeValidate;
                if (countryId === "US") {
                    if(postCode.length > 5) {
                        let digits = postCode.split(/(\d{1,5})/);
                        let str = "";
                        for (let group of digits)
                        {
                            if (/^\d+$/.test(group))
                            {
                                str += group + "-";
                            }
                        }
                        postCode = str.substring(0, str.length - 1);
                        $('.billing-address-form input[name="postcode"]').val(postCode);
                    }
                    piPostcodeValidate = /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(postCode);
                } else if(countryId === "CA") {
                    piPostcodeValidate = /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/.test(postCode);
                } else {
                    piPostcodeValidate = postCode.trim().length <= 9;
                }

                if (piPostcodeValidate) {
                    return true;
                }

                return false;
            }

            return true;
        }
    };
});
