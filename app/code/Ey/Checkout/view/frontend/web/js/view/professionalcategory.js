define([
    'jquery',
    'Magento_Ui/js/form/element/abstract',
    'underscore'
], function($,Component,_) {
    'use strict';

    return Component.extend({
        
        defaults: {
            template: 'Ey_Checkout/professionalcategoryone'
        },

        initialize: function () {
            this._super();
            //$("#pi_suffix").change(this.onUpdate());
            return this;
        },
        getProfessionalCategory: function () {
            var result = [];
            var professionalCatData = window.checkoutConfig.customerData.professional_category_options;
            _.each(professionalCatData, function (pCat) {
                result.push({
                    'pcatCode': pCat.value,
                    'pcatName': pCat.label
                });
            });

            return result;
        }
    });
});

