define([
    'ko',
    'jquery',
    'underscore',
    'uiComponent',
    'uiRegistry',
    'mage/translate',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/select-billing-address',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/totals',
    'Ey_Checkout/js/model/billingValidator',
    'Ey_Checkout/js/model/custombillingValidator',
    'Magento_Checkout/js/action/set-billing-address',
    'Magento_Checkout/js/action/set-shipping-information',
    'Magento_Ui/js/model/messageList',
    'Magento_Ui/js/modal/alert',
    'mage/url',
    'Magento_Customer/js/model/customer',
    'mage/cookies'
], function (
    ko,
    $,
    _,
    Component,
    registry,
    $t,
    quote,
    selectBillingAddress,
    additionalValidators,
    shippingService,
    checkoutData,
    totals,
    billingValidator,
    customValidator,
    setBillingAddressAction,
    setShippingInformationAction,
    globalMessageList,
    alert,
    urlBuilder,
    customer
) {
    'use strict';

    var actionPaymentButtonSelector = '.payment-method._active button.action',
        htmlBodySelector = 'html, body',
        FormLoginFieldErrorSelector = '.form-login .field-error',
        FormLoginMageErrorSelector = '.ey_personal_information_step .form-login .mage-error';

    return Component.extend({
        defaults: {
            template: 'Ey_Checkout/placeorder'
        },

        placeOrderLabel: ko.observable($t('Place Order')),
        isVisible: ko.observable(true),
        isPlaceOrderActionAllowed: ko.observable(quote.billingAddress() != null && quote.paymentMethod() != null),
        isPlaceOrderExtendAllowed: ko.observable(false),

        isEnabled: function () {
            return true;
        },

        initialize: function() {
            this._super();
            var self = this;

            self.isPlaceOrderActionAllowed(true);
            self.isVisible(true);

            if (this.getCode() === 'paypal_express') {
                self.isVisible(false);
            }

            quote.paymentMethod.subscribe(function (newValue) {
                if (typeof newValue == 'object' && newValue != null) {
                    if (newValue.method === 'paypal_express') {
                        self.isVisible(false);
                    } else {
                        self.isVisible(true);
                    }
                }
            });
        },

        placeOrder: function (data, event) {
            if (this.isPlaceOrderExtendAllowed()) {
                return false;
            }

            var _this = this;

            var date = new Date();
            date.setTime(date.getTime() + (10 * 60 * 1000));

            $.mage.cookies.set('ManualCheck', 'true', {
                expires: date
            });

            var shippingAddressComponent = registry.get('checkout.steps.shipping-step.shippingAddress');
            var billingAddressComponent = registry.get('checkout.steps.billing-step.payment.payments-list.billing-address-form-shared');
            if (billingAddressComponent === undefined) {
                billingAddressComponent = registry.get('checkout.steps.billing-step.payment.afterMethods.billing-address-form');
            }
            if (billingAddressComponent === undefined) {
                billingAddressComponent = registry.get('checkout.steps.shipping-step.shippingAddress.billing-address');
            }
            if (event) {
                event.preventDefault();
            }

            if (additionalValidators.validate()) {
                var billingValidateResult = billingAddressComponent.validateOnPlaceOrderClick();
                var validateCustomAttributesResult = _this.validateCustomAttributes();
                var validateCustomPaymentFormResult = _this.validateCustomPaymentForm();
                if (quote.isVirtual()) {
                    var validateResultVirtual = billingValidateResult && validateCustomAttributesResult && validateCustomPaymentFormResult;
                    if (validateResultVirtual) {
                        if (!_this.isPaymentMethodChecked()) {
                            alert({
                                content: $t('Please select a payment method.')
                            });
                            return false;
                        }
                        if (window.checkoutConfig.reloadOnBillingAddress ||
                            !window.checkoutConfig.displayBillingOnPaymentMethod
                        ) {
                            setBillingAddressAction(globalMessageList).done(function () {
                                setTimeout(function () {
                                    _this.updateAccessTokenFromLocalStorage();
                                },1000);
                            });
                        } else {
                            _this.updateAccessTokenFromLocalStorage();
                        }
                    }
                } else {
                    var billingValidateInfoResult =  shippingAddressComponent.validateBillingInformation();
                    var shippingValidateResult = shippingAddressComponent.validateShippingInformation();
                    var validateResult = shippingValidateResult && billingValidateInfoResult && billingValidateResult && validateCustomAttributesResult && validateCustomPaymentFormResult;
                    if (validateResult) {
                        if (!_this.isPaymentMethodChecked()) {
                            alert({
                                content: $t('Please select a payment method.')
                            });
                            return false;
                        }
                        billingAddressComponent.updateAddresses();
                        setShippingInformationAction().done(function () {
                            setTimeout(function () {
                                _this.updateAccessTokenFromLocalStorage();
                            },1000);
                        });
                    }
                }
            } else {
                if ($(FormLoginFieldErrorSelector).first().length) {
                    $(htmlBodySelector).animate({
                        scrollTop: $(FormLoginFieldErrorSelector).first().offset().top - 100
                    }, 1000 );
                } else if ($(FormLoginMageErrorSelector).first().length) {
                    $(htmlBodySelector).animate({
                        scrollTop: $(FormLoginMageErrorSelector).first().offset().top - 100
                    }, 1000);
                } else {
                    $(htmlBodySelector).animate({
                        scrollTop: $(htmlBodySelector).offset().top + 20
                    }, 1000);
                }
                return false;
            }
        },

        isPaymentMethodChecked: function () {
            return quote.paymentMethod() ? quote.paymentMethod().method : false;
        },

        getCode: function () {
            if (quote.paymentMethod()) {
                return quote.paymentMethod().method;
            }
            return null;
        },

        validateCustomPaymentForm: function () {
            if (this.getCode() === 'md_firstdata') {
                return $('#firstdata-transparent-form').valid();
            }
            return true;
        },

        validateCustomAttributes: function () {
            /*if (this.source) {
                if (this.source.order) {
                    this.source.set('params.invalid', false);
                    for (var step in this.source.order) {
                        this.source.trigger('order.'+step+'.data.validate');
                    }
                }
            }*/
            return true;
        },

        updateAccessTokenFromLocalStorage: function () {
            var _this = this;
            let accessToken = $.localStorage.get('janrainCaptureToken');
            if (customer.isLoggedIn() && accessToken) {
                $.ajax({
                    type: 'POST',
                    url: urlBuilder.build('orderfields/index/updateAccessToken'),
                    dataType: 'json',
                    data: {accessToken: accessToken},
                    success: function(data) {
                        _this.placeOrderAction();
                    }, error: function(xhr, ajaxOptions, thrownError){
                        _this.placeOrderAction();
                    }
                });
            } else {
                _this.placeOrderAction();
            }
        },

        placeOrderAction: function () {
            if ($(actionPaymentButtonSelector).length > 0) {
                $(actionPaymentButtonSelector).get(0).click();
            }
        }
    });
});
