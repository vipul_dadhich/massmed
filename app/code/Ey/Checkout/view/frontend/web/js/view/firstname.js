define([
	'jquery',
    'Magento_Ui/js/form/form',
	'mage/url'
], function($,Component, urlBuilder) {
    'use strict';
    return Component.extend({
		defaults: {
			template: 'Ey_Checkout/firstname'
		},

        initialize: function () {
            this._super();
            return this;
        },

        /**
         * Form submit handler
         *
         * This method can have any name.
         */
        onSubmit: function() {
			this.source.set('params.invalid', false);
            if (!this.source.get('params.invalid')) {
				$.ajax({
					url : urlBuilder.build('orderfields/index/firstname'),
        	   		data : $('form.form.form-login').serialize(),
            		method : 'POST',
               		success: function(response) {
						var responseObj = JSON.parse(response);
               		}
				});   
            }
        }
    });
});
