define([
	'jquery',
    'Magento_Ui/js/form/form'
], function($,Component) {
    'use strict';

    return Component.extend({
		defaults: {
			template: 'Ey_Checkout/lastname'
		},

        initialize: function () {
            this._super();
            return this;
        },

        /**
         * Form submit handler
         *
         * This method can have any name.
         */
        onSubmit: function() {

        }
    });
});
