require([
    'jquery',
    'Magento_Customer/js/zxcvbn',
    'Magento_Customer/js/model/customer',
	'mage/url',
	'Magento_Ui/js/lib/view/utils/async'
], function ($,zxcvbn, customer, urlBuilder) {
    'use strict';

	var eyCheckoutJS,
		piPrimarySpecialtyBoxSelector = '.checkout-index-index .field[name="billingAddressshared\\.pi_primaryspecialty"]',
		piPrimarySpecialtySelector = '#pi_primaryspecialty',
		customerEmailSelector = '#customer-email',
		piFirstNameSelector = '#pi_firstname',
		piLastNameSelector = '#pi_lastname',
		thisEmailSelector = '#thisemail',
		piSuffixSelector = '#pi_suffix',
		baPasswordSelector = '#ba_password',
		passwordSMSelector = '[data-role=password-strength-meter]',
		passwordSMLSelector = '[data-role=password-strength-meter-label]',
		billingFirstNameSelector = '.billing-address-form [name=firstname]',
		shippingFirstNameSelector = '.form-shipping-address [name=firstname]',
		billingLastNameSelector = '.billing-address-form [name=lastname]',
		shippingLastNameSelector = '.form-shipping-address [name=lastname]',
		billingCountryIdSelector = '.billing-address-form select[name=country_id]',
		billingAddressSharedIdSelector = '.checkout-index-index .field[name="billingAddressshared\\.vat_id"]',
		billingAddressSharedVatIdSelector = 'div[name="billingAddressshared\\.vat_id"] input',
		emailMeCheckboxSelector = '.email_me_section input[type=checkbox]',
		emailMeSectionSelector = '.email_me_section',
		emailMeSectionCaSoiSelector = '.email_me_section input[name=catalyst_soi]',
		emailMeSectionCaConnectSelector = '.email_me_section input[name=catalyst_connect]',
        customAttributesCaSoiSelector = '.billing-address-form [name="custom_attributes[customer_catalyst_soi]"]',
        customAttributesCaConnectSelector = '.billing-address-form [name="custom_attributes[customer_catalyst_connect]"]',
		catalystSoiSelector = 'input[name=catalyst_soi]',
		catalystConnectSelector = 'input[name=catalyst_connect]',

        emailMeSectionNejmSoiSelector = '.email_me_section input[name=nejm_soi]',
        emailMeSectionNejmEtocSelector = '.email_me_section input[name=nejm_etoc]',
        nejmSoiSelector = 'input[name=nejm_soi]',
        nejmEtocSelector = 'input[name=nejm_etoc]',
		buttonPlaceOrderSelector = 'button[title="Place Order"]',
		billingAddressIdSelectSelector = 'select[name="billing_address_id"]',
		paymentMethodSelectSelector = 'input[name="payment[method]"]',
		billingAddressDetailsSelector = '.billing-address-details',
		checkoutBillingDetailsSelector = '.checkout-billing-address .fieldset'
	;

	var array_vals2 = ["US"];

	eyCheckoutJS = {
		_listen: function() {
			$(piFirstNameSelector).on('blur', function(){
				$(billingFirstNameSelector).val($(this).val());
				$(billingFirstNameSelector).trigger("change");

				if (shippingFirstNameSelector.length > 0) {
                    $(shippingFirstNameSelector).val($(this).val());
                    $(shippingFirstNameSelector).trigger("change");
                }
			});

			$(piLastNameSelector).on('blur', function(){
				$(billingLastNameSelector).val($(this).val());
				$(billingLastNameSelector).trigger("change");

                if (shippingLastNameSelector.length > 0) {
                    $(shippingLastNameSelector).val($(this).val());
                    $(shippingLastNameSelector).trigger("change");
                }
			});
		},

		callEmailAjaxFunction: function () {
			$.ajax({
				url : urlBuilder.build('orderfields/index/checkemail'),
				data : {'email' : $(customerEmailSelector).val() },
				method : 'POST',
				dataType: 'json',
				success: function(data) {
					if(data.fname && data.lname){
						$(billingFirstNameSelector).val(data.fname);
						$(billingFirstNameSelector).trigger("change");
						$(billingLastNameSelector).val(data.lname);
						$(billingLastNameSelector).trigger("change");

                        if (shippingFirstNameSelector.length > 0) {
                            $(shippingFirstNameSelector).val(data.fname);
                            $(shippingFirstNameSelector).trigger("change");
                        }

                        if (shippingLastNameSelector.length > 0) {
                            $(shippingLastNameSelector).val(data.lname);
                            $(shippingLastNameSelector).trigger("change");
                        }
					}
					return true;
				}
			});
		},

		runMyFunction: function () {
			$(baPasswordSelector).on('keyup', function() {
				var password = $(this).val(),
					displayScore = 0,
					strengthLabel = '',
					className = '';

				if (password === "" || password === null) {
					displayScore = 0;
				} else {
					if ($(customerEmailSelector).length > 0
						&& password.toLowerCase() === $(customerEmailSelector).val().toLowerCase()) {
						displayScore = 1;
					} else {
						displayScore = zxcvbn(password).score;
						if (!displayScore) {
							displayScore = 1;
						}
					}
				}
				
				switch (displayScore) {
					case 0:
						strengthLabel = 'No Password';
						className = 'password-none';
						break;
					case 1:
						strengthLabel = 'Weak';
						className = 'password-weak';
						break;
					case 2:
						strengthLabel = 'Medium';
						className = 'password-medium';
						break;
					case 3:
						strengthLabel = 'Strong';
						className = 'password-strong';
						break;
					case 4:
						strengthLabel = 'Very Strong';
						className = 'password-very-strong';
						break;
				}
				$(passwordSMSelector).removeClass().addClass(className);
				$(passwordSMLSelector).text(strengthLabel);
			});
		}
	};

	$.async(piSuffixSelector, function(element) {
		/*$(element).on('change', function()  {
			if ($(this).val() === "MD" || $(this).val() === "DO" || $(this).val() === "MBBS") {
				$(piPrimarySpecialtyBoxSelector).show();
				$(piPrimarySpecialtySelector).addClass('required-entry');
			} else {
				$(piPrimarySpecialtyBoxSelector).hide();
				$(piPrimarySpecialtySelector).removeClass('required-entry');
			}
		});*/

		eyCheckoutJS.callEmailAjaxFunction();

		$(customerEmailSelector).on('change', function() {
			eyCheckoutJS.callEmailAjaxFunction();
		});
	});

	$.async(piFirstNameSelector, function (element) {
		$(element).on('blur', function(){
			$(billingFirstNameSelector).val($(this).val());
			$(billingFirstNameSelector).trigger("change");

            if (shippingFirstNameSelector.length > 0) {
                $(shippingFirstNameSelector).val($(this).val());
                $(shippingFirstNameSelector).trigger("change");
            }
		});
	});

	$.async(piLastNameSelector, function (element) {
		$(element).on('blur', function(){
			$(billingLastNameSelector).val($(this).val());
			$(billingLastNameSelector).trigger("change");

            if (shippingLastNameSelector.length > 0) {
                $(shippingLastNameSelector).val($(this).val());
                $(shippingLastNameSelector).trigger("change");
            }
		});
	});
	$.async(baPasswordSelector, function (element) {
		eyCheckoutJS.runMyFunction();
	});
	$.async(thisEmailSelector, function (element) {
		if ($(customerEmailSelector).val() !== "") {
			$(element).html($(customerEmailSelector).val());
		}
		$(customerEmailSelector).on('change', function() {
			$(element).html($(this).val());
		});
	});

	$.async(billingCountryIdSelector, function (element) {
		$(element).on('change', function () {
			var val = $(this).val(), vatTriggerCalls = true;
			var array_vals = ["AT","BE","DK","FR","DE","GR","IE","IT","NL","PT","ES","SE","GB"];

			if ($('[name="shipping-address-same-as-billing"]').length > 0
                && !$('[name="shipping-address-same-as-billing"]').is(":checked")) {
                vatTriggerCalls = false;
            }

			if (vatTriggerCalls) {
                if ($.inArray(val, array_vals) >= 0) {
                    $(billingAddressSharedIdSelector).show();
                } else {
                    $(billingAddressSharedIdSelector).hide();
                    $(billingAddressSharedVatIdSelector).val('');
                }
            }

			var manualCheck = $.cookie("ManualCheck");
			var array_vals2 = ["US"];
			if (customer.isLoggedIn()) {
				let defaultCountry = window.checkoutConfig.customerData.customer_default_country;

				let selectedBrand = window.checkoutConfig.brand;

				//if(defaultCountry === val){
				    if( selectedBrand === 'Catalyst' ){
                        if(window.checkoutConfig.customerData.catalystsoi === '1'
                            && window.checkoutConfig.customerData.catalystconnect === '1') {
                            $(emailMeCheckboxSelector).attr('checked','checked');
                            $(emailMeSectionSelector).hide();
                        }
                        if(window.checkoutConfig.customerData.catalystsoi === '1') {
                            $(emailMeSectionCaSoiSelector).attr('checked','checked');
                            $(catalystSoiSelector).closest('label').hide();
                        }
                        if(window.checkoutConfig.customerData.catalystconnect === '1') {
                            $(emailMeSectionCaConnectSelector).attr('checked','checked');
                            $(catalystConnectSelector).closest('label') .hide();
                        }
                        if(window.checkoutConfig.customerData.catalystsoi === '0') {
                            if (manualCheck === undefined || manualCheck === null || manualCheck === 'null') {
                                if ($.inArray(val, array_vals2) >= 0) {
                                    $(emailMeSectionCaSoiSelector).attr('checked', 'checked');
                                } else {
                                    $(emailMeSectionCaSoiSelector).attr('checked', false);
                                }
                            }
                        }
                        if(window.checkoutConfig.customerData.catalystconnect === '0') {
                            if (manualCheck === undefined || manualCheck === null || manualCheck === 'null') {
                                if ($.inArray(val, array_vals2) >= 0) {
                                    $(emailMeSectionCaConnectSelector).attr('checked', 'checked');
                                } else {
                                    $(emailMeSectionCaConnectSelector).attr('checked', false);
                                }
                            }
                        }
                    } else {
                        if(window.checkoutConfig.customerData.nejmsoi === '1'
                            && window.checkoutConfig.customerData.nejmetoc === '1') {
                            $(emailMeCheckboxSelector).attr('checked','checked');
                            $(emailMeSectionSelector).hide();
                        }
                        if(window.checkoutConfig.customerData.nejmsoi === '1') {
                            $(emailMeSectionNejmSoiSelector).attr('checked','checked');
                            $(nejmSoiSelector).closest('label').hide();
                        }
                        if(window.checkoutConfig.customerData.nejmsoi === '0') {
                            if (manualCheck === undefined || manualCheck === null || manualCheck === 'null') {
                                if ($.inArray(val, array_vals2) >= 0) {
                                    $(emailMeSectionNejmSoiSelector).attr('checked', 'checked');
                                } else {
                                    $(emailMeSectionNejmSoiSelector).attr('checked', false);
                                }
                            }
                        }

                        if(val === 'US'){
                        	if ($(emailMeSectionNejmSoiSelector).is(':checked')) {
                            	$(emailMeSectionNejmEtocSelector).attr('checked','checked');
                            }
                            $(nejmEtocSelector).closest('label').hide();
                            if(window.checkoutConfig.customerData.nejmsoi === '1'){
                                $(emailMeSectionSelector).hide();
                            }
                        } else {
                            $(nejmEtocSelector).closest('label').show();
                            if (window.checkoutConfig.customerData.nejmetoc === '1') {
                                $(emailMeSectionNejmEtocSelector).attr('checked','checked');
                                $(nejmEtocSelector).closest('label').hide();
                            }
                            if(window.checkoutConfig.customerData.nejmetoc === '0') {
                                if (manualCheck === undefined || manualCheck === null || manualCheck === 'null') {
                                    if ($.inArray(val, array_vals2) >= 0) {
                                        $(emailMeSectionNejmEtocSelector).attr('checked', 'checked');
                                    } else {
                                        $(emailMeSectionNejmEtocSelector).attr('checked', false);
                                    }
                                }
                            }
                        }
                    }


				} else {
                    $(emailMeSectionSelector).show();
                    $(nejmEtocSelector).closest('label').show();
                    if (manualCheck === undefined || manualCheck === null || manualCheck === 'null') {
                        if ($.inArray(val, array_vals2) >= 0) {
                            $(emailMeCheckboxSelector).attr('checked', 'checked');
                        } else {
                            $(emailMeCheckboxSelector).attr('checked', false);
                        }
                    }
                    if(val == 'US'){
                    	if ($(emailMeSectionNejmSoiSelector).is(':checked')) {
                        	$(emailMeSectionNejmEtocSelector).attr('checked','checked');
                        }
                        $(nejmEtocSelector).closest('label').hide();
                    }
				}
		});
	});

	$.async(emailMeSectionCaSoiSelector, function(element) {
		if (!customer.isLoggedIn()) {
			var val = $(billingCountryIdSelector).val();
			var manualCheck = $.cookie("ManualCheck");
			if(!val){
				val = window.checkoutConfig.currencyEngineCountryId;
			}
			if (manualCheck === undefined || manualCheck === null) {
				if ($.inArray(val, array_vals2) >= 0) {
					$(emailMeSectionCaSoiSelector).attr('checked',false);
				} else {
					$(emailMeSectionCaSoiSelector).attr('checked','checked');
				}
			}
		}
	});
	$.async(emailMeSectionNejmSoiSelector, function(element) {
		let selectedBrand = window.checkoutConfig.brand;
		if (!customer.isLoggedIn() && selectedBrand != 'Catalyst') {
			var val = $(billingCountryIdSelector).val();
			if(!val || val === undefined || val === null){
				val = window.checkoutConfig.currencyEngineCountryId;
			}
			if ($.inArray(val, array_vals2) >= 0) {
				$(emailMeSectionNejmSoiSelector).attr('checked', 'checked');
				$(emailMeSectionNejmEtocSelector).attr('checked', 'checked');
			} else {
				$(emailMeSectionNejmSoiSelector).attr('checked', false);
				$(emailMeSectionNejmEtocSelector).attr('checked', false);
				$(nejmEtocSelector).closest('label').show();
			}
		}
	});
	$.async(emailMeSectionCaConnectSelector, function(element) {
		if (!customer.isLoggedIn()) {
			var val = $(billingCountryIdSelector).val();
			var manualCheck = $.cookie("ManualCheck");
			if(!val){
				val = window.checkoutConfig.currencyEngineCountryId;
			}
			if (manualCheck === undefined || manualCheck === null) {
				if ($.inArray(val, array_vals2) >= 0) {
					$(emailMeSectionCaConnectSelector).attr('checked', false);
				} else {
					$(emailMeSectionCaConnectSelector).attr('checked', 'checked');
				}
			}
		}
	});

	$.async(buttonPlaceOrderSelector, function(element) {
		$(element).removeClass('disabled');
	});
    $.async(buttonPlaceOrderSelector, function(element) {
        $(element).on('click', function() {
            $.mage.cookies.clear('ManualCheck');
        });
    });
    $.async(emailMeSectionNejmSoiSelector, function(element) {
        $(element).on('change', function() {
            if ($(billingCountryIdSelector).val() == 'US') {
                if ($(nejmSoiSelector).is(':checked')) {
                    $(emailMeSectionNejmEtocSelector).attr('checked', 'checked');
                } else {
                    $(emailMeSectionNejmEtocSelector).attr('checked', false);
                }
            }
        });
    });

	$.async(billingAddressIdSelectSelector, function (element) {
		$(element).on('change', function() {
            let defaultCountry = window.checkoutConfig.customerData.customer_default_country;
            let customerDefaultntry = 'US';
            if (defaultCountry && (defaultCountry !== '0' || defaultCountry != '0' || parseInt(defaultCountry) != 0 || parseInt(defaultCountry) !== 0)) {
                customerDefaultntry = defaultCountry;
            }
			if ($(element).find(":selected").text() === "New Address") {
				$(buttonPlaceOrderSelector).removeClass('disabled');
                $(billingCountryIdSelector).val(customerDefaultntry).trigger('change');

			}
		});
	});

	$.async(paymentMethodSelectSelector, function (element) {
		$(element).on('click', function () {

			if($(billingAddressIdSelectSelector + ' option:selected').text() === "New Address"){
				$(billingAddressDetailsSelector).hide();
				$(checkoutBillingDetailsSelector).show();
			}
		});
	});

    $.async(emailMeSectionCaSoiSelector, function (element) {
        $(element).on('change', function() {
            let val = $(catalystSoiSelector).is(':checked') ? 1 : '';
            $(customAttributesCaSoiSelector).val(val);
        });
    });

    $.async(emailMeSectionCaConnectSelector, function (element) {
        $(element).on('change', function() {
            let val = $(catalystConnectSelector).is(':checked') ? 1 : '';
            $(customAttributesCaConnectSelector).val(val);
        });
    });
});
