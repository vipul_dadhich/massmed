define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    return function (setShippingInformationAction) {
        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            var billingAddress = quote.billingAddress(),
                shippingAddress = quote.shippingAddress(),
                shipAddrSameAsBillElement = $('#shipping-address-same-as-billing')
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }

            let billingAddressCode = 'C1', mailingAddressCode = 'C2';
            if (billingAddress.customAttributes !== undefined) {
                $.each(billingAddress.customAttributes, function (key, value) {
                    if (value['attribute_code'] === 'addresscode') {
                        if($.isPlainObject(value)){
                            value = value['value'];
                        }
                        billingAddressCode = value;
                        return false;
                    }
                });
            }

            if (shippingAddress.customAttributes !== undefined) {
                $.each(shippingAddress.customAttributes, function (key, value) {
                    if (value['attribute_code'] === 'addresscode') {
                        if($.isPlainObject(value)){
                            value = value['value'];
                        }
                        mailingAddressCode = value;
                        return false;
                    }
                });
            }

            if (shipAddrSameAsBillElement.length > 0 && shipAddrSameAsBillElement.is(":checked")) {
                mailingAddressCode = billingAddressCode;
            }

            shippingAddress.customAttributes['billing_address_code'] = {'attribute_code': 'billing_address_code','value': billingAddressCode};
            shippingAddress.customAttributes['mailing_address_code'] = {'attribute_code': 'mailing_address_code','value': mailingAddressCode};

            if (shippingAddress.customAttributes !== undefined) {
                $.each(shippingAddress.customAttributes, function (key, value) {
                    let formatValue = value
                    if($.isPlainObject(value)){
                        formatValue = value['value'];
                    }
                    shippingAddress['extension_attributes'][value['attribute_code']] = formatValue;
                });
            }

            return originalAction();
        });
    };
});
