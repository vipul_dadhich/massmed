/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'ko',
    'underscore',
    'Magento_Ui/js/form/form',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/create-billing-address',
    'Magento_Checkout/js/action/select-billing-address',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/action/set-billing-address',
    'Magento_Ui/js/model/messageList',
    'mage/translate',
    'Ey_Checkout/js/model/billingValidator',
    'Ey_Checkout/js/model/custombillingValidator',
    'uiRegistry',
    'Ey_Checkout/js/model/tax-calculation',
    'Magento_Ui/js/lib/view/utils/async',
    'mage/cookies'
], function (
    $,
    ko,
    _,
    Component,
    customer,
    addressList,
    quote,
    createBillingAddress,
    selectBillingAddress,
    checkoutData,
    checkoutDataResolver,
    customerData,
    setBillingAddressAction,
    globalMessageList,
    $t,
    additionalValidators,
    customValidators,
    registry,
    taxCalculation
) {
    'use strict';

    var billingAddressCountryIdSelector = '.billing-address-form select[name="country_id"]',
        emailMeSectionCheckboxSelector = '.email_me_section input[type=checkbox]',
        emailMeSectionSelector = '.email_me_section',
        emailMeSectionCatalystSoiSelector = '.email_me_section input[name=catalyst_soi]',
        emailMeSectionCatalystConnectSelector = '.email_me_section input[name=catalyst_connect]',
        emailMeSectionNejmSoiSelector = '.email_me_section input[name=nejm_soi]',
        emailMeSectionNejmEtocSelector = '.email_me_section input[name=nejm_etoc]',
        catalystConnectSelector = 'input[name=catalyst_connect]',
        catalystSoiSelector = 'input[name=catalyst_soi]',
        nejmSoiSelector = 'input[name=nejm_soi]',
        nejmEtocSelector = 'input[name=nejm_etoc]',
        billingAddressForm = '.billing-address-form',
        eyPaymentMethodStepSelector = '.ey_payment_method_step',
        actionUpdateSpanSelector = '.action.action-update span',
        zipCodeErrorSelector = '.zip-code-error',
        billingAddressFormErrorSelector = '.billing-address-form .field-error',
        htmlBodySelector = 'html, body',
        lastSelectedBillingAddress = null,
        billingPostcodeElement = '.billing-address-form input[name="postcode"]',
        countryData = customerData.get('directory-data'),
        paymentMethodSelectSelector = '.ey_payment_method_step input[name="payment[method]"]',
        billingAddrFormRegionIdSelector = '.billing-address-form select[name="region_id"]',
        customCheckboxSelector = '.custom-checkbox',
        customCheckboxInputSelector = '.custom-checkbox input',
        placeOrderSelector = '#checkout-payment-method-load .actions-toolbar button.checkout',
        paypalOrderSelector = "#paypal-express-in-context-button",
        ccNumberElement = '#md_firstdata_cc_number',
        ccNumberErrorElement = '#md_firstdata_cc_number-error',
        expireYearElement = '#md_firstdata_expiration_yr',
        expireYearErrorElement = '#md_firstdata_expiration_yr-error',
        expireMonthElement = '#md_firstdata_expiration',
        expireMonthErrorElement = '#md_firstdata_expiration-error',
        expireCvvElement = '#md_firstdata_cc_cid',
        expireCvvErrorElement = '#md_firstdata_cc_cid-error',
        customerEmailElement = '#customer-email',
        customerEmailErrorElement = '#customer-email-error',
        addressOptions = addressList().filter(function (address) {
            return address.getType() === 'customer-address';
        }),
        newAddressOption = {
            /**
             * Get new address label
             * @returns {String}
             */
            getAddressInline: function () {
                return $t('New Address');
            },
            customerAddressId: null
        },
        lastSelectedAddressKey = null,
        lastSelectedPayment = (quote.paymentMethod()) ? quote.paymentMethod().method : null;
    $.async(ccNumberElement, function(element) {
        $(element).on('keyup', function() {
            if($(ccNumberErrorElement).length){
                $(element).removeClass('mage-error');
                $(ccNumberErrorElement).hide();
            }
        });
    });
    $.async(expireMonthElement, function(element) {
        $(element).on('change', function() {
            if($(expireMonthErrorElement).length){
                $(element).removeClass('mage-error');
                $(expireMonthErrorElement).hide();
            }
        });
    });
    $.async(expireYearElement, function(element) {
        $(element).on('change', function() {
            var selectedYear = $(element).val();
            var selectedMonth = $(expireMonthElement).val();
            var dateObj = new Date();
            var month = dateObj.getMonth()
            var year = dateObj.getFullYear();
            if (selectedYear == year) {
                if (selectedMonth >= month) {
                    if($(expireMonthErrorElement).length){
                        $(expireMonthElement).removeClass('mage-error');
                        $(expireMonthErrorElement).hide();
                    }
                }
            }
            if (selectedYear > year) {
                if($(expireMonthErrorElement).length){
                    $(expireMonthElement).removeClass('mage-error');
                    $(expireMonthErrorElement).hide();
                }
            }

            if($(expireYearErrorElement).length){
                $(element).removeClass('mage-error');
                $(expireYearErrorElement).hide();
            }
        });
    });
    $.async(expireCvvElement, function(element) {
        $(element).on('keyup', function() {
            if($(expireCvvErrorElement).length){
                $(element).removeClass('mage-error');
                $(expireCvvErrorElement).hide();
            }
        });
    });
    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/billing-address',
            actionsTemplate: 'Magento_Checkout/billing-address/actions',
            formTemplate: 'Magento_Checkout/billing-address/form',
            detailsTemplate: 'Magento_Checkout/billing-address/details',
            links: {
                isAddressFormVisible: '${$.billingAddressListProvider}:isNewAddressSelected'
            }
        },
        currentBillingAddress: quote.billingAddress,
        customerHasAddresses: addressOptions.length > 0,

        isEnabledActions: function () {
            return false;
        },

        /**
         * Init component
         */
        initialize: function () {
            let fieldsetName = 'checkout.steps.billing-step.payment.afterMethods.billing-address-form.form-fields';
            if (!quote.isVirtual()) {
                fieldsetName = 'checkout.steps.shipping-step.shippingAddress.billing-address.form-fields';
            }
            this._super();
            let self = this;
            quote.paymentMethod.subscribe(function () {
                checkoutDataResolver.resolveBillingAddress();
            }, this);

            // When User Fill All details and then update Zip Code
            /*$("body").delegate(billingPostcodeElement, "change", function(){
                self.updateAddress();
            });*/

            // For First Time when user fill all details
            $("body").delegate(paymentMethodSelectSelector,"click",function(){
                if ((lastSelectedPayment && lastSelectedPayment !== $(this).val())
                    || (!lastSelectedPayment && $(this).val() !== '')) {
                    self.updateAddress();
                    lastSelectedPayment = $(this).val();
                }
            });

            registry.async('checkoutProvider')(function (checkoutProvider) {
                let billingAddressData = checkoutData.getBillingAddressFromData();

                if (billingAddressData) {
                    checkoutProvider.set(
                        'billingAddress',
                        $.extend(true, {}, checkoutProvider.get('billingAddress'), billingAddressData)
                    );
                }
                checkoutProvider.on('billingAddress', function (billingAddressData) {
                    checkoutData.setBillingAddressFromData(billingAddressData);
                });
                taxCalculation.initFields(fieldsetName);
            });
        },

        /**
         * @return {exports.initObservable}
         */
        initObservable: function () {
            let self = this;
            this._super()
                .observe({
                    selectedAddress: null,
                    isAddressDetailsVisible: quote.billingAddress() != null,
                    isAddressFormVisible: !customer.isLoggedIn() || !addressOptions.length,
                    isAddressSameAsShipping: false,
                    saveInAddressBook: 1
                });

            if (typeof (window.checkoutConfig.mmsCheckout) == 'undefined') {
                quote.billingAddress.subscribe(function (newAddress) {
                    if (quote.isVirtual()) {
                        this.isAddressSameAsShipping(false);
                    } else {
                        this.isAddressSameAsShipping(
                            newAddress != null &&
                            newAddress.getCacheKey() == quote.shippingAddress().getCacheKey() //eslint-disable-line eqeqeq
                        );
                    }

                    if (newAddress != null && newAddress.saveInAddressBook !== undefined) {
                        this.saveInAddressBook(newAddress.saveInAddressBook);
                    } else {
                        this.saveInAddressBook(1);
                    }
                    this.isAddressDetailsVisible(false);
                    this.fillAddressCustom();
                }, this);
            }

            quote.billingAddress.subscribe(function (addressOption) {
                if (!addressOption) {
                    _.each(addressList(), function (address) {
                        if (address.isDefaultBilling && address.isDefaultBilling()) {
                            lastSelectedAddressKey = address;
                            selectBillingAddress(address);
                            checkoutData.setSelectedBillingAddress(address.getKey());
                            self.updateAddresses();
                        }
                    }, this);
                }
            }, this);

            this.selectedAddress.subscribe(function (address) {
                if (address.getAddressInline() !== newAddressOption.getAddressInline()
                    && address.customerAddressId !== newAddressOption.customerAddressId) {
                    self._bindWithSavedAddress(address, true);
                    lastSelectedAddressKey = address;
                    selectBillingAddress(address);
                    checkoutData.setSelectedBillingAddress(address.getKey());
                    self.updateAddresses();
                } else if (address.getAddressInline() === newAddressOption.getAddressInline()
                    && address.customerAddressId === newAddressOption.customerAddressId) {
                    self._bindWithSavedAddress(address, false);
                    taxCalculation.validateFields();
                }
            }, this);

            quote.billingAddress.subscribe(function (newAddress) {
                if ((newAddress.trigger_tax === undefined || newAddress.trigger_tax !== true)
                    && lastSelectedAddressKey !== null && lastSelectedAddressKey.getKey() === newAddress.getKey()) {
                    return;
                }
                newAddress.saveInAddressBook = self.saveInAddressBook() ? 1 : 0;
                lastSelectedAddressKey = newAddress;
                if (newAddress != null && newAddress.trigger_tax !== undefined && newAddress.trigger_tax === true) {
                    delete newAddress.trigger_tax;
                    checkoutData.setSelectedBillingAddress(newAddress.getKey());
                    self.updateAddresses();
                }
            });

            this.customerDefaultCountryTrigger();

            return this;
        },

        canUseShippingAddress: ko.computed(function () {
            return !quote.isVirtual() && quote.shippingAddress() && quote.shippingAddress().canUseForBilling();
        }),

        /**
         * @param {Object} address
         * @return {*}
         */
        addressOptionsText: function (address) {
            return address.getAddressInline();
        },

        /**
         * @return {Boolean}
         */
        useShippingAddress: function () {
            if (this.isAddressSameAsShipping()) {
                selectBillingAddress(quote.shippingAddress());

                this.updateAddresses();
                this.isAddressDetailsVisible(true);
            } else {
                lastSelectedBillingAddress = quote.billingAddress();
                quote.billingAddress(null);
                this.isAddressDetailsVisible(false);
            }
            checkoutData.setSelectedBillingAddress(null);

            return true;
        },

        validateOnPlaceOrderClick: function () {
            var date = new Date();
            date.setTime(date.getTime() + (10 * 60 * 1000));

            $.mage.cookies.set('ManualCheck', 'true', {
                expires: date
            });

            var addressData, newBillingAddress;

            if (this.selectedAddress() && !this.isAddressFormVisible()) {
                selectBillingAddress(this.selectedAddress());
                checkoutData.setSelectedBillingAddress(this.selectedAddress().getKey());
                $(eyPaymentMethodStepSelector).show();
                $(actionUpdateSpanSelector).html("update");
            } else {
                this.source.set('params.invalid', false);
                this.source.trigger(this.dataScopePrefix + '.data.validate');

                if (this.source.get(this.dataScopePrefix + '.custom_attributes')) {
                    this.source.trigger(this.dataScopePrefix + '.custom_attributes.data.validate');
                }
                if (!customer.isLoggedIn()) {
                    if ($(customerEmailElement).length){
                            if (!$.validator.validateSingleElement(customerEmailElement)) {
                                $(customerEmailErrorElement).remove();
                                $(htmlBodySelector).animate({
                                    scrollTop: $(customerEmailElement).first().offset().top - 100
                                }, 1000);
                            return false;
                        }
                    }
                }
                if (!customValidators.validate()) {
                    return false;
                }
                if (!this.source.get('params.invalid')) {
                    addressData = this.source.get(this.dataScopePrefix);
                    if (customer.isLoggedIn() && !this.customerHasAddresses) { //eslint-disable-line max-depth
                        this.saveInAddressBook(1);
                    }
                    addressData['save_in_address_book'] = this.saveInAddressBook() ? 1 : 0;
                    newBillingAddress = createBillingAddress(addressData);

                    // New address must be selected as a billing address
                    selectBillingAddress(newBillingAddress);
                    checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                    checkoutData.setNewCustomerBillingAddress(addressData);

                    if (!customer.isLoggedIn()) {
                        if (!additionalValidators.validate()) {
                            return false;
                        }
                    } else if($(zipCodeErrorSelector).length) {
                        $(htmlBodySelector).animate({
                            scrollTop: $(zipCodeErrorSelector).first().offset().top - 100
                        }, 1000);
                        return false;
                    }

                    $(eyPaymentMethodStepSelector).show();
                    $(actionUpdateSpanSelector).html("update");
                    $(htmlBodySelector).animate({
                        scrollTop: $(eyPaymentMethodStepSelector).offset().top - 10
                    }, 1000);
                }else{
                    $(htmlBodySelector).animate({
                        scrollTop: $(billingAddressFormErrorSelector).first().offset().top - 200
                    }, 1000);

                    return false;
                }
            }
            return true;
        },

        /**
         * Update address action
         */
        updateAddress: function () {
            var date = new Date();
            date.setTime(date.getTime() + (10 * 60 * 1000));

            $.mage.cookies.set('ManualCheck', 'true', {
                expires: date
            });

            var addressData, newBillingAddress;

            if (this.selectedAddress() && !this.isAddressFormVisible()) {
                selectBillingAddress(this.selectedAddress());
                checkoutData.setSelectedBillingAddress(this.selectedAddress().getKey());
                $(eyPaymentMethodStepSelector).show();
                $(actionUpdateSpanSelector).html("update");
            } else {
                if (!customer.isLoggedIn()) {
                    if ($(customerEmailElement).length){
                        $(customerEmailErrorElement).remove();
                        if (!$.validator.validateSingleElement(customerEmailElement)) {
                            $(htmlBodySelector).animate({
                                scrollTop: $(customerEmailElement).first().offset().top - 100
                            }, 1000);
                            return false;
                        }
                    }
                }
                this.source.set('params.invalid', false);
                this.source.trigger(this.dataScopePrefix + '.data.validate');

                if (this.source.get(this.dataScopePrefix + '.custom_attributes')) {
                    this.source.trigger(this.dataScopePrefix + '.custom_attributes.data.validate');
                }
                if(!customer.isLoggedIn()){
                    if (!customValidators.validate()) {
                        return false;
                    }
                }
                if (!this.source.get('params.invalid')) {
                    addressData = this.source.get(this.dataScopePrefix);
                    if (customer.isLoggedIn() && !this.customerHasAddresses) { //eslint-disable-line max-depth
                        this.saveInAddressBook(1);
                    }
                    addressData['save_in_address_book'] = this.saveInAddressBook() ? 1 : 0;
                    newBillingAddress = createBillingAddress(addressData);

                    // New address must be selected as a billing address
                    selectBillingAddress(newBillingAddress);
                    checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                    checkoutData.setNewCustomerBillingAddress(addressData);

                    if (!customer.isLoggedIn()) {
                        if (!additionalValidators.validate()) {
                            return false;
                        }
                    } else if($(zipCodeErrorSelector).length) {
                        $(htmlBodySelector).animate({
                            scrollTop: $(zipCodeErrorSelector).first().offset().top - 100
                        }, 1000);
                        return false;
                    }

                    $(eyPaymentMethodStepSelector).show();
                    $(actionUpdateSpanSelector).html("update");
                    $(htmlBodySelector).animate({
                        scrollTop: $(eyPaymentMethodStepSelector).offset().top - 10
                    }, 1000);
                }else{
                    $(htmlBodySelector).animate({
                        scrollTop: $(billingAddressFormErrorSelector).first().offset().top - 200
                    }, 1000);

                    return false;
                }
            }
            this.updateAddresses();
        },

        /**
         * Edit address action
         */
        editAddress: function () {
            lastSelectedBillingAddress = quote.billingAddress();
            this.fillAddressCustom();
            quote.billingAddress(null);
            this.isAddressDetailsVisible(false);
            $(eyPaymentMethodStepSelector).hide();
        },

        fillAddressCustom: function(){
            if(!customer.isLoggedIn()){
                lastSelectedBillingAddress = quote.billingAddress();
                if (lastSelectedBillingAddress != null) {
                    var ba_password = '';
                    if (lastSelectedBillingAddress.customAttributes !== undefined) {
                        for (var i = 0; i < lastSelectedBillingAddress.customAttributes.length; i++) {
                            if (lastSelectedBillingAddress.customAttributes[i].attribute_code === 'ba_password') {
                                ba_password = lastSelectedBillingAddress.customAttributes[i].value;
                            }
                        }
                    }

                    $(billingAddressForm + ' select[name="country_id"]').val(lastSelectedBillingAddress.countryId).trigger('change');
                    let street0 = '', street1 = '';
                    if (lastSelectedBillingAddress.street !== undefined) {
                        if (lastSelectedBillingAddress.street.length === 1) {
                            street0 = lastSelectedBillingAddress.street[0];
                        } else if (lastSelectedBillingAddress.street.length > 1) {
                            street0 = lastSelectedBillingAddress.street[0];
                            street1 = lastSelectedBillingAddress.street[1];
                        }
                    }
                    $(billingAddressForm + ' input[name="street[0]"]').val(street0).trigger('keyup');
                    $(billingAddressForm + ' input[name="street[1]"]').val(street1).trigger('keyup');
                    $(billingAddressForm + ' input[name="city"]').val(lastSelectedBillingAddress.city).trigger('keyup');
                    $(billingAddressForm + ' input[name="postcode"]').val(lastSelectedBillingAddress.postcode).trigger('keyup');
                    $(billingAddressForm + ' #ba_password').val(ba_password).trigger('keyup');
                    $(billingAddressForm + ' .confirmpassword').val(ba_password).trigger('keyup');
                    $(billingAddressForm + ' select[name="region_id"]').val(lastSelectedBillingAddress.regionId).trigger('change');
                    $(billingAddressForm + ' input[name="region_id"]').val(lastSelectedBillingAddress.region).trigger('keyup');
                }
            }
        },

        customerDefaultCountryTrigger: function () {
            // let array_vals2 = [
            //     "AT","BE","BG","CA","HR","CY","CZ","DK","EE","FI","FR","DE",
            //     "GR","HU","IE","IT","LV","LT","LU","MT","NL","PL","PT","RO",
            //     "SK","SI","ES","SE","GB","AL","ME","RS","MK","TR","NO","IS","LI","CH"
            // ];
            let array_vals2 = ["US"];
            let val = $(billingAddressCountryIdSelector).val();
            let manualCheck = $.cookie("ManualCheck");
            if (customer.isLoggedIn()) {
                let defaultCountry = window.checkoutConfig.customerData.customer_default_country;
                if (defaultCountry && (defaultCountry !== '0' || defaultCountry != '0' || parseInt(defaultCountry) != 0 || parseInt(defaultCountry) !== 0)) {
                    let fillCountry = setInterval(function() {
                        $(billingAddressCountryIdSelector).val(defaultCountry).trigger('change');
                        if ($(billingAddressCountryIdSelector).val() === defaultCountry) {
                            clearInterval(fillCountry);
                        }
                    }, 500);
                } else {
                    let emailMeSection = setInterval(function() {
                        if(defaultCountry === val){
                            if(isBrandCatalyst()){
                                if (window.checkoutConfig.customerData.catalystsoi === '1'
                                    && window.checkoutConfig.customerData.catalystconnect === '1') {
                                    $(emailMeSectionCheckboxSelector).attr('checked','checked');
                                    $(emailMeSectionSelector).hide();
                                }
                                if (window.checkoutConfig.customerData.catalystsoi === '1') {
                                    $(emailMeSectionCatalystSoiSelector).attr('checked','checked');
                                    $(catalystSoiSelector).closest('label').hide();
                                }
                                if (window.checkoutConfig.customerData.catalystconnect === '1') {
                                    $(emailMeSectionCatalystConnectSelector).attr('checked','checked');
                                    $(catalystConnectSelector).closest('label') .hide();
                                }
                                if(window.checkoutConfig.customerData.catalystsoi === '0') {
                                    if (manualCheck === undefined || manualCheck === null || manualCheck === 'null') {
                                        if ($.inArray(val, array_vals2) >= 0) {
                                            $(emailMeSectionCatalystSoiSelector).attr('checked', 'checked');
                                        } else {
                                            $(emailMeSectionCatalystSoiSelector).attr('checked', false);
                                        }
                                    }
                                }
                                if(window.checkoutConfig.customerData.catalystconnect === '0') {
                                    if (manualCheck === undefined || manualCheck === null || manualCheck === 'null') {
                                        if ($.inArray(val, array_vals2) >= 0) {
                                            $(emailMeSectionCatalystConnectSelector).attr('checked', 'checked');
                                        } else {
                                            $(emailMeSectionCatalystConnectSelector).attr('checked', false);
                                        }
                                    }
                                }
                            }else{
                                alert($(emailMeSectionNejmSoiSelector).is(':checked'));
                                if (window.checkoutConfig.customerData.nejmsoi === '1'
                                    && window.checkoutConfig.customerData.nejmetoc === '1') {
                                    $(emailMeSectionCheckboxSelector).attr('checked','checked');
                                    $(emailMeSectionSelector).hide();
                                }

                                if (window.checkoutConfig.customerData.nejmsoi === '1') {
                                    $(emailMeSectionNejmSoiSelector).attr('checked','checked');
                                    $(nejmSoiSelector).closest('label') .hide();
                                }
                                if(window.checkoutConfig.customerData.nejmsoi === '0') {
                                    if (manualCheck === undefined || manualCheck === null || manualCheck === 'null') {
                                        if ($.inArray(val, array_vals2) >= 0) {
                                            $(emailMeSectionNejmSoiSelector).attr('checked', 'checked');
                                        } else {
                                            $(emailMeSectionNejmSoiSelector).attr('checked', false);
                                        }
                                    }
                                }
                                if(val === 'US'){
                                    if ($(emailMeSectionNejmSoiSelector).is(':checked')) {
                                        $(emailMeSectionNejmEtocSelector).attr('checked','checked');
                                    }
                                    $(nejmEtocSelector).closest('label').hide();
                                    if(window.checkoutConfig.customerData.nejmsoi === '1'){
                                        $(emailMeSectionSelector).hide();
                                    }
                                } else {
                                    $(nejmEtocSelector).closest('label').show();
                                    if (window.checkoutConfig.customerData.nejmetoc === '1') {
                                        $(emailMeSectionNejmEtocSelector).attr('checked','checked');
                                        $(nejmEtocSelector).closest('label').hide();
                                    }
                                    if(window.checkoutConfig.customerData.nejmetoc === '0') {
                                        if (manualCheck === undefined || manualCheck === null || manualCheck === 'null') {
                                            if ($.inArray(val, array_vals2) >= 0) {
                                                $(emailMeSectionNejmEtocSelector).attr('checked', 'checked');
                                            } else {
                                                $(emailMeSectionNejmEtocSelector).attr('checked', false);
                                            }
                                        }
                                    }
                                }

                            }

                        }else{
                            $(emailMeSectionSelector).show();
                            $(nejmEtocSelector).closest('label').show();
                            if (manualCheck === undefined || manualCheck === null || manualCheck === 'null') {
                                if ($.inArray(val, array_vals2) >= 0) {
                                    $(emailMeSectionCheckboxSelector).attr('checked', 'checked');
                                } else {
                                    $(emailMeSectionCheckboxSelector).attr('checked', false);
                                }
                            }
                            if(val == 'US'){
                                $(nejmEtocSelector).closest('label').hide();
                                if ($(emailMeSectionNejmSoiSelector).is(':checked')) {
                                    $(emailMeSectionNejmEtocSelector).attr('checked','checked');
                                }
                            }
                        }
                        clearInterval(emailMeSection);
                    }, 1000);
                }
            } else {
            $(emailMeSectionSelector).show();

            let selectemailme = setInterval(function() {
                val = $(billingAddressCountryIdSelector).val();
                if(val != undefined || val != null){
                    clearInterval(selectemailme);
                }
                    $(nejmEtocSelector).closest('label').show();
                    if (manualCheck === undefined || manualCheck === null || manualCheck === 'null') {
                        if ($.inArray(val, array_vals2) >= 0) {
                            $(emailMeSectionCheckboxSelector).attr('checked', 'checked');
                        } else {
                            $(emailMeSectionCheckboxSelector).attr('checked', false);
                        }
                    }
                    if (val == 'US') {
                        if ($(emailMeSectionNejmSoiSelector).is(':checked')) {
                            $(emailMeSectionNejmSoiSelector).attr('checked', 'checked');
                        }
                        $(nejmEtocSelector).closest('label').hide();
                    }

            }, 1000);

        }
        },

        /**
         * Cancel address edit action
         */
        cancelAddressEdit: function () {
            this.restoreBillingAddress();

            if (quote.billingAddress()) {
                // restore 'Same As Shipping' checkbox state
                this.isAddressSameAsShipping(
                    quote.billingAddress() != null &&
                    quote.billingAddress().getCacheKey() == quote.shippingAddress().getCacheKey() && //eslint-disable-line
                    !quote.isVirtual()
                );
                this.isAddressDetailsVisible(true);
            }
        },

        /**
         * Manage cancel button visibility
         */
        canUseCancelBillingAddress: ko.computed(function () {
            return quote.billingAddress() || lastSelectedBillingAddress;
        }),

        /**
         * Restore billing address
         */
        restoreBillingAddress: function () {
            if (lastSelectedBillingAddress != null) {
                selectBillingAddress(lastSelectedBillingAddress);
            }
        },

        /**
         * @param {Number} countryId
         * @return {*}
         */
        getCountryName: function (countryId) {
            return countryData()[countryId] !== undefined ? countryData()[countryId].name : ''; //eslint-disable-line
        },

        /**
         * Trigger action to update shipping and billing addresses
         */
        updateAddresses: function () {
            if (window.checkoutConfig.reloadOnBillingAddress ||
                !window.checkoutConfig.displayBillingOnPaymentMethod
            ) {
                setBillingAddressAction(globalMessageList);
            }

        },

        /**
         * Get code
         * @param {Object} parent
         * @returns {String}
         */
        getCode: function (parent) {
            return _.isFunction(parent.getCode) ? parent.getCode() : 'shared';
        },

        _bindWithSavedAddress: function (address, isSaved) {
            if (isSaved && address && address.regionId === '178') {
                this.hideCheckboxElement();
            } else if (!isSaved && $(billingAddrFormRegionIdSelector).val() === '178') {
                this.hideCheckboxElement();
            } else {
                $.async(customCheckboxSelector, function (element) {
                    $(element).hide();
                });
                $.async(placeOrderSelector, function (element) {
                    $(element).removeAttr('disabled');
                });
                $.async(paypalOrderSelector, function (element) {
                    $(element).removeClass('paypal-disabled');
                });
            }
        },
        isBrandNejm: function() {
            if (window.checkoutConfig.brand == 'NEJM') {
                return true;
            }
            return false;
        },
        isBrandCatalyst: function() {
            if (window.checkoutConfig.brand != 'NEJM') {
                return true;
            }
            return false;
        },
        hideCheckboxElement: function () {
            $.async(customCheckboxSelector, function (element) {
                $(element).show();
            });
            $.async(customCheckboxInputSelector, function (element) {
                $(element).prop('checked', false);
            });

            $.async(placeOrderSelector, function (element) {
                $(element).attr('disabled', 'disabled');
            });
            $.async(paypalOrderSelector, function (element) {
                $(element).addClass('paypal-disabled');
            });
        }
    });
});
