define([
    'jquery',
    'mage/cookies'
], function ($) {
    'use strict';

    return {
        piInformationCookie: 'pi_information',
        backToArticleCookie: 'back_to_article',
        manualCheckCookie: 'ManualCheck',

        _init: function() {
            let now = new Date();
            now.setMinutes(now.getMinutes() + 2); // timestamp
            now = new Date(now); // Date object

            $.mage.cookies.set(this.piInformationCookie, '', {
                expires: now
            });
            $.mage.cookies.set(this.backToArticleCookie, '', {
                expires: now
            });
            $.mage.cookies.set(this.manualCheckCookie, '', {
                expires: now
            });
        }
    };
});
