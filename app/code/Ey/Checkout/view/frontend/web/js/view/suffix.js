define([
	'jquery',
    'Magento_Ui/js/form/element/abstract'
], function($,Component) {
    'use strict';

    return Component.extend({
    	
		defaults: {
			template: 'Ey_Checkout/suffix'
		},

        initialize: function () {
            this._super();
            //$("#pi_suffix").change(this.onUpdate());
            return this;
        }
    });
});
