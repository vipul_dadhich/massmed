define(
    [
        'uiComponent'
    ],
    function (Component) {
        "use strict";

        var quoteItemData = window.checkoutConfig.quoteItemData;

        return Component.extend({
            defaults: {
                template: 'Ey_Checkout/summary/item/details'
            },
            quoteItemData: quoteItemData,

            getValue: function(quoteItem) {
                var item = this.getItem(quoteItem.item_id);
                if (item) {
                    return item.name;
                }
            },

            getTerm: function(quoteItem) {
                var item = this.getItem(quoteItem.item_id);
                if (item) {
                    return item.term;
                }
            },

            getItem: function(item_id) {
                var itemElement = null;
                _.each(this.quoteItemData, function(element, index) {
                    if (parseInt(element.item_id) === parseInt(item_id)) {
                        itemElement = element;
                    }
                });
                return itemElement;
            }
        });
    }
);
