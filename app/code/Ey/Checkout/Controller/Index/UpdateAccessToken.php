<?php

namespace Ey\Checkout\Controller\Index;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class UpdateAccessToken
 * @package Ey\Checkout\Controller\Index
 */
class UpdateAccessToken extends Action
{
    /**
     * @var Session
     */
    protected $_customerSession;
    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * UpdateAccessToken constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Session $customerSession
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Session $customerSession
    ) {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $response = ['status' => false];
        if ($this->_customerSession->isLoggedIn()) {
            $postParams = $this->getRequest()->getPostValue();
            if (isset($postParams['accessToken']) && $postParams['accessToken'] != '') {
                $this->_customerSession->setAkamaiAccessToken($postParams['accessToken']);
                $response['status'] = true;
            }
        }

        $resultJson = $this->_resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
