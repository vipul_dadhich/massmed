<?php
namespace Ey\Checkout\Controller\Index;
use Ey\MuleSoft\Model\Core\Logger\Monolog;
class CheckEmail extends \Magento\Framework\App\Action\Action{

	/**
 	* @param \Magento\Backend\App\Action\Context $context
 	* @param \Magento\Customer\Model\Session $customerSession
 	* @param \Magento\Customer\Model\Customer $customer
 	* @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
 	*/
	public function __construct(
    	\Magento\Backend\App\Action\Context $context,
    	\Magento\Customer\Model\Session $customerSession,
    	\Magento\Customer\Model\Customer $customer,
    	\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Ey\MuleSoft\Model\Catalyst\Api\GetCustomer $getCustomerApi,
        \Ey\MuleSoft\Helper\Data $mulesoftHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        Monolog $mulesoftLogger
	) {
		parent::__construct($context);
    	$this->customerSession = $customerSession;
    	$this->customer = $customer;
    	$this->resultJsonFactory = $resultJsonFactory;
        $this->getCustomerApi = $getCustomerApi;
        $this->jsonHelper = $jsonHelper;
        $this->mulesoftHelper = $mulesoftHelper;
        $this->mulesoftLogger = $mulesoftLogger;
  	}
    public function execute(){
        $postParams = $this->getRequest()->getPostValue();
        $response = array();
        $response = ['login' => false];
        if($this->customerSession->isLoggedIn()){
                $customer = $this->customerSession->getCustomer();
                $fname = $customer->getFirstname();
                $lname = $customer->getLastname();
                $response = [
                    'login' => true,
                    'fname' => $fname,
                    'lname' => $lname,
                ];
        }
        else{
            if(isset($postParams['email']) && $postParams['email'] != ""){
                $customer = $this->customer;
                $customer->setWebsiteId(1);
                $customer->loadByEmail($postParams['email']);
                if ($customer && $customer->getId()) {
                    if($customer->getData('ucid')){
                        $fname = $customer->getFirstname();
                        $lname = $customer->getLastname();
                        $response = [
                            'login' => false,
                            'fname' => $fname,
                            'lname' => $lname,
                        ];
                    }else{
                        $customerResponse = $this->getCustomerApi->execute($postParams['email']);
                        $this->mulesoftLogger->log($customerResponse->getStatusCode(),'/var/log/checkEmail.log');
                        
                        if($customerResponse->getStatusCode() == 200){
                            $response = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                            $this->mulesoftLogger->log(print_r($response,true),'/var/log/checkEmail.log');
                            $customer = $this->mulesoftHelper->updateCustomer($response, $customer);
                            $fname = $customer->getFirstname();
                            $lname = $customer->getLastname();
                            $response = [
                                'login' => false,
                                'fname' => $fname,
                                'lname' => $lname,
                            ];
                        }
                    }
                }else{
                    $response = [
                        'login' => false,
                    ];
                }
            }
        }
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
