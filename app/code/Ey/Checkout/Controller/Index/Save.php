<?php
namespace Ey\Checkout\Controller\Index;

use Magento\Framework\Encryption\EncryptorInterface;

class Save extends \Magento\Framework\App\Action\Action{   
	/**
 	* @param \Magento\Backend\App\Action\Context $context
 	* @param \Magento\Checkout\Model\Session $checkoutSession
 	* @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
 	*/

 	const COOKIE_NAME = 'pi_information';
	const COOKIE_DURATION = 86400; // lifetime in seconds
 	protected $encryptor;
 	public function __construct(
    	\Magento\Backend\App\Action\Context $context,
    	 \Magento\Checkout\Model\Session $checkoutSession,
    	 \Magento\Customer\Model\Session $session,
    	\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
    	\Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
     	\Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
     	\Magento\Quote\Model\QuoteFactory $quoteFactory,
     	\Magento\Framework\Encryption\EncryptorInterface $encryptor
	) {
		
    	$this->session = $session;
    	$this->checkoutSession = $checkoutSession;
    	$this->resultJsonFactory = $resultJsonFactory;
    	$this->quoteFactory = $quoteFactory;
    	//Technical debt to inititae the object in the constructor. 
    	//Call the DI class in the contruct.
    	$this->_cookieManager = $cookieManager;
     	$this->_cookieMetadataFactory = $cookieMetadataFactory;
     	$this->encryptor = $encryptor;
    	parent::__construct($context);
  	}

	public function execute() {
		try{
			$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/mulesoft_sendorder.log');
        	$logger = new \Zend\Log\Logger();
        	$logger->addWriter($writer);
			$postParams = file_get_contents('php://input');
			//parse_str($request_body, $postParams);
			$quote = $this->checkoutSession->getQuote();	
		     //Technical Debt:
		     //Remove the logic of saving the pi information from cookie
		     //move this logic to JS object.
		     //This will change the logic to remove the cookie from here and write optimized code
			
   			//$logger->info("postParams Save.php:-> " .$postParams);
			$quoteId = $quote->getId();
   			$quote = $this->quoteFactory->create()->load($quoteId);
			$postParamsArray = json_decode($postParams);
			foreach($postParamsArray as $k=>$v){
				$quote->setData($k, $v);
			}
			$quote->setData('cart_was_updated', 1);
			$quote->save();

			foreach($quote->getData() as $key => $value){
            $exp_key = explode('_', $key);
            if($exp_key[0] == 'pi' || $exp_key[0] == 'catalyst'){
                 $arr_result[$key] = $value;
            }
        	}

     		$cryptedtoken='';
			if(!$this->session->isLoggedIn()){
        		$cryptedtoken = $this->setCrypt($postParams, $logger);

       	 	}
			return $this->resultJsonFactory->create()->setData(['message' => 'Success','quoteData' => $quote->getData() , 'pi_cookie' => $cryptedtoken  ]);

		}catch(Exception $e){
			return $this->resultJsonFactory->create()->setData(['message' => $e->getMessage()]);
		}
	}

	public function setCrypt($postParams , $logger){
		$data = $this->encryptor->encrypt($postParams);
		return base64_encode($data);
		}
	
}




