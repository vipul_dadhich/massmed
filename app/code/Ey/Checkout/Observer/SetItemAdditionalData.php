<?php

namespace Ey\Checkout\Observer;

use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote\Item;

class SetItemAdditionalData implements ObserverInterface
{
    /**
     * @var Product
     */
    protected $_product;

    /**
     * SetItemAdditionalData constructor.
     * @param Product $product
     */
    public function __construct(
        Product $product
    ) {
        $this->_product = $product;
    }

    /**
     * @param Observer $observer
     * @return $this|void
     */
    public function execute(Observer $observer )
    {
        /** @var Item $item */
        $item = $observer->getEvent()->getItem();
        $product = $this->_product->load($item->getProduct()->getId());
        $term = $product->getResource()->getAttribute('months')->getFrontend()->getValue($product);
        if ($term) {
            $item->setTerm($term);
        }
        $pdp_title = $product->getResource()->getAttribute('pdp_title')->getFrontend()->getValue($product);
        if ($pdp_title) {
            $item->setName($pdp_title);
        }
        return $this;
    }
}