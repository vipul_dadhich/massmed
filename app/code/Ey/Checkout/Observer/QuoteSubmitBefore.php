<?php

namespace Ey\Checkout\Observer;
use Magento\Framework\App\Area;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;

class QuoteSubmitBefore implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $_cookieManager;
    /**
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
     **/
    protected $encryptor;
    /**
     * @var \Ey\MuleSoft\Helper\Data
     */
    protected $helper;
    /**
     * @var \MMS\Logger\LoggerInterface
     */
    protected $logger;
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;
    /**
     * @var mixed
     */
    protected $isDebugMode;
    /**
     * @var boolean
     */
    protected $isDebugModeType;
    /**
     * @var \Ey\MuleSoft\Model\Catalyst\Api\GetCustomer
     */
    protected $getCustomerApi;
    /**
     * @var \Ey\MuleSoft\Model\Catalyst\Api\CreateCustomer
     */
    protected $createCustomerApi;
    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_messageManager;
    /**
     * QuoteSubmitBefore constructor.
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
     * @param \Magento\Customer\Model\Session $session
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Ey\MuleSoft\Model\Catalyst\Api\GetCustomer $getCustomerApi
     * @param \Ey\MuleSoft\Model\Catalyst\Api\CreateCustomer $createCustomerApi
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Ey\MuleSoft\Helper\Data $helper
     * @param \Ey\MuleSoft\Model\Core\Logger\Monolog $mulesoftLogger
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Checkout\Model\Session $checkoutSession,
     * @param \Magento\Framework\Message\ManagerInterface $messageManager,
     *
     */
    public function __construct(
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Ey\MuleSoft\Model\Catalyst\Api\GetCustomer $getCustomerApi,
        \Ey\MuleSoft\Model\Catalyst\Api\CreateCustomer $createCustomerApi,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Ey\MuleSoft\Helper\Data $helper,
        \MMS\Logger\LoggerInterface $logger,
        \Magento\Framework\App\State $state,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\RequestInterface $request,
        \MMS\ExclusiveAgent\Helper\Data $agentHelper,
        SessionCurrencyEngine $currencyenginesession
    ) {
        $this->_cookieManager = $cookieManager;
        $this->session = $session;
        $this->encryptor = $encryptor;
        $this->getCustomerApi = $getCustomerApi;
        $this->createCustomerApi = $createCustomerApi;
        $this->jsonHelper = $jsonHelper;
        $this->helper = $helper;
        $this->_logger = $logger;
        $this->isDebugMode = $this->helper->getMuleSoftDebug();
        $this->isDebugModeType = $this->helper->getMuleSoftDebugMode();
        $this->_state = $state;
        $this->checkoutSession = $checkoutSession;
        $this->_messageManager = $messageManager;
        $this->_request = $request;
        $this->agentHelper = $agentHelper;
        $this->currencyenginesession = $currencyenginesession;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function redirectToErrorPage($redirectUrl)
    {
        if ($this->_state->getAreaCode() !== Area::AREA_ADMINHTML) {
            $this->session->setCountryAllowedError(true);
            $this->session->setCustomRedirectUrl($redirectUrl);
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Order not allowed for selected country.')
            );
        }
        return '';
    }
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getOrder();
        $quote = $observer->getQuote();
        $isAgent = $this->agentHelper->getIsExclusiveAgent($quote);
        if ($isAgent) {
            $redirectUrl = $this->agentHelper->getOrderCountryJapanOrKoreaUrl($order);
            if ($redirectUrl) {
                $this->redirectToErrorPage($redirectUrl);
            }
        }
        $postParamsArray = [];
        if($this->_state->getAreaCode() != 'adminhtml') {
            $postParams = file_get_contents('php://input');
            $postParamsArray = json_decode($postParams, true);
            $this->processCustomer($order, $postParamsArray);
            if ($quote->getData('is_quote_renew')) {
                $customerEmail = $order->getData('customer_email');
                $websiteId = $this->helper->getWebsiteId();
                $customer = $this->helper->customerExists($customerEmail,$websiteId);
                $professionalCat = $this->checkoutSession->getProfessionalCatagory();
                $this->checkoutSession->unsProfessionalCatagory();
                 if ($this->isDebugMode) {
                        $this->_logger->debug(" ****************** Renew Professional category change *******************");
                        $this->_logger->debug("Quote Professional Category:".$professionalCat);
                        $this->_logger->debug("Customer Professional Category:".$customer->getData('professional_category'));
                    }
               if ($professionalCat && $professionalCat != $this->session->getCustomer()->getData('professional_category')) {
                    $this->updateCustomerProfessionalCategory($quote, $order, $postParamsArray, $customer, $professionalCat);
               }
            }
            else {
                if ($this->session->isLoggedIn() && !$quote->getData('is_paybill')) {
                    $this->_logger->debug("Checking Professional Category:");
                    $customerEmail = $order->getData('customer_email');
                    $websiteId = $this->helper->getWebsiteId();
                    $customer = $this->helper->customerExists($customerEmail,$websiteId);
                    $professionalCat = $this->currencyenginesession->getProfessionalCategory();
                    $this->_logger->debug("Session Professional Category:".$professionalCat);
                    $this->_logger->debug("Customer Professional Category:".$this->session->getCustomer()->getData('professional_category'));
                    if ($professionalCat && $professionalCat != $this->session->getCustomer()->getData('professional_category')) {
                        $this->updateCustomerProfessionalCategory($quote, $order, $postParamsArray, $customer, $professionalCat);
                    }
                }

            }
        }else{
            $this->processCustomerforAdmin($order);
        }

        return $this;
    }

    public function updateCustomerProfessionalCategory($quote, $order, $postParamsArray, $customer, $professionalCat){

        $customerPayload = $this->helper->createCustomerPayload($customer, $order, $postParamsArray);
        $customerPayload = $this->addMandatoryFieldstoPayload($customer, $customerPayload);
        $customerPayload['customer']['professionalCategory'] = $professionalCat;
        $this->_logger->debug("Professional Category change Payload:". json_encode($customerPayload, JSON_PRETTY_PRINT));
        try {
            $customerResponse = $this->createCustomerApi->execute($customerPayload);
        } catch(\Exception $e) {
            $this->_logger->debug($e->getMessage());
            $this->helper->redirectToErrorPage();
            return;
        }
        if ($this->isDebugMode) {
            $this->_logger->debug("Professional Update API Response Code-");
            $this->_logger->debug($customerResponse->getStatusCode());
        }
        if ($customerResponse->getStatusCode() != 201) {
            $this->helper->redirectToErrorPage();
            return ;
        }
        $customer->setProfessionalCategory($professionalCat)->save();
        if ($this->isDebugMode) {
            $this->_logger->debug(" ****************** Professional Update API Call End *******************");
        }
    }
    /**
     * @param $order
     * @param $postParamsArray
     */
    public function processCustomer($order, $postParamsArray)
    {
        try {
            if (!$this->session->getCustomer()->getData('ucid')) {
                $customerEmail = $order->getData('customer_email');
                $websiteId = $this->helper->getWebsiteId();
                $customer = $this->helper->customerExists($customerEmail,$websiteId);
                if ($customer && $customer->getData('ucid') == "") {
                    // Existing User with no UCID
                    // Call validating customer AKAMAI API and get the UCID and pi information
                    if ($this->isDebugMode) {
                        $this->_logger->debug(" ****************** Get Customer API Call Start *******************");
                    }

                    try {
                        $customerResponse = $this->getCustomerApi->execute($customer->getEmail());
                    } catch(\Exception $e) {
                        if ($this->isDebugMode) {
                            $this->_logger->debug("Get Customer API Exception-");
                            $this->_logger->debug($e->getMessage());
                        }
                        $this->helper->redirectToErrorPage();
                        return;
                    }

                    if ($this->isDebugMode) {
                        $this->_logger->debug("Get Customer API Response Code-");
                        $this->_logger->debug($customerResponse->getStatusCode());
                    }

                    if ($customerResponse->getStatusCode() != 200) {
                        $this->helper->redirectToErrorPage();
                        return;
                    }

                    $response = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                    if ($this->isDebugMode) {
                        if ($this->isDebugModeType) {
                            $this->_logger->debug("Get Customer API Response Content-");
                            $this->_logger->debug(json_encode($response, JSON_PRETTY_PRINT));
                        }
                        $this->_logger->debug(" ****************** Get Customer API Call End *******************");
                    }
                    $customer = $this->helper->updateCustomer($response, $customer);
                    if (!$this->session->isLoggedIn() && $this->_state->getAreaCode() != 'adminhtml') {
                        if ($customer->getId() != '') {
                            $this->session->setCustomerAsLoggedIn($customer);
                        }
                        $this->session->setUcid($response['Customer']['ucid']);
                    }
                } else if(!$customer) {
                    /**
                     * New User
                     * Call create customer AKAMAI API and get the UCID
                     */

                    if ($this->isDebugMode) {
                        $this->_logger->debug(" ****************** Customer detected as LEAD Type  *******************");
                        $this->_logger->debug($postParamsArray);
                        //$this->_logger->debug($order->getBillingAddress());
                    }

                    $customer = $this->helper->createCustomerBeforeOrder($order, $postParamsArray);
                    // Check if new customer is a LEAD customer
                    if ($this->isDebugMode) {
                        $this->_logger->debug(" ****************** Get Customer API Call For LEAD User Start *******************");
                    }
                    try {
                        $customerResponse = $this->getCustomerApi->execute($customer->getEmail());
                    } catch(\Exception $e) {
                        if ($this->isDebugMode) {
                            $this->_logger->debug("Get LEAD Customer API Exception-");
                            $this->_logger->debug($e->getMessage());
                        }
                        $this->helper->redirectToErrorPage();
                        return;
                    }
                    if ($this->isDebugMode) {
                        $this->_logger->debug("Get LEAD Customer API Response Code-");
                        $this->_logger->debug($customerResponse->getStatusCode());
                    }
                    if ($customerResponse->getStatusCode() == 200) {
                        $response = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                        if(isset($response['ucid'])){
                            $customer->setUcid($response['ucid'])->save();
                        }
                        if ($this->isDebugMode && $this->isDebugModeType) {
                            $this->_logger->debug("Get LEAD Customer API Response Content-");
                            $this->_logger->debug(json_encode($response, JSON_PRETTY_PRINT));
                        }
                    }

                    if ($this->isDebugMode) {
                        $this->_logger->debug(" ****************** Get LEAD Customer API Call End *******************");
                    }

                    if ($this->isDebugMode) {
                        $this->_logger->debug(" ****************** Customer Create API Call Start *******************");
                    }
                    $customerPayload = $this->helper->createCustomerPayload($customer, $order, $postParamsArray);
                    if ($this->isDebugMode && $this->isDebugModeType) {
                        $loggingObject = $customerPayload;
                        if (isset($loggingObject['customer']['password'])) {
                            $loggingObject['customer']['password'] = '************';
                        }
                        $this->_logger->debug("Customer Create API Payload-");
                        $this->_logger->debug(json_encode($loggingObject, JSON_PRETTY_PRINT));
                    }

                    try {
                        $customerResponse = $this->createCustomerApi->execute($customerPayload);
                    } catch(\Exception $e) {
                        $this->_logger->debug($e->getMessage());
                        $this->helper->redirectToErrorPage();
                        return;
                    }
                    if ($this->isDebugMode) {
                        $this->_logger->debug("Customer Create API Response Code-");
                        $this->_logger->debug($customerResponse->getStatusCode());
                    }
                    if ($customerResponse->getStatusCode() != 201) {
                        $this->helper->redirectToErrorPage();
                        return ;
                    }

                    $response = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                    if ($this->isDebugMode && $this->isDebugModeType) {
                        $this->_logger->debug("Customer Create API Response Content-");
                        $this->_logger->debug(json_encode($response, JSON_PRETTY_PRINT));
                    }

                    $this->helper->saveUcid($customer->getId(),$response['ucid']);
                    if (!$this->session->isLoggedIn() && $this->_state->getAreaCode() != 'adminhtml') {
                        if ($customer->getId() != '') {
                            $this->session->setCustomerAsLoggedIn($customer);
                        }
                        $this->session->setUcid($response['ucid']);
                        $this->session->setAccessToken($response['authenticationToken']);
                        $this->session->setCustomerCreatedByOrder('NO');
                    }

                    $this->checkoutSession->setPiInformationSession(false);

                    if ($this->isDebugMode) {
                        $this->_logger->debug(" ****************** Customer Create API Call End *******************");
                    }
                }
            } else {
                $customerEmail = $order->getData('customer_email');
                $websiteId = $this->helper->getWebsiteId();
                $customer = $this->helper->customerExists($customerEmail,$websiteId);
                $customer = $this->updateCustomerBeforeOrder($customer, $order, $postParamsArray);
                if ($this->isDebugMode) {
                    $this->_logger->debug(" ****************** Customer Update API Call Start *******************");
                }
                $customerPayload = $this->helper->createCustomerPayload($customer, $order, $postParamsArray);
                $customerPayload = $this->addMandatoryFieldstoPayload($customer, $customerPayload);

                if ($this->isDebugMode && $this->isDebugModeType) {
                    $this->_logger->debug("Customer Update API Payload-");
                    $this->_logger->debug(json_encode($customerPayload, JSON_PRETTY_PRINT));
                }

                try {
                    $customerResponse = $this->createCustomerApi->execute($customerPayload);
                } catch(\Exception $e) {
                    $this->_logger->debug($e->getMessage());
                    $this->helper->redirectToErrorPage();
                    return;
                }
                if ($this->isDebugMode) {
                    $this->_logger->debug("Customer Update API Response Code-");
                    $this->_logger->debug($customerResponse->getStatusCode());
                }
                if ($customerResponse->getStatusCode() != 201) {
                    $this->helper->redirectToErrorPage();
                    return ;
                }
                $this->checkoutSession->setPiInformationSession(false);

                if ($this->isDebugMode) {
                    $this->_logger->debug(" ****************** Customer Update API Call End *******************");
                }
            }
        } catch(\Exception $e) {
            if ($this->isDebugMode) {
                $this->_logger->debug("Process Customer API Exception-");
                $this->_logger->debug($e->getMessage());
            }
            $this->helper->redirectToErrorPage();
            return;
        }
    }
    public function addMandatoryFieldstoPayload($customer, $customerPayload){
        if (isset($customerPayload['customer']['password'])) {
            unset($customerPayload['customer']['password']);
        }
        if (isset($customerPayload['customer']['country'])) {
            unset($customerPayload['customer']['country']);
        }
        if (isset($customerPayload['customer']['insightsCouncilMember'])) {
            unset($customerPayload['customer']['insightsCouncilMember']);
        }
        return $customerPayload;
    }
    public function updateCustomerBeforeOrder($customer, $order, $postParams)
    {
        $orderData = $this->helper->getCustomerInfoFromOrder($order, $postParams);
        if ($this->isDebugMode) {
            $this->_logger->debug("Update Customer");
            $this->_logger->debug(json_encode($orderData, JSON_PRETTY_PRINT));
        }
        try {
            $customerData = $customer->getDataModel();
            if (isset($orderData['customer']['catalystConnect']) && $orderData['customer']['catalystConnect'] != '') {
                $customerData->setCustomAttribute(
                    'catalystconnect',
                    $orderData['customer']['catalystConnect']
                );
            }
            if (isset($orderData['customer']['catalystSOI']) && $orderData['customer']['catalystSOI'] != '') {
                $customerData->setCustomAttribute(
                    'catalystsoi', $orderData['customer']['catalystSOI']
                );
            }

            if (isset($orderData['customer']['nejmsoi']) && $orderData['customer']['nejmsoi'] != '') {
                $customerData->setCustomAttribute(
                    'nejmsoi', $orderData['customer']['nejmsoi']
                );
            }
            if (isset($orderData['customer']['nejmetoc']) && $orderData['customer']['nejmetoc'] != '') {
                $customerData->setCustomAttribute(
                    'nejmetoc', $orderData['customer']['nejmetoc']
                );
            }
            $customer->updateData($customerData);
            $customer->setConfirmation(null);
            $customer->save();
            return $customer;
        } catch(\Exception $e) {
            $this->_logger->debug("Create Customer Exception:-> ".$e->getMessage());
            $this->redirectToErrorPage();
            return true;
        }
    }
    /**
     * @param $order
     */
    public function processCustomerforAdmin($order)
    {
        try {
            $customerEmail = $order->getData('customer_email');
            $websiteId = $this->helper->getWebsiteId();
            $customer = $this->helper->customerExists($customerEmail,$websiteId);
            try {
                $customerResponse = $this->getCustomerApi->execute($customer->getEmail());
                if ($customerResponse->getStatusCode() == 200) {
                    $existingCustomerResponse = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                }
            } catch(\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __($e->getMessage())
                );
            }

            if ($this->isDebugMode) {
                $this->_logger->debug("Get Customer API Response Code-");
                $this->_logger->debug($customerResponse->getStatusCode());
            }

            if ($customerResponse->getStatusCode() != 200 || (isset($existingCustomerResponse['audienceType']) && $existingCustomerResponse['audienceType'] == 'LEAD')) {
                try {
                    if ($this->isDebugMode) {
                        $this->_logger->debug("Create Customer payload request for admin-");
                    }
                    $postvalues = $this->_request->getPost();
                    $customerPayload = $this->helper->createCustomerPayloadForAdmin($customer, $order, $postvalues);
                    if (isset($existingCustomerResponse['audienceType']) && $existingCustomerResponse['audienceType'] == 'LEAD') {
                        $customerPayload['customer']['ucid'] = $existingCustomerResponse['ucid'];
                    }
                    if ($this->isDebugMode && $this->isDebugMode) {
                        $this->_logger->debug("Create Customer payload " . json_encode($customerPayload, JSON_PRETTY_PRINT));
                    }
                    $customerResponse = $this->createCustomerApi->execute($customerPayload);

                } catch(\Exception $e) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __($e->getMessage())
                    );
                }
                if ($this->isDebugMode) {
                    $this->_logger->debug("Response code-" . $customerResponse->getStatusCode());
                }
                if ($customerResponse->getStatusCode() != 201) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __('Customer Create API Error code: '.$customerResponse->getStatusCode())
                    );
                }

                $response = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                if ($this->isDebugMode && $this->isDebugMode) {
                    $this->_logger->debug("Response content-" . json_encode($response, JSON_PRETTY_PRINT));
                }
                if (isset($response['ucid'])) {
                    $this->helper->saveUcid($customer->getId(),$response['ucid']);
                }
                if ($this->isDebugMode) {
                    $this->_logger->debug("Admin Create Customer API End-");
                }
                return;
            }
            if ($this->isDebugMode) {
                $this->_logger->debug("Get Customer API Response Content-");
                $this->_logger->debug(json_encode($existingCustomerResponse, JSON_PRETTY_PRINT));
                $this->_logger->debug(" ****************** Get Customer API Call End *******************");
            }

        } catch(\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
            );
        }
    }
}
