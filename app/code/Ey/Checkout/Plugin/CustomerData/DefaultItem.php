<?php

namespace Ey\Checkout\Plugin\CustomerData;

use Magento\Catalog\Model\Product;
use Magento\Checkout\Model\Session;

class DefaultItem
{

    /**
     * @var Product
     */
    protected $product;
    /**
     * @var Session
     */
    protected $_checkoutSession;

    /**
     * DefaultItem constructor.
     * @param Product $product
     * @param Session $checkoutSession
     */
    public function __construct(
        Product $product,
        Session $checkoutSession
    ) {
        $this->product = $product;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Checkout\CustomerData\DefaultItem $subject
     * @param $result
     * @return mixed
     */
    public function afterGetItemData(
        \Magento\Checkout\CustomerData\DefaultItem $subject,
        $result
    ) {
        try {
            $product = $this->product->load($result['product_id']);
            $term = $product->getResource()->getAttribute('months')->getFrontend()->getValue($product);
            $pdp_title = $product->getResource()->getAttribute('pdp_title')->getFrontend()->getValue($product);
            $result['term'] = $term;
            $result['product_name'] = $pdp_title;
            $result['name'] = $pdp_title;
            $result['showDeleteButton'] = 1;
            if ($this->_checkoutSession->getQuote()->getData('is_quote_renew')) {
                $result['showDeleteButton'] = 0;
            }
            $result['showOptions'] = 1;
            if ($this->_checkoutSession->getQuote()->getData('is_paybill')) {
                $result['showOptions'] = 0;
            }
        } catch (\Exception $e) {
        }

        return $result;
    }
}
