<?php

namespace Ey\Checkout\Plugin\Checkout\Block\Checkout;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session;

/**
 * Class LayoutProcessor
 * @package Ey\Checkout\Plugin\Checkout\Block\Checkout
 */
class LayoutProcessor
{
    /**
     * @var null
     */
    public $quote = null;
    /**
     * @var Session
     */
    protected $customerSession;
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * LayoutProcessor constructor.
     * @param Session $customerSession
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        Session $customerSession,
        CheckoutSession $checkoutSession
    ) {
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
    }

    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {

        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['firstname']['validation']['required-entry'] = false;
		$jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['lastname']['validation']['required-entry'] = false;
    	$jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['country_id']['sortOrder'] = 0;
    	$jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['street']['sortOrder'] = 1;
    	$jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['city']['sortOrder'] = 2;
    	$jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['region']['sortOrder'] = 3;
    	$jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['region_id']['sortOrder'] = 4;
	    $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['postcode']['sortOrder'] = 5;
    	$jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['vat_id']['sortOrder'] = 6;


    	if (
    		isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['street']['children']) &&
    		is_array($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['street']['children'])
    	) {
    		foreach ($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['street']['children'] as $k => $v) {
				$nv = $v;
				$nv['validation']['max_text_length'] = 140;
				$jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['street']['children'][$k] = $nv;
    		}
    	}

		$jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['city']['validation']['max_text_length'] = 80;
    	//Vipul - INTEG- 4143 - This code has been retained for future use as the Business was only want a hotfix specific code. This code block might be useful in future.  We have standadize the code across the github.

        /* NEJM SOI*/
        $jsLayout['components']
        ['checkout']
        ['children']
        ['steps']
        ['children']
        ['billing-step']
        ['children']
        ['payment']
        ['children']
        ['afterMethods']
        ['children']
        ['billing-address-form']
        ['children']
        ['form-fields']
        ['children']
        ['nejm_soi'] = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'billingAddressshared.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'id' => 'nejm_soi'
            ],
            'dataScope' => 'billingAddressshared.custom_attributes.nejm_soi',
            'label' => '',
            'provider' => 'checkoutProvider',
            'sortOrder' => 350,
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => false,
            'value' => ''
        ];

        /* NEJM ETOC*/
        $jsLayout['components']
        ['checkout']
        ['children']
        ['steps']
        ['children']
        ['billing-step']
        ['children']
        ['payment']
        ['children']
        ['afterMethods']
        ['children']
        ['billing-address-form']
        ['children']
        ['form-fields']
        ['children']
        ['nejm_etoc'] = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'billingAddressshared.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'id' => 'nejm_etoc'
            ],
            'dataScope' => 'billingAddressshared.custom_attributes.nejm_etoc',
            'label' => '',
            'provider' => 'checkoutProvider',
            'sortOrder' => 350,
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => false,
            'value' => ''
        ];
        /* Catalyst Connect */
        $jsLayout['components']
        ['checkout']
        ['children']
        ['steps']
        ['children']
        ['billing-step']
        ['children']
        ['payment']
        ['children']
        ['afterMethods']
        ['children']
        ['billing-address-form']
        ['children']
        ['form-fields']
        ['children']
        ['customer_catalyst_connect'] = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'billingAddressshared.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'id' => 'customer_catalyst_connect'
            ],
            'dataScope' => 'billingAddressshared.custom_attributes.customer_catalyst_connect',
            'label' => '',
            'provider' => 'checkoutProvider',
            'sortOrder' => 350,
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => false,
            'value' => ''
        ];
        /* Catalyst SOI */
        $jsLayout['components']
        ['checkout']
        ['children']
        ['steps']
        ['children']
        ['billing-step']
        ['children']
        ['payment']
        ['children']
        ['afterMethods']
        ['children']
        ['billing-address-form']
        ['children']
        ['form-fields']
        ['children']
        ['customer_catalyst_soi'] = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'billingAddressshared.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'id' => 'customer_catalyst_soi'
            ],
            'dataScope' => 'billingAddressshared.custom_attributes.customer_catalyst_soi',
            'label' => '',
            'provider' => 'checkoutProvider',
            'sortOrder' => 350,
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => false,
            'value' => ''
        ];

        if ($this->getQuote()->isVirtual()) {
            $this->loadVirtualQuoteFields($jsLayout);
        } else {
            $this->loadPhysicalQuoteFields($jsLayout);
        }

        $this->getShippingAddressCodeFields($jsLayout);
        $this->getBillingAddressCodeFields($jsLayout);

        return $jsLayout;
    }

    /**
     * @param $jsLayout
     */
    private function loadVirtualQuoteFields(&$jsLayout)
    {
        if (!$this->customerSession->isLoggedIn()) {
            $jsLayout['components']['checkout']['children']['paymentMethod']['children']['billingAddress']['children']['region_id']['validation']['custom-rule'] = 1;

            //Vipul - INTEG- 4143 - This code has been retained for future use as the Business was only want a hotfix specific code. This code block might be useful in future. We have standadize the code across the github.

            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['city']['validation']['max_text_length'] = 80;


            /* PASSWORD */
            $jsLayout['components']
            ['checkout']
            ['children']
            ['steps']
            ['children']
            ['billing-step']
            ['children']
            ['payment']
            ['children']
            ['afterMethods']
            ['children']
            ['billing-address-form']
            ['children']
            ['form-fields']
            ['children']
            ['ba_password'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/password',
                    'id' => 'ba_password'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.ba_password',
                'label' => 'Create Password',
                'provider' => 'checkoutProvider',
                'sortOrder' => 350,
                'validation' => [
                    'required-entry' => true,
                    'validate-no-empty' => true,
                    'max_text_length' => 128,
                    'min_text_length' => 8
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];


            /* PASSWORD CONFIRM */
            $jsLayout['components']
            ['checkout']
            ['children']
            ['steps']
            ['children']
            ['billing-step']
            ['children']
            ['payment']
            ['children']
            ['afterMethods']
            ['children']
            ['billing-address-form']
            ['children']
            ['form-fields']
            ['children']
            ['ba_password_confirm'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/password_confirm',
                    'id' => 'ba_password_confirm'
                ],
                // 'dataScope' => 'billingAddressshared.custom_attributes.ba_password_confirm',
                'label' => 'Confirm Password',
                'provider' => 'checkoutProvider',
                'sortOrder' => 360,
                'validation' => [
                    'required-entry' => true,
                    'validate-no-empty' => true,
                    'max_text_length' => 128,
                    'min_text_length' => 8,
                    'equalTo' => '#ba_password'
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];

            /* FIRST NAME */
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['firstname'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'billingAddressshared',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/firstname',
                    'id' => 'pi_firstname'
                ],
                'dataScope' => 'billingAddressshared.firstname',
                'label' => 'First Name',
                'provider' => 'checkoutProvider',
                'sortOrder' => 350,
                'validation' => [
                    'required-entry' => true,
                    'validate-no-empty' => true,
                    'max_text_length' => 40
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];

            /* LAST NAME */
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['lastname'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'billingAddressshared',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/lastname',
                    'id' => 'pi_lastname'
                ],
                'dataScope' => 'billingAddressshared.lastname',
                'label' => 'Last Name',
                'provider' => 'checkoutProvider',
                'sortOrder' => 360,
                'validation' => [
                    'required-entry' => true,
                    'validate-no-empty' => true,
                    'max_text_length' => 40
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];

            /* SUFFIX */
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_suffix'] = [
                'component' => 'Ey_Checkout/js/view/suffix',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/suffix',
                    'id' => 'pi_suffix'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.pi_suffix',
                'label' => 'Suffix',
                'provider' => 'checkoutProvider',
                'sortOrder' => 370,
                'validation' => [
                    'required-entry' => true
                ],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];

            /* PROFESSIONAL CATEGORY */
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_professional_category'] = [
                'component' => 'Ey_Checkout/js/view/professionalcategory',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/professionalcategory',
                    'id' => 'pi_professional_category'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.pi_professional_category',
                'label' => 'Professional Category',
                'provider' => 'checkoutProvider',
                'sortOrder' => 380,
                'validation' => [
                    'required-entry' => true
                ],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];
            /* STUDENT TYPE */
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_student_type'] = [
                'component' => 'Ey_Checkout/js/view/studenttype',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/studenttype',
                    'id' => 'pi_student_type'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.pi_student_type',
                'label' => 'Student Type',
                'provider' => 'checkoutProvider',
                'sortOrder' => 385,
                'validation' => [
                    'required-entry' => true
                ],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => false
            ];
            /* PROFESSION */
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_profession'] = [
                'component' => 'Ey_Checkout/js/view/profession',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/profession',
                    'id' => 'pi_student_type'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.pi_profession',
                'label' => 'Profession',
                'provider' => 'checkoutProvider',
                'sortOrder' => 390,
                'validation' => [
                    'required-entry' => true
                ],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => false
            ];
            /* PRIMARY SPECIALTY */
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_primaryspecialty'] = [
                'component' => 'Ey_Checkout/js/view/primaryspecialty',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/primaryspecialty',
                    'id' => 'pi_primaryspecialty'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.pi_primaryspecialty',
                'label' => 'Primary Specialty',
                'provider' => 'checkoutProvider',
                'sortOrder' => 385,
                'validation' => [
                    'required-entry' => true
                ],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => false
            ];

            /* ROLE */
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_role'] = [
                'component' => 'Ey_Checkout/js/view/role',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/role',
                    'id' => 'pi_role'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.pi_role',
                'label' => 'Role',
                'provider' => 'checkoutProvider',
                'sortOrder' => 400,
                'validation' => [
                    'required-entry' => true
                ],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true
            ];

            /* ORGANIZATION */
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['organization'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/organization',
                    'id' => 'pi_organization'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.organization',
                'label' => 'Name of Organization',
                'provider' => 'checkoutProvider',
                'sortOrder' => 410,
                'validation' => [
                    'required-entry' => true,
                    'validate-no-empty' => true,
                    'max_text_length' => 80
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];
        }
    }

    /**
     * @param $jsLayout
     */
    private function loadPhysicalQuoteFields(&$jsLayout)
    {
        if (!$this->customerSession->isLoggedIn()) {
            $jsLayout['components']['checkout']['children']['paymentMethod']['children']['billingAddress']['children']['region_id']['validation']['custom-rule'] = 1;

            //Vipul - INTEG- 4143 - This code has been retained for future use as the Business was only want a hotfix specific code. This code block might be useful in future. We have standadize the code across the github.

            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['city']['validation']['max_text_length'] = 80;


            /* PASSWORD */
            $jsLayout['components']
            ['checkout']
            ['children']
            ['steps']
            ['children']
            ['billing-step']
            ['children']
            ['payment']
            ['children']
            ['afterMethods']
            ['children']
            ['billing-address-form']
            ['children']
            ['form-fields']
            ['children']
            ['ba_password'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/password',
                    'id' => 'ba_password'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.ba_password',
                'label' => 'Create Password',
                'provider' => 'checkoutProvider',
                'sortOrder' => 350,
                'validation' => [
                    'required-entry' => true,
                    'validate-no-empty' => true,
                    'max_text_length' => 128,
                    'min_text_length' => 8
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];


            /* PASSWORD CONFIRM */
            $jsLayout['components']
            ['checkout']
            ['children']
            ['steps']
            ['children']
            ['billing-step']
            ['children']
            ['payment']
            ['children']
            ['afterMethods']
            ['children']
            ['billing-address-form']
            ['children']
            ['form-fields']
            ['children']
            ['ba_password_confirm'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/password_confirm',
                    'id' => 'ba_password_confirm'
                ],
                // 'dataScope' => 'billingAddressshared.custom_attributes.ba_password_confirm',
                'label' => 'Confirm Password',
                'provider' => 'checkoutProvider',
                'sortOrder' => 360,
                'validation' => [
                    'required-entry' => true,
                    'validate-no-empty' => true,
                    'max_text_length' => 128,
                    'min_text_length' => 8,
                    'equalTo' => '#ba_password'
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];

            /* FIRST NAME */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['firstname'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'billingAddressshared',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/firstname',
                    'id' => 'pi_firstname'
                ],
                'dataScope' => 'billingAddressshared.firstname',
                'label' => 'First Name',
                'provider' => 'checkoutProvider',
                'sortOrder' => 350,
                'validation' => [
                    'required-entry' => true,
                    'validate-no-empty' => true,
                    'max_text_length' => 40
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];

            /* LAST NAME */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['lastname'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'billingAddressshared',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/lastname',
                    'id' => 'pi_lastname'
                ],
                'dataScope' => 'billingAddressshared.lastname',
                'label' => 'Last Name',
                'provider' => 'checkoutProvider',
                'sortOrder' => 360,
                'validation' => [
                    'required-entry' => true,
                    'validate-no-empty' => true,
                    'max_text_length' => 40
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];

            /* SUFFIX */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_suffix'] = [
                'component' => 'Ey_Checkout/js/view/suffix',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/suffix',
                    'id' => 'pi_suffix'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.pi_suffix',
                'label' => 'Suffix',
                'provider' => 'checkoutProvider',
                'sortOrder' => 370,
                'validation' => [
                    'required-entry' => true
                ],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];

            /* PROFESSIONAL CATEGORY */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_professional_category'] = [
                'component' => 'Ey_Checkout/js/view/professionalcategory',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/professionalcategory',
                    'id' => 'pi_professional_category'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.pi_professional_category',
                'label' => 'Professional Category',
                'provider' => 'checkoutProvider',
                'sortOrder' => 380,
                'validation' => [
                    'required-entry' => true
                ],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];
            /* STUDENT TYPE */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_student_type'] = [
                'component' => 'Ey_Checkout/js/view/studenttype',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/studenttype',
                    'id' => 'pi_student_type'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.pi_student_type',
                'label' => 'Student Type',
                'provider' => 'checkoutProvider',
                'sortOrder' => 385,
                'validation' => [
                    'required-entry' => true
                ],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => false
            ];
            /* PROFESSION */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_profession'] = [
                'component' => 'Ey_Checkout/js/view/profession',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/profession',
                    'id' => 'pi_student_type'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.pi_profession',
                'label' => 'Profession',
                'provider' => 'checkoutProvider',
                'sortOrder' => 390,
                'validation' => [
                    'required-entry' => true
                ],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => false
            ];
            /* PRIMARY SPECIALTY */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_primaryspecialty'] = [
                'component' => 'Ey_Checkout/js/view/primaryspecialty',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/primaryspecialty',
                    'id' => 'pi_primaryspecialty'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.pi_primaryspecialty',
                'label' => 'Primary Specialty',
                'provider' => 'checkoutProvider',
                'sortOrder' => 385,
                'validation' => [
                    'required-entry' => true
                ],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => false
            ];

            /* ROLE */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_role'] = [
                'component' => 'Ey_Checkout/js/view/role',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/role',
                    'id' => 'pi_role'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.pi_role',
                'label' => 'Role',
                'provider' => 'checkoutProvider',
                'sortOrder' => 400,
                'validation' => [
                    'required-entry' => true
                ],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true
            ];

            /* ORGANIZATION */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['organization'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Ey_Checkout/organization',
                    'id' => 'pi_organization'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.organization',
                'label' => 'Name of Organization',
                'provider' => 'checkoutProvider',
                'sortOrder' => 410,
                'validation' => [
                    'required-entry' => true,
                    'validate-no-empty' => true,
                    'max_text_length' => 80
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
                'value' => ''
            ];
        }
    }

    /**
     * Get Quote
     *
     * @return \Magento\Quote\Model\Quote|null
     */
    public function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }

        return $this->quote;
    }

    /**
     * @param $jsLayout
     */
    private function getShippingAddressCodeFields(&$jsLayout)
    {
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']
            ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'])
        ) {
            $jsLayout['components']['checkout']['children']['steps']['children']
            ['shipping-step']['children']['shippingAddress']['children']
            ['shipping-address-fieldset']['children']
            ['addresscode'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'id' => 'addresscode'
                ],
                'dataScope' => 'shippingAddress.custom_attributes.addresscode',
                'label' => '',
                'provider' => 'checkoutProvider',
                'sortOrder' => 1050,
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => false,
                'value' => 'C2'
            ];
        }
    }

    /**
     * @param $jsLayout
     */
    private function getBillingAddressCodeFields(&$jsLayout)
    {
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
            ['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children'])
        ) {
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
            ['payment']['children']['afterMethods']['children']['billing-address-form']['children']
            ['form-fields']['children']['addresscode'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'billingAddressshared.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'id' => 'addresscode'
                ],
                'dataScope' => 'billingAddressshared.custom_attributes.addresscode',
                'label' => '',
                'provider' => 'checkoutProvider',
                'sortOrder' => 1050,
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => false,
                'value' => 'C1'
            ];
        }
    }
}
