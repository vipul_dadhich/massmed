<?php

namespace Ey\Checkout\Plugin;

/**
 * Class ConfigProviderPlugin
 * @package Ey\Checkout\Plugin
 */
class ConfigProviderPlugin extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;
    /**
     * @var \MMS\Customer\Model\Source\StudentType
     */
    protected $studenttype;

    /**
     * @var \MMS\Customer\Model\Source\Profession
     */
    protected $profession;

    /**
     * @var \MMS\Customer\Model\Source\ProfessionalCategory
     */
    protected $professionalcategory;

    /**
     * ConfigProviderPlugin constructor.
     * @param \Magento\Quote\Model\Quote\Item $quoteItem
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \MMS\Customer\Model\Source\StudentType $studenttype
     * @param \MMS\Customer\Model\Source\Profession $profession
     * @param \MMS\Customer\Model\Source\ProfessionalCategory $professionalcategory
     */
	public function __construct(
	    \Magento\Quote\Model\Quote\Item $quoteItem,
        \Magento\Catalog\Model\Product $product,
        \Magento\Customer\Model\Session $customerSession,
        \MMS\Customer\Model\Source\StudentType $studenttype,
        \MMS\Customer\Model\Source\Profession $profession,
        \MMS\Customer\Model\Source\ProfessionalCategory $professionalcategory
    ){
        $this->quoteItem = $quoteItem;
        $this->product = $product;
        $this->customerSession = $customerSession;
        $this->studenttype = $studenttype;
        $this->profession = $profession;
        $this->professionalcategory = $professionalcategory;
    }

    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, array $result)
    {
        $customerData = $this->customerSession->getCustomer()->getData();
        $items = $result['quoteItemData'];
        $result['customerData']['customer_default_country'] = $this->customerSession->getCustomer()->getCustomerDefaultCountry();
        $result['customerData']['catalystsoi'] = $this->customerSession->getCustomer()->getCatalystsoi();
        $result['customerData']['catalystconnect'] = $this->customerSession->getCustomer()->getCatalystconnect();
        $result['customerData']['nejmsoi'] = $this->customerSession->getCustomer()->getNejmsoi();
        $result['customerData']['nejmetoc'] = $this->customerSession->getCustomer()->getNejmetoc();
        $result['customerData']['student_type_options'] = $this->studenttype->getAllOptions();
        $result['customerData']['profession'] = $this->profession->getAllOptions();
        $result['customerData']['professional_category_options'] = $this->professionalcategory->getAllOptions();
        $brand = 'Catalyst';
        for($i=0;$i<count($items);$i++){

            $quoteId = $items[$i]['item_id'];
            $quote = $this->quoteItem->load($quoteId);
            $productId = $quote->getProductId();
            $product = $this->product->load($productId);
            $term = $product->getResource()->getAttribute('months')->getFrontend()->getValue($product);
            $pdp_title = $product->getResource()->getAttribute('pdp_title')->getFrontend()->getValue($product);
            if ($product->getResource()->getAttribute('brand')->getFrontend()->getValue($product) == 'NEJM') {
                $brand = $product->getResource()->getAttribute('brand')->getFrontend()->getValue($product);
            }
            $items[$i]['term'] = $term;
            $items[$i]['name'] = $pdp_title;
        }
        $result['brand'] = $brand;
        $result['quoteItemData'] = $items;
        return $result;
    }
}
