<?php
namespace Ey\Checkout\Plugin\Model;
use Ey\MuleSoft\Model\Core\Logger\Monolog;

/**
 * Class AccountManagementPlugin
 * @package Ey\Checkout\Plugin\Model
 */
class AccountManagementPlugin{

    /**
     * AccountManagementPlugin constructor.
     * @param \Ey\MuleSoft\Model\Catalyst\Api\GetCustomer $getCustomerApi
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param Monolog $mulesoftLogger
     */
	public function __construct(
        \Ey\MuleSoft\Model\Catalyst\Api\GetCustomer $getCustomerApi,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        Monolog $mulesoftLogger
	) {
        $this->getCustomerApi = $getCustomerApi;
        $this->jsonHelper = $jsonHelper;
        $this->mulesoftLogger = $mulesoftLogger;
  	}

    /**
     * @param \Magento\Customer\Model\AccountManagement $subject
     * @param $result
     * @param $customerEmail
     * @param null $websiteId
     * @return false
     */
    public function afterIsEmailAvailable(\Magento\Customer\Model\AccountManagement
        $subject,
        $result,
        $customerEmail, $websiteId = null
    ){
        if ($result && $customerEmail) {
            $this->mulesoftLogger->log('********** Check Email On Checkout Page Start ***********');
            $customerResponse = $this->getCustomerApi->execute($customerEmail);
            $this->mulesoftLogger->log($customerResponse->getStatusCode());
            if ($customerResponse->getStatusCode() == 200) {
                $response = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                $this->mulesoftLogger->log(print_r($response,true));
                if (isset($response['audienceType']) && $response['audienceType'] == 'LEAD') {
                    $result = true;
                } else {
                    $result = false;
                }
            }
            $this->mulesoftLogger->log('********** Check Email On Checkout Page End ***********');
        }
        return $result;
    }
}
