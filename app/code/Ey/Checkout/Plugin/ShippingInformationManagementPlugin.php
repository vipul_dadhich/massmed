<?php

namespace Ey\Checkout\Plugin;

use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\ShippingInformationManagement;

/**
 * Class ShippingInformationManagementPlugin
 * @package Ey\Checkout\Plugin
 */
class ShippingInformationManagementPlugin
{
    /**
     * @var Session
     */
    private $_checkoutSession;

    /**
     * ShippingInformationManagementPlugin constructor.
     * @param Session $checkoutSession
     */
    public function __construct(
        Session $checkoutSession
    ) {
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param ShippingInformationManagement $subject
     * @param $cartId
     * @param ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        ShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    ) {
        $shippingAddress = $addressInformation->getShippingAddress();
        $shippingAddressExtensionAttributes = $shippingAddress->getExtensionAttributes();
        if ($shippingAddressExtensionAttributes) {
            $billingAddressCode = $shippingAddressExtensionAttributes->getBillingAddressCode();
            $shippingAddressCode = $shippingAddressExtensionAttributes->getMailingAddressCode();
            $this->_checkoutSession->setBillingAddressCode($billingAddressCode);
            $this->_checkoutSession->setMailingAddressCode($shippingAddressCode);
        }
    }
}
