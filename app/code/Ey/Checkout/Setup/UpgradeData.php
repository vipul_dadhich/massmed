<?php

namespace Ey\Checkout\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Module\Setup;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * Class UpgradeData
 * @package Ey\Checkout\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CustomerSetupFactory
     */
    private $_customerSetupFactory;

    /**
     * UpgradeData constructor.
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->_customerSetupFactory = $customerSetupFactory;
    }

    /**
     * @inheritDoc
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.1.0') < 0) {
            $this->addAddressCodeColumn($setup);
        }
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    private function addAddressCodeColumn(ModuleDataSetupInterface $setup)
    {
        $attributeCode = 'addresscode';
        $customerSetup = $this->_customerSetupFactory->create(['setup' => $setup]);
        if (!$customerSetup->getAttribute('customer_address', $attributeCode)) {
            $customerSetup->addAttribute('customer_address', $attributeCode, [
                'label' => 'Address Code',
                'input' => 'text',
                'type' => 'varchar',
                'source' => '',
                'backend' => '',
                'required' => false,
                'position' => 999,
                'visible' => false,
                'system' => false,
                'user_defined' => false,
                'is_user_defined' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => true
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', $attributeCode)
                ->addData(['used_in_forms' => ['customer_address_edit']]);
            $attribute->save();
        }

        $quoteTable = $setup->getTable('quote_address');
        if (!$this->getConnection($setup, 'checkout')->tableColumnExists($quoteTable, $attributeCode)) {
            $this->getConnection($setup, 'checkout')->addColumn(
                $quoteTable,
                $attributeCode,
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 50,
                    'nullable' => true,
                    'comment' => 'Address Code'
                ]
            );
        }

        $orderTable = $setup->getTable('sales_order_address');
        if (!$this->getConnection($setup, 'sales')->tableColumnExists($orderTable, $attributeCode)) {
            $this->getConnection($setup, 'sales')->addColumn(
                $orderTable,
                $attributeCode,
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 50,
                    'nullable' => true,
                    'comment' => 'Address Code'
                ]
            );
        }
    }

    /**
     * Retrieve Connection
     *
     * @param ModuleDataSetupInterface $setup
     * @param string $connectionName
     * @return AdapterInterface
     */
    private function getConnection(ModuleDataSetupInterface $setup, $connectionName): AdapterInterface
    {
        if ($setup instanceof Setup) {
            return $setup->getConnection($connectionName);
        }
        return $setup->getConnection();
    }
}
