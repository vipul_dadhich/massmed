<?php

namespace Ey\Checkout\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
    
        $installer = $setup;
        $installer->startSetup();

		
		// Add pi_firstname field to quote, sales_order tables        
        
        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'pi_firstname',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'First Name',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'pi_firstname',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'First Name',
            ]
        );

		// Add pi_lastname field to quote, sales_order tables        
        
        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'pi_lastname',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Last Name',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'pi_lastname',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Last Name',
            ]
        );
        
        // Add pi_suffix field to quote, sales_order tables        
        
        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'pi_suffix',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Suffix',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'pi_suffix',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Suffix',
            ]
        );
        
        // Add pi_role field to quote, sales_order tables        
        
        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'pi_role',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Role',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'pi_role',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Role',
            ]
        );
        
        // Add pi_organization field to quote, sales_order tables        
        
        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'pi_organization',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Name of Organization',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'pi_organization',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Name of Organization',
            ]
        );
        
        // Add ba_password field to quote, sales_order tables        
        
        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'ba_password',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Password',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'ba_password',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Password',
            ]
        );
        
        $setup->endSetup();
    }
}