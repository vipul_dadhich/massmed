<?php

namespace Ey\Checkout\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup,ModuleContextInterface $context){
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {

			// Add pi_primaryspecialty field to quote, sales_order tables

    		$installer->getConnection()->addColumn(
            	$installer->getTable('quote'),
            	'pi_primaryspecialty',
            	[
                	'type' => 'text',
                	'nullable' => true,
                	'comment' => 'Primary Specialty',
            	]
        	);

        	$installer->getConnection()->addColumn(
        	    $installer->getTable('sales_order'),
        	    'pi_primaryspecialty',
        	    [
        	        'type' => 'text',
        	        'nullable' => true,
        	        'comment' => 'Primary Specialty',
        	    ]
        	);

        	// Add pi_promocode field to quote, sales_order tables

    		$installer->getConnection()->addColumn(
            	$installer->getTable('quote'),
            	'pi_promocode',
            	[
                	'type' => 'text',
                	'nullable' => true,
                	'comment' => 'Promo Code',
            	]
        	);

        	$installer->getConnection()->addColumn(
        	    $installer->getTable('sales_order'),
        	    'pi_promocode',
        	    [
        	        'type' => 'text',
        	        'nullable' => true,
        	        'comment' => 'Promo Code',
        	    ]
        	);

        	// Add pi_doi field to quote, sales_order tables

    		$installer->getConnection()->addColumn(
            	$installer->getTable('quote'),
            	'pi_doi',
            	[
                	'type' => 'text',
                	'nullable' => true,
                	'comment' => 'DOI',
            	]
        	);

        	$installer->getConnection()->addColumn(
        	    $installer->getTable('sales_order'),
        	    'pi_doi',
        	    [
        	        'type' => 'text',
        	        'nullable' => true,
        	        'comment' => 'DOI',
        	    ]
        	);
        }
		if (version_compare($context->getVersion(), '1.0.2') < 0) {

			// Add catalyst_soi field to quote, sales_order tables

    		$installer->getConnection()->addColumn(
            	$installer->getTable('quote'),
            	'catalyst_soi',
            	[
                	'type' => 'text',
                	'nullable' => true,
                	'comment' => 'Catalyst SOI',
            	]
        	);

        	$installer->getConnection()->addColumn(
        	    $installer->getTable('sales_order'),
        	    'catalyst_soi',
        	    [
        	        'type' => 'text',
        	        'nullable' => true,
        	        'comment' => 'Catalyst SOI',
        	    ]
        	);

        	// Add catalyst_connect field to quote, sales_order tables

    		$installer->getConnection()->addColumn(
            	$installer->getTable('quote'),
            	'catalyst_connect',
            	[
                	'type' => 'text',
                	'nullable' => true,
                	'comment' => 'Catalyst Connect',
            	]
        	);

        	$installer->getConnection()->addColumn(
        	    $installer->getTable('sales_order'),
        	    'catalyst_connect',
        	    [
        	        'type' => 'text',
        	        'nullable' => true,
        	        'comment' => 'Catalyst Connect',
        	    ]
        	);


        }
        $setup->endSetup();
    }
}
