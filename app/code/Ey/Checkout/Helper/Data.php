<?php
namespace Ey\Checkout\Helper;

use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Class Data
 * @package Ey\Checkout\Helper
 */
class Data extends AbstractHelper{

    /**
     * Data constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Customer $customer
     * @param Config $eavConfig
     * @param Context $context
     */
    public function __construct(
    ScopeConfigInterface $scopeConfig,
    Customer $customer,
    Config $eavConfig,
    Context $context
    ){
        $this->scopeConfig = $scopeConfig;
        $this->customer = $customer;
        $this->eavConfig = $eavConfig;
        return parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getManageProfileURL()
    {
        return $this->scopeConfig->getValue('manageprofile/url/manageprofile_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed\
     */
    public function getEmailAlertsUrl()
    {
        return $this->scopeConfig->getValue('manageprofile/url/email_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $order
     * @return false|Customer
     */
    public function getOrderCustomer($order){
        $customerId = $order->getCustomerId();
        if ($customerId) {
            return $this->customer->load($customerId);
        }
        return false;
    }

    /**
     * @param $customer
     * @return false
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPrimarySpeciality($customer){
        if ($customer && $customer->getData('primaryspecialty')) {
            $primaryspecialty = $this->eavConfig->getAttribute('customer', 'primaryspecialty');
            return $primaryspecialty->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)
                ->getSource()
                ->getOptionText($customer->getData('primaryspecialty'));
        }
        return false;
    }
}
