<?php
namespace Ey\FreeGift\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\UrlInterface;
use Magento\Ui\Component\Form\Field;

class FreePdf extends AbstractModifier{
    const FIELD_CODE = 'free_pdf';
    protected $_helper;
    
    public function __construct(
        LocatorInterface $locator,
        UrlInterface $urlBuilder,
        \Ey\FreeGift\Helper\Data $helper,
        ArrayManager $arrayManager
    ){
        $this->locator = $locator;
        $this->urlBuilder = $urlBuilder;
        $this->arrayManager = $arrayManager;
        $this->_helper = $helper;
    }

    
    public function modifyMeta(array $meta){
        $meta = $this->customiseFreePdfField($meta);
        return $meta;
    }

    public function modifyData(array $data){
        $product = $this->locator->getProduct();
        $modelId = $product->getId();
        if (isset($data[$modelId]['product'][self::FIELD_CODE])){
            $freePdf = $data[$modelId]['product'][self::FIELD_CODE];
            unset($data[$modelId]['product'][self::FIELD_CODE]);
            $url = $this->_helper->getFreePdfUrl($freePdf);
            $data[$modelId]['product'][self::FIELD_CODE][0]['name'] = basename($freePdf);
            $data[$modelId]['product'][self::FIELD_CODE][0]['url'] = $url;
            $path = $this->_helper->getFreePdfPath($freePdf);
            if(is_file($path)){
                $fileSize = filesize($path);
            }else{
                $fileSize = 0;
            }
            $data[$modelId]['product'][self::FIELD_CODE][0]['size'] = $fileSize;
        }
        return $data;
    }

    protected function customiseFreePdfField(array $meta){
        $fieldCode = self::FIELD_CODE;
        $elementPath = $this->arrayManager->findPath($fieldCode, $meta, null, 'children');
        $containerPath = $this->arrayManager->findPath(static::CONTAINER_PREFIX . $fieldCode, $meta, null, 'children');

        if (!$elementPath) {
            return $meta;
        }

        $meta = $this->arrayManager->merge(
            $containerPath,
            $meta,
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label'         => __('Free PDF'),
                            'dataScope'     => '',
                            'breakLine'     => false,
                            'formElement'   => 'container',
                            'componentType' => 'container',
                            'component'     => 'Magento_Ui/js/form/components/group',
                            'scopeLabel'    => __('[GLOBAL]'),
                        ],
                    ],
                ],
                'children'  => [
                    $fieldCode => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'formElement' => 'fileUploader',
                                    'componentType' => 'fileUploader',
                                    'component' => 'Magento_Ui/js/form/element/file-uploader',
                                    'elementTmpl' => 'Ey_FreeGift/components/file-uploader',
                                    "previewTmpl" => 'Ey_FreeGift/components/preview',
                                    'fileInputName' => self::FIELD_CODE,
                                    'uploaderConfig' => [
                                        'url' => $this->urlBuilder->addSessionParam()->getUrl(
                                            'freegift/product_file/upload',
                                            ['type' => self::FIELD_CODE, '_secure' => true]
                                        ),
                                    ],
                                    'dataScope' => self::FIELD_CODE,
                                ],
                            ],
                        ],
                    ]
                ]
            ]
        );

        return $meta;
    }

    protected function getOptions(){
    }
}