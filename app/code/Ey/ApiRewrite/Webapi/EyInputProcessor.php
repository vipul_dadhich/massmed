<?php
namespace Ey\ApiRewrite\Webapi;

use Magento\Framework\Api\AttributeValue;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\SimpleDataObjectConverter;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\SerializationException;
use Magento\Framework\ObjectManager\ConfigInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Phrase;
use Magento\Framework\Reflection\MethodsMap;
use Magento\Framework\Reflection\TypeProcessor;
use Magento\Framework\Webapi\Exception as WebapiException;
use Magento\Framework\Webapi\CustomAttribute\PreprocessorInterface;
use Zend\Code\Reflection\ClassReflection;
use Magento\Framework\Webapi\CustomAttributeTypeLocatorInterface;
use Magento\Framework\Webapi\ServiceTypeToEntityTypeMap;

class EyInputProcessor extends \Magento\Framework\Webapi\ServiceInputProcessor implements \Magento\Framework\Webapi\ServicePayloadConverterInterface
{
	

    /**
     * @var array
     */
    private $serviceTypeToEntityTypeMap;

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var PreprocessorInterface[]
     */
    private $customAttributePreprocessors;

    /**
     * @var array
     */
    private $attributesPreprocessorsMap = [];


	public function __construct(
		\Magento\Eav\Model\Entity\Attribute $attribute,
        TypeProcessor $typeProcessor,
        ObjectManagerInterface $objectManager,
        AttributeValueFactory $attributeValueFactory,
        CustomAttributeTypeLocatorInterface $customAttributeTypeLocator,
        MethodsMap $methodsMap,
        ServiceTypeToEntityTypeMap $serviceTypeToEntityTypeMap = null,
        ConfigInterface $config = null,
        array $customAttributePreprocessors = []
    ) {
    	$this->attribute = $attribute;
    	$this->typeProcessor = $typeProcessor;
        $this->objectManager = $objectManager;
        $this->attributeValueFactory = $attributeValueFactory;
        $this->customAttributeTypeLocator = $customAttributeTypeLocator;
        $this->methodsMap = $methodsMap;
        $this->serviceTypeToEntityTypeMap = $serviceTypeToEntityTypeMap
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(ServiceTypeToEntityTypeMap::class);
        $this->config = $config
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(ConfigInterface::class);
        $this->customAttributePreprocessors = $customAttributePreprocessors;
        parent::__construct($typeProcessor,$objectManager,$attributeValueFactory,$customAttributeTypeLocator,$methodsMap,$serviceTypeToEntityTypeMap,$config,$customAttributePreprocessors);
    }

    protected function convertCustomAttributeValue($customAttributesValueArray, $dataObjectClassName)
    {
        $result = [];
        $dataObjectClassName = ltrim($dataObjectClassName, '\\');
        foreach ($customAttributesValueArray as $key => $customAttribute) {
            $this->runCustomAttributePreprocessors($key, $customAttribute);
            if (!is_array($customAttribute)) {
                $customAttribute = [AttributeValue::ATTRIBUTE_CODE => $key, AttributeValue::VALUE => $customAttribute];
            }
            list($customAttributeCode, $customAttributeValue) = $this->processCustomAttribute($customAttribute);
            $entityType = $this->serviceTypeToEntityTypeMap->getEntityType($dataObjectClassName);
            if ($entityType) {
                $type = $this->customAttributeTypeLocator->getType(
                    $customAttributeCode,
                    $entityType
                );
            } else {
                $type = TypeProcessor::ANY_TYPE;
            }

            if ($this->typeProcessor->isTypeAny($type) || $this->typeProcessor->isTypeSimple($type)
                || !is_array($customAttributeValue)
            ) {
                try {
                	if($type == "int" && is_string($customAttributeValue)){
                		
                		
                		$attr = $this->attribute->loadByCode($entityType, $customAttributeCode);
                		$options    = $attr->setStoreId(0)->getSource()->getAllOptions();
                		$attribute_option_id = null;
                		foreach ($options as $val) {
       						if ($val['label'] === $customAttributeValue) {
       							$attribute_option_id = $val['value'];
       							break;
           					}
       					}
                		if($attribute_option_id){
                			$customAttributeValue = $attribute_option_id;
                		}else{
                			throw new SerializationException(
                        		new Phrase('Attribute '.$customAttributeCode.' does not have value '.$customAttributeValue)
                    		);
                		}
                	}
                    $attributeValue = $this->convertValue($customAttributeValue, $type);
                } catch (SerializationException $e) {
                    throw new SerializationException(
                        new Phrase(
                            'Attribute "%attribute_code" has invalid value. %details',
                            ['attribute_code' => $customAttributeCode, 'details' => $e->getMessage()]
                        )
                    );
                }
            } else {
                $attributeValue = $this->_createDataObjectForTypeAndArrayValue($type, $customAttributeValue);
            }
            $result[$customAttributeCode] = $this->attributeValueFactory->create()
                ->setAttributeCode($customAttributeCode)
                ->setValue($attributeValue);
        }

        return $result;
    }
    
    /**
     * Prepare attribute value by loaded attribute preprocessors
     *
     * @param mixed $key
     * @param mixed $customAttribute
     */
    private function runCustomAttributePreprocessors($key, &$customAttribute)
    {
        $preprocessorsMap = $this->getAttributesPreprocessorsMap();
        if ($key && is_array($customAttribute) && array_key_exists($key, $preprocessorsMap)) {
            $preprocessorsList = $preprocessorsMap[$key];
            foreach ($preprocessorsList as $attributePreprocessor) {
                if ($attributePreprocessor->shouldBeProcessed($key, $customAttribute)) {
                    $attributePreprocessor->process($key, $customAttribute);
                }
            }
        }
    }
    
    /**
     * Get map of preprocessors related to the custom attributes
     *
     * @return array
     */
    private function getAttributesPreprocessorsMap(): array
    {
        if (!$this->attributesPreprocessorsMap) {
            foreach ($this->customAttributePreprocessors as $attributePreprocessor) {
                foreach ($attributePreprocessor->getAffectedAttributes() as $attributeKey) {
                    $this->attributesPreprocessorsMap[$attributeKey][] = $attributePreprocessor;
                }
            }
        }

        return $this->attributesPreprocessorsMap;
    }
    
    /**
     * Derive the custom attribute code and value.
     *
     * @param string[] $customAttribute
     * @return string[]
     * @throws SerializationException
     */
    private function processCustomAttribute($customAttribute)
    {
        $camelCaseAttributeCodeKey = lcfirst(
            SimpleDataObjectConverter::snakeCaseToUpperCamelCase(AttributeValue::ATTRIBUTE_CODE)
        );
        // attribute code key could be snake or camel case, depending on whether SOAP or REST is used.
        if (isset($customAttribute[AttributeValue::ATTRIBUTE_CODE])) {
            $customAttributeCode = $customAttribute[AttributeValue::ATTRIBUTE_CODE];
        } elseif (isset($customAttribute[$camelCaseAttributeCodeKey])) {
            $customAttributeCode = $customAttribute[$camelCaseAttributeCodeKey];
        } else {
            $customAttributeCode = null;
        }

        if (!$customAttributeCode && !isset($customAttribute[AttributeValue::VALUE])) {
            throw new SerializationException(
                new Phrase('An empty custom attribute is specified. Enter the attribute and try again.')
            );
        } elseif (!$customAttributeCode) {
            throw new SerializationException(
                new Phrase(
                    'A custom attribute is specified with a missing attribute code. Verify the code and try again.'
                )
            );
        } elseif (!array_key_exists(AttributeValue::VALUE, $customAttribute)) {
            throw new SerializationException(
                new Phrase(
                    'The "' . $customAttributeCode .
                    '" attribute code doesn\'t have a value set. Enter the value and try again.'
                )
            );
        }

        return [$customAttributeCode, $customAttribute[AttributeValue::VALUE]];
    }

}
