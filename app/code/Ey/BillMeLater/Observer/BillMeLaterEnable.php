<?php

namespace Ey\BillMeLater\Observer;

class BillMeLaterEnable implements \Magento\Framework\Event\ObserverInterface{
	
	public function __construct(
	\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
	\Magento\Checkout\Model\Cart $cart,
	\Magento\Catalog\Api\ProductRepositoryInterface $product
	) {
        $this->scopeConfig = $scopeConfig;
        $this->cart = $cart;
        $this->product = $product;
    }
	public function execute(\Magento\Framework\Event\Observer $observer){
        $result = $observer->getEvent()->getResult();
	    $method_instance = $observer->getEvent()->getMethodInstance()->getCode();
		$payment_method = $this->scopeConfig->getValue('billmelater/conditions/payment_method',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $condition_type = $this->scopeConfig->getValue('billmelater/conditions/condition_type',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $product_attribute = $this->scopeConfig->getValue('billmelater/conditions/product_attribute',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    	if($method_instance == $payment_method) {
    		$bill_me_later = $this->cart->getQuote()->getData('bill_me_later');
    		if(isset($bill_me_later) && $bill_me_later!=""){
    			$result->setData('is_available', true);
    		}
    		else{
    			if($condition_type == "url_parameter"){
    				$result->setData('is_available', false);
    			}
    			else if($condition_type == "product_attribute"){
    				$allItems = $this->cart->getQuote()->getAllItems();
    				$flag = 0;
    				foreach($allItems as $item){
    					$product = $this->product->get($item->getSku());
    					$custom_attribute_value = $product->getResource()->getAttribute($product_attribute)->getFrontend()->getValue($product); 
    					if($custom_attribute_value == "Yes"){
    						$flag ++;
    					}
    				}
    				
    				if($flag == count($allItems)){
    					$result->setData('is_available', true);
    				}else{
    					$result->setData('is_available', false);
    				}
    			}else{
    				$result->setData('is_available', false);
    			}
    		}
    	}
	}
}