<?php
namespace Ey\BillMeLater\Model\Config\Source;

class AttributesList implements \Magento\Framework\Option\ArrayInterface{
	public function __construct(\Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory) {
        $this->_attributeFactory = $attributeFactory;
    }
    public function toOptionArray(){
  		$attributeInfo = $this->_attributeFactory->getCollection();
  		$pa = array();
  		$pa_empty = array('value' => '', 'label' => 'Select Product Attribute');
		array_push($pa,$pa_empty);
  		foreach($attributeInfo as $attribute){
  			if($attribute->getFrontendInput() == "boolean"){
  				$pa_temp = array('value' => $attribute->getAttributeCode(), 'label' => $attribute->getFrontendLabel());
  				array_push($pa,$pa_temp);
  			}
    	}
  		return $pa;
	}
}