<?php
namespace Ey\BillMeLater\Model\Config\Source;

class PaymentMethodList implements \Magento\Framework\Option\ArrayInterface{
	public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scope) {
        $this->scope = $scope;
    }
	public function toOptionArray(){
		$apm = array();
		$apm_empty = array('value' => '', 'label' => 'Select Payment Method');
		array_push($apm,$apm_empty);
		$methodList = $this->scope->getValue('payment');
		foreach( $methodList as $code => $_method ){
			if(isset($_method['title'])){
	    		$apm_temp = array('value' => $code, 'label' => $_method['title']);
				array_push($apm,$apm_temp);
			}
		}
  		return $apm;
 	}
}