<?php
namespace Ey\BillMeLater\Model\Config\Source;

class ConditionType implements \Magento\Framework\Option\ArrayInterface{
	public function toOptionArray(){
	  	return [
    		['value' => '', 'label' => 'Select Condition Type'],
    		['value' => 'product_attribute', 'label' => 'Product Attribute'],
    		['value' => 'url_parameter', 'label' => 'URL Parameter']
  		];
	}
}