<?php

namespace Ey\MuleSoft\Model\Catalyst\Api;

use Ey\MuleSoft\Model\Core\Api\GetResponse;
use Ey\MuleSoft\Model\Core\Api\SetRequest;
/**
 * Class setRequest
 */
class GetCustomerData
{
	/**
     * API request endpoint
     */
    const API_REQUEST_ENDPOINT = 'v1/api/customers';

    /**
     * API request method
     */
    const API_REQUEST_METHOD = 'GET';

	/**
     * getCustomer constructor
     *
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     * @param \Ey\MuleSoft\Helper\Endpoints $endpoints
     */
    public function __construct(
        GetResponse $response,
        SetRequest $request,
        \Ey\MuleSoft\Helper\Endpoints $endpoints
    ) {
        $this->response = $response;
        $this->request = $request;
        $this->endpoints = $endpoints;
    }

    /**
     * Fetch some data from API
     * @param $params array() or string
     */
    public function execute($ucid)
    {
        $requestParams = $this->prepareRequest($ucid);
        $endpoint = $this->getCustomerEndpoint().'/'.$ucid;
        $response = $this->request->makeRequest( $endpoint, self::API_REQUEST_METHOD, $requestParams, '', true);
        return $response;
    }
    /**
     * Get Customer API Endpoint
     * @return string
     */
    public function getCustomerEndpoint(){
        if($this->endpoints->getCustomerEndpoint()){
            return $this->endpoints->getCustomerEndpoint();
        }
        return self::API_REQUEST_ENDPOINT;
    }
     /**
     * Prepare the request for an API
     */
    private function prepareRequest($ucid){
        return array("ucid" => $ucid);
    }

    /***
        {
          "Customer": {
            "ucid": "46080fc2-2f90-46aa-b1d0-80b69bfe8b61",
            "firstName": "Vipul",
            "lastName": "Dadhich",
            "email": "vdadhich@mms.org",
            "role": "MGR",
            "suffix": "MBBS",
            "primarySpecialty": "Allergy",
            "nameOfOrganization": "MMS",
            "catalystConnect": true,
            "catalystSOI": true,
            "country": "AF",
            "insightsCouncilMember": null,
            "audienceType": "REGISTERED USER"
          }
    */
    private function prepareResponse($response){
        return $responseData= json_decode($response->getBody() , true);
    }

    private function ApiErrorMessages(){
        $error = "Customer doesn't exists";
        //Enhance the Error API error messages.
        //Use the Response setting classes for it.
    }
}
