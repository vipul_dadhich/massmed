<?php

namespace Ey\MuleSoft\Model\Catalyst\Api;

use Ey\MuleSoft\Model\Core\Api\GetResponse;
use Ey\MuleSoft\Model\Core\Api\SetRequest;
/**
 * Class CreateCustomer
 */
class CreateCustomer
{
	/**
     * API request endpoint
     */
    const API_REQUEST_ENDPOINT = 'v1/api/customers';

    /**
     * API request method
     */
    const API_REQUEST_METHOD = 'POST';

	/**
     * getCustomer constructor
     *
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     * @param \Ey\MuleSoft\Helper\Endpoints $endpoints
     */
    public function __construct(
        GetResponse $response,
        SetRequest $request,
        \Ey\MuleSoft\Helper\Endpoints $endpoints
    ) {
        $this->response = $response;
        $this->request = $request;
        $this->endpoints = $endpoints;
    }

    /**
     * Fetch some data from API
     * @param $params array() or string
     */
    public function execute($requestParams)
    {
        return $this->request->makeRequest( $this->getCustomerEndpoint(), static::API_REQUEST_METHOD, $requestParams);
    }
    /**
     * Get Customer API Endpoint
     * @return string
     */
    public function getCustomerEndpoint(){
        if($this->endpoints->getCustomerPostEndpoint()){
            return $this->endpoints->getCustomerPostEndpoint();
        }
        return self::API_REQUEST_ENDPOINT;
    }

    /***
        {
          "Customer": {
            "ucid": "46080fc2-2f90-46aa-b1d0-80b69bfe8b61",
            "firstName": "Vipul",
            "lastName": "Dadhich",
            "email": "vdadhich@mms.org",
            "role": "MGR",
            "suffix": "MBBS",
            "primarySpecialty": "Allergy",
            "nameOfOrganization": "MMS",
            "catalystConnect": true,
            "catalystSOI": true,
            "country": "AF",
            "insightsCouncilMember": null,
            "audienceType": "REGISTERED USER"
          }
    **/
    private function prepareResponse($response){
        return json_decode($response->getBody() , true);
    }
}
