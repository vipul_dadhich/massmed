<?php

namespace Ey\MuleSoft\Model\Core\Api;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ResponseFactory;

/**
 * Class setRequest
 */
class GetResponse
{
	/**
     * getResponse constructor
     *
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     */
    public function __construct(
        ResponseFactory $responseFactory
    ) {
        $this->responseFactory = $responseFactory;
    }
    /**
     * Fetch some data from API
     * Incomplete methods
     */
    public function execute()
    {
        $status = $response->getStatusCode(); // 200 status code
        $responseBody = $response->getBody();
        $responseContent = $responseBody->getContents(); // here you will have the API response in JSON format
        // Add your logic using $responseContent
    }

    
}