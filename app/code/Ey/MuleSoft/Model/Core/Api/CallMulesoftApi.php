<?php

namespace Ey\MuleSoft\Model\Core\Api;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Psr7\ResponseFactory;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Framework\Webapi\Rest\Request;
use Ey\MuleSoft\Helper\Data;

class CallMulesoftApi
{
    /**
     * SetRequest constructor
     *
     * @param ClientFactory $clientFactory
     */
    public function __construct(
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory,
        Data $helper,
        \Ey\MuleSoft\Model\Core\Logger\Monolog $mulesoftLogger
    ) {
        $this->clientFactory = $clientFactory;
        $this->responseFactory = $responseFactory;
        $this->helper = $helper;
        $this->mulesoftLogger = $mulesoftLogger;
    }

    /**
     * Do API request with provided params
     *
     * @param string $apiMethod
     * @param string $requestMethod
     * @param array $params
     *
     * @return Response
     */
    public function makeRequest($baseUrl, $endpoint, $requestMethod, $payload = [], $headers = []){
        return $this->doRequest($baseUrl, $endpoint, $requestMethod, $payload, $headers);
    }
    /**
     * Do API request with provided params
     *
     * @param string $uriEndpoint
     * @param array $params
     * @param string $requestMethod
     *
     * @return Response
     */
    private function doRequest(string $baseUrl,
                               string $uriEndpoint,
                               string $requestMethod = Request::HTTP_METHOD_GET,
                               array $payload = [],
                               array $headers = []
    ) {
        /** @var Client $client */
        $headers = $this->prepareHeader($headers);
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => $baseUrl,
            'headers' => $headers
        ]]);
        if($this->helper->getMuleSoftDebug()){
            $this->mulesoftLogger->mulesoftLog("headers-> ");
            $this->mulesoftLogger->mulesoftLog(json_encode($headers, JSON_PRETTY_PRINT));
            $this->mulesoftLogger->mulesoftLog("Base URL-> ". $baseUrl);
            $this->mulesoftLogger->mulesoftLog("Endpoint-> ". $uriEndpoint);
        }
        try {
            if($requestMethod == 'POST'){
                $payload = array(
                    'json' => $payload
                );
                $response = $client->request(
                    $requestMethod,
                    $uriEndpoint,
                    $payload
                );
            } else {
                $response = $client->request(
                    $requestMethod,
                    $uriEndpoint
                );
            }
        } catch (GuzzleException $exception) {
            /** @var Response $response */
            $response = $this->responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);
            $this->mulesoftLogger->log(print_r($response, true));
        }
        return $response;
    }

    /**
     * prepare header for request
     **/

    private function prepareHeader($headers){
        $headers['client_id'] = $this->helper->getMuleSoftClientId();
        $headers['client_secret'] = $this->helper->getMuleSoftClientSecret();
        return $headers;
    }

}
