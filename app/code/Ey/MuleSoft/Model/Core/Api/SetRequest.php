<?php

namespace Ey\MuleSoft\Model\Core\Api;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Psr7\ResponseFactory;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Framework\Webapi\Rest\Request;
use Ey\MuleSoft\Helper\Data;
use MMS\Paybill\Helper\Data as paybillHelper;
use MMS\Logger\LoggerInterface;

Class SetRequest{


    /**
     * API request URL
     */
    const API_REQUEST_URI = '';
    /**
     * SetRequest constructor
     *
     * @param ClientFactory $clientFactory
     */
    
     /**
     * @var LoggerInterface
     */
    protected $_logger;

    public function __construct(
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory,
        Data $helper,
        paybillHelper $paybillHelper,
        LoggerInterface $logger
    ) {
        $this->clientFactory = $clientFactory;
        $this->responseFactory = $responseFactory;
        $this->helper = $helper;
        $this->paybillHelper = $paybillHelper;
        $this->isDebugMode = $this->helper->getMuleSoftDebug();
        $this->_logger = $logger;
    }
    /**
     * Api end point
     **/
    private function getMulesoftApiURI(){
        return $this->helper->getMuleSoftCustomerURI();
    }
    /**
     * Api client ID
     **/
    private function getMulesoftClientId(){
        return $this->helper->getMuleSoftClientId();
    }
    /**
     * Api secret
     **/
    private function getMulesoftSecret(){
        return $this->helper->getMuleSoftClientSecret();
    }
    /**
     * Api end point for Paybill
     **/
    private function getBillApiUri(){
        return $this->paybillHelper->getBillApiUrl();
    }
    /**
     * Api Auth token
     **/
    private function getAkamaiAuthToken(){
        return $this->paybillHelper->getAkamaiAuthToken();
    }
    /**
     * Do API request with provided params
     *
     * @param string $apiMethod
     * @param string $requestMethod
     * @param array $params
     *
     * @return Response
     */
    public function makeRequest($apiMethod, $requestMethod, $params = [], $isPaybill = false, $isCustomer = false){
        return $this->doRequest($apiMethod, $params, $requestMethod, $isPaybill, $isCustomer);
    }
    /**
     * Do API request with provided params
     *
     * @param string $uriEndpoint
     * @param array $params
     * @param string $requestMethod
     *
     * @return Response
     */
    private function doRequest(
        string $uriEndpoint,
        array $params = [],
        string $requestMethod = Request::HTTP_METHOD_GET,
        $isPaybill,
        $isCustomer
    ) {
        /** @var Client $client */
        if($isPaybill){
            $client =$this->getPaybillClient();
        }else if($isCustomer){
            $client =$this->getCustomerClientWithToken();
        }
        else{
            $client =$this->getCustomerClient();
        }
        try {
            if($requestMethod == 'POST'){
                $params = array(
                    'json' => $params
                );
                $response = $client->request(
                    $requestMethod,
                    $uriEndpoint,
                    $params
                );
            }else{

                $response = $client->request(
                    $requestMethod,
                    $uriEndpoint
                );
            }
            
                $this->_logger->debug("RESPONSE CODE: ". $response->getStatusCode());
            
            
                $this->_logger->debug('Customer Validation API Start');
                $this->_logger->debug('URI End Point->');
                $this->_logger->debug($uriEndpoint);
                $this->_logger->debug('Client ID->');
                $this->_logger->debug($this->getMulesoftClientId());
                $this->_logger->debug('Client Secert->');
                $this->_logger->debug($this->getMulesoftSecret());
                $this->_logger->debug('API Response->');
                $this->_logger->debug(print_r($response,true));
            


        } catch (GuzzleException $exception) {
            /** @var Response $response */
            $response = $this->responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);

            
                $this->_logger->debug('Customer Validation API Exception');
                $this->_logger->debug('URI End Point->');
                $this->_logger->debug($uriEndpoint);
                $this->_logger->debug('Client ID->');
                $this->_logger->debug($this->getMulesoftClientId());
                $this->_logger->debug('Client Secert->');
                $this->_logger->debug($this->getMulesoftSecret());
                $this->_logger->debug('API Response->');
                $this->_logger->debug(print_r($response,true));
            
        }

        return $response;
    }
    /**
     * get customer client
     **/
    private function getCustomerClient(){
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => $this->getMulesoftApiURI(),
            'headers' => ['client_id' => $this->getMulesoftClientId(), 'client_secret' => $this->getMulesoftSecret()]
        ]]);
        
            $requestHeader = [
                'base_uri' => $this->getMulesoftApiURI(),
                'headers' => ['client_id' => $this->getMulesoftClientId(), 'client_secret' => $this->getMulesoftSecret()]
            ];
            $this->_logger->debug('GET/POST CUSTOMER API HEADER:'. json_encode($requestHeader, JSON_PRETTY_PRINT) );
        
        return $client;
    }
    /**
     * get customer client with auth token
     **/
    private function getCustomerClientWithToken(){
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => $this->getMulesoftApiURI(),
            'headers' => ['client_id' => $this->getMulesoftClientId(), 'client_secret' => $this->getMulesoftSecret(), 'akamai-auth-token' => $this->getAkamaiAuthToken()]
        ]]);
        
            $requestHeader = [
                'base_uri' => $this->getMulesoftApiURI(),
                'headers' => ['client_id' => $this->getMulesoftClientId(), 'client_secret' => $this->getMulesoftSecret(), 'akamai-auth-token' => $this->getAkamaiAuthToken()]
            ];
            $this->_logger->debug('Get CUSTOMER API HEADER:'. json_encode($requestHeader, JSON_PRETTY_PRINT) );
        
        return $client;
    }
    /**
     * get paybill client
     **/
    private function getPaybillClient(){
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => $this->getBillApiUri(),
            'headers' => ['akamai-auth-token' => $this->getAkamaiAuthToken()]
        ]]);
        
            $requestHeader = [
                'base_uri' => $this->getBillApiUri(),
                'headers' => ['akamai-auth-token' => $this->getAkamaiAuthToken()]
            ];
            $this->_logger->debug('Get Bill API HEADER:'. json_encode($requestHeader, JSON_PRETTY_PRINT) );
        
        return $client;
    }
}

