<?php

namespace Ey\MuleSoft\Model\Core\Logger;

use \Psr\Log\LoggerInterface;
/**
 * Class setRequest
 */
class Monolog
{
    const LOG_FILE = '/var/log/mulesoft-API.log';
    const MULESOFT_LOG_FILE = '/var/log/MMS-Magento.log';
    /**
     * getCustomer constructor
     *
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        LoggerInterface $loggerInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {

        $this->logger = $loggerInterface;
        $this->scopeConfig = $scopeConfig;
    }
    /**
     * Write the log in to the log file
     */
    public function log($inputStream, $logfilename = false)
    {
        $logfilename = $logfilename ? $logfilename : self::LOG_FILE;
        $writer = new \Zend\Log\Writer\Stream(BP . $logfilename  );
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(($inputStream));
    }
    /**
     * Write the log in to the log file
     */
    public function mulesoftLog($inputStream, $logfilename = false)
    {
        $logfilename = $logfilename ? $logfilename : self::MULESOFT_LOG_FILE;
        $writer = new \Zend\Log\Writer\Stream(BP . $logfilename  );
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        if ($this->isLoggerInDebugMode()) {
            $logger->debug(($inputStream));
        } else {
            $logger->info(($inputStream));
        }

    }
    public function isLoggerInDebugMode()
    {
        return ($this->scopeConfig->getValue('mulesoft/api/debug_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 2)?true:false;
    }
}
