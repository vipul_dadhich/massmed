<?php

namespace Ey\MuleSoft\Model\Paybill\Api;

use Ey\MuleSoft\Model\Core\Api\CallMulesoftApi;
use Ey\MuleSoft\Helper\Data;

class AbstractOrderPayment implements \Ey\MuleSoft\Api\Paybill\HttpClientInterface
{
    const PAYMENT_API_REQUEST_TYPE = 'POST';
    const PAYMENT_API_BASE_URL = 'https://api.nejm-qa.org/store/';

    /**
     * @var \Ey\MuleSoft\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    public function __construct(
        CallMulesoftApi $mulesoftApi,
        Data $helper,
        \Magento\Customer\Model\Session $customerSession,
        \Ey\MuleSoft\Helper\Endpoints $endpoints

    ) {
        $this->_mulesoftApi = $mulesoftApi;
        $this->_helper = $helper;
        $this->_customerSession = $customerSession;
        $this->endpoints = $endpoints;
    }

    public function getApiBaseUrl(){
        return $this->endpoints->getCustomerEndpoint();
    }

    public function getRequestMethod(){
        return self::PAYMENT_API_REQUEST_TYPE;
    }

    public function request($payload, $endpoint, $baseApiUrl = '', $headers = array())
    {
        $errorMessages = false;
        try {
            if(!$baseApiUrl){
                $baseApiUrl = $this->getApiBaseUrl();
            }
            $requestType = $this->getRequestMethod();
            $response = $this->_mulesoftApi->makeRequest($baseApiUrl, $endpoint, $requestType, $payload, $headers);
            return $response;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $errorMessages = $e->getMessage();
        } catch (\Exception $ex) {
            $errorMessages = $ex->getMessage();
        }

        if ($errorMessages) {
            throw new \Magento\Framework\Exception\LocalizedException(__($errorMessages));
        }
    }
}
