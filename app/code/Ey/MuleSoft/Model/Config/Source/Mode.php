<?php
namespace Ey\MuleSoft\Model\Config\Source;

class Mode implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 1, 'label' => __('Info')],
            ['value' => 2, 'label' => __('Debug')],
        ];
    }
}

