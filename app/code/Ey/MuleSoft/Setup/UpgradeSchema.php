<?php

namespace EY\MuleSoft\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;


class UpgradeSchema implements  UpgradeSchemaInterface
{
  public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
{
    $setup->startSetup();

      $setup->getConnection()->addColumn(
        $setup->getTable("sales_order"),
        'mulesoft_response',
        [
          "type" => Table::TYPE_TEXT,
          "nullable" => true,
          "comment" => "MuleSoft Response"
        ]
      );
      $setup->getConnection()->addColumn(
        $setup->getTable("sales_order_grid"),
        'mulesoft_response',
        [
          "type" => Table::TYPE_TEXT,
          "nullable" => true,
          "comment" => "MuleSoft Response"
        ]
      );

    $setup->endSetup();
}
}