<?php

namespace Ey\MuleSoft\Observer;

use Magento\Framework\App\Area;
use MMS\Paybill\Helper\Data as PaybillHelper;
use MMS\Logger\LoggerInterface;

class OrderCreated implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $_cookieManager;
    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;

    protected $state;
    protected $flowId;

    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * OrderCreated constructor.
     * @param \Ey\MuleSoft\Helper\Data $helper
     * @param \Zend\Http\Client $zendClient
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Customer\Model\Session $session
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
     * @param \Magento\Framework\App\State $state
     * @param PaybillHelper $paybillHelper
     * @param LoggerInterface $logger
     */

    public function __construct(
        \Ey\MuleSoft\Helper\Data $helper,
        \Zend\Http\Client $zendClient,
        \Magento\Sales\Model\Order $order,
        \Magento\Customer\Model\Session $session,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\App\State $state,
        PaybillHelper $paybillHelper,
        LoggerInterface $logger
    ){
        $this->helper = $helper;
        $this->paybillHelper = $paybillHelper;
        $this->zendClient = $zendClient;
        $this->order = $order;
        $this->session = $session;
        $this->_cookieManager = $cookieManager;
        $this->isDebugMode = $this->helper->getMuleSoftDebug();
        $this->isDebugModeType = $this->helper->getMuleSoftDebugMode();
        $this->_state = $state;
        $this->flowId = "Subscriber-Checkout";
        $this->_logger = $logger;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */

    public function execute(\Magento\Framework\Event\Observer $observer){
        $order = $observer->getEvent()->getOrder();
        if($order->getIsPaybill()){
            $this->paybillHelper->sendPayment($order);
            return;
        }
        if($this->_state->getAreaCode() == 'adminhtml'){
            $orderData = $this->helper->getOrderDataForAdmin($order,$this->session);
        }else{
            $orderData = $this->helper->getOrderData($order,$this->session);
        }

        if(isset($orderData['order']['isRenewal'])){
            $orderData = $this->helper->getOrderData($order,$this->session);
            $this->flowId= "Renew";
        }
        $this->sendOrder($orderData,$order);
        $this->_checkoutSession->unsetData('billing_address_code');
        $this->_checkoutSession->unsetData('mailing_address_code');
        $this->_cookieManager->deleteCookie('pi_information');
        return $this;
    }

    /**
     * @param $orderData
     * @param $order
     */
    public function sendOrder($orderData,$order)
    {

            $this->_logger->debug("Flow Id ->" . $this->flowId . " ****************** Payment API Log Start *******************");

        try {
            $mulesoftURI = $this->helper->getMuleSoftURI();
            $mulesoftOrderEndpoint = $this->helper->getMuleSoftOrderEndpoint($orderData);
            $apiUrl = $mulesoftURI.$mulesoftOrderEndpoint;
            $this->zendClient->reset();
            $this->zendClient->setUri($apiUrl);
            $this->zendClient->setMethod(\Zend\Http\Request::METHOD_POST);
            $this->zendClient->setOptions(['timeout' => 60]);
            $clientId = $this->helper->getMuleSoftClientId();
            $clientSecret = $this->helper->getMuleSoftClientSecret();
            $headers = $this->getApiHeader($clientId, $clientSecret);
            $this->zendClient->setHeaders($headers);
            $this->zendClient->setRawBody(json_encode($orderData, JSON_PRETTY_PRINT));

                $this->_logger->debug("HEADER FOR ->" . $this->flowId);
                $this->_logger->debug(json_encode($headers, JSON_PRETTY_PRINT));
                $this->_logger->debug("API URI: ".$apiUrl);
                if ($this->isDebugModeType) {
                    $this->_logger->debug("Work FlowId ->". $this->flowId . " POST PAYLOAD -> " . json_encode($orderData, JSON_PRETTY_PRINT));
                }

            $this->zendClient->send();
            $response = $this->zendClient->getResponse();

                $this->_logger->debug("Payment API Response Code: ". $response->getStatusCode() );
                if ($this->isDebugModeType) {
                    $this->_logger->debug("Work FlowId ->". $this->flowId . " RESPONSE BODY -> " . $response->getBody());
                    $this->_logger->debug("Work FlowId ->". $this->flowId . " RESPONSE -> " . print_r($response, true));
                }

            $responseData = json_decode($response->getBody() , true);
            $order->setMulesoftResponse($response->getStatusCode())->save();
            if ($response->getStatusCode() == 201 ) {
                $this->orderResponse($orderData);
            } else {

                    $this->_logger->debug("!!~~~~URGHH~~~!!! ERROR RESPONSE CODE:-". $response->getStatusCode());


                if ($this->_state->getAreaCode() !== Area::AREA_ADMINHTML) {
                    $this->session->setWebApiCustomError(true);
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __('External service failed, redirecting to fallback page.')
                    );
                    } else {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __('Order API fail with status code: '.$response->getStatusCode())
                    );
                }
            }
        } catch (\Zend\Http\Exception\RuntimeException $runtimeException) {
            $this->_logger->debug("Exception MULESOFT ". $runtimeException->getMessage());
            if ($this->_state->getAreaCode() !== Area::AREA_ADMINHTML) {
                $this->session->setWebApiCustomError(true);
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('External service failed, redirecting to fallback page.')
                );
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Order API fail with exception: '.$runtimeException->getMessage())
                );
            }
        }

        if ($this->flowId == "Renew") {
            $this->_logger->debug("****************** RENEW API Log ENDS *******************");
        } else {
            $this->_logger->debug("****************** Subscriber Regular CHECKOUT API Log ENDS *******************");
        }
    }

    /**
     * @param $clientId
     * @param $clientSecret
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getApiHeader($clientId, $clientSecret){
        if ($this->_state->getAreaCode() != 'adminhtml') {
            $header = [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'akamai-auth-token' => $this->getAkamaiAuthToken()
            ];
        } else {
            $header = [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'client_id' => $clientId,
                'client_secret' => $clientSecret
            ];
        }
        return $header;
    }

    /**
     * @return mixed
     */
    public function getAkamaiAuthToken()
    {
        if($this->session->getAkamaiAccessToken()){
            return $this->session->getAkamaiAccessToken();
        }
        return $this->session->getAccessToken();
    }
    /**
     * @param $ucid
     * @param $accessToken
     * @param $orderData
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function orderResponse($orderData){
      $customerEmail = $orderData['customer']['email'];
      $customer = $this->helper->customerExists($customerEmail,1);
      $this->_logger->debug("CustomerID");
      $this->_logger->debug($customer->getId());
      if($customer){
            $this->helper->updateCustomerGroupAfterSubscribe($customer);
        }

            $this->_logger->debug("Flow Id". $this->flowId . " ****************** PAYMENT LOG CALL ENDS *******************");

    }
}
