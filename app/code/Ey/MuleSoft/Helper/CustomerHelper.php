<?php

namespace Ey\MuleSoft\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class CustomerHelper
 * @package Ey\MuleSoft\Helper
 */
class CustomerHelper extends AbstractHelper
{
    protected $apiDataKeys = [
        'firstName' => 'firstname',
        'lastName' => 'lastname',
        'role' => 'role',
        'suffix' => 'customersuffix',
        'primarySpecialty' => 'primaryspecialty',
        'professionalCategory' => 'professional_category',
        'country' => 'customer_default_country',
        'insightsCouncilMember' => 'insightscouncilmember',
        'nejm.mediaAccess' => 'media_access'
    ];

    /**
     * @param $customerData
     * @param $apiResponseData
     * @return bool
     */
    public function compareCustomerData($customerData, $apiResponseData)
    {
        if (!empty($customerData) && !empty($apiResponseData)) {
            foreach ($this->apiDataKeys as $apiKey => $customerKey) {
                if (isset($apiResponseData[$apiKey]) && isset($customerData[$customerKey])
                    && $apiResponseData[$apiKey] != $customerData[$customerKey]) {
                    return true;
                }
            }
        }

        return false;
    }
}
