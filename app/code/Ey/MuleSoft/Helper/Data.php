<?php
/**
 * Mulesoft helper
 *
 * @package     Ey_MuleSoft
 * @author      Dinesh Kaswan
 * @copyright   Copyright (c) 2019 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */

namespace Ey\MuleSoft\Helper;

use Magento\Framework\App\Area;
use Magento\Framework\App\Helper\Context;

/**
 * Class Data
 * @package Ey\MuleSoft\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper{
    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;
    /**
     * @var mixed
     */
    public $isDebugMode;
    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customer;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;
    /**
     * @var \Magento\Directory\Api\CountryInformationAcquirerInterface
     */
    protected $countryInformationAcquirer;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var \Magento\Customer\Api\Data\CustomerInterfaceFactory
     */
    protected $customerInterfaceFactory;
    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encryptorInterface;
    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Store\Model\StoreResolver
     */
    protected $storeResolver;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;
    /**
     * @var \Magento\Customer\Model\Group
     */
    protected $group;
    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    protected $addressRepository;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * @var \Magento\Customer\Api\Data\AddressInterfaceFactory
     */
    protected $addressDataFactory;
    /**
     * @var \Magento\Customer\Api\Data\RegionInterfaceFactory
     */
    protected $regionInterfaceFactory;
    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * Data constructor.
     * @param Context $context
     * @param \Magento\Customer\Model\Customer $customer
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Directory\Api\CountryInformationAcquirerInterface $countryInformationAcquirer
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerInterfaceFactory
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptorInterface
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Store\Model\StoreResolver $storeResolver
     * @param \Magento\Customer\Model\Session $session
     * @param \Magento\Customer\Model\Group $group
     * @param \Magento\Customer\Api\AddressRepositoryInterface $addressRepository
     * @param \Magento\Customer\Api\Data\AddressInterfaceFactory $addressDataFactory
     * @param \Magento\Customer\Api\Data\RegionInterfaceFactory $regionInterfaceFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Catalog\Model\Product $product,
        \Magento\Directory\Api\CountryInformationAcquirerInterface $countryInformationAcquirer,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerInterfaceFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptorInterface,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\StoreResolver $storeResolver,
        \Magento\Customer\Model\Session $session,
        \Magento\Customer\Model\Group $group,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
        \Magento\Customer\Api\Data\AddressInterfaceFactory $addressDataFactory,
        \Magento\Customer\Api\Data\RegionInterfaceFactory $regionInterfaceFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\State $state,
        \MMS\Logger\LoggerInterface $logger,
        \Ey\MuleSoft\Helper\Speciality $speciality
    ){
        $this->customer = $customer;
        $this->eavConfig = $eavConfig;
        $this->scopeConfig = $scopeConfig;
        $this->product = $product;
        $this->countryInformationAcquirer = $countryInformationAcquirer;
        $this->timezone = $timezone;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->customerInterfaceFactory =$customerInterfaceFactory;
        $this->encryptorInterface = $encryptorInterface;
        $this->_countryFactory = $countryFactory;
        $this->storeManager = $storeManager;
        $this->storeResolver = $storeResolver;
        $this->session = $session;
        $this->isDebugMode = $this->getMuleSoftDebug();
        $this->isDebugModeType = $this->getMuleSoftDebugMode();
        $this->group = $group;
        $this->addressRepository = $addressRepository;
        $this->addressDataFactory = $addressDataFactory;
        $this->regionInterfaceFactory = $regionInterfaceFactory;
        $this->checkoutSession = $checkoutSession;
        $this->_state = $state;
        $this->_logger = $logger;
        $this->speciality = $speciality;
        parent::__construct($context);
    }


    public function getOrderData($order,$session){
        $orderData = array();
        $orderLine = array();
        $states = $this->getAllStates();
        $items = $order->getAllVisibleItems();

        $billingAddress = $order->getBillingAddress();
        $country = $this->_countryFactory->create()->loadByCode($billingAddress->getData('country_id'));
        $regionCountry = $billingAddress->getCountryId();
        if($country->getData('iso3_code')){
            $regionCountry = $country->getData('iso3_code');
        }

        $billingAddressField = array(
            "addressCode" => '',
            "address" => '',
            "city" => '',
            "state" => '',
            "country" => '',
            "postalCode" => ''
        );

        $state = "";
        if (isset($states[$billingAddress->getData('region_id')])) {
            $state = $states[$billingAddress->getData('region_id')];

        } else if($billingAddress->getData('region_id')){
            $state = $billingAddress->getData('region_id');
        } else {
            $state = $billingAddress->getData('region');
        }

        if($billingAddress){

            $postal_code = str_replace("-", "", $billingAddress->getData('postcode'));
            $postalCode = str_replace(" ", "", $postal_code);

            $billingAddressCode = $billingAddress->getData('addresscode');
            if (!$billingAddressCode) {
                $billingAddressCode = $this->checkoutSession->getBillingAddressCode();
                if (!$billingAddressCode) {
                    $billingAddressCode = 'C1';
                }
            }

            $billingAddressField = array(
                "addressCode" => $billingAddressCode,
                "address" => $billingAddress->getData('street'),
                "city" => $billingAddress->getData('city'),
                "country" => $regionCountry,
                "postalCode" => $postalCode
            );

            if (isset($billingAddressField['postalCode']) && ($postalCode == '' || $postalCode == null)) {
                unset($billingAddressField['postalCode']);
            }

            if ($state != '') {
                $billingAddressField['state'] = $state;
            }
        }


        $shippingAddress = $order->getShippingAddress();
        $shippingAddressField = array();
        if($shippingAddress){
            $country = $this->_countryFactory->create()->loadByCode($shippingAddress->getData('country_id'));
            $ShippingRegionCountry = $shippingAddress->getCountryId();
            if($country->getData('iso3_code')){
                $ShippingRegionCountry = $country->getData('iso3_code');
            }
            $state = "";
            if (isset($states[$shippingAddress->getData('region_id')])) {
                $state = $states[$shippingAddress->getData('region_id')];

            } else if($shippingAddress->getData('region_id')){
                $state = $shippingAddress->getData('region_id');
            } else {
                $state = $shippingAddress->getData('region');
            }

            $mailingAddressCode = $shippingAddress->getData('addresscode');
            if (!$mailingAddressCode) {
                $mailingAddressCode = $this->checkoutSession->getMailingAddressCode();
                if (!$mailingAddressCode) {
                    $mailingAddressCode = 'C1';
                }
            }

            $shippingAddressPostcode = str_replace("-", "", $shippingAddress->getData('postcode'));
            $shippingAddressPostcode = str_replace(" ", "", $shippingAddressPostcode);
            $shippingAddressField = array(
                "addressCode" => $mailingAddressCode,
                "address" => $shippingAddress->getData('street'),
                "city" => $shippingAddress->getData('city'),
                "country" => $ShippingRegionCountry,
                "postalCode" => $shippingAddressPostcode
            );
            if (isset($shippingAddressField['postalCode']) && ($shippingAddressPostcode == '' || $shippingAddressPostcode == null)) {
                unset($shippingAddressField['postalCode']);
            }
            if ($state != '') {
                $shippingAddressField['state'] = $state;
            }
        }

        //TECHNICAL DEBT ON THE WAY WE APPROACHED TO HANDLE THE CODE AND INSTEAD CALLING QUOTE FOR THE FIRNAME AND LASTNAME.
        //EY HAVE DONE SERIOUS SHIT HERE.
        //IT IS VERY HEAVY OBJECT TO LOAD AND CAUASE CHECKOUT SLOW PROCESS.
        // NON-LOGGED-IN CUSTOMER
        //Check if the customer already exits:
        $storeId=$this->storeResolver->getCurrentStoreId();
        $store = $this->storeManager->getStore($storeId);
        $websiteId = $store->getWebsiteId();
        $customer = $this->customer->setWebsiteId($websiteId)->loadByEmail($order->getData('customer_email'));
        //$customer = $this->customer->setWebsiteId(1)->loadByEmail($order->getData('customer_email'));
        $CustomerUcid="";
        $existingCustomer=false;
        if($customer->getId()){
            $CustomerUcid = $customer->getData('ucid');

                $this->_logger->debug("customer ucid:-" . $CustomerUcid);

            $existingCustomer=true;

            $this->session->setCustomerExist('YES');
            //$order->getData('customer_is_guest')
        }else{
            $this->session->setCustomerExist('NO');
        }


        if(!$existingCustomer && $CustomerUcid == ""){

                $this->_logger->debug("NEW-VISITING-CUSTOMER" . $CustomerUcid);

            $orderData['customer'] = array();
            $orderData['customer']['customerId'] = "0000000-GST";

            if($order->getData('pi_firstname') != NULL){
                $orderData['customer']['firstName'] = $order->getData('pi_firstname');
            }

            if($order->getData('pi_lastname') != NULL){
                $orderData['customer']['lastName'] =$order->getData('pi_lastname');
            }

            if($order->getData('customer_email') != NULL){
                $orderData['customer']['email'] =$order->getData('customer_email');
            }

            if($order->getData('ba_password') != NULL){
                $orderData['customer']['password'] =$order->getData('ba_password');
            }

            if($order->getData('pi_role') != NULL){
                $orderData['customer']['role'] =$order->getData('pi_role');
            }

            if($order->getData('pi_suffix') != NULL){
                $orderData['customer']['suffix'] =$order->getData('pi_suffix');
            }

            if($order->getData('pi_primaryspecialty') != NULL){
                $orderData['customer']['primarySpecialty'] =$order->getData('pi_primaryspecialty');
            }

            if($order->getData('pi_organization') != NULL){
                $orderData['customer']['nameOfOrganization'] =$order->getData('pi_organization');
            }

            if($regionCountry != NULL){
                $orderData['customer']['regionCountry'] = $regionCountry;
            }

            if($order->getData('catalyst_connect') != NULL){
                $orderData['customer']['catalystConnect'] = (bool)$order->getData('catalyst_connect');
            }

            if($order->getData('catalyst_soi') != NULL){
                $orderData['customer']['catalystSOI'] = (bool)$order->getData('catalyst_soi');
            }


        }elseif($CustomerUcid !="" &&  $existingCustomer && !$session->isLoggedIn() ){


                $this->_logger->debug("NON LOGGED-IN CUSTOMER WITH UCID ");



            $role = $this->eavConfig->getAttribute('customer', 'role');
            $customersuffix = $this->eavConfig->getAttribute('customer', 'customersuffix');
            $primaryspecialty = $this->eavConfig->getAttribute('customer', 'primaryspecialty');
            $orderData['customer'] = array();
            $orderData['customer']['customerId'] = (string)$customer->getId();


                $this->_logger->debug("ucid: ".$customer->getData('ucid'));
                $this->_logger->debug("customer_email: ".$customer->getData('email'));

            if($customer->getData('ucid') != NULL){
                $orderData['customer']['ucId'] =$customer->getData('ucid');
            }

            if($customer->getData('firstname') != NULL){
                $orderData['customer']['firstName'] =$customer->getData('firstname');
            }


            if($customer->getData('lastname') != NULL){
                $orderData['customer']['lastName'] =$customer->getData('lastname');
            }


            if($customer->getData('email')!= NULL){
                $orderData['customer']['email'] =$customer->getData('email');
            }

            //TEchnical DebtHere add the logic for the admin customer
            $roleAttr = $role->setStoreId(0)->getSource()->getOptionText($customer->getData('role'));


                $this->_logger->debug("roleAttr: ".$roleAttr);


            if($roleAttr != NULL){
                $orderData['customer']['role'] =$roleAttr;
            }

            $suffAttr = $customersuffix->setStoreId(1)->getSource()->getOptionText($customer->getData('customersuffix'));

                $this->_logger->debug("suffAttr1: ".$customer->getData('customersuffix'));


            if($suffAttr != NULL){
                $orderData['customer']['suffix'] =$suffAttr;
            }


            $primAttr = $primaryspecialty->setStoreId(0)->getSource()->getOptionText($customer->getData('primaryspecialty'));


                $this->_logger->debug("primaryspecialtyAttr: ".$primAttr);

            if($primAttr != NULL){
                $orderData['customer']['primarySpecialty'] =$primAttr;
            }


                $this->_logger->debug("nameoforganization: ".$customer->getData('nameoforganization'));


            $primAttr = $primaryspecialty->setStoreId(0)->getSource()->getOptionText($customer->getData('nameoforganization'));
            if($customer->getData('nameoforganization') != NULL){
                $orderData['customer']['nameOfOrganization'] =$customer->getData('nameoforganization');
            }



                $this->_logger->debug("regionCountry: ".$regionCountry);



            if($regionCountry != NULL){
                $orderData['customer']['regionCountry'] =$regionCountry;
            }



                $this->_logger->debug("catalyst_connect: ".$customer->getData('catalystconnect'));



            if($customer->getData('catalystconnect') != NULL){
                $orderData['customer']['catalystConnect'] = (bool)$customer->getData('catalystconnect');
            }



                $this->_logger->debug("catalyst_soi: " . $customer->getData('catalystsoi'));




            if($customer->getData('catalystsoi') != NULL){
                $orderData['customer']['catalystSOI'] = (bool)$customer->getData('catalystsoi');
            }



                $this->_logger->debug("catalystRenewalCode: ".$customer->getData('catalystrenewalcode'));



            if($customer->getData('catalystrenewalcode') != NULL){
                $orderData['customer']['catalystRenewalCode'] =$customer->getData('catalystrenewalcode');
            }


                $this->_logger->debug("insightscouncilmember: " . $customer->getData('insightscouncilmember'));


            if($customer->getData('insightscouncilmember') != NULL){
                $orderData['customer']['insightsCouncilMember'] = (bool)$customer->getData('insightscouncilmember');
            }
        }
        // LOGGED-IN CUSTOMER
        else{


                $this->_logger->debug("LOGGED-IN CUSTOMER");
                $this->_logger->debug(" Logged in Customer:-> ".json_encode($customer->getData(), JSON_PRETTY_PRINT));



            $role = $this->eavConfig->getAttribute('customer', 'role');
            $customersuffix = $this->eavConfig->getAttribute('customer', 'customersuffix');
            $primaryspecialty = $this->eavConfig->getAttribute('customer', 'primaryspecialty');
            $orderData['customer'] = array();
            $orderData['customer']['customerId'] = (string)$order->getData('customer_id');


                $this->_logger->debug("ucid: ".$customer->getData('ucid'));
                $this->_logger->debug("customer_email: ".$customer->getData('email'));


            if($customer->getData('ucid') != NULL){
                $orderData['customer']['ucId'] =$customer->getData('ucid');
            }

            if($customer->getData('firstname') != NULL){
                $orderData['customer']['firstName'] =$customer->getData('firstname');
            }


            if($customer->getData('lastname') != NULL){
                $orderData['customer']['lastName'] =$customer->getData('lastname');
            }


            if($customer->getData('email')!= NULL){
                $orderData['customer']['email'] =$customer->getData('email');
            }


                $this->_logger->debug("nameoforganization: ".$customer->getData('nameoforganization'));


            if($customer->getData('nameoforganization') != NULL){
                $orderData['customer']['nameOfOrganization'] =$customer->getData('nameoforganization');
            }



                $this->_logger->debug("regionCountry: ".$regionCountry);



            if($regionCountry != NULL){
                $orderData['customer']['regionCountry'] =$regionCountry;
            }


                $this->_logger->debug("catalystrenewalcode: ".$customer->getData('catalystrenewalcode'));



            if($customer->getData('catalystrenewalcode') != NULL){
                $orderData['customer']['catalystRenewalCode'] =$customer->getData('catalystrenewalcode');
            }
        }

        $orderLineNumber = 1;
        foreach($items as $item){
            $prod = $this->product->load($item->getProduct()->getId());
            $term = $this->getTermFromOrderOrProduct($item, $prod);
            $autorenew = false;
            if($order->getPayment()->getMethodInstance()->getCode() == "md_firstdata"){
                $autorenew = true;  // (bool)$prod->getData('autorenew')
            } else if ($order->getPayment()->getMethodInstance()->getCode() == 'paypal_express') {
                $autorenew = true;
            }


            //Technical debt - we have all autorenew set to false regardless of product attribute.
            // Setting autorenew to yes when card type is md_firstdata.
            $ol_array = array(
                "orderLineNumber" => $orderLineNumber,  //Technical Debt This needs to be fixed in the future version.
                "brand" => $prod->getResource()->getAttribute('brand')->getFrontend()->getValue($prod),
                "sku" => $prod->getData('sku'),
                "term" => $term,
                "autoRenew" => (bool)$autorenew, // If md_first data then use autorenew = true
                "price" => (float)number_format($item->getData('price'), 4),
                "tax" => (float)$item->getData('tax_amount'),
                "quantity"=> (float)$item->getData('qty_ordered'),
            );
            if($order->getData('ucc_pricecode') && $order->getData('ucc_pricecode')!=""){
                $ol_array["priceCode"] = $order->getData('ucc_pricecode');
            }
            if($order->getData('ucc_promocode') && $order->getData('ucc_promocode')!=""){
                $ol_array["promoCode"] = $order->getData('ucc_promocode');
            }
            if($order->getData('pi_doi') && $order->getData('pi_doi')!=""){
                $ol_array["DOI"] = $order->getData('pi_doi');
            }
            array_push($orderLine,$ol_array);
            $orderLineNumber++;
        }
        $orderData['order'] = array(
            "orderEntityId" => $order->getIncrementId(),
            "createdAt" => $this->timezone->date(new \DateTime($order->getData('created_at')))->format('Y-m-d\TH:i:s\Z'),
            "totalDue" => (float)$order->getData('grand_total'),
            "storeId" => (int)$order->getData('store_id'), //0 for admin orders
            "storeCurrencyCode" => $order->getData('order_currency_code'),
            "orderLines" => $orderLine,
            "billToAddress" => $billingAddressField
        );
        if($shippingAddress){
            $orderData['order']["shipToAddress"] = $shippingAddressField;
        }
        if($billingAddress->getData('vat_id') && $billingAddress->getData('vat_id')!=""){
            $orderData['order']['vatNumber'] = $billingAddress->getData('vat_id');
        }


        $orderData['payment'] = array(
            "paymentAmount" => (float)$order->getData('grand_total'),
            "paymentType" => $order->getPayment()->getMethodInstance()->getCode()
        );
        //Technical Debt change the event logger to magento logger

        if($order->getPayment()->getMethodInstance()->getCode() == "md_firstdata") {
            $additonal_information = $order->getPayment()->getAdditionalInformation();
            foreach($additonal_information['md_firstdata_cards'] as $e => $val) {
                $finalData = $val;
            }
            $cardType = $finalData['cc_type'];
            $cardExpMonth =$finalData['cc_exp_month'];
            $cardExpDate =$finalData['cc_exp_year'];
            $lastFour=$finalData['cc_last_4'];
            $authorization = array(
                'authDate' => $this->timezone->date(new \DateTime($order->getPayment()->getCreatedAt()))->format('Y-m-d\TH:i:s\Z'),  //DateTime  // Created date time of transcation.^M
                'authCode' =>   (string)$order->getPayment()->getCcTransId(), //String  //cc_trans_id^M
                'authNumber' => (string)$order->getPayment()->getTransactionTag() //Stringtran
            );

            $cardCcCid = $this->checkoutSession->getSaveCardCcCid() ?? "123";
            $this->checkoutSession->setSaveCardCcCid(null);

            $orderData['payment']['creditCardInfo']= array(
                //( Only Mandatory for the md_firstdata )  From the order payment
                "cardType" => $cardType,  // ( Various type of card VI,MA, ( 3 card types )
                "cardToken" =>  (string)$order->getPayment()->getFirstdataToken(),  //( coming from the first data to charge card details )
                "customerName" => $billingAddress->getData('firstname')."  ".$billingAddress->getData('lastname'),
                "expirationMonth" => (int)$cardExpMonth,

                "expirationYear" => ((int)$cardExpDate < 100 ) ? (int)$cardExpDate + 2000 : (int)$cardExpDate,
                "lastFour" => (string)$lastFour,
                "securityCode" => $cardCcCid,
                "authorization" => $authorization
            );
        }

        return $orderData;
    }

    public function getOrderDataForAdmin($order, $session){
        $addressCode  = $session->getAddressCode();
        if (!$addressCode) {
            $addressCode = 'C1';
        }

        $this->_logger->debug("Order Data for Admin");

        $orderData = array();
        $orderLine = array();
        $states = $this->getAllStates();
        $items = $order->getAllVisibleItems();

        $billingAddress = $order->getBillingAddress();
        $country = $this->_countryFactory->create()->loadByCode($billingAddress->getData('country_id'));
        $regionCountry = $billingAddress->getCountryId();
        if ($country->getData('iso3_code')) {
            $regionCountry = $country->getData('iso3_code');
        }

        $billingAddressField = array(
            "addressCode" => '',
            "address" => '',
            "city" => '',
            "state" => '',
            "country" => '',
            "postalCode" => ''
        );

        $state = "";
        if (isset($states[$billingAddress->getData('region_id')])) {
            $state = $states[$billingAddress->getData('region_id')];

        } else if($billingAddress->getData('region_id')){
            $state = $billingAddress->getData('region_id');
        } else {
            $state = $billingAddress->getData('region');
        }

        if ($billingAddress) {
            $postal_code = str_replace("-", "", $billingAddress->getData('postcode'));
            $postalCode = str_replace(" ", "", $postal_code);

            $billingAddressField = array(
                "addressCode" => $addressCode,
                "address" => $billingAddress->getData('street'),
                "city" => $billingAddress->getData('city'),
                "country" => $regionCountry,
                "postalCode" => $postalCode
            );
            if (isset($billingAddressField['postalCode']) && ($postalCode == '' || $postalCode == null)) {
                unset($billingAddressField['postalCode']);
            }
            if ($state != '') {
                $billingAddressField['state'] = $state;
            }
        }


        $shippingAddress = $order->getShippingAddress();
        $shippingAddressField = array();
        if ($shippingAddress) {
            $country = $this->_countryFactory->create()->loadByCode($shippingAddress->getData('country_id'));
            $ShippingRegionCountry = $shippingAddress->getCountryId();
            if ($country->getData('iso3_code')) {
                $ShippingRegionCountry = $country->getData('iso3_code');
            }
            $state = "";
            if (isset($states[$shippingAddress->getData('region_id')])) {
                $state = $states[$shippingAddress->getData('region_id')];

            } else if($shippingAddress->getData('region_id')){
                $state = $shippingAddress->getData('region_id');
            } else {
                $state = $shippingAddress->getData('region');
            }

            $shippingAddressPostcode = str_replace("-", "", $shippingAddress->getData('postcode'));
            $shippingAddressPostcode = str_replace(" ", "", $shippingAddressPostcode);
            $shippingAddressField = array(
                "addressCode" => $addressCode,
                "address" => $shippingAddress->getData('street'),
                "city" => $shippingAddress->getData('city'),
                "country" => $ShippingRegionCountry,
                "postalCode" => $shippingAddressPostcode
            );
            if (isset($shippingAddressField['postalCode']) && ($shippingAddressPostcode == '' || $shippingAddressPostcode == null)) {
                unset($shippingAddressField['postalCode']);
            }
            if ($state != '') {
                $shippingAddressField['state'] = $state;
            }
            if ($state != '') {
                $shippingAddressField['state'] = $state;
            }
        }

        //TECHNICAL DEBT ON THE WAY WE APPROACHED TO HANDLE THE CODE AND INSTEAD CALLING QUOTE FOR THE FIRNAME AND LASTNAME.
        //EY HAVE DONE SERIOUS SHIT HERE.
        //IT IS VERY HEAVY OBJECT TO LOAD AND CAUASE CHECKOUT SLOW PROCESS.
        // NON-LOGGED-IN CUSTOMER
        //Check if the customer already exits:
        $storeId = $this->storeResolver->getCurrentStoreId();
        $store = $this->storeManager->getStore($storeId);
        $websiteId = $store->getWebsiteId();
        $customer = $this->customer->setWebsiteId($websiteId)->loadByEmail($order->getData('customer_email'));
        //$customer = $this->customer->setWebsiteId(1)->loadByEmail($order->getData('customer_email'));
        $CustomerUcid = "";
        $existingCustomer = false;
        if ($customer->getId()) {
            $CustomerUcid = $customer->getData('ucid');

                $this->_logger->debug("customer ucid:-" . $CustomerUcid);

            $existingCustomer = true;
            //$order->getData('customer_is_guest')
        }


            $this->_logger->debug("ADMIN ORDER CUSTOMER");



            $this->_logger->debug(" Logged in Customer:-> " . json_encode($customer->getData(), JSON_PRETTY_PRINT));



        $role = $this->eavConfig->getAttribute('customer', 'role');
        $customersuffix = $this->eavConfig->getAttribute('customer', 'customersuffix');
        $primaryspecialty = $this->eavConfig->getAttribute('customer', 'primaryspecialty');
        $orderData['customer'] = array();
        $orderData['customer']['customerId'] = (string)$order->getData('customer_id');


            $this->_logger->debug("ucid: " . $customer->getData('ucid'));
            $this->_logger->debug("customer_email: " . $customer->getData('email'));

        if ($customer->getData('ucid') != NULL) {
            $orderData['customer']['ucId'] = $customer->getData('ucid');
        }

        if ($customer->getData('firstname') != NULL) {
            $orderData['customer']['firstName'] = $customer->getData('firstname');
        }


        if ($customer->getData('lastname') != NULL) {
            $orderData['customer']['lastName'] = $customer->getData('lastname');
        }


        if ($customer->getData('email') != NULL) {
            $orderData['customer']['email'] = $customer->getData('email');
        }

        //TEchnical DebtHere add the logic for the admin customer



            $this->_logger->debug("nameoforganization: " . $customer->getData('nameoforganization'));


        if ($customer->getData('nameoforganization') != NULL) {
            $orderData['customer']['nameOfOrganization'] = $customer->getData('nameoforganization');
        }



            $this->_logger->debug("regionCountry: " . $regionCountry);



        if ($regionCountry != NULL) {
            $orderData['customer']['regionCountry'] = $regionCountry;
        }

            $this->_logger->debug("catalystrenewalcode: " . $customer->getData('catalystrenewalcode'));



        if ($customer->getData('catalystrenewalcode') != NULL) {
            $orderData['customer']['catalystRenewalCode'] = $customer->getData('catalystrenewalcode');
        }


        $orderLineNumber = 1;
        foreach ($items as $item) {
            $prod = $this->product->load($item->getProduct()->getId());
            $term = $this->getTermFromOrderOrProduct($item, $prod);

            $autorenew = false;
            if ($order->getPayment()->getMethodInstance()->getCode() == "md_firstdata") {
                $autorenew = true;  // (bool)$prod->getData('autorenew')
            } else if ($order->getPayment()->getMethodInstance()->getCode() == 'paypal_express') {
                $autorenew = true;
            }


            //Technical debt - we have all autorenew set to false regardless of product attribute.
            // Setting autorenew to yes when card type is md_firstdata.
            $ol_array = array(
                "orderLineNumber" => $orderLineNumber,  //Technical Debt This needs to be fixed in the future version.
                "brand" => $prod->getResource()->getAttribute('brand')->getFrontend()->getValue($prod),
                "sku" => $prod->getData('sku'),
                "term" => $term,
                "autoRenew" => (bool)$autorenew, // If md_first data then use autorenew = true
                "price" => (float)number_format($item->getData('price'), 4),
                "tax" => (float)$item->getData('tax_amount'),
                "quantity" => (float)$item->getData('qty_ordered'),
            );

            if ($item->getSku() == 'CAT-ANL-CMP' || $item->getSku() == "CAT-MTH-CMP") {
                $ol_array["priceCode"] = 'COMP';
            }
            if ($order->getData('ucc_promocode') && $order->getData('ucc_promocode') != "") {
                $ol_array["promoCode"] = $order->getData('ucc_promocode');
            }
            if ($order->getData('pi_doi') && $order->getData('pi_doi') != "") {
                $ol_array["DOI"] = $order->getData('pi_doi');
            }
            array_push($orderLine, $ol_array);
            $orderLineNumber++;
        }
        $orderData['order'] = array(
            "orderEntityId" => $order->getIncrementId(),
            "createdAt" => $this->timezone->date(new \DateTime($order->getData('created_at')))->format('Y-m-d\TH:i:s\Z'),
            "totalDue" => (float)$order->getData('grand_total'),
            "storeId" => 0, //0 for admin orders
            "storeCurrencyCode" => $order->getData('order_currency_code'),
            "orderLines" => $orderLine,
            "billToAddress" => $billingAddressField
        );
        if ($shippingAddress) {
            $orderData['order']["shipToAddress"] = $shippingAddressField;
        }
        if ($billingAddress->getData('vat_id') && $billingAddress->getData('vat_id') != "") {
            $orderData['order']['vatNumber'] = $billingAddress->getData('vat_id');
        }


        $orderData['payment'] = array(
            "paymentAmount" => (float)$order->getData('grand_total'),
            "paymentType" => $order->getPayment()->getMethodInstance()->getCode()
        );

        if ((float)$order->getData('grand_total') == 0) {
            $orderData["isComp"] = true;
        }

        //Technical Debt change the event logger to magento logger

        if ($order->getPayment()->getMethodInstance()->getCode() == "md_firstdata") {
            $additonal_information = $order->getPayment()->getAdditionalInformation();
            foreach ($additonal_information['md_firstdata_cards'] as $e => $val) {
                $finalData = $val;
            }
            $cardType = $finalData['cc_type'];
            $cardExpMonth = $finalData['cc_exp_month'];
            $cardExpDate = $finalData['cc_exp_year'];
            $lastFour = $finalData['cc_last_4'];
            $authorization = array(
                'authDate' => $this->timezone->date(new \DateTime($order->getPayment()->getCreatedAt()))->format('Y-m-d\TH:i:s\Z'),  //DateTime  // Created date time of transcation.^M
                'authCode' => (string)$order->getPayment()->getCcTransId(), //String  //cc_trans_id^M
                'authNumber' => (string)$order->getPayment()->getTransactionTag() //Stringtran
            );

            $orderData['payment']['creditCardInfo'] = array(
                //( Only Mandatory for the md_firstdata )  From the order payment
                "cardType" => $cardType,  // ( Various type of card VI,MA, ( 3 card types )
                "cardToken" => (string)$order->getPayment()->getFirstdataToken(),  //( coming from the first data to charge card details )
                "customerName" => $billingAddress->getData('firstname') . "  " . $billingAddress->getData('lastname'),
                "expirationMonth" => (int)$cardExpMonth,

                "expirationYear" => ((int)$cardExpDate < 100) ? (int)$cardExpDate + 2000 : (int)$cardExpDate,
                "lastFour" => (string)$lastFour,
                "securityCode" => "123",  //Its going to be the new ticket for adding the security code.
                "authorization" => $authorization
            );
        }
        return $orderData;
    }
    public function getAllStates(){
        $countries = $this->countryInformationAcquirer->getCountriesInfo();
        $states = array();
        foreach ($countries as $country) {
            if ($availableRegions = $country->getAvailableRegions()) {
                foreach ($availableRegions as $region) {
                    $states[$region->getId()] = $region->getCode();

                }
            }
        }
        return $states;
    }

    public function customerExists($email, $websiteId = null){
        $customer = $this->customer;
        if ($websiteId) {
            $customer->setWebsiteId($websiteId);
        }
        $customer->loadByEmail($email);
        if ($customer->getId()) {
            return $customer;
        }

        return false;
    }

    public function createCustomer($orderData){

        //Technical debt write some better way to write the option values. its tooheavy for success order page.
        $role = $this->eavConfig->getAttribute('customer', 'role');
        $customersuffix = $this->eavConfig->getAttribute('customer', 'customersuffix');
        $primaryspecialty = $this->eavConfig->getAttribute('customer', 'primaryspecialty');



            $this->_logger->debug("Order Object before creating customer" . json_encode($orderData, JSON_PRETTY_PRINT));

        try{

            $customer  = $this->customerFactory->create();
            $customer->setWebsiteId(1);
            $customer->setEmail($orderData['customer']['email']);

            if( isset($orderData['customer']['firstName']) ){
                $firstname = $orderData['customer']['firstName'];
            }else{
                $firstname= 'NA';
            }

            if( isset($orderData['customer']['lastName']) ){
                $lastName = $orderData['customer']['lastName'];
            }else{
                $lastName= 'NA';
            }

            if( isset($orderData['customer']['password']) ){
                $password = $orderData['customer']['password'];
                $hashedPassword = $this->encryptorInterface->getHash($orderData['customer']['password'], true);
                $customer->setPasswordHash($hashedPassword);
            }else{
                $password= 'NA';
            }


            $customer->setFirstname( $firstname );
            $customer->setLastname( $lastName );
            $customerData = $customer->getDataModel();

            if(isset($orderData['customer']['role'])){
                $customerData->setCustomAttribute('role',$role->setStoreId(0)->getSource()->getOptionId($orderData['customer']['role']));


                    $this->_logger->debug("Role saved - " . $role->setStoreId(0)->getSource()->getOptionId($orderData['customer']['role']));

            }
            if(isset($orderData['customer']['suffix'])){
                $customerData->setCustomAttribute('customersuffix',$customersuffix->setStoreId(0)->getSource()->getOptionId($orderData['customer']['suffix']));

                    $this->_logger->debug("suffix Saved - " . $customersuffix->setStoreId(0)->getSource()->getOptionId($orderData['customer']['suffix']));

            }
            if(isset($orderData['customer']['primarySpecialty'])){

                    $this->_logger->debug("primarySpecialty saved - " . $primaryspecialty->setStoreId(0)->getSource()->getOptionId($orderData['customer']['primarySpecialty']));

                $customerData->setCustomAttribute('primaryspecialty',$primaryspecialty->setStoreId(0)->getSource()->getOptionId($orderData['customer']['primarySpecialty']));
            }
            if(isset($orderData['customer']['nameOfOrganization'])){
                $customerData->setCustomAttribute('nameoforganization',$orderData['customer']['nameOfOrganization']);
            }
            if(isset($orderData['customer']['catalystConnect'])){
                $customerData->setCustomAttribute('catalystconnect',$orderData['customer']['catalystConnect']);
            }
            if(isset($orderData['customer']['catalystSOI'])){
                $customerData->setCustomAttribute('catalystsoi',$orderData['customer']['catalystSOI']);
            }


            //Technical Debt for storing the value in the correct way.
            /*
            $customerData->setCustomAttribute('role','337');
            $customerData->setCustomAttribute('suffix','348');
            $customerData->setCustomAttribute('primaryspecialty','246');
            $customerData->setCustomAttribute('nameoforganization','FideleSys LLC.');
            $customerData->setCustomAttribute('catalystconnect',1);
            $customerData->setCustomAttribute('catalystsoi',1);
            */

            $customer->updateData($customerData);
            $customer->setConfirmation(null);
            $customer->save();


                $this->_logger->debug("Saving Customer 1st time:-> ".json_encode( $customer->getData(), JSON_PRETTY_PRINT));


            return $customer;
        }catch(Exception $e){
            throw new \Magento\Framework\Exception\LocalizedException($e->getMessage());
        }

    }
    public function updateCustomer($response, $customer){

        //Technical debt write some better way to write the option values. its tooheavy for success order page.
        $role = $this->eavConfig->getAttribute('customer', 'role');
        $customersuffix = $this->eavConfig->getAttribute('customer', 'customersuffix');
        $primaryspecialty = $this->eavConfig->getAttribute('customer', 'primaryspecialty');
        $professionalCategory = $this->eavConfig->getAttribute('customer', 'professional_category');

        if (isset($response['Customer']['firstName'])) {
            $customer->setData('firstname', $response['Customer']['firstName']);
        }
        if (isset($response['Customer']['lastName'])) {
            $customer->setData('lastname', $response['Customer']['lastName']);
        }

        $customerData = $customer->getDataModel();
        if (isset($response['Customer']['firstName'])) {
            $customerData->setCustomAttribute('firstname', $response['Customer']['firstName']);
        }
        if (isset($response['Customer']['lastName'])) {
            $customerData->setCustomAttribute('lastname', $response['Customer']['lastName']);
        }

        if (isset($response['Customer']['role'])) {
            $customerData->setCustomAttribute('role',$role->setStoreId(0)->getSource()->getOptionId($response['Customer']['role']));
        }
        if (isset($response['Customer']['suffix'])) {
            $customerData->setCustomAttribute('customersuffix',$customersuffix->setStoreId(0)->getSource()->getOptionId($response['Customer']['suffix']));
        }
        if (isset($response['Customer']['primarySpecialty'])) {
            $customerData->setCustomAttribute('primaryspecialty',$primaryspecialty->setStoreId(0)->getSource()->getOptionId($response['Customer']['primarySpecialty']));
        }
        if (isset($response['Customer']['professionalCategory'])) {
            $customerData->setCustomAttribute('professional_category',$professionalCategory->setStoreId(0)->getSource()->getOptionId($response['Customer']['professionalCategory']));
        }
        if (isset($response['Customer']['nameOfOrganization'])) {
            $customerData->setCustomAttribute('nameoforganization',$response['Customer']['nameOfOrganization']);
        }
        if (isset($response['Customer']['ucid'])) {
            $customerData->setCustomAttribute('ucid',$response['Customer']['ucid']);
        }
        if (isset($response['Customer']['catalystConnect'])) {
            $customerData->setCustomAttribute('catalystconnect',$response['Customer']['catalystConnect']);
        }
        if (isset($response['Customer']['catalystSOI'])) {
            $customerData->setCustomAttribute('catalystsoi',$response['Customer']['catalystSOI']);
        }

        if (isset($response['Customer']['nejmSOI'])) {
            $customerData->setCustomAttribute('nejmsoi',$response['Customer']['nejmSOI']);
        }
        if (isset($response['Customer']['nejmTOC'])) {
            $customerData->setCustomAttribute('nejmetoc',$response['Customer']['nejmTOC']);
        }
        if (isset($response['Customer']['nejm.mediaAccess'])) {
            $customerData->setCustomAttribute('media_access',$response['Customer']['nejm.mediaAccess']);
        }

        if (isset($response['Customer']['insightsCouncilMember'])) {
            $customerData->setCustomAttribute('insightscouncilmember',$response['Customer']['insightsCouncilMember']);
        }
        if (isset($response['Customer']['country'])) {
            $customerData->setCustomAttribute('customer_default_country',$response['Customer']['country']);
        }

        $customer->updateData($customerData);
        $customer->setConfirmation(null);
        $customer->save();

        return $customer;
    }
    public function saveUcid($customer_id,$ucid){
        try{
            $customer = $this->customer->load($customer_id);
            $customerData = $customer->getDataModel();
            $customerData->setCustomAttribute('ucid',$ucid);
            $customer->updateData($customerData);
            $customer->save();
        }catch(Exception $e){
            throw new \Magento\Framework\Exception\LocalizedException($e->getMessage());
        }
    }


    public function getMuleSoftClientId()
    {
        return $this->scopeConfig->getValue('mulesoft/api/client_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getMuleSoftClientSecret()
    {
        return $this->scopeConfig->getValue('mulesoft/api/client_secret', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getMuleSoftDebug()
    {
        return $this->scopeConfig->getValue('mulesoft/api/debug', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getMuleSoftDebugMode()
    {
        return ($this->scopeConfig->getValue('mulesoft/api/debug_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 2)?true:false;
    }

    public function getMuleSoftCustomerURI()
    {
        return $this->getMuleSoftURI();
    }

    public function getMuleSoftURI()
    {
        return $this->scopeConfig->getValue('mulesoft/api/mulesoft_uri', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $orderData
     * @return mixed|string
     */
    public function getMuleSoftOrderEndpoint($orderData)
    {
        $orderApiEndpoint = $this->scopeConfig->getValue('mulesoft/api/mulesoft_order_uri', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if (isset($orderData['customer']['ucId'])) {
            $orderApiEndpoint = str_replace("{UCID}",$orderData['customer']['ucId'], $orderApiEndpoint);
        }
        return $orderApiEndpoint;
    }

    /**
     * Get token validate uri from configuration.
     * @return String
     */
    public function getMuleSoftTokenValidationURI()
    {
        return $this->scopeConfig->getValue(
            'mulesoft/api/mulesoft_token_uri',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param \Magento\Sales\Model\Order\Item $orderItem
     * @param \Magento\Catalog\Model\Product $product
     * @return bool|string
     */
    public function getTermFromOrderOrProduct(
        \Magento\Sales\Model\Order\Item $orderItem,
        \Magento\Catalog\Model\Product $product
    ) {
        $term = $this->getTermsFromOrderItem($orderItem);
        if (!$term) {
            $term = $this->getTermsFromProduct($product);
        }
        return $term;
    }

    /**
     * @param \Magento\Sales\Model\Order\Item $orderItem
     * @return bool|string
     */
    protected function getTermsFromOrderItem(\Magento\Sales\Model\Order\Item $orderItem)
    {
        $term = false;
        $options = $orderItem->getProductOptions();
        if (isset($options['options']) && !empty($options['options'])) {
            foreach ($options['options'] as $option) {
                if (strtolower($option['label']) == 'term'
                    || strtolower($option['label']) == 'terms') {
                    $term = $option['value'];
                }
            }
        }
        return $term;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    protected function getTermsFromProduct(\Magento\Catalog\Model\Product $product)
    {
        return $product->getResource()->getAttribute('months')->getFrontend()->getValue($product);
    }


    public function postSendMuleSoftRequest($params , $header){
        // TODO
        //use that in future for the post call make all the post
        //request call end up calling this function for all mulesoft API logs.

            $this->_logger->debug("****************** Renew Payment API Log START *******************");
            $this->_logger->debug("HEADERS-> ". json_encode($header, JSON_PRETTY_PRINT));
            $this->_logger->debug("Renew - Order Request Data Object-> ");
            $this->_logger->debug(json_encode($params, JSON_PRETTY_PRINT));
            $this->_logger->debug("Response Code-> ". $params->getStatusCode());
            $this->_logger->debug("MuleSoft Response->body-> ");
            $this->_logger->debug($params->getBody()->getContents());
            $this->_logger->debug("MuleSoft POST log-> ");
            $this->_logger->debug(print_r($params, true));
            $this->_logger->debug("****************** Renew - Payment API Log ENDS *******************");

    }

    public function getSendMuleSoftRequest($params,$header){


            $this->_logger->debug("****************** Renew - GetEligibility API Log STARTS *******************");
            $this->_logger->debug("HEADERS-> ". json_encode($header, JSON_PRETTY_PRINT));
            if($this->isDebugModeType){
                $this->_logger->info("Renew -Get Eligibility Request Data Object-> ");
                $this->_logger->info(json_encode($params, JSON_PRETTY_PRINT));
            }
            $this->_logger->debug("Response Code-> ". $params->getStatusCode());

    }

    public function getSendMuleSoftRequestException($params,$header){


            $this->_logger->debug("****************** Renew - GetEligibility API Log STARTS *******************");
            $this->_logger->debug("HEADERS-> ". json_encode($header, JSON_PRETTY_PRINT));
            $this->_logger->debug("Response Code-> ". $params->getStatusCode());
            $this->_logger->debug("Renew -Get Eligibility Request Exception-> ");
            $this->_logger->debug(json_encode($params, JSON_PRETTY_PRINT));
            $this->_logger->debug("****************** Renew - GetEligibility API Log ENDS *******************");

    }

    /**
     * @param $order
     * @param $quote
     * @param $postParams
     * @return bool|\Magento\Customer\Model\Customer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function createCustomerBeforeOrder($order, $postParams)
    {
        /* Technical debt write some better way to write the option values. its tooheavy for success order page. */
        $role = $this->eavConfig->getAttribute('customer', 'role');
        $customersuffix = $this->eavConfig->getAttribute('customer', 'customersuffix');
        $primaryspecialty = $this->eavConfig->getAttribute('customer', 'primaryspecialty');
        $professionalCategory = $this->eavConfig->getAttribute('customer', 'professional_category');
        $profession = $this->eavConfig->getAttribute('customer', 'profession');
        $studentType = $this->eavConfig->getAttribute('customer', 'student_type');
        $orderData = $this->getCustomerInfoFromOrder($order, $postParams);
        $storeId = $this->storeResolver->getCurrentStoreId();
        $store = $this->storeManager->getStore($storeId);
        $websiteId = $store->getWebsiteId();

            $this->_logger->debug("Order Object before creating customer" . json_encode($orderData, JSON_PRETTY_PRINT));

        try {
            $customer  = $this->customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->setEmail($orderData['customer']['email']);
            if (isset($orderData['customer']['firstName'])) {
                $firstname = $orderData['customer']['firstName'];
            } else {
                $firstname= 'NA';
            }
            if (isset($orderData['customer']['lastName'])) {
                $lastName = $orderData['customer']['lastName'];
            } else {
                $lastName= 'NA';
            }
            if (isset($orderData['customer']['password'])) {
                $hashedPassword = $this->encryptorInterface->getHash($orderData['customer']['password'], true);
                $customer->setPasswordHash($hashedPassword);
            }

            $customer->setFirstname( $firstname );
            $customer->setLastname( $lastName );
            $customerData = $customer->getDataModel();

            if (isset($orderData['customer']['role'])) {
                $customerData->setCustomAttribute(
                    'role',
                    $role->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionId($orderData['customer']['role'])
                );

                    $this->_logger->debug("Role saved - " . $role->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionId($orderData['customer']['role']));

            }
            if (isset($orderData['customer']['suffix'])) {
                $customerData->setCustomAttribute(
                    'customersuffix',
                    $customersuffix->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)
                        ->getSource()->getOptionId($orderData['customer']['suffix'])
                );

                    $this->_logger->debug("suffix Saved - " . $customersuffix->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionId($orderData['customer']['suffix']));

            }
            // Professional category saving for customer
            if (isset($orderData['customer']['professional_category'])) {
                $customerData->setCustomAttribute(
                    'professional_category',
                    $professionalCategory->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)
                        ->getSource()
                        ->getOptionId($orderData['customer']['professional_category'])
                );

                    $this->_logger->debug("Professional Category saved - " .
                        $professionalCategory->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)
                            ->getSource()
                            ->getOptionId($orderData['customer']['professional_category']));

            }
            if (isset($orderData['customer']['primarySpecialty'])) {
                $customerData->setCustomAttribute(
                    'primaryspecialty',
                    $primaryspecialty->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)
                        ->getSource()
                        ->getOptionId($orderData['customer']['primarySpecialty'])
                );

                    $this->_logger->debug("primarySpecialty saved - " . $primaryspecialty->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionId($orderData['customer']['primarySpecialty']));


            }

            if (isset($orderData['customer']['student_type'])) {
                $customerData->setCustomAttribute(
                    'student_type',
                    $studentType->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)
                        ->getSource()
                        ->getOptionId($orderData['customer']['student_type'])
                );

                    $this->_logger->debug("student_type saved - " . $primaryspecialty->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionId($orderData['customer']['student_type']));


            }

            if (isset($orderData['customer']['profession'])) {
                $customerData->setCustomAttribute(
                    'student_type',
                    $profession->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)
                        ->getSource()
                        ->getOptionId($orderData['customer']['profession'])
                );

                    $this->_logger->debug("profession saved - " . $primaryspecialty->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionId($orderData['customer']['profession']));


            }
            if (isset($orderData['customer']['nameOfOrganization'])){
                $customerData->setCustomAttribute(
                    'nameoforganization',
                    $orderData['customer']['nameOfOrganization']
                );
            }
            if (isset($orderData['customer']['catalystConnect'])) {
                $customerData->setCustomAttribute(
                    'catalystconnect',
                    $orderData['customer']['catalystConnect']
                );
            }
            if (isset($orderData['customer']['catalystSOI'])) {
                $customerData->setCustomAttribute(
                    'catalystsoi', $orderData['customer']['catalystSOI']
                );
            }

            if (isset($orderData['customer']['nejmsoi'])) {
                $customerData->setCustomAttribute(
                    'nejmsoi', $orderData['customer']['nejmsoi']
                );
            }
            if (isset($orderData['customer']['nejmetoc'])) {
                $customerData->setCustomAttribute(
                    'nejmetoc', $orderData['customer']['nejmetoc']
                );
            }
            $customer->updateData($customerData);
            $customer->setConfirmation(null);
            $groupId = $this->getCustomerGroupIdByName('LEAD');
            if ($groupId) {
                $customer->setGroupId($groupId);
            }
            $customer->save();
            $this->createAddressForNewCustomer($order, $customer, $postParams);

                $this->_logger->debug("Saving Customer 1st time:-> ".json_encode( $customer->getData(), JSON_PRETTY_PRINT));

            $order->setCustomerId($customer->getId());
            $order->setCustomerIsGuest(0);
            return $customer;
        } catch(\Exception $e) {
            $this->_logger->debug("Create Customer Exception:-> ".$e->getMessage());
            $this->redirectToErrorPage();
            return true;
        }
    }

    public function createCustomerLoginTime($customerResponse)
    {
        $role = $this->eavConfig->getAttribute('customer', 'role');
        $customersuffix = $this->eavConfig->getAttribute('customer', 'customersuffix');
        $primaryspecialty = $this->eavConfig->getAttribute('customer', 'primaryspecialty');
        $professionalCategory = $this->eavConfig->getAttribute('customer', 'professional_category');
        $storeId=$this->storeResolver->getCurrentStoreId();
        $store = $this->storeManager->getStore($storeId);
        $websiteId = $store->getWebsiteId();
        try{
            $customer  = $this->customerFactory->create();
            $customer->setWebsiteId($websiteId);
            if (isset($customerResponse['Customer']['email'])) {
                $customer->setEmail($customerResponse['Customer']['email']);
            }
            if (isset($customerResponse['Customer']['firstName'])) {
                $firstname = $customerResponse['Customer']['firstName'];
            } else {
                $firstname= 'NA';
            }

            if ( isset($customerResponse['Customer']['lastName']) ) {
                $lastName = $customerResponse['Customer']['lastName'];
            } else {
                $lastName= 'NA';
            }

            if ( isset($customerResponse['Customer']['password']) ) {
                $password = $customerResponse['Customer']['password'];
                $hashedPassword = $this->encryptorInterface->getHash($password, true);
                $customer->setPasswordHash($hashedPassword);
            } else {
                $password= 'NA';
            }


            $customer->setFirstname( $firstname );
            $customer->setLastname( $lastName );
            $customerData = $customer->getDataModel();

            if (isset($customerResponse['Customer']['role'])) {
                $customerData->setCustomAttribute('role',$role->setStoreId(0)->getSource()->getOptionId($customerResponse['Customer']['role']));

                    $this->_logger->debug("Role saved - " . $role->setStoreId(0)->getSource()->getOptionId($customerResponse['Customer']['role']));

            }
            if (isset($customerResponse['Customer']['suffix'])) {
                $customerData->setCustomAttribute('customersuffix',$customersuffix->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionId($customerResponse['Customer']['suffix']));

                    $this->_logger->debug("suffix Saved - " . $customersuffix->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionId($customerResponse['Customer']['suffix']));

            }
            if (isset($customerResponse['Customer']['primarySpecialty'])) {

                    $this->_logger->debug("primarySpecialty saved - " . $primaryspecialty->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionId($customerResponse['Customer']['primarySpecialty']));

                $customerData->setCustomAttribute('primaryspecialty',$primaryspecialty->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionId($customerResponse['Customer']['primarySpecialty']));
            }
            if (isset($customerResponse['Customer']['professionalCategory'])) {

                    $this->_logger->debug("professionalCategory saved - " . $primaryspecialty->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionId($customerResponse['Customer']['professionalCategory']));

                $customerData->setCustomAttribute('professional_category',$professionalCategory->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionId($customerResponse['Customer']['professionalCategory']));
            }
            if (isset($customerResponse['Customer']['nameOfOrganization'])) {
                $customerData->setCustomAttribute('nameoforganization',$customerResponse['Customer']['nameOfOrganization']);
            }
            if (isset($customerResponse['Customer']['catalystConnect'])) {
                $customerData->setCustomAttribute('catalystconnect',$customerResponse['Customer']['catalystConnect']);
            }
            if (isset($customerResponse['Customer']['catalystSOI'])) {
                $customerData->setCustomAttribute('catalystsoi',$customerResponse['Customer']['catalystSOI']);
            }

            if (isset($customerResponse['Customer']['nejmSOI'])) {
                $customerData->setCustomAttribute('nejmsoi',$customerResponse['Customer']['nejmSOI']);
            }
            if (isset($customerResponse['Customer']['nejmTOC'])) {
                $customerData->setCustomAttribute('nejmetoc',$customerResponse['Customer']['nejmTOC']);
            }
            if (isset($customerResponse['Customer']['nejm.mediaAccess'])) {
                $customerData->setCustomAttribute('media_access',$customerResponse['Customer']['nejm.mediaAccess']);
            }
            if (isset($customerResponse['Customer']['country'])) {
                $customerData->setCustomAttribute('customer_default_country',$customerResponse['Customer']['country']);
            }
            if (isset($customerResponse['Customer']['ucid'])) {
                $customerData->setCustomAttribute('ucid',$customerResponse['Customer']['ucid']);
            }
            $customer->updateData($customerData);
            $customer->setConfirmation(null);
            // $groupId = $this->getCustomerGroupIdByName('general');
            // if($groupId){
            //     $customer->setGroupId($groupId);
            // }
            $customer->save();

                $this->_logger->debug("Saving Customer 1st time:-> ".json_encode( $customer->getData(), JSON_PRETTY_PRINT));

            return $customer;
        } catch(Exception $e) {
            return false;
        }

    }

    /**
     * @param $order
     * @param $customer
     * @param $postParams
     */
    public function createAddressForNewCustomer($order, $customer, $postParams){
        $billingAddress = false;
        $regionId = '';
        $regionName = '';
        $city = '';
        $vatId = '';
        $street = '';
        $countryId = '';
        if (isset($postParams['billingAddress']) && !empty($postParams['billingAddress'])) {
            $billingAddress = $postParams['billingAddress'];
            $regionId = isset($billingAddress['regionId'])?$billingAddress['regionId']:'';
            $regionName = isset($billingAddress['region'])?$billingAddress['region']:'';
            $city = isset($billingAddress['city'])?$billingAddress['city']:'';
            $vatId = isset($billingAddress['vatId'])?$billingAddress['vatId']:'';
            $street = isset($billingAddress['street'])?$billingAddress['street']:'';
            $postcode = isset($billingAddress['postcode'])?$billingAddress['postcode']:'';
            $countryId = isset($billingAddress['countryId']) ? $billingAddress['countryId'] : 'US';
        } else if ($order->getBillingAddress()) {
            $billingAddress = $order->getBillingAddress();
            $regionId = $billingAddress->getRegionId();
            $regionName = $billingAddress->getRegion();
            $city = $billingAddress->getCity();
            $vatId = $billingAddress->getVatId();
            $street = $billingAddress->getStreet();
            $postcode = $billingAddress->getPostcode();
            $countryId = $billingAddress->getCountryId();
        }
        if ($billingAddress) {
            $regionObj = $this->regionInterfaceFactory->create();
            $address = $this->addressDataFactory->create();
            $address->setFirstname($customer->getFirstname());
            $address->setLastname($customer->getLastname());
            $address->setStreet($street);
            $address->setCity($city);

            $address->setCountryId($countryId);
            $address->setPostcode($postcode);
            if($regionId){
                $address->setRegionId($regionId);
            }
            if($regionName && !$regionId){
                $region = $regionObj->setRegion($regionName);
                $address->setRegion($region);
            }
            if ($vatId) {
                $address->setVatId($vatId);
            }
            $address->setIsDefaultShipping(1);
            $address->setIsDefaultBilling(1);
            $address->setCustomerId($customer->getId());
            try {
                $this->addressRepository->save($address);
            } catch (\Exception $exception) {
                $this->_logger->debug("billingAddress Excpetion:-> ".$exception->getMessage());
            }

        }
    }

    /**
     * @param $name
     * @return mixed|string
     */
    public function getCustomerGroupIdByName($name){
        $group = $this->group->load($name, 'customer_group_code');
        if($group){
            return $group->getId();
        }
        return '';
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function redirectToErrorPage()
    {
        if ($this->_state->getAreaCode() !== Area::AREA_ADMINHTML) {
            $this->session->setWebApiCustomError(true);
            throw new \Magento\Framework\Exception\LocalizedException(
                __('External service failed, redirecting to fallback page.')
            );
        }
        //throw new StateException(__('Something went wrong. Please try later.'));
        return '';
    }

    /**
     * @param $order
     * @return string
     */
    public function getOrderItemBrand($order){
        $brand = 'Catalyst';
        foreach($order->getAllItems() as $item){
            $prod = $this->product->load($item->getProduct()->getId());
            if ($prod->getResource()->getAttribute('brand')->getFrontend()->getValue($prod) == 'NEJM') {
                $brand = $prod->getResource()->getAttribute('brand')->getFrontend()->getValue($prod);
            }
        }
        return $brand;
    }
    /**
     * @param $order
     * @param $postParams
     * @return mixed
     */
    public function getCustomerInfoFromOrder($order, $postParams)
    {
        /** @var $order \Magento\Sales\Model\Order */
        $billingAddress = $order->getBillingAddress();
        $productBrand = $this->getOrderItemBrand($order);
        $_piInformation = $this->checkoutSession->getPiInformationSession();
        if (!$_piInformation) {
            //$postParams = (array)$postParams;
            //$_piInformation = (array) $postParams['billingAddress']['extension_attributes'];
            //TODO extension_attributes are failing here. new way of mangento code fails.
            if (isset($postParams['billingAddress']['extension_attributes'])) {
                $_piInformation = (array)$postParams['billingAddress']['extension_attributes'];
            } elseif (isset($postParams['billingAddress']['customAttributes'])) {
                $customAttributes = [];
                foreach ($postParams['billingAddress']['customAttributes'] as $attribute) {
                    $customAttributes[$attribute['attribute_code']] = $attribute['value'];
                }
                $_piInformation = (array) $customAttributes;
            }
        }
        $piInformation = new \Magento\Framework\DataObject();
        $piInformation->setData($_piInformation);

        $country = $this->_countryFactory->create()->loadByCode($billingAddress->getCountryId());
        $regionCountry = $billingAddress->getCountryId();
        if($country->getData('iso3_code')){
            $regionCountry = $country->getData('iso3_code');
        }
        $customerPiInfo['customer'] = array();
        if($billingAddress->getFirstname() != NULL){
            $customerPiInfo['customer']['firstName'] = $billingAddress->getFirstname();
        }

        if($billingAddress->getLastname() != NULL){
            $customerPiInfo['customer']['lastName'] = $billingAddress->getLastname();
        }

        if($billingAddress->getEmail() != NULL){
            $customerPiInfo['customer']['email'] = $billingAddress->getEmail();
        }

        if($piInformation->getBaPassword() != NULL){
            $customerPiInfo['customer']['password'] = $piInformation->getBaPassword();
        }

        if($piInformation->getPiRole() != NULL){
            $customerPiInfo['customer']['role'] = $piInformation->getPiRole();
        }

        if($piInformation->getPiSuffix() != NULL){
            $customerPiInfo['customer']['suffix'] = $piInformation->getPiSuffix();
        }

        if($piInformation->getPiProfessionalCategory() != NULL){
            $customerPiInfo['customer']['professional_category'] = $piInformation->getPiProfessionalCategory();
        }

        if($piInformation->getPiProfession() != NULL){
            $customerPiInfo['customer']['profession'] = $piInformation->getPiProfession();
        }

        if($piInformation->getPiStudentType() != NULL){
            $customerPiInfo['customer']['student_type'] = $piInformation->getPiStudentType();
        }

        if($piInformation->getPiPrimaryspecialty() != NULL){
            $customerPiInfo['customer']['primarySpecialty'] = $piInformation->getPiPrimaryspecialty();
        }

        if($piInformation->getOrganization() != NULL){
            $customerPiInfo['customer']['nameOfOrganization'] = $piInformation->getOrganization();
        }

        if($regionCountry != NULL){
            $customerPiInfo['customer']['regionCountry'] = $regionCountry;
        }

        if($billingAddress->getCountryId() != NULL){
            $customerPiInfo['customer']['country'] = $billingAddress->getCountryId();
        }

        if($piInformation->getCustomerCatalystConnect() != NULL){
            $customerPiInfo['customer']['catalystConnect'] = (bool)$piInformation->getCustomerCatalystConnect();
        }

        if($piInformation->getCustomerCatalystSoi() != NULL){
            $customerPiInfo['customer']['catalystSOI'] = (bool)$piInformation->getCustomerCatalystSoi();
        }
        if($piInformation->getNejmSoi() != NULL){
            $customerPiInfo['customer']['nejmsoi'] = (bool)$piInformation->getNejmSoi();
        }
        if($piInformation->getNejmEtoc() != NULL){
            $customerPiInfo['customer']['nejmetoc'] = (bool)$piInformation->getNejmEtoc();
        }
        $customerPiInfo['product']['brand'] = $productBrand;
        return $customerPiInfo;
    }

    /**
     * @param $customer , $order
     * @param $order
     * @param $quote
     * @param $postParams
     * @return array
     */
    public function createCustomerPayload($customer, $order, $postParams){
        $orderData = $this->getCustomerInfoFromOrder($order, $postParams);
        $this->_logger->debug("ORDERDATA:-> ".json_encode( $orderData, JSON_PRETTY_PRINT));
        $role = $this->eavConfig->getAttribute('customer', 'role');
        $primaryspecialty = $this->eavConfig->getAttribute('customer', 'primaryspecialty');
        $customersuffix = $this->eavConfig->getAttribute('customer', 'customersuffix');
        $payload['customer'] = array();
        if($customer->getData('ucid')){
            $payload['customer']['ucid'] = $customer->getData('ucid');
        }
        $payload['customer']['customerId'] = $customer->getData('entity_id');
        $payload['customer']['firstName'] = $customer->getData('firstname');
        $payload['customer']['lastName'] = $customer->getData('lastname');
        $payload['customer']['email'] = $customer->getData('email');
        if(isset($orderData['customer']['role'])){
            $payload['customer']['role'] = $orderData['customer']['role'];
        } else if($customer->getData('role')) {
            $payload['customer']['role'] = $role->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionText($customer->getData('role'));
        }
        if (isset($orderData['customer']['suffix'])) {
            $payload['customer']['suffix'] = $orderData['customer']['suffix'];
        } elseif($customer->getData('suffix')) {
            $payload['customer']['suffix'] = $customersuffix->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionText($customer->getData('customersuffix'));
        }
        $payload['customer']['password'] = isset($orderData['customer']['password']) ? $orderData['customer']['password'] : 'NA';
        if(isset($orderData['customer']['primarySpecialty'])){
            $payload['customer']['primarySpecialty'] = $orderData['customer']['primarySpecialty'];
        }
        if(isset($orderData['customer']['professional_category'])){
            $payload['customer']['professionalCategory'] = $orderData['customer']['professional_category'];
        }
        if(isset($orderData['customer']['profession'])){
            $payload['customer']['profession'] = $orderData['customer']['profession'];
        }
        if(isset($orderData['customer']['student_type'])){
            $payload['customer']['studentType'] = $orderData['customer']['student_type'];
        }
        if (isset($orderData['customer']['nameOfOrganization'])) {
            $payload['customer']['nameOfOrganization'] = isset($orderData['customer']['nameOfOrganization']) ? $orderData['customer']['nameOfOrganization'] : '';
        }
        $alerts = [];
        $nejmtoc = [];
        if (isset($orderData['product']['brand'])
            && $orderData['product']['brand'] == 'NEJM'
        ) {
            if ($customer->getData('nejmsoi')) {
                $alerts[] = ['alert' => 'nejmGeneralInformation'];
            }
            if ($customer->getData('nejmetoc')) {
                $alerts[] = ['alert' => 'nejmTOC'];
            }

            if (isset($orderData['customer']['country'])
                && $orderData['customer']['country'] == 'US') {
                if ($customer->getId()) {
                    if ($customer->getProfessionalCategory() && $customer->getProfessionalCategory() == 'RES') {
                        $alerts[] = ['alert' => 'nejmWeeklyResidentBriefing'];
                    }
                    if ($customer->getPrimaryspecialty()) {
                        $psname = $primaryspecialty->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionText($customer->getData('primaryspecialty'));
                        $specialityKeyName = $this->speciality->getNejmSpecialityKeyName($psname);
                        if ($specialityKeyName) {
                            $alerts[] = ['alert' => $specialityKeyName];
                        }
                    }
                } else {
                    if (isset($payload['customer']['professionalCategory']) && $payload['customer']['professionalCategory'] == 'RES') {
                        $alerts[] = ['alert' => 'nejmWeeklyResidentBriefing'];
                    }
                    if (isset($payload['customer']['primarySpecialty'])) {
                        $specialityKeyName = $this->speciality->getNejmSpecialityKeyName($payload['customer']['primarySpecialty']);
                        if ($specialityKeyName) {
                            $alerts[] = ['alert' => $specialityKeyName];
                        }
                    }
                }
            }

        } else {
            if ($customer->getData('catalystconnect')) {
                $alerts[] = ['alert' => 'catalystConnect'];
            }
            if ($customer->getData('catalystsoi')) {
                $alerts[] = ['alert' => 'catalystSOI'];
            }
        }
        $payload['customer']['emailPreferences'] = $alerts;
        if (isset($orderData['customer']['country'])) {
            $payload['customer']['country'] = $orderData['customer']['country'];
        }
        $payload['customer']['insightsCouncilMember'] = (bool)$customer->getData('insightscouncilmember');
        return $payload;
    }


    /**
     * @param $customer , $order
     * @param $order
     * @param $quote
     * @param $postParams
     * @return array
     */
    public function createCustomerPayloadForAdmin($customer, $order, $postParams){
        $orderData['customer'] = $postParams['order']['account'];
        $customerData = $customer->getDataModel();
        $billingAddress = $customer->getDefaultBillingAddress();
        $role = $this->eavConfig->getAttribute('customer', 'role');
        $customersuffix = $this->eavConfig->getAttribute('customer', 'customersuffix');
        $primaryspecialty = $this->eavConfig->getAttribute('customer', 'primaryspecialty');
        if(isset($orderData['customer']['role'])){
            $customerData->setCustomAttribute('role',$orderData['customer']['role']);
        }
        if(isset($orderData['customer']['customersuffix'])){
            $customerData->setCustomAttribute('customersuffix', $orderData['customer']['customersuffix']);
        }
        if(isset($orderData['customer']['primaryspecialty'])){
            $customerData->setCustomAttribute('primaryspecialty', $orderData['customer']['primaryspecialty']);
        }
        if(isset($orderData['customer']['nameoforganization'])){
            $customerData->setCustomAttribute('nameoforganization', $orderData['customer']['nameoforganization']);
        }

        if(isset($orderData['customer']['catalystconnect'])){
            $customerData->setCustomAttribute('catalystconnect', $orderData['customer']['catalystconnect']);
        }
        if(isset($orderData['customer']['catalystsoi'])){
            $customerData->setCustomAttribute('catalystsoi', $orderData['customer']['catalystsoi']);
        }
        if(isset($orderData['customer']['nejmetoc'])){
            $customerData->setCustomAttribute('nejmetoc', $orderData['customer']['nejmetoc']);
        }
        if(isset($orderData['customer']['nejmsoi'])){
            $customerData->setCustomAttribute('nejmsoi', $orderData['customer']['nejmsoi']);
        }


        if($billingAddress->getCountryId()){
            $customerData->setCustomAttribute('customer_default_country', $billingAddress->getCountryId());
        }

        $customer->updateData($customerData);
        $customer->setConfirmation(null);
        $customer->save();
        $payload['customer'] = array();
        $professionalCat = '';
        $payload['customer']['customerId'] = $customer->getData('entity_id');
        $payload['customer']['firstName'] = $customer->getData('firstname');
        $payload['customer']['lastName'] = $customer->getData('lastname');
        $payload['customer']['email'] = $customer->getData('email');
        $payload['customer']['role'] = $role->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionText($customer->getData('role'));
        $payload['customer']['suffix'] = $customersuffix->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionText($customer->getData('customersuffix'));
        $payload['customer']['password'] = isset($orderData['customer']['password']) ? $orderData['customer']['password'] : 'NA';
        if($customer->getData('primaryspecialty')){
            $payload['customer']['primarySpecialty'] = $primaryspecialty->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionText($customer->getData('primaryspecialty'));
        }
        if(isset($payload['customer']['role']) && isset($payload['customer']['suffix'])){
            $professionalCat = $this->getProfessionCategory($payload['customer']['role'], $payload['customer']['suffix']);
            $payload['customer']['professionalCategory'] = $professionalCat;
            $customer->setProfessionalCategory($professionalCat)->save();
        }
        $payload['customer']['nameOfOrganization'] = $customer->getData('nameoforganization');
        $alerts = [];
        if (isset($orderData['product']['brand'])
            && $orderData['product']['brand'] == 'NEJM'
        ) {
            if ($customer->getData('nejmsoi')) {
                $alerts[] = ['alert' => 'nejmGeneralInformation'];
            }
            if ($customer->getData('nejmetoc')) {
                $alerts[] = ['alert' => 'nejmTOC'];
            }
            if (isset($orderData['customer']['country'])
                && $orderData['customer']['country'] == 'US') {

                if (isset($payload['customer']['professionalCategory']) && $payload['customer']['professionalCategory'] == 'RES') {
                    $alerts[] = ['alert' => 'nejmWeeklyResidentBriefing'];
                }
                if (isset($payload['customer']['primarySpecialty'])) {
                    $specialityKeyName = $this->speciality->getNejmSpecialityKeyName($payload['customer']['primarySpecialty']);
                    if ($specialityKeyName) {
                        $alerts[] = ['alert' => $specialityKeyName];
                    }
                }
            }

        } else {
            if ($customer->getData('catalystconnect')) {
                $alerts[] = ['alert' => 'catalystConnect'];
            }
            if ($customer->getData('catalystsoi')) {
                $alerts[] = ['alert' => 'catalystSOI'];
            }
        }
        $payload['customer']['emailPreferences'] = $alerts;



        $billingAddress = $customer->getDefaultBillingAddress();
        $payload['customer']['country'] = $billingAddress->getCountryId();
        $payload['customer']['insightsCouncilMember'] = (bool)$customer->getData('insightscouncilmember');
        return $payload;
    }

    /**
     * @param string
     * @return string
     */
    public function getProfessionCategory($role, $suffix){
        if ($role == 'STU') {
            return 'STU';
        } else if ($role == 'RSD') {
            return 'RES';
        } else if ($suffix == 'None' || $suffix == 'Other' || $suffix == 'MPH' || $suffix == 'PhD' || $suffix == 'MD/PhD') {
            return 'OTH';
        }
        else if ($suffix == 'MD' || $suffix == 'DO' || $suffix == 'MBBS') {
            return 'PHY';
        }
        else if ($suffix == 'PA-C') {
            return 'PA';
        }
        else if ($suffix == 'NP/ApN') {
            return 'NP';
        }
        else if ($suffix == 'RN') {
            return 'NUR';
        }
        else {
            return 'OTH';
        }
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     * @return string
     */
    public function updateCustomerGroupAfterSubscribe($customer){
        $groupId = $this->getCustomerGroupIdByName('general');
        if($groupId){
            $customer->setGroupId($groupId);
            $customer->setConfirmation(null);
            $customer->save();
        }
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getWebsiteId(){
        $storeId=$this->storeResolver->getCurrentStoreId();
        $store = $this->storeManager->getStore($storeId);
        return $store->getWebsiteId();
    }
    public function getProductBrandFromOrder($order){
        foreach ($order->getAllItems() as $item) {
            $prod = $this->product->load($item->getProduct()->getId());
            if ($prod->getData('brand')) {
                return $prod->getAttributeText('brand');
            }
        }
    }
}
