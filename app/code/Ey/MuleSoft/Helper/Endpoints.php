<?php
namespace Ey\MuleSoft\Helper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Area;

class Endpoints extends \Magento\Framework\App\Helper\AbstractHelper{

	/**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;
	/**
     * Endpoints constructor.
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\App\State $state
     */
	public function __construct(
		Context $context,
        \Magento\Catalog\Model\Product $product,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\State $state
    ){
        $this->product = $product;
        $this->checkoutSession = $checkoutSession;
        $this->_state = $state;
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getCustomerEndpoint()
    {
	    return $this->scopeConfig->getValue('mulesoft/api/mulesoft_customer_endpoint', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getCustomerPostEndpoint()
    {
    	$brand = $this->getCheckoutBrand();
    	if ($brand) {
	        return $this->scopeConfig->getValue('mulesoft/api/mulesoft_customer_endpoint', \Magento\Store\Model\ScopeInterface::SCOPE_STORE).'?brand='.$brand;
	    }
    }

    /**
     * @return string
     */
    public function getCheckoutBrand()
    {
        if ($this->_state->getAreaCode() == Area::AREA_ADMINHTML) {
            return 'catalyst';
        }
    	$allBrands = [];
    	$brandParam = 'nejmgroup';
    	$quote = $this->checkoutSession->getQuote();
        if ($quote->hasItems()) {
        	foreach($quote->getAllVisibleItems() as $item){
        		$productId = $item->getProductId();
                $product = $this->product->load($productId);
                $allBrands[] = strtolower($product->getResource()->getAttribute('brand')->getFrontend()->getValue($product));
        	}
        }
    	if (!empty($allBrands)) {
    		if (in_array('nejm', $allBrands) && in_array('catalyst', $allBrands)) {
    			$brandParam = 'nejmgroup';
    		} else if(in_array('nejm', $allBrands)) {
    			$brandParam = 'nejm';
    		} else if(in_array('catalyst', $allBrands)) {
    			$brandParam = 'catalyst';
    		}
    	}
    	return $brandParam;
    }
}
