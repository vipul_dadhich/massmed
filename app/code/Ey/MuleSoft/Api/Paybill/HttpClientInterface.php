<?php

namespace Ey\MuleSoft\Api\Paybill;

/**
 * Create Quote by paybill api service
 */
interface HttpClientInterface
{
    /**
    * @return mixed
    */
    public function getRequestMethod();

    /**
    * @return string
    */
    public function getApiBaseUrl();

    /**
    * @param array()
    * @return object
    */
    public function request($payload, $endpoint, $baseApiUrl = '', $headers = array());
}