<?php

namespace Ey\URLParams\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup,ModuleContextInterface $context){
        $installer = $setup;
        $installer->startSetup();
        
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
			
			// Add pi_primaryspecialty field to quote, sales_order tables  
			
    		$installer->getConnection()->addColumn(
            	$installer->getTable('quote'),
            	'bill_me_later',
            	[
                	'type' => 'text',
                	'nullable' => true,
                	'comment' => 'Bill Me Later',
            	]
        	);

        	
        }
		
        $setup->endSetup();
    }
}