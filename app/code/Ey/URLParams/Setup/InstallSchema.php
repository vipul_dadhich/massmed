<?php

namespace Ey\URLParams\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
    
        $installer = $setup;
        $installer->startSetup();

		
		// Add back_to_article field to quote, sales_order tables        
        if ($installer->getConnection()->tableColumnExists('quote', 'back_to_article') === false) {
        	$installer->getConnection()->addColumn(
        	    $installer->getTable('quote'),
        	    'back_to_article',
        	    [
        	        'type' => 'text',
        	        'nullable' => true,
        	        'comment' => 'Back To Article',
        	    ]
        	);
        }
        
        if ($installer->getConnection()->tableColumnExists('sales_order', 'back_to_article') === false) {
        	$installer->getConnection()->addColumn(
        	    $installer->getTable('sales_order'),
        	    'back_to_article',
        	    [
        	        'type' => 'text',
        	        'nullable' => true,
        	        'comment' => 'Back To Article',
        	    ]
        	);
        }
        
        
        
        
        $setup->endSetup();
    }
}