<?php
namespace Ey\URLParams\Plugin;

use \Magento\UrlRewrite\Controller\Router as MagentoRouter;
use \Magento\UrlRewrite\Model\UrlFinderInterface;

class Router extends MagentoRouter
{

public function __construct(
    \Magento\Framework\App\ActionFactory $actionFactory,
    \Magento\Framework\UrlInterface $url,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Framework\App\ResponseInterface $response,
    UrlFinderInterface $urlFinder
)
{
    parent::__construct($actionFactory, $url, $storeManager, $response, $urlFinder);
}

/**
 * @param \Magento\Framework\App\RequestInterface $request
 * @param string $url
 * @param int $code
 * @return \Magento\Framework\App\ActionInterface
 */
protected function redirect($request, $url, $code)
{
    if(!empty($request->getParams())){
        $urlParams = $this->formatParams($request->getParams());
        if($urlParams != false){
            $url = $url.$urlParams;
        }
    }
    $this->response->setRedirect($url, $code);
    $request->setDispatched(true);
    return $this->actionFactory->create('Magento\Framework\App\Action\Redirect');
}

/**
 * @param $params
 * @return bool|string
 */
public function formatParams($params){
        $paramsArray = [];
        foreach($params as $key => $value){
            $paramsArray[] = $key.'='.$value;
        }
        if(!empty($paramsArray)){
            $paramstring = '?'.implode('&',$paramsArray);
            return $paramstring;
        }
        return false;
}
}