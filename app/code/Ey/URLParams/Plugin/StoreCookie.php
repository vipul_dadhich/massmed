<?php
namespace Ey\URLParams\Plugin;

use Magento\Store\Api\StoreCookieManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Model\StoreIsInactiveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Ey\URLParams\Helper\Data;
use \InvalidArgumentException;

/**
 * Class StoreCookie
 */
class StoreCookie
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var StoreCookieManagerInterface
     */
    protected $storeCookieManager;

    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @param StoreManagerInterface $storeManager
     * @param StoreCookieManagerInterface $storeCookieManager
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        StoreCookieManagerInterface $storeCookieManager,
        StoreRepositoryInterface $storeRepository,
        Data $helper
    ) {
        $this->storeManager = $storeManager;
        $this->storeCookieManager = $storeCookieManager;
        $this->storeRepository = $storeRepository;
        $this->helper = $helper;
    }

    /**
     * Delete cookie "store" if the store (a value in the cookie) does not exist or is inactive
     *
     * @param \Magento\Framework\App\FrontController $subject
     * @param \Magento\Framework\App\RequestInterface $request
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeDispatch(
        \Magento\Framework\App\FrontController $subject,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $back_to_article = $request->getParam('url');
        $doi = $request->getParam('doi');
        $agreement = $request->getParam('agreement');
        $product = $request->getParam('product');

		if (strval($back_to_article)) {
			$this->helper->setCustomCookie('back_to_article', base64_encode($back_to_article), 3600);
        }

        if (strval($doi)) {
			$this->helper->setCustomCookie('doi', $doi, 3600);
        }

        if (strval($agreement)) {
			$this->helper->setCustomCookie('agreement', $agreement, 3600);
        }

        if(!is_numeric($product) && strval($product)){
            $this->helper->setCustomCookie('product', $product, 3600);
        }
    }
}
