<?php

namespace Ey\URLParams\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	/**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
    */
	public function __construct(
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
		\Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
		\Magento\Framework\Session\SessionManagerInterface $sessionManager,
        \Magento\Framework\App\Helper\Context $context
    )
    {
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
        return parent::__construct($context);
    }
    
    
    public function setCustomCookie($cookiename,$value, $duration = 86400)
    {
        $metadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration($duration)
            ->setPath($this->sessionManager->getCookiePath())
            ->setDomain($this->sessionManager->getCookieDomain())
            ->setHttpOnly(true)
            ->setSecure(true);

        $this->cookieManager->setPublicCookie(
            $cookiename,
            $value,
            $metadata
        );
    }
    
    public function getCustomCookie($cookiename)
    {
        return $this->cookieManager->getCookie($cookiename);
    }
    
    public function deleteCustomCookie($cookiename)
    {
        return $this->cookieManager->deleteCookie($cookiename);
    }

}