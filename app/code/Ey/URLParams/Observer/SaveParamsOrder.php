<?php

namespace Ey\URLParams\Observer;

class SaveParamsOrder implements \Magento\Framework\Event\ObserverInterface
{
	
	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		$quote = $observer->getQuote();
		$quote_bta_val = $quote->getData('back_to_article');
       	$order = $observer->getOrder();

       	if(isset($quote_bta_val)) {
            $order->setData('back_to_article', $quote->getData('back_to_article'));
        }

		return $this;
	}
}