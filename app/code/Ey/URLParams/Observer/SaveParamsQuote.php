<?php

namespace Ey\URLParams\Observer;

class SaveParamsQuote implements \Magento\Framework\Event\ObserverInterface{
	public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Ey\URLParams\Helper\Data $helper
    ){
        $this->checkoutSession = $checkoutSession;
        $this->helper = $helper;
    }
	
	public function execute(\Magento\Framework\Event\Observer $observer){
		$quote = $this->checkoutSession->getQuote();
		
		$back_to_article_cookie_val = $this->helper->getCustomCookie('back_to_article');
		if(strval($back_to_article_cookie_val) != ""){
			$back_to_article = base64_decode($back_to_article_cookie_val);
			if($quote->getId() && $back_to_article !== ""){
				$quote->setData('back_to_article',$back_to_article);
				$quote->save();
				$this->helper->deleteCustomCookie('back_to_article');
			}
		}
		$bill_me_later = $this->helper->getCustomCookie('bill_me_later');
		if(strval($bill_me_later) != ""){
			if($quote->getId()){
				$quote->setData('bill_me_later',$bill_me_later);
				$quote->save();
				$this->helper->deleteCustomCookie('bill_me_later');
			}
		}
		return $this;
	}
}