<?php
/**
 * Copyright: DSTax, LLC, 2020
 * The DSTAX SOFTWARE is protected by copyright laws and international treaty
 * provisions.  You may not reverse engineer, decompile, or disassemble the
 * DSTAX SOFTWARE.
 *
 * Classes for calling Thomson-Reuters ONESOURCE Indirect Tax Determination
 * (OSITD) services.
 *
 * There are two primary services:
 * Simple Tax Service
 * Tax Calculation Service
 *
 * The Simple Tax Service is older and Thomson-Reuters is encouraging customers to
 * move to the Tax Calculation Service.
 *
 * @link www.dstax.com
 */

namespace DSTax\OSITD;

use DSTax\OSITD\Service\CommonTrait;
use DSTax\OSITD\Service\SimpleTaxService;
use DSTax\OSITD\Service\TaxCalculationService;

/**
 * For making tax calls to Thomson Reuters ONESOURCE indirect tax
 * determination service.  Both the 'Simple Tax Service' and the 'Tax
 * Calculation Service' are
 * supported.
 */
class OSITD
{
    use CommonTrait;

    /**
     * @var null
     */
    protected $taxService;

    /**
     * OSITD constructor.
     */
    public function __construct()
    {
        $this->taxService = null;
    }

    /**
     * Return an empty config dictionary.
     *
     * @return array
     */
    public function emptyConfigurables()
    {
        return array(
            'AmountType' => null,
            'CallingClient' => null,
            'CallingSource' => null,
            'CallingSystemNumber' => null,
            'CallingUser' => null,
            'CompanyRole' => null,
            'CurrencyCode' => null,
            'DbVersion' => null,
            'DocumentType' => null,
            'ERPVersion' => null,
            'ExternalCompanyId' => null,
            'HostSystemNumber' => null,
            'Incoterms' => null,
            'IntegrationVersion' => null,
            'Password' => null,
            'PointOfTitleTransfer' => null,
            'PostToAudit' => null,
            'ProductCode_Discount' => null,
            'ProductCode_Shipping' => null,
            'SDKVersion' => null,
            'ShipFromAddress' => null,
            'ShipFromCity' => null,
            'ShipFromCountry' => null,
            'ShipFromGeoCode' => null,
            'ShipFromPostalCode' => null,
            'ShipFromRegion' => null,
            'UOM' => null,
            'Username' => null,
        );
    }

    /**
     * Return an empty transaction dictionary.
     *
     * @return array
     */
    public function emptyTransaction()
    {
        return array(
            'addresses' => array(
                'BillToAddress' => array(
                    'Country' => null,
                    'Region' => null,
                    'City' => null,
                    'PostalCode' => null,
                    'GeoCode' => null,
                ),
                'ShipToAddress' => array(
                    'Country' => null,
                    'Region' => null,
                    'City' => null,
                    'PostalCode' => null,
                    'GeoCode' => null,
                    'PartnerNumber' => null,
                    'PartnerName' => null,
                ),
            ),
            'carrier_code' => null,
            'carrier_method' => null,
            'customer_vat' => null,
            'items' => array(
                array(
                    'discount' => null,
                    'line' => null,
                    'quantity' => null,
                    'sku' => null,
                    'tax_class' => null,
                    'unit_price' => null,
                    'unit_shipping' => null,
                ),
            ),
            'order_number' => null,
            'product_codes' => null,
            'total_shipping' => null,
            'total_subtotal' => null,
        );
    }

    /**
     * Get a property.
     *
     * @param string $key name of property
     *
     * @return null
     */
    public function get($key)
    {
        return $this->taxService->$key;
    }

    /**
     * Make a tax call to OSITD with the supplied transaction data.
     *
     * @param array $transaction data for tax call
     *
     * @return object
     */
    public function getTax($transaction)
    {
        //$this->check_ositd_params($transaction);
        return $this->taxService->getTax($transaction);
    }

    /**
     * Set a property.
     *
     * @param string $key   name of property
     * @param mixed  $value value of property
     *
     * @return null
     */
    public function set($key, $value)
    {
        $this->taxService->set($key, $value);
    }

    /**
     * Use Thomson-Reuters Simple Tax Service.
     *
     * @return null
     */
    public function useSimpleTaxService()
    {
        $this->taxService = new SimpleTaxService();
    }

    /**
     * Use Thomson-Reuters Tax Calculation Service.
     *
     * @return null
     */
    public function useTaxCalculationService()
    {
        $this->taxService = new TaxCalculationService();
    }
}
