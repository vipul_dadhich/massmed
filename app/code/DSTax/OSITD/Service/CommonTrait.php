<?php

namespace DSTax\OSITD\Service;

use Exception;

/**
 * trait Common
 * @package DSTax\OSITD
 */
trait CommonTrait
{
    /**
     * Check that arguments required for the OSITD tax call exist and are the
     * expected type.
     */
    public function check_ositd_params($data)
    {
        $required = array(
            'addresses'=> array(
                // 'BillToAddress'=> array(
                //     'Country'=> 'string',
                //     'Region'=> 'string',
                //     'City'=> 'string',
                //     'PostalCode'=> 'string',
                // ),
                'ShipToAddress'=> array(
                    'Country'=> 'string',
                    'Region'=> 'string',
                    'City'=> 'string',
                    'PostalCode'=> 'string',
                ),
            ),
            'items'=> [
                array(
                    'line'=> 'string',
                    'sku'=> 'string',
                    'quantity'=> 'integer',
                    // 'unit_price'=> 'double',
                    // 'discount'=> 'double',
                    // 'unit_shipping'=> 'double',
                    // 'tax_class'=> 'string',
                )
            ],
            // 'order_number'=> 'string',
            // 'product_codes'=> 'array',
            'total_shipping'=> 'double',
            'total_subtotal'=> 'double',
        );

        if (!function_exists('type_check_key_value')) {
            function type_check_key_value($arguments, $required)
            {
                // check for required fields
                foreach ($required as $key => $value) {
                    // check that key is in arguments
                    if (!array_key_exists($key, $arguments)) {
                        // return HTTP 400 bad request error
                        throw new BadRequestException(
                            sprintf('Required parameter %s missing', $key)
                        );
                    }
                    if (gettype($value) == 'string') {
                        // check type of arguments[key]
                        if (gettype($arguments[$key]) != $value) {
                            if (gettype($arguments[$key]) == 'string') {
                                // try converting argument to required type
                                try {
                                    settype($arguments[$key], $value);
                                } catch (Exception $e) {
                                    // return HTTP 400 bad request error
                                    throw new BadRequestException(
                                        sprintf('Unsupported value for %s', $key)
                                    );
                                }
                            }
                        }
                    } else {
                        // check type and call check on arguments[key]
                        if (gettype($arguments[$key]) == gettype($required[$key])) {
                            type_check($arguments[$key], $required[$key]);
                        } else {
                            // return HTTP 400 bad request error
                            throw new BadRequestException(
                                sprintf('Unsupported value for %s', $key)
                            );
                        }
                    }
                }
            }
        }

        if (!function_exists('type_check')) {
            function type_check($arguments, $required)
            {
                // check arguments for required fields and their expected data types
                if (gettype(array_keys($required)[0]) == 'string') {
                    type_check_key_value($arguments, $required);
                }
                if (gettype(array_keys($required)[0]) == 'integer') {
                    // support variable number of identical key-value stores for items
                    foreach ($arguments as $argument) {
                        type_check_key_value($argument, $required[0]);
                    }
                }
            }
        }

        type_check($data, $required);
    }

    /**
     * WSDL URL for Simple Tax Service
     *
     * @return string
     */
    public function simpleTaxServiceWSDL()
    {
        return ('https://onesource-idt-det-uat-ws.hostedtax.thomsonreuters.com'
            . '/sabrix/services/taxservice/2009-12-20/taxservice?wsdl');
    }


    /**
     * WSDL URL for Tax Calculation Service
     *
     * @return string
     */
    public function taxCalculationServiceWSDL()
    {
        return ('https://onesource-idt-det-uat-ws.hostedtax.thomsonreuters.com'
            . '/sabrix/services/taxcalculationservice/2011-09-01'
            . '/taxcalculationservice?wsdl');
    }


    /**
     * Return the current UTC time plus minutes.
     *
     * @param number $minutes return time $minutes into the future
     *
     * @return string
     */
    public function utcTime($minutes=0)
    {
        return gmdate(
            'Y-m-d\TH:i:s\Z', gmdate('U') + 60 * $minutes
        );
    }
}
