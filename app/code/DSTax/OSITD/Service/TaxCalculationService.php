<?php

namespace DSTax\OSITD\Service;

/**
 * For making tax calls to Thomson Reuters ONESOURCE Tax Calculation Service.
 *
 * Class TaxCalculationService
 * @package DSTax\OSITD
 */
class TaxCalculationService
{
    use CommonTrait;

    public function __construct()
    {
        $this->wsdl = $this->taxCalculationServiceWSDL();
    }


    /**
     * Return configurable item if set, or default value if it isn't.
     *
     * @param string $name name of configurable item
     *
     * @return mixed value of configurable item
     */
    public function config($name)
    {
        if (isset($this->configurables[$name])) {
            return $this->configurables[$name];
        } else {
            return $this->defaultConfigurables($name);
        }
    }

    /**
     * Return default configurables dictionary or default value of named
     * configurable.
     *
     * @param string $name name of configurable item
     *
     * @return mixed value of configurable item or array of all items
     */
    public function defaultConfigurables($name=null)
    {
        $config = [
            'CallingClient' => null,
            'CallingSource' => null,
            'CallingSystemNumber' => null,
            'CallingUser' => null,
            'CompanyRole' => null,
            'CurrencyCode' => null,
            'DbVersion' => null,
            'ERPVersion' => null,
            'ExternalCompanyId' => null,
            'HostSystemNumber' => null,
            'Incoterms' => null,
            'IntegrationVersion' => null,
            'Password' => null,
            'PointOfTitleTransfer' => null,
            'PostToAudit' => null,
            'ProductCode_Discount' => null,
            'ProductCode_Shipping' => null,
            'SDKVersion' => null,
            'ShipFromAddress' => null,
            'ShipFromCity' => null,
            'ShipFromCountry' => null,
            'ShipFromGeoCode' => null,
            'ShipFromPostalCode' => null,
            'ShipFromRegion' => null,
            'UOM' => null,
            'Username' => null,
        ];
        if (isset($name)) {
            return $config[$name];
        } else {
            return $config;
        }
    }

    /**
     * Call the Tax Calculation Service with the supplied transaction data.
     *
     * @param array $transaction tax data for a transaction
     *
     * @return object
     */
    public function getTax($transaction)
    {
        // save XML request for logging
        $this->xml_request = $this->taxRequest($transaction)->asXML();
        // create empty response
        $taxResponse = [
            'TotalTaxAmount'=> 0.0,
            'TaxDetails'=> [],
        ];
        try {
            $soapClient = new \soapClient(
                $this->wsdl,
                array('trace' => true,
                    'cache_wsdl' => WSDL_CACHE_NONE,
                    'exceptions'=>true)
            );
            $soapClient->__setLocation(str_replace('?wsdl', '', $this->wsdl));
            $soapClient->__setSoapHeaders($this->security());
            $soapResponse = $soapClient->CalculateTax(
                new \SoapVar($this->xml_request, XSD_ANYXML)
            );
            // save XML response for logging
            $this->xml_response = $soapClient->__getLastResponse();
            // fill in tax response
            if ($soapResponse->OUTDATA->REQUEST_STATUS->IS_SUCCESS) {
                $invoice = $soapResponse->OUTDATA->INVOICE;
                $taxResponse['TotalTaxAmount']
                    = floatval($invoice->TOTAL_TAX_AMOUNT);
                if (is_array($invoice->LINE)) {
                    // $invoice->LINE is an array when there are multiple lines
                    $lines = $invoice->LINE;
                } else {
                    // $invoice->LINE is a stdClass when there is one line
                    $lines = array($invoice->LINE);
                }
                foreach ($lines as $line) {
                    // skip freight and discount lines
                    if (!property_exists($line, "RELATED_LINE_NUMBER")) {
                        $taxResponse['TaxDetails'][] = [
                            'TaxRate' => $line->TAX_SUMMARY->TAX_RATE,
                            'TaxAmount' => $line->TOTAL_TAX_AMOUNT,
                        ];
                    }
                    // if (is_array($line->TAX)) {
                    //     // $line->TAX is an array when there are multiple authorities
                    //     $authorities = $line->TAX;
                    // } else {
                    //     // $line->TAX is a stdClass when there is one authoritie
                    //     $authorities = array($line->TAX);
                    // }
                    // foreach ($authorities as $authority) {
                    //     $taxResponse['TaxDetails'][] = [
                    //         'Authority' => $authority->AUTHORITY_NAME,
                    //         'TaxRate' => 100 * $authority->TAX_RATE,
                    //         'TaxAmount' => $authority->TAX_AMOUNT->AUTHORITY_AMOUNT,
                    //     ];
                    // }
                }
            }
        } catch (\SoapFault $exception) {
            $this->exception = $exception->getMessage();
            $taxResponse = $this->exception;
            $this->response = $soapResponse;
            //require 'vendor/psy/psysh/bin/psysh';
            //eval(\Psy\sh());
        }
        return $taxResponse;
    }

    /**
     * Add an invoice to a tax calculation request.
     *
     * @param SimpleXMLElement $inData INDATA XML
     * @param array            $data   data for tax call
     *
     * @return null
     */
    public function invoice($inData, $data)
    {
        $invoice = $inData->addChild('INVOICE');
        if ($data['addresses']['BillToAddress'] != null) {
            $bill_to_address = $data['addresses']['BillToAddress'];
            $billTo = $invoice->addChild('BILL_TO');
            $billTo->addChild('COUNTRY', $bill_to_address['Country']);
            if (isset($bill_to_address['Province'])) {
                $billTo->addChild('PROVINCE', $bill_to_address['Province']);
            } else {
                $billTo->addChild('STATE', $bill_to_address['Region']);
            }

            $billTo->addChild('CITY', $bill_to_address['City']);
            $billTo->addChild('POSTCODE', $bill_to_address['PostalCode']);
            if ($bill_to_address['GeoCode']) {
                $billTo->addChild('GEOCODE', $bill_to_address['GeoCode']);
            }
        }
        $invoice->addChild('CALCULATION_DIRECTION', 'F');
        $invoice->addChild('CURRENCY_CODE', $this->config('CurrencyCode'));
        $ship_to_address = $data['addresses']['ShipToAddress'];
        if ($ship_to_address['PartnerName']) {
            $invoice->addChild(
                'CUSTOMER_NAME', $ship_to_address['PartnerName']
            );
        }
        if ($ship_to_address['PartnerNumber']) {
            $invoice->addChild(
                'CUSTOMER_NUMBER', $ship_to_address['PartnerNumber']
            );
        }
        $invoice->addChild('INVOICE_DATE', gmdate('Y-m-d'));
        $invoice->addChild('IS_AUDITED', $this->config('PostToAudit'));
        if ($this->config('PointOfTitleTransfer')) {
            $invoice->addChild(
                'POINT_OF_TITLE_TRANSFER',
                substr($this->config('PointOfTitleTransfer'), 0, 1)
            );
        }
        $invoice->addChild('TRANSACTION_TYPE', 'GS');
        $invoice->addChild('INVOICE_NUMBER', $data['order_number']);
        $invoice->addChild('UNIQUE_INVOICE_NUMBER', $data['order_number']);
        // Registration section for VAT number
        $registrations = $invoice->addChild('REGISTRATIONS');
        $registrations->addChild('BUYER_ROLE', $data['customer_vat']);
        $this->lines($invoice, $data);
    }

    /**
     * Add a line item to an invoice.
     *
     * @param SimpleXMLElement $invoice INVOICE XML
     * @param array            $item    item to add
     * @param array            $data    data for tax call
     *
     * @return null
     */
    public function line($invoice, $item, $line_number, $data, $related_line=null)
    {
        $line = $invoice->addChild('LINE');
        $line->addAttribute('ID', $line_number);
        $line->addChild('COUNTRY_OF_ORIGIN', $this->config('ShipFromCountry'));
        $line->addChild('DELIVERY_TERMS', $this->config('Incoterms'));
        $line->addChild('DESCRIPTION', $item['sku']);
        $line->addChild('PART_NUMBER', $item['sku']);
        $lineSubtotal = $item['quantity'] * $item['unit_price'];
        $line->addChild('GROSS_AMOUNT', $lineSubtotal);
        // related line logic for freight and discount lines
        if ($related_line) {
            $line->addChild(
                'LINE_NUMBER', 10*$related_line['number'] + $related_line['offset']
            );
            $line->addChild('RELATED_LINE_NUMBER', 10*$related_line['number']);
        } else {
            $line->addChild('LINE_NUMBER', 10*$line_number);
        }
        $line->addChild('PRODUCT_CODE', $item['product_code']);
        $quantities = $line->addChild('QUANTITIES');
        $quantity = $quantities->addChild('QUANTITY');
        $quantity->addChild('AMOUNT', $item['quantity']);
        $quantity->addChild('UOM', $this->config('UOM'));
        $shipFrom = $line->addChild('SHIP_FROM');
        $shipFrom->addChild('COUNTRY', $this->config('ShipFromCountry'));
        $shipFrom->addChild('STATE', $this->config('ShipFromRegion'));
        $shipFrom->addChild('CITY', $this->config('ShipFromCity'));
        $shipFrom->addChild('POSTCODE', $this->config('ShipFromPostalCode'));
        if ($this->config('ShipFromGeoCode')) {
            $shipFrom->addChild('GEOCODE', $this->config('ShipFromGeoCode'));
        }
        if ($this->config('ShipFromAddress')) {
            $shipFrom->addChild('ADDRESS_1', $this->config('ShipFromAddress'));
        }
        $ship_to_address = $data['addresses']['ShipToAddress'];
        $shipTo = $line->addChild('SHIP_TO');
        $shipTo->addChild('COUNTRY', $ship_to_address['Country']);
        if (isset($ship_to_address['Province'])) {
            $shipTo->addChild('PROVINCE', $ship_to_address['Province']);
        } else {
            $shipTo->addChild('STATE', $ship_to_address['Region']);
        }
        $shipTo->addChild('CITY', $ship_to_address['City']);
        $shipTo->addChild('POSTCODE', $ship_to_address['PostalCode']);
        if ($ship_to_address['GeoCode']) {
            $shipTo->addChild('GEOCODE', $ship_to_address['GeoCode']);
        }
    }

    /**
     * Add line items to an invoice.
     *
     * @param SimpleXMLElement $invoice INVOICE XML
     * @param array            $data    data for tax call
     *
     * @return null
     */
    public function lines($invoice, $data)
    {
        $line_number = 0;
        foreach ($data['items'] as $item) {
            $line_number += 1;
            // save line number for related freight and discount lines
            $related_line = ['number' => $line_number, 'offset' => 0];
            $this->line($invoice, $item, $line_number, $data);
            if ($item['unit_shipping'] > 0) {
                $line_number += 1;
                $related_line['offset'] += 1;
                // copy item dictionary to create freight item
                $freight_item = $item;
                // update new freight item's product_code, unit_price, sku
                $freight_item['product_code']
                    = $this->config('ProductCode_Shipping');
                $lineSubtotal = $item['quantity'] * $item['unit_price'];
                $freight_item['unit_price'] = round(
                    $data['total_shipping']
                    * ($lineSubtotal / $data['total_subtotal']),
                    2
                );
                $freight_item['sku'] = 'FREIGHT';
                $this->line(
                    $invoice, $freight_item, $line_number, $data, $related_line
                );
            }
            if (abs($item['discount'] > 0)) {
                $line_number += 1;
                $related_line['offset'] += 1;
                // copy item dictionary to create discount item
                $discount_item = $item;
                // update new discount item's product_code, unit_price, sku
                $discount_item['product_code']
                    = $this->config('ProductCode_Discount');
                // make sure discount is negative
                if ($item['discount'] > 0) {
                    $discount_item['unit_price']
                        = -$item['discount'] / $item['quantity'];
                } else {
                    $discount_item['unit_price']
                        = $item['discount'] / $item['quantity'];
                }
                $discount_item['sku'] = 'DISCOUNT';
                $this->line(
                    $invoice, $discount_item, $line_number, $data, $related_line
                );
            }
        }
    }

    /**
     * Add WSS Security section to XML header for authentication.
     *
     * @access public
     * @return SoapHeader
     */
    public function security()
    {
        $wsse = ('http://docs.oasis-open.org/wss/2004/01/'
            . 'oasis-200401-wss-wssecurity-secext-1.0.xsd');
        $wsu = ('http://docs.oasis-open.org/wss/2004/01/'
            . 'oasis-200401-wss-wssecurity-utility-1.0.xsd');
        $wsse_password = (
            'http://docs.oasis-open.org/wss/2004/01/'
            . 'oasis-200401-wss-username-token-profile-1.0#PasswordText');
        $root = new \SimpleXMLElement('<root/>');
        $root->registerXPathNamespace('wsse', $wsse);
        $security = $root->addChild('wsse:Security', null, $wsse);
        // Username
        $usernameToken = $security->addChild('wsse:UsernameToken', null, $wsse);
        $usernameToken->addChild('wsse:Username', $this->config('Username'), $wsse);
        $usernameToken->addChild(
            'wsse:Password', $this->config('Password'), $wsse
        )->addAttribute('Type', $wsse_password);
        $authObject = new \SoapVar($security->asXML(), XSD_ANYXML);
        $header = new \SoapHeader($wsse, 'Security', $authObject, true);
        return $header;
    }

    /**
     * Set a class property.
     *
     * @param string $key   name of property
     * @param mixed  $value value of property
     *
     * @return null
     */
    public function set($key, $value)
    {
        $this->$key = $value;
    }

    /**
     * Build the XML for a tax calculation request.
     *
     * @param array $transaction transaction data for tax call
     *
     * @return null
     */
    public function taxRequest($transaction)
    {
        $taxNameSpace = (
            "http://www.sabrix.com/services/"
            . "taxcalculationservice/2011-09-01");
        $root = new \SimpleXMLElement('<root/>');
        $taxRequest = $root->addChild('taxCalculationRequest');
        $taxRequest->addAttribute('xmlns', $taxNameSpace);
        $inData = $taxRequest->addChild('INDATA');
        $inData->addAttribute('version', 'G');
        $inData->addChild(
            'COMPANY_ROLE', substr($this->config('CompanyRole'), 0, 1)
        );
        $inData->addChild(
            'EXTERNAL_COMPANY_ID', $this->config('ExternalCompanyId')
        );
        $inData->addChild('HOST_SYSTEM', $this->config('HostSystemNumber'));
        $inData->addChild(
            'CALLING_SYSTEM_NUMBER', $this->config('CallingSystemNumber')
        );
        $this->invoice($inData, $transaction);
        return $taxRequest;
    }
}
