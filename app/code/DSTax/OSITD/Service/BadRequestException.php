<?php

namespace DSTax\OSITD\Service;

use Exception;

/**
 * Class BadRequestException
 * @package DSTax\OSITD\Service
 */
class BadRequestException extends Exception
{

}
