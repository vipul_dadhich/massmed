<?php
/**
 * Copyright: DSTax, LLC, 2018
 * The DSTAX SOFTWARE is protected by copyright laws and international treaty
 * provisions.  You may not reverse engineer, decompile, or disassemble the
 * DSTAX SOFTWARE.
 **/
namespace DSTax\TaxConnect\Model\Sales\Total\Quote;

use DSTax\OSITD\OSITD;

use Magento\Customer\Api\Data\AddressInterfaceFactory as CustomerAddressFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory as CustomerAddressRegionFactory;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote\Address;
use Magento\Tax\Api\Data\TaxClassKeyInterface;
use Magento\Tax\Model\Calculation;

/**
 * Tax totals calculation model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Tax extends \Magento\Tax\Model\Sales\Total\Quote\Tax
{
    const AVS_ENABLED = false; // Address validation service

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var \Magento\Tax\Api\Data\QuoteDetailsItemExtensionFactory
     */
    protected $extensionFactory;
    protected $logger;
    protected $scopeConfig;

    /**
     * Class constructor
     *
     * @param \Magento\Tax\Model\Config $taxConfig
     * @param \Magento\Tax\Api\TaxCalculationInterface $taxCalculationService
     * @param \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsDataObjectFactory
     * @param \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemDataObjectFactory
     * @param \Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory $taxClassKeyDataObjectFactory
     * @param CustomerAddressFactory $customerAddressFactory
     * @param CustomerAddressRegionFactory $customerAddressRegionFactory
     * @param \Magento\Tax\Helper\Data $taxData
    // CUSTOM PARAMS BELOW
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
     * @param \Magento\Tax\Api\Data\QuoteDetailsItemExtensionFactory $extensionFactory
     */
    public function __construct(
        \Magento\Tax\Model\Config $taxConfig,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculationService,
        \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsDataObjectFactory,
        \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemDataObjectFactory,
        \Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory $taxClassKeyDataObjectFactory,
        CustomerAddressFactory $customerAddressFactory,
        CustomerAddressRegionFactory $customerAddressRegionFactory,
        \Magento\Tax\Helper\Data $taxData,
        // CUSTOM CLASSES BELOW
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Tax\Api\Data\QuoteDetailsItemExtensionFactory $extensionFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        // CUSTOM DEFINITIONS
        $this->priceCurrency    = $priceCurrency;
        $this->extensionFactory = $extensionFactory;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;

        // Find magento root directory for logging
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $this->logDirectory = $directory->getRoot() . '/var/log/';
        $this->loggingEnabled = $this->getConfigValue('Logging');

        parent::__construct(
            $taxConfig,
            $taxCalculationService,
            $quoteDetailsDataObjectFactory,
            $quoteDetailsItemDataObjectFactory,
            $taxClassKeyDataObjectFactory,
            $customerAddressFactory,
            $customerAddressRegionFactory,
            $taxData
        );
    }


    /**
     * Collect tax totals for quote address
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Address\Total $total
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        $url = basename($_SERVER['REQUEST_URI']);
        $address = $shippingAssignment->getShipping()->getAddress();
        $hasValidShippingAddress = true;
        // Do not make a tax call for US and Canadian addresses without a postal code
        if (($address->getCountry() == 'US' or $address->getCountry() == 'CA')
            and !$address->getPostcode()
        ) {
            $hasValidShippingAddress = false;
        }
        if (!$shippingAssignment->getItems()) {
        } elseif (!$hasValidShippingAddress) {
        } elseif ($url == 'estimate-shipping-methods-by-address-id') {
        } else {
            $baseQuoteTaxDetails = $this->getQuoteTaxDetailsInterface($shippingAssignment, $total, true);
            $baseTaxDetails = $this->getQuoteTaxDetails($shippingAssignment, $total, true);
            $taxDetails     = $this->getQuoteTaxDetails($shippingAssignment, $total, false);

            //Populate address and items with tax calculation results
            $itemsByType = $this->organizeItemTaxDetailsByType($taxDetails, $baseTaxDetails);
            if (isset($itemsByType[self::ITEM_TYPE_PRODUCT])) {
                $this->processProductItems($shippingAssignment, $itemsByType[self::ITEM_TYPE_PRODUCT], $total);
            }

            if (isset($itemsByType[self::ITEM_TYPE_SHIPPING])) {
                $shippingTaxDetails     = $itemsByType[self::ITEM_TYPE_SHIPPING][self::ITEM_CODE_SHIPPING][self::KEY_ITEM];
                $baseShippingTaxDetails =
                    $itemsByType[self::ITEM_TYPE_SHIPPING][self::ITEM_CODE_SHIPPING][self::KEY_BASE_ITEM];
                $this->processShippingTaxInfo($shippingAssignment, $total, $shippingTaxDetails, $baseShippingTaxDetails);
            }

            $shippingQtyCosts = $this->getShippingQtyCosts($baseQuoteTaxDetails);
            $headerQuote = $this->getItemDetails($shippingAssignment, $quote, $shippingQtyCosts);
            $this->getTaxFromOneSource($quote, $shippingAssignment, $total, $headerQuote);

            //Process taxable items that are not product or shipping
            $this->processExtraTaxables($total, $itemsByType);

            //Save applied taxes for each item and the quote in aggregation
            $this->processAppliedTaxes($total, $shippingAssignment, $itemsByType);
            if ($this->includeExtraTax()) {
                $total->addTotalAmount('extra_tax', $total->getExtraTaxAmount());
                $total->addBaseTotalAmount('extra_tax', $total->getBaseExtraTaxAmount());
            }
        }
        return $this;
    }

    /**
     * Get tax from OneSource
     * @param  \Magento\Quote\Model\Quote $quote
     * @param  \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param  \Magento\Quote\Model\Quote\Address\Total $total
     * @return string
     */
    public function getTaxFromOneSource(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total,
        $headerQuote
    ) {
        $address = $shippingAssignment->getShipping()->getAddress();
        // Do not make a tax call if the cart is empty
        if (!count($address->getAllItems())) {
            return;
        }
        // Call ONESOURCE for a tax quote
        $ositd = new OSITD();
        $ositd->useTaxCalculationService();
        // if ($this->getConfigValue('TaxService') == 'Simple Tax Service') {
        //     $ositd->useSimpleTaxService();
        // } else {
        //     $ositd->useTaxCalculationService();
        // }
        $ositd->set('wsdl', $this->getConfigValue('SOAP_WSDL'));
        $ositd->set(
            'configurables',
            array(
                'CallingClient' => $this->getConfigValue('CallingClient'),
                'CallingSource' => $this->getConfigValue('CallingSource'),
                'CallingSystemNumber' => $this
                    ->getConfigValue('CallingSystemNumber'),
                'CallingUser' => $this->getConfigValue('CallingUser'),
                'CompanyRole' => $this->getConfigValue('CompanyRole'),
                'CurrencyCode' => $this->getConfigValue('CurrencyCode'),
                'DbVersion' => "dummy db info",
                'ERPVersion' => $this->getConfigValue('ERPVersion'),
                'ExternalCompanyId' => $this->getConfigValue('ExternalCompanyId'),
                'HostSystemNumber' => $this->getConfigValue('HostSystemNumber'),
                'Incoterms' => $this->getConfigValue('Incoterms'),
                'IntegrationVersion' => $this->getConfigValue('IntegrationVersion'),
                'Password' => $this->getConfigValue('Password'),
                'PointOfTitleTransfer' => $this
                    ->getConfigValue('PointOfTitleTransfer'),
                'PostToAudit' => $this->getConfigValue('PostToAudit'),
                'ProductCode_Discount' => $this
                    ->getConfigValue('ProductCode_Discount'),
                'ProductCode_Shipping' => $this
                    ->getConfigValue('ProductCode_Shipping'),
                'SDKVersion' => $this->getConfigValue('SDKVersion'),
                'ShipFromAddress' => null,
                'ShipFromCity' => $this->getConfigValue('ShipFromCity'),
                'ShipFromCountry' => $this->getConfigValue('ShipFromCountry'),
                'ShipFromGeoCode' => $this->getConfigValue('ShipFromGeoCode'),
                'ShipFromPostalCode' => $this->getConfigValue('ShipFromPostalCode'),
                'ShipFromRegion' => $this->getConfigValue('ShipFromState'),
                'UOM' => $this->getConfigValue('UOM'),
                'Username' => $this->getConfigValue('UserName')
            )
        );
        /*
         * Create an associative array for the bill to address.
         */
        $BillToAddress = array();
        $billingAddress = $quote->getBillingAddress();
        if ($billingAddress->getCountry() != null
            AND $billingAddress->getCity() != null
            AND $billingAddress->getPostcode() != null
        ) {
            $BillToAddress = array(
                'Country' => $billingAddress->getCountry(),
                'Region' => $billingAddress->getRegionCode(),
                'City' => $billingAddress->getCity(),
                'PostalCode' => $this->getZipcode(
                    $billingAddress->getCountry(), $billingAddress->getPostcode()
                ),
                'GeoCode' => $this->getZipExtension(
                    $billingAddress->getCountry(), $billingAddress->getPostcode()
                ),
            );
        }
        /*
         * Create an associative array for the ship to address.
         */
        $ShipToAddress = [
            'Address1' => $address->getStreetFull(),
            'City' => $address->getCity(),
            'Country' => $address->getCountry(),
            'GeoCode' => $this
                ->getZipExtension($address->getCountry(), $address->getPostcode()),
            'PartnerName' => $headerQuote['customerName'],
            'PartnerNumber' => $headerQuote['customerID'],
            'PostalCode' => $this
                ->getZipcode($address->getCountry(), $address->getPostcode()),
            'Region' => $address->getRegionCode(),
        ];
        /*
         * Generate a mapping from tax_class to ProductCode.
         */
        $product_codes = array();
        foreach ($headerQuote['items'] as $item) {
            $product_code = substr($this->getProductCode($item['tax_class']), 0, 4);
            $product_codes[$item['tax_class']] = $product_code;
        }
        /*
         * Get carrier code and carrier method.
         */
        $carrierCode = null;
        $carrierMethod = null;
        if (strpos($address->getShippingMethod(), '_') !== false) {
            $carrierInfo = explode("_", $address->getShippingMethod());
            $carrierCode = $carrierInfo[0];
            $carrierMethod = $carrierInfo[1];
        }
        try {
            $response = $ositd->getTax(
                array(
                    'addresses' => array(
                        'BillToAddress' => $BillToAddress,
                        'ShipToAddress' => $ShipToAddress,
                    ),
                    'carrier_code' => $carrierCode,
                    'carrier_method' => $carrierMethod,
                    'customer_vat' => $quote->getCustomerTaxvat(),
                    'items' => $headerQuote['items'],
                    'order_number' => $headerQuote['orderNumber'],
                    'product_codes' => $product_codes,
                    'total_shipping' => $total->getTotalAmount('shipping'),
                    'total_subtotal' => $total->getTotalAmount('subtotal'),
                )
            );
            if (Tax::AVS_ENABLED
                AND strpos($url, 'shipping-information') !== false
                AND ($address->getCountry() == "USA"
                || $address->getCountry() == "US")
            ) {
                //Override saved address with the OneSource Cleansed address
                $shippingAssignment->getShipping()->getAddress()->setCity(
                    $validatedAddress[0]['city']
                );
                $shippingAssignment->getShipping()->getAddress()->setStreet(
                    $validatedAddress[0]['address1']
                );
                $shippingAssignment->getShipping()->getAddress()->setPostcode(
                    $validatedAddress[0]['postalCode']
                );
            };

            if ($this->loggingEnabled) {
                $file = $this->logDirectory . 'taxConnect.log';
                $logs = str_replace(
                    $this->getConfigValue('Password'),
                    'password redacted',
                    $ositd->get('xml_request')
                );
                $logs .= "\n\n";
                $logs .= $ositd->get('xml_response');
                $logs .= "\n\n";
                file_put_contents($file, $logs, FILE_APPEND);
            }
        } catch (\SoapFault $exception) {
            $errorFile = $this->logDirectory . 'taxConnectError.log';
            $logs = str_replace(
                $this->getConfigValue('Password'),
                'password redacted',
                $ositd->get('xml_request')
            );
            $logs .= "\n\n";
            $logs .= $exception->getMessage() . "\n";
            $logs .= "\n\n";
            file_put_contents($errorFile, $logs, FILE_APPEND);
            return $this;
        }
        if ($this->getConfigValue('TaxService') == 'Simple Tax Service') {
            if (isset($response->TaxDocuments->TaxDocument->TotalTaxAmount)) {
                $taxDocument = $response->TaxDocuments->TaxDocument;
                $totalTaxAmount = floatval($taxDocument->TotalTaxAmount);
                $total->setTotalAmount('tax', $totalTaxAmount);
                $total->setBaseTotalAmount('tax', $totalTaxAmount);
                $subtotalInclTax = $total->getSubtotal() + $total->getTotalAmount('tax');
                $baseSubtotalInclTax = $total->getBaseSubtotal() +
                    $total->getBaseTotalAmount('tax');
                $total->setSubtotalInclTax($subtotalInclTax);
                $total->setBaseSubtotalTotalInclTax($baseSubtotalInclTax);
                $total->setBaseSubtotalInclTax($baseSubtotalInclTax);
                $shippingAssignment->getShipping()->getAddress()
                    ->setBaseSubtotalTotalInclTax($baseSubtotalInclTax);
                $shippingAssignment->getShipping()->getAddress()
                    ->setTaxAmount($totalTaxAmount);
                $shippingAssignment->getShipping()->getAddress()
                    ->setBaseTaxAmount($totalTaxAmount);

                //Transfer line by line tax data to $shippingAssignment
                $items = $shippingAssignment->getItems();
                $taxLine = $taxDocument->TaxLines->TaxLine;
                for ($i=0; $i < count($items); $i++) {
                    $item = $items[$i];
                    if (is_array($taxLine)) {
                        // multiple lines
                        $line = $taxLine[$i];
                    } else {
                        // only one line, $taxLine is a stdClass
                        $line = $taxLine;
                    }
                    if (isset($line->TaxSummary->EffectiveTaxRate)) {
                        $taxRate = 100 * floatval($line->TaxSummary->EffectiveTaxRate);
                        $item->setTaxPercent(floatval($taxRate));
                    }
                    if (isset($line->TaxSummary->CalculatedTaxAmount)) {
                        $taxAmount = floatval($line->TaxSummary->CalculatedTaxAmount);
                        $item->setTaxAmount($taxAmount);
                    }
                    if ($address->getCountry() == "CA"
                        || $address->getCountry() == "CANADA"
                    ) {
                        // Break out federal and provincial taxes
                        if (!isset($authorities)) {
                            $authorities = array();
                        }
                        for ($j=0; $j < count($line->TaxDetails->TaxDetail); $j++) {
                            $taxDetail = $line->TaxDetails->TaxDetail;
                            if (is_array($taxDetail)) {
                                $taxDetail = $line->TaxDetails->TaxDetail[$j];
                            }
                            // build a key-value structure of authorities and tax amounts
                            $tax = $taxDetail->CalculatedTaxAmount;
                            if (abs($tax) > 0) {
                                $authority = $taxDetail->AuthorityType;
                                if (isset($authorities[$authority])) {
                                    $authorities[$authority] += $tax;
                                } else {
                                    $authorities[$authority] = $tax;
                                }
                            }
                        }
                        // this is just a place holder
                        //$total->setFullInfo($authorities);
                        //$total->setFullInfo(
                            //array(
                                //'percent'=>'1',
                                //'amount'=>'2',
                                //'rates'=>array(
                                        //'title'=>'QST',
                                        //'percent'=>'3',
                                //)
                            //)
                        //);
                    }
                }
            }
        } else {
            if (array_key_exists('TotalTaxAmount', $response)) {
                $totalTaxAmount = floatval($response['TotalTaxAmount']);
                $total->setTotalAmount('tax', $totalTaxAmount);
                $total->setBaseTotalAmount('tax', $totalTaxAmount);
                $subtotalInclTax = $total->getSubtotal() + $total->getTotalAmount('tax');
                $baseSubtotalInclTax = $total->getBaseSubtotal() +
                    $total->getBaseTotalAmount('tax');
                $total->setSubtotalInclTax($subtotalInclTax);
                $total->setBaseSubtotalTotalInclTax($baseSubtotalInclTax);
                $total->setBaseSubtotalInclTax($baseSubtotalInclTax);
                $shippingAssignment->getShipping()->getAddress()
                    ->setBaseSubtotalTotalInclTax($baseSubtotalInclTax);
                $shippingAssignment->getShipping()->getAddress()
                    ->setTaxAmount($totalTaxAmount);
                $shippingAssignment->getShipping()->getAddress()
                    ->setBaseTaxAmount($totalTaxAmount);
            }
            if (array_key_exists('TaxDetails', $response)) {
                //Transfer line by line tax data to $shippingAssignment
                $items = $shippingAssignment->getItems();
                $taxLine = $response['TaxDetails'];
                for ($i=0; $i < count($items); $i++) {
                    $item = $items[$i];
                    if (is_array($taxLine)) {
                        // multiple lines
                        $line = $taxLine[$i];
                    } else {
                        // only one line, $taxLine is a stdClass
                        $line = $taxLine;
                    }
                    if (isset($line['TaxRate'])) {
                        $taxRate = floatval($line['TaxRate']);
                        $item->setTaxPercent(floatval(100 * $taxRate));
                    }
                    if (isset($line['TaxAmount'])) {
                        $taxAmount = floatval($line['TaxAmount']);
                        $item->setTaxAmount($taxAmount);
                    }
                }
            }

        }
        return $this;
    }


    /**
     * Get the value of configurable items from the Magento admin panel.
     *
     * @param string                                           $name
     * @param \Magento\Store\Model\ScopeInterface::SCOPE_STORE $storeScope
     *
     * @access public
     *
     * @return mixed
     */
    public function getConfigValue($name)
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $configPath = 'DSTax_TaxConnect/general/';
        $configName = $configPath . $name;
        return $this->scopeConfig->getValue($configName, $storeScope);
    }

    /**
     * For the USA, return the first 5 digits in the zipcode.
     * For everyone else, return the entire postalcode.
     *
     * @param string $country    name of country
     * @param string $postalcode postal code
     *
     * @return string
     */
    private function getZipcode($country, $postalcode) {
        if ($country == "USA" || $country == "US") {
            if ($postalcode && strlen($postalcode) > 5) {
                return substr($postalcode, 0, 5);
            }
        }
        return $postalcode;
    }

    /**
     * For the USA, return the last 4 digits of the zipcode.
     * There may or may not be a dash in the code.
     * For everyone else, the zip extension is not used.
     *
     * @param string $country    name of country
     * @param string $postalcode postal code
     *
     * @return string
     */
    private function getZipExtension($country, $postalcode) {
        if ($country == "USA" || $country == "US") {
            if ($postalcode && (strlen($postalcode) == 9 || strlen($postalcode) == 10)) {
                return substr($postalcode, -4);
            }
        }
        return null;
    }

    /**
     * Call tax calculation service to get tax details on the quote and items
     *
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Address\Total               $total
     * @param bool                        $useBaseCurrency
     *
     * @return \Magento\Tax\Api\Data\TaxDetailsInterface
     */
    protected function getQuoteTaxDetailsInterface($shippingAssignment, $total, $useBaseCurrency)
    {
        $address = $shippingAssignment->getShipping()->getAddress();
        //Setup taxable items
        $priceIncludesTax = $this->_config->priceIncludesTax($address->getQuote()->getStore());
        $itemDataObjects  = $this->mapItems($shippingAssignment, $priceIncludesTax, $useBaseCurrency);

        //Add shipping
        $shippingDataObject = $this->getShippingDataObject($shippingAssignment, $total, $useBaseCurrency);
        if ($shippingDataObject != null) {
            # Does shipping also need custom tax? Add protected function here
            # extendShippingItem($shippingDataObject);
            $itemDataObjects[] = $shippingDataObject;
        }

        //process extra taxable items associated only with quote
        $quoteExtraTaxables = $this->mapQuoteExtraTaxables(
            $this->quoteDetailsItemDataObjectFactory,
            $address,
            $useBaseCurrency
        );
        if (!empty($quoteExtraTaxables)) {
            $itemDataObjects = array_merge($itemDataObjects, $quoteExtraTaxables);
        }

        //Preparation for calling taxCalculationService
        $quoteDetails = $this->prepareQuoteDetails($shippingAssignment, $itemDataObjects);

        // $taxDetails = $this->taxCalculationService
        //     ->calculateTax($quoteDetails, $address->getQuote()->getStore()->getStoreId());

        // return $taxDetails;
        return $quoteDetails;
    }

    /**
     * Get quote tax details for calculation
     *
     * @param \Magento\Quote\Model\Quote                  $quote
     * @param \Magento\Tax\Api\Data\QuoteDetailsInterface $taxDetails
     * @param bool                                        $useBaseCurrency
     *
     * @return array
     */
    public function getQuoteTaxDetailsOverride(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Tax\Api\Data\QuoteDetailsInterface $taxDetails,
        $useBaseCurrency
    ) {
        $store      = $quote->getStore();
        $taxDetails = $this->taxCalculation->calculateTaxDetails($taxDetails, $useBaseCurrency, $store);
        return $taxDetails;
    }

    /**
     * Get tax from OneSource
     *
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote  $quote
     *
     * @return array
     */
    protected function getItemDetails($shippingAssignment, $quote, $shippingQtyCosts)
    {
        $taxClassId = 0;
        $orderNumber = $quote->reserveOrderId()->getReservedOrderId();
        $customerID = $quote->getCustomerId();
        $firstName = $quote->getCustomerFirstname();
        $lastName = $quote->getCustomerLastname();

        $lineItems = [];
        $items     = $shippingAssignment->getItems();
        $lineNumber = 0;
        if (count($items) > 0) {
            foreach ($items as $item) {
                $lineNumber = $lineNumber + 10;
                $sku                 = $item->getSku();
                $quantity            = $item->getQty();
                $unitPrice           = (float) $item->getPrice();
                $discount            = (float) $item->getDiscountAmount();
                // $unitShipping        = (float) $shippingQtyCosts[0]['unit_price'] * $quantity;
                $unitShipping        = (float) $shippingQtyCosts[0]['unit_price'] / $quantity;
                $taxClassId          = $item->getTaxClassId();
                $productCode         = $this->getProductCode($taxClassId);
                // Don't double tax dynamically priced bundles
                if ($item->getParentItem() != null) {
                    $unitPrice = (float) 0;
                }

                array_push(
                    $lineItems,
                    [
                        'line'             => $lineNumber,
                        'sku'              => $sku,
                        'quantity'         => $quantity,
                        'unit_price'       => $unitPrice,
                        'discount'         => $discount,
                        'unit_shipping'    => $unitShipping,
                        'product_code'     => $productCode,
                        'tax_class'        => $taxClassId,
                    ]
                );
            }
        }


        $headerQuote['orderNumber'] = $orderNumber;
        $headerQuote['customerID'] = $customerID;
        if ($firstName == null and $lastName == null) {
            $headerQuote['customerName'] = "GUEST";
        } else {
            $headerQuote['customerName'] = $firstName.' '.$lastName;
        }
        $headerQuote['items'] = $lineItems;

        return $headerQuote;
    }

    /**
     * Get tax from OneSource
     *
     * @param \Magento\Quote\Model\Quote $baseQuoteTaxDetails
     *
     * @return array
     */
    protected function getShippingQtyCosts($baseQuoteTaxDetails) {

        $shippingQtyCosts = [];
        $items     = $baseQuoteTaxDetails->getItems();
        $unitPrice = 0.0;

        //Get total "Product" items count
        $totalNumberOfItems = 0;
        if (count($items) > 0) {
            foreach ($items as $item) {
                if ($item->getType() == 'product') {
                    $totalNumberOfItems = $totalNumberOfItems + $item->getQuantity();
                }
                if ($item->getType() == 'shipping') {
                    $unitPrice = (float) $item->getUnitPrice();
                }
            }
        }

        array_push($shippingQtyCosts, [
            'quantity'         => $totalNumberOfItems,
            'unit_price'       => $unitPrice,
        ]);
        // echo '<pre>'; print_r($shippingQtyCosts[0]['unit_price']); echo '</pre>';

        return $shippingQtyCosts;
    }

    /**
     * Get product code
     *
     * @param string $taxClassId tax class for product
     *
     * @return string
     */
    protected function getProductCode($taxClassId)
    {
        // Start query - tax class table
        // Get an instance of the object manager
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        // get table name with prefix
        $tableName = $resource->getTableName('tax_class');
        //Select Data from table
        $sql = "Select * FROM " . $tableName;
        // get associative array, table fields as key in array.
        $tax_classes_table = $connection->fetchAll($sql);
        // End query
        foreach ($tax_classes_table as $tax_class) {
            if ($tax_class['class_id'] == $taxClassId) {
                return $tax_class['class_name'];
            };
        };
        return 'NONE';
    }

}
