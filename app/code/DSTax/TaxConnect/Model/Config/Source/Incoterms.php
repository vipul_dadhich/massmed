<?php
/**
 * Copyright: DSTax, LLC, 2020
 * The DSTAX SOFTWARE is protected by copyright laws and international treaty
 * provisions.  You may not reverse engineer, decompile, or disassemble the
 * DSTAX SOFTWARE.
 **/
namespace DSTax\TaxConnect\Model\Config\Source;

/**
 * These are the options for the Company Role drop down menu in the admin UI.
 */
class Incoterms implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Retrieve list of options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'CFR', 'label' => __('CFR')],
            ['value' => 'CIF', 'label' => __('CIF')],
            ['value' => 'CIP', 'label' => __('CIP')],
            ['value' => 'CPT', 'label' => __('CPT')],
            ['value' => 'CUS', 'label' => __('CUS')],
            ['value' => 'DAF', 'label' => __('DAF')],
            ['value' => 'DAP', 'label' => __('DAP')],
            ['value' => 'DAT', 'label' => __('DAT')],
            ['value' => 'DDP', 'label' => __('DDP')],
            ['value' => 'DDU', 'label' => __('DDU')],
            ['value' => 'DEQ', 'label' => __('DEQ')],
            ['value' => 'DES', 'label' => __('DES')],
            ['value' => 'EXW', 'label' => __('EXW')],
            ['value' => 'FAS', 'label' => __('FAS')],
            ['value' => 'FCA', 'label' => __('FCA')],
            ['value' => 'FOB', 'label' => __('FOB')],
            ['value' => 'SUP', 'label' => __('SUP')]
        ];
    }
}
