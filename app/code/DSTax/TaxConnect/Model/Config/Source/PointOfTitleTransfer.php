<?php
/**
 * Copyright: DSTax, LLC, 2020
 * The DSTAX SOFTWARE is protected by copyright laws and international treaty
 * provisions.  You may not reverse engineer, decompile, or disassemble the
 * DSTAX SOFTWARE.
 **/
namespace DSTax\TaxConnect\Model\Config\Source;

/**
 * These are the options for the Company Role drop down menu in the admin UI.
 */
class PointOfTitleTransfer implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Retrieve list of options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'Destination', 'label' => __('Destination')],
            ['value' => 'Origin', 'label' => __('Origin')],
            ['value' => 'In Transit', 'label' => __('In Transit')]
        ];
    }
}
