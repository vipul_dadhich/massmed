<?php
/**
 * Copyright: DSTax, LLC, 2020
 * The DSTAX SOFTWARE is protected by copyright laws and international treaty
 * provisions.  You may not reverse engineer, decompile, or disassemble the
 * DSTAX SOFTWARE.
 **/
namespace DSTax\TaxConnect\Model\Config\Source;

/**
 * These are the options for the Company Role drop down menu in the admin UI.
 */
class ProductIdentifierTarget implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Retrieve list of options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => __('Product Code')],
            ['value' => 1, 'label' => __('Commodity Code')]
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            0 => __('PRODUCT_CODE'),
            1 => __('COMMODITY_CODE')
        ];
    }

}
