<?php
/**
 * Copyright: DSTax, LLC, 2020
 * The DSTAX SOFTWARE is protected by copyright laws and international treaty
 * provisions.  You may not reverse engineer, decompile, or disassemble the
 * DSTAX SOFTWARE.
 **/
namespace DSTax\TaxConnect\Model\Config\Source;

/**
 * These are the options for the Company Role drop down menu in the admin UI.
 */
class ProductIdentifierSource implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Retrieve list of options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => __('Default')],
            ['value' => 1, 'label' => __('SKU')],
            ['value' => 2, 'label' => __('Item Tax Class')]
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            0 => __('Default'),
            1 => __('SKU'),
            2 => __('Item Tax Class')
        ];
    }

}
