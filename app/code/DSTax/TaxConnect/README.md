# Magento 2 extension for Thomson Reuters ONESOURCE Tax Services
This Magento 2 extension uses Thomson Reuters ONESOURCE Tax Service to
calculate sales tax for orders.  It can make tax calls to Thomson Reuters
Simple Tax Service API or their Tax Calculation Service API.

## Installation
From the Linux terminal go to your Magento root directory and run these
commands:
```
composer config repositories.ositd git@gitlab.com:dstax/ositd-php.git
composer require dstax/ositd:@dev
composer config repositories.taxconnect git@gitlab.com:dstax/magento-2.git
composer require dstax/taxconnect:@dev

bin/magento setup:upgrade
```
