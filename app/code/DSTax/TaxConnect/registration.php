<?php
/**
 * Copyright © 2020 DSTax. All rights reserved.
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'DSTax_TaxConnect',
    __DIR__
);
