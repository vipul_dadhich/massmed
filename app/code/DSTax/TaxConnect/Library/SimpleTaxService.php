<?php
/**
 * Copyright: DSTax, LLC, 2020
 * The DSTAX SOFTWARE is protected by copyright laws and international treaty
 * provisions.  You may not reverse engineer, decompile, or disassemble the
 * DSTAX SOFTWARE.
 *
 * Classes for calling Thomson-Reuters ONESOURCE Indirect Tax Determination
 * (OSITD) services.
 *
 * There are two primary services:
 * Simple Tax Service
 * Tax Calculation Service
 *
 * The Simple Tax Service is older and Thomson-Reuters is encouraging customers to
 * move to the Tax Calculation Service.
 *
 * @link www.dstax.com
 */

namespace DSTax\TaxConnect\Library;

use function DSTax\TaxConnect\Library\simpleTaxServiceWSDL;
use function DSTax\TaxConnect\Library\utcTime;

/**
 * For making tax calls to Thomson Reuters ONESOURCE Simple Tax Service.
 */
class SimpleTaxService
{
    public function __construct()
    {
        $this->wsdl = simpleTaxServiceWSDL();
    }

    /**
     * Add Amounts XML section to Line.
     *
     * @param SimpleXMLElement $line Line XML section
     * @param array            $item tax data for item
     * @param array            $data tax data for transaction
     *
     * @return null
     */
    public function amounts($line, $item, $data)
    {
        $lineSubtotal = $item['quantity'] * $item['unit_price'];
        $perItemShipping = round(
            $data['total_shipping'] * ($lineSubtotal / $data['total_subtotal']),
            2
        );
        $amounts = $line->addChild('Amounts');
        $amounts->addChild('GrossAmount', $lineSubtotal);
        if ($item['unit_shipping'] > 0 or $item['discount'] > 0) {
            $chargeCollection = $line->addChild('ChargeCollection');
            // Freight
            if ($item['unit_shipping'] > 0) {
                $charge = $chargeCollection->addChild('Charge');
                $charge->addChild('ChargeType', 'FREIGHT');
                $charge->addChild('ChargeAmount', $perItemShipping);
                $charge->addChild(
                    'ChargeProductCode', $this->config('ProductCode_Shipping')
                );
            }
            // Discount
            if ($item['discount'] > 0) {
                $charge = $chargeCollection->addChild('Charge');
                $chargeCollection->addChild('ChargeType', 'DISCOUNT');
                $chargeCollection->addChild('ChargeAmount', -1 * $item['discount']);
                $chargeCollection->addChild(
                    'ChargeProductCode', $this->config('ProductCode_Discount')
                );
            }
            $chargeCollection->addChild(
                'ProcessingOption', 'ChargeAsSeparateAuthority'
            );
        }
        $attributes = $line->addChild('Attributes');
        $attributes->addChild('ProductCode', substr($item['product_code'], 0, 4));
        $attributes->addChild('PartNumber', $item['sku']);
        $container = $attributes->addChild('UserAttributeContainer');
        if ($data['carrier_code'] != null) {
            $userAttributes = $container->addChild('UserAttributes');
            $userAttributes->addChild('Key', '1');
            $userAttributes->addChild('Value', $data['carrier_code']);
        }
        if ($data['carrier_method'] != null) {
            $userAttributes = $container->addChild('UserAttributes');
            $userAttributes->addChild('Key', '2');
            $userAttributes->addChild('Value', $data['carrier_method']);
        }
        if ($item['discount']) {
            $userAttributes = $container->addChild('UserAttributes');
            $userAttributes->addChild('Key', '3');
            $userAttributes->addChild(
                'Value', $lineSubtotal / abs($item['discount'])
            );
        }
        if ($this->config('Incoterms')) {
            $userAttributes = $container->addChild('UserAttributes');
            $userAttributes->addChild('Key', '4');
            $userAttributes->addChild('Value', $this->config('Incoterms'));
        }
        if ($item['discount']) {
            $userAttributes = $container->addChild('UserAttributes');
            $userAttributes->addChild('Key', '5');
            $userAttributes->addChild(
                'Value', $item['discount'] + $item['unit_price']
            );
        }
        $quantity = $line->addChild('Quantity');
        $quantity->addChild('Amount', $item['quantity']);
        $quantity->addChild('UOM', $this->config('UOM'));
    }

    /**
     * Return configurable item if set, or default value if it isn't.
     *
     * @param string $name name of configurable item
     *
     * @return mixed value of configurable item
     */
    public function config($name)
    {
        if (isset($this->configurables[$name])) {
            return $this->configurables[$name];
        } else {
            return $this->defaultConfigurables($name);
        }
    }

    /**
     * Return default configurables dictionary or default value of named
     * configurable.
     *
     * @param string $name name of configurable item
     *
     * @return mixed value of configurable item or array of all items
     */
    public function defaultConfigurables($name=null)
    {
        $config = [
            'AmountType' => 'GrossAmountBased',
            'CallingClient' => null,
            'CallingSource' => null,
            'CallingSystemNumber' => null,
            'CallingUser' => null,
            'CompanyRole' => null,
            'CurrencyCode' => null,
            'DbVersion' => null,
            'DocumentType' => 'SalesInvoice',
            'ERPVersion' => null,
            'ExternalCompanyId' => null,
            'HostSystemNumber' => null,
            'Incoterms' => null,
            'IntegrationVersion' => null,
            'Password' => null,
            'PointOfTitleTransfer' => null,
            'PostToAudit' => null,
            'ProductCode_Discount' => null,
            'ProductCode_Shipping' => null,
            'SDKVersion' => null,
            'ShipFromAddress' => null,
            'ShipFromCity' => null,
            'ShipFromCountry' => null,
            'ShipFromGeoCode' => null,
            'ShipFromPostalCode' => null,
            'ShipFromRegion' => null,
            'UOM' => null,
            'Username' => null,
        ];
        if (isset($name)) {
            return $config[$name];
        } else {
            return $config;
        }
    }

    /**
     * Build Documents XML for OSITD tax request.
     *
     * @param SimpleXMLElement $taxRequest TaxRequest XML section
     * @param array            $data       TaxRequest XML section
     *
     * @return null
     */
    public function documents($taxRequest, $data)
    {
        $documents = $taxRequest->addChild('Documents');
        $document = $documents->addChild('Document');
        $document->addChild('DocumentNumber', $data['order_number']);
        if ($data['addresses']['BillToAddress'] != null) {
            $bill_to_address = $data['addresses']['BillToAddress'];
            $addresses = $document->addChild('Addresses');
            $billToAddress = $addresses->addChild('BillToAddress');
            $billToAddress->addChild('Country', $bill_to_address['Country']);
            $billToAddress->addChild('Region', $bill_to_address['Region']);
            $billToAddress->addChild('City', $bill_to_address['City']);
            $billToAddress->addChild('PostalCode', $bill_to_address['PostalCode']);
            if ($billToAddress['GeoCode'] != null) {
                $billToAddress->addChild('GeoCode', $bill_to_address['GeoCode']);
            }
        }
        $attributes = $document->addChild('Attributes');
        $attributes->addChild('IsAuditResult', $this->config('PostToAudit'));
        $attributes->addChild('AmountType', $this->config('AmountType'));
        $attributes->addChild('CompanyRole', $this->config('CompanyRole'));
        $attributes->addChild('CurrencyCode', $this->config('CurrencyCode'));
        $attributes->addChild('DocumentType', $this->config('DocumentType'));
        if ($this->config('PointOfTitleTransfer') != null) {
            $attributes->addChild(
                'PointOfTitleTransfer', $this->config('PointOfTitleTransfer')
            );
        }
        $dates = $document->addChild('Dates');
        $dates->addChild('DocumentDate', gmdate('Y-m-d'));
        $dates->addChild('FiscalDate', gmdate('Y-m-d'));
        $this->lines($document, $data);
    }

    /**
     * Call the Simple Tax Service with the supplied transaction data.
     *
     * @param array $transaction tax data for a transaction
     *
     * @return object
     */
    public function getTax($transaction)
    {
        // save XML request for logging
        $this->xml_request = $this->taxRequest($transaction)->asXML();
        // create empty tax response
        $taxResponse = [
            'TotalTaxAmount'=> 0.0,
            'TaxDetails'=> [],
        ];
        try {
            $soapClient = new \soapClient(
                $this->wsdl,
                array('trace' => true,
                    'cache_wsdl' => WSDL_CACHE_NONE,
                    'exceptions'=>true)
            );
            $soapClient->__setLocation(str_replace('?wsdl', '', $this->wsdl));
            $soapClient->__setSoapHeaders($this->security());
            $soapResponse = $soapClient->getTax(
                new \SoapVar($this->xml_request, XSD_ANYXML)
            );
            // save XML response for logging
            $this->xml_response = $soapClient->__getLastResponse();
            // fill in tax response
            $status = $soapResponse->RequestStatus->Description;
            if ($status == 'SUCCESS') {
                $taxDocument = $soapResponse->TaxDocuments->TaxDocument;
                $taxResponse['TotalTaxAmount']
                    = floatval($taxDocument->TotalTaxAmount);
                foreach ($taxDocument->TaxLines as $line) {
                    $taxAuthorities = [];
                    foreach ($line->TaxDetails as $taxDetail) {
                        foreach ($taxDetail as $authority) {
                            array_push(
                                $taxAuthorities,
                                [
                                    'Name' => $authority->AuthorityName,
                                    'TaxRate' => 100 * $authority->TaxRate,
                                    'TaxAmount' => $authority->CalculatedTaxAmount,
                                ]
                            );
                        }
                    }
                    array_push(
                        $taxResponse['TaxDetails'],
                        [
                            'TaxRate' => 100 * $line->TaxSummary->EffectiveTaxRate,
                            'TaxAmount' => $line->TaxSummary->CalculatedTaxAmount,
                            'TaxAuthorities' => $taxAuthorities,
                        ]
                    );
                }
            }
        } catch (\SoapFault $exception) {
            $this->exception = $exception->getMessage();
            $taxResponse = $this->exception;
            $this->xml_response = $soapResponse;
            //require 'vendor/psy/psysh/bin/psysh';
            //eval(\Psy\sh());
        }
        return $taxResponse;
    }

    /**
     * Add HostRequestInfo XML section to taxRequest.
     *
     * @param SimpleXMLElement $taxRequest tax request for simple tax service
     *
     * @return null
     */
    public function hostRequestInfo($taxRequest)
    {
        $requestId = uniqid().uniqid();
        $hostRequestInfo = $taxRequest->addChild('HostRequestInfo');
        $hostRequestInfo->addChild('CallingClient', $this->config('CallingClient'));
        $hostRequestInfo->addChild('CallingSource', $this->config('CallingSource'));
        $hostRequestInfo->addChild(
            'CallingSystemNumber', $this->config('CallingSystemNumber')
        );
        $hostRequestInfo->addChild('CallingUser', $this->config('CallingUser'));
        $hostRequestInfo->addChild('DbVersion', $this->config('DbVersion'));
        $hostRequestInfo->addChild('ErpVersion', $this->config('ERPVersion'));
        $hostRequestInfo->addChild('HostRequestId', $requestId);
        $hostRequestInfo->addChild('HostRequestLoggingId', $requestId);
        $hostRequestInfo->addChild(
            'HostSystemNumber', $this->config('HostSystemNumber')
        );
        $hostRequestInfo->addChild(
            'IntegrationVersion', $this->config('IntegrationVersion')
        );
        $hostRequestInfo->addChild('SdkVersion', $this->config('SDKVersion'));
    }

    /**
     * Build Lines XML for OSITD tax request.
     *
     * @param SimpleXMLElement $document document in tax request
     * @param array            $data     transaction data for tax calculation
     *
     * @return null
     */
    public function lines($document, $data)
    {
        $lines = $document->addChild('Lines');
        $item_number = 0;
        foreach ($data['items'] as $item) {
            $item_number += 1;
            $line = $lines->addChild('Line');
            $line->addAttribute('Id', $item_number);
            $line->addChild('LineNumber', $item['line']);
            $addresses = $line->addChild('Addresses');
            $shipFromAddress = $addresses->addChild('ShipFromAddress');
            $shipFromAddress->addChild('Country', $this->config('ShipFromCountry'));
            $shipFromAddress->addChild('Region', $this->config('ShipFromRegion'));
            $shipFromAddress->addChild('City', $this->config('ShipFromCity'));
            $shipFromAddress->addChild(
                'PostalCode', $this->config('ShipFromPostalCode')
            );
            if ($this->config('ShipFromGeoCode') != null) {
                $shipFromAddress->addChild(
                    'GeoCode', $this->config('ShipFromGeoCode')
                );
            }
            $ship_to_address = $data['addresses']['ShipToAddress'];
            $shipToAddress = $addresses->addChild('ShipToAddress');
            $shipToAddress->addChild('Country', $ship_to_address['Country']);
            if ($ship_to_address['Region'] != null) {
                $shipToAddress->addChild('Region', $ship_to_address['Region']);
            }
            if ($ship_to_address['City'] != null) {
                $shipToAddress->addChild('City', $ship_to_address['City']);
            }
            $shipToAddress->addChild(
                'PostalCode', $ship_to_address['PostalCode']
            );
            if ($ship_to_address['GeoCode'] != null) {
                $shipToAddress->addChild(
                    'GeoCode', $ship_to_address['GeoCode']
                );
            }
            if ($ship_to_address['PartnerNumber'] != null) {
                $shipToAddress->addChild(
                    'PartnerNumber', $ship_to_address['PartnerNumber']
                );
            }
            if ($ship_to_address['PartnerName'] != null) {
                $shipToAddress->addChild(
                    'PartnerName', $ship_to_address['PartnerName']
                );
            }
            // Add Amounts XML section
            $this->amounts($line, $item, $data);
            // Add Registrations XML section for Value Added Tax
            if ($data['customer_vat'] != null) {
                $bill_to_address = data['addresses']['BillToAddress'];
                $registrations = $line->addChild('Registrations');
                $buyerRegistrations = $registrations->addChild('BuyerRegistrations');
                $registration = $buyerRegistrations->addChild('Registration');
                $registration->addChild('Country', bill_to_address['Country']);
                $registration->addChild('Type', 'Provincial');
                $registration->addChild('Value', data['customer_vat']);
            }
        }
    }

    /**
     * Add WSS Security section to XML header for authentication.
     *
     * @access public
     * @return SoapHeader
     */
    public function security()
    {
        $wsse = ('http://docs.oasis-open.org/wss/2004/01/'
            . 'oasis-200401-wss-wssecurity-secext-1.0.xsd');
        $wsu = ('http://docs.oasis-open.org/wss/2004/01/'
            . 'oasis-200401-wss-wssecurity-utility-1.0.xsd');
        $wsse_password = (
            'http://docs.oasis-open.org/wss/2004/01/'
            . 'oasis-200401-wss-username-token-profile-1.0#PasswordText');
        $root = new \SimpleXMLElement('<root/>');
        $root->registerXPathNamespace('wsse', $wsse);
        $security = $root->addChild('wsse:Security', null, $wsse);
        // Timestamp
        $timestamp = $security->addChild('wsu:Timestamp', null, $wsu);
        $timestamp->addAttribute('wsu:Id', 'Timestamp-28');
        $timestamp->addChild('wsu:Created', utcTime(), $wsu);
        $timestamp->addChild('wsu:Expires', utcTime(5), $wsu);
        // Username
        $usernameToken = $security->addChild('wsse:UsernameToken', null, $wsse);
        $usernameToken->addChild('wsse:Username', $this->config('Username'), $wsse);
        $usernameToken->addChild(
            'wsse:Password', $this->config('Password'), $wsse
        )->addAttribute('Type', $wsse_password);
        $authObject = new \SoapVar($security->asXML(), XSD_ANYXML);
        $header = new \SoapHeader($wsse, 'Security', $authObject, true);
        return $header;
    }

    /**
     * Set a class property.
     *
     * @param string $key   name of property
     * @param mixed  $value value of property
     *
     * @return null
     */
    public function set($key, $value)
    {
        $this->$key = $value;
    }

    /**
     * Build the TaxRequest XML structure
     *
     * @param array $transaction transaction data for tax call
     *
     * @return SimpleXMLElement
     */
    public function taxRequest($transaction)
    {
        $root = new \SimpleXMLElement('<root/>');
        $taxNameSpace = 'http://www.sabrix.com/services/taxservice/2009-12-20/';
        $taxRequest = $root->addChild('TaxRequest');
        $taxRequest->addAttribute('xmlns', $taxNameSpace);
        $this->hostRequestInfo($taxRequest);
        $taxRequest->addChild(
            'ExternalCompanyId', $this->config('ExternalCompanyId')
        );
        $this->documents($taxRequest, $transaction);
        return $taxRequest;
    }
}
