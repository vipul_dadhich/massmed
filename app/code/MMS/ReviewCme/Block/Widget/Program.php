<?php
namespace MMS\ReviewCme\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use MMS\ReviewCme\Helper\Data as ReviewCmeHelper;

/**
 * Class ReviewCme
 * @package MMS\ReviewCme\Block\Widget
 */
class Program extends Template implements BlockInterface
{

    protected $_template = "widget/program.phtml";

    /**
     * @var ReviewCmeHelper
     */
    protected $_reviewCmeHelper;


    /**
     * ReviewCme constructor.
     * @param Template\Context $context
     * @param ReviewCmeHelper $reviewCmeHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        ReviewCmeHelper $reviewCmeHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_reviewCmeHelper = $reviewCmeHelper;
    }


    /**
     * @return string
     */
    public function getProgramSchedule()
    {
        $programSchedule =  $this->_reviewCmeHelper->getProgramNumber($this->getData('productSku'));
        return $programSchedule;
    }

    
}
