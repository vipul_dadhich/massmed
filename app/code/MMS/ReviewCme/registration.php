<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_ReviewCme extension
 * NOTICE OF LICENSE
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'MMS_ReviewCme',
    __DIR__
);
