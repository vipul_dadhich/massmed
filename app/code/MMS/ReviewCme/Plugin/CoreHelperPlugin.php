<?php

namespace MMS\ReviewCme\Plugin;
use MMS\ReviewCme\Helper\Data;

/**
 * Class CoreHelperPlugin
 * @package MMS\ReviewCme\Plugin
 */
class CoreHelperPlugin
{
    /**
     * CoreHelperPlugin constructor.
     * @param Data $config
     */
    public function __construct(
        Data $config
    ) {
        $this->config = $config;
    }

    /**
     * @param \Ey\Core\Helper\Data $subject
     * @param $result
     * @param $orderId
     * @return mixed|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterGetPaymentTitle(\Ey\Core\Helper\Data $subject, $result, $orderId)
    {
        if ($orderId) {
            $order = $subject->getOrder($orderId);
            $methodCode = '';
            if($order && $order->getPayment()){
                $methodCode = $order->getPayment()->getMethodInstance()->getCode();
                $isRenew = $this->config->getIsRenewAvailable($order);
                if (!$isRenew && $methodCode) {
                    if ($methodCode == 'md_firstdata') {
                        $result = 'Credit Card';
                    }
                    if ($methodCode == 'paypal_express') {
                        $result = 'PayPal';
                    }
                }
            }
        }
        return $result;
    }
}
