<?php

namespace MMS\ReviewCme\Plugin;
use Magento\Framework\View\LayoutInterface;

class CompositeConfigProviderPlugin
{
    /**
     * Url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * Paybill checkout helper
     *
     * @var \MMS\Paybill\Helper\Data
     */
    protected $_config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * Initialize dependencies.
     *
     * @param \MMS\ReviewCme\Helper\Data $config
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        LayoutInterface $layout,
        \MMS\ReviewCme\Helper\Data $config,
        \Magento\Framework\UrlInterface $url,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Api\PaymentMethodManagementInterface $paymentMethodManagement
    ) {
        $this->_config = $config;
        $this->_layout = $layout;
        $this->_checkoutSession = $checkoutSession;
        $this->url = $url;
        $this->paymentMethodManagement = $paymentMethodManagement;
    }

    /**
     * @param \Magento\Checkout\Model\CompositeConfigProvider $subject
     * @param array $result
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetConfig(\Magento\Checkout\Model\CompositeConfigProvider $subject, array $result)
    {
        if (!$this->getCheckoutSession()->getQuote()->getData('is_quote_renew') &&
            !$this->getCheckoutSession()->getQuote()->getData('is_paybill')
        ) {
            $result['checkoutCmsPayBill'] = true;
            $isRenewAvailable = $this->_config->getIsRenewAvailable($this->getCheckoutSession()->getQuote());
            $result['paymentMethods'] = $this->getPaymentMethods();
            //print_r($result);
            if (!$isRenewAvailable) {
                if (isset($result['paymentMethods']) && !empty($result['paymentMethods'])) {
                    foreach ($result['paymentMethods'] as $key => $method) {
                        $result['checkoutCms'][$method['code']] = '';
                        if (isset($method['code']) && $method['code'] == 'md_firstdata') {
                            $result['paymentMethods'][$key]['title'] = __('Credit Card');
                        }
                        if (isset($method['code']) && $method['code'] == 'paypal_express') {
                            $result['paymentMethods'][$key]['title'] = __('PayPal');
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
    /**
     * Get list of available payment methods
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    private function getPaymentMethods()
    {
        $paymentMethods = [];
        $quote = $this->_checkoutSession->getQuote();
        if ($quote->getIsActive()) {
            foreach ($this->paymentMethodManagement->getList($quote->getId()) as $paymentMethod) {
                $paymentMethods[] = [
                    'code' => $paymentMethod->getCode(),
                    'title' => $paymentMethod->getTitle()
                ];
            }
        }
        return $paymentMethods;
    }
}
