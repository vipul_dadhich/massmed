<?php

namespace MMS\ReviewCme\Model\Api;

use MMS\ReviewCme\Model\Core\Api\SetRequest;
/**
 * Class setRequest
 */
class GetSubscription
{
	/**
     * API request endpoint
     */
    const API_REQUEST_ENDPOINT = 'v1/api/customers';

    /**
     * API request method
     */
    const API_REQUEST_METHOD = 'GET';

	/**
     * getCustomer constructor
     *
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     * @param \MMS\ReviewCme\Helper\Config $endpoints
     */
    public function __construct(
        SetRequest $request,
        \MMS\ReviewCme\Helper\Config $endpoints
    ) {
        $this->request = $request;
        $this->endpoints = $endpoints;
    }

    /**
     * Fetch some data from API
     * @param $params array() or string
     */
    public function execute($sku)
    {
        $requestParams = $this->prepareRequest($sku);
        $endpoint = $this->getSubscriptionEndpoint($sku);
        $response = $this->request->makeRequest( $endpoint, static::API_REQUEST_METHOD, $requestParams);
        return $response;
    }
    /**
     * Get Subscription API Endpoint
     * @return string
     */
    public function getSubscriptionEndpoint($sku){
        if($this->endpoints->getSubscriptionEndpoint()){
            $ucid = $this->endpoints->getCustomerUcid();
            $subscriptionUrl = str_replace('{ucid}', $ucid, $this->endpoints->getSubscriptionEndpoint());
            $subscriptionUrl = str_replace('{product-code}', $sku, $subscriptionUrl);
            return $this->endpoints->getCustomerEndpoint().$subscriptionUrl;
        }
        return self::API_REQUEST_ENDPOINT;
    }

     /**
     * Prepare the request for an API
     */
    private function prepareRequest($email){
        return array("email" => $email);
    }

    /***
        {
          "Customer": {
            "ucid": "46080fc2-2f90-46aa-b1d0-80b69bfe8b61",
            "firstName": "Vipul",
            "lastName": "Dadhich",
            "email": "vdadhich@mms.org",
            "role": "MGR",
            "suffix": "MBBS",
            "primarySpecialty": "Allergy",
            "nameOfOrganization": "MMS",
            "catalystConnect": true,
            "catalystSOI": true,
            "country": "AF",
            "insightsCouncilMember": null,
            "audienceType": "REGISTERED USER"
          }
    */
    private function prepareResponse($response){
        return $responseData= json_decode($response->getBody() , true);
    }

    private function ApiErrorMessages(){
        $error = "Customer doesn't exists";
        //Enhance the Error API error messages.
        //Use the Response setting classes for it.
    }
}
