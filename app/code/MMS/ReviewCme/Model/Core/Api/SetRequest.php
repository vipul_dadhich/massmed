<?php

namespace MMS\ReviewCme\Model\Core\Api;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Psr7\ResponseFactory;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Framework\Webapi\Rest\Request;
use Ey\MuleSoft\Helper\Data;
use MMS\ReviewCme\Helper\Config as cmeHelper;
use MMS\Logger\LoggerInterface;

Class SetRequest{


    /**
     * API request URL
     */
    const API_REQUEST_URI = '';
        
     /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * SetRequest constructor
     *
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     * @param Data $helper
     * @param cmeHelper $cmeHelper
     * @param LoggerInterface $logger
     */
    public function __construct(
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory,
        Data $helper,
        cmeHelper $cmeHelper,
        LoggerInterface $logger
    ) {
        $this->clientFactory = $clientFactory;
        $this->responseFactory = $responseFactory;
        $this->helper = $helper;
        $this->cmeHelper = $cmeHelper;
        $this->isDebugMode = $this->helper->getMuleSoftDebug();
        $this->_logger = $logger;
    }

    /**
     * Api end point
     **/
    private function getMulesoftApiURI(){
        return $this->helper->getMuleSoftCustomerURI();
    }

    /**
     * Api client ID
     **/
    private function getMulesoftClientId(){
        return $this->helper->getMuleSoftClientId();
    }

    /**
     * Api secret
     **/
    private function getMulesoftSecret(){
        return $this->helper->getMuleSoftClientSecret();
    }

    /**
     * Api Auth token
     **/
    private function getAkamaiAuthToken(){
        return $this->cmeHelper->getAkamaiAuthToken();
    }

    /**
     * Do API request with provided params
     *
     * @param string $apiMethod
     * @param string $requestMethod
     * @param array $params
     *
     * @return Response
     */
    public function makeRequest($apiMethod, $requestMethod, $params = []){
        return $this->doRequest($apiMethod, $params, $requestMethod);
    }

    /**
     * Do API request with provided params
     *
     * @param string $uriEndpoint
     * @param array $params
     * @param string $requestMethod
     *
     * @return Response
     */
    private function doRequest(
        string $uriEndpoint,
        array $params = [],
        string $requestMethod = Request::HTTP_METHOD_GET
    ) {
        /** @var Client $client */
        $client =$this->getCustomerClientWithToken();
        try {
            
            $response = $client->request(
                $requestMethod,
                $uriEndpoint
            );
            $this->_logger->debug("RESPONSE CODE: ". $response->getStatusCode());
            $this->_logger->debug('RCME Subscription Status');
            $this->_logger->debug('URI End Point->');
            $this->_logger->debug($uriEndpoint);
            $this->_logger->debug('Client ID->');
            $this->_logger->debug($this->getMulesoftClientId());
            $this->_logger->debug('Client Secert->');
            $this->_logger->debug($this->getMulesoftSecret());
            $this->_logger->debug('API Response->');
            $this->_logger->debug(print_r($response,true));
        } catch (GuzzleException $exception) {
            /** @var Response $response */
            $response = $this->responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);
            $this->_logger->debug('RCME Subscription Status');
            $this->_logger->debug('URI End Point->');
            $this->_logger->debug($uriEndpoint);
            $this->_logger->debug('Client ID->');
            $this->_logger->debug($this->getMulesoftClientId());
            $this->_logger->debug('Client Secert->');
            $this->_logger->debug($this->getMulesoftSecret());
            $this->_logger->debug('API Response->');
            $this->_logger->debug(print_r($response,true));
        }
        return $response;
    }
    
    /**
     * get customer client with auth token
     **/
    private function getCustomerClientWithToken(){
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => $this->getMulesoftApiURI(),
            'headers' => ['client_id' => $this->getMulesoftClientId(), 'client_secret' => $this->getMulesoftSecret(), 'akamai-auth-token' => $this->getAkamaiAuthToken()]
        ]]);
        
            $requestHeader = [
                'base_uri' => $this->getMulesoftApiURI(),
                'headers' => ['client_id' => $this->getMulesoftClientId(), 'client_secret' => $this->getMulesoftSecret(), 'akamai-auth-token' => $this->getAkamaiAuthToken()]
            ];
            $this->_logger->debug('Get SUBSCRIPTION HEADER:'. json_encode($requestHeader, JSON_PRETTY_PRINT) );
        
        return $client;
    }   
}
