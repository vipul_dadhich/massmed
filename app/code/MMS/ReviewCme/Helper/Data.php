<?php

namespace MMS\ReviewCme\Helper;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Model\SessionFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use MMS\Logger\LoggerInterface;
use MMS\ReviewCme\Model\Api\GetSubscription;


/**
 * Class Data
 * @package MMS\Renew\Helper
 */
class Data extends AbstractHelper
{
    /**
     * Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    private $productRepository;

    /**
     * Data constructor.
     * @param Context $context
     * @param GetSubscription $getSubscription
     * @param SessionFactory $customerSessionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param TimezoneInterface $date
     * @param JsonHelper $jsonHelper
     * @param LoggerInterface $logger
     * @param ProductFactory $_productloader
     */
    public function __construct(
        Context $context,
        GetSubscription $getSubscription,
        SessionFactory $customerSessionFactory,
        ProductRepositoryInterface $productRepository,
        TimezoneInterface $date,
        JsonHelper $jsonHelper,
        LoggerInterface $logger,
        ProductFactory $_productloader
    ) {
        parent::__construct($context);
        $this->getSubscription = $getSubscription;
        $this->customerSessionFactory = $customerSessionFactory;
        $this->productRepository = $productRepository;
        $this->_date =  $date;
        $this->jsonHelper = $jsonHelper;
        $this->_logger = $logger;
        $this->_productloader = $_productloader;
    }

    /**
     * get RCME Option
     *
     * @param string $sku
     * @return string
     */
    public function getProgramNumber($sku)
    {
        $availableProgram = array();
        if ($sku) {
            $product = $this->productRepository->get($sku);
            if ($product->getRcmeJsonData()) {
                $currentExamDate = $this->getExamDateFromSessionOrCurrentDate($sku);
                $programData = json_decode($product->getRcmeJsonData());
                if (!empty($programData)) {
                    usort($programData, function ($a, $b) {
                        $dateA = date('Y-m-d', strtotime($a->enrollBy));
                        $dateB = date('Y-m-d', strtotime($b->enrollBy));
                        return $dateA >= $dateB;
                    });
                    foreach($programData as $program){
                        if (strtotime($currentExamDate) < strtotime($program->enrollBy)) {
                            $availableProgram = (array)$program;
                            break;
                        }
                    }
                }
            }
            return $availableProgram;
        }
    }

    /**
     * @param $sku
     * @return false|string
     */
    public function getExamDateFromSessionOrCurrentDate($sku){
        $examDate = $this->_date->date()->format('m/d/Y');
        if ($this->customerIsLogin()) {
            $existingSubscription = $this->getSubscription->execute($sku);
            if ($existingSubscription->getStatusCode() == 200) {
                $response = $this->jsonHelper->jsonDecode($existingSubscription->getBody()->getContents());
                $this->_logger->debug('Get SUBSCRIPTION API RESPONSE:'. json_encode($response, JSON_PRETTY_PRINT) );
                if (isset($response['products']) && !empty($response['products'])) {
                    $examDate = $this->getHighestEnrolledDate($response, $sku);
                    $currentDate = $this->_date->date()->format('m/d/Y');
                    if (strtotime($examDate) < strtotime($currentDate)) {
                        $examDate = $currentDate;
                    }
                }
            }
        }
       return $examDate;
    }

    /**
     * @param $response
     * @param $sku
     * @return false|string
     */
    public function getHighestEnrolledDate($response, $sku){
        $enrollDates = [];
        foreach($response['products'] as $product){
            if (isset($product['productCode']) && $product['endDate'] && $product['productCode'] == $sku) {
                $enrollDates[] = $product['endDate'];
            }
        }
        if (!empty($enrollDates)) {
            $max = max(array_map('strtotime', $enrollDates));
            return date('m/d/Y', $max);
        }
    }

    /**
     * @return bool
     */
    public function customerIsLogin(){
        $sessionCustomer = $this->customerSessionFactory->create();
        return $sessionCustomer->isLoggedIn();
    }

    /**
     * @param $quote
     * @return bool
     */
    public function getIsRenewAvailable($quote){
        $isAutoRenew = false;
        foreach($quote->getAllVisibleItems() as $item) {
            $product = $this->_productloader->create()->load($item->getProductId());
            if ($product->getData('autorenew')) {
                $isAutoRenew = true;
            }
        }
        return $isAutoRenew;
    }
}
