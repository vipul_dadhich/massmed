<?php

namespace MMS\ReviewCme\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 * @package MMS\ReviewCme\Helper
 */
class Config extends AbstractHelper
{
    const ENABLE = 'reviewcme/general/enable';
    const RCME_SUBSCRIPTION_URI = 'reviewcme/general/rcme_api_url';

    public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Session $session,
        \Magento\Customer\Model\SessionFactory $customerSession
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->session = $session;
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * Is module enabled
     *
     * @param null $storeId
     * @return string
     */
    public function isEnabled($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::ENABLE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @return mixed
     */
    public function getAkamaiAuthToken()
    {
        if($this->session->getAkamaiAccessToken()){
            return $this->session->getAkamaiAccessToken();
        }
        return $this->session->getAccessToken();
    }

    /**
     * @return mixed
     */
    public function getCustomerUcid()
    {
        $customer = $this->customerSession->create()->getCustomer();
        if ($customer->getId()) {
            return $customer->getData('ucid');
        }
    }

    /**
     * @return mixed
     */
    public function getCustomerEndpoint()
    {
        return $this->scopeConfig->getValue('mulesoft/api/mulesoft_customer_endpoint', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * Api end point for check subscription
     **/
    public function getSubscriptionEndpoint($storeId = null){
        return $this->scopeConfig->getValue(
            self::RCME_SUBSCRIPTION_URI,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

}
