<?php

namespace MMS\FileAttachment\Helper;

use Exception;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Magento\MediaStorage\Model\File\Uploader;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Data
 * @package MMS\FileAttachment\Helper
 */
class Data extends AbstractHelper
{
    /**
     * Media path to extension images
     *
     * @var string
     */
    const MEDIA_PATH = 'assets';

    /**
     * @var WriteInterface
     */
    private $mediaDirectory;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * File Uploader factory
     *
     * @var UploaderFactory
     */
    private $fileUploaderFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var UrlInterface
     */
    private $backendUrl;

    /**
     * @param Context $context
     * @param UrlInterface $backendUrl
     * @param Filesystem $filesystem
     * @param UploaderFactory $fileUploaderFactory
     * @param StoreManagerInterface $storeManager
     * @throws FileSystemException
     */
    public function __construct(
        Context $context,
        UrlInterface $backendUrl,
        Filesystem $filesystem,
        UploaderFactory $fileUploaderFactory,
        StoreManagerInterface $storeManager
    )
    {
        $this->backendUrl = $backendUrl;
        $this->filesystem = $filesystem;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Upload image and return uploaded image file name or false
     *
     * @param string $scope the request key for file
     * @param $model
     * @return bool
     * @throws Exception
     */
    public function uploadFile($scope, $model)
    {
        $upload_maxsize = $this->getUploadMaxSize();
        $max_file_size = (float)$upload_maxsize * 1024 * 1024;

        $uploader = $this->fileUploaderFactory->create(['fileId' => $scope]);
        $uploader->setAllowedExtensions($this->getAllowedExt());
        $uploader->setAllowRenameFiles(true);
        $size = $uploader->getFileSize();
        if ($size > $max_file_size) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('File size must be less than %1MB. Large file will effect page load time.', $upload_maxsize)
            );
        }

        $uploader->setFilesDispersion(false);
        $uploader->setAllowCreateFolders(true);

        $fullDirectoryPath = $this->getBaseDir();
        if ($filePath = $model->getData('filepath')) {
            $filePath = ltrim($filePath, '/');
            $filePath = rtrim($filePath, '/');
            $fullDirectoryPath .= '/' . $filePath;
        }

        if ($uploader->save($fullDirectoryPath)) {
            $model->setFile($uploader->getUploadedFileName());
            $model->setFileExt($uploader->getFileExtension());
        }

        return $model;
    }

    /**
     * Look on the file system if this file is present, according to the dispersion principle
     * @param $fileName
     * @return bool
     */
    public function checkIfFileExists($fileName)
    {
        return file_exists($this->getDispersionFolderAbsolutePath($fileName) . "/" . $fileName);
    }

    /**
     * Save file-content to the file on the file-system
     * @param $filename
     * @param $fileContent
     * @return string
     */
    public function saveFile($filename, $fileContent)
    {
        if ($fileContent != "") {
            try {
                $folderAbsolutePath = $this->getDispersionFolderAbsolutePath($filename);
                if (!file_exists($folderAbsolutePath)) {
                    //create folder
                    mkdir($folderAbsolutePath, 0777, true);
                }
                // create file
                file_put_contents($folderAbsolutePath . "/" . $filename, base64_decode($fileContent));
                return true;
            } catch (Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Return the base media directory for FileAttachment Item images
     *
     * @return string
     */
    public function getBaseDir()
    {
        return $this->filesystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath(self::MEDIA_PATH);
    }

    /**
     * Return the Base URL for FileAttachment Item images
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function getBaseUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ) . self::MEDIA_PATH;
    }

    /**
     * Return current store Id
     *
     * @return Int
     * @throws NoSuchEntityException
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     * Return the path pof the file that will be save in the database
     * @param string $fileName file name with file-extension
     * @return string
     */
    public function getFilePathForDB($fileName)
    {
        return $this->getFileDispersionPath($fileName) . "/" . $fileName;
    }

    /**
     * Return the path to the file acording to the dispersion principle (first and second letter)
     * Example file.tyt => f/i/file.txt
     * @param $fileName
     * @return string
     */
    public function getFileDispersionPath($fileName)
    {
        return Uploader::getDispretionPath($fileName);
    }

    /**
     * Delete the file in the folder media folder of file attachment
     * @param $filepathInMediaFolder
     */
    public function deleteFile($filepathInMediaFolder)
    {
        $exts = explode('.', $filepathInMediaFolder);
        $ext = "";
        if (count($exts)) {
            $ext = $exts[count($exts) - 1];
        }
        if (in_array($ext, $this->getAllowedExt()) &&
            strpos($filepathInMediaFolder, "..") === false) {
            $finalPath = $this->getFileAttachMediaFolderAbsolutePath() . "/" . $filepathInMediaFolder;
            if (file_exists($finalPath)) {
                unlink($finalPath);
            }
        }
    }

    /**
     * Return the media folder absolute path
     * @return string
     */
    private function getFileAttachMediaFolderAbsolutePath()
    {
        $mediaPath = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
        return $mediaPath . self::MEDIA_PATH;
    }

    /**
     * Return the dispersion folder absoluite path for the given filename
     * @param $filename
     * @return string
     */
    public function getDispersionFolderAbsolutePath($filename)
    {
        return $this->getFileAttachMediaFolderAbsolutePath() . "/" . $this->getFileDispersionPath($filename);
    }

    /**
     * Return the allowed file extensions
     * @return array
     */
    public function getAllowedExt()
    {
        return ['css', 'js', 'pdf', 'pptx', 'xls', 'xlsx', 'flash', 'mp3', 'docx', 'doc', 'zip', 'jpg', 'jpeg', 'png', 'gif', 'ini', 'readme', 'avi', 'csv', 'txt', 'wma', 'mpg', 'flv', 'mp4'];
    }

    /**
     * Return mediaurl
     * @return string
     * @throws NoSuchEntityException
     */
    public function getMediaUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * Retrive file size by attachment
     *
     * @param $file
     * @return string
     */
    public function getFileSize($file)
    {
        $fileSize = $this->mediaDirectory->stat($file)['size'];
        return $this->convertToReadableSize($fileSize);
    }

    /**
     * Convert size into readable format
     * @param $size
     * @return string
     */
    public function convertToReadableSize($size)
    {
        $base = log($size) / log(1024);
        $suffix = ["", " KB", " MB", " GB", " TB"];
        $f_base = floor($base);
        return round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
    }

    public function getConfig($key, $store = null)
    {
        $store = $this->storeManager->getStore($store);
        return $this->scopeConfig->getValue(
            'fileattachment/' . $key,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @return float|int
     */
    public function getUploadMaxSize()
    {
        $upload_maxsize = $this->getConfig("general/upload_maxsize");
        $upload_maxsize = $upload_maxsize ? (float)$upload_maxsize : 2;
        return $upload_maxsize;
    }
}
