<?php

namespace MMS\FileAttachment\Model\ResourceModel\FileAttachment;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package MMS\FileAttachment\Model\ResourceModel\FileAttachment
 */
class Collection extends AbstractCollection
{

    protected $_idFieldName = 'file_attach_id';

    /**
     * Resource initialization
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'MMS\FileAttachment\Model\FileAttachment',
            'MMS\FileAttachment\Model\ResourceModel\FileAttachment'
        );
    }
}
