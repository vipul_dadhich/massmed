<?php

namespace MMS\FileAttachment\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * FileAttachment Resource Model
 */
class FileAttachment extends AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('fileattachment', 'file_attach_id');
    }
}
