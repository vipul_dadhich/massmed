<?php

namespace MMS\FileAttachment\Model;

use Exception;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use MMS\FileAttachment\Api\Api;
use MMS\FileAttachment\Api\Data\FileAttachmentTableInterface;
use MMS\FileAttachment\Api\Data\FileAttachmentTableInterfaceFactory;
use MMS\FileAttachment\Api\FileAttachment;
use MMS\FileAttachment\Api\Data;
use Magento\Framework\Exception\NotFoundException;
use MMS\FileAttachment\Api\FileAttachmentInterface;
use MMS\FileAttachment\Model\ResourceModel\FileAttachment\Collection;
use MMS\FileAttachment\Model\ResourceModel\FileAttachment\CollectionFactory;

/**
 * Class FileAttachmentWebApi
 * @package MMS\FileAttachment\Model
 */
class FileAttachmentWebApi implements FileAttachmentInterface
{
    const CUSTOM_PATH = "custom/upload";

    /**
     * @var ResourceModel\FileAttachment
     */
    protected $_fileAttachment;

    /**
     * @var CollectionFactory
     */
    protected $_fileAttachmentCollectionFactory;

    /**
     * @var FileAttachmentTableInterface
     */
    protected $_fileAttachmentTableInterface;

    /**
     * @var FileAttachmentTableInterfaceFactory
     */
    protected $_fileAttachmentTableInterfaceFactory;

    /**
     * @var ExtensibleDataObjectConverter
     */
    protected $_extensibleDataObjectConverter;

    /**
     * @var \MMS\FileAttachment\Helper\Data
     */
    protected $_dataHelper;

    /**
     * FileAttachmentWebApi constructor.
     * @param ResourceModel\FileAttachment $fileAttachment
     * @param FileAttachmentTableFactory $fileAttachmentCollectionFactory
     * @param FileAttachmentTableInterface $fileAttachmentTableInterface
     * @param FileAttachmentTableInterfaceFactory $fileAttachmentTableInterfaceFactory
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param \MMS\FileAttachment\Helper\Data $dataHelper
     */
    public function __construct(
        ResourceModel\FileAttachment $fileAttachment,
        FileAttachmentTableFactory $fileAttachmentCollectionFactory,
        FileAttachmentTableInterface $fileAttachmentTableInterface,
        FileAttachmentTableInterfaceFactory $fileAttachmentTableInterfaceFactory,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        \MMS\FileAttachment\Helper\Data $dataHelper
    )
    {
        $this->_fileAttachment = $fileAttachment;
        $this->_fileAttachmentCollectionFactory = $fileAttachmentCollectionFactory;
        $this->_fileAttachmentTableInterface = $fileAttachmentTableInterface;
        $this->_fileAttachmentTableInterfaceFactory = $fileAttachmentTableInterfaceFactory;
        $this->_extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->_dataHelper = $dataHelper;
    }

    /**
     * @param FileAttachmentTableInterface $fileAttachmentTableInterface
     * @param $fileName
     * @param $fileContent
     * @return mixed
     * @throws Exception
     */
    public function UpdateInsertAttachment(
        FileAttachmentTableInterface $fileAttachmentTableInterface,
        $fileName,
        $fileContent
    )
    {
        $objectArray = $fileAttachmentTableInterface->getData();

        $id = $fileAttachmentTableInterface->getId();
        if ($id == 0)
            $objectArray["file_attach_id"] = null;

        if (array_key_exists('files', $objectArray)) {
            $fileIds = $objectArray['files'];
            $objectArray['files'] = str_replace(',', '&', $fileIds);
        }

        $attachment = $this->_fileAttachmentCollectionFactory->create();
        $attachment->setData($objectArray);

        $this->_fileAttachment->load($attachment, $id);

        if ($attachment->isObjectNew() == false) {
            //UPDATE ATTACHMENT RECORD
            if (array_key_exists('name', $objectArray))
                $attachment->setName($objectArray['name']);
            if (array_key_exists('description', $objectArray))
                $attachment->setDescription($objectArray['description']);
            if (array_key_exists('url', $objectArray))
                $attachment->setUrl($objectArray['url']);
        }

        //check if file already exists on the file system
        if ($fileContent) {
            //this is a new file or an updated version of it => check if file already exists on the system
            if ($this->_dataHelper->checkIfFileExists($fileName)) {
                //delete file
                $this->_dataHelper->deleteFile($this->_dataHelper->getFileDispersionPath($fileName) . "/" . $fileName);
            }
            //create file
            if (!$this->_dataHelper->saveFile($fileName, $fileContent)) {
                return -1;
            } else {
                //update file path
                $attachment->setFile($this->_dataHelper->getFilePathForDB($fileName));

                $fileExt = "";
                $slicedFileName = explode('.', $fileName);
                if (count($slicedFileName) > 1) {
                    $fileExt = $slicedFileName[count($slicedFileName) - 1];
                }
                $attachment->setFileExt($fileExt);
            }
        } else {
            $attachment->setFileExt('');
        }

        //save attachment record
        $this->_fileAttachment->save($attachment);

        //return the id of the create/updated record
        return $attachment->getId();
    }

    /**
     * @param int $int
     * @return bool
     * @throws Exception
     */
    public function DeleteAttachment(
        $int
    )
    {
        //delete DB record
        $attachment = $this->_fileAttachmentCollectionFactory->create();
        $this->_fileAttachment->load($attachment, $int);
        if (!$attachment->getId())
            return false;
        $this->_fileAttachment->delete($attachment);

        //check if this is the last record from the DB linked to this file => if it's true, than delete this file
        /** @var Collection $collection */
        $collection = $this->_fileAttachmentCollectionFactory->create()->getCollection();
        $collection->addFieldToFilter('file', $attachment->getData("file"));
        if ($collection->count() == 0) {
            //delete file on the file system
            $this->_dataHelper->deleteFile($attachment->getData("file"));
        }

        return true;
    }

    /**
     * @param int $int
     * @return FileAttachmentTable
     * @throws NotFoundException
     */
    public function GetAttachment(
        $int
    )
    {
        $attachment = $this->_fileAttachmentCollectionFactory->create();
        $this->_fileAttachment->load($attachment, $int);
        if (!$attachment->getId()) {
            throw new NotFoundException(
                __('no attachment found')
            );
        }
        $attachResponse = $this->_fileAttachmentCollectionFactory->create();
        if ($attachment->getData()) {
            $attachResponse->setFileAttachId($attachment->getId());
            $attachResponse->setName($attachment->getName());
            $attachResponse->setDescription($attachment->getDescription());
            $attachResponse->setFile($attachment->getFile());
            $attachResponse->setUrl($attachment->getUrl());
        }

        return $attachResponse;
    }
}
