<?php

namespace MMS\FileAttachment\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class FileAttachment
 * @package MMS\FileAttachment\Model
 */
class FileAttachment extends AbstractModel
{

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'pt_files_grid';

    /**
     * @var string
     */
    protected $_cacheTag = 'pt_files_grid';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'pt_files_grid';

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('MMS\FileAttachment\Model\ResourceModel\FileAttachment');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
