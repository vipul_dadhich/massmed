<?php

namespace MMS\FileAttachment\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use MMS\FileAttachment\Api\Data\FileAttachmentTableInterface;

/**
 * Class FileAttachmentTable
 * @package MMS\FileAttachment\Model
 */
class FileAttachmentTable extends AbstractModel implements FileAttachmentTableInterface, IdentityInterface
{
    const CACHE_TAG = 'fileattachment_fileattachmenttable';
    protected $_cacheTag = 'fileattachment_fileattachmenttable';
    protected $_eventPrefix = 'fileattachment_fileattachmenttable';

    protected function _construct()
    {
        $this->_init('MMS\FileAttachment\Model\ResourceModel\FileAttachment');
    }

    /**
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return string
     */
    public function getFileAttachId()
    {
        return $this->getData("file_attach_id");
    }

    /**
     * @param string $val
     * @return void
     */
    public function setFileAttachId($val)
    {
        $this->setData("file_attach_id", $val);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData("name");
    }

    /**
     * @param string $val
     * @return void
     */
    public function setName($val)
    {
        $this->setData("name", $val);
    }

    /**
     * @return string
     */
    public function getFilepath()
    {
        return $this->getData("filepath");
    }

    /**
     * @param string $val
     * @return void
     */
    public function setFilepath($val)
    {
        $this->setData("filepath", $val);
    }

    /**
     * @return string
     */
    public function getGroup()
    {
        return $this->getData("group");
    }

    /**
     * @param string $val
     * @return void
     */
    public function setGroup($val)
    {
        $this->setData("group", $val);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData("description");
    }

    /**
     * @param string $val
     * @return void
     */
    public function setDescription($val)
    {
        $this->setData("description", $val);
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->getData("file");
    }

    /**
     * @param string $val
     * @return void
     */
    public function setFile($val)
    {
        $this->setData("file", $val);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->getData("url");
    }

    /**
     * @param string $val
     * @return void
     */
    public function setUrl($val)
    {
        $this->setData("url", $val);
    }
}
