<?php

namespace MMS\FileAttachment\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

/**
 * Class FileAttachment
 * @package MMS\FileAttachment\Block\Adminhtml
 */
class FileAttachment extends Container
{
    /**
     * Constructor
     *
     * @return void
     */
    public function _construct()
    {
        $this->_controller = 'adminhtml_fileattach';
        $this->_blockGroup = 'MMS_FileAttachment';
        $this->_headerText = __('CMS Attachments');
        $this->_addButtonLabel = __('Add New Attachment');
        parent::_construct();
        if ($this->_isAllowedAction('MMS_FileAttachment::save')) {
            $this->buttonList->update('add', 'label', __('Add New Attachment'));
        } else {
            $this->buttonList->remove('add');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    public function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
