<?php

namespace MMS\FileAttachment\Block\Adminhtml\FileAttachment\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Customer\Model\ResourceModel\Group\Collection;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;
use MMS\FileAttachment\Block\Adminhtml\FileAttachment\Renderer\FileIconAdmin;

/**
 * Class Main
 * @package MMS\FileAttachment\Block\Adminhtml\FileAttachment\Edit\Tab
 */
class Main extends Generic implements TabInterface
{
    /**
     * Prepare form
     *
     * @return $this
     * @throws LocalizedException
     */
    public function _prepareForm()
    {

        $model = $this->_coreRegistry->registry('fileattach');

        /*
         * Checking if user have permissions to save information
         */
        if ($this->_isAllowedAction('MMS_FileAttachment::save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }

        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('fileattachment_main_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Attachment Information')]
        );

        if ($model->getId()) {
            $fieldset->addField('file_attach_id', 'hidden', ['name' => 'file_attach_id']);
        }

        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Attachment Name'),
                'title' => __('Attachment Name'),
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'filepath',
            'text',
            [
                'name' => 'filepath',
                'label' => __('File Path'),
                'title' => __('File Path'),
                'required' => false,
                'disabled' => $isElementDisabled,
                'note' => 'Example of directory structure: example1/example2'
            ]
        );

        $fieldset->addField(
            'group',
            'text',
            [
                'name' => 'group',
                'label' => __('Group Name'),
                'title' => __('Group Name'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'description',
            'textarea',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
                'disabled' => $isElementDisabled
            ]
        );

        $upload_maxsize = $this->_coreRegistry->registry('fileattach_upload_size');
        $fieldset->addField(
            'files',
            'file',
            [
                'name' => 'file',
                'label' => __('File'),
                'title' => __('File'),
                'required' => true,
                'note' => 'File size must be less than ' . $upload_maxsize . ' Mb.', // TODO: show ACCTUAL file-size
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addType(
            'uploadedfile',
            FileIconAdmin::class
        );

        $fieldset->addField(
            'file',
            'uploadedfile',
            [
                'name' => 'uploadedfile',
                'label' => __('Uploaded File'),
                'title' => __('Uploaded File'),

            ]
        );

        /*$fieldset->addField(
            'url',
            'text',
            [
                'name' => 'url',
                'label' => __('URL'),
                'title' => __('URL'),
                'required' => false,
                'disabled' => $isElementDisabled,
                'note' => 'Upload file or Enter url'
            ]
        );*/

        $this->_eventManager->dispatch('adminhtml_fileattachment_edit_tab_main_prepare_form', ['form' => $form]);

        if ($model->getId()) {
            $form->setValues($model->getData());
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Attachment Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Attachment Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    public function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
