<?php

namespace MMS\FileAttachment\Block\Adminhtml\FileAttachment\Edit;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Form
 * @package MMS\FileAttachment\Block\Adminhtml\FileAttachment\Edit
 */
class Form extends Generic
{
    /**
     * Prepare form
     *
     * @return $this
     * @throws LocalizedException
     */
    public function _prepareForm()
    {
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ]
            ]
        );
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
