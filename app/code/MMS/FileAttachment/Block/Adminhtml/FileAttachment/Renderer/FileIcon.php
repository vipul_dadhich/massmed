<?php

namespace MMS\FileAttachment\Block\Adminhtml\FileAttachment\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Asset\Repository;
use MMS\FileAttachment\Helper\Data;

/**
 * Class FileIcon
 * @package MMS\FileAttachment\Block\Adminhtml\FileAttachment\Renderer
 */
class FileIcon extends AbstractRenderer
{

    /**
     * @var Repository
     */
    private $assetRepo;

    /**
     * @var Data
     */
    private $dataHelper;

    /**
     * @param Repository $assetRepo
     * @param Data $dataHelper
     */
    public function __construct(
        Repository $assetRepo,
        Data $dataHelper
    )
    {
        $this->dataHelper = $dataHelper;
        $this->assetRepo = $assetRepo;
    }

    /**
     * get customer group name
     * @param DataObject $row
     * @return string
     * @throws NoSuchEntityException
     */
    public function render(DataObject $row)
    {
        $file = $row->getFile();
        $fileExt = pathinfo($file, PATHINFO_EXTENSION);
        if ($fileExt) {
            $iconImage = $this->assetRepo->getUrl('MMS_FileAttachment::images/' . $fileExt . '.png');
            $fileIcon = "<a href=" . $this->dataHelper->getBaseUrl() . '/' . $file . " target='_blank'>
            <img src='" . $iconImage . "' /></a>";
        } else {
            $iconImage = $this->assetRepo->getUrl('MMS_FileAttachment::images/unknown.png');
            $fileIcon = "<img src='" . $iconImage . "' />";
        }

        return $fileIcon;
    }
}
