<?php

namespace MMS\FileAttachment\Block\Adminhtml\FileAttachment\Renderer;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\CollectionFactory;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\DataObject;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository;
use MMS\FileAttachment\Helper\Data;

/**
 * Class FileIconAdmin
 * @package MMS\FileAttachment\Block\Adminhtml\FileAttachment\Renderer
 */
class FileIconAdmin extends AbstractElement
{

    /**
     * @var Repository
     */
    private $assetRepo;

    /**
     * @var Data
     */
    private $dataHelper;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * Core registry
     *
     * @var Registry
     */
    private $coreRegistry = null;

    /**
     * @param Factory $factoryElement
     * @param CollectionFactory $factoryCollection
     * @param Escaper $escaper
     * @param Repository $assetRepo
     * @param Data $dataHelper
     * @param \Magento\Backend\Helper\Data $helper
     * @param UrlInterface $urlBuilder
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Factory $factoryElement,
        CollectionFactory $factoryCollection,
        Escaper $escaper,
        Repository $assetRepo,
        Data $dataHelper,
        \Magento\Backend\Helper\Data $helper,
        UrlInterface $urlBuilder,
        Registry $registry,
        $data = []
    )
    {
        $this->dataHelper = $dataHelper;
        $this->assetRepo = $assetRepo;
        $this->helper = $helper;
        $this->urlBuilder = $urlBuilder;
        $this->coreRegistry = $registry;
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data);
    }

    /**
     * get customer group name
     * @return string
     * @throws NoSuchEntityException
     */
    public function getElementHtml()
    {
        $model = $this->coreRegistry->registry('fileattach');
        $fileIcon = '<h3>No File Uploaded</h3>';
        $file = $this->getValue();
        if ($file) {
            $fileExt = pathinfo($file, PATHINFO_EXTENSION);
            if ($fileExt) {
                $iconImage = $this->assetRepo->getUrl(
                    'MMS_FileAttachment::images/' . $fileExt . '.png'
                );

                $fullDirectoryPath = $this->dataHelper->getBaseUrl();
                if ($filePath = $model->getData('filepath')) {
                    $filePath = ltrim($filePath, '/');
                    $filePath = rtrim($filePath, '/');
                    $fullDirectoryPath .= '/' . $filePath;
                }
                $url = $fullDirectoryPath . '/' . $file;
                $fileIcon = sprintf("<a href=%s target='_blank'>
                    <img src='%s' style='float: left;' />
                    <div>OPEN FILE</div></a>", $url, $iconImage);
            } else {
                $iconImage = $this->assetRepo->getUrl('MMS_FileAttachment::images/unknown.png');
                $fileIcon = "<img src='" . $iconImage . "' style='float: left;' />";
            }
            $attachId = $this->coreRegistry->registry('file_attach_id');
            $fileIcon .= sprintf("<a href='%s'>
                <div style='color:red;'>DELETE FILE</div></a>", $this->urlBuilder->getUrl(
                'fileattachment/index/deletefile', $param = ['file_attach_id' => $attachId]));
        }
        return $fileIcon;
    }
}
