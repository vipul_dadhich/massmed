<?php

namespace MMS\FileAttachment\Ui\Component\Listing\Column;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use MMS\FileAttachment\Helper\Data;

/**
 * Class File
 * @package MMS\FileAttachment\Ui\Component\Listing\Column
 */
class File extends Column
{
    /**
     * @var Data
     */
    private $_dataHelper;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Data $dataHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Data $dataHelper,
        array $components = [],
        array $data = []
    )
    {
        $this->_dataHelper = $dataHelper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     * @throws NoSuchEntityException
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (!isset($item['file'])) {
                    $item['file'] = __('--- No File ---');
                } else {
                    $fullBaseUrl = $this->_dataHelper->getBaseUrl();
                    if (isset($item['filepath']) && $item['filepath'] != '') {
                        $filePath = ltrim($item['filepath'], '/');
                        $filePath = rtrim($filePath, '/');
                        $fullBaseUrl .= '/' . $filePath;
                    }
                    $item['file'] = $fullBaseUrl . '/' . ltrim($item['file'], "/");
                }
            }
        }
        return $dataSource;
    }
}
