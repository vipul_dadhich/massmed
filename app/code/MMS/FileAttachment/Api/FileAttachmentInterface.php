<?php

namespace MMS\FileAttachment\Api;

use Exception;
use Magento\Framework\Exception\NotFoundException;
use MMS\FileAttachment\Api\Data\FileAttachmentTableInterface;

/**
 * Interface FileAttachmentInterface
 * @package MMS\FileAttachment\Api
 */
interface FileAttachmentInterface
{
    /**
     * Update / insert attachment
     * @param FileAttachmentTableInterface $fileAttachTable
     * @param string $filename
     * @param string $fileContent
     * @return int
     */
    public function UpdateInsertAttachment(
        FileAttachmentTableInterface $fileAttachTable,
        $filename,
        $fileContent
    );

    /**
     * Delete the attachment
     * @param int $int
     * @return bool
     * @throws Exception
     * @throws NotFoundException
     */
    public function DeleteAttachment(
        $int
    );

    /**
     * Get attachment
     * @param int $int
     * @return FileAttachmentTableInterface
     * @throws Exception
     * @throws NotFoundException
     */
    public function GetAttachment(
        $int
    );

}
