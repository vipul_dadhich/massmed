<?php

namespace MMS\FileAttachment\Api\Data;

/**
 * Interface FileAttachmentTableInterface
 * @package MMS\FileAttachment\Api\Data
 */
interface FileAttachmentTableInterface
{

    /**
     * @return string mixed
     */
    public function getFileAttachId();

    /**
     * @param string $val
     * @return void
     */
    public function setFileAttachId($val);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $val
     * @return void
     */
    public function setName($val);

    /**
     * @return string
     */
    public function getFilepath();

    /**
     * @param string $val
     * @return void
     */
    public function setFilepath($val);

    /**
     * @return string
     */
    public function getGroup();

    /**
     * @param string $val
     * @return void
     */
    public function setGroup($val);

    /**
     * @return string mixed
     */
    public function getDescription();

    /**
     * @param string $val
     * @return void
     */
    public function setDescription($val);

    /**
     * @return string mixed
     */
    public function getFile();

    /**
     * @param string $val
     * @return void
     */
    public function setFile($val);

    /**
     * @return string mixed
     */
    public function getUrl();

    /**
     * @param string $val
     * @return void
     */
    public function setUrl($val);

}
