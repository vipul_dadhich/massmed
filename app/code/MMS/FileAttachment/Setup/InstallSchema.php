<?php

namespace MMS\FileAttachment\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Zend_Db_Exception;

/**
 * Class InstallSchema
 * @package MMS\FileAttachment\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /*
         * Drop tables if exists
         */
        $installer->getConnection()->dropTable($installer->getTable('fileattachment'));

        /**
         * Creating table fileattachment
         */
        $tableFileAttachment = $installer->getConnection()->newTable($installer->getTable('fileattachment'));

        $tableFileAttachment->addColumn(
            'file_attach_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Entity Id'
        );

        $tableFileAttachment->addColumn(
            'name',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Name'
        );

        $tableFileAttachment->addColumn(
            'description',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => null],
            'Description'
        );

        $tableFileAttachment->addColumn(
            'file',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => null],
            'Content'
        );

        $tableFileAttachment->addColumn(
            'file_ext',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => null],
            'File Extension'
        );

        $tableFileAttachment->addColumn(
            'url',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true, 'default' => null],
            'FileAttachment image media path'
        );

        $tableFileAttachment->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
            'Created At'
        );

        $tableFileAttachment->addColumn(
            'updated_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => true, 'default' => Table::TIMESTAMP_INIT_UPDATE],
            'Updated At'
        )->setComment('File Attachment item');

        $installer->getConnection()->createTable($tableFileAttachment);

        $installer->endSetup();
    }
}
