<?php


namespace MMS\FileAttachment\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.2.0', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable("fileattachment"),
                'filepath',
                [
                    "type" => Table::TYPE_TEXT,
                    'length' => '255',
                    'default' => '',
                    'nullable' => true,
                    'after' => 'name',
                    "comment" => "File Path"
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable("fileattachment"),
                'group',
                [
                    "type" => Table::TYPE_TEXT,
                    'length' => '255',
                    'default' => '',
                    'nullable' => true,
                    'after' => 'filepath',
                    "comment" => "Group Name"
                ]
            );
        }

        $setup->endSetup();
    }
}
