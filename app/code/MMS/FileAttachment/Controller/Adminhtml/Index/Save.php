<?php

namespace MMS\FileAttachment\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use MMS\FileAttachment\Helper\Data;
use MMS\FileAttachment\Model\FileAttachment;
use RuntimeException;

/**
 * Class Save
 * @package MMS\FileAttachment\Controller\Adminhtml\Index
 */
class Save extends Action
{
    /**
     * @var PostDataProcessor
     */
    private $dataProcessor;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var FileAttachment
     */
    private $attachModel;

    /**
     * @var Session
     */
    private $backSession;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param PostDataProcessor $dataProcessor
     * @param FileAttachment $attachModel
     * @param Data $helper
     */
    public function __construct(
        Action\Context $context,
        PostDataProcessor $dataProcessor,
        FileAttachment $attachModel,
        Data $helper
    )
    {
        $this->dataProcessor = $dataProcessor;
        $this->attachModel = $attachModel;
        $this->backSession = $context->getSession();
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('MMS_FileAttachment::save');
    }

    /**
     * Save action
     *
     * @return void
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $data = $this->dataProcessor->filter($data);

            $uploadedFile = '';
            $model = $this->attachModel;
            $id = $this->getRequest()->getParam('file_attach_id');

            if ($id) {
                $model->load($id);
                $uploadedFile = $model->getFile();
            }

            $model->addData($data);

            if (!$this->dataProcessor->validate($data)) {
                $this->_redirect('*/*/edit', ['file_attach_id' => $model->getId(), '_current' => true]);
                return;
            }

            try {
                $model = $this->helper->uploadFile('file', $model);
                $model->save();
                $this->messageManager->addSuccess(__('Attachment has been saved.'));
                $this->backSession->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['file_attach_id' => $model->getId(), '_current' => true]);
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch(\Magento\Framework\Exception\LocalizedException $ex) {
                $this->messageManager->addError($ex->getMessage());
            } catch (\Magento\Framework\Model\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the attachment.'));
            }

            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', ['file_attach_id' => $this->getRequest()->getParam('file_attach_id')]);
            return;
        }
        $this->_redirect('*/*/');
    }
}
