<?php

namespace MMS\FileAttachment\Controller\Adminhtml\Index;

/**
 * Class Grid
 * @package MMS\FileAttachment\Controller\Adminhtml\Index
 */
class Grid extends \Magento\Customer\Controller\Adminhtml\Index
{
    /**
     * Customer grid action
     *
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout(false);
        $this->_view->renderLayout();
    }
}
