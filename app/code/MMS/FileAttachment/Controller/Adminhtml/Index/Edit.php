<?php

namespace MMS\FileAttachment\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use MMS\FileAttachment\Model\FileAttachment;
use MMS\FileAttachment\Helper\Data as DataHelper;

/**
 * Class Edit
 * @package MMS\FileAttachment\Controller\Adminhtml\Index
 */
class Edit extends Action
{
    /**
     * Core registry
     *
     * @var Registry
     */
    private $coreRegistry = null;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var FileAttachment
     */
    private $attachModel;

    /**
     * @var  Session
     */
    private $backSession;
    /**
     * @var DataHelper
     */
    protected $dataHelper;

    /**
     * @param Action\Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param FileAttachment $attachModel
     * @param DataHelper $dataHelper
     */
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        FileAttachment $attachModel,
        DataHelper $dataHelper
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        $this->attachModel = $attachModel;
        $this->dataHelper = $dataHelper;
        $this->backSession = $context->getSession();
        parent::__construct($context);
    }

    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('MMS_FileAttachment::manage');
    }

    /**
     * Init actions
     *
     * @return Page
     */
    public function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(
            'MMS_FileAttachment::fileattachment_manage'
        )->addBreadcrumb(
            __('Attachment'),
            __('Attachment')
        )->addBreadcrumb(
            __('Manage Attachment'),
            __('Manage Attachment')
        );
        return $resultPage;
    }

    /**
     * Edit CMS page
     *
     * @return Page|Redirect
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('file_attach_id');
        $model = $this->attachModel;

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This Attachment no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
            $this->coreRegistry->register('file_attach_id', $model->getId());
        }

        $data = $this->backSession->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        $this->coreRegistry->register('fileattach', $model);

        $upload_maxsize = $this->dataHelper->getUploadMaxSize();
        $this->coreRegistry->register('fileattach_upload_size', $upload_maxsize);

        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Attachment') : __('New Attachment'),
            $id ? __('Edit Attachment') : __('New Attachment')
        );
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getName() : __('New Attachment'));
        return $resultPage;
    }
}
