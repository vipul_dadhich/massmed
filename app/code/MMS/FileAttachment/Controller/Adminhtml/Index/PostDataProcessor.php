<?php

namespace MMS\FileAttachment\Controller\Adminhtml\Index;

use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Zend_Filter_Input;

/**
 * Class PostDataProcessor
 * @package MMS\FileAttachment\Controller\Adminhtml\Index
 */
class PostDataProcessor
{
    /**
     * @var Date
     */
    private $dateFilter;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * PostDataProcessor constructor.
     * @param Date $dateFilter
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        Date $dateFilter,
        ManagerInterface $messageManager
    )
    {
        $this->dateFilter = $dateFilter;
        $this->messageManager = $messageManager;
    }

    /**
     * Filtering posted data. Converting localized data if needed
     *
     * @param array $data
     * @return array
     */
    public function filter($data)
    {
        $inputFilter = new Zend_Filter_Input(
            ['updated_at' => $this->dateFilter],
            [],
            $data
        );
        $data = $inputFilter->getUnescaped();
        return $data;
    }

    /**
     * Validate post data
     *
     * @return bool     Return FALSE if someone item is invalid
     */
    public function validate()
    {
        return true;
    }
}
