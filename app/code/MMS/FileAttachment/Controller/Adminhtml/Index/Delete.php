<?php

namespace MMS\FileAttachment\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use MMS\FileAttachment\Model\FileAttachment;

/**
 * Class Delete
 * @package MMS\FileAttachment\Controller\Adminhtml\Index
 */
class Delete extends Action
{
    /**
     * @var FileAttachment
     */
    private $attachModel;

    /**
     * @param Action\Context $context
     * @param FileAttachment $attachModel
     */
    public function __construct(
        Action\Context $context,
        FileAttachment $attachModel
    )
    {
        $this->attachModel = $attachModel;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('MMS_FileAttachment::delete');
    }

    /**
     * Delete action
     *
     * @return Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('file_attach_id');
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                // init model and delete
                $model = $this->attachModel;
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccess(__('The data has been deleted.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['page_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a data to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
