<?php

namespace MMS\FileAttachment\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use MMS\FileAttachment\Model\FileAttachment;
use MMS\FileAttachment\Helper\Data;

/**
 * Class DeleteFile
 * @package MMS\FileAttachment\Controller\Adminhtml\Index
 */
class DeleteFile extends Action
{
    /**
     * @var FileAttachment
     */
    private $attachModel;

    /**
     * @var File
     */
    private $file;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @param Action\Context $context
     * @param FileAttachment $attachModel
     * @param File $file
     * @param Filesystem $fileSystem
     */
    public function __construct(
        Action\Context $context,
        FileAttachment $attachModel,
        File $file,
        Filesystem $fileSystem
    )
    {
        $this->attachModel = $attachModel;
        $this->file = $file;
        $this->fileSystem = $fileSystem;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('MMS_FileAttachment::delete');
    }

    /**
     * Delete action
     *
     * @return Redirect
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('file_attach_id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->attachModel;
                $model->load($id);
                $currentFile = $model->getFile();
                $mediaDirectory = $this->fileSystem
                    ->getDirectoryRead(DirectoryList::MEDIA);
                $fileRootDir = $mediaDirectory->getAbsolutePath() . Data::MEDIA_PATH;
                if ($filePath = $model->getData('filepath')) {
                    $filePath = ltrim($filePath, '/');
                    $filePath = rtrim($filePath, '/');
                    $fileRootDir .= '/' . $filePath . '/';
                }

                if ($this->file->isExists($fileRootDir . $currentFile)) {
                    $this->file->deleteFile($fileRootDir . $currentFile);
                    $model->setFile('');
                    $model->save();
                    $this->messageManager->addSuccess(__('The file has been deleted.'));
                }
                return $resultRedirect->setPath('*/*/edit', ['file_attach_id' => $id]);
            } catch (Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['file_attach_id' => $id]);
            }
        }
    }
}
