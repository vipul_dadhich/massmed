<?php

namespace MMS\FileAttachment\Controller\AbstractController;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use MMS\FileAttachment\Model\FileAttachmentFactory;

class FileAttachmentLoader implements FileAttachmentLoaderInterface
{
    /**
     * @var FileAttachmentFactory
     */
    private $fileAttachFactory;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @param FileAttachmentFactory $fileAttachmentFactory
     * @param Registry $registry
     * @param UrlInterface $url
     */
    public function __construct(
        FileAttachmentFactory $fileAttachmentFactory,
        Registry $registry,
        UrlInterface $url
    )
    {
        $this->fileAttachFactory = $fileAttachmentFactory;
        $this->registry = $registry;
        $this->url = $url;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return bool
     */
    public function load(RequestInterface $request, ResponseInterface $response)
    {
        $id = (int)$request->getParam('id');
        if (!$id) {
            $request->initForward();
            $request->setActionName('noroute');
            $request->setDispatched(false);
            return false;
        }

        $fileAttachment = $this->fileAttachFactory->create()->load($id);
        $this->registry->register('current_fileattach', $fileAttachment);
        return true;
    }
}
