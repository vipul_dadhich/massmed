<?php

namespace MMS\FileAttachment\Controller\AbstractController;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use MMS\FileAttachment\Model\FileAttachment;

/**
 * Interface FileAttachmentLoaderInterface
 * @package MMS\FileAttachment\Controller\AbstractController
 */
interface FileAttachmentLoaderInterface
{
    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return FileAttachment
     */
    public function load(RequestInterface $request, ResponseInterface $response);
}
