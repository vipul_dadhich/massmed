<?php

namespace MMS\FileAttachment\Controller\AbstractController;

use Magento\Framework\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class View
 * @package MMS\FileAttachment\Controller\AbstractController
 */
abstract class View extends Action\Action
{
    /**
     * @var FileAttachmentLoaderInterface
     */
    private $fileAttachmentLoader;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param FileAttachmentLoaderInterface $fileAttachmentLoader
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Action\Context $context,
        FileAttachmentLoaderInterface $fileAttachmentLoader,
        PageFactory $resultPageFactory
    )
    {
        $this->fileAttachmentLoader = $fileAttachmentLoader;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * FileAttachment view page
     *
     * @return ResponseInterface|ResultInterface|Page|void
     */
    public function execute()
    {
        if (!$this->fileAttachmentLoader->load($this->_request, $this->_response)) {
            return;
        }

        return $this->resultPageFactory->create();
    }
}
