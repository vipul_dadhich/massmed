<?php

namespace MMS\CustomTinyMce\Plugin;


class Config
{

 protected $activeEditor;
 /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    private $assetRepo;

    /**
     * @param \Magento\Framework\View\Asset\Repository $assetRepo
     */

    public function __construct(\Magento\Ui\Block\Wysiwyg\ActiveEditor $activeEditor,\Magento\Framework\View\Asset\Repository $assetRepo)
    {
        $this->activeEditor = $activeEditor;
        $this->assetRepo = $assetRepo;
    }

    /**
     * Return WYSIWYG configuration
     *
     * @param \Magento\Ui\Component\Wysiwyg\ConfigInterface $configInterface
     * @param \Magento\Framework\DataObject $result
     * @return \Magento\Framework\DataObject
     */
    public function afterGetConfig(
        \Magento\Ui\Component\Wysiwyg\ConfigInterface $configInterface,
        \Magento\Framework\DataObject $result
    ) {

        // Get current wysiwyg adapter's path
        $editor = $this->activeEditor->getWysiwygAdapterPath();

        // Is the current wysiwyg tinymce v4?
        if (strpos($editor,'tinymce4Adapter')) {

            $resultArr = $result->getData();
            if (isset($resultArr['settings'])) {
                $settings = $resultArr['settings'];
                $settings['menubar'] = true;
                $settings['toolbar'] = 'undo redo | styleselect | fontsizeselect | fontselect | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | image | hr | code';
                $settings['plugins'] = 'textcolor colorpicker image code lists hr table importcss';
                $settings['font_formats'] = "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats; LyonDisplay Medium Web=lyondisplay medium web; LyonText Regular Web=lyontext regular web; ff-scala-sans-web-pro = ff-scala-sans-web-pro;";

                $settings['importcss_append'] = true;

                $settings['style_formats'] = array(
                    array( "title" => 'Headings', "items" => array(
                      array( "title" => 'Heading 1', "format"=> 'h1' ),
                      array( "title" => 'Heading 2', "format"=> 'h2' ),
                      array( "title"=> 'Heading 3', "format"=> 'h3' ),
                      array( "title"=> 'Heading 4', "format"=> 'h4' ),
                      array( "title"=> 'Heading 5', "format"=> 'h5' ),
                      array( "title"=> 'Heading 6', "format"=> 'h6' )
                            )),
                    array( "title"=> 'Inline', "items"=> array(
                      array( "title"=> 'Bold', "format"=> 'bold' ),
                      array( "title"=> 'Italic', "format"=> 'italic' ),
                      array( "title"=> 'Underline', "format"=> 'underline' ),
                      array( "title"=> 'Strikethrough', "format"=> 'strikethrough' ),
                      array( "title"=> 'Superscript', "format"=> 'superscript' ),
                      array( "title"=> 'Subscript', "format"=> 'subscript' ),
                      array( "title"=> 'Code', "format"=> 'code' )
                    )),
                    array( "title"=> 'Blocks', "items"=> array(
                      array( "title"=> 'Paragraph', "format"=> 'p' ),
                      array( "title"=> 'Blockquote', "format"=> 'blockquote' ),
                      array( "title"=> 'Div', "format"=> 'div' ),
                      array( "title"=> 'Pre', "format"=> 'pre' ),
                    )),
                    array( "title"=> 'Align', "items"=> array(
                      array( "title"=> 'Left', "format"=> 'alignleft' ),
                      array( "title"=> 'Center', "format"=> 'aligncenter' ),
                      array( "title"=> 'Right', "format"=> 'alignright' ),
                      array( "title"=> 'Justify', "format"=> 'alignjustify' )
                    ))
                    );
                
                //We need this section for future use so i am keeping this code

                /*    $formatcustom = array("customformat" => array( 
                        "inline" => 'span', 
                        "styles" => array( 
                            "background-color" => '#f5fbfa',
                            // "border-top" => '1px solid #009977',
                            "margin-bottom"=>  '25px',
                            "fontSize"=> '18px' ,
                            "color"=> '#333',
                            "font-family" => "'ff-scala-sans-pro'"), 
                        "attributes" => array( "title" => 'Custom Colored Block') , "classes" => 'custom_box'));
                */

                // $settings['formats'] = $formatcustom;
                  

                $result->setData('settings', $settings);
            }
            
            if (isset($resultArr['tinymce4'])) {
                $tinymce4 = $resultArr['tinymce4'];
                array_push($tinymce4['content_css'], $this->assetRepo->getUrl('MMS/CustomTinyMce/view/adminhtml/web/css/custom_fonts.css'));

                $result->setData('tinymce4', $tinymce4);
            }     
        }

        return $result;
    }
}