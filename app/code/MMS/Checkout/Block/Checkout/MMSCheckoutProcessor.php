<?php

namespace MMS\Checkout\Block\Checkout;

use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Ui\Component\Form\AttributeMapper;
use Magento\Checkout\Block\Checkout\AttributeMerger;
use Magento\Customer\Model\AttributeMetadataDataProvider;

/**
 * Class MMSCheckoutProcessor
 * @package MMS\Checkout\Block\Checkout
 */
class MMSCheckoutProcessor implements LayoutProcessorInterface
{
    /**
     * @var null
     */
    public $quote = null;
    /**
     * @var AttributeMetadataDataProvider
     */
    public $attributeMetadataDataProvider;
    /**
     * @var AttributeMapper
     */
    public $attributeMapper;

    /**
     * @var AttributeMerger
     */
    public $merger;
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * MMSCheckoutProcessor constructor.
     * @param AttributeMetadataDataProvider $attributeMetadataDataProvider
     * @param AttributeMapper $attributeMapper
     * @param AttributeMerger $merger
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        AttributeMetadataDataProvider $attributeMetadataDataProvider,
        AttributeMapper $attributeMapper,
        AttributeMerger $merger,
        CheckoutSession $checkoutSession
    ) {
        $this->attributeMetadataDataProvider = $attributeMetadataDataProvider;
        $this->attributeMapper = $attributeMapper;
        $this->merger = $merger;
        $this->checkoutSession = $checkoutSession;
    }


    /**
     * {@inheritdoc}
     */
    public function process($jsLayout)
    {
        if (!$this->getQuote()->isVirtual()) {
            $jsLayout = $this->moveBillingAddress($jsLayout);
            $jsLayout = $this->changeStepsSortOrder($jsLayout);
        }
        return $jsLayout;
    }

    /**
     * Move Billing Address After Shipping Address
     *
     * @param array $jsLayout
     * @return array
     */
    private function moveBillingAddress($jsLayout)
    {
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset'])) {
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['firstname']['validation']['required-entry'] = false;
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['lastname']['validation']['required-entry'] = false;
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['country_id']['sortOrder'] = 0;
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['street']['sortOrder'] = 1;
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['city']['sortOrder'] = 2;
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['region']['sortOrder'] = 3;
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['region_id']['sortOrder'] = 4;
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['postcode']['sortOrder'] = 5;
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['vat_id']['sortOrder'] = 6;

            if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                ['payment']['children']['afterMethods']['children']['billing-address-form'])) {
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
                ['children']['shippingAddress']['children']['billing-address'] = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                ['payment']['children']['afterMethods']['children']['billing-address-form'];
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
                ['children']['shippingAddress']['children']['billing-address']['component'] = 'Ey_Checkout/js/view/billing-address';
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
                ['children']['shippingAddress']['children']['billing-address']['displayArea'] = 'billing-address';
                unset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                    ['payment']['children']['afterMethods']['children']['billing-address-form']);
            }
            /*$jsLayout = $this->removeBillingAddressInPaymentMethod($jsLayout);
            $elements = $this->getAddressAttributes();
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['billing-address'] = $this->getCustomBillingAddressComponent($elements);*/
        }

        return $jsLayout;
    }

    /**
     * Prepare billing address field for shipping step for physical product
     *
     * @param $elements
     * @return array
     */
    public function getCustomBillingAddressComponent($elements)
    {

        $fields = [
            'component' => 'MMS_Checkout/js/view/billing-address',
            'displayArea' => 'billing-address',
            'provider' => 'checkoutProvider',
            'deps' => ['checkoutProvider'],
            'dataScopePrefix' => 'billingAddress',
            'children' => [
                'form-fields' => [
                    'component' => 'uiComponent',
                    'displayArea' => 'additional-fieldsets',
                    'children' => $this->merger->merge(
                        $elements,
                        'checkoutProvider',
                        'billingAddress',
                        [
                            'country_id' => [
                                'sortOrder' => 115,
                            ],
                            'region' => [
                                'visible' => false,
                            ],
                            'region_id' => [
                                'component' => 'Magento_Ui/js/form/element/region',
                                'config' => [
                                    'template' => 'ui/form/field',
                                    'elementTmpl' => 'ui/form/element/select',
                                    'customEntry' => 'billingAddress.region',
                                ],
                                'validation' => [
                                    'required-entry' => true,
                                ],
                                'filterBy' => [
                                    'target' => '${ $.provider }:${ $.parentScope }.country_id',
                                    'field' => 'country_id',
                                ],
                            ],
                            'postcode' => [
                                'component' => 'Magento_Ui/js/form/element/post-code',
                                'validation' => [
                                    'required-entry' => true,
                                ],
                            ],
                            'company' => [
                                'validation' => [
                                    'min_text_length' => 0,
                                ],
                            ],
                            'fax' => [
                                'validation' => [
                                    'min_text_length' => 0,
                                ],
                            ],
                            'telephone' => [
                                'config' => [
                                    'tooltip' => [
                                        'description' => __('For delivery questions.'),
                                    ],
                                ],
                            ],
                        ]
                    ),
                ],
            ],
        ];
        foreach ($elements as $attributeCode => $attribute) {
            if (!empty($attribute['is_user_defined'])) {
                $fields['children']['form-fields']['children'][$attributeCode]['config']['customScope'] = 'billingAddress.custom_attributes';
                $fields['children']['form-fields']['children'][$attributeCode]['dataScope'] = 'billingAddress.custom_attributes.' . $attributeCode;

            }
        }
        return $fields;
    }

    /**
     * Get all visible address attribute
     *
     * @return array
     */
    private function getAddressAttributes()
    {
        /** @var \Magento\Eav\Api\Data\AttributeInterface[] $attributes */
        $attributes = $this->attributeMetadataDataProvider->loadAttributesCollection(
            'customer_address',
            'customer_register_address'
        );
        $elements = [];
        foreach ($attributes as $attribute) {
            $code = $attribute->getAttributeCode();
            /*
            if ($attribute->getIsUserDefined()) {
                continue;
            }*/
            $elements[$code] = $this->attributeMapper->map($attribute);
            if (isset($elements[$code]['label'])) {
                $label = $elements[$code]['label'];
                $elements[$code]['label'] = __($label);
            }
            if ($attribute->getIsUserDefined()) {
                $elements[$code]['is_user_defined'] = true;
                if ($elements[$code]['formElement'] == 'checkbox' && $elements[$code]['dataType'] == 'boolean') {
                    $elements[$code]['formElement'] = 'select';
                }
            }
        }
        return $elements;
    }

    /**
     * @param $jsLayout
     * @return mixed
     */
    private function removeBillingAddressInPaymentMethod($jsLayout)
    {
        unset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
            ['payment']['children']['payments-list']['children']);
        return $jsLayout;
    }

    /**
     * Changes steps sort order.
     *
     * @param array $jsLayout
     * @return array
     */
    private function changeStepsSortOrder($jsLayout)
    {
        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['sortOrder'] = 2;
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['sortOrder'] = 1;
        $jsLayout['components']['checkout']['children']['steps']['children']['summary-step']['sortOrder'] = 3;
        return $jsLayout;
    }

    /**
     * Get Quote
     *
     * @return \Magento\Quote\Model\Quote|null
     */
    public function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }

        return $this->quote;
    }
}
