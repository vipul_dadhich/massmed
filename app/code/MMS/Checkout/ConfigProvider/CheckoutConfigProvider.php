<?php

namespace MMS\Checkout\ConfigProvider;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

/**
 * Class CheckoutConfigProvider
 * @package MMS\Checkout\ConfigProvider
 */
class CheckoutConfigProvider implements ConfigProviderInterface
{
    /**
     * @var null
     */
    public $quote = null;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * CheckoutConfigProvider constructor.
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        CheckoutSession $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $isVirtual = $this->getQuote()->isVirtual();
        if ($isVirtual) {
            return [];
        }
        return [
            'mmsCheckout' => [
                'hideShippingMethods' => true,
                'hideShippingTitle' => true,
                'moveBillingAddressBeforeShippingAddressEnabled' => true,
                'isPayBillCheckout' => (bool) $this->getQuote()->getData('is_paybill')
            ]
        ];
    }

    /**
     * Get Quote
     *
     * @return \Magento\Quote\Model\Quote|null
     */
    public function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }

        return $this->quote;
    }
}
