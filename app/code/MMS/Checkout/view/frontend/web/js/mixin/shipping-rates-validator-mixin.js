define([
    'jquery',
    'ko',
    'Magento_Checkout/js/model/shipping-rates-validation-rules',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/postcode-validator',
    'Ey_Checkout/js/model/tax-calculation',
    'Ey_Checkout/js/model/postcode-validator',
    'Magento_Checkout/js/model/default-validator',
    'mage/translate',
    'uiRegistry',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/model/quote'
], function (
    $,
    ko,
    shippingRatesValidationRules,
    addressConverter,
    selectShippingAddress,
    postcodeValidator,
    taxCalculation,
    eyPostcodeValidator,
    defaultValidator,
    $t,
    uiRegistry,
    formPopUpState
) {
    'use strict';

    var checkoutConfig = window.checkoutConfig,
        validators = [],
        observedElements = [],
        postcodeElements = [],
        postcodeElementName = 'postcode',
        cityElementName = 'city',
        vatIdElementName = 'vat_id';

    validators.push(defaultValidator);

    return function (target) {
        if (typeof (window.checkoutConfig.mmsCheckout) == 'undefined') {
            return target;
        }

        return {
            validateAddressTimeout: 0,
            validateZipCodeTimeout: 0,
            validateDelay: 2000,

            /**
             * @param {String} carrier
             * @param {Object} validator
             */
            registerValidator: function (carrier, validator) {
                if (checkoutConfig.activeCarriers.indexOf(carrier) !== -1) {
                    validators.push(validator);
                }
            },

            /**
             * @param {Object} address
             * @return {Boolean}
             */
            validateAddressData: function (address) {
                if (!taxCalculation.validate(address, true)) {
                    return false;
                }
                return validators.some(function (validator) {
                    return validator.validate(address);
                });
            },

            /**
             * Perform postponed binding for fieldset elements
             *
             * @param {String} formPath
             */
            initFields: function (formPath) {
                var self = this,
                    elements = shippingRatesValidationRules.getObservableFields();

                if ($.inArray(postcodeElementName, elements) === -1) {
                    // Add postcode field to observables if not exist for zip code validation support
                    elements.push(postcodeElementName);
                }

                if ($.inArray(cityElementName, elements) === -1) {
                    elements.push(cityElementName);
                }

                if ($.inArray(vatIdElementName, elements) === -1) {
                    elements.push(vatIdElementName);
                }

                $.each(elements, function (index, field) {
                    uiRegistry.async(formPath + '.' + field)(self.doElementBinding.bind(self));
                });
            },

            /**
             * Bind shipping rates request to form element
             *
             * @param {Object} element
             * @param {Boolean} force
             * @param {Number} delay
             */
            doElementBinding: function (element, force, delay) {
                var observableFields = shippingRatesValidationRules.getObservableFields();
                if ($.inArray(vatIdElementName, observableFields) === -1) {
                    observableFields.push(vatIdElementName);
                }

                if ($.inArray(cityElementName, observableFields) === -1) {
                    observableFields.push(cityElementName);
                }

                if (element && (observableFields.indexOf(element.index) !== -1 || force)) {
                    if (element.index !== postcodeElementName) {
                        this.bindHandler(element, delay);
                    }
                }

                if (element.index === postcodeElementName) {
                    this.bindHandler(element, delay);
                    postcodeElements.push(element);
                }
            },

            /**
             * @param {*} elements
             * @param {Boolean} force
             * @param {Number} delay
             */
            bindChangeHandlers: function (elements, force, delay) {
                var self = this;

                $.each(elements, function (index, elem) {
                    self.doElementBinding(elem, force, delay);
                });
            },

            /**
             * @param {Object} element
             * @param {Number} delay
             */
            bindHandler: function (element, delay) {
                var self = this;

                delay = typeof delay === 'undefined' ? self.validateDelay : delay;

                if (element.component.indexOf('/group') !== -1) {
                    $.each(element.elems(), function (index, elem) {
                        self.bindHandler(elem);
                    });
                } else {
                    element.on('value', function () {
                        clearTimeout(self.validateZipCodeTimeout);
                        self.validateZipCodeTimeout = setTimeout(function () {
                            if (element.index === postcodeElementName) {
                                self.postcodeValidation(element);
                            } else {
                                $.each(postcodeElements, function (index, elem) {
                                    self.postcodeValidation(elem);
                                });
                            }
                        }, delay);

                        if (!formPopUpState.isVisible()) {
                            clearTimeout(self.validateAddressTimeout);
                            self.validateAddressTimeout = setTimeout(function () {
                                self.validateFields();
                            }, delay);
                        }
                    });
                    observedElements.push(element);
                }
            },

            /**
             * @return {*}
             */
            postcodeValidation: function (postcodeElement) {
                var countryId = $('div[name="shippingAddress\\.country_id"] select[name="country_id"]:visible').val(),
                    validationResult,
                    errorMessage;

                if (postcodeElement == null || postcodeElement.value() == null) {
                    return true;
                }

                postcodeElement.error(null);
                postcodeElement.warn(null);
                if (countryId === 'US') {
                    let val = postcodeElement.value();
                    if(val.length > 5) {
                        let digits = val.split(/(\d{1,5})/);
                        let str = "";
                        for (let group of digits)
                        {
                            if (/^\d+$/.test(group))
                            {
                                str += group + "-";
                            }
                        }
                        str = str.substring(0, str.length - 1);
                        postcodeElement.value(str);
                    }
                }
                validationResult = eyPostcodeValidator.validate(postcodeElement.value(), countryId);
                if (!validationResult) {
                    errorMessage = $t('Please enter a valid Zip/Postal Code for your region.');
                    postcodeElement.error(errorMessage);
                }

                return validationResult;
            },

            /**
             * Convert form data to quote address and validate fields for shipping rates
             */
            validateFields: function () {
                var addressFlat = addressConverter.formDataProviderToFlatData(
                    this.collectObservedData(),
                    'shippingAddress'
                    ),
                    address;

                if (this.validateAddressData(addressFlat)) {
                    addressFlat = uiRegistry.get('checkoutProvider').shippingAddress;
                    address = addressConverter.formAddressDataToQuoteAddress(addressFlat);
                    selectShippingAddress(address);
                }
            },

            /**
             * Collect observed fields data to object
             *
             * @returns {*}
             */
            collectObservedData: function () {
                var observedValues = {};

                $.each(observedElements, function (index, field) {
                    observedValues[field.dataScope] = field.value();
                });

                return observedValues;
            }
        };
    }
});
