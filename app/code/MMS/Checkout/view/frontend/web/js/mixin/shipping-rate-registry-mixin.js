define([
], function () {
    'use strict';

    /**
     * Disable shipping rate cache
     */
    return function (target) {
        if (typeof (window.checkoutConfig.mmsCheckout) == 'undefined') {
            return target;
        }

        target.get = function (key)  {
            return false;
        }

        return target;
    }
});
