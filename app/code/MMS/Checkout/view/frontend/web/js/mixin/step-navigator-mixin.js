define([

], function (

) {
    'use strict';
    return function (target) {
        var registerStep = target.registerStep;
        target.registerStep = function (code, alias, title, isVisible, navigate, sortOrder) {
            if (typeof (window.checkoutConfig.mmsCheckout) == 'object' && code === 'shipping') {
                return true;
            }
            return registerStep.apply(this, arguments);
        }
        return target;
    }
});
