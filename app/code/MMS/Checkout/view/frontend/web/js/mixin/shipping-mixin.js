define([
    'ko',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/shipping-save-processor/default',
    'Magento_Checkout/js/action/create-billing-address',
    'Magento_Checkout/js/action/select-billing-address',
    'Magento_Checkout/js/action/get-payment-information',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/checkout-data',
    'mage/translate',
    'jquery',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'MMS_Checkout/js/model/quote-extend',
    'underscore'
], function (
    ko,
    customer,
    addressConverter,
    quote,
    selectShippingAddress,
    shippingSaveProcessor,
    createBillingAddress,
    selectBillingAddress,
    getPaymentInformation,
    addressList,
    checkoutData,
    $t,
    $,
    checkoutDataResolver,
    quoteExtend,
    _
) {
    'use strict';

    return function (target) {
        if (typeof (window.checkoutConfig.mmsCheckout) == 'undefined') {
            return target.extend({});
        }
        return target.extend({
            /**
             * Override shipping template so we can hide rates when there's only 1 available.
             */
            defaults: {
                template: 'MMS_Checkout/shipping',
                shippingFormTemplate: 'Magento_Checkout/shipping-address/form',
                shippingMethodListTemplate: 'MMS_Checkout/shipping-address/shipping-method-list',
                shippingMethodItemTemplate: 'MMS_Checkout/shipping-address/shipping-method-item'
            },

            /**
             * Disable visibility on shipping, since it's no longer the first step.
             */
            //visible: ko.observable(customer.isLoggedIn() && !quote.isVirtual()),
            visibleAccountInformation: ko.observable(!customer.isLoggedIn()),
            canUseBillingAddress: ko.observable(true),
            isAddressSameAsBilling: ko.observable(true),
            isAddressSameAsBillingByEngine: ko.observable(false),
            quoteIsVirtual: ko.observable(quote.isVirtual()),
            initialize: function () {
                this._super();
                var self = this;

                quoteExtend.isAddressSameAsBilling(self.isAddressSameAsBilling());

                getPaymentInformation();

                this.isAddressSameAsBilling.subscribe(function (value) {
                    quoteExtend.isAddressSameAsBilling(value);
                    if (!self.isAddressSameAsBillingByEngine()) {
                        if (value) {
                            self.useBillingForShipping();
                        } else {
                            self.useShippingForShipping();
                        }
                    }
                });

                if (this.isBillingBeforeShipping()) {
                    var previousBillingAddress;
                    quote.billingAddress.subscribe(function (_oldValue) {
                        if (self.isAddressSameAsBilling()) {
                            previousBillingAddress = _oldValue;
                        }
                    }, this, 'beforeChange');
                    quote.billingAddress.subscribe(function (newAddress) {
                        if (self.isAddressSameAsBilling() && !_.isEqual(previousBillingAddress, newAddress)) {
                            self.useBillingForShipping();
                            previousBillingAddress = newAddress;
                        }
                    });
                }

                if (this.isBillingBeforeShipping() && 0) {
                    window.timeCheckBillingFormExist = setInterval(function () {
                        if ($('#billing-new-address-form').find('input,select').length) {
                            $('#billing-new-address-form').find('input,select').change(function () {
                                if (self.isAddressSameAsBilling()) {
                                    self.useBillingForShipping();
                                }
                            });
                            clearInterval(window.timeCheckBillingFormExist);
                        }
                        if ($('[data-form="billing-new-address"]').find('input,select').length) {
                            $('[data-form="billing-new-address"]').find('input,select').change(function () {
                                if (self.isAddressSameAsBilling()) {
                                    self.useBillingForShipping();
                                }
                            });
                            clearInterval(window.timeCheckBillingFormExist);
                        }
                    },1000)
                }
            },

            useBillingForShipping : function () {
                var self = this,
                    addressChanged = false,
                    addressData = quote.billingAddress(),
                    shippingAddress = quote.shippingAddress();

                if (!addressData) {
                    addressData = addressConverter.formAddressDataToQuoteAddress(
                        self.source.get('billingAddressshared')
                    );
                }

                //Copy form data to quote shipping address object
                for (var field in addressData) {
                    if (field === 'saveInAddressBook') {continue;}
                    if (addressData.hasOwnProperty(field) && //eslint-disable-line max-depth
                        shippingAddress.hasOwnProperty(field) &&
                        typeof addressData[field] != 'function' &&
                        _.isEqual(shippingAddress[field], addressData[field])
                    ) {
                        shippingAddress[field] = addressData[field];
                    } else if (typeof addressData[field] != 'function' &&
                        !_.isEqual(shippingAddress[field], addressData[field])) {
                        shippingAddress = addressData;
                        addressChanged = true;
                        break;
                    }
                }

                if (addressChanged) {
                    console.log(addressData);
                    selectShippingAddress(shippingAddress);
                }
            },

            useShippingForShipping : function () {
                let isShippingAddressInitialized;

                if (addressList().length > 0) {
                    if (checkoutData.getSelectedShippingAddress()) {
                        isShippingAddressInitialized = addressList.some(function (addressFromList) {
                            if (checkoutData.getSelectedShippingAddress() == addressFromList.getKey()) { //eslint-disable-line
                                selectShippingAddress(addressFromList);
                                return true;
                            }
                            return false;
                        });
                    }

                    if (!isShippingAddressInitialized) {
                        checkoutDataResolver.applyShippingAddress();
                    }
                    return;
                }
                var self = this;
                var shippingAddress = quote.shippingAddress();
                var addressData = addressConverter.formAddressDataToQuoteAddress(
                    self.source.get('shippingAddress')
                );

                //Copy form data to quote shipping address object
                for (var field in addressData) {
                    if (addressData.hasOwnProperty(field) && //eslint-disable-line max-depth
                        shippingAddress.hasOwnProperty(field) &&
                        typeof addressData[field] != 'function' &&
                        _.isEqual(shippingAddress[field], addressData[field])
                    ) {
                        shippingAddress[field] = addressData[field];
                    } else if (typeof addressData[field] != 'function' &&
                        !_.isEqual(shippingAddress[field], addressData[field])) {
                        shippingAddress = addressData;
                        break;
                    }
                }
                selectShippingAddress(shippingAddress);
            },

            shouldHideShipping: function () {
                return window.checkoutConfig.mmsCheckout.hideShippingMethods;
            },

            isBillingBeforeShipping: function () {
                return window.checkoutConfig.mmsCheckout.moveBillingAddressBeforeShippingAddressEnabled;
            },

            isPayBillCheckout: function () {
                return window.checkoutConfig.mmsCheckout.isPayBillCheckout;
            },

            /**
             * These steps don't set itself to visible on refresh.
             */
            navigate: function () {
                this.visible(true);
            },

            /**
             * Removed email validation as we do that in a previous step.
             */
            validateShippingInformation: function () {
                var validateResult=true,
                    shippingAddress,
                    addressData,
                    loginFormSelector = 'form[data-role=email-with-possible-login]',
                    emailValidationResult = customer.isLoggedIn(),
                    field;
                if (!quote.shippingMethod()) {
                    this.errorValidationMessage($t('Please specify a shipping method.'));

                    validateResult = validateResult && false;
                }

                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }


                if (window.checkoutConfig.mmsCheckout.isPayBillCheckout !== undefined
                    && window.checkoutConfig.mmsCheckout.isPayBillCheckout === true) {
                    this.useBillingForShipping();
                    return validateResult;
                }

                if (!this.isBillingBeforeShipping() || !$('[name="shipping-address-same-as-billing"]').is(":checked")) {
                    if (this.isFormInline) {
                        this.source.set('params.invalid', false);

                        if (typeof this.triggerShippingDataValidateEvent !== 'undefined') {
                            this.triggerShippingDataValidateEvent();
                        }

                        if (this.source.get('params.invalid')) {
                            this.focusInvalid();
                            validateResult = validateResult && false;
                        }

                        if (validateResult) {
                            let addressChanged = false;
                            shippingAddress = quote.shippingAddress();
                            addressData = addressConverter.formAddressDataToQuoteAddress(
                                this.source.get('shippingAddress')
                            );

                            //Copy form data to quote shipping address object
                            for (field in addressData) {
                                if (addressData.hasOwnProperty(field) && //eslint-disable-line max-depth
                                    shippingAddress.hasOwnProperty(field) &&
                                    typeof addressData[field] != 'function' &&
                                    _.isEqual(shippingAddress[field], addressData[field])
                                ) {
                                    shippingAddress[field] = addressData[field];
                                } else if (typeof addressData[field] != 'function' &&
                                    !_.isEqual(shippingAddress[field], addressData[field])) {
                                    shippingAddress = addressData;
                                    addressChanged = 1;
                                    break;
                                }
                            }

                            if (customer.isLoggedIn()) {
                                shippingAddress['save_in_address_book'] = 1;
                            }
                            if (addressChanged) {
                                selectShippingAddress(shippingAddress);
                            }
                        }
                    }

                }

                if (!emailValidationResult) {
                    $(loginFormSelector + ' input[name=username]').focus();

                    validateResult = validateResult && false;
                }

                if (!this.source.get('params.invalid') && !quote.shippingMethod()) {
                    $('html, body').animate({
                        scrollTop: $("#co-shipping-method-form").offset().top
                    }, 0);
                }
                return validateResult;
            },
            /**
             * @param {Object} shippingMethod
             * @return {Boolean}
             */
            selectShippingMethod: function (shippingMethod) {
                this._super(shippingMethod);
                shippingSaveProcessor.saveShippingInformation();
                return true;
            },

            validateBillingInformation: function () {
                var validateResult=true,
                    loginFormSelector = 'form[data-role=email-with-possible-login]',
                    emailValidationResult = customer.isLoggedIn();
                if (!quote.shippingMethod()) {
                    this.errorValidationMessage($t('Please specify a shipping method.'));

                    validateResult = validateResult && false;
                }

                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }
                if (!emailValidationResult) {
                    $(loginFormSelector + ' input[name=username]').focus();
                    validateResult = validateResult && false;
                }

                if ($('[name="billing-address-same-as-shipping"]').is(":checked") && !this.isBillingBeforeShipping()) {
                    selectBillingAddress(quote.shippingAddress());
                    validateResult = validateResult && true;
                } else {
                    var selectedAddress = checkoutData.getSelectedBillingAddress(),
                        newCustomerBillingAddressData = checkoutData.getNewCustomerBillingAddress();;
                    if (selectedAddress) {
                        var res = true;
                        if (selectedAddress === 'new-customer-billing-address' && newCustomerBillingAddressData) {
                            selectBillingAddress(createBillingAddress(newCustomerBillingAddressData));
                        } else {
                            res = addressList.some(function (addressFromList) {
                                if (selectedAddress === addressFromList.getKey()) {
                                    selectBillingAddress(addressFromList);
                                    return true;
                                }
                                return false;
                            });
                        }
                        validateResult = validateResult && res;
                    } else {

                        this.source.set('params.invalid', false);
                        this.source.trigger('billingAddressshared.data.validate');

                        if (this.source.get('params.invalid')) {
                            validateResult = validateResult && false;
                        }
                        var addressData = this.source.get('billingAddressshared'),
                            newBillingAddress;

                        if ($('#billing-save-in-address-book').is(":checked")) {
                            addressData.save_in_address_book = 1;
                        }

                        newBillingAddress = createBillingAddress(addressData);
                        selectBillingAddress(newBillingAddress);
                    }

                    validateResult = validateResult && true;
                }
                return validateResult;
            }
        });
    }
});
