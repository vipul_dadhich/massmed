/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'underscore',
    'uiComponent',
    'ko',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Checkout/js/model/payment-service',
    'Magento_Checkout/js/model/payment/method-converter',
    'Magento_Checkout/js/action/get-payment-information',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'mage/translate'
], function (
    $,
    _,
    Component,
    ko,
    quote,
    stepNavigator,
    paymentService,
    methodConverter,
    getPaymentInformation,
    checkoutDataResolver,
    $t
) {
    'use strict';

    return function (target) {
        if (typeof (window.checkoutConfig.mmsCheckout) == 'undefined') {
            return target.extend({});
        }
        return target.extend({
            /**
             * Navigate method.
             */
            navigate: function () {
                var self = this;

                if (!self.hasShippingMethod()) {
                    self.isVisible(true);
                } else {
                    getPaymentInformation().done(function () {
                        self.isVisible(true);
                    });
                }
            }
        });
    };
});
