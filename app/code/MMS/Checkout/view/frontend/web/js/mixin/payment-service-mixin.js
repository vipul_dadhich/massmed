define([
    'underscore',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/select-payment-method'
], function (_, wrapper, quote, selectPaymentMethod) {
    'use strict';

    return function (target) {
        if (typeof (window.checkoutConfig.mmsCheckout) == 'undefined') {
            return target;
        }

        target.setPaymentMethods = wrapper.wrap(target.setPaymentMethods, function (setPaymentMethods, methods) {
            return setPaymentMethods(methods);
        });

        return target;
    }
});
