/**
 * @api
 */
define([
    'ko',
    'underscore',
    'domReady!'
], function (ko, _) {
    'use strict';

    return {
        isAddressSameAsBilling: ko.observable(true)
    };
});
