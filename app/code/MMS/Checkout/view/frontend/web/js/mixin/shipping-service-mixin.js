define([
    'ko',
    'jquery',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/action/get-payment-information',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Checkout/js/model/shipping-save-processor/default',
    'Magento_Checkout/js/model/quote',
    'MMS_CurrencyEngine/js/model/currency-engine',
    'MMS_CurrencyEngine/js/action/currency-engine'
], function (
    ko,
    $,
    checkoutDataResolver,
    getPaymentInformation,
    getTotalsAction,
    shippingSaveProcessor,
    quote,
    currencyEngineModel,
    currencyEngineAction
) {
    'use strict';

    return function (target) {
        if (typeof (window.checkoutConfig.mmsCheckout) == 'undefined') {
            return target;
        }

        var shippingRates = ko.observableArray([]);

        return {
            isLoading: ko.observable(false),

            /**
             * Set Shipping rates
             * @param {*} ratesData
             */
            setShippingRates: function (ratesData) {
                shippingRates(ratesData);
                shippingRates.valueHasMutated();
                checkoutDataResolver.resolveShippingRates(ratesData);

                let engineCalled = false;
                if (this.isCurrencyEngineEnabled()) {
                    let shippingAddress = quote.shippingAddress();
                    if (currencyEngineModel.triggerShippingEvents()
                        && shippingAddress && currencyEngineModel.lastCountryId() !== shippingAddress.countryId) {
                        engineCalled = true;
                        currencyEngineAction(shippingAddress.countryId).always(function () {
                            var deferred = $.Deferred();
                            shippingSaveProcessor.saveShippingInformation().always(function () {
                                getTotalsAction([]);
                            });
                        });
                        currencyEngineModel.lastCountryId(shippingAddress.countryId);
                    }
                }

                if (!engineCalled) {
                    var deferred = $.Deferred();
                    shippingSaveProcessor.saveShippingInformation().always(function () {
                        getTotalsAction([]);
                    });
                }
            },

            /**
             * Get shipping rates
             *
             * @returns {*}
             */
            getShippingRates: function () {
                return shippingRates;
            },

            /**
             * @return {|Boolean}
             */
            isCurrencyEngineEnabled: function() {
                return !!window.checkoutConfig.currencyEngineEnabled;
            }
        };
    }
});
