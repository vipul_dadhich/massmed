define([
    'jquery',
    'ko',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/resource-url-manager',
    'mage/storage',
    'Magento_Checkout/js/model/payment-service',
    'Magento_Checkout/js/model/payment/method-converter',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/action/select-billing-address'
], function (
    $,
    ko,
    quote,
    resourceUrlManager,
    storage,
    paymentService,
    methodConverter,
    errorProcessor,
    fullScreenLoader,
    selectBillingAddressAction
) {
    'use strict';

    return {
        saveShippingInformation: function () {
            if (!quote.shippingMethod()) {
                return;
            }

            var payload,
                billingAddress = quote.billingAddress(),
                shippingAddress = quote.shippingAddress();

            if (billingAddress != null && billingAddress.trigger_tax !== undefined) {
                delete billingAddress.trigger_tax;
            }

            if (shippingAddress != null && shippingAddress.trigger_tax !== undefined) {
                delete shippingAddress.trigger_tax;
            }

            payload = {
                addressInformation: {
                    shipping_address: shippingAddress,
                    billing_address: billingAddress,
                    shipping_method_code: quote.shippingMethod().method_code,
                    shipping_carrier_code: quote.shippingMethod().carrier_code
                }
            }

            fullScreenLoader.startLoader();

            return storage.post(
                resourceUrlManager.getUrlForSetShippingInformation(quote),
                JSON.stringify(payload)
            ).done(
                function (response) {
                    quote.setTotals(response.totals);
                    paymentService.setPaymentMethods(methodConverter(response.payment_methods));
                    fullScreenLoader.stopLoader();
                }
            ).fail(
                function (response) {
                    errorProcessor.process(response);
                    fullScreenLoader.stopLoader();
                }
            );
        }
    }
});
