var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/model/step-navigator': {
                'MMS_Checkout/js/mixin/step-navigator-mixin': true
            },
            'Magento_Checkout/js/view/shipping': {
                'MMS_Checkout/js/mixin/shipping-mixin': true
            },
            'Ey_Checkout/js/view/billing-address': {
                'MMS_Checkout/js/mixin/billing-address-mixin': true
            },
            'Magento_Checkout/js/view/payment': {
                'MMS_Checkout/js/mixin/payment-mixin': true
            },
            'Magento_Checkout/js/model/payment-service': {
                'MMS_Checkout/js/mixin/payment-service-mixin': true
            },
            'Magento_Checkout/js/model/shipping-service': {
                'MMS_Checkout/js/mixin/shipping-service-mixin': true
            },
            'Magento_Checkout/js/model/shipping-rate-registry':{
                'MMS_Checkout/js/mixin/shipping-rate-registry-mixin': true
            },
            'Magento_Checkout/js/model/shipping-rates-validator':{
                'MMS_Checkout/js/mixin/shipping-rates-validator-mixin': true
            },
            'Magento_Checkout/js/model/shipping-rate-processor/new-address':{
                'MMS_Checkout/js/mixin/shipping-rate-processor/new-address-mixin': true
            }
        }
    },
    map: {
        '*': {
            'Magento_Checkout/template/billing-address/list.html': 'MMS_Checkout/template/billing-address-mixin/list.html',
            'Magento_Checkout/js/model/shipping-save-processor/default': 'MMS_Checkout/js/model/shipping-save-processor/default',
            'Magento_Checkout/js/model/customer-email-validator': 'MMS_Checkout/js/mixin/customer-email-validator',
            'Magento_Customer/js/zxcvbn': 'MMS_Checkout/js/zxcvbn'
        }
    }
};
