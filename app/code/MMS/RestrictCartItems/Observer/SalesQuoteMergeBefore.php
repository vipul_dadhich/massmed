<?php

namespace MMS\RestrictCartItems\Observer;

use \Magento\Framework\Message\ManagerInterface ;

class SalesQuoteMergeBefore implements \Magento\Framework\Event\ObserverInterface{

    public function execute(\Magento\Framework\Event\Observer $observer){
        if ($observer->getSource()->hasItems()) {
            if (is_object($observer->getQuote()) && $observer->getQuote()->getId()) {
                $observer->getQuote()->removeAllItems();
            }
        }
        return true;
    }
}