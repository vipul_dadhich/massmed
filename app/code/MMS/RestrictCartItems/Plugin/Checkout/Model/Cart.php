<?php

namespace MMS\RestrictCartItems\Plugin\Checkout\Model;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Message\ManagerInterface;

class Cart
{
    /**
     * @var Session
     */
    protected $checkoutSession;
    /**
     * @var ManagerInterface
     */
    protected $_messageManager;
    /**
     * @var JsonFactory
     */
    protected $jsonResultFactory;
    /**
     * @var Context
     */
    private $context;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var ResponseFactory
     */
    protected $responseFactory;

    /**
     * @var RedirectInterface 
     */
    protected $redirect;

    /**
     * Cart constructor.
     * @param Context $context
     * @param JsonFactory $jsonResultFactory
     * @param ManagerInterface $messageManager
     * @param Session $checkoutSession
     * @param ResponseFactory $responseFactory
     * @param RedirectInterface $redirect
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonResultFactory,
        ManagerInterface $messageManager,
        Session $checkoutSession,
        ResponseFactory $responseFactory,
        RedirectInterface $redirect
    ) {
        $this->_messageManager = $messageManager;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->checkoutSession = $checkoutSession;
        $this->responseFactory = $responseFactory;
        $this->redirect             = $redirect;
        $this->context = $context;
    }

    /**
     * @param \Magento\Checkout\Model\Cart $subject
     * @param \Closure $proceed
     * @param $productInfo
     * @param null $requestInfo
     * @return mixed
     */
    public function beforeAddProduct(
            \Magento\Checkout\Model\Cart $subject,
            $productInfo,
            $requestInfo = null
        ) {
            $quote = $this->checkoutSession->getQuote();
            if((isset($requestInfo['qty']) && $requestInfo['qty'] > 1) || $quote->hasItems()){
                $error = "You already have an item in your cart. Carts cannot contain multiple subscription items. Click on the cart icon to manage your selection or proceed to checkout.";
                $this->_messageManager->addError($error);
                $redirectUrl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
                if (!$redirectUrl) {
                    $redirectUrl = $this->redirect->getRefererUrl();
                }
                $this->responseFactory->create()->setRedirect($redirectUrl)->sendResponse();
                exit();
            }
            return array($productInfo, $requestInfo);
        }
    /**
     * @param \Magento\Checkout\Model\Cart $subject
     * @param $data
     * @return mixed
     */
    public function beforeupdateItems(\Magento\Checkout\Model\Cart $subject,$data)
    {
        $quote = $subject->getQuote();
        $allowedCartQty  = 1;
        foreach($data as $key => $value){
            $item = $quote->getItemById($key);
            $itemQty= $value['qty'];
            if($itemQty>$allowedCartQty){
                $this->_messageManager->addErrorMessage('You already have a subscription in your cart. Carts cannot contain multiple items. Click on the cart icon to manage your selection or proceed to checkout.');
                $data[$key]['qty']=1;
            }
        }
        return [$data];
    }
}
