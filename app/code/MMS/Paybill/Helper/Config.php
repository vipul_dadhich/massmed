<?php

namespace MMS\Paybill\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;


/**
 * Class Config
 * @package MMS\Paybill\Helper
 */
class Config extends AbstractHelper
{
    const DEFAULT_PRODUCT_FAMILY = 'paybill/api/default_product_family';

    /**
     * Config constructor.
     * @param Context $context
     * @param \Magento\Customer\Model\SessionFactory $customerSessionFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param GetCustomer $getCustomerApi
     * @param Data $jsonHelper
     */
    public function __construct(
        Context $context
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
    }

    /**
     * get default product family
     *
     * @param null $storeId
     * @return string
     */
    public function getDefaultProductFamily($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::DEFAULT_PRODUCT_FAMILY,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
