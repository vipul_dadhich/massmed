<?php
/**
 * @package     MMS_Paybill
 * @author      Dinesh Kaswan
 * @copyright   Copyright (c) 2019 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */

namespace MMS\Paybill\Helper;
use Ey\MuleSoft\Model\Catalyst\Api\GetCustomerData;
use Magento\Customer\Model\SessionFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use MMS\Logger\LoggerInterface;
use MMS\Paybill\Model\Catalyst\Api\CheckEligibility;

class CustomerData extends AbstractHelper{

    /**
     * Customer API
     *
     * @var \Ey\MuleSoft\Model\Catalyst\Api\GetCustomer
     */
    protected $getCustomerApi;

    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * JSON helper
     *
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * Response
     *
     * @var Http
     */
    protected $response;

    /**
     * CheckEligibility
     *
     * @var \MMS\Paybill\Model\Catalyst\Api\CheckEligibility
     */
    protected $checkeligibility;

    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * CustomerData constructor.
     * @param GetCustomerData $getCustomerApi
     * @param SessionFactory $customerSession
     * @param JsonHelper $jsonHelper
     * @param CheckEligibility $checkeligibility
     * @param Http $response
     * @param LoggerInterface $logger
     */
    public function __construct(
        GetCustomerData $getCustomerApi,
        SessionFactory $customerSession,
        JsonHelper $jsonHelper,
        CheckEligibility $checkeligibility,
        Http $response,
        LoggerInterface $logger
    ){
        $this->getCustomerApi = $getCustomerApi;
        $this->customerSession = $customerSession;
        $this->jsonHelper = $jsonHelper;
        $this->response = $response;
        $this->checkeligibility = $checkeligibility;
        $this->_logger = $logger;
    }

    /**
     * @return mixed|string
     */
    public function getCustomerAudienceType(){

        if($this->customerIsLogin()){
            $customer = $this->getCustomerData();
            $this->_logger->debug("GET Customer Audience Type Start");
            $customerResponse = $this->getCustomerApi->execute($customer->getData('ucid'));
            $paybillCustomerStatus = '';
            $this->_logger->debug("Response Code: ".$customerResponse->getStatusCode());
            if ($customerResponse->getStatusCode() == 200) {
                $response = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                $this->_logger->debug("Response: ". json_encode($response, JSON_PRETTY_PRINT));
                $this->_logger->debug("GET Customer Audience Type End");
                if (strtolower($this->checkeligibility->getProductFamily()) == 'nejm') {
                    if (isset($response['Customer']['nejm.subscriptionStatus'])) {
                        return $response['Customer']['nejm.subscriptionStatus'];
                    }
                }
                else {
                    if (isset($response['Customer']['catalyst.subscriptionStatus'])) {
                        return $response['Customer']['catalyst.subscriptionStatus'];
                    }
                }
            }

            return $paybillCustomerStatus;
        }
        $this->goToHomePage();
    }

    /**
     * @return \Magento\Customer\Model\Session
     */

    public function getCustomerData(){
        return $this->customerSession->create()->getCustomer();
    }

    /**
     * @return bool
     */

    public function customerIsLogin(){
        return $this->customerSession->create()->isLoggedIn();
    }

    /**
     * @return 301 redirect
     */

    public function goToHomePage(){
        $this->response->setRedirect('/');
    }
}
