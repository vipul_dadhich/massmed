<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_Paybill extension
 * NOTICE OF LICENSE
 */
namespace MMS\Paybill\Helper;
use Magento\Store\Model\ScopeInterface;
use Ey\MuleSoft\Helper\Data as MuleSoftHelper;
use MMS\Logger\LoggerInterface;


/**
 * Class Data
 * @package MMS\Paybill\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper{
    const ENABLE = 'paybill/general/enable';
    const PAYBILL_SUCCESS_ENABLE = 'paybill/api/is_paybill_success';
    const PAYBILL_LANGUAGE_BLOCK = 'paybill/api/paybill_language';
    const PAYBILL_LANGUAGE_BLOCK_PAYPAL = 'paybill/api/paybill_language_paypal';
    const DEFAULT_PROMO_CODE = 'paybill/api/default_promo_code';

    /**
     * @var string[]
     */
    protected $autoRenewalLanguageStatus = ['SUSPENDED', 'BILLED'];
   
    /**
     * @var LoggerInterface
     */
    protected $_logger;


    /**
     * Data constructor.
     * @param \Magento\Customer\Model\SessionFactory $customerSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Ey\MuleSoft\Api\Paybill\HttpClientInterface $sendPayment
     * @param MuleSoftHelper $mulesoftHelper
     * @param \Magento\Customer\Model\Session $session
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \LoggerInterface $logger
     */

    public function __construct(
        \Magento\Customer\Model\SessionFactory $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Ey\MuleSoft\Api\Paybill\HttpClientInterface $sendPayment,
        MuleSoftHelper $mulesoftHelper,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Ey\URLParams\Helper\Data $urlHelper,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        LoggerInterface $logger
    ){
        $this->customerSession = $customerSession;
        $this->scopeConfig = $scopeConfig;
        $this->jsonHelper = $jsonHelper;
        $this->_sendPayment = $sendPayment;
        $this->_mulesoftHelper = $mulesoftHelper;
        $this->isDebugMode = $this->_mulesoftHelper->getMuleSoftDebug();
        $this->isDebugModeType = $this->_mulesoftHelper->getMuleSoftDebugMode();
        $this->session = $session;
        $this->timezone = $timezone;
        $this->urlHelper = $urlHelper;
        $this->_orderFactory = $orderFactory;
        $this->quoteFactory = $quoteFactory;
        $this->_logger = $logger;
    }

    /**
     * Is module enabled
     *
     * @param null $storeId
     * @return string
     */
    public function isEnabled($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::ENABLE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Is Success page enable
     *
     * @param null $storeId
     * @return string
     */
    public function isPaybillSuccessEnabled($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::PAYBILL_SUCCESS_ENABLE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * get language block identifier
     *
     * @param null $storeId
     * @return string
     */
    public function getLanguageBlock($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::PAYBILL_LANGUAGE_BLOCK,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * get language block identifier for paypal
     *
     * @param null $storeId
     * @return string
     */
    public function getPaypalLanguageBlock($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::PAYBILL_LANGUAGE_BLOCK_PAYPAL,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function customerIsLogin(){
        $sessionCustomer = $this->customerSession->create();
        return $sessionCustomer->isLoggedIn();
    }
    public function goToHomePage(){
        $this->response->setRedirect('/');
    }
    /**
     * @return \Magento\Customer\Model\Session
     */

    public function getCustomerData(){
        return $this->customerSession->create()->getCustomer();
    }

    /**
     * @return mixed
     */
    public function getAPIURI()
    {
        return $this->_mulesoftHelper->getMuleSoftURI();
    }
    /**
     * @return mixed
     */
    public function getPaybillPaymentEndpoint()
    {
        return $this->scopeConfig->getValue('mulesoft/api/mulesoft_customer_endpoint', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getAkamaiAuthToken()
    {       
        if($this->session->getAkamaiAccessToken()){
            return $this->session->getAkamaiAccessToken();
        }
        return $this->session->getAccessToken();
    }
    
    /**
     * @return array()
     */

    public function getInvoiceDataFromQuote($order){
        if($order->getCustomInvoice()){
            return $this->jsonHelper->jsonDecode($order->getCustomInvoice());
        }
        return '';
    }

    /**
     * get logger status
     */

    public function getMuleSoftDebug(){
        return $this->isDebugMode;
    }
    /**
     * get logger status
     */

    public function getMuleSoftDebugMode(){
        return $this->isDebugModeType;
    }

    /**
     * @param $order
     * @return array
     * @throws \Exception
     */
    public function getPaymentPayload($order){
        $paymentInfo = $this->getPaymentInfo($order);
        $customerNumber = $this->getCustomerNumber($order);
        $promoCode = $this->getPromotionCode($order);
        $currencyCode = $this->getCurrencyCode($order);
        $productData = $this->getOrderItemData($order);
        $invoicePromoCode = $this->getAcsPromoCode($order);
        $acsOrderId = $this->getAcsOrderId($order);
        $extOrderNumber = $this->getAcsExtOrderNumber($order);
        $orderData = array();
        $orderData = array(
            'customerNumber' => $customerNumber,
            'orderId' => $acsOrderId,
            'extOrderNumber' => $extOrderNumber,
            'orderPromoCode' => $invoicePromoCode,
            'paymentPromoCode' => $promoCode,
            'paymentAmount' => (float)number_format($order->getGrandTotal(), 4),
            'currencyCode' => $currencyCode,
            'creditCardInfo' => $paymentInfo,
            'products' => $productData,

        );
        return $orderData;
    }

    /**
     * get order item data
     * @return array()
     */

    public function getOrderItemData($order){
        $invoiceData = $this->getInvoiceDataFromQuote($order);
        $products = array();
        if(isset($invoiceData['products'])){
            foreach($invoiceData['products'] as $item){
                $itemData['productCode'] = $item['productCode'];
                if (isset($item['term'])) {
                    $itemData['term'] = $item['term'];
                }
                $products[] = $itemData;
            }
        }
        return $products;
    }

    /**
     * get order id from invoice data
     * @return string
     */

    public function getAcsOrderId($order){
        $invoiceData = $this->getInvoiceDataFromQuote($order);
        if(isset($invoiceData['orderId'])){
            return $invoiceData['orderId'];
        }
        return '';
    }

    /**
     * get ext order number from invoice data
     * @return string
     */

    public function getAcsExtOrderNumber($order){
        $invoiceData = $this->getInvoiceDataFromQuote($order);
        if(isset($invoiceData['extOrderNumber'])){
            return $invoiceData['extOrderNumber'];
        }
        return '';
    }

    /**
     * get promo code from invoice data
     * @return string
     */

    public function getAcsPromoCode($order){
        $invoiceData = $this->getInvoiceDataFromQuote($order);
        if(isset($invoiceData['promotionCode'])){
            return $invoiceData['promotionCode'];
        }
        return '';
    }

    /**
     * get currency code
     * @return string
     */

    public function getCurrencyCode($order){
        $invoiceData = $this->getInvoiceDataFromQuote($order);
        if(isset($invoiceData['currency'])){
            return $invoiceData['currency'];
        }
        return 'USD';
    }

    /**
     * get Customer number
     * @return string
     */

    public function getCustomerNumber($order){
        $invoiceData = $this->getInvoiceDataFromQuote($order);
        if(isset($invoiceData['billTo']['customerNumber'])){
            return $invoiceData['billTo']['customerNumber'];
        }
        return '';
    }

    /**
     * get promotion code
     * @return string
     */

    public function getPromotionCode($order){
        $storeId = null;
        if($order->getData('ucc_promocode')){
            return $order->getData('ucc_promocode');
        }else{
            return $this->scopeConfig->getValue(
                self::DEFAULT_PROMO_CODE,
                ScopeInterface::SCOPE_STORE,
                $storeId
            );
        }
    }

    /**
     * get payment info
     * @return array()
     */
    public function getPaymentInfo($order){
        $paymentInfo = array();
        if($order->getPayment()->getMethodInstance()->getCode() == "md_firstdata") {
            $billingAddress = $order->getBillingAddress();
            $additonal_information = $order->getPayment()->getAdditionalInformation();
            foreach($additonal_information['md_firstdata_cards'] as $e => $val) {
                $finalData = $val;
            }
            $cardType = $finalData['cc_type'];
            $cardExpMonth =$finalData['cc_exp_month'];
            $cardExpDate =$finalData['cc_exp_year'];
            $lastFour=$finalData['cc_last_4'];
            $authorization = array(
                'authDate' => $this->timezone->date(new \DateTime($order->getPayment()->getCreatedAt()))->format('Y-m-d\TH:i:s\Z'),  //DateTime  // Created date time of transcation.^M
                'authCode' =>   (string)$order->getPayment()->getCcTransId(), //String  //cc_trans_id^M
                'authNumber' => (string)$order->getPayment()->getTransactionTag() //Stringtran
            );

            $paymentInfo = array(
                //( Only Mandatory for the md_firstdata )  From the order payment
                "cardType" => $cardType,  // ( Various type of card VI,MA, ( 3 card types )
                "cardToken" =>  (string)$order->getPayment()->getFirstdataToken(),  //( coming from the first data to charge card details )
                "customerName" => $billingAddress->getData('firstname')."  ".$billingAddress->getData('lastname'),
                "expirationMonth" => (int)$cardExpMonth,
                "expirationYear" => (int)$cardExpDate,
                "lastFour" => (string)$lastFour,
                "securityCode" => "123",  //Its going to be the new ticket for adding the security code.
                "authorization" => $authorization
            );
            return $paymentInfo;
        }
        return $paymentInfo;
    }

    public function sendPayment($order){
        try{
           
                $this->_logger->debug("***************** Paybill Payment API LOG Start ********************");
            
            $endpoint = $this->getPaymentEndpoint();
            $orderData = $this->getPaymentPayload($order);
            $headers['Content-Type'] = 'application/json';
            $headers['akamai-auth-token'] = $this->getAkamaiAuthToken();
            $response = $this->_sendPayment->request($orderData, $endpoint, $this->getAPIURI(), $headers);
            
                $this->_logger->debug("Response Code-> ". $response->getStatusCode());
                if($this->isDebugModeType){
                    $this->_logger->debug("Order Request Data Object-> ");
                    $this->_logger->debug(json_encode($orderData, JSON_PRETTY_PRINT));
                    $this->_logger->debug("MuleSoft Response->body-> ");
                    $this->_logger->debug($response->getBody()->getContents());
                }
            
            $order->setMulesoftResponse($response->getStatusCode())->save();
        }
        catch (\Magento\Framework\Exception\LocalizedException $e)
        {
            
                $this->_logger->debug("Mulesoft Response Exception" . $e->getMessage());
            
        } catch (\Exception $e) {
            
                $this->_logger->debug($e->getMessage());
            
        }
        
            $this->_logger->debug("***************** Payment API LOG End ********************");
        
    }

    /**
     * @return string
     */
    public function getPaymentEndpoint(){
        $paybillPaymentEndpoint = $this->getPaybillPaymentEndpoint();
        return $paybillPaymentEndpoint.'/'.$this->getCustomerData()->getData('ucid').'/payments';
    }

    /**
     * @param $id
     * @return string
     */
    public function getPaymentTitle($id)
    {
        $titleSuffix = '';
        $order = $this->getOrder($id);
        $quote = $this->getQuoteFromOrder($order->getQuoteId());
        if (!in_array($quote->getData('acs_order_status'), $this->autoRenewalLanguageStatus)) {
            $titleSuffix = ' with Automatic Renewal';
        }
        if ($order->getPayment()) {
            if ($order->getPayment()->getMethodInstance()->getCode() == 'md_firstdata') {
                return 'Credit Card'. $titleSuffix;
            } else if ($order->getPayment()->getMethodInstance()->getCode() == 'paypal_express') {
                return 'PayPal'. $titleSuffix;
            }
            return  $order->getPayment()->getMethodInstance()->getTitle();
        } else {
            return "";
        }
    }

    /**
     * @param $quoteId
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuoteFromOrder($quoteId){
        return $this->quoteFactory->create()->load($quoteId);
    }

    public function getOrder($id)
    {
        $order = $this->_orderFactory->create()->loadByIncrementId($id);
        return $order;
    }

    /**
     * Set Paybill Response Config To Customer Session
     * @param $response
     */
    public function setPaybillResponseData($response)
    {
        $this->session->setPaybillResponseData($response);
    }

    /**
     * Return Paybill Response Data From Customer Session
     */
    public function getPaybillResponseData()
    {
        $this->session->getPaybillResponseData() ?? false;
    }
}
