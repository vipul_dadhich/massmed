<?php

namespace MMS\Paybill\Api;

/**
 * Create Quote by paybill api service
 */
interface CreateQuoteServiceInterface
{
    /**
     * @return mixed
     */
    public function execute();

    /**
     * @param $request
     * @return mixed
     */
    public function setRequestData($request);

    /**
     * @return mixed
     */
    public function getRequestData();
}