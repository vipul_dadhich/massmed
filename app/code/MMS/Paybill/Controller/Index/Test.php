<?php


namespace MMS\Paybill\Controller\Index;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\PageFactory;
use MMS\Paybill\Api\CreateQuoteServiceInterface;
use MMS\Paybill\Helper\Config;
use MMS\Paybill\Model\Catalyst\Api\GetEligibility;

/**
 * Class Test
 * @package MMS\Paybill\Controller\Index
 */
class Test extends Action implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;
    /**
     * @var Config
     */
    protected $dataHelper;
    /**
     * @var GetEligibility
     */
    protected $_eligibilityApi;
    /**
     * @var CreateQuoteServiceInterface
     */
    protected $_createQuoteService;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        GetEligibility $eligibilityApi,
        Config $dataHelper,
        CreateQuoteServiceInterface $createQuoteService
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->_eligibilityApi = $eligibilityApi;
        $this->dataHelper = $dataHelper;
        $this->_createQuoteService = $createQuoteService;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        if ($this->getRequest()->getParam('debug', false) == true) {
            $billInfo = $this->sampleData();
            if (isset($billInfo['invoices']) && !empty($billInfo['invoices'])) {
                $postalCode  = $billInfo['invoices'][0]['billTo']['postalCode'] ?? '';
                $country = $billInfo['invoices'][0]['billTo']['country'];
                if(($country === 'US' || $country === 'USA') && strlen($postalCode) === 9 ){
                    $postalCodeArray = str_split($postalCode,5);
                    $billInfo['invoices'][0]['billTo']['postalCode'] = $postalCodeArray[0] . "-" . $postalCodeArray[1];
                }

                try {
                    $this->_createQuoteService->setRequestData($billInfo);
                    $this->_createQuoteService->execute();
                    return $this->_redirect('paybill/checkout');
                } catch (LocalizedException $e) {
                    $this->messageManager->addNoticeMessage(
                        $this->_objectManager->get(Escaper::class)->escapeHtml($e->getMessage())
                    );
                } catch (Exception $e) {
                    $this->messageManager->addExceptionMessage(
                        $e,
                        __('We can\'t add this item to your shopping cart right now.')
                    );
                }
                return $this->_redirect('/');
            }
            return $this->_redirect('no-payment-due');
        } else {
            return $this->_redirect('no-route');
        }
    }

    public function sampleData()
    {
        return json_decode('{
    "ucid": "42a00055-cd8b-4385-83db-d0b4faec7efc",
    "customerNumber": "000102255500",
    "invoices": [
        {
            "extOrderNumber": "??????????",
            "orderId": "22847342",
            "orderDate": "2021-03-22",
            "promotionCode": "????????",
            "currency": "EUR",
            "products": [
                {
                    "lineNumber": 1,
                    "productCode": "NEJ-NEJ",
                    "priceCode": "WREGEP",
                    "unitPrice": 229,
                    "term": "52 issues",
                    "quantity": 1
                }
            ],
            "amounts": {
                "sales": 229,
                "discount": 0,
                "tax": 0,
                "total": 229,
                "paymentReceived": 0,
                "balanceDue": 229
            },
            "billTo": {
                "customerNumber": "000102255500",
                "addressCode": "OFFICE",
                "firstName": "BARTOLOMEO",
                "lastName": "CANDELA",
                "address": "SS 113 KM 246 BAGHERIA PA",
                "city": "BAGHERIA",
                "country": "ITA",
                "postalCode": ""
            },
            "orderStatus": "BILLED"
        }
    ]
}', true);
    }
}
