<?php
/**
 * See COPYING.txt for license details.
 *
 * Paybill extension
 * NOTICE OF LICENSE
 */
namespace MMS\Paybill\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use MMS\Logger\LoggerInterface;

/**
 * Tax totals calculation model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;
    /**
     * @var \MMS\Paybill\Helper\Data
     */
    protected $dataHelper;
    /**
     * @var \MMS\Paybill\Model\Catalyst\Api\GetEligibility
     */
    protected $_eligibilityApi;
    /**
     * @var \MMS\Paybill\Api\CreateQuoteServiceInterface
     */
    protected $_createQuoteService;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Magento\Framework\App\RequestInterface $request,
        \MMS\Paybill\Model\Catalyst\Api\GetEligibility $eligibilityApi,
        \MMS\Paybill\Helper\Data $dataHelper,
        \MMS\Paybill\Api\CreateQuoteServiceInterface $createQuoteService,
        \Ey\PromoCode\Helper\Data $promoHelper,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->_eligibilityApi = $eligibilityApi;
        $this->dataHelper = $dataHelper;
        $this->request = $request;
        $this->_createQuoteService = $createQuoteService;
        $this->isDebugMode = $this->dataHelper->getMuleSoftDebug();
        $this->isDebugModeType = $this->dataHelper->getMuleSoftDebugMode();
        $this->promoHelper = $promoHelper;
        $this->_logger = $logger;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        if ($this->dataHelper->customerIsLogin()) {

            $customer = $this->dataHelper->getCustomerData();
            if ($customer->getData('ucid')) {
                $promoCodeParam = $this->promoHelper->getPromoCodeUrlParam();
                if($promoCodeParam){
                    $promoCode = $this->request->getParam($promoCodeParam);
                    if($promoCode){
                        $this->promoHelper->setCustomCookie('PromoCode', $promoCode);
                    }
                }

                    $this->_logger->debug("****************** GetBill API Log Start *******************");

                $billInfo = $this->_eligibilityApi->execute($customer->getData('ucid'));

                    if($this->isDebugModeType){
                        $this->_logger->debug("MuleSoft Response BODY-> ");
                        $this->_logger->debug(json_encode($billInfo, JSON_PRETTY_PRINT));
                    }
                    $this->_logger->debug("****************** GetBill API Log End *******************");

                if($billInfo){
                    $this->dataHelper->setPaybillResponseData($billInfo);
                    if(isset($billInfo['invoices']) && !empty($billInfo['invoices'])){

                        // putting - between USA 9 Length postalCode
                        $postalCode = '';
                        if (isset($billInfo['invoices'][0]['billTo']['postalCode'])) {
                            $postalCode = $billInfo['invoices'][0]['billTo']['postalCode'];
                        }
                        $country = $billInfo['invoices'][0]['billTo']['country'];

                        if(($country === 'US' || $country === 'USA') && strlen($postalCode) === 9 ){
                            $postalCodeArray = str_split($postalCode,5);
                            $billInfo['invoices'][0]['billTo']['postalCode'] = $postalCodeArray[0] . "-" . $postalCodeArray[1];

                        }

                        try {
                            $this->_createQuoteService->setRequestData($billInfo);
                            $this->_createQuoteService->execute();
                            //$this->messageManager->addSuccessMessage(__('You added items for paybill to your shopping cart.'));
                            return $this->_redirect('paybill/checkout');
                        } catch (\Magento\Framework\Exception\LocalizedException $e) {
                            $this->messageManager->addNoticeMessage(
                                $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($e->getMessage())
                            );
                        } catch (\Exception $e) {
                            $this->messageManager->addExceptionMessage(
                                $e,
                                __('We can\'t add this item to your shopping cart right now.')
                            );
                        }
                        return $this->_redirect('/');
                    }
                } else {
                    return $this->_redirect('apologies');
                }
            }
            return $this->_redirect('no-payment-due');
        }
        $page = $this->pageFactory->create();
        $page->getConfig()->getTitle()->set(__('Pay Your Bill'));
        return $page;
    }

}
