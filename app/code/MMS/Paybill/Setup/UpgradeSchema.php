<?php
/**
 * @package     MMS_Paybill
 * @author      Dinesh Kaswan
 * @copyright   Copyright (c) 2019 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Paybill\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Cms\Model\BlockFactory;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * UpgradeSchema constructor.
     * @param BlockFactory $blockFactory
     */
    public function __construct(BlockFactory $blockFactory) {
        $this->blockFactory = $blockFactory;
    }
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.1.0') < 0) {
            $quoteTable = 'quote';
            $orderTable = 'sales_order';
            //Quote table
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'is_paybill',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        'length' => '1',
                        'default' => 0,
                        'nullable' => true,
                        'comment' => 'Is Paybill'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'acs_order_id',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => '255',
                        'default' => NULL,
                        'nullable' => true,
                        'comment' => 'ACS Order ID'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'custom_invoice',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => '',
                        'default' => NULL,
                        'nullable' => true,
                        'comment' => 'Invoice data for paybill'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'acs_order_status',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => '',
                        'default' => NULL,
                        'nullable' => true,
                        'comment' => 'ACS Order Status'
                    ]
                );
            //Order table
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'is_paybill',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        'length' => '1',
                        'default' => 0,
                        'nullable' => true,
                        'comment' => 'Is Paybill'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'acs_order_id',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => '255',
                        'default' => NULL,
                        'nullable' => true,
                        'comment' => 'ACS Order ID'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'custom_invoice',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => '',
                        'default' => NULL,
                        'nullable' => true,
                        'comment' => 'Invoice data for paybill'
                    ]
                );
        }
        if (version_compare($context->getVersion(), '1.1.1') < 0) {
            $customCss = 'font-size: 1.6rem';
            $customHtmlblockNoPaymentDue = '<div style = '.$customCss.'>
            <p>
            Our records show that you do not have a current subscription requiring payment.</p> </br>

            <p> If you placed an order recently, it is possible that our database has not yet been updated with your account information. </p> </br>

            <p> You can review your current subscription information at any time on the <a target="_blank" href="https://myaccount.nejm.org">My Account page</a>.</p> </br> </br>

            <p> If you would like to subscribe to the New England Journal of Medicine, please click <a target="_blank" href="https://myaccount.nejm.org">here</a>.</p> </br> </br>

            <p> You can review your account information at any time on the <a target="_blank" href="https://www.nejm.org/toc/nejm/medical-journal">My Account</a> page. </p>

            </div>';
            $customHtmlblockSubscriber = '<div style = '.$customCss.'>
            <p>Our records show your current subscription is paid in full.</p> </br>

            <p> If you placed an order recently, it is possible that our database has not yet been updated with your account information. </p> </br>

            <p> You can review your current subscription information at any time on the <a target="_blank" href="https://myaccount.nejm.org">My Account page</a>.</p> page. </br> </br>

            <p> <a target="_blank" href="https://www.nejm.org/toc/nejm/medical-journal">Go to Latest Issue</a> </p>

            </div>';
            $staticBlockInfo = [
                'title' => 'NEJM No Payment Due Others',
                'identifier' => 'nejm-no-payment-due-others',
                'stores' => ['0'],
                'is_active' => 1,
                'content' => $customHtmlblockNoPaymentDue
            ];
            $this->blockFactory->create()->setData($staticBlockInfo)->save();
            $staticBlockInfo1 = [
                'title' => 'NEJM No Payment Due SUBSCRIBER',
                'identifier' => 'nejm-no-payment-due-subscriber',
                'stores' => ['0'],
                'is_active' => 1,
                'content' => $customHtmlblockSubscriber
            ];
            $this->blockFactory->create()->setData($staticBlockInfo1)->save();
        }
        $setup->endSetup();
    }
}
