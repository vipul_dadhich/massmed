<?php

namespace MMS\Paybill\Model\Catalyst\Api;

use MMS\Paybill\Model\Api\Mulesoft\Core\SetRequest;
use Ey\URLParams\Helper\Data;

/**
 * Class setRequest
 */
class GetEligibility
{
    /**
     * API request endpoint
     */
    const API_REQUEST_ENDPOINT = 'v1/api/customers';
    /**
     * API request methodyt
     */
    const API_REQUEST_METHOD = 'GET';

    /**
     * JSON helper
     *
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * getCustomer constructor
     *
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     * @param \Ey\MuleSoft\Helper\Endpoints $endpoints
     */
    public function __construct(
        SetRequest $request,
        Data $urlHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\App\RequestInterface $requestParam,
        \MMS\Paybill\Helper\Config $config,
        \Ey\MuleSoft\Helper\Endpoints $endpoints
    ) {
        $this->request = $request;
        $this->urlHelper = $urlHelper;
        $this->jsonHelper = $jsonHelper;
        $this->requestParam = $requestParam;
        $this->config = $config;
        $this->endpoints = $endpoints;
    }

    /**
     * Fetch some data from API
     * @param $params array() or string
     */
    public function execute($ucid)
    {
       $response = $this->request->makeRequest( $this->getEndpointUri($ucid), static::API_REQUEST_METHOD, array() );
        if($response->getStatusCode() == 200){
            return $this->jsonHelper->jsonDecode($response->getBody()->getContents());
        }
        return false;
    }


     /**
      * get endpoint uri
      */
     private function getEndpointUri($ucid){
         return $this->getEligibilityEndpoint().'/'.$ucid.'/invoices?productFamily='.$this->getProductFamily();
     }

    /**
     * get eligibility endpoint
     */
    private function getEligibilityEndpoint(){
        if($this->endpoints->getCustomerEndpoint()) {
            return $this->endpoints->getCustomerEndpoint();
        }
        return self::API_REQUEST_ENDPOINT;
    }
    /**
      * get product family
      */
     private function getProductFamily(){
     	if($this->requestParam->getParam('product')){
     		return $this->requestParam->getParam('product');
     	}
     	else if($this->urlHelper->getCustomCookie('product')){
        	return $this->urlHelper->getCustomCookie('product');
        }
        else if($this->config->getDefaultProductFamily()){
            return $this->config->getDefaultProductFamily();
        }
        else{
        	return 'catalyst';
        }
     }

    private function ApiErrorMessages(){
        $error = "Customer doesn't exists";
        //Enhance the Error API error messages.
        //TODO
        //Use the Response setting classes for it.
    }
}
