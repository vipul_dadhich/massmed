<?php

namespace MMS\Paybill\Model\Api\Mulesoft\Core;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Psr7\ResponseFactory;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Framework\Webapi\Rest\Request;
use Ey\MuleSoft\Helper\Data;
use MMS\Logger\LoggerInterface;


class SetRequest
{
    /**
     * API request URL
     */
    const API_REQUEST_URI = '';

    /**
     * SetRequest constructor
     *
     * @param ClientFactory $clientFactory
     */

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    public function __construct(
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory,
        Data $helper,
        \MMS\Paybill\Helper\Data $paybillHelper,
        LoggerInterface $logger
    )
    {
        $this->clientFactory = $clientFactory;
        $this->responseFactory = $responseFactory;
        $this->helper = $helper;
        $this->paybillHelper = $paybillHelper;
        $this->isDebugMode = $this->helper->getMuleSoftDebug();
        $this->_logger = $logger;
    }

    /**
     * Api end point
     **/
    private function getAPIURI()
    {
        return $this->paybillHelper->getAPIURI();
    }

    /**
     * Api client ID
     **/
    private function getMulesoftClientId()
    {
        return $this->helper->getMuleSoftClientId();
    }

    /**
     * Api secret
     **/
    private function getMulesoftSecret()
    {
        return $this->helper->getMuleSoftClientSecret();
    }

    /**
     * Akamai Auth Token
     **/
    private function getAkamaiAuthToken()
    {
        return $this->paybillHelper->getAkamaiAuthToken();
    }

    /**
     * Do API request with provided params
     *
     * @param string $apiMethod
     * @param string $requestMethod
     * @param array $params
     *
     * @return Response
     */
    public function makeRequest($apiMethod, $requestMethod, $params = [])
    {
        return $this->doRequest($apiMethod, $params, $requestMethod);
    }

    /**
     * Do API request with provided params
     *
     * @param string $uriEndpoint
     * @param array $params
     * @param string $requestMethod
     *
     * @return Response
     */
    private function doRequest(
        string $uriEndpoint,
        array $params = [],
        string $requestMethod = Request::HTTP_METHOD_GET
    )
    {
        /** @var Client $client */
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => $this->getAPIURI(),
            'headers' => [
                'client_id' => $this->getMulesoftClientId(),
                'client_secret' => $this->getMulesoftSecret(),
                'akamai-auth-token' => $this->getAkamaiAuthToken()
            ]
        ]]);
        
            $this->_logger->debug("****************** GetBill API Log Start *******************");
            $this->_logger->debug("Base URL-> " . $this->getAPIURI());
            $this->_logger->debug("client_id-> " . $this->getMulesoftClientId());
            $this->_logger->debug("client_secret-> " . $this->getMulesoftSecret());
            $this->_logger->debug("akamai-auth-token-> " . $this->getAkamaiAuthToken());
            $this->_logger->debug("Endpoint-> " . $uriEndpoint);
        
        try {
            $response = $client->request(
                $requestMethod,
                $uriEndpoint
            );
        } catch (GuzzleException $exception) {
            /** @var Response $response */
            $response = $this->responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);
        }
        
            if ($response->getBody()) {
                $this->_logger->debug("GetBill API Mulesoft Response log-> " . json_encode($response->getBody(), JSON_PRETTY_PRINT));
            } else {
                $this->_logger->debug("GetBill API Mulesoft Response log-> " . print_r($response, true));
            }
        

        return $response;
    }
}

