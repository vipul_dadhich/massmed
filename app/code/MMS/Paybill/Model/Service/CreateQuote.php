<?php

namespace MMS\Paybill\Model\Service;

use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

class CreateQuote implements \MMS\Paybill\Api\CreateQuoteServiceInterface
{
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cartModel;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_productModel;
    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $_formKey;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $_cartRepository;
    /**
     * @var \Magento\Quote\Api\CartManagementInterface
     */
    protected $_cartManagement;
    /**
     * @var \Magento\Directory\Model\CurrencyFactory
     */
    protected $_currencyFactory;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepository;
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $_quoteFactory;
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;
    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    public $_request;

    /**
     * @var \Magento\Quote\Model\Quote
     */
    public $_currentQuote;
    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_product;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    protected $currentQuoteId = null;

    public function __construct(
        \Magento\Checkout\Model\Cart $cartModel,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepository,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        LoggerInterface $logger,
        \Magento\Catalog\Model\ProductFactory $product,
        ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\DataObject\Factory $objectFactory
    ) {
        $this->_cartModel = $cartModel;
        $this->_productModel = $productModel;
        $this->_formKey = $formKey;
        $this->_checkoutSession = $checkoutSession;
        $this->_storeManager = $storeManager;
        $this->_cartRepository = $cartRepository;
        $this->_cartManagement = $cartManagement;
        $this->_currencyFactory = $currencyFactory;
        $this->_customerSession = $customerSession;
        $this->_customerRepository = $customerRepository;
        $this->_quoteFactory = $quoteFactory;
        $this->_jsonHelper = $jsonHelper;
        $this->_countryFactory = $countryFactory;
        $this->_logger = $logger;
        $this->_objectManager = $objectManager;
        $this->_productRepository = $productRepository;
        $this->_productFactory = $productFactory;
        $this->_product = $product;
        $this->objectFactory = $objectFactory;
    }

    public function execute()
    {
        $errorMessages = false;
        try {
            $this->_currentQuote = null;

            /*$this->emptyCurrentCart();*/
            $this->initQuote();
            $this->setStoreToQuote();
            $this->setCurrencyToQuote();
            $this->setQuoteIsVirtual();
            $this->setCustomerNoteNotify();
            $this->addProductsToQuote();

            $this->saveCurrentQuote();
            //$this->setShippingAddress();
            $this->assignCustomerToQuote();
            //$this->setBillingAddress();

           // $this->collectShippingRates();
            $this->cartSave();
            $this->collectTotalsAndSave();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $errorMessages = $e->getMessage();
        } catch (\Exception $ex) {
            $errorMessages = $ex->getMessage();
        }

        if ($errorMessages) {
            $this->_currentQuote = null;
            throw new \Magento\Framework\Exception\LocalizedException(__($errorMessages));
        }
    }

    /**
     * @param $request
     */
    public function setRequestData($request)
    {
        $this->_request = $request;
    }

    /**
     * @return mixed
     */
    public function getRequestData()
    {
        return $this->_request;
    }

    /**
     * @throws \Exception
     */
    protected function saveCurrentQuote()
    {
        $this->_currentQuote->save();
    }

    /**
     * empty existing quote for cart items
     */
    protected function emptyCurrentCart()
    {
        if ($this->_checkoutSession->getQuoteId()) {
            $this->_checkoutSession->resetCheckout();
            $this->_checkoutSession->clearQuote();
        }
    }

    /**
     * Initialize the quote
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Exception
     */
    protected function initQuote()
    {
        $this->clearAllQuoteToInActive();

        $cart_id = $this->_cartManagement->createEmptyCartForCustomer($this->_customerSession->getCustomerId());
        $this->_currentQuote = $this->_cartRepository->get($cart_id);
        $this->_cartModel->setQuote($this->_currentQuote);

        $this->_currentQuote->setIsActive(1)->setReservedOrderId(null);
        $this->_currentQuote->setData('is_paybill', 1);
        $this->_currentQuote->save();
        $this->currentQuoteId = $this->_currentQuote->getId();

        $invoiceData = $this->getRequestData();
        $invoice = $invoiceData['invoices'][0];
        $this->_currentQuote->setAcsOrderId($invoice['orderId']);
        $this->_currentQuote->setAcsOrderStatus($invoice['orderStatus']);
        $this->_currentQuote->setCustomInvoice($this->_jsonHelper->jsonEncode($invoice));

    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function setStoreToQuote()
    {
        $store = $this->_storeManager->getStore();
        $this->_currentQuote->setStore($store);
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function assignCustomerToQuote()
    {
        $customerId = $this->_customerSession->getCustomer()->getId();
        $customer = $this->_customerRepository->getById($customerId);
        $this->_currentQuote->assignCustomer($customer);
    }

    /**
     * Set current to current quote
     */
    protected function setCurrencyToQuote()
    {
        $invoiceData = $this->getRequestData();
        $currency = (isset($invoiceData['invoices'][0]['currency'])) ? $invoiceData['invoices'][0]['currency'] : 'USD';
        $this->updateCurrencyToSession($currency);
    }

    /**
     * Updates current to current quote
     */
    public function updateCurrencyToSession($currencyCode)
    {
        /** @var \Magento\Store\Model\StoreManagerInterface $storeManager */
        $storeManager = $this->_objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
        $storeManager->getStore()->setCurrentCurrencyCode($currencyCode);
        $this->_currentQuote->setQuoteCurrencyCode($currencyCode);
        $this->_currentQuote->setBaseCurrencyCode($currencyCode);
        $this->_currentQuote->setStoreCurrencyCode($currencyCode);
        $quoteCurrency = $this->_currencyFactory->create()->load($currencyCode);
        $this->_currentQuote->setForcedCurrency($quoteCurrency);
    }

    /**
     * set current Quote isVirtual
     */
    protected function setQuoteIsVirtual()
    {
        $this->_currentQuote->setIsVirtual(true);
    }

    /**
     * Set customer note notify to current quote
     */
    protected function setCustomerNoteNotify()
    {
        $this->_currentQuote->setCustomerNoteNotify(false);
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function addProductsToQuote()
    {
        $errorMessages = false;
        try {
            $invoiceData = $this->getRequestData();
            $request = $invoiceData['invoices'][0];
            if (isset($request['products']) && count($request['products'])) {
                foreach ($request['products'] as $item) {
                    $product = $this->_product->create()->loadByAttribute('sku', $item['productCode']);
                    if ($product && $product->getId()) {
                        $product = $this->_productFactory->create()->load($product->getId());
                        $product->setPrice($item['unitPrice']);
                        $product->setFinalPrice($item['unitPrice']);
                        $params['qty'] = (int)$item['quantity'];
                        $options = [];
                        if ( $product->getData('has_options') ) {
                            foreach ($product->getOptions() as $o) {
                                foreach ($o->getValues() as $value) {
                                    if (isset($item['term']) && $value['title'] == $item['term']) {
                                        $options[$value['option_id']] = $value['option_type_id'];
                                    }
                                }
                            }
                            if (!empty($options)) {
                                $params['options'] = $options;
                            }
                        }
                        $requestObj = $this->objectFactory->create($params);
                        $quoteItem = $this->_currentQuote->addProduct($product, $requestObj);
                        if ($quoteItem) {
                            $quoteItem->setCustomPrice($item['unitPrice']);
                            $quoteItem->setOriginalCustomPrice($item['unitPrice']);
                            $quoteItem->getProduct()->setIsSuperMode(true);
                        }
                    }
                }
            }

            $this->_cartModel->save();
            /*$this->_cartModel->addProductsByIds($productIds);*/

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $errorMessages = $e->getMessage();
        } catch (\Exception $exception) {
            $errorMessages = $exception->getMessage();
        }

        if ($errorMessages) {
            throw new \Magento\Framework\Exception\LocalizedException(__($errorMessages));
        }
    }

    /**
     * Set Shipping Address to current quote, if quote is not virtual
     */
    protected function setShippingAddress()
    {
        if (!$this->_currentQuote->isVirtual()) {
            /** here shipping will be set for current quote */
            $invoiceData = $this->getRequestData();
            $invoice = $invoiceData['invoices'][0];
            $country = $this->_countryFactory->create()->loadByCode($invoice['billTo']['country']);
            $shipAddress['firstname'] = $invoice['billTo']['firstName'];
            $shipAddress['lastname'] = $invoice['billTo']['lastName'];
            $shipAddress['street'] = $invoice['billTo']['address'];
            $shipAddress['street2'] = isset($invoice['billTo']['address2'])?$invoice['billTo']['address2']:'';
            $shipAddress['city'] = $invoice['billTo']['city'];
            $shipAddress['region'] = $invoice['billTo']['state'];
            $shipAddress['postcode'] = $invoice['billTo']['postalCode'] ?? '';
            $shipAddress['save_in_address_book'] = '1';
            $shipAddress['country_id'] = $country->getCountryId();

            $this->_currentQuote->getShippingAddress()->addData($shipAddress);
        }
    }

    /**
     * Set Billing Address to current quote
     */
    protected function setBillingAddress()
    {
        $invoiceData = $this->getRequestData();
        $invoice = $invoiceData['invoices'][0];
        $country = $this->_countryFactory->create()->loadByCode($invoice['billTo']['country']);
        $billAddress['firstname'] = $invoice['billTo']['firstName'];
        $billAddress['lastname'] = $invoice['billTo']['lastName'];
        $billAddress['street'] = $invoice['billTo']['address'];
        $billAddress['street2'] = isset($invoice['billTo']['address2'])?$invoice['billTo']['address2']:'';
        $billAddress['city'] = $invoice['billTo']['city'];
        $billAddress['region'] = $invoice['billTo']['state'];
        $billAddress['postcode'] = $invoice['billTo']['postalCode'] ?? '';
        $billAddress['save_in_address_book'] = '1';
        $billAddress['country_id'] = $country->getCountryId();

        $this->_currentQuote->getBillingAddress()->addData($billAddress);
    }

    /**
     * Load shipping rates based on shipping address
     * and assign to current quote
     */
    protected function collectShippingRates()
    {
        if (!$this->_currentQuote->isVirtual()) {
            $shippingAddress = $this->_currentQuote->getShippingAddress();
            $shippingAddress->setCollectShippingRates(true)
                ->collectShippingRates();
        }
    }

    /**
     * Save current Quote to Cart Repository
     */
    protected function cartSave()
    {
        $this->_cartRepository->save($this->_currentQuote);
    }

    /**
     * @throws \Exception
     */
    protected function collectTotalsAndSave()
    {
        $this->saveCurrentQuote();
        $this->_currentQuote->collectTotals();
        $this->saveCurrentQuote();
    }

    /**
     * Get checkout model
     *
     * @return \Magento\Checkout\Model\Session
     */
    protected function _getCheckout()
    {
        return $this->_objectManager->get(\Magento\Checkout\Model\Session::class);
    }

    /**
     * Clear all active quote from customer quote collection
     */
    protected function clearAllQuoteToInActive()
    {
        $collection = $this->getQuoteCollection();
        if ($collection->getSize() > 0) {
            foreach ($collection as $quote) {
                /** @var $quote \Magento\Quote\Model\Quote */
                try {
                    $quote->setIsActive(0)->save();
                } catch (\Exception $ex) {
                    /** no execution need */
                }
            }
        }
    }

    /**
     * @return \Magento\Framework\Data\Collection\AbstractDb|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection|null
     */
    public function getQuoteCollection()
    {
        $quoteCollection = $this->_quoteFactory->create()->getCollection();
        $quoteCollection->addFieldToFilter('customer_id', $this->_customerSession->getCustomerId());
        $quoteCollection->addFieldToFilter('is_active', 1);
        if ($this->currentQuoteId != null) {
            $quoteCollection->addFieldToFilter(
                'entity_id',
                ['nin' => [$this->currentQuoteId]]
            );
        }
        return $quoteCollection;
    }
}
