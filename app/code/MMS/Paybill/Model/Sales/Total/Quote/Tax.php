<?php

namespace MMS\Paybill\Model\Sales\Total\Quote;

use \Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Api\Data\AddressInterfaceFactory as CustomerAddressFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory as CustomerAddressRegionFactory;
use Magento\Framework\Json\Helper\Data;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address;

/**
 * Class Tax
 * @package MMS\Paybill\Model\Sales\Total\Quote
 */
class Tax extends \MMS\Tax\Model\Tax
{
    /** @var CheckoutSession */
    protected $checkoutSession;
    /**
     * @var Data
     */
    protected $jsonHelper;

    /**
     * Search constructor.
     * @param \Magento\Tax\Model\Config $taxConfig
     * @param \Magento\Tax\Api\TaxCalculationInterface $taxCalculationService
     * @param \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsDataObjectFactory
     * @param \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemDataObjectFactory
     * @param \Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory $taxClassKeyDataObjectFactory
     * @param CustomerAddressFactory $customerAddressFactory
     * @param CustomerAddressRegionFactory $customerAddressRegionFactory
     * @param \Magento\Tax\Helper\Data $taxData
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Tax\Api\Data\QuoteDetailsItemExtensionFactory $extensionFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \MMS\Logger\LoggerInterface $mmsLogger
     * @param Data $jsonHelper
     * @param CheckoutSession $checkoutSession
     * @param \Magento\Directory\Helper\Data $directoryHelper
     */
    public function __construct(
        \Magento\Tax\Model\Config $taxConfig,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculationService,
        \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsDataObjectFactory,
        \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemDataObjectFactory,
        \Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory $taxClassKeyDataObjectFactory,
        CustomerAddressFactory $customerAddressFactory,
        CustomerAddressRegionFactory $customerAddressRegionFactory,
        \Magento\Tax\Helper\Data $taxData,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Tax\Api\Data\QuoteDetailsItemExtensionFactory $extensionFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \MMS\Logger\LoggerInterface $mmsLogger,
        Data $jsonHelper,
        CheckoutSession $checkoutSession,
        \Magento\Directory\Helper\Data $directoryHelper
    ) {
        parent::__construct(
            $taxConfig,
            $taxCalculationService,
            $quoteDetailsDataObjectFactory,
            $quoteDetailsItemDataObjectFactory,
            $taxClassKeyDataObjectFactory,
            $customerAddressFactory,
            $customerAddressRegionFactory,
            $taxData,
            $priceCurrency,
            $extensionFactory,
            $logger,
            $scopeConfig,
            $mmsLogger,
            $directoryHelper
        );

        $this->jsonHelper = $jsonHelper;
        $this->checkoutSession = $checkoutSession;
    }

    /**
    * Custom Collect tax totals for quote address
    *
    * @param Quote $quote
    * @param ShippingAssignmentInterface $shippingAssignment
    * @param Address\Total $total
    * @return $this
    */
    public function collect(
        Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        if (!count($shippingAssignment->getItems())) {
            return $this;
        }

        if ($quote->getIsPaybill()) {
            $this->updateCustomSubtotal($total, $quote);
            $this->updateCustomGrandTotal($total, $quote);
            $this->updateCustomTaxAmount($total, $quote);
            $this->updateCustomDiscountAmount($total, $quote);
        } else {
            parent::collect($quote, $shippingAssignment, $total);
        }

        return $this;
    }

    /**
     * update subtotal for paybill
     * @param $total
     * @param $quote
     */
    public function updateCustomSubtotal($total, $quote)
    {
       $invoiceData = $this->getInvoiceDataFromQuote($quote);
        $sales = $invoiceData['amounts']['sales'];
        $total->setSubtotal($sales);
        $total->setBaseSubtotal($sales);
    }

    /**
     * update subtotal for paybill
     * @param $total
     * @param $quote
     */
    public function updateCustomGrandTotal($total, $quote)
    {
        $invoiceData = $this->getInvoiceDataFromQuote($quote);
        //$totalAmount = $invoiceData['amounts']['total'];
        $totalAmount = $invoiceData['amounts']['tax'];
        $total->setBaseGrandTotal($totalAmount);
        $total->setGrandTotal($totalAmount);
    }

    /**
     * update subtotal for paybill
     * @param $total
     * @param $quote
     */
    public function updateCustomTaxAmount($total, $quote)
    {
        $invoiceData = $this->getInvoiceDataFromQuote($quote);
        $tax = $invoiceData['amounts']['tax'];
        $total->setTaxAmount($tax);
        $total->setBaseTaxAmount($tax);
    }

    /**
     * update subtotal for paybill
     * @param $total
     * @param $quote
     */
    public function updateCustomDiscountAmount($total, $quote)
    {
        $invoiceData = $this->getInvoiceDataFromQuote($quote);
        $discount = $invoiceData['amounts']['discount'];
        $total->setDiscountAmount($discount);
        $total->setBaseDiscountAmount($discount);
    }

    /**
     * @param $quote
     * @return array()
     */
    public function getInvoiceDataFromQuote($quote)
    {
        return $this->jsonHelper->jsonDecode($quote->getCustomInvoice());
    }
}
