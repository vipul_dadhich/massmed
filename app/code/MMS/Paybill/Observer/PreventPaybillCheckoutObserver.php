<?php

namespace MMS\Paybill\Observer;

use Magento\Framework\Event\ObserverInterface;

class PreventPaybillCheckoutObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;
    /**
     * @var \MMS\Paybill\Helper\Data
     */
    protected $_config;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \MMS\Paybill\Helper\Data $config,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->redirect = $redirect;
        $this->_config = $config;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * Redirect to login page
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->_config->isEnabled() && $this->getCheckoutSession()->getQuote()->getData('is_paybill')) {
            /** @var \Magento\Framework\App\Action\Action $controller */
            $controller = $observer->getControllerAction();
            $this->redirect->redirect($controller->getResponse(), 'paybill/checkout/index');
        }

        return $this;
    }

    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}