<?php
/**
 * @package     MMS_Paybill
 * @author      Dinesh Kaswan
 * @copyright   Copyright (c) 2019 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Paybill\Observer;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer;
use \Magento\Checkout\Model\Session as CheckoutSession;

class PaymentMethodAvailable implements ObserverInterface
{
	/** @var CheckoutSession */
    protected $checkoutSession;

    /** @var Totals */
    protected $totalsHelper;

    /** @var _allowedMethodPaybill */
    protected $_allowedMethodPaybill = ['md_firstdata', 'paypal_express'];
    /**
     * Search constructor.
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param CheckoutSession $checkoutSession
     * @param Totals $totalsHelper
     */

    public function __construct(
	    \Magento\Framework\Json\Helper\Data $jsonHelper,
	    CheckoutSession $checkoutSession
    ){

        $this->jsonHelper = $jsonHelper;
        $this->checkoutSession = $checkoutSession;
    }
    public function execute(Observer $observer)
    {
        $quote = $this->checkoutSession->getQuote();
        if($quote->getIsPaybill()){
        	$method = $observer->getEvent()->getMethodInstance()->getCode();
			$result          = $observer->getEvent()->getResult();
			if(!in_array($method, $this->_allowedMethodPaybill)){
	            $result->setData('is_available', false);
	        }

        }
        return $this;
    }
}
