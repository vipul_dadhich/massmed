require([
    'jquery',
    'Magento_Ui/js/lib/view/utils/async',
    'domReady!'
], function ($) {
    'use strict';
    var billingAddressIdSelectSelector = 'select[name="billing_address_id"]';

    $.async(billingAddressIdSelectSelector, function (element) {
        $(element).find('option:last').attr("selected", "selected").trigger('change');
    });

    $.async('.checkout-billing-address select[name="billing_address_id"]', function (element) {
        $(element).hide();
    });

    $.async('.opc-progress-bar li:first-child span', function (element) {
        $(element).html("Review & Make Payment");
    });

    $.async('.actions-toolbar button span', function (element) {
        $(element).html('Submit Payment');
    });

    $.async('.opc-block-summary .title', function (element) {
        $(element).html('SUMMARY');
    });

    $.async('.opc-block-summary .items-in-cart .title', function (element) {
        $(element).html('Items');
    });

    $.async('.opc-block-summary .table-totals tbody .sub th', function (element) {
        $(element).html('Subtotal');
    });

    $.async('.opc-block-summary .table-totals tbody .grand th strong', function (element) {
        $(element).html('Total');
    });

    $.async('#billing-save-in-address-book-shared', function (element) {
        $(element).prop('checked', false);
    });


    let timeinterval = setInterval(function(){
        let billingPostcodeVal = $('.billing-address-form input[name="postcode"]').val();
        let billingCityVal = $('.billing-address-form input[name="city"]').val();
        let billingStreet0Val = $('.billing-address-form input[name="street[0]"]').val();
        let billingStreet1Val = $('.billing-address-form input[name="street[1]"]').val();
        let billingRegionVal = $('.billing-address-form input[name="region"]').val();
        if(!billingRegionVal){
            let billingRegionOptionVal = $('.billing-address-form select[name="region_id"]').val();
            if (billingRegionOptionVal !== "") {
                billingRegionVal = $('.billing-address-form select[name="region_id"] option:selected').html();
            }

            let billingAddrFormRegionId = $('.billing-address-form select[name="region_id"]').val();
            let customCheckboxSelector = '.custom-checkbox';
            let customCheckboxInputSelector = '.custom-checkbox input';
            let placeOrderSelector =  '#checkout-payment-method-load .actions-toolbar button.checkout';
            let paypalOrderSelector = "#paypal-express-in-context-button";

            if (billingAddrFormRegionId === '178' && window.checkoutConfig.checkoutCmsPayBill === true) {
                $.async(customCheckboxSelector, function (element) {
                    $(element).show();
                });
                $.async(customCheckboxInputSelector, function (element) {
                    $(element).prop('checked', false);
                });
                $.async(placeOrderSelector, function (element) {
                    $(element).attr('disabled', 'disabled');
                });
                $.async(paypalOrderSelector, function (element) {
                    $(element).addClass('paypal-disabled');
                });
            }
        }
        let billingCountryIdVal = $('.billing-address-form select[name="country_id"] option:selected').html();

        if(billingCountryIdVal !=undefined){
            let streetAddress = [];
            let cityAddress = [];
            let countryAddressStr = billingCountryIdVal;
            if(billingStreet0Val)
                streetAddress.push(billingStreet0Val);
            if(billingStreet1Val)
                streetAddress.push(billingStreet1Val);

            if(billingCityVal)
                cityAddress.push(billingCityVal);
            if(billingRegionVal)
                cityAddress.push(billingRegionVal);
            if(billingPostcodeVal)
                cityAddress.push(billingPostcodeVal);

            let streetAddressStr = streetAddress.join(", ");
            let cityAddressStr = cityAddress.join(", ");

            if(countryAddressStr.length>0 && streetAddressStr.length>0 && cityAddressStr.length>0){
                $('.billing-address-form').after(
                    "<div class='custom-address'>" +
                    "<span>" + streetAddressStr+"</span>" +
                    "</br>" +
                    "<span>" + cityAddressStr+"</span>" +
                    "</br>" +
                    "<span>" + countryAddressStr+"</span>" +
                    "</div>"
                );

                clearTimeInterval();
            }

        }
    },1000);

    function clearTimeInterval(){
        clearInterval(timeinterval);
    }




});
