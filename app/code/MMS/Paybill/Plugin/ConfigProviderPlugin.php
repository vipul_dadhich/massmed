<?php

namespace MMS\Paybill\Plugin;

class ConfigProviderPlugin
{
    /**
     * Url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * Paybill checkout helper
     *
     * @var \MMS\Paybill\Helper\Data
     */
    protected $_config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * Initialize dependencies.
     *
     * @param \MMS\Paybill\Helper\Data $config
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \MMS\Paybill\Helper\Data $config,
        \Magento\Framework\UrlInterface $url,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Api\PaymentMethodManagementInterface $paymentMethodManagement
    ) {
        $this->_config = $config;
        $this->_checkoutSession = $checkoutSession;
        $this->url = $url;
        $this->paymentMethodManagement = $paymentMethodManagement;
    }

    /**
     * @param \Magento\Checkout\Model\DefaultConfigProvider $subject
     * @param array $result
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, array $result)
    {
        if ($this->_config->isEnabled()
            && $this->getCheckoutSession()->getQuote()->getData('is_paybill')
        ) {
            $result['checkoutUrl'] = $this->url->getUrl('paybill/checkout');
            $result['customerData']['customer_default_country'] = '';
            if ($this->_config->isPaybillSuccessEnabled()) {
                $result['defaultSuccessPageUrl'] = 'paybill/checkout/success';
            }
        }
       
        return $result;
    }

    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}