<?php

namespace MMS\Paybill\Plugin;
use Magento\Framework\View\LayoutInterface;

class CompositeConfigProviderPlugin
{
    /**
     * Url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * Paybill checkout helper
     *
     * @var \MMS\Paybill\Helper\Data
     */
    protected $_config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    protected $autoRenewalLanguageStatus = ['SUSPENDED', 'BILLED'];
    /**
     * Initialize dependencies.
     *
     * @param \MMS\Paybill\Helper\Data $config
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        LayoutInterface $layout,
        \MMS\Paybill\Helper\Data $config,
        \Magento\Framework\UrlInterface $url,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Api\PaymentMethodManagementInterface $paymentMethodManagement
    ) {
        $this->_config = $config;
        $this->_layout = $layout;
        $this->_checkoutSession = $checkoutSession;
        $this->url = $url;
        $this->paymentMethodManagement = $paymentMethodManagement;
    }

    /**
     * @param \Magento\Checkout\Model\CompositeConfigProvider $subject
     * @param array $result
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetConfig(\Magento\Checkout\Model\CompositeConfigProvider $subject, array $result)
    {
        $paymentMethodTitle = 'Credit Card';
        if ($this->_config->isEnabled() && $this->getCheckoutSession()->getQuote()->getData('is_paybill')) {
            $result['checkoutCmsPayBill'] = true;
            if (in_array($this->getCheckoutSession()->getQuote()->getData('acs_order_status'), $this->autoRenewalLanguageStatus)) {
                $result['checkoutCmsPayBill'] = false;
                foreach($this->getPaymentMethods() as $paymentMethod)
                {
                    $result['checkoutCms'][$paymentMethod['code']] = '';
                }
            } else {
                foreach($this->getPaymentMethods() as $paymentMethod)
                {
                    $result['checkoutCms'][$paymentMethod['code']] = '';
                    if ($paymentMethod['code'] == 'md_firstdata' && $this->_config->getLanguageBlock()) {
                        $result['checkoutCms'][$paymentMethod['code']] = $this->_layout->createBlock('Magento\Cms\Block\Block')->setBlockId($this->_config->getLanguageBlock())->toHtml();
                    }
                    if ($paymentMethod['code'] == 'paypal_express' && $this->_config->getPaypalLanguageBlock()) {
                        $result['checkoutCms'][$paymentMethod['code']] = $this->_layout->createBlock('Magento\Cms\Block\Block')->setBlockId($this->_config->getPaypalLanguageBlock())->toHtml();
                    }
                    if ($paymentMethod['code'] == 'md_firstdata') {
                        $paymentMethodTitle = 'Credit Card with Automatic Renewal';
                    }
                }
            }
            $result['payment']['md_firstdata']['storedCards'] = array();
            $result['payment']['md_firstdata']['storedCards']['new'] = 'Use other card';
            $result['payment']['md_firstdata']['canSaveCard'] = false;
            $result['payment']['md_firstdata']['title'] = __($paymentMethodTitle);
            $result['paymentMethods'] = $this->getPaymentMethods();
            if (isset($result['paymentMethods']) && !empty($result['paymentMethods'])) {
                foreach ($result['paymentMethods'] as $key => $method) {
                    if (isset($method['code']) && $method['code'] == 'md_firstdata') {
                        $result['paymentMethods'][$key]['title'] = __($paymentMethodTitle);
                        break;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
    /**
     * Get list of available payment methods
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    private function getPaymentMethods()
    {
        $paymentMethods = [];
        $quote = $this->_checkoutSession->getQuote();
        if ($quote->getIsActive()) {
            foreach ($this->paymentMethodManagement->getList($quote->getId()) as $paymentMethod) {
                $paymentMethods[] = [
                    'code' => $paymentMethod->getCode(),
                    'title' => $paymentMethod->getTitle()
                ];
            }
        }
        return $paymentMethods;
    }
}
