<?php
namespace MMS\Paybill\Plugin;

class LayoutProcessor
{
    /**
    * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
    * @param array $jsLayout
    * @return array
    */
    public function __construct(\Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $collectionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_jsonHelper = $jsonHelper;
        $this->collectionFactory = $collectionFactory;
        $this->_countryFactory = $countryFactory;
    }
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {
        if($this->_checkoutSession->getQuote()->getIsPaybill()){
        $addressData = $this->getAddressData();
        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['firstname']['value'] = $addressData['firstname'];

        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['firstname']['disabled'] = true;



        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['lastname']['value'] = $addressData['lastname'];

        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['lastname']['disabled'] = true;



        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['city']['value'] = $addressData['city'];

        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['city']['disabled'] = true;

        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['postcode']['value'] = $addressData['postcode'];

        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['postcode']['disabled'] = true;
        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['postcode']['validation']['required-entry'] = false;


            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['country_id']['value'] = $addressData['country'];

        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['country_id']['disabled'] = true;

        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['region']['value'] = $addressData['region'];
        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['region_id']['value'] = $addressData['region'];


        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['region']['disabled'] = true;
        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['region_id']['disabled'] = true;
        if (
            isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['street']['children']) &&
            is_array($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['street']['children'])
        ) {
            $count = 0;
            foreach ($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['street']['children'] as $k => $v) {
                $nv = $v;
                $nv['value'] = $addressData['street'][$count];
                $nvd['disabled'] = 'true';
                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['street']['children'][$k] = $nv;

                $count++;
            }
            foreach ($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['street']['children'] as $k1 => $v1) {
                $nvd = $v1;
                $nvd['disabled'] = 'true';

                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['street']['children'][$k1] = $nvd;
                $count++;
            }
        }
    }
        //$jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['street[1]']['value'] = 'Street 2';
        return $jsLayout;
    }
    public function getAddressData(){
        $request = $this->_jsonHelper->jsonDecode($this->_checkoutSession->getQuote()->getCustomInvoice());
        $addressArray = array();
        if(isset($request['billTo'])){
            $regionId = null;
            $addressObj = $request['billTo'];
            $address1 = isset($addressObj['address'])?$addressObj['address']:'';
            $address2 = isset($addressObj['address2'])?$addressObj['address2']:'';
            $countryId = $this->getCountryCode($addressObj['country']);
            if (isset($addressObj['state'])) {
                $regionId = $this->getRegionId($addressObj['state'], $countryId);
                if (!$regionId) {
                    $regionId = $addressObj['state'];
                }
            }

            $addressArray['firstname'] = $addressObj['firstName'];
            $addressArray['lastname'] = $addressObj['lastName'];
            $addressArray['city'] = $addressObj['city'];
            $addressArray['region'] = ($regionId != null) ? $regionId : '';
            $addressArray['country'] = $countryId;
            $addressArray['postcode'] = $addressObj['postalCode'] ?? '';
            $addressArray['street'] = array($address1, $address2, '');
        }
        return $addressArray;

    }
    public function getRegionId($region, $countryId){
        $regionCode = $this->collectionFactory->create()
            ->addRegionCodeFilter($region)
            ->addFieldToFilter('country_id', $countryId)
            ->getFirstItem()
            ->toArray();
        if(!empty($regionCode) && isset($regionCode['region_id'])){
            return $regionCode['region_id'];
        }
        return $region;
    }
    public function getCountryCode($countryCode){
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        return $country->getCountryId();
    }
}
