<?php

namespace MMS\Paybill\Plugin\Checkout\CustomerData;
/**
 * Class Cart
 * @package MMS\Paybill\Plugin\Checkout\CustomerData
 */
class Cart
{
    /**
     * @var \Magento\Quote\Model\Quote|null
     */
    protected $quote = null;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * @var \MMS\Paybill\Helper\Data
     */
    protected $config;

    /**
     * Cart constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \MMS\Paybill\Helper\Data $config
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \MMS\Paybill\Helper\Data $config
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->config = $config;
    }

    /**
     * Add paybill data to result
     *
     * @param \Magento\Checkout\CustomerData\Cart $subject
     * @param array $result
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetSectionData(\Magento\Checkout\CustomerData\Cart $subject, $result) {
        $result['is_paybill_active'] = (bool) $this->config->isEnabled();
        $result['is_quote_paybill'] = (bool) $this->getQuote()->getData('is_paybill');

        return $result;
    }

    /**
     * Get active quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    protected function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }
        return $this->quote;
    }
}
