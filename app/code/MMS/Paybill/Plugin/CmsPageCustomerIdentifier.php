<?php

namespace MMS\Paybill\Plugin;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Http\Context;
use Magento\Framework\App\PageCache\Identifier;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Serialize\Serializer\Json;
use MMS\Paybill\Helper\CustomerData as ConfigHelper;
use MMS\Paybill\Helper\Data as DataHelper;
use Magento\PageCache\Model\Config;

class CmsPageCustomerIdentifier
{
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var DataHelper
     */
    private $dataHelper;
    /**
     * @var Session
     */
    protected $_customerSession;
    /**
     * @var Config
     */
    protected $config;
    /**
     * @var Http
     */
    protected $request;
    /**
     * @var Json
     */
    protected $serializer;
    /**
     * @var Context
     */
    protected $context;

    /**
     * @param Http $request
     * @param Context $context
     * @param Json $serializer
     * @param Config $config
     * @param ConfigHelper $configHelper
     * @param DataHelper $dataHelper
     * @param Session $customerSession
     */
    public function __construct(
        Http $request,
        Context $context,
        Json $serializer,
        Config $config,
        ConfigHelper $configHelper,
        DataHelper $dataHelper,
        Session $customerSession
    ) {
        $this->request = $request;
        $this->context = $context;
        $this->serializer = $serializer;
        $this->config = $config;
        $this->configHelper = $configHelper;
        $this->dataHelper = $dataHelper;
        $this->_customerSession = $customerSession;
    }

    /**
     * Add/Override a unique key identifier for paybill specific page and variables that skips X-Magento-Vary cookie
     *
     * @param Identifier $identifier
     * @param string $result
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetValue(Identifier $identifier, $result)
    {
        if ($this->config->isEnabled() && $this->dataHelper->isEnabled()) {
            $urlString = $this->request->getUriString();
            if (strpos($urlString, 'no-payment-due') !== false) {
                $payBillStatus = $this->configHelper->getCustomerAudienceType();
                if (!$payBillStatus) {
                    $payBillStatus = 'NOT_A_SUBSCRIBER';
                }

                $data = [
                    $this->request->isSecure(),
                    $this->request->getUriString(),
                    $this->request->get(\Magento\Framework\App\Response\Http::COOKIE_VARY_STRING)
                        ?: $this->context->getVaryString(),
                    sha1($this->serializer->serialize(['PAY_BILL_STATUS' => $payBillStatus]))
                ];

                return sha1($this->serializer->serialize($data));
            }
        }
        return $result;
    }
}