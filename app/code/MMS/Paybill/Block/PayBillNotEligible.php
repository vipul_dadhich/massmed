<?php

namespace MMS\Paybill\Block;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template\Context;
use MMS\Paybill\Helper\CustomerData;
use MMS\Paybill\Model\Catalyst\Api\CheckEligibility;

/**
 * Class PaybillNotEligible
 * @package MMS\Paybill\Block
 */
class PayBillNotEligible extends \Magento\Framework\View\Element\Template
{
    const OTHERS_BLOCK_ID = 'no-payment-due-others';
    const SUBSCRIBER_BLOCK_ID = 'no-payment-due-subscriber';
    const NEJM_OTHERS_BLOCK_ID = 'nejm-no-payment-due-others';
    const NEJM_SUBSCRIBER_BLOCK_ID = 'nejm-no-payment-due-subscriber';

    /**
     * @var CustomerData
     */
    protected $dataHelper;

    /**
     * PayBillNotEligible constructor.
     * @param Context $context
     * @param CustomerData $dataHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        CustomerData $dataHelper,
        CheckEligibility $checkEligibility,
        array $data = []
    ) {
        $this->checkEligibility = $checkEligibility;
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
    }

    /**
     * @return mixed
     */
    public function getCustomerAudienceType()
    {
        return $this->dataHelper->getCustomerAudienceType();
    }

    /**
     * @return string
     */
    public function getBlockIdByPayBillStatus()
    {
        $payBillStatus = $this->getCustomerAudienceType();
        if (strtolower($this->checkEligibility->getProductFamily()) == 'nejm') {
            $blockId = self::NEJM_OTHERS_BLOCK_ID;
            if ($payBillStatus == 'SUBSCRIBER') {
                $blockId = self::NEJM_SUBSCRIBER_BLOCK_ID;
            }
        } else {
            $blockId = self::OTHERS_BLOCK_ID;
            if ($payBillStatus == 'SUBSCRIBER') {
                $blockId = self::SUBSCRIBER_BLOCK_ID;
            }
        }
        return $blockId;
    }

    /**
     * @return bool
     */
    public function getCmsBlockHtml()
    {
        try {
            return $this->getLayout()->createBlock(
                \Magento\Cms\Block\Block::class
            )->setBlockId(
                $this->getBlockIdByPayBillStatus()
            )->toHtml();
        } catch (LocalizedException $e) {
            return false;
        }
    }

    /**
     * Get block cache life time
     *
     * @return int|bool|null
     */
    protected function getCacheLifetime()
    {
        return null;
    }

}
