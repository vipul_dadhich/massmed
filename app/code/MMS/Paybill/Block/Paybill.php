<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_Paybill extension
 * NOTICE OF LICENSE
 */

namespace MMS\Paybill\Block;

class Paybill extends \Magento\Framework\View\Element\Template
{
	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\SessionFactory $customerSession
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
    }
    public function isCustomerLoggedIn(){
    	$sessionCustomer = $this->customerSession->create();
    	return $sessionCustomer->isLoggedIn();
    }
}