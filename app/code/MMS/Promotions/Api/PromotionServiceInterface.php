<?php

namespace MMS\Promotions\Api;

use MMS\PriceEngine\Service\PriceEngine;
use MMS\Promotions\Model\Promotion;

/**
 * Interface PromotionServiceInterface
 * @package MMS\Promotions\Api
 */
interface PromotionServiceInterface
{
    /**
     * @return Promotion|bool
     */
    public function execute();

    /**
     * @return PriceEngine
     */
    public function getPriceServiceEngine(): PriceEngine;

    /**
     * @param $promotionData
     * @param $sku
     * @return array|bool
     */
    public function filterByProductSku($promotionData, $sku);
}
