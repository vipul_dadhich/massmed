<?php
namespace MMS\Promotions\Api;

use MMS\Promotions\Api\Data\PcTermsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @api
 */
interface PcTermsRepositoryInterface
{
    /**
     * Save Promotion.
     *
     * @param \MMS\Promotions\Api\Data\PcTermsInterface $pcTerms
     * @return \MMS\Promotions\Api\Data\PcTermsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(PcTermsInterface $pcTerms);

    /**
     * Retrieve PcTerms
     *
     * @param int $Id
     * @return \MMS\Promotions\Api\Data\PcTermsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve Promotions matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MMS\Promotions\Api\Data\PcTermsSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete PcTerms.
     *
     * @param \MMS\Promotions\Api\Data\PcTermsInterface $pcTerms
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(PcTermsInterface $pcTerms);

    /**
     * Delete PcTerm by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
