<?php
namespace MMS\Promotions\Api;

use MMS\Promotions\Api\Data\PromotionInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @api
 */
interface PromotionRepositoryInterface
{
    /**
     * Save Promotion.
     *
     * @param \MMS\Promotions\Api\Data\PromotionInterface $promotion
     * @return \MMS\Promotions\Api\Data\PromotionInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(PromotionInterface $promotion);

    /**
     * Retrieve Promotion
     *
     * @param int $promotionId
     * @return \MMS\Promotions\Api\Data\PromotionInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($promotionId);

    /**
     * Retrieve Promotions matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MMS\Promotions\Api\Data\PromotionSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Promotion.
     *
     * @param \MMS\Promotions\Api\Data\PromotionInterface $promotion
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(PromotionInterface $promotion);

    /**
     * Delete Promotion by ID.
     *
     * @param int $promotionId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($promotionId);
}
