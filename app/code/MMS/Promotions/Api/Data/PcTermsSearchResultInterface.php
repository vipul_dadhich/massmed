<?php
namespace MMS\Promotions\Api\Data;

/**
 * @api
 */
interface PcTermsSearchResultInterface
{
    /**
     * Get PcTerms list.
     *
     * @return \MMS\Promotions\Api\Data\PcTermsInterface[]
     */
    public function getItems();

    /**
     * Set PcTerms list.
     *
     * @param \MMS\Promotions\Api\Data\PcTermsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
