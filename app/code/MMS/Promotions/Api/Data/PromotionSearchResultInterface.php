<?php
namespace MMS\Promotions\Api\Data;

/**
 * @api
 */
interface PromotionSearchResultInterface
{
    /**
     * Get Promotions list.
     *
     * @return \MMS\Promotions\Api\Data\PromotionInterface[]
     */
    public function getItems();

    /**
     * Set Promotions list.
     *
     * @param \MMS\Promotions\Api\Data\PromotionInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
