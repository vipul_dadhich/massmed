<?php
namespace MMS\Promotions\Api\Data;

/**
 * @api
 */
interface PromotionInterface
{
    /**
     * ID
     *
     * @var string
     */
    const PROMOTION_ID = 'promotion_id';

    /**
     * Campaign Code attribute constant
     *
     * @var string
     */
    const CAMPAIGN_CODE = 'campaign_code';

    /**
     * Promotion Code attribute constant
     *
     * @var string
     */
    const PROMO_CODE = 'promo_code';

    /**
     * Promotion Description attribute constant
     *
     * @var string
     */
    const PROMO_DESCRIPTION = 'promo_description';

    /**
     * Cart Anchor one attribute constant
     *
     * @var string
     */
    const CART_ANCHOR_ONE = 'cartanchorone';

    /**
     * Cart Anchor two attribute constant
     *
     * @var string
     */
    const CART_ANCHOR_TWO = 'cartanchortwo';

    /**
     * Checkout Anchor one attribute constant
     *
     * @var string
     */
    const CHECKOUT_ANCHOR_ONE = 'checkoutanchorone';

    /**
     * Checkout Anchor two attribute constant
     *
     * @var string
     */
    const CHECKOUT_ANCHOR_TWO = 'checkoutanchortwo';

    /**
     * Confirmation Anchor one attribute constant
     *
     * @var string
     */
    const CONFIRMATION_ANCHOR_ONE = 'confirmationanchorone';

    /**
     * Confirmation Anchor two attribute constant
     *
     * @var string
     */
    const CONFIRMATION_ANCHOR_TWO = 'confirmationanchortwo';

    /**
     * Landing Anchor one attribute constant
     *
     * @var string
     */
    const LANDING_ANCHOR_ONE = 'landinganchorone';

    /**
     * Landing anchor two Anchor two attribute constant
     *
     * @var string
     */
    const LANDING_ANCHOR_TWO = 'landinganchortwo';

    /**
     * Price config file attribute constant
     *
     * @var string
     */
    const PRICE_CONFIG_FILE = 'priceconfigfile';

    /**
     * Free Gift SKU attribute constant
     *
     * @var string
     */
    const FREE_GIFT_SKU = 'free_gift_sku';

    /**
     * Free Gift Eligible SKU attribute constant
     *
     * @var string
     */
    const FREE_GIFT_ELIGIBLE_SKUS = 'free_gift_eligible_skus';

    /**
     * Default Professional Category attribute constant
     *
     * @var string
     */
    const DEFAULT_PROFESSIONAL_CATEGORY = 'professional_category';

    /**
     * Allowed Payment attribute constant
     *
     * @var string
     */
    const ALLOWED_PAYMENT = 'allowed_payments';

    /**
     * Bind To Template attribute constant
     *
     * @var string
     */
    const BINDED_TEMPLATE = 'binded_template';

    /**
     * Live Date attribute constant
     *
     * @var string
     */
    const LIVE_DATE = 'live_date';

    /**
     * End Date attribute constant
     *
     * @var string
     */
    const END_DATE = 'end_date';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getPromotionId();

    /**
     * Set ID
     *
     * @param int $promotionId
     * @return PromotionInterface
     */
    public function setPromotionId($promotionId);

    /**
     * Get Campaign Code
     *
     * @return mixed
     */
    public function getCampaignCode();

    /**
     * Set Campaign Code
     *
     * @param mixed $campaignCode
     * @return PromotionInterface
     */
    public function setCampaignCode($campaignCode);

    /**
     * Get Promotion Code
     *
     * @return mixed
     */
    public function getPromoCode();

    /**
     * Set Promotion Code
     *
     * @param mixed $promoCode
     * @return PromotionInterface
     */
    public function setPromoCode($promoCode);

    /**
     * Get Promotion Description
     *
     * @return mixed
     */
    public function getPromoDescription();

    /**
     * Set Promotion Description
     *
     * @param mixed $promoDescription
     * @return PromotionInterface
     */
    public function setPromoDescription($promoDescription);

    /**
     * Get Cart Anchor One
     *
     * @return mixed
     */
    public function getCartanchorone();

    /**
     * Set Cart Anchor One
     *
     * @param mixed $cartanchorone
     * @return PromotionInterface
     */
    public function setCartanchorone($cartanchorone);

    /**
     * Get Cart Anchor Two
     *
     * @return mixed
     */
    public function getCartanchortwo();

    /**
     * Set Cart Anchor Two
     *
     * @param mixed $cartanchortwo
     * @return PromotionInterface
     */
    public function setCartanchortwo($cartanchortwo);

    /**
     * Get Checkout Anchor One
     *
     * @return mixed
     */
    public function getCheckoutanchorone();

    /**
     * Set Checkout Anchor One
     *
     * @param mixed $checkoutanchorone
     * @return PromotionInterface
     */
    public function setCheckoutanchorone($checkoutanchorone);

    /**
     * Get Checkout Anchor Two
     *
     * @return mixed
     */
    public function getCheckoutanchortwo();

    /**
     * Set Checkout Anchor Two
     *
     * @param mixed $checkoutanchortwo
     * @return PromotionInterface
     */
    public function setCheckoutanchortwo($checkoutanchortwo);

    /**
     * Get Confirmation Anchor One
     *
     * @return mixed
     */
    public function getConfirmationanchorone();

    /**
     * Set Confirmation Anchor One
     *
     * @param mixed $confirmationanchorone
     * @return PromotionInterface
     */
    public function setConfirmationanchorone($confirmationanchorone);

    /**
     * Get Confirmation Anchor Two
     *
     * @return mixed
     */
    public function getConfirmationanchortwo();

    /**
     * Set Confirmation Anchor Two
     *
     * @param mixed $confirmationanchortwo
     * @return PromotionInterface
     */
    public function setConfirmationanchortwo($confirmationanchortwo);

    /**
     * Get Landing Page Anchor One
     *
     * @return mixed
     */
    public function getLandinganchorone();

    /**
     * Set Landing Page Anchor One
     *
     * @param mixed $landinganchorone
     * @return PromotionInterface
     */
    public function setLandinganchorone($landinganchorone);

    /**
     * Get Landing Page Anchor Two
     *
     * @return mixed
     */
    public function getLandinganchortwo();

    /**
     * Set Landing Page Anchor Two
     *
     * @param mixed $landinganchortwo
     * @return PromotionInterface
     */
    public function setLandinganchortwo($landinganchortwo);

    /**
     * Get Price Config File
     *
     * @return mixed
     */
    public function getPriceconfigfile();

    /**
     * Set Price Config File
     *
     * @param mixed $priceconfigfile
     * @return PromotionInterface
     */
    public function setPriceconfigfile($priceconfigfile);

    /**
     * Get Free Gift SKU
     *
     * @return mixed
     */
    public function getFreeGiftSku();

    /**
     * Set Free Gift
     *
     * @param mixed $freeGiftSku
     * @return PromotionInterface
     */
    public function setFreeGiftSku($freeGiftSku);

    /**
     * Get Free Gift Eligible Skus
     *
     * @return mixed
     */
    public function getFreeGiftEligibleSkus();

    /**
     * Set Free Gift Products
     *
     * @param mixed $freeGiftEligibleSkus
     * @return PromotionInterface
     */
    public function setFreeGiftEligibleSkus($freeGiftEligibleSkus);

    /**
     * Get Professional Category
     *
     * @return mixed
     */
    public function getProfessionalCategory();

    /**
     * Set Professional Category
     *
     * @param mixed $ProfessionalCategory
     * @return PromotionInterface
     */
    public function setProfessionalCategory($ProfessionalCategory);

    /**
     * Get Allowed Payments
     *
     * @return mixed
     */
    public function getAllowedPayments();

    /**
     * Set Allowed Payments
     *
     * @param mixed $allowedPayments
     * @return PromotionInterface
     */
    public function setAllowedPayments($allowedPayments);

    /**
     * Get Bind To Template
     *
     * @return mixed
     */
    public function getBindedTemplate();

    /**
     * Set Bind To Template
     *
     * @param mixed $bindedTemplate
     * @return PromotionInterface
     */
    public function setBindedTemplate($bindedTemplate);

    /**
     * Get Live Date
     *
     * @return mixed
     */
    public function getLiveDate();

    /**
     * Set Live Date
     *
     * @param mixed $liveDate
     * @return PromotionInterface
     */
    public function setLiveDate($liveDate);

    /**
     * Get End Date
     *
     * @return mixed
     */
    public function getEndDate();

    /**
     * Set End Date
     *
     * @param mixed $endDate
     * @return PromotionInterface
     */
    public function setEndDate($endDate);
}
