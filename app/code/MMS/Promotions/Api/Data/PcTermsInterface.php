<?php
namespace MMS\Promotions\Api\Data;

/**
 * @api
 */
interface PcTermsInterface
{
    /**
     * ID
     *
     * @var string
     */
    const ID = 'entity_id';

    /**
     * Campaign Code attribute constant
     *
     * @var string
     */
    const CAMPAIGN_CODE = 'campaign_code';

    /**
     * Promotion Code attribute constant
     *
     * @var string
     */
    const PROMO_CODE = 'promo_code';

    /**
     * Professional category attribute constant
     *
     * @var string
     */
    const PROFESSIONAL_CATEGORY = 'professional_category';

    /**
     * Term attribute constant
     *
     * @var string
     */
    const TERM = 'term';

    /**
     * Term attribute constant
     *
     * @var string
     */
    const SKU = 'sku';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getEntityId();

    /**
     * Set ID
     *
     * @param int $Id
     * @return PcTermsInterface
     */
    public function setEntityId($Id);

    /**
     * Get Campaign Code
     *
     * @return mixed
     */
    public function getCampaignCode();

    /**
     * Set Campaign Code
     *
     * @param mixed $campaignCode
     * @return PcTermsInterface
     */
    public function setCampaignCode($campaignCode);

    /**
     * Get Promotion Code
     *
     * @return mixed
     */
    public function getPromoCode();

    /**
     * Set Promotion Code
     *
     * @param mixed $promoCode
     * @return PcTermsInterface
     */
    public function setPromoCode($promoCode);

    /**
     * Get Professional Category
     *
     * @return mixed
     */
    public function getProfessionalCategory();

    /**
     * Set Professional Category
     *
     * @param mixed $professionalCategory
     * @return PcTermsInterface
     */
    public function setProfessionalCategory($professionalCategory);

    /**
     * Get term
     *
     * @return mixed
     */
    public function getTerm();

    /**
     * Set term
     *
     * @param mixed $term
     * @return PcTermsInterface
     */
    public function setTerm($term);
}
