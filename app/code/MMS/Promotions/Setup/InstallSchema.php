<?php
namespace MMS\Promotions\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package MMS\Promotions\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * install tables
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('mms_promotions_promotion')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('mms_promotions_promotion')
            )
            ->addColumn(
                'promotion_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Promotion ID'
            )
            ->addColumn(
                'campaign_code',
                Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'Promotion Campaign Code'
            )
            ->addColumn(
                'promo_code',
                Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'Promotion Promotion Code'
            )
            ->addColumn(
                'promo_description',
                Table::TYPE_TEXT,
                '64k',
                [],
                'Promotion Promotion Description'
            )
            ->addColumn(
                'cartanchorone',
                Table::TYPE_TEXT,
                255,
                [],
                'Promotion Cart View Anchor 1'
            )
            ->addColumn(
                'cartanchortwo',
                Table::TYPE_TEXT,
                255,
                [],
                'Promotion Cart View Anchor 2'
            )
            ->addColumn(
                'checkoutanchorone',
                Table::TYPE_TEXT,
                255,
                [],
                'Promotion Checkout View Anchor 1'
            )
            ->addColumn(
                'checkoutanchortwo',
                Table::TYPE_TEXT,
                255,
                [],
                'Promotion Checkout View Anchor 2'
            )
            ->addColumn(
                'confirmationanchorone',
                Table::TYPE_TEXT,
                255,
                [],
                'Promotion Confirmation View Anchor 1'
            )
            ->addColumn(
                'confirmationanchortwo',
                Table::TYPE_TEXT,
                255,
                [],
                'Promotion Confirmation View Anchor 2'
            )
            ->addColumn(
                'landinganchorone',
                Table::TYPE_TEXT,
                255,
                [],
                'Promotion Landing Page View Anchor 1'
            )
            ->addColumn(
                'landinganchortwo',
                Table::TYPE_TEXT,
                255,
                [],
                'Promotion Landing Page View Anchor 2'
            )
            ->addColumn(
                'free_gift_sku',
                Table::TYPE_TEXT,
                255,
                [],
                'Promotion Free Gift SKU'
            )
            ->addColumn(
                'free_gift_eligible_skus',
                Table::TYPE_TEXT,
                '64k',
                [],
                'Products Eligible for Free Gift'
            )
            ->addColumn(
                'professional_category',
                Table::TYPE_INTEGER,
                null,
                [],
                'Promotion Default Professional Category'
            )
            ->addColumn(
                'allowed_payments',
                Table::TYPE_TEXT,
                '64k',
                [],
                'Promotion Allowed Payment Methods'
            )
            ->addColumn(
                'binded_template',
                Table::TYPE_TEXT,
                '64k',
                [],
                'Promotion Bind To Template'
            )
            ->addColumn(
                'live_date',
                Table::TYPE_DATETIME,
                null,
                [],
                'Promotion Live Date'
            )
            ->addColumn(
                'end_date',
                Table::TYPE_DATETIME,
                null,
                [],
                'Promotion End Date'
            )
            ->addColumn(
                'priceconfigfile',
                Table::TYPE_TEXT,
                '64k',
                [],
                'Price Config File'
            )
            ->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => Table::TIMESTAMP_INIT
                ],
                'Promotion Created At'
            )
            ->addColumn(
                'updated_at',
                Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => Table::TIMESTAMP_INIT_UPDATE
                ],
                'Promotion Updated At'
            )
            ->setComment('Promotion Table');
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable('mms_promotions_promotion'),
                $setup->getIdxName(
                    $installer->getTable('mms_promotions_promotion'),
                    ['campaign_code', 'promo_code', 'promo_description', 'cartanchorone', 'cartanchortwo',
                        'checkoutanchorone', 'checkoutanchortwo', 'confirmationanchorone', 'confirmationanchortwo',
                        'landinganchorone', 'landinganchortwo', 'free_gift_sku'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['campaign_code', 'promo_code', 'promo_description', 'cartanchorone', 'cartanchortwo',
                    'checkoutanchorone', 'checkoutanchortwo', 'confirmationanchorone', 'confirmationanchortwo',
                    'landinganchorone', 'landinganchortwo', 'free_gift_sku'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }
        $installer->endSetup();
    }
}
