<?php
namespace MMS\Promotions\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 * @package MMS\Promotions\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * install tables
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $connection = $setup->getConnection();
            $connection->changeColumn(
                    'mms_promotions_promotion',
                    'professional_category',
                    'professional_category',
                    ['type' => Table::TYPE_TEXT, 'nullable' => false, 'default' => ''],
                    'Professional Category'
                );

        }
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                    'mms_promotions_promotion',
                    'is_synced',
                        [
                        'type' => Table::TYPE_INTEGER,
                        'length' => 2,
                        'nullable' => true,
                        'comment' => 'Is Synced with ACS'
                    ]
                );
        }
        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                    'cms_page',
                    'is_promotional',
                        [
                        'type' => Table::TYPE_INTEGER,
                        'length' => 2,
                        'nullable' => true,
                        'comment' => 'Is Promotional Template'
                    ]
                );
        }
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                    'mms_promotions_promotion',
                    'premium_set_code',
                        [
                        'type' => Table::TYPE_TEXT,
                        'length' => 255,
                        'nullable' => true,
                        'comment' => 'Premium Set Code'
                    ]
                );
        }

        if (version_compare($context->getVersion(), '1.1.0') < 0) {
            $connection = $setup->getConnection();
            if (!$connection->isTableExists('mms_promotions_pc_terms_relation')) {
                $table = $connection->newTable(
                    $setup->getTable('mms_promotions_pc_terms_relation')
                )
                    ->addColumn(
                        'entity_id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'nullable' => false,
                            'primary' => true,
                            'unsigned' => true,
                        ],
                        'Entity ID'
                    )
                    ->addColumn(
                        'campaign_code',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable => false'],
                        'Promotion Campaign Code'
                    )
                    ->addColumn(
                        'promo_code',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable => false'],
                        'Promotion Code'
                    )
                    ->addColumn(
                        'professional_category',
                        Table::TYPE_TEXT,
                        '255',
                        [],
                        'Professional Category'
                    )
                    ->addColumn(
                        'term',
                        Table::TYPE_TEXT,
                        255,
                        [],
                        'Professional Category Term'
                    )
                    ->addColumn(
                        'created_at',
                        Table::TYPE_TIMESTAMP,
                        null,
                        [
                            'nullable' => false,
                            'default' => Table::TIMESTAMP_INIT
                        ],
                        'PC Created At'
                    )
                    ->addColumn(
                        'updated_at',
                        Table::TYPE_TIMESTAMP,
                        null,
                        [
                            'nullable' => false,
                            'default' => Table::TIMESTAMP_INIT_UPDATE
                        ],
                        'PC Updated At'
                    )
                    ->setComment('Professional Category and Terms Relationship Table');
                $connection->createTable($table);

                $connection->addIndex(
                    $setup->getTable('mms_promotions_pc_terms_relation'),
                    $setup->getIdxName(
                        $setup->getTable('mms_promotions_pc_terms_relation'),
                        ['campaign_code', 'promo_code', 'professional_category', 'term'],
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    ['campaign_code', 'promo_code', 'professional_category', 'term'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                );
            }
        }
        if (version_compare($context->getVersion(), '1.1.2') < 0) {
            $connection = $setup->getConnection();
                $connection->addColumn(
                    'mms_promotions_pc_terms_relation',
                    'sku',
                    [
                        'type' => Table::TYPE_TEXT,
                        'length' => 255,
                        'nullable' => true,
                        'comment' => 'SKU'
                    ]
                );
            }
        $setup->endSetup();
    }
}
