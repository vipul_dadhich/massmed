<?php

namespace MMS\Promotions\Service;

use MMS\PriceEngine\Service\PriceEngine;
use MMS\Promotions\Api\PromotionServiceInterface;
use MMS\Promotions\Helper\Data as DataHelper;
use MMS\Promotions\Model\Promotion as PromotionModel;
use MMS\Promotions\Model\ResourceModel\PcTerms\CollectionFactory as PcTermsCollectionFactory;

/**
 * Class PromotionService
 * @package MMS\Promotions\Service
 */
class PromotionService implements PromotionServiceInterface
{
    /**
     * @var array
     */
    private $promotions = [];

    /**
     * @var array
     */
    private $promotionsTerms = [];

    /**
     * @var DataHelper
     */
    private $_dataHelper;

    /**
     * @var PriceEngine
     */
    protected $_priceEngineService;

    /**
     * @var PcTermsCollectionFactory
     */
    protected $_pcTermsCollectionFactory;

    /**
     * PromotionService constructor.
     * @param DataHelper $dataHelper
     * @param PriceEngine $priceEngineService
     * @param PcTermsCollectionFactory $pcTermsCollectionFactory
     */
    public function __construct(
        DataHelper $dataHelper,
        PriceEngine $priceEngineService,
        PcTermsCollectionFactory $pcTermsCollectionFactory
    ) {
        $this->_dataHelper = $dataHelper;
        $this->_priceEngineService = $priceEngineService;
        $this->_pcTermsCollectionFactory = $pcTermsCollectionFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        if ($this->_dataHelper->isModuleEnabled() && $this->_priceEngineService->isEnabled()) {
            $promoCode = $this->_priceEngineService->getPromoCode();
            if ($promoCode) {
                $promotion = $this->getPromotion($promoCode);
                if ($promotion && $promotion->getId()) {
                    $this->loadPcTerms($promotion, $this->_priceEngineService->getProfessionalCategory());
                    return $promotion;
                }
            }
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getPriceServiceEngine(): PriceEngine
    {
        return $this->_priceEngineService;
    }

    /**
     * @inheritDoc
     */
    public function filterByProductSku($promotionData, $sku)
    {
        if (!empty($promotionData['pc_terms_condition'])) {
            foreach ($promotionData['pc_terms_condition'] as $pcTermsCondition) {
                if (strtolower($pcTermsCondition['sku']) == strtolower($sku)) {
                    return $pcTermsCondition;
                }
            }
        }

        return false;
    }

    /**
     * @param $promoCode
     * @return PromotionModel
     */
    private function getPromotion($promoCode): PromotionModel
    {
        if (! isset($this->promotions[$promoCode])) {
            $this->promotions[$promoCode] = $this->_dataHelper->getPromotionByPromoCode($promoCode);
        }

        return $this->promotions[$promoCode];
    }

    /**
     * @param PromotionModel $promotion
     * @param null $profCat
     */
    private function loadPcTerms(PromotionModel $promotion, $profCat = null)
    {
        $profCategory = $profCat ?? $promotion->getProfessionalCategory();
        $campaignCode = $promotion->getCampaignCode();
        $promoCode = $promotion->getPromoCode();
        if (!isset($this->promotionsTerms[$campaignCode][$promoCode][$profCategory])) {
            $collection = $this->_pcTermsCollectionFactory->create();
            $collection->addFieldToFilter('campaign_code', $campaignCode);
            $collection->addFieldToFilter('promo_code', $promoCode);
            $collection->addFieldToFilter('professional_category', $profCategory);
            $collection->load();

            $pcTermsCondition = [];
            if (count($collection) > 0) {
                $pcTermsCondition = $collection->getData();
            }

            $this->promotionsTerms[$campaignCode][$promoCode][$profCategory] = $pcTermsCondition;
        }

        $promotion->setPcTermsCondition($this->promotionsTerms[$campaignCode][$promoCode][$profCategory]);
    }
}
