<?php

namespace MMS\Promotions\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use MMS\Logger\LoggerInterface;
use MMS\Promotions\Model\Api\PostCampaign;
use MMS\Promotions\Model\Api\PostPromotion;
use MMS\Promotions\Model\Api\GetCampaign;

/**
 * Class PromotionSaveAfter
 * @package MMS\Promotions\Observer
 */
class PromotionSaveAfter implements ObserverInterface
{
    /**
     * @var PostCampaign
     */
    private $postCampaign;

    /**
     * @var PostPromotion
     */
    private $postPromotion;

    /**
     * @var getCampaign
     */
    private $getCampaign;
    /**
     * PromotionSaveAfter constructor.
     * @param PostCampaign $postCampaign
     * @param PostPromotion $postPromotion
     */
    public function __construct(
        PostCampaign $postCampaign,
        PostPromotion $postPromotion,
        GetCampaign $getCampaign,
        LoggerInterface $logger,
        ManagerInterface $messageManager
    )
    {
        $this->postCampaign  = $postCampaign;
        $this->postPromotion = $postPromotion;
        $this->getCampaign   = $getCampaign;
        $this->_logger       = $logger;
        $this->messageManager = $messageManager;
    }
    /**

     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_logger->debug(" ****************** Promotion Save after started*******************");
        /* @var MMS\Promotions\Model\Promotion $promotion */
        $promotion = $observer->getEvent()->getData('promotion');
        $isEdit = $observer->getEvent()->getData('is_edit');
        $campaignResponse = '';
        if (!$promotion->getData('is_synced')) {
            if ($promotion->getData('campaign_code')) {
                $isCampaignExist = $this->getCampaign->execute($promotion->getData('campaign_code'));
                if ($isCampaignExist != 1) {
                    $campaignResponse = $this->postCampaign->execute($promotion->getData('campaign_code'));
                }
            }
            if ((isset($campaignResponse['status']) && $campaignResponse['status'] == 201) || $isCampaignExist == 1) {
                $promotionResponse = $this->postPromotion->execute($promotion);
                if ($promotionResponse == 201 && $promotionResponse == 400) {
                    $promotion->setData('is_synced', 1);
                    $promotion->save();
                }
                if ($promotionResponse == 400 && !$isEdit) {
                    $message = $promotion->getPromoCode().' is already exist in ACS.';
                    $this->messageManager->addWarningMessage($message);
                }
            } else {
                $statusCode = ($campaignResponse['status'])? $campaignResponse['status'] : $isCampaignExist;
                $message = 'POST Campaign API Error with errorcode: '.$statusCode;
                if (isset($campaignResponse['message'])) {
                    $message = $campaignResponse['message'];
                }
                throw new \Magento\Framework\Exception\LocalizedException(__($message));
            }

        }
        $this->_logger->debug(" ****************** Promotion Save after ended*******************");
        $this->_logger->debug(json_encode($promotion->getData(),JSON_PRETTY_PRINT));
        return $this;
    }
}
