<?php

namespace MMS\Promotions\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use MMS\Promotions\Api\PromotionServiceInterface;

/**
 * Class PromoTermsText
 * @package MMS\Promotions\Block\Widget
 */
class PromoTermsText extends Template implements BlockInterface
{
    /**
     * @var string
     */
    protected $_template = "widget/promo_terms_text.phtml";

    /**
     * @var PromotionServiceInterface
     */
    protected $_promotionService;

    /**
     * PromoTermsText constructor.
     * @param Template\Context $context
     * @param PromotionServiceInterface $promotionService
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        PromotionServiceInterface $promotionService,
        array $data = []
    ) {
        $this->_promotionService = $promotionService;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    protected function getDefaultTermText(): string
    {
        return $this->getData('default_term');
    }

    /**
     * @return string
     */
    protected function getSku(): string
    {
        return $this->getData('sku');
    }

    /**
     * @return string|null
     */
    protected function getTermTextFromPromotion(): ?string
    {
        if ($this->getSku() && $promotion = $this->_promotionService->execute()) {
            $promotionData = $promotion->getData();
            if (isset($promotionData['pc_terms_condition']) && !empty($promotionData['pc_terms_condition'])) {
                $isUsedPromoTerm = $this->_promotionService->filterByProductSku($promotionData, $this->getSku());
                if ($isUsedPromoTerm && isset($isUsedPromoTerm['term']) && $isUsedPromoTerm['term'] != '') {
                    $term = $isUsedPromoTerm['term'];
                    $termsTextArr = explode(" ", $isUsedPromoTerm['term']);
                    if (count($termsTextArr) > 0) {
                        $term = $termsTextArr[0];
                    }

                    return $term;
                }
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function getPromoTermText(): string
    {
        $defaultTermText = $this->getDefaultTermText();
        $promoTermText = $this->getTermTextFromPromotion();

        $html = '<span class="default-term-text">' . $defaultTermText . '</span>';
        if ($promoTermText != null && $promoTermText != ''
            && strtolower(trim($defaultTermText)) !== strtolower(trim($promoTermText))) {
            $html = '<span class="default-term-text strikeThroughStyle">' . $defaultTermText . '</span> ';
            $html .= '<span class="promo-term-text">' . $promoTermText . '</span>';
        }

        return $html;
    }
}
