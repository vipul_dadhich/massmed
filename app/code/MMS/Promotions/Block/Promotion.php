<?php

namespace MMS\Promotions\Block;

class Promotion extends \Magento\Framework\View\Element\Template
{
    /**
     * Url builder
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Renew checkout helper
     *
     * @var \MMS\Promotions\Helper\Config
     */
    protected $_config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * Initialize dependencies.
     *
     * @param \MMS\Renew\Helper\Config $config
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Catalog\Model\Product $product
     */

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \MMS\Promotions\Helper\Data $config,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_config = $config;
        $this->_checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Magento\Checkout\Model\DefaultConfigProvider $subject
     * @param array $result
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getPromotion($param)
    {
        if ($this->_config->isModuleEnabled()) {
            $promoCode = $this->getPromoCode();
            if ($promoCode) {
                $promotion = $this->_config->getPromotionByPromoCode($promoCode);
                if ($promotion && $promotion->getData($param)) {
                    return $this->getPromotionAnchor($promotion->getData($param));
                }
                
            }
        }
        return false;
    }

    public function getPromoCode(){
        return $this->getCheckoutSession()->getQuote()->getData('ucc_promocode');
    }
    public function getPromotionAnchor($anchorValue){
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'promotions'.$anchorValue;
    }
    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}