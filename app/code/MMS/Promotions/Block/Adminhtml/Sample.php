<?php
namespace MMS\Promotions\Block\Adminhtml;
/**
 * Class Sample
 * @package MMS\Promotions\Block\Adminhtml
 */
class Sample extends \Magento\Backend\Block\Template {
    /**
     * Block template
     *
     * @var string
     */
    protected $_template = 'sample.phtml';
    /**
     * @var
     */
    protected $urlBuider;

    /**
     * Sample constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Model\UrlInterface $urlBuilder
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\UrlInterface $urlBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->urlBuilder = $urlBuilder;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @return string
     */
    public function getSampleUrl()
    {
        if ($this->getFileUrl()) {
            return $this->getFileUrl();
        }
        return '#';
    }

    /**
     * @return mixed
     */
    public function getFileUrl(){
        return $this->_scopeConfig->
        getValue('promotions/api/smaple_file_url',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
