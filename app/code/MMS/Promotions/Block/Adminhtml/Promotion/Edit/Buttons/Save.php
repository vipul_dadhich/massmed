<?php
namespace MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons;

use MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons\Generic;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Ui\Component\Control\Container;
use MMS\Promotions\Api\PromotionRepositoryInterface;
use Magento\Backend\Block\Widget\Context;

/**
 * Class Save
 * @package MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons
 */
class Save extends Generic implements ButtonProviderInterface
{
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        PromotionRepositoryInterface $promotionRepository,
        Context $context
    ) {
        parent::__construct($context, $promotionRepository);
        $this->request = $request;
    }
    /**
     * get button data
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Save'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => [
                    'buttonAdapter' => [
                        'actions' => [
                            [
                                'targetName' => 'mms_promotions_promotion_form.mms_promotions_promotion_form',
                                'actionName' => 'save',
                                'params' => [
                                    true,
                                    [
                                        'back' => 'continue'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'class_name' => Container::SPLIT_BUTTON,
            'options' => $this->getOptions(),
        ];
    }

    /**
     * Retrieve options
     *
     * @return array
     */
    private function getOptions()
    {
        $promoData = $this->request->getParam('promoData');
        $promoDataArray = [];
        if ($promoData) {
            $promoDataArray = (array)json_decode(base64_decode($promoData));
        }
        if (!empty($promoDataArray)) {
            $options = [
            [
                'id_hard' => 'save_and_close',
                'label' => __('Save & Close'),
                'data_attribute' => [
                    'mage-init' => [
                        'buttonAdapter' => [
                            'actions' => [
                                [
                                    'targetName' => 'mms_promotions_promotion_form.mms_promotions_promotion_form',
                                    'actionName' => 'save',
                                    'params' => [
                                        true,
                                        [
                                            'back' => 'close'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        } else {
                $options = [
                [
                    'label' => __('Save & Duplicate'),
                    'id_hard' => 'save_and_duplicate',
                    'data_attribute' => [
                        'mage-init' => [
                            'buttonAdapter' => [
                                'actions' => [
                                    [
                                        'targetName' => 'mms_promotions_promotion_form.mms_promotions_promotion_form',
                                        'actionName' => 'save',
                                        'params' => [
                                            true,
                                            [
                                                'back' => 'duplicate'
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'id_hard' => 'save_and_close',
                    'label' => __('Save & Close'),
                    'data_attribute' => [
                        'mage-init' => [
                            'buttonAdapter' => [
                                'actions' => [
                                    [
                                        'targetName' => 'mms_promotions_promotion_form.mms_promotions_promotion_form',
                                        'actionName' => 'save',
                                        'params' => [
                                            true,
                                            [
                                                'back' => 'close'
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }
        return $options;
    }
}
