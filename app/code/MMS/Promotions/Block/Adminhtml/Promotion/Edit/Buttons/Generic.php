<?php
namespace MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons;

use MMS\Promotions\Api\PromotionRepositoryInterface;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Generic
 * @package MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons
 */
class Generic
{
    /**
     * Widget Context
     *
     * @var Context
     */
    protected $context;

    /**
     * Promotion Repository
     *
     * @var PromotionRepositoryInterface
     */
    protected $promotionRepository;

    /**
     * constructor
     *
     * @param Context $context
     * @param PromotionRepositoryInterface $promotionRepository
     */
    public function __construct(
        Context $context,
        PromotionRepositoryInterface $promotionRepository
    ) {
        $this->context             = $context;
        $this->promotionRepository = $promotionRepository;
    }

    /**
     * Return Promotion ID
     *
     * @return int|null
     */
    public function getPromotionId()
    {
        try {
            return $this->promotionRepository->getById(
                $this->context->getRequest()->getParam('promotion_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }
    /**
     * Return Promotion
     *
     * @return int|null
     */
    public function getPromotionById()
    {
        try {
            return $this->promotionRepository->getById(
                $this->context->getRequest()->getParam('promotion_id')
            );
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
