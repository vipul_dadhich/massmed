<?php
namespace MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons\Pcterm;

use MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons\Generic;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Ui\Component\Control\Container;

/**
 * Class Upload
 * @package MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons
 */
class Upload extends Generic implements ButtonProviderInterface
{
    /**
     * get button data
     *
     * @return array
     */
    public function getButtonData()
    {
        $url = $this->getUrl('promotions/promition/pctermProcess');
        return [
            'label' => __('Upload'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
            'action' => $url
        ];
    }

}
