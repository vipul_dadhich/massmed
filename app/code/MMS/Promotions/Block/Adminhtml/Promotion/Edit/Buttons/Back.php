<?php
namespace MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons;

use MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons\Generic;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class Back
 * @package MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons
 */
class Back extends Generic implements ButtonProviderInterface
{
    /**
     * get button data
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
            'class' => 'back',
            'sort_order' => 10
        ];
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/');
    }
}
