<?php
namespace MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons;

use MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons\Generic;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
/**
 * Class Preview
 * @package MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons
 */
class Preview extends Generic implements ButtonProviderInterface
{
    /**
     * get button data
     *
     * @return array
     */
    public function getButtonData()
    {
        if($this->getPromotionId()){
            $promotion = $this->getPromotionById($this->getPromotionId());
            if($promotion->getBindedTemplate()){
                return [
                    'label' => __('Preview Template'),
                    'class' => 'preview',
                    'on_click' => 'window.open(\'' . $this->getPreviewUrl($promotion) . '\',\'_blank\')',
                    'sort_order' => 30,
                    'target' => '_blank',
                ];
            }
        }
    }

    /**
     * @return string
     */
    public function getPreviewUrl($promotion)
    {
        return $this->context->getUrlBuilder()->getBaseUrl().$promotion->getBindedTemplate().'?promo='.$promotion->getPromoCode();
    }
}
