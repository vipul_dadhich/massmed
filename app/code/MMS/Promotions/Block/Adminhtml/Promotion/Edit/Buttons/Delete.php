<?php
namespace MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons;

use MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons\Generic;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class Delete
 * @package MMS\Promotions\Block\Adminhtml\Promotion\Edit\Buttons
 */
class Delete extends Generic implements ButtonProviderInterface
{
    /**
     * get button data
     *
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        if ($this->getPromotionId()) {
            $data = [
                'label' => __('Delete Promotion'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to do this?'
                ) . '\', \'' . $this->getDeleteUrl() . '\')',
                'sort_order' => 20,
            ];
        }
        return $data;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/delete', ['promotion_id' => $this->getPromotionId()]);
    }
}
