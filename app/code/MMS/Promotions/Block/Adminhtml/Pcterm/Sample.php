<?php
namespace MMS\Promotions\Block\Adminhtml\Pcterm;
/**
 * Class Sample
 * @package MMS\Promotions\Block\Adminhtml\Pcterm
 */
class Sample extends \Magento\Backend\Block\Template {
    /**
     * Block template
     *
     * @var string
     */
    protected $_template = 'pctermsample.phtml';

}
