<?php

namespace MMS\Promotions\Helper;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 * @package MMS\Promotions\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * Url builder
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \MMS\Promotions\Model\Promotion $promotionFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param CheckoutSession $checkoutSession
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     */
	public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \MMS\Promotions\Model\Promotion $promotionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        CheckoutSession $checkoutSession,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->promotionFactory  = $promotionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->checkoutSession = $checkoutSession;
        $this->authSession = $authSession;
        $this->timezone = $timezone;
        $this->storeManager = $storeManager;
        return parent::__construct($context);
    }

    /**
     * @param $promocode
     * @return \Magento\Framework\DataObject
     */
    public function getPromotionByPromoCode($promocode){
        $currentDate = date("Y-m-d h:i:s");

        return $this->promotionFactory->getCollection()
        ->addFieldToFilter("promo_code",$promocode)
        ->addFieldToFilter("live_date",array("lteq" => $currentDate))
        ->addFieldToFilter("end_date",array("gteq" => $currentDate))
        ->getFirstItem();
    }

    /**
     * @param $promocode
     * @return \Magento\Framework\DataObject
     */
    public function checkPromoExist($promocode){

        return $this->promotionFactory->getCollection()
        ->addFieldToFilter("promo_code",$promocode)
        ->getFirstItem();
    }

    /**
     * @return mixed
     */
    public function isModuleEnabled()
    {
        return $this->scopeConfig->getValue('promotions/general/enable', ScopeInterface::SCOPE_STORE);
    }


    /**
     * @return array|false|string[]
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAllowedPaymentMethodsForPromotion(){
        $paymentMethods = '';
        $promoCode = $this->checkoutSession->getQuote()->getData('ucc_promocode');
        if ($promoCode) {
            $promotion = $this->getPromotionByPromoCode($promoCode);
            if ($promotion && $promotion->getAllowedPayments()) {
                $paymentMethods = $promotion->getAllowedPayments();
            }
        }
        return $paymentMethods ? explode(",", $paymentMethods) : [];
    }

    /**
     * @param $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */

    public function restrictViaPromo($observer)
    {
        if ($this->isModuleEnabled()) {
            $result = $observer->getEvent()->getResult();
            $paymentMethod = $observer->getEvent()->getMethodInstance()->getCode();
            $allowedpaymentMethods = $this->getAllowedPaymentMethodsForPromotion();
            if(!empty($allowedpaymentMethods)){
                if (!(in_array($paymentMethod, $allowedpaymentMethods))) {
                    $result->setData('is_available', false);
                }
            }

        }
    }


    /**
     * @return mixed
     */
    public function getCampaignPostApiUrl($campaignCode)
    {
        return str_replace('{campaignCode}', $campaignCode, $this->scopeConfig->getValue('promotions/api/campaign_api_url', ScopeInterface::SCOPE_STORE));
    }

    /**
     * @return mixed
     */
    public function getCampaignGetApiUrl($campaignCode)
    {
        return str_replace('{campaignName}', $campaignCode, $this->scopeConfig->getValue('promotions/api/campaign_get_api_url', ScopeInterface::SCOPE_STORE));
    }

    /**
     * @return mixed
     */
    public function getTermTranslationApiUrl($term)
    {
        return $this->scopeConfig->getValue('promotions/api/term_api_url', ScopeInterface::SCOPE_STORE).'?term='.$term;
    }

    /**
     * @return mixed
     */
    public function getPromotionPostApiUrl()
    {
        return $this->scopeConfig->getValue('promotions/api/promotion_api_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getPricingPostApiUrl()
    {
        return $this->scopeConfig->getValue('promotions/api/pricing_api_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getMuleSoftPromoURI()
    {
        return $this->scopeConfig->getValue('promotions/api/promo_base_api_url', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getMuleSoftPromoClientId()
    {
        return $this->scopeConfig->getValue('promotions/api/promo_client_id', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getMuleSoftPromoClientSecret()
    {
        return $this->scopeConfig->getValue('promotions/api/promo_client_secret', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $promotion
     * @return array
     */
    public function getApiPostParams($campaignCode){
        return array(
             "name" => $campaignCode,
              "initiator" => $this->authSession->getUser()->getUsername(),
              "currencyCode" => "USD"
        );
    }

    /**
     * @param $promotion
     * @return array
     */
    public function getPromotionApiPostParams($promotion){
        $payload = array(
            "promotionCode" => $promotion->getData('promo_code'),
            "campaignCode" => $promotion->getData('campaign_code'),
            "description" => $promotion->getData('promo_description'),
            "initiator" => $this->authSession->getUser()->getUsername(),
            "startDate" => $this->timezone->date(new \DateTime($promotion->getData('live_date')))->format('Y-m-d\TH:i:s\Z'),
            "endDate" => $this->timezone->date(new \DateTime($promotion->getData('end_date')))->format('Y-m-d\TH:i:s\Z')
        );
        if ($promotion->getData('premium_set_code')) {
            $payload['premiumSetCode'] = $promotion->getData('premium_set_code');
        }
        return $payload;
    }

    /**
     * @param $order
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPromotionAnchorForConfirmation($order){
        if ($this->isModuleEnabled()) {
            if ($order->getData('ucc_promocode')) {
                $promotion = $this->getPromotionByPromoCode($order->getData('ucc_promocode'));
                if ($promotion && $promotion->getConfirmationanchorone()) {
                    return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'promotions'.$promotion->getConfirmationanchorone();
                }
            }
        }
        return '';
    }

    /**
     * @param $promoId
     * @return bool
     */
    public function checkIsEdit($promoId){
        $promotion = $this->promotionFactory->load($promoId);
        if ($promotion && $promotion->getPromoCode() && $promotion->getCampaignCode()) {
            return true;
        }
        return false;
    }
}

