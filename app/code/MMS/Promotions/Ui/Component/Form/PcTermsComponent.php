<?php
namespace MMS\Promotions\Ui\Component\Form;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\ComponentVisibilityInterface;
use Magento\Ui\Component\Form\Fieldset;

/**
 * Class PcTermsComponent
 * @package MMS\Promotions\Ui\Component\Form
 */
class PcTermsComponent extends Fieldset
{
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * CustomFieldset constructor.
     * @param ContextInterface $context
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        RequestInterface $request,
        array $components = [],
        array $data = []
    ) {
        $this->request = $request;
        $this->context = $context;

        parent::__construct($context, $components, $data);
    }

    public function prepare()
    {
        $visiable = true;
        $config = $this->getData('config');
        if ($this->request->getParam('promoData')) {
            $visiable = false;
        }
        $config['visible'] = $visiable;
        $this->setData('config', $config);
        parent::prepare();
    }
}
