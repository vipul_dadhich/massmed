<?php
namespace MMS\Promotions\Ui\Component\Form\Element;

/**
 * Class Enddate
 * @package MMS\Promotions\Ui\Component\Form\Element
 */
class Enddate extends \Magento\Ui\Component\Form\Element\Input
{
    /**
     * Prepare component configuration
     *
     * @return void
     */
    public function prepare()
    {
        parent::prepare();
        $config = $this->getData('config');
        if(isset($config['dataScope']) && $config['dataScope']=='end_date'){
            $config['default']= date('Y/m/d', strtotime('+5 years'));
            $this->setData('config', (array)$config);
        }
    }
}
