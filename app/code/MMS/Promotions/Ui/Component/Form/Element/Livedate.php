<?php

namespace MMS\Promotions\Ui\Component\Form\Element;

/**
 * Class Livedate
 * @package MMS\Promotions\Ui\Component\Form\Element
 */
class Livedate extends \Magento\Ui\Component\Form\Element\Input
{
    /**
     * Prepare component configuration
     * @return void
     */
    public function prepare()
    {
        parent::prepare();

        $config = $this->getData('config');
        if(isset($config['dataScope']) && $config['dataScope']=='live_date'){
            //$config['default']= date('Y/m/d', strtotime('+3 years'));
            $config['default']= date('Y/m/d');
            $this->setData('config', (array)$config);
        }
    }
}
