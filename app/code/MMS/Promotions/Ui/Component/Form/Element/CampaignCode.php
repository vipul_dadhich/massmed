<?php
namespace MMS\Promotions\Ui\Component\Form\Element;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

/**
 * Class CampaignCode
 * @package MMS\Promotions\Ui\Component\Form\Element
 */
class CampaignCode extends \Magento\Ui\Component\Form\Element\Input
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * CampaignCode constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param ContextInterface $context
     * @param \MMS\Promotions\Helper\Data $helper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        ContextInterface $context,
        \MMS\Promotions\Helper\Data $helper,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $components, $data);
        $this->request = $request;
        $this->helper = $helper;
    }

    /**
     * Prepare component configuration
     *
     * @return void
     */
    public function prepare()
    {
        parent::prepare();
        $config = $this->getData('config');
        $promoData = $this->request->getParam('promoData');
        if ($promoData) {
            $promoData = (array)json_decode(base64_decode($promoData));
        }
        if (isset($promoData['campaignCode'])) {
            if(isset($config['dataScope']) && $config['dataScope']=='campaign_code'){
                $config['default']= $promoData['campaignCode'];
                $config['disabled']= true;
                $this->setData('config', (array)$config);

            }
        } else {
            if($this->request->getParam('promotion_id')){
            	$isEdit = $this->helper->checkIsEdit($this->request->getParam('promotion_id'));
    	        if($isEdit && isset($config['dataScope']) && $config['dataScope']=='campaign_code'){
    	            $config['disabled']= true;
    	            $this->setData('config', (array)$config);
    	        }
            }
        }
    }
}
