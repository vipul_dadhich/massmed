<?php

namespace MMS\Promotions\Ui\Component\Form\Element;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

/**
 * Class VanityData
 * @package MMS\Promotions\Ui\Component\Form\Element
 */
class VanityData extends \Magento\Ui\Component\Form\Element\Input
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * VanityData constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param ContextInterface $context
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        ContextInterface $context,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $components, $data);
        $this->request = $request;
    }

    /**
     * Prepare component configuration
     * @return void
     */
    public function prepare()
    {
        parent::prepare();
        $config = $this->getData('config');
        if(isset($config['dataScope']) && $config['dataScope']=='vanity_data'){
            $promoData = $this->request->getParam('promoData');
            if ($promoData) {
                $promoData = (array)json_decode(base64_decode($promoData));
            }
            if (isset($promoData['vanityUrl'])) {
                $config['default']= json_encode($promoData['vanityUrl']);
                $this->setData('config', (array)$config);
            } else {
                $config['default']= '';
                $this->setData('config', (array)$config);
            }
        }
    }
}
