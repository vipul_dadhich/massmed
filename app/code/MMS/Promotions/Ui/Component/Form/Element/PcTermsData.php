<?php

namespace MMS\Promotions\Ui\Component\Form\Element;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Form\Element\Input;

/**
 * Class PcTermsData
 * @package MMS\Promotions\Ui\Component\Form\Element
 */
class PcTermsData extends Input
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * VanityData constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param ContextInterface $context
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        ContextInterface $context,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $components, $data);
        $this->request = $request;
    }

    /**
     * Prepare component configuration
     * @return void
     */
    public function prepare()
    {
        parent::prepare();
        $config = $this->getData('config');
        if (isset($config['dataScope']) && $config['dataScope']=='pc_term_data') {
            $promoData = $this->request->getParam('promoData');
            if ($promoData) {
                $promoData = (array)json_decode(base64_decode($promoData));
            }
            if (isset($promoData['pcTermsData'])) {
                $config['default']= json_encode($promoData['pcTermsData']);
                $this->setData('config', (array)$config);
            } else {
                $config['default']= '';
                $this->setData('config', (array)$config);
            }
        }
    }
}
