define([
    'jquery',
    'underscore',
    'uiComponent',
    'knockout'
], function ($, _, Component, ko) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'MMS_Promotions/view/promotion-aside'
        },
        isVisible: ko.observable(false),

        initialize: function() {
            this._super();
        },

        isActive: function () {
            if (this.getPromotionAnchor() !== undefined) {
                this.isVisible(true);
                return true;
            }
            return false;
            
        },
        getPromotionAnchor: function() {
            return window.checkoutConfig.promotionAnchorTwo;
        }
    });
});
