define([
    'jquery',
    'underscore',
    'uiComponent',
    'knockout'
], function ($, _, Component, ko) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'MMS_Promotions/view/promotion'
        },
        isVisible: ko.observable(false),

        initialize: function() {
            this._super();
        },

        isActive: function () {
            if (window.checkoutConfig.promotionAnchorOne !== undefined) {
                this.isVisible(true);
                return true;
            }
            return false;
            
        },
        getPromotionAnchor: function() {
            if (window.checkoutConfig.promotionAnchorOne !== undefined) {
                this.isVisible(true);
                return window.checkoutConfig.promotionAnchorOne;
            }
            return false;
        }
    });
});
