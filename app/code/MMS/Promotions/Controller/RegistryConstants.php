<?php
namespace MMS\Promotions\Controller;

/**
 * Class RegistryConstants
 * @package MMS\Promotions\Controller
 */
class RegistryConstants
{
    /**
     * Registry key where current Promotion ID is stored
     *
     * @var string
     */
    const CURRENT_PROMOTION_ID = 'current_promotion_id';
}
