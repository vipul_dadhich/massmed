<?php
namespace MMS\Promotions\Controller\Adminhtml\Promotion;

use MMS\Promotions\Controller\Adminhtml\Promotion as PromotionController;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Delete
 * @package MMS\Promotions\Controller\Adminhtml\Promotion
 */
class Delete extends PromotionController
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('promotion_id');
        if ($id) {
            try {
                $this->promotionRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The Promotion has been deleted.'));
                $resultRedirect->setPath('mms_promotions/*/');
                return $resultRedirect;
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The Promotion no longer exists.'));
                return $resultRedirect->setPath('mms_promotions/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('mms_promotions/promotion/edit', ['promotion_id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem deleting the Promotion'));
                return $resultRedirect->setPath('mms_promotions/promotion/edit', ['promotion_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a Promotion to delete.'));
        $resultRedirect->setPath('mms_promotions/*/');
        return $resultRedirect;
    }
}
