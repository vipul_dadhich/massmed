<?php
namespace MMS\Promotions\Controller\Adminhtml\Promotion;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use MMS\Logger\LoggerInterface;
use MMS\Promotions\Api\Data\PcTermsInterface;
use MMS\Promotions\Api\PcTermsRepositoryInterface;
use MMS\Promotions\Model\ResourceModel\PcTerms as PcTermsResourceModel;
use MMS\Promotions\Model\Uploader;
use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use MMS\Promotions\Library\XLSReader;
use MMS\Promotions\Library\XLSXReader;
use MMS\Promotions\Api\Data\PcTermsInterfaceFactory;
use MMS\Promotions\Model\Api\GetTermTranslation;


/**
 * Class PctermProcess
 * @package MMS\Promotions\Controller\Adminhtml\Promotion
 */
class PctermProcess extends Action
{
    protected $pcData = [];
    /**
     * Uploader pool
     *
     * @var UploaderPool
     */
    protected $uploaderPool;
    /**
     * @var Uploader
     */
    protected $uploader;
    /**
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var Csv
     */
    protected $csvProcessor;
    /**
     * @var File
     */
    protected $fileDriver;

    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @var PcTermsResourceModel
     */
    protected $pcTermsResourceModel;
    /**
     * @var PcTermsInterfaceFactory
     */
    protected $pcTermsFactory;
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * @var PcTermsRepositoryInterface
     */
    protected $pcTermsRepository;
    /**
     * @var GetTermTranslation
     */
    protected $getTermTranslation;

    /**
     * PctermProcess constructor.
     * @param Context $context
     * @param Uploader $uploader
     * @param Filesystem $filesystem
     * @param Csv $csvProcessor
     * @param File $fileDriver
     * @param LoggerInterface $logger
     * @param PcTermsInterfaceFactory $pcTermsFactory
     * @param PcTermsResourceModel $pcTermsResourceModel
     * @param DataObjectHelper $dataObjectHelper
     * @param PcTermsRepositoryInterface $pcTermsRepository
     * @param GetTermTranslation $getTermTranslation
     */
    public function __construct(
        Context $context,
        Uploader $uploader,
        Filesystem $filesystem,
        Csv $csvProcessor,
        File $fileDriver,
        LoggerInterface $logger,
        PcTermsInterfaceFactory $pcTermsFactory,
        PcTermsResourceModel $pcTermsResourceModel,
        DataObjectHelper $dataObjectHelper,
        PcTermsRepositoryInterface $pcTermsRepository,
        GetTermTranslation $getTermTranslation
    ) {
        $this->uploader          = $uploader;
        $this->_filesystem       = $filesystem;
        $this->csvProcessor      = $csvProcessor;
        $this->fileDriver        = $fileDriver;
        $this->_logger           = $logger;
        $this->pcTermsResourceModel = $pcTermsResourceModel;
        $this->pcTermsFactory    = $pcTermsFactory;
        $this->dataObjectHelper    = $dataObjectHelper;
        $this->pcTermsRepository = $pcTermsRepository;
        $this->getTermTranslation = $getTermTranslation;
        parent::__construct($context);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $this->_logger->debug(" ****************** Promotion upload file started *******************");
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $createdData = [];
            $data['pctermfile'] = $this->uploader->uploadFileAndGetName('pctermfile', $data);
            if (isset($data['pctermfile'])) {
                $filePath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath()
                     . 'promotions' . $data['pctermfile'];
                if ($this->fileDriver->isExists($filePath)) {
                    $createdData = $this->ProcessFile($filePath);
                }
            }
            if (!empty($createdData)) {
                $resultRedirect->setPath('mms_promotions/promotion/index');
            } else {
                $resultRedirect->setPath('mms_promotions/promotion/pcterm');
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $resultRedirect->setPath('mms_promotions/promotion/pcterm');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem uploading the PC and Term'.$e->getMessage()));
            $resultRedirect->setPath('mms_promotions/promotion/pcterm');
        }
        $this->_logger->debug(" ****************** PC and Term upload file ended *******************");
        return $resultRedirect;
    }
    public function getDataFromFile($filePath){
        $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        if ($ext == 'csv') {
            return $this->csvProcessor->getData($filePath);
        } else if($ext == 'xls') {
            $fileObj = XLSReader::parse($filePath, false, true);
            return $fileObj->rows();
        } else {
            $fileObj = XLSXReader::parse($filePath, false, true);
            return $fileObj->rows();
        }
    }
    /**
     * @param $filepath
     * @return array|false
     * @throws \Exception
     */

    public function ProcessFile($filepath){
        $rawData = $this->getDataFromFile($filepath);
        if (!$this->validateFileData($rawData)) {
            $message = 'File format does not match template. A sample template can be downloaded from the link on this form.';
            $this->messageManager->addErrorMessage(__($message));
            return false;
        }
        $pcTermData = array();
        $processedData = [];
        foreach ($rawData as $rowIndex => $dataRow)
        {
            if($rowIndex > 0)
            {
                $pcTermData[] = $dataRow;
            }
        }
        if (!empty($pcTermData)) {
            $processedData = $this->savePcTermsData($pcTermData);
        }
        return $processedData;
    }

    public function savePcTermsData($pcTermData){
        $count = 1;
        if (count($pcTermData) > 0) {
            foreach($pcTermData as $pcterm){
                $pcData = [];
                if (isset($pcterm[0]) && isset($pcterm[1]) && isset($pcterm[2]) && isset($pcterm[3]) && isset($pcterm[4])) {
                    if ($pcterm[0] && $pcterm[1] && $pcterm[2] && $pcterm[3] && $pcterm[4]) {
                        $pcData['promo_code'] = $pcterm[1];
                        $pcData['campaign_code'] = $pcterm[0];
                        $pcData['professional_category'] = $pcterm[2];
                        $termResponse = $this->getTermTranslation->execute($pcterm[3], $pcterm[4]);
                        $pcData['term'] = $termResponse['term'];
                        $pcData['sku'] = $termResponse['magentoSku'];
                        if ($this->pcTermsResourceModel->checkIsPcTerms($pcData['promo_code'], $pcData['professional_category'], $pcData['term'], $pcData['sku'])) {
                            $message = 'Professional Category ' . $pcData['professional_category'] . ' already linked to ' . $pcData['term'] . ' term.';
                            $this->messageManager->addWarningMessage(__($message));
                            continue;
                        }
                        $pcTerms = $this->pcTermsFactory->create();
                    }
                 }
                $this->dataObjectHelper->populateWithArray($pcTerms, $pcData, PcTermsInterface::class);
                $this->pcTermsRepository->save($pcTerms);
                $message = 'Row no. '.$count++.' has been saved.';
                $this->messageManager->addSuccessMessage(__($message));
                }
            }
        return $this;
    }

    /**
     * @param $rawData
     * @return bool
     */
    public function validateFileData($rawData){
        $expectedRow = array(
            'Campaign Code',
            'Promo Code',
            'Professional Category',
            'Term',
            'Product Code'
        );
        if (isset($rawData[0]) && $expectedRow == $rawData[0]) {
            return true;
        }
        return false;
    }
    /**
     * @param string $type
     * @return \MMS\Promotions\Model\Uploader
     * @throws \Exception
     */
    protected function getUploader($type)
    {
        return $this->uploaderPool->getUploader($type);
    }
}
