<?php
namespace MMS\Promotions\Controller\Adminhtml\Promotion;

use MMS\Promotions\Controller\Adminhtml\Promotion as PromotionController;
use MMS\Promotions\Controller\RegistryConstants;

/**
 * Class Edit
 * @package MMS\Promotions\Controller\Adminhtml\Promotion
 */
class Edit extends PromotionController
{
    /**
     * Initialize current Promotion and set it in the registry.
     *
     * @return int
     */
    protected function initPromotion()
    {
        $promotionId = $this->getRequest()->getParam('promotion_id');
        $this->coreRegistry->register(RegistryConstants::CURRENT_PROMOTION_ID, $promotionId);

        return $promotionId;
    }

    /**
     * Edit or create Promotion
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $promotionId = $this->initPromotion();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('MMS_Promotions::promotions_promotion');
        $resultPage->getConfig()->getTitle()->prepend(__('Promotions'));
        $resultPage->addBreadcrumb(__('Promotions'), __('Promotions'));
        $resultPage->addBreadcrumb(__('Promotions'), __('Promotions'), $this->getUrl('mms_promotions/promotion'));

        if ($promotionId === null) {
            $resultPage->addBreadcrumb(__('New Promotion'), __('New Promotion'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Promotion'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Promotion'), __('Edit Promotion'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->promotionRepository->getById($promotionId)->getCampaign_code()
            );
        }
        return $resultPage;
    }
}
