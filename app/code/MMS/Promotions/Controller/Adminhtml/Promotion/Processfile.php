<?php
namespace MMS\Promotions\Controller\Adminhtml\Promotion;

use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\StoreManagerInterface;
use MMS\Logger\LoggerInterface;
use MMS\Promotions\Api\Data\PcTermsInterface;
use MMS\Promotions\Api\Data\PromotionInterface;
use MMS\Promotions\Api\PcTermsRepositoryInterface;
use MMS\Promotions\Api\Data\PcTermsInterfaceFactory;
use MMS\Promotions\Api\PromotionRepositoryInterface;
use MMS\Promotions\Model\Api\GetTermTranslation;
use MMS\Promotions\Model\Promotion;
use MMS\Promotions\Api\Data\PromotionInterfaceFactory;
use MMS\Promotions\Model\Uploader;
use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use MMS\Promotions\Model\Api\PostCampaign;
use MMS\Promotions\Model\Api\GetCampaign;
use MMS\Promotions\Model\Api\PostPromotionBulk;
use MMS\Promotions\Model\Api\PostPricing;
use Magento\Framework\App\Filesystem\DirectoryList;
use MMS\Promotions\Library\XLSReader;
use MMS\Promotions\Library\XLSXReader;
use MMS\Promotions\Model\ResourceModel\PcTerms as PcTermsResourceModel;


/**
 * Class Processfile
 * @package MMS\Promotions\Controller\Adminhtml\Promotion
 */
class Processfile extends Action
{
    protected $pcData = [];
    /**
     * Uploader pool
     *
     * @var UploaderPool
     */
    protected $uploaderPool;
    /**
     * @var Uploader
     */
    protected $uploader;
    /**
     * @var Filesystem
     */
    protected $_filesystem;
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var Csv
     */
    protected $csvProcessor;
    /**
     * @var PostCampaign
     */
    protected $postCampaign;
    /**
     * @var GetCampaign
     */
    protected $getCampaign;
    /**
     * @var PostPromotionBulk
     */
    protected $postPromotion;
    /**
     * @var PostPricing
     */
    protected $postPricing;
    /**
     * @var File
     */
    protected $fileDriver;
    /**
     * @var Session
     */
    protected $authSession;
    /**
     * @var TimezoneInterface
     */
    protected $timezone;
    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @var Promotion
     */
    protected $promotionModel;
    /**
     * @var PromotionInterfaceFactory
     */
    protected $promotionFactory;
    /**
     * @var PromotionRepositoryInterface
     */
    protected $promotionRepository;
    /**
     * @var PcTermsInterfaceFactory
     */
    protected $pcTermsFactory;
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * @var PcTermsRepositoryInterface
     */
    protected $pcTermsRepository;
    /**
     * @var GetTermTranslation
     */
    protected $getTermTranslation;
    /**
     * @var PcTermsResourceModel
     */
    protected $pcTermsResourceModel;

    /**
     * @param Context $context
     * @param Uploader $uploader
     * @param StoreManagerInterface $storeManager
     * @param Filesystem $filesystem
     * @param Csv $csvProcessor
     * @param PostCampaign $postCampaign
     * @param GetCampaign $getCampaign
     * @param PostPromotionBulk $postPromotion
     * @param PostPricing $postPricing
     * @param File $fileDriver
     * @param TimezoneInterface $timezone
     * @param Session $authSession
     * @param LoggerInterface $logger
     * @param Promotion $promotionModel
     * @param PromotionRepositoryInterface $promotionRepository
     * @param PromotionInterfaceFactory $promotionFactory
     * @param PcTermsInterfaceFactory $pcTermsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param PcTermsRepositoryInterface $pcTermsRepository
     * @param GetTermTranslation $getTermTranslation
     * @param PcTermsResourceModel $pcTermsResourceModel
     */
    public function __construct(
        Context $context,
        Uploader $uploader,
        StoreManagerInterface $storeManager,
        Filesystem $filesystem,
        Csv $csvProcessor,
        PostCampaign $postCampaign,
        GetCampaign $getCampaign,
        PostPromotionBulk $postPromotion,
        PostPricing $postPricing,
        File $fileDriver,
        TimezoneInterface $timezone,
        Session $authSession,
        LoggerInterface $logger,
        Promotion $promotionModel,
        PromotionRepositoryInterface $promotionRepository,
        PromotionInterfaceFactory $promotionFactory,
        PcTermsInterfaceFactory $pcTermsFactory,
        DataObjectHelper $dataObjectHelper,
        PcTermsRepositoryInterface $pcTermsRepository,
        GetTermTranslation $getTermTranslation,
        PcTermsResourceModel $pcTermsResourceModel
    ) {
        $this->uploader          = $uploader;
        $this->_filesystem       = $filesystem;
        $this->_storeManager     = $storeManager;
        $this->csvProcessor      = $csvProcessor;
        $this->postCampaign      = $postCampaign;
        $this->getCampaign       = $getCampaign;
        $this->postPromotion     = $postPromotion;
        $this->postPricing       = $postPricing;
        $this->fileDriver        = $fileDriver;
        $this->authSession       = $authSession;
        $this->timezone          = $timezone;
        $this->_logger           = $logger;
        $this->promotionModel    = $promotionModel;
        $this->promotionFactory  = $promotionFactory;
        $this->promotionRepository  = $promotionRepository;
        $this->pcTermsFactory    = $pcTermsFactory;
        $this->dataObjectHelper    = $dataObjectHelper;
        $this->pcTermsRepository = $pcTermsRepository;
        $this->getTermTranslation = $getTermTranslation;
        $this->pcTermsResourceModel = $pcTermsResourceModel;
        parent::__construct($context);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $this->_logger->debug(" ****************** Promotion upload file started *******************");
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $createdData = [];
            $data['priceuploadfile'] = $this->uploader->uploadFileAndGetName('priceuploadfile', $data);
            if (isset($data['priceuploadfile'])) {
                $filePath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath()
                     . 'promotions' . $data['priceuploadfile'];
                if ($this->fileDriver->isExists($filePath)) {
                    $createdData = $this->ProcessFile($filePath);
                }
            }
            if (!empty($createdData)) {
                $resultRedirect->setPath('mms_promotions/promotion/new', ['promoData' => base64_encode(json_encode($createdData))]);
            } else {
                $resultRedirect->setPath('mms_promotions/promotion/pricefile');
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $resultRedirect->setPath('mms_promotions/promotion/pricefile');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem uploading the Promotion'.$e->getMessage()));
            $resultRedirect->setPath('mms_promotions/promotion/pricefile');
        }
        $this->_logger->debug(" ****************** Promotion upload file ended *******************");
        return $resultRedirect;
    }
    public function getDataFromFile($filePath){
        $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        if ($ext == 'csv') {
            return $this->csvProcessor->getData($filePath);
        } else if($ext == 'xls') {
            $fileObj = XLSReader::parse($filePath, false, true);
            return $fileObj->rows();
        } else {
            $fileObj = XLSXReader::parse($filePath, false, true);
            return $fileObj->rows();
        }
    }
    /**
     * @param $filepath
     * @return array|false
     * @throws \Exception
     */

    public function ProcessFile($filepath){
        $rawData = $this->getDataFromFile($filepath);
        if (!$this->validateFileData($rawData)) {
            $message = 'File format does not match template. A sample template can be downloaded from the link on this form.';
            $this->messageManager->addErrorMessage(__($message));
            return false;
        }
        $count = 0;
        $campaignData = array();
        foreach ($rawData as $rowIndex => $dataRow)
        {
            if($rowIndex > 0)
            {
                $campaignCode = $dataRow[0];
                unset($dataRow[0]);
                if(!empty($dataRow) && isset($dataRow[1]) && trim($dataRow[1]) != ''){
                    $campaignData[$campaignCode][] = $dataRow;
                }

            }
        }
        if (!empty($campaignData)) {
            $processedData = $this->processCampaigns($campaignData);
        }
        $processedPromoCount = 0;
        $createdData = [];
        $promocodes = [];
        $processedCampaign = array();
        $processedPromos = array();
        $pcTermDataForNo = array();
        $otherCount = 0;
        foreach ($processedData as $campaignCode => $promoData) {
            if (!in_array($campaignCode, $processedCampaign)) {
                if (isset($promoData['response_status'])) {
                    if ($promoData['response_status'] == 1) {
                        $createdData['campaignCode'] = $campaignCode;
                        $message = 'Campaign already exist with '.$campaignCode;
                        $this->messageManager->addWarningMessage(__($message));
                    } else if ($promoData['response_status'] == 201) {
                        $createdData['campaignCode'] = $campaignCode;
                        $message = 'Campaign created with '.$campaignCode;
                        $this->messageManager->addSuccessMessage(__($message));
                    } else {
                        if (isset($promoData['response_campaign_message'])) {
                            $message = $promoData['response_campaign_message'];
                        } else {
                            $message = 'Campaign API failed with status code '.$promoData['response_status'].' for '.$campaignCode;
                        }

                        $this->messageManager->addErrorMessage(__($message));
                    }
                }
                $processedCampaign[] = $campaignCode;
            }
            foreach ($promoData as $promotion) {
                if (isset($promotion['response_status'])) {
                    if (!in_array($promotion[1], $processedPromos)) {
                        if ($promotion['response_status'] == 201 || $promotion['response_status'] == 400) {
                            if (isset($promotion[12]) && $promotion[12] == 'Y') {
                                $promocodes[] = $promotion[1];
                                $processedPromoCount ++;
                                if (isset($promotion[6]) && $promotion[6] != '' && isset($promotion[7]) && $promotion[7] != '') {
                                    if (isset($this->pcData[$promotion[1]])) {
                                        $createdData['pcTermsData'][$promotion[1]] = $this->pcData[$promotion[1]];
                                    }
                                }
                            } else if (isset($promotion[12]) && ($promotion[12] == 'N' || $promotion[12] == '')) {
                                $this->createPromoInMagento($promotion, $campaignCode);
                                if (isset($promotion[6]) && $promotion[6] != '' && isset($promotion[7]) && $promotion[7] != '') {
                                    if (isset($this->pcData[$promotion[1]])) {
                                        $pcTermDataForNo['pcTermsData'][$promotion[1]] = $this->pcData[$promotion[1]];
                                    }
                                }
                                $otherCount++;
                            }
                            $startDate = date('m/d/Y', strtotime($promotion[3]));
                            $endDate = date('m/d/Y', strtotime($promotion[4]));
                            $createdData['description'] = $promotion[2];
                            $createdData['startDate'] = $startDate;
                            $createdData['endDate'] = $endDate;
                            if (isset($promotion[13]) && $promotion[13] != '') {
                                $createdData['vanityUrl'][$promotion[1]] = $promotion[13];
                            }

                            $message = 'Promotion created successfully in ACS with '.$promotion[1];
                            $this->messageManager->addSuccessMessage(__($message));
                        } else {
                            if (isset($promotion['response_message'])) {
                                $message = $promotion['response_message'];
                            } else {
                                $message = 'Promotion API failed with status code '.$promotion['response_status'].' for '.$promotion[1];
                            }
                            if ($promotion['response_status'] == 400) {
                                $this->messageManager->addWarningMessage(__($message));
                            } else {
                                $this->messageManager->addErrorMessage(__($message));
                            }

                        }
                    }
                    $processedPromos[] = $promotion[1];
                    if ($promotion['pricing_response_status'] == 200) {
                        $message = 'Pricing created successfully in ACS with '.$promotion[1];
                        $this->messageManager->addSuccessMessage(__($message));
                    } else {
                        //$message = 'Pricing API failed for '.$promotion[1].' with '.$promotion['pricing_response_message'];
                        if (isset($promotion['pricing_response_message'])) {
                            $message = $promotion['pricing_response_message']. ' for '.$promotion[1];
                        } else {
                            $message = 'Pricing API failed with status code '.$promotion['pricing_response_status'].' for '.$promotion[1];
                        }
                        $this->messageManager->addErrorMessage(__($message));
                    }
                }
            }
        }
        if (!empty($pcTermDataForNo)) {
            $this->createPcTermRelationShip($pcTermDataForNo);
        }
        if ($otherCount > 0) {
            $message = $otherCount . ' promo has been saved with Customize Online Experience as NO';
            $this->messageManager->addSuccessMessage(__($message));
        }
        if ($processedPromoCount > 0) {
            $createdData['promoCode'] = implode(',', $promocodes);
            return $createdData;
        } else {
            $message = 'There is not any promo code with "Customize Online Experience"';
            $this->messageManager->addWarningMessage(__($message));
            return false;
        }
    }

    /**
     * @param $pcTermDataForNo
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createPcTermRelationShip($pcTermDataForNo) {
            if (!empty($pcTermDataForNo)) {
                $pcTermData = $pcTermDataForNo['pcTermsData'];
                foreach($pcTermData as $pCode => $pcterms) {
                    foreach ($pcterms as $pcterm) {
                        if ($pcterm['pc'] != '' && $pcterm['term'] != '') {
                            $pcData['promo_code'] = $pCode;
                            $pcData['campaign_code'] = $pcterm['c_code'];
                            $pcData['professional_category'] = $pcterm['pc'];
                            $responseObj = $this->getTermTranslation->execute($pcterm['term'], $pcterm['pcode']);
                            $pcData['term'] = $responseObj['term'];
                            $magentoSKu = $responseObj['magentoSku'];
                            if ($this->pcTermsResourceModel->checkIsPcTerms($pcData['promo_code'], $pcData['professional_category'], $pcData['term'], $magentoSKu)) {
                                $message = 'Professional Category ' . $pcData['professional_category'] . ' already linked to ' . $pcData['term'] . ' term.';
                                if ($magentoSKu) {
                                    $message .= ' with sku "'.$magentoSKu.'"';
                                }
                                $this->messageManager->addWarningMessage(__($message));
                                continue;
                            }
                            $pcTerms = $this->pcTermsFactory->create();
                            $pcTerms->setSku($magentoSKu);
                            $this->dataObjectHelper->populateWithArray($pcTerms, $pcData, PcTermsInterface::class);
                            $this->pcTermsRepository->save($pcTerms);
                        }
                    }
                }
            }
        return $this;
    }

    /**
     * @param $promotion
     * @param $campaignCode
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createPromoInMagento($promotion, $campaignCode) {
        $promoCode = !empty($promotion[1]) ? $promotion[1] : null;
         $promotionQuery = $this->promotionModel->getCollection()
                ->addFieldToFilter("promo_code", $promoCode)
                ->getFirstItem();
        $promotionModel = $this->promotionFactory->create();
        if ($promotionQuery->getId()) {
            $promotionModel = $promotionQuery;
        }
        $liveDate = date('Y-m-d', strtotime($promotion[3]));
        $endDate = date('Y-m-d', strtotime($promotion[4]));
        $data = ['promo_code' => $promoCode,'campaign_code' => $campaignCode,
            'promo_description' => $promotion[2], 'professional_category' => $promotion[7],
            'live_date' => $liveDate,
            'end_date' => $endDate, 'premium_set_code' => $promotion[14]
            ];

        $this->dataObjectHelper->populateWithArray($promotionModel, $data, PromotionInterface::class);
        $this->promotionRepository->save($promotionModel);

    }
    /**
     * @param $campaignData
     * @return mixed
     */
    public function processCampaigns($campaignData){
        foreach($campaignData as $campaignCode => $campaign){
            $campaignResponse = 0;
            try {
            $isCampaignExist = $this->getCampaign->execute($campaignCode);
            $campaignApiStatus = $isCampaignExist;
            $campaignApiMessage = '';

            if ($isCampaignExist != 1) {
                $campaignResponse = $this->postCampaign->execute($campaignCode);
                $campaignApiStatus = $campaignResponse['status'];
                $campaignApiMessage = $campaignResponse['message'];
            }
            } catch(\Exception $e) {
                $this->_logger->debug($e->getMessage());
                $campaignApiStatus = 500;
                $campaignApiMessage = $e->getMessage();
            }
            if ($campaignApiStatus == 1 || $campaignApiStatus == 201) {
                foreach($campaign as $key => $promoData){
                    $pricingResponse = '';
                    $promoResponse = $this->processPromoCodes($campaignCode, $promoData);
                    if ($promoResponse['status'] == 201 || $promoResponse['status'] == 400) {
                        $pricingResponse = $this->processPricingData($campaignCode, $promoData);
                        if (isset($promoData[6]) && $promoData[6] != '' && isset($promoData[7]) && $promoData[7] != '') {
                            $this->pcData[$promoData[1]][] = ['c_code' => $campaignCode, 'term' => $promoData[6], 'pc' => $promoData[7], 'pcode' => $promoData[5]];
                        }
                    }
                    $promoData['response_status'] = $promoResponse['status'];
                    $promoData['response_message'] = $promoResponse['message'];
                    $promoData['pricing_response_status'] = $pricingResponse['status'];
                    $promoData['pricing_response_message'] = $pricingResponse['message'];
                    $campaign[$key] = $promoData;
                }
                $campaign['response_status'] = $campaignApiStatus;
                $campaign['response_campaign_message'] = $campaignApiMessage;
            } else {
                $campaign['response_status'] = $campaignApiStatus;
                $campaign['response_campaign_message'] = $campaignApiMessage;
            }
            $campaignData[$campaignCode] = $campaign;
        }
        return $campaignData;
    }

    /**
     * @param $campaignCode
     * @param $campaign
     * @return int
     */
    public function processPricingData($campaignCode, $campaign){
        try {
            if (!empty($campaign)) {
                $startDate = date('m/d/Y', strtotime($campaign[3]));
                $endDate = date('m/d/Y', strtotime($campaign[4]));
                $payload = array(
                    "campaign" => $campaignCode,
                    "promo" => $campaign[1],
                    "promoDescription" => $campaign[2],
                    "effectiveDate" => $startDate,
                    "endDate" => $endDate,
                    "productCode" => $campaign[5],
                    "term" => strval($campaign[6]),
                    "currency" => $campaign[8],
                    "domestic" => $campaign[10],
                    "professionalCategory" => $campaign[7],
                    "autoRenew" => $campaign[11],
                    "price" => $campaign[9],
                    "quantity" => 0,
                );
                return $this->postPricing->execute($payload);
            }
        } catch(\Exception $e) {
            $this->_logger->debug($e->getMessage());
            return 500;
        }

    }

    /**
     * @param $campaignCode
     * @param $campaign
     * @return int
     */
    public function processPromoCodes($campaignCode, $campaign){
        try {
            if (!empty($campaign)) {
                $startDate = date('Y-m-d\TH:i:s\Z', strtotime($campaign[3]));
                $endDate = date('Y-m-d\TH:i:s\Z', strtotime($campaign[4]));
                $premiumSetCode = (isset($campaign[14])) ? $campaign[14] : '';
                $payload = array(
                    "promotionCode" => $campaign[1],
                    "campaignCode" => $campaignCode,
                    "description" => $campaign[2],
                    "initiator" => $this->authSession->getUser()->getUsername(),
                    "startDate" => $startDate,
                    "endDate" => $endDate
                );
                if ($premiumSetCode) {
                    $payload['premiumSetCode'] = $premiumSetCode;
                }
                return $this->postPromotion->execute($payload);
            }
        } catch(\Exception $e) {
            $this->_logger->debug($e->getMessage());
            return 500;
        }

    }

    /**
     * @param $rawData
     * @return bool
     */
    public function validateFileData($rawData){
        $expectedRow = array(
            'Campaign Code',
            'Promo Code',
            'Promo Description',
            'Effective Date',
            'End Date',
            'Product Code',
            'Term',
            'Professional Category',
            'Currency',
            'Price',
            'Domestic',
            'Require Auto Renew',
            'Customize Online Experience',
            'Vanity Url',
            'Premium set Code'
        );
        if (isset($rawData[0]) && $expectedRow == $rawData[0]) {
            return true;
        }
        return false;
    }
    /**
     * @param string $type
     * @return \MMS\Promotions\Model\Uploader
     * @throws \Exception
     */
    protected function getUploader($type)
    {
        return $this->uploaderPool->getUploader($type);
    }
}
