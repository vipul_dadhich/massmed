<?php
namespace MMS\Promotions\Controller\Adminhtml\Promotion;

use MMS\Promotions\Api\Data\PromotionInterface;
use MMS\Promotions\Api\Data\PcTermsInterface;
use MMS\Promotions\Api\Data\PromotionInterfaceFactory;
use MMS\Promotions\Api\Data\PcTermsInterfaceFactory;
use MMS\Promotions\Api\PromotionRepositoryInterface;
use MMS\Promotions\Api\PcTermsRepositoryInterface;
use MMS\Promotions\Controller\Adminhtml\Promotion as PromotionController;
use MMS\Promotions\Model\Promotion;
use MMS\Promotions\Model\Uploader;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\Filter\Date as DateFilter;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use MMS\Promotions\Model\ResourceModel\PcTerms as PcTermsResourceModel;
use MMS\Promotions\Model\Api\GetTermTranslation;

/**
 * Class Save
 * @package MMS\Promotions\Controller\Adminhtml\Promotion
 */
class Save extends PromotionController
{
    /**
     * Promotion factory
     *
     * @var PromotionInterfaceFactory
     */
    protected $promotionFactory;

    /**
     * Data Object Processor
     *
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * Data Object Helper
     *
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Uploader pool
     *
     * @var UploaderPool
     */
    protected $uploaderPool;

    /**
     * Data Persistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
    * @var EventManager
    */
    private $eventManager;

    /**
     * @var PcTermsInterfaceFactory
     */
    protected $pcTermsFactory;

    /**
     * @var PcTermsRepositoryInterface
     */
    protected $pcTermsRepository;

    /**
     * @var Promotion
     */
    protected $promotionModel;

    /**
     * @var PcTermsResourceModel
     */
    protected $pcTermsResourceModel;

    /**
     * @var Uploader
     */
    protected $uploader;
    /**
     * @var GetTermTranslation
     */
    protected $getTermTranslation;

    /**
     * Save constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PromotionRepositoryInterface $promotionRepository
     * @param PcTermsRepositoryInterface $pcTermsRepository
     * @param PageFactory $resultPageFactory
     * @param Promotion $promotionModel
     * @param DateFilter $dateFilter
     * @param PromotionInterfaceFactory $promotionFactory
     * @param PcTermsInterfaceFactory $pcTermsFactory
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param Uploader $uploader
     * @param DataPersistorInterface $dataPersistor
     * @param EventManager $eventManager
     * @param PcTermsResourceModel $pcTermsResourceModel
     * @param GetTermTranslation $getTermTranslation
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PromotionRepositoryInterface $promotionRepository,
        PcTermsRepositoryInterface $pcTermsRepository,
        PageFactory $resultPageFactory,
        Promotion $promotionModel,
        DateFilter $dateFilter,
        PromotionInterfaceFactory $promotionFactory,
        PcTermsInterfaceFactory $pcTermsFactory,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        Uploader $uploader,
        DataPersistorInterface $dataPersistor,
        EventManager $eventManager,
        PcTermsResourceModel $pcTermsResourceModel,
        GetTermTranslation $getTermTranslation
    ) {
        $this->promotionFactory    = $promotionFactory;
        $this->pcTermsFactory    = $pcTermsFactory;
        $this->pcTermsRepository = $pcTermsRepository;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper    = $dataObjectHelper;
        $this->promotionModel    = $promotionModel;
        $this->uploader        = $uploader;
        $this->dataPersistor       = $dataPersistor;
        $this->eventManager = $eventManager;
        $this->pcTermsResourceModel = $pcTermsResourceModel;
        $this->getTermTranslation = $getTermTranslation;
        parent::__construct($context, $coreRegistry, $promotionRepository, $resultPageFactory, $dateFilter);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \MMS\Promotions\Api\Data\PromotionInterface $promotion */
        $promotion = null;
        $postData = $this->getRequest()->getPostValue();
        $oldPromotionCode = '';
        $data = $postData;
        $resultRedirect = $this->resultRedirectFactory->create();
        if(isset($data['is_bulk_import']) && $data['is_bulk_import'] == 1){
            $promoCodes = explode(',', $data['promo_code']);
            foreach ($promoCodes as $promoCode) {
                $data['promo_code'] = $promoCode;
                $result = $this->executeSave($data);
                if (isset($result['status'])) {
                    $message = isset($result['message'])?$result['message']:'';
                    if ($result['status'] == 'success') {
                        $this->messageManager->addSuccessMessage(__($message));
                    } else {
                        $this->messageManager->addErrorMessage(__($message));
                    }
                }
            }
            $resultRedirect->setPath('mms_promotions/promotion');
            return $resultRedirect;
        }
        $isEdit = false;
        $data = $this->filterData($data);
        $id = !empty($data['promotion_id']) ? $data['promotion_id'] : null;

        try {
            if ($id) {
                $promotion = $this->promotionRepository->getById((int)$id);
                $oldPromotionCode = $promotion->getPromoCode();
                $isEdit = true;
            } else {
                unset($data['promotion_id']);
                $promotion = $this->promotionFactory->create();
            }
            $data['cartanchorone'] = $this->uploader->uploadFileAndGetName('cartanchorone', $data);
            $data['cartanchortwo'] = $this->uploader->uploadFileAndGetName('cartanchortwo', $data);
            $data['checkoutanchorone'] = $this->uploader->uploadFileAndGetName('checkoutanchorone', $data);
            $data['checkoutanchortwo'] = $this->uploader->uploadFileAndGetName('checkoutanchortwo', $data);
            $data['confirmationanchorone'] = $this->uploader->uploadFileAndGetName('confirmationanchorone', $data);
            $data['confirmationanchortwo'] = $this->uploader->uploadFileAndGetName('confirmationanchortwo', $data);
            $data['landinganchorone'] = $this->uploader->uploadFileAndGetName('landinganchorone', $data);
            $data['landinganchortwo'] = $this->uploader->uploadFileAndGetName('landinganchortwo', $data);
            $data['priceconfigfile'] = $this->uploader->uploadFileAndGetName('priceconfigfile', $data);
            $premiumSetCode = (isset($postData['premium_set_code'])) ? $postData['premium_set_code'] : '';
            $this->dataObjectHelper->populateWithArray($promotion, $data, PromotionInterface::class);
            $promotion->setPremiumSetCode($premiumSetCode);
            $this->promotionRepository->save($promotion);
            $this->savePcTermsData($data);


            $this->eventManager->dispatch('mms_promotion_save_after', ['promotion' => $promotion, 'is_edit' => $isEdit]);


            if ($this->getRequest()->getParam('back') == 'duplicate') {

                $newPromo = $this->duplicatePromo($promotion, $postData, $resultRedirect);
                $this->messageManager->addSuccessMessage(__('You saved & duplicated the Promotion'));
                $resultRedirect->setPath('mms_promotions/promotion/edit', ['promotion_id' => $newPromo->getId()]);
                return $resultRedirect;
            }
            $this->messageManager->addSuccessMessage(__('You saved the Promotion'));
            $this->dataPersistor->clear('mms_promotions_promotion');
            if ($this->getRequest()->getParam('back') == 'continue') {
                $resultRedirect->setPath('mms_promotions/promotion/edit', ['promotion_id' => $promotion->getId()]);
            } else {
                $resultRedirect->setPath('mms_promotions/promotion');
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            if($oldPromotionCode){
                $postData['promo_code'] = $oldPromotionCode;
            }
            $this->dataPersistor->set('mms_promotions_promotion', $postData);
            $resultRedirect->setPath('mms_promotions/promotion/edit', ['promotion_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the Promotion'.$e->getMessage()));
            $this->dataPersistor->set('mms_promotions_promotion', $postData);
            $resultRedirect->setPath('mms_promotions/promotion/edit', ['promotion_id' => $id]);
        }
        return $resultRedirect;
    }

    /**
     * @param $data
     * @return $this
     * @throws LocalizedException
     */
    public function savePcTermsData($data, $promotion = ''){
        if (isset($data['professional_category_and_terms'])) {
            $savedIds = [];
            foreach($data['professional_category_and_terms'] as $pcPost){
                $pcData = [];
                if (isset($pcPost['professional_category']) && $pcPost['professional_category'] != '' && isset($pcPost['term']) && $pcPost['term'] != ''){
                    $pcData['promo_code'] = $data['promo_code'];
                    $pcData['campaign_code'] = $data['campaign_code'];
                    $pcData['professional_category'] = $pcPost['professional_category'];
                    $term = $pcPost['term'];
                    $pcData['term'] = $term;
                    $magentoSKu = $pcPost['sku'];
                    if (isset($pcPost['entity_id']) && $pcPost['entity_id']) {
                        $pcTerms = $this->pcTermsRepository->getById((int)$pcPost['entity_id']);
                        if ($this->pcTermsResourceModel->checkIsPcTermsEditCheck($pcTerms->getEntityId(), $pcData['promo_code'], $pcData['professional_category'], $pcData['term'], $magentoSKu)) {
                            $message = 'Professional Category '.$pcData['professional_category'].' already linked to '.$pcData['term']. ' term.';
                            if ($magentoSKu) {
                                $message .= ' with sku "'.$magentoSKu.'"';
                            }
                            $this->messageManager->addWarningMessage(__($message));
                            continue;
                        }
                    } else {
                        unset($pcPost['entity_id']);
                        if ($this->pcTermsResourceModel->checkIsPcTerms($pcData['promo_code'], $pcData['professional_category'], $pcData['term'], $magentoSKu)) {
                            $message = 'Professional Category '.$pcData['professional_category'].' already linked to '.$pcData['term']. ' term.';
                            if ($magentoSKu) {
                                $message .= ' with sku "'.$magentoSKu.'"';
                            }
                            $this->messageManager->addWarningMessage(__($message));
                            continue;
                        }
                        $pcTerms = $this->pcTermsFactory->create();
                    }
                    $pcTerms->setSku($magentoSKu);
                    $this->dataObjectHelper->populateWithArray($pcTerms, $pcData, PcTermsInterface::class);
                    $pcTermObj = $this->pcTermsRepository->save($pcTerms);
                    $savedIds[] = $pcTermObj->getEntityId();
                }
            }
            $this->pcTermsResourceModel->deleteExisting($data['promo_code'], $savedIds);
        }
        if (isset($data['pc_term_data']) && $data['pc_term_data'] != '' && $promotion->getPromoCode()) {
            $pcTermData = (array)json_decode($data['pc_term_data']);
            if (!empty($pcTermData)) {
                foreach($pcTermData as $pCode => $pcterms) {
                    if ($promotion->getPromoCode() == $pCode) {
                        foreach ($pcterms as $pcterm) {
                            if ($pcterm->pc != '' && $pcterm->term != '') {
                                $pcData['promo_code'] = $pCode;
                                $pcData['campaign_code'] = $promotion->getCampaignCode();
                                $pcData['professional_category'] = $pcterm->pc;
                                $responseObj = $this->getTermTranslation->execute($pcterm->term, $pcterm->pcode);
                                $pcData['term'] = $responseObj['term'];
                                $magentoSKu = $responseObj['magentoSku'];
                                if ($this->pcTermsResourceModel->checkIsPcTerms($pcData['promo_code'], $pcData['professional_category'], $pcData['term'], $magentoSKu)) {
                                    $message = 'Professional Category ' . $pcData['professional_category'] . ' already linked to ' . $pcData['term'] . ' term';
                                    if ($magentoSKu) {
                                        $message .= ' with sku "'.$magentoSKu.'"';
                                    }
                                    $this->messageManager->addWarningMessage(__($message));
                                    continue;
                                }
                                $pcTerms = $this->pcTermsFactory->create();
                                $pcTerms->setSku($magentoSKu);
                                $this->dataObjectHelper->populateWithArray($pcTerms, $pcData, PcTermsInterface::class);
                                $this->pcTermsRepository->save($pcTerms);
                            }
                        }
                    }
                }
            }

        }
        return $this;
    }

    /**
     * @param $data
     * @return array
     */
    public function executeSave($data){
        $data = $this->filterData($data);
        $promoCode = !empty($data['promo_code']) ? $data['promo_code'] : null;

        try {
            $promotionQuery = $this->promotionModel->getCollection()
                ->addFieldToFilter("promo_code",$promoCode)
                ->getFirstItem();
            if ($promotionQuery->getId()) {
                $result['status'] = 'fail';
                $result['message'] = $data['promo_code']. ' already exists.';
                return $result;
            }
            $promotion = $this->promotionFactory->create();
            $data['cartanchorone'] = $this->uploader->uploadFileAndGetName('cartanchorone', $data);
            $data['cartanchortwo'] = $this->uploader->uploadFileAndGetName('cartanchortwo', $data);
            $data['checkoutanchorone'] = $this->uploader->uploadFileAndGetName('checkoutanchorone', $data);
            $data['checkoutanchortwo'] = $this->uploader->uploadFileAndGetName('checkoutanchortwo', $data);
            $data['confirmationanchorone'] = $this->uploader->uploadFileAndGetName('confirmationanchorone', $data);
            $data['confirmationanchortwo'] = $this->uploader->uploadFileAndGetName('confirmationanchortwo', $data);
            $data['landinganchorone'] = $this->uploader->uploadFileAndGetName('landinganchorone', $data);
            $data['landinganchortwo'] = $this->uploader->uploadFileAndGetName('landinganchortwo', $data);
            $data['priceconfigfile'] = $this->uploader->uploadFileAndGetName('priceconfigfile', $data);
            if (isset($data['promotion_id'])) {
                unset($data['promotion_id']);
            }
            $this->dataObjectHelper->populateWithArray($promotion, $data, PromotionInterface::class);
            $premiumSetCode = (isset($postData['premium_set_code'])) ? $postData['premium_set_code'] : '';
            $promotion->setPremiumSetCode($premiumSetCode);
            $this->promotionRepository->save($promotion);
            $this->savePcTermsData($data, $promotion);
            $result['status'] = 'success';
            $result['message'] = $data['promo_code']. ' was created successfully';
            return $result;
        } catch (LocalizedException $e) {
            $result['status'] = 'fail';
            $result['message'] = 'There was a problem saving the Promotion with '.$data['promo_code'].' promo code'.$e->getMessage();
            return $result;
        } catch (\Exception $e) {
            $result['status'] = 'fail';
            $result['message'] = 'There was a problem saving the Promotion with '.$data['promo_code'].' promo code'.$e->getMessage();
            return $result;
        }
    }
    /**
     * @param $promotion
     * @param $postData
     * @param $resultRedirect
     * @return mixed
     */
    public function duplicatePromo($promotion, $postData, $resultRedirect){
        if($promotion && $promotion->getPromoCode()){
            try {
                $newPromotion = clone $promotion;
                $newPromotion->setPromoCode('');
                $newPromotion->setPromotionId(null);
                $newPromotion->isObjectNew(true);
                $newPromotion->getResource()->save($newPromotion);
                return $newPromotion;
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem saving the Promotion'));
                $this->dataPersistor->set('mms_promotions_promotion', $postData);
                $resultRedirect->setPath('mms_promotions/promotion/edit', ['promotion_id' => $promotion->getPromotionId()]);
            }
        }
    }

    /**
     * @param string $type
     * @return \MMS\Promotions\Model\Uploader
     * @throws \Exception
     */
    protected function getUploader($type)
    {
        return $this->uploaderPool->getUploader($type);
    }
}
