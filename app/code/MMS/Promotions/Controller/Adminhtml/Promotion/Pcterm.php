<?php
namespace MMS\Promotions\Controller\Adminhtml\Promotion;

use MMS\Promotions\Controller\Adminhtml\Promotion as PromotionController;
use MMS\Promotions\Controller\RegistryConstants;
/**
 * Class Pcterm
 * @package MMS\Promotions\Controller\Adminhtml\Promotion
 */
class Pcterm extends PromotionController
{

    /**
     * create price file view
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('MMS_Promotions::promotions_pcterm');
        $resultPage->getConfig()->getTitle()->prepend(__('PC and Term File Upload'));
        $resultPage->addBreadcrumb(__('Promotions'), __('PC and Term File Upload'));
        $resultPage->addBreadcrumb(__('Promotions'), __('PC and Term File Upload'), $this->getUrl('mms_promotions/promotion/pcterm'));

            $resultPage->addBreadcrumb(__('PC and Term File Upload'), __('PC and Term File Upload'));
            $resultPage->getConfig()->getTitle()->prepend(__('PC and Term File Upload'));

        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('MMS_Promotions::promotions_pcterm');
    }
}
