<?php
namespace MMS\Promotions\Controller\Adminhtml\Promotion;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
/**
 * Class Sample
 * @package MMS\Promotions\Controller\Adminhtml\Promotion
 */
class Sample extends Action
{
    const SAMPLE_FILE_NAME = 'sample_promotion.csv';
    /**
     * @var Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_downloader;

    /**
     * @var Magento\Framework\Filesystem\DirectoryList
     */
    protected $_directory;

    /**
     * @param Context     $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem $directory
    ) {
        $this->_downloader =  $fileFactory;
        $this->directory = $directory->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function execute()
    {
        $filepath = 'export/'.self::SAMPLE_FILE_NAME;
        $this->directory->create('export');
        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();

        $fileheader = [];
        $filedata = [];
        $headerData = $this->getSampleCsvHeaderAndData();
        foreach($headerData as $header => $data){
            $fileheader[] = $header;
            $filedata[] = $data;
        }
        //echo '<pre>';print_r($fileheader); print_r($filedata);
        $stream->writeCsv($fileheader);
        $stream->writeCsv($filedata);
        $content = [];
        $content['type'] = 'filename'; // must keep filename
        $content['value'] = $filepath;
        $content['rm'] = '1'; //remove csv from var folder

        $csvfilename = self::SAMPLE_FILE_NAME;
        return $this->_downloader->create($csvfilename, $content, DirectoryList::VAR_DIR);
    }

    /**
     * @return string[]
     */
    public function getSampleCsvHeaderAndData(){
        return [
            'Campaign Code' => 'DNJD1909',
            'Promo Code' => 'DN9JPCQQ',
            'Promo Description' => 'PHY DM 52/$69AR or $89',
            'Effective Date' => '2019-09-02',
            'End Date' => '2020-10-31',
            'Product Code' => 'NEJ',
            'Term' => '52',
            'Professional Category' => 'PHY',
            'Currency' => 'USD',
            'Price' => '69',
            'Domestic' => 'Y',
            'Require Auto Renew' => 'Y',
            'Customize Online Experience' => 'N',
            'Vanity Url' => 'vanity-url',
        ];
    }
}
