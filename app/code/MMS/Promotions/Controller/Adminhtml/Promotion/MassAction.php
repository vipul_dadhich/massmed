<?php
namespace MMS\Promotions\Controller\Adminhtml\Promotion;

use MMS\Promotions\Api\Data\PromotionInterface;
use MMS\Promotions\Api\PromotionRepositoryInterface;
use MMS\Promotions\Model\ResourceModel\Promotion\CollectionFactory as PromotionCollectionFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class MassAction
 * @package MMS\Promotions\Controller\Adminhtml\Promotion
 */
abstract class MassAction extends Action
{
    /**
     * Promotion repository
     *
     * @var PromotionRepositoryInterface
     */
    protected $promotionRepository;

    /**
     * Mass Action filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * Promotion collection factory
     *
     * @var PromotionCollectionFactory
     */
    protected $collectionFactory;

    /**
     * Action success message
     *
     * @var string
     */
    protected $successMessage;

    /**
     * Action error message
     *
     * @var string
     */
    protected $errorMessage;

    /**
     * constructor
     *
     * @param Context $context
     * @param PromotionRepositoryInterface $promotionRepository
     * @param Filter $filter
     * @param PromotionCollectionFactory $collectionFactory
     * @param string $successMessage
     * @param string $errorMessage
     */
    public function __construct(
        Context $context,
        PromotionRepositoryInterface $promotionRepository,
        Filter $filter,
        PromotionCollectionFactory $collectionFactory,
        $successMessage,
        $errorMessage
    ) {
        $this->promotionRepository = $promotionRepository;
        $this->filter              = $filter;
        $this->collectionFactory   = $collectionFactory;
        $this->successMessage      = $successMessage;
        $this->errorMessage        = $errorMessage;
        parent::__construct($context);
    }

    /**
     * @param \MMS\Promotions\Api\Data\PromotionInterface $promotion
     * @return mixed
     */
    abstract protected function massAction(PromotionInterface $promotion);

    /**
     * execute action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $promotion) {
                $this->massAction($promotion);
            }
            $this->messageManager->addSuccessMessage(__($this->successMessage, $collectionSize));
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, $this->errorMessage);
        }
        $redirectResult = $this->resultRedirectFactory->create();
        $redirectResult->setPath('mms_promotions/*/index');
        return $redirectResult;
    }
}
