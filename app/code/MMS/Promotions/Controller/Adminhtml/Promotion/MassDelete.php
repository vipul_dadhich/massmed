<?php
namespace MMS\Promotions\Controller\Adminhtml\Promotion;

use MMS\Promotions\Api\Data\PromotionInterface;
use MMS\Promotions\Controller\Adminhtml\Promotion\MassAction;

/**
 * Class MassDelete
 * @package MMS\Promotions\Controller\Adminhtml\Promotion
 */
class MassDelete extends MassAction
{
    /**
     * @param \MMS\Promotions\Api\Data\PromotionInterface $promotion
     * @return $this
     */
    protected function massAction(PromotionInterface $promotion)
    {
        $this->promotionRepository->delete($promotion);
        return $this;
    }
}
