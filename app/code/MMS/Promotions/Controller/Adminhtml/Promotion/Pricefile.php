<?php
namespace MMS\Promotions\Controller\Adminhtml\Promotion;

use MMS\Promotions\Controller\Adminhtml\Promotion as PromotionController;
use MMS\Promotions\Controller\RegistryConstants;
/**
 * Class Pricefile
 * @package MMS\Promotions\Controller\Adminhtml\Promotion
 */
class Pricefile extends PromotionController
{
    
    /**
     * create price file view
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('MMS_Promotions::promotions_pricefile');
        $resultPage->getConfig()->getTitle()->prepend(__('Price File Upload'));
        $resultPage->addBreadcrumb(__('Promotions'), __('Price File Upload'));
        $resultPage->addBreadcrumb(__('Promotions'), __('Price File Upload'), $this->getUrl('mms_promotions/promotion/pricefile'));

            $resultPage->addBreadcrumb(__('Price File Upload'), __('Price File Upload'));
            $resultPage->getConfig()->getTitle()->prepend(__('Price File Upload'));

        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('MMS_Promotions::promotions_pricefile');
    }
}
