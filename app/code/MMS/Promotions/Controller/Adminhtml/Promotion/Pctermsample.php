<?php
namespace MMS\Promotions\Controller\Adminhtml\Promotion;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Filesystem;

/**
 * Class Pctermsample
 * @package MMS\Promotions\Controller\Adminhtml\Promotion
 */
class Pctermsample extends Action
{
    const SAMPLE_FILE_NAME = 'sample_pcterm.csv';
    /**
     * @var Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_downloader;

    /**
     * @var Magento\Framework\Filesystem\DirectoryList
     */
    protected $_directory;
    /**
     * @var Filesystem\Directory\WriteInterface
     */
    protected $directory;

    /**
     * Pctermsample constructor.
     * @param Context $context
     * @param FileFactory $fileFactory
     * @param Filesystem $directory
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        Filesystem $directory
    ) {
        $this->_downloader =  $fileFactory;
        $this->directory = $directory->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function execute()
    {
        $filepath = 'export/'.self::SAMPLE_FILE_NAME;
        $this->directory->create('export');
        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();

        $fileheader = [];
        $filedata = [];
        $headerData = $this->getSampleCsvHeaderAndData();
        foreach($headerData as $header => $data){
            $fileheader[] = $header;
            $filedata[] = $data;
        }
        $stream->writeCsv($fileheader);
        $stream->writeCsv($filedata);
        $content = [];
        $content['type'] = 'filename';
        $content['value'] = $filepath;
        $content['rm'] = '1';

        $csvfilename = self::SAMPLE_FILE_NAME;
        return $this->_downloader->create($csvfilename, $content, DirectoryList::VAR_DIR);
    }

    /**
     * @return string[]
     */
    public function getSampleCsvHeaderAndData(){
        return [
            'Campaign Code' => 'DNJD1909',
            'Promo Code' => 'DN9JPCQQ',
            'Professional Category' => 'PHY',
            'Term' => '104',
            'Product Code' => 'NEJ'
        ];
    }
}
