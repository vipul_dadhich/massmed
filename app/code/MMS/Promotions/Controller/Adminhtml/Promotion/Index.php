<?php
namespace MMS\Promotions\Controller\Adminhtml\Promotion;

use MMS\Promotions\Controller\Adminhtml\Promotion as PromotionController;

/**
 * Class Index
 * @package MMS\Promotions\Controller\Adminhtml\Promotion
 */
class Index extends PromotionController
{
    /**
     * Promotions list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('MMS_Promotions::promotion');
        $resultPage->getConfig()->getTitle()->prepend(__('Promotions'));
        $resultPage->addBreadcrumb(__('Promotions'), __('Promotions'));
        $resultPage->addBreadcrumb(__('Promotions'), __('Promotions'));
        return $resultPage;
    }
}
