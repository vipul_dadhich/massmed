<?php
namespace MMS\Promotions\Controller\Adminhtml;

use MMS\Promotions\Api\PromotionRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\Filter\Date as DateFilter;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Promotion
 * @package MMS\Promotions\Controller\Adminhtml
 */
abstract class Promotion extends Action
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Promotion repository
     *
     * @var PromotionRepositoryInterface
     */
    protected $promotionRepository;

    /**
     * Page factory
     *
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Date filter
     *
     * @var DateFilter
     */
    protected $dateFilter;

    /**
     * constructor
     *
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PromotionRepositoryInterface $promotionRepository
     * @param PageFactory $resultPageFactory
     * @param DateFilter $dateFilter
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PromotionRepositoryInterface $promotionRepository,
        PageFactory $resultPageFactory,
        DateFilter $dateFilter
    ) {
        $this->coreRegistry        = $coreRegistry;
        $this->promotionRepository = $promotionRepository;
        $this->resultPageFactory   = $resultPageFactory;
        $this->dateFilter          = $dateFilter;
        parent::__construct($context);
    }

    /**
     * filter values
     *
     * @param array $data
     * @return array
     */
    protected function filterData($data)
    {
        $inputFilter = new \Zend_Filter_Input(
            [
            ],
            [],
            $data
        );
        $data = $inputFilter->getUnescaped();
        if (isset($data['free_gift_eligible_skus'])) {
            if (is_array($data['free_gift_eligible_skus'])) {
                $data['free_gift_eligible_skus'] = implode(',', $data['free_gift_eligible_skus']);
            }
        }
        if (isset($data['allowed_payments'])) {
            if (is_array($data['allowed_payments'])) {
                $data['allowed_payments'] = implode(',', $data['allowed_payments']);
            }
        }
        return $data;
    }
}
