<?php

namespace MMS\Promotions\Plugin;

use Closure;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Pricing\Helper\Data as PricingDataHelper;
use MMS\PriceEngine\Service\PriceEngine;
use MMS\Promotions\Api\PromotionServiceInterface;
use MMS\Promotions\Model\Promotion;
use Nejm\Cmswidgets\Block\Widget\Price as CmswidgetsPriceWidget;

/**
 * Class PriceWidget
 * @package MMS\Promotions\Plugin
 */
class PriceWidget
{
    /**
     * @var PromotionServiceInterface
     */
    protected $_promotionService;

    /**
     * @var PriceEngine
     */
    protected $_priceEngineService;

    /**
     * @var PricingDataHelper
     */
    protected $_pricingHelper;

    /**
     * PriceWidget constructor.
     * @param PromotionServiceInterface $promotionService
     * @param PricingDataHelper $pricingHelper
     */
    public function __construct(
        PromotionServiceInterface $promotionService,
        PricingDataHelper $pricingHelper
    ) {
        $this->_promotionService = $promotionService;
        $this->_pricingHelper = $pricingHelper;

        $this->_priceEngineService = $this->_promotionService->getPriceServiceEngine() ?:
            ObjectManager::getInstance()->get(PriceEngine::class);
    }

    /**
     * @param CmswidgetsPriceWidget $subject
     * @param Closure $proceed
     * @param bool $format
     * @return mixed
     */
    public function aroundGetPriceData(
        CmswidgetsPriceWidget $subject,
        Closure $proceed,
        $format = true
    ) {
        /** @var Promotion $promotion */
        if ($promotion = $this->_promotionService->execute()) {
            $promotionData = $promotion->getData();
            if (isset($promotionData['pc_terms_condition']) && !empty($promotionData['pc_terms_condition'])) {
                $productData = $subject->getProduct();
                $isUsedPromoTerm = $this->_promotionService->filterByProductSku($promotionData, $productData->getSku());
                if ($isUsedPromoTerm) {
                    $payload = [
                        'sku' => $productData->getSku()
                    ];
                    $payload['term'] = $isUsedPromoTerm['term'];
                    $finalPrice = $this->callService($payload);
                    if (!$finalPrice) {
                        $finalPrice = $productData->getPrice();
                    }

                    if(is_numeric($finalPrice) && floor($finalPrice ) != $finalPrice){
                        $finalPrice = number_format($finalPrice, 2);
                    }

                    if ($format) {
                        return $this->_pricingHelper->currency($finalPrice, true, true);
                    } else {
                        return number_format($finalPrice, 2);
                    }
                }
            }
        }

        return $proceed($format);
    }

    /**
     * @param $payload
     * @return Float|int
     */
    protected function callService($payload)
    {
        $this->_priceEngineService->setRequest($payload);
        $this->_priceEngineService->setFullResponse(false);
        $this->_priceEngineService->buildPrice();
        return $this->_priceEngineService->getPrice();
    }
}
