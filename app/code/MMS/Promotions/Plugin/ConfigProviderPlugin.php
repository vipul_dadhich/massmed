<?php

namespace MMS\Promotions\Plugin;

class ConfigProviderPlugin
{
    /**
     * Url builder
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Renew checkout helper
     *
     * @var \MMS\Promotions\Helper\Config
     */
    protected $_config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    /**
     * Initialize dependencies.
     *
     * @param \MMS\Renew\Helper\Config $config
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Catalog\Model\Product $product
     */
    public function __construct(
        \MMS\Promotions\Helper\Data $config,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\Product $product
    ) {
        $this->_config = $config;
        $this->_checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->product = $product;
    }

    /**
     * @param \Magento\Checkout\Model\DefaultConfigProvider $subject
     * @param array $result
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, array $result)
    {
        if ($this->_config->isModuleEnabled()) {
            $promoCode = $this->getCheckoutSession()->getQuote()->getData('ucc_promocode');
            if ($promoCode) {
                $promotion = $this->_config->getPromotionByPromoCode($promoCode);
                if ($promotion && $promotion->getCheckoutanchorone()) {
                    $result['promotionAnchorOne'] = $this->getPromotionAnchor($promotion->getCheckoutanchorone());
                }
                if ($promotion && $promotion->getCheckoutanchortwo()) {
                    $result['promotionAnchorTwo'] = $this->getPromotionAnchor($promotion->getCheckoutanchortwo());
                }
                
            }
        }
        return $result;
    }

    public function getPromotionAnchor($anchorValue){
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'promotions'.$anchorValue;
    }
    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}