<?php

namespace MMS\Promotions\Plugin;
use Magento\Checkout\Model\Cart;
use MMS\Promotions\Helper\Data;


/**
 * Class ExtendRenewPaymentObserver
 * @package MMS\Promotions\Plugin
 */

class ExtendRenewPaymentObserver {

	/**
     * @var ConfigHelper
     */
    protected $_helper;
    /**
     * @var Cart
     */
    protected $_cart;

    /**
     * ExtendRenewPaymentObserver constructor.
     * @param Data $helper
     * @param Cart $cart
     */
    public function __construct(
        Data $helper,
        Cart $cart
	) {
        $this->_helper = $helper;
        $this->_cart = $cart;
    }


    /**
     * @param \Ey\Renew\Observer\PaymentMethodAvailable $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundExecute(
      \Ey\Renew\Observer\PaymentMethodAvailable $subject,
       \Closure $proceed,
       \Magento\Framework\Event\Observer $observer
   ){
        $proceed($observer);
        $this->_helper->restrictViaPromo($observer);
    }
}
