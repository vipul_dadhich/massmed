<?php

namespace MMS\Promotions\Plugin;

use Magento\Catalog\Model\Product\Option;
use MMS\Promotions\Api\PromotionServiceInterface;
use Nejm\Cmswidgets\Block\Widget\CallToAction;

/**
 * Class CallToActionWidget
 * @package MMS\Promotions\Plugin
 */
class CallToActionWidget
{
    /**
     * @var Option
     */
    protected $_optionFactory;

    /**
     * @var PromotionServiceInterface
     */
    protected $_promotionService;

    /**
     * CallToActionWidget constructor.
     * @param Option $optionFactory
     * @param PromotionServiceInterface $promotionService
     */
    public function __construct(
        Option $optionFactory,
        PromotionServiceInterface $promotionService
    ) {
        $this->_optionFactory = $optionFactory;
        $this->_promotionService = $promotionService;
    }

    /**
     * @param CallToAction $subject
     * @param \Closure $proceed
     * @param $product
     * @return mixed
     */
    public function aroundGetProductOptions(
        CallToAction $subject,
        \Closure $proceed,
        $product
    ) {
        if ($promotion = $this->_promotionService->execute()) {
            $promotionData = $promotion->getData();
            if (isset($promotionData['pc_terms_condition']) && !empty($promotionData['pc_terms_condition'])) {
                $isUsedPromoTerm = $this->_promotionService->filterByProductSku($promotionData, $product->getSku());
                if ($isUsedPromoTerm) {
                    $options = $this->_optionFactory->getProductOptionCollection($product) ?: [];
                    if (count($options) > 0) {
                        foreach ($options as $o) {
                            foreach ($o->getValues() as $value) {
                                if (strtolower($value['default_title']) === strtolower($isUsedPromoTerm['term'])) {
                                    return $value;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $proceed($product);
    }
}
