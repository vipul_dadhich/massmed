<?php

namespace MMS\Promotions\Plugin;

use MMS\PriceEngine\Service\PriceEngine;
use MMS\Promotions\Helper\Data as PromotionHelper;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;
use MMS\Customer\Model\Source\ProfessionalCategory;
use MMS\Logger\LoggerInterface;

/**
 * Class PriceEngineGetProfessionalCategory
 * @package MMS\Promotions\Plugin
 */
class PriceEngineGetProfessionalCategory
{

    /**
     * @var PromotionHelper
     */
    protected $_promotionHelper;

    /**
     * @var SessionCurrencyEngine
     */
    protected $_session;

    /**
     * @var ProfessionalCategory
     */
    protected $professionalCategorylist;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * PriceEngineGetProfessionalCategory constructor.
     * @param PromotionHelper $promotionHelper
     * @param SessionCurrencyEngine $session
     * @param ProfessionalCategory $professionalCategorylist
     * @param LoggerInterface $logger
     */
    public function __construct(
        PromotionHelper $promotionHelper,
        SessionCurrencyEngine $session,
        ProfessionalCategory $professionalCategorylist,
        LoggerInterface $logger
    ) {
        $this->_promotionHelper  = $promotionHelper;
        $this->_session = $session;
        $this->professionalCategorylist = $professionalCategorylist;
        $this->_logger = $logger;
    }

    /**
     * @param PriceEngine $subject
     * @param \Closure $proceed
     * @return mixed
     */
    public function aroundGetProfessionalCategory(
        PriceEngine $subject,
        \Closure $proceed
    ) {
        $isPromoApplied = false;
        $professionalCategory = false;
        $currentPromoProfessionalCat = null;
        if ($promoCode = $subject->getPromoCode()) {
            if ($professionalCategory = $this->getProfessionalCategoryFromPromotion($promoCode)) {
                $currentPromoProfessionalCat = $promoCode.'_'.$professionalCategory;
                $promoProfessionCat = $this->_session->getPromoProfessionalCategory();
                if ($currentPromoProfessionalCat != $promoProfessionCat) {
                    $isPromoApplied = true;
                }
            }
        }
        if ($subject->getCustomer() && $subject->getCustomer()->getData('professional_category')) {
            $customerProfessionalCat = $subject->getCustomer()->getData('professional_category');
            if ($isPromoApplied) {
                if ($customerProfessionalCat && $customerProfessionalCat != $professionalCategory) {
                    if ($this->checkIsPromoProfessionalCategoryAllowed($customerProfessionalCat, $professionalCategory)) {
                        $this->_session->addProfessionalCategory($professionalCategory);
                        $this->_session->addPromoProfessionalCategory($currentPromoProfessionalCat);
                        $this->_logger->debug("Returning From Promotions:-". $professionalCategory);
                        $this->_logger->debug("customerProfessionalCat:-". $customerProfessionalCat);
                        $this->_logger->debug("professionalCategory:-". $professionalCategory);
                        return $professionalCategory;
                    } else {
                        $this->_session->addProfessionalCategory($customerProfessionalCat);
                        $this->_logger->debug("Returning From Promotions Not Allowed:-". $customerProfessionalCat);
                        return $customerProfessionalCat;
                    }
                }
            }
        }
        if (!$subject->getCustomer() && !$subject->getCheckoutSession()->getCheckoutPriceUpdateFlag()) {
            if ($isPromoApplied) {
                $promoProfessionCat = $this->_session->getPromoProfessionalCategory();
                if ($promoProfessionCat != $currentPromoProfessionalCat) {
                    $this->_session->addProfessionalCategory($professionalCategory);
                    $this->_session->addPromoProfessionalCategory($currentPromoProfessionalCat);
                    $this->_logger->debug("Returning From Promotions:-". $professionalCategory);
                    $this->_logger->debug("promoProfessionCat:-". $promoProfessionCat);
                    $this->_logger->debug("currentPromoProfessionalCat:-". $currentPromoProfessionalCat);
                    return $professionalCategory;
                }
            }
        }
        return $proceed();
    }

    /**
     * @param $promocode
     * @return false
     */
    public function getProfessionalCategoryFromPromotion($promocode)
    {
        $promotionData = $this->_promotionHelper->getPromotionByPromoCode($promocode);
        if (!empty($promotionData) && $promotionData->getProfessionalCategory()) {
            return $promotionData->getProfessionalCategory();
        }
        return false ;
    }

    /**
     * @param $customerProfessionalCat
     * @param $professionalCategory
     * @return bool
     */
    public function checkIsPromoProfessionalCategoryAllowed($customerProfessionalCat, $professionalCategory): bool
    {
        $allowedCat = $this->getFilteredProfessionalCategory($customerProfessionalCat);
        if (!empty($allowedCat) && array_search($professionalCategory, array_column($allowedCat, 'value'))) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getFilteredProfessionalCategory($customerProfessionalCat): array
    {
        $profCatOptions = [];
        $professionalCategoryList = $this->professionalCategorylist->getAllOptions();
        if (count($professionalCategoryList) > 0) {
            foreach ($professionalCategoryList as $professionalCategory) {
                if (strlen($professionalCategory['value']) > 0) {
                    if ($this->applyPhysicianCatRule($professionalCategory, $customerProfessionalCat)) {
                        continue;
                    }

                    if ($this->applyOtherCatRule($professionalCategory, $customerProfessionalCat)) {
                        continue;
                    }

                    if ($this->applyResidentCatRule($professionalCategory, $customerProfessionalCat)) {
                        continue;
                    }

                    $profCatOptions[] = $professionalCategory;
                }
            }
        }
        return $profCatOptions;
    }

    /**
     * @param $professionalCategory
     * @return bool
     */
    private function applyPhysicianCatRule($professionalCategory, $customerProfessionalCat): bool
    {
        if ($customerProfessionalCat && $customerProfessionalCat == 'PHY'
            && ($professionalCategory['value'] == 'RES'
                || $professionalCategory['value'] == 'PA'
                || $professionalCategory['value'] == 'NUR'
                || $professionalCategory['value'] == 'NP'
                || $professionalCategory['value'] == 'STU'
            )
        ) {
            return true;
        }
        return false;
    }

    /**
     * @param $professionalCategory
     * @return bool
     */
    private function applyResidentCatRule($professionalCategory, $customerProfessionalCat): bool
    {
        if ($customerProfessionalCat && $customerProfessionalCat == 'RES'
            && ($professionalCategory['value'] == 'STU')
        ) {
            return true;
        }
        return false;
    }

    /**
     * @param $professionalCategory
     * @return bool
     */
    private function applyOtherCatRule($professionalCategory, $customerProfessionalCat): bool
    {
        if ($customerProfessionalCat && $customerProfessionalCat == 'OTH'
            && ($professionalCategory['value'] == 'RES'
                || $professionalCategory['value'] == 'PA'
                || $professionalCategory['value'] == 'NUR'
                || $professionalCategory['value'] == 'NP'
                || $professionalCategory['value'] == 'STU'
            )
        ) {
            return true;
        }
        return false;
    }
}
