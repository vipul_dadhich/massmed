<?php

namespace MMS\Promotions\Model\Api;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use MMS\Paybill\Model\Catalyst\Api\CheckEligibility;
use MMS\Promotions\Helper\Data;
use MMS\Promotions\Model\Api\Term\SetRequest;
use Ey\MuleSoft\Model\Core\Logger\Monolog;
use Ey\MuleSoft\Helper\Data as MuleSoftHelper;

/**
 * Class GetTermTranslation
 */
class GetTermTranslation
{
    /**
     * API request methodyt
     */
    const API_REQUEST_METHOD = 'GET';

    /**
     * JSON helper
     *
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var mixed
     */
    protected $isDebugMode;
    /**
     * @var SetRequest
     */
    protected $request;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var RequestInterface
     */
    protected $requestParam;
    /**
     * @var Monolog
     */
    protected $mulesoftLogger;
    /**
     * @var MuleSoftHelper
     */
    protected $MuleSoftHelper;
    /**
     * @var CheckEligibility
     */
    protected $checkEligibilityService;
    /**
     * @var bool
     */
    protected $isDebugModeType;

    /**
     * PostCampaign constructor.
     * @param SetRequest $request
     * @param Data $helper
     * @param JsonHelper $jsonHelper
     * @param RequestInterface $requestParam
     * @param Monolog $mulesoftLogger
     * @param MuleSoftHelper $MuleSoftHelper
     */
    public function __construct(
        SetRequest $request,
        Data $helper,
        JsonHelper $jsonHelper,
        RequestInterface $requestParam,
        Monolog $mulesoftLogger,
        MuleSoftHelper $MuleSoftHelper,
        CheckEligibility $checkEligibility
    ) {
        $this->request = $request;
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        $this->requestParam = $requestParam;
        $this->mulesoftLogger = $mulesoftLogger;
        $this->MuleSoftHelper = $MuleSoftHelper;
        $this->checkEligibilityService = $checkEligibility;
        $this->isDebugMode = $this->MuleSoftHelper->getMuleSoftDebug();
        $this->isDebugModeType = $this->MuleSoftHelper->getMuleSoftDebugMode();
    }

    /**
     * Fetch some data from API
     * @param $params array() or string
     */
    public function execute($term, $productCode)
    {
        $responseObj = ['term' => $term, 'magentoSku' => ''];
        $endpointUrl = $this->getEndpointUri($term, $productCode);
        //$endpointUrl = 'v1/api/products/details?term=104&productCode=NEJ';

        if (!$endpointUrl) {
            return $term;
        }
       $this->logApiRequest("************** GET Term Transaletion API Start ***************");
       $this->logApiRequest("API Endpoint: ".$endpointUrl);
       $response = $this->request->makeRequest(
           $endpointUrl,
         static::API_REQUEST_METHOD
          );
       $this->logApiRequest("Status Code: ".$response->getStatusCode());
        if($response->getStatusCode() == 200){
            $responseData = $this->jsonHelper->jsonDecode($response->getBody()->getContents());
            $this->logApiRequest("Reponse Data: ".json_encode($responseData, JSON_PRETTY_PRINT));
            $this->logApiRequest("************** GET Term Transaletion API End ***************");
            if (isset($responseData['product']['magentoTerm'])) {
                $responseObj['term'] = $responseData['product']['magentoTerm'];
                $responseObj['magentoSku'] = $responseData['product']['magentoSku'];
            }
        }
        $this->logApiRequest("************** GET Term Transaletion API End ***************");
        return $responseObj;
    }

    /**
     * @param $term
     * @param $productCode
     * @return mixed|string
     */
     private function getEndpointUri($term, $productCode){
         if ($productCode) {
             return $this->helper->getTermTranslationApiUrl($term).'&productCode='.$productCode;
         }
         return $this->helper->getTermTranslationApiUrl($term);

     }

    /**
     * @param $content
     * @param false $isCritical
     */
     public function logApiRequest($content, $isCritical = false){
        if($this->isDebugMode){
            if(!$isCritical){
                $this->mulesoftLogger->mulesoftLog($content);
            }
            elseif($isCritical && $this->isDebugModeType){
                $this->mulesoftLogger->mulesoftLog($content);
            }
        }

     }
}
