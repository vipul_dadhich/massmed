<?php

namespace MMS\Promotions\Model\Api;

use MMS\Promotions\Model\Api\Core\SetRequest;
use Ey\MuleSoft\Model\Core\Logger\Monolog;
use Ey\MuleSoft\Helper\Data as MuleSoftHelper;

/**
 * Class PostPricing
 */
class PostPricing
{
    /**
     * API request methodyt
     */
    const API_REQUEST_METHOD = 'POST';

    /**
     * JSON helper
     *
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var mixed
     */
    protected $isDebugMode;

    /**
     * PostPromotion constructor.
     * @param SetRequest $request
     * @param \MMS\Promotions\Helper\Data $helper
     * @param Monolog $mulesoftLogger
     * @param MuleSoftHelper $MuleSoftHelper
     */
    public function __construct(
        SetRequest $request,
        \MMS\Promotions\Helper\Data $helper,
        Monolog $mulesoftLogger,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        MuleSoftHelper $MuleSoftHelper
    ) {
        $this->request = $request;
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        $this->mulesoftLogger = $mulesoftLogger;
        $this->MuleSoftHelper = $MuleSoftHelper;
        $this->isDebugMode = $this->MuleSoftHelper->getMuleSoftDebug();
        $this->isDebugModeType = $this->MuleSoftHelper->getMuleSoftDebugMode();
    }

    /**
     * @param $promotion
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute($payload)
    {
       $priceResponse = ['status' => '', 'message' => ''];
       $this->logApiRequest("************** POST Pricing API Start ***************");
       $this->logApiRequest("API Endpoint: ".$this->getEndpointUri());
       $this->logApiRequest("Payload: ". json_encode($payload, JSON_PRETTY_PRINT), true);
       $response = $this->request->makeRequest( $this->getEndpointUri(),
         static::API_REQUEST_METHOD,
           $payload
          );
        $this->logApiRequest("Status Code: ".$response->getStatusCode());
        if($response->getStatusCode() == 200){
            $priceResponse['status'] = $response->getStatusCode();
            $this->logApiRequest("************** POST Pricing API End ***************");
            return $priceResponse;
        }
        $this->logApiRequest("ERROR:".$this->request->getErrorResponse());
        if ($this->request->getErrorResponse()) {
            $responseData = $this->jsonHelper->jsonDecode($this->request->getErrorResponse());
            if (isset($responseData['message'])) {
                $priceResponse['message'] = $responseData['message'];
            }
        }
        $priceResponse['status'] = $response->getStatusCode();
        $this->logApiRequest("************** POST Pricing API End ***************");
        return $priceResponse;
    }
     /**
      * get endpoint uri
      */
     private function getEndpointUri(){
         return $this->helper->getPricingPostApiUrl();
     }

    /**
     * @param $content
     * @param false $isCritical
     */
     public function logApiRequest($content, $isCritical = false){
        if($this->isDebugMode){
            if(!$isCritical){
                $this->mulesoftLogger->mulesoftLog($content);
            }
            elseif($isCritical && $this->isDebugModeType){
                $this->mulesoftLogger->mulesoftLog($content);
            }
        }

     }
}
