<?php

namespace MMS\Promotions\Model\Api;

use MMS\Promotions\Model\Api\Core\SetRequest;
use Ey\MuleSoft\Model\Core\Logger\Monolog;
use Ey\MuleSoft\Helper\Data as MuleSoftHelper;

/**
 * Class PostCampaign
 */
class PostCampaign
{
    /**
     * API request methodyt
     */
    const API_REQUEST_METHOD = 'POST';

    /**
     * JSON helper
     *
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var mixed
     */
    protected $isDebugMode;

    /**
     * PostCampaign constructor.
     * @param SetRequest $request
     * @param \MMS\Promotions\Helper\Data $helper
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\App\RequestInterface $requestParam
     * @param Monolog $mulesoftLogger
     * @param MuleSoftHelper $MuleSoftHelper
     */
    public function __construct(
        SetRequest $request,
        \MMS\Promotions\Helper\Data $helper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\App\RequestInterface $requestParam,
        Monolog $mulesoftLogger,
        MuleSoftHelper $MuleSoftHelper
    ) {
        $this->request = $request;
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        $this->requestParam = $requestParam;
        $this->mulesoftLogger = $mulesoftLogger;
        $this->MuleSoftHelper = $MuleSoftHelper;
        $this->isDebugMode = $this->MuleSoftHelper->getMuleSoftDebug();
        $this->isDebugModeType = $this->MuleSoftHelper->getMuleSoftDebugMode();
    }

    /**
     * Fetch some data from API
     * @param $params array() or string
     */
    public function execute($campaignCode)
    {
       $campaignResponse = ['status' => '', 'message' => ''];
       $this->logApiRequest("************** POST Campaign API Start ***************");
       $this->logApiRequest("API Endpoint: ".$this->getEndpointUri($campaignCode));
       $postParams = $this->helper->getApiPostParams($campaignCode);
       $this->logApiRequest("Payload: ". json_encode($postParams, JSON_PRETTY_PRINT), true);
       $response = $this->request->makeRequest(
        $this->getEndpointUri($campaignCode),
         static::API_REQUEST_METHOD,
         $this->helper->getApiPostParams($campaignCode));
       $this->logApiRequest("Status Code: ".$response->getStatusCode());
        if($response->getStatusCode() == 201){
            $campaignResponse['status'] = $response->getStatusCode();
            $this->logApiRequest("************** POST Campaign API End ***************");
            return $campaignResponse;
        }
        $this->logApiRequest("ERROR:".$this->request->getErrorResponse());
        if ($this->request->getErrorResponse()) {
            $responseData = $this->jsonHelper->jsonDecode($this->request->getErrorResponse());
            if (isset($responseData['message'])) {
                $campaignResponse['message'] = $responseData['message'];
            }
        }
        $this->logApiRequest("************** POST Campaign API End ***************");
        $campaignResponse['status'] = $response->getStatusCode();
        return $campaignResponse;
    }
     /**
      * get endpoint uri
      */
     private function getEndpointUri($campaignCode){
         return $this->helper->getCampaignPostApiUrl($campaignCode);
     }

    /**
     * @param $content
     * @param false $isCritical
     */
     public function logApiRequest($content, $isCritical = false){
        if($this->isDebugMode){
            if(!$isCritical){
                $this->mulesoftLogger->mulesoftLog($content);
            }
            elseif($isCritical && $this->isDebugModeType){
                $this->mulesoftLogger->mulesoftLog($content);
            }
        }

     }
}
