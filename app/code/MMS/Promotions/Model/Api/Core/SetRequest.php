<?php

namespace MMS\Promotions\Model\Api\Core;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Psr7\ResponseFactory;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Framework\Webapi\Rest\Request;

class SetRequest
{
    /**
     * API request URL
     */
    const API_REQUEST_URI = '';

    /**
     * SetRequest constructor.
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     * @param \MMS\Promotions\Helper\Data $helper
     */
    public function __construct(
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory,
        \MMS\Promotions\Helper\Data $helper
    )
    {
        $this->clientFactory = $clientFactory;
        $this->responseFactory = $responseFactory;
        $this->helper  = $helper;
    }


    /**
     * Do API request with provided params
     *
     * @param string $apiMethod
     * @param string $requestMethod
     * @param array $params
     *
     * @return Response
     */
    public function makeRequest($apiMethod, $requestMethod, $params = [])
    {
        return $this->doRequest($apiMethod, $params, $requestMethod);
    }

    /**
     * Do API request with provided params
     *
     * @param string $uriEndpoint
     * @param array $params
     * @param string $requestMethod
     *
     * @return Response
     */
    private function doRequest(
        string $uriEndpoint,
        array $params = [],
        string $requestMethod = Request::HTTP_METHOD_GET
    )
    {
        /** @var Client $client */
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => $this->helper->getMuleSoftPromoURI(),
            'headers' => [
                'client_id' => $this->helper->getMuleSoftPromoClientId(),
                'client_secret' => $this->helper->getMuleSoftPromoClientSecret()
            ]
        ]]);
        try {
            if ($requestMethod == 'POST') {
                $params = array(
                    'json' => $params
                );
                $response = $client->request(
                    $requestMethod,
                    $uriEndpoint,
                    $params
                );
            } else {

                $response = $client->request(
                    $requestMethod,
                    $uriEndpoint
                );
            }
        } catch (GuzzleException $exception) {
            /** @var Response $response */
            $response = $this->responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);
            $this->errorResponse = $exception->getResponse()->getBody()->getContents();
        }
        return $response;
    }

    public function getErrorResponse(){
        return $this->errorResponse;
    }
}

