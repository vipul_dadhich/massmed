<?php

namespace MMS\Promotions\Model\Api;

use MMS\Promotions\Model\Api\Core\SetRequest;
use Ey\MuleSoft\Model\Core\Logger\Monolog;
use Ey\MuleSoft\Helper\Data as MuleSoftHelper;

/**
 * Class PostPromotion
 */
class PostPromotionBulk
{
    /**
     * API request methodyt
     */
    const API_REQUEST_METHOD = 'POST';

    /**
     * JSON helper
     *
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var mixed
     */
    protected $isDebugMode;

    /**
     * PostPromotion constructor.
     * @param SetRequest $request
     * @param \MMS\Promotions\Helper\Data $helper
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\App\RequestInterface $requestParam
     * @param Monolog $mulesoftLogger
     * @param MuleSoftHelper $MuleSoftHelper
     */
    public function __construct(
        SetRequest $request,
        \MMS\Promotions\Helper\Data $helper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\App\RequestInterface $requestParam,
        Monolog $mulesoftLogger,
        MuleSoftHelper $MuleSoftHelper
    ) {
        $this->request = $request;
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        $this->requestParam = $requestParam;
        $this->mulesoftLogger = $mulesoftLogger;
        $this->MuleSoftHelper = $MuleSoftHelper;
        $this->isDebugMode = $this->MuleSoftHelper->getMuleSoftDebug();
        $this->isDebugModeType = $this->MuleSoftHelper->getMuleSoftDebugMode();
    }

    /**
     * @param $promotion
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute($promotionPayload)
    {
       $promoResponse = ['status' => '', 'message' => ''];
       $this->logApiRequest("************** POST Promotion Bulk API Start ***************");
       $this->logApiRequest("API Endpoint: ".$this->getEndpointUri());
       
       $this->logApiRequest("Payload: ". json_encode($promotionPayload, JSON_PRETTY_PRINT), true);
       $responseData = '';
       $response = $this->request->makeRequest(
        $this->getEndpointUri(),
         static::API_REQUEST_METHOD,
         $promotionPayload
          );
       $this->logApiRequest("Status Code: ".$response->getStatusCode());
       $this->logApiRequest("Response Object:- ");

        if($response->getStatusCode() == 201){
            $this->logApiRequest("************** POST Promotion Bulk API End ***************");
            $promoResponse['status'] = $response->getStatusCode();
            return $promoResponse;
        }
        $this->logApiRequest("ERROR:".$this->request->getErrorResponse());
        if ($this->request->getErrorResponse()) {
            $responseData = $this->jsonHelper->jsonDecode($this->request->getErrorResponse());
            if (isset($responseData['message'])) {
                $promoResponse['message'] = $responseData['message'];
            }
        }
        
        $this->logApiRequest("************** POST Promotion API Bulk End ***************");
        $promoResponse['status'] = $response->getStatusCode();
        return $promoResponse;
    }
     /**
      * get endpoint uri
      */
     private function getEndpointUri(){
         return $this->helper->getPromotionPostApiUrl();
     }

    /**
     * @param $content
     * @param false $isCritical
     */
     public function logApiRequest($content, $isCritical = false){
        if($this->isDebugMode){
            if(!$isCritical){
                $this->mulesoftLogger->mulesoftLog($content);
            }
            elseif($isCritical && $this->isDebugModeType){
                $this->mulesoftLogger->mulesoftLog($content);
            }
        }

     }
}
