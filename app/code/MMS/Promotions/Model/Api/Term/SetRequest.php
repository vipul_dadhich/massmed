<?php

namespace MMS\Promotions\Model\Api\Term;

use GuzzleHttp\Client;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Psr7\ResponseFactory;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Framework\Webapi\Rest\Request;
use Ey\MuleSoft\Helper\Data;

class SetRequest
{
    /**
     * @var ClientFactory
     */
    protected $clientFactory;
    /**
     * @var ResponseFactory
     */
    protected $responseFactory;
    /**
     * @var Data
     */
    protected $helper;

    /**
     * SetRequest constructor.
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     * @param Data $helper
     */
    public function __construct(
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory,
        Data $helper
    )
    {
        $this->clientFactory = $clientFactory;
        $this->responseFactory = $responseFactory;
        $this->helper  = $helper;
    }


    /**
     * Do API request with provided params
     *
     * @param string $apiMethod
     * @param string $requestMethod
     * @param array $params
     *
     * @return Response
     */
    public function makeRequest($apiMethod, $requestMethod, $params = [])
    {
        return $this->doRequest($apiMethod, $params, $requestMethod);
    }

    /**
     * Do API request with provided params
     *
     * @param string $uriEndpoint
     * @param array $params
     * @param string $requestMethod
     *
     * @return Response
     */
    private function doRequest(
        string $uriEndpoint,
        array $params = [],
        string $requestMethod = Request::HTTP_METHOD_GET
    )
    {
        /** @var Client $client */
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => $this->helper->getMuleSoftCustomerURI(),
            'headers' => [
                'client_id' => $this->helper->getMuleSoftClientId(),
                'client_secret' => $this->helper->getMuleSoftClientSecret()
            ]
        ]]);
        try {
            if ($requestMethod == 'POST') {
                $params = array(
                    'json' => $params
                );
                $response = $client->request(
                    $requestMethod,
                    $uriEndpoint,
                    $params
                );
            } else {

                $response = $client->request(
                    $requestMethod,
                    $uriEndpoint
                );
            }
        } catch (GuzzleException $exception) {
            /** @var Response $response */
            $response = $this->responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);
            $this->errorResponse = $exception->getResponse()->getBody()->getContents();
        }
        return $response;
    }

    public function getErrorResponse(){
        return $this->errorResponse;
    }
}

