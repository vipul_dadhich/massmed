<?php

namespace MMS\Promotions\Model\Api;

use MMS\Promotions\Model\Api\Core\SetRequest;
use Ey\MuleSoft\Model\Core\Logger\Monolog;
use Ey\MuleSoft\Helper\Data as MuleSoftHelper;

/**
 * Class GetCampaign
 */
class GetCampaign
{
    /**
     * API request methodyt
     */
    const API_REQUEST_METHOD = 'GET';

    /**
     * JSON helper
     *
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var mixed
     */
    protected $isDebugMode;

    /**
     * PostCampaign constructor.
     * @param SetRequest $request
     * @param \MMS\Promotions\Helper\Data $helper
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\App\RequestInterface $requestParam
     * @param Monolog $mulesoftLogger
     * @param MuleSoftHelper $MuleSoftHelper
     */
    public function __construct(
        SetRequest $request,
        \MMS\Promotions\Helper\Data $helper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\App\RequestInterface $requestParam,
        Monolog $mulesoftLogger,
        MuleSoftHelper $MuleSoftHelper
    ) {
        $this->request = $request;
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        $this->requestParam = $requestParam;
        $this->mulesoftLogger = $mulesoftLogger;
        $this->MuleSoftHelper = $MuleSoftHelper;
        $this->isDebugMode = $this->MuleSoftHelper->getMuleSoftDebug();
        $this->isDebugModeType = $this->MuleSoftHelper->getMuleSoftDebugMode();
    }

    /**
     * Fetch some data from API
     * @param $params array() or string
     */
    public function execute($campaignCode)
    {
       $this->logApiRequest("************** GET Campaign API Start ***************");
       $this->logApiRequest("API Endpoint: ".$this->getEndpointUri($campaignCode));
       $response = $this->request->makeRequest(
        $this->getEndpointUri($campaignCode),
         static::API_REQUEST_METHOD
          );
       $this->logApiRequest("Status Code: ".$response->getStatusCode());
        if($response->getStatusCode() == 200){
            $responseData = $this->jsonHelper->jsonDecode($response->getBody()->getContents());
            $this->logApiRequest("Reponse Data: ".json_encode($responseData, JSON_PRETTY_PRINT));
            $this->logApiRequest("************** GET Campaign API End ***************");
            if (isset($responseData['totalCount']) && $responseData['totalCount'] >= 1) {
                return true;
            }
            return $response->getStatusCode();
        }
        $this->logApiRequest("************** GET Campaign API End ***************");
        return $response->getStatusCode();
    }
     /**
      * get endpoint uri
      */
     private function getEndpointUri($campaignCode){
         return $this->helper->getCampaignGetApiUrl($campaignCode);
     }

    /**
     * @param $content
     * @param false $isCritical
     */
     public function logApiRequest($content, $isCritical = false){
        if($this->isDebugMode){
            if(!$isCritical){
                $this->mulesoftLogger->mulesoftLog($content);
            }
            elseif($isCritical && $this->isDebugModeType){
                $this->mulesoftLogger->mulesoftLog($content);
            }
        }

     }
}
