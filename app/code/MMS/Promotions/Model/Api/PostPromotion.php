<?php

namespace MMS\Promotions\Model\Api;

use MMS\Promotions\Model\Api\Core\SetRequest;
use Ey\MuleSoft\Model\Core\Logger\Monolog;
use Ey\MuleSoft\Helper\Data as MuleSoftHelper;

/**
 * Class PostPromotion
 */
class PostPromotion
{
    /**
     * API request methodyt
     */
    const API_REQUEST_METHOD = 'POST';

    /**
     * JSON helper
     *
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var mixed
     */
    protected $isDebugMode;

    /**
     * PostPromotion constructor.
     * @param SetRequest $request
     * @param \MMS\Promotions\Helper\Data $helper
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\App\RequestInterface $requestParam
     * @param Monolog $mulesoftLogger
     * @param MuleSoftHelper $MuleSoftHelper
     */
    public function __construct(
        SetRequest $request,
        \MMS\Promotions\Helper\Data $helper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\App\RequestInterface $requestParam,
        Monolog $mulesoftLogger,
        MuleSoftHelper $MuleSoftHelper
    ) {
        $this->request = $request;
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        $this->requestParam = $requestParam;
        $this->mulesoftLogger = $mulesoftLogger;
        $this->MuleSoftHelper = $MuleSoftHelper;
        $this->isDebugMode = $this->MuleSoftHelper->getMuleSoftDebug();
        $this->isDebugModeType = $this->MuleSoftHelper->getMuleSoftDebugMode();
    }

    /**
     * @param $promotion
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute($promotion)
    {
       $this->logApiRequest("************** POST Promotion API Start ***************");
       $this->logApiRequest("API Endpoint: ".$this->getEndpointUri());
       $postParams = $this->helper->getPromotionApiPostParams($promotion);
       $this->logApiRequest("Payload: ". json_encode($postParams, JSON_PRETTY_PRINT), true);
       $responseData = '';
       $response = $this->request->makeRequest(
        $this->getEndpointUri(),
         static::API_REQUEST_METHOD,
         $postParams
          );
       $this->logApiRequest("Status Code: ".$response->getStatusCode());
       $this->logApiRequest("Response Object:- ");
       $this->logApiRequest(json_encode($response, JSON_PRETTY_PRINT));
        if($response->getStatusCode() == 201){
            $this->logApiRequest("************** POST Promotion API End ***************");
            return $response->getStatusCode();
        }
        if($response->getStatusCode() == 400){
            $this->logApiRequest("************** POST Promotion API End ***************");
            return $response->getStatusCode();
        }
        $this->logApiRequest("************** POST Promotion API End ***************");
        $message = 'POST Promotion API Error with errorcode: '.$response->getStatusCode();
        if ($this->request->getErrorResponse()) {
            $message = $this->request->getErrorResponse();
        }
        throw new \Magento\Framework\Exception\LocalizedException(__($message));
    }
     /**
      * get endpoint uri
      */
     private function getEndpointUri(){
         return $this->helper->getPromotionPostApiUrl();
     }

    /**
     * @param $content
     * @param false $isCritical
     */
     public function logApiRequest($content, $isCritical = false){
        if($this->isDebugMode){
            if(!$isCritical){
                $this->mulesoftLogger->mulesoftLog($content);
            }
            elseif($isCritical && $this->isDebugModeType){
                $this->mulesoftLogger->mulesoftLog($content);
            }
        }

     }
}
