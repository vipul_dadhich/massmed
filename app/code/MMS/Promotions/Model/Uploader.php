<?php

namespace MMS\Promotions\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\UrlInterface;
use Magento\MediaStorage\Helper\File\Storage\Database;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Filesystem\Driver\File;

/**
 * Class Uploader
 * @package MMS\Promotions\Model
 */
class Uploader
{
    /**
     * @var string
     */
    const IMAGE_TMP_PATH    = 'promotions/tmp/';
    /**
     * @var string
     */
    const IMAGE_PATH        = 'promotions/';
    /**
     * @var string
     */
    const FILE_TMP_PATH     = 'promotions/tmp/file';
    /**
     * @var string
     */
    const FILE_PATH         = 'promotions/file';

    /**
     * Core file storage database
     *
     * @var \Magento\MediaStorage\Helper\File\Storage\Database
     */
    protected $coreFileStorageDatabase;

    /**
     * Media directory object (writable).
     *
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    protected $mediaDirectory;

    /**
     * Pub directory object (writable).
     *
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    protected $pubDirectory;

    /**
     * Uploader factory
     *
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    private $uploaderFactory;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Base tmp path
     *
     * @var string
     */
    protected $baseTmpPath;

    /**
     * Base path
     *
     * @var string
     */
    protected $basePath;

    /**
     * Allowed extensions
     *
     * @var string
     */
    protected $allowedExtensions;

    protected $_file;
    protected $mediaRootDir;
    /**
     * Uploader constructor.
     * @param Database $coreFileStorageDatabase
     * @param Filesystem $filesystem
     * @param UploaderFactory $uploaderFactory
     * @param StoreManagerInterface $storeManager
     * @param LoggerInterface $logger
     * @param Filesystem\Io\File $filesystemIo
     * @param string $baseTmpPath
     * @param string $basePath
     * @param array $allowedExtensions
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Database $coreFileStorageDatabase,
        Filesystem $filesystem,
        UploaderFactory $uploaderFactory,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger,
        \Magento\Framework\Filesystem\Io\File $filesystemIo,
        File $file,
        $baseTmpPath = self::IMAGE_TMP_PATH,
        $basePath = self::IMAGE_PATH,
        $allowedExtensions = []
    ) {
        $this->coreFileStorageDatabase  = $coreFileStorageDatabase;
        $this->mediaDirectory           = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->pubDirectory             = $filesystem->getDirectoryWrite(DirectoryList::PUB);
        $this->mediaRootDir             = $filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
        $this->uploaderFactory          = $uploaderFactory;
        $this->storeManager             = $storeManager;
        $this->logger                   = $logger;
        $this->baseTmpPath              = $baseTmpPath;
        $this->filesystemIo             = $filesystemIo;
        $this->basePath                 = $basePath;
        $this->allowedExtensions        = $allowedExtensions;
        $this->_file = $file;
    }

    /**
     * Set base tmp path
     *
     * @param string $baseTmpPath
     *
     * @return void
     */
    public function setBaseTmpPath($baseTmpPath)
    {
        $this->baseTmpPath = $baseTmpPath;
    }

    /**
     * Set base path
     *
     * @param string $basePath
     *
     * @return void
     */
    public function setBasePath($basePath)
    {
        $this->basePath = $basePath;
    }

    /**
     * Set allowed extensions
     *
     * @param string[] $allowedExtensions
     *
     * @return void
     */
    public function setAllowedExtensions($allowedExtensions)
    {
        $this->allowedExtensions = $allowedExtensions;
    }

    /**
     * Retrieve base tmp path
     *
     * @return string
     */
    public function getBaseTmpPath()
    {
        return $this->baseTmpPath;
    }

    /**
     * Retrieve base path
     *
     * @return string
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * Retrieve base path
     *
     * @return string[]
     */
    public function getAllowedExtensions()
    {
        return $this->allowedExtensions;
    }

    /**
     * Retrieve path
     *
     * @param string $path
     * @param string $name
     *
     * @return string
     */
    public function getFilePath($path, $name)
    {
        // return $this->mediaDirectory->getAbsolutePath() . rtrim($path, '/') . '/' . ltrim($name, '/');
        return rtrim($path, '/') . '/' . ltrim($name, '/');
    }

    /**
     * Retrieve url
     *
     * @param string $path
     * @param string $name
     *
     * @return string
     */
    public function getFileUrl($path, $name)
    {
        return rtrim($path, '/') . '/' . ltrim($name, '/');
    }

    /**
     * Checking file for moving and move it
     *
     * @param string $name
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function moveFileFromTmp($name)
    {
        $baseTmpPath = $this->getBaseTmpPath();
        $basePath = $this->getBasePath();
        $baseFilePath = $this->getFilePath($basePath, $name);
        $baseTmpFilePath = $this->getFilePath($baseTmpPath, $name);
        try {
            //echo $baseTmpFilePath.'-----'.$baseFilePath;die;
            $this->mediaDirectory->copyFile($baseTmpFilePath ,$baseFilePath);
            // if($this->filesystemIo->cp($baseTmpFilePath, $baseFilePath)){
            //     echo 'Uploaded';die;
            // }else{
            //     echo 'Not';die;
            // }
        } catch (\Exception $e) {
            throw new LocalizedException(
                __('Something went wrong while saving the file(s).'.$e->getMessage())
            );
        }
        return $name;
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBaseUrl()
    {
        return $this->storeManager
            ->getStore()
            ->getBaseUrl(
                UrlInterface::URL_TYPE_MEDIA
            );
    }
    /**
     * Checking file for save and save it to tmp dir
     *
     * @param string $fileId
     * @return string[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function saveFileToTmpDir($fileId)
    {
        $result = array();
        $baseTmpPath = $this->getBaseTmpPath();
        $uploader = $this->uploaderFactory->create(['fileId' => $fileId]);
        $uploader->setAllowedExtensions($this->getAllowedExtensions());
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(true);
        $result = $uploader->save($this->mediaDirectory->getAbsolutePath($baseTmpPath));
        if (!$result) {
            throw new LocalizedException(
                __('File can not be saved to the destination folder.')
            );
        }
        $result['tmp_name'] = str_replace('\\', '/', $result['tmp_name']);
        $result['path'] = str_replace('\\', '/', $result['path']);
        $result['url'] =  $this->getBaseUrl() . $this->getFileUrl($baseTmpPath, $result['file']);

        if (isset($result['file'])) {
            try {
                $relativePath = rtrim($baseTmpPath, '/') . '/' . ltrim($result['file'], '/');
                $this->coreFileStorageDatabase->saveFile($relativePath);
            } catch (\Exception $e) {
                $this->logger->critical($e);
                throw new LocalizedException(
                    __('Something went wrong while saving the file(s).')
                );
            }
        }
        return $result;
    }

    /**
     * @param $input
     * @param $data
     * @return string
     */
    public function uploadFileAndGetName($input, $data)
    {
        if (!isset($data[$input])) {
            return '';
        }

        if (isset($data[$input][0]['name']) && isset($data[$input][0]['tmp_name'])) {
            try {
                $result = $this->moveFileFromTmp($data[$input][0]['file']);
                $this->deleteTmpFile($input, $data);
                return $data[$input][0]['file'];
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        elseif (isset($data[$input][0]['type']) && !isset($data[$input][0]['cookie'])) {
            try {
                $result = $this->copyFileFromGallery($data[$input][0]['url'], $data[$input][0]['name']);
                return $result;
            } catch (\Exception $e) {
                $this->logger->critical($e);
                throw new LocalizedException(
                    __('Something went wrong while saving the file(s).'.$e->getMessage())
                );
            }
        }
        elseif (isset($data[$input][0]['name'])) {
            return $data[$input][0]['name'];
        }

        return '';
    }

    public function deleteTmpFile($input, $data){
        if(isset($data[$input][0]['path']) && isset($data[$input][0]['file'])){
            $tmpFilepath = rtrim($data[$input][0]['path'], "/") . $data[$input][0]['file'];
            if ($this->_file->isExists($tmpFilepath)) {
                $this->_file->deleteFile($tmpFilepath);
            }
        }
    }

    /**
     * @param $path
     * @param $name
     * @return string
     * @throws LocalizedException
     */
    public function copyFileFromGallery($path, $name)
    {
        $filename = '/'.time().$name;
        $basePath = $this->getBasePath();
        $baseFilePath = rtrim($this->pubDirectory->getAbsolutePath(), '/') . '/media/'.$this->getFilePath($basePath, $filename);
        $baseTmpFilePath = rtrim($this->pubDirectory->getAbsolutePath(), '/') . $path;
        try {

            $this->filesystemIo->cp($baseTmpFilePath, $baseFilePath);
        } catch (\Exception $e) {
            throw new LocalizedException(
                __('Something went wrong while saving the file(s).')
            );
        }
        return $filename;
    }
}
