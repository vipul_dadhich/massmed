<?php
namespace MMS\Promotions\Model;

use MMS\Promotions\Api\Data\PromotionInterface;
use MMS\Promotions\Api\Data\PromotionInterfaceFactory;
use MMS\Promotions\Api\Data\PromotionSearchResultInterfaceFactory;
use MMS\Promotions\Api\PromotionRepositoryInterface;
use MMS\Promotions\Model\ResourceModel\Promotion as PromotionResourceModel;
use MMS\Promotions\Model\ResourceModel\Promotion\Collection;
use MMS\Promotions\Model\ResourceModel\Promotion\CollectionFactory as PromotionCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;

/**
 * Class PromotionRepository
 * @package MMS\Promotions\Model
 */
class PromotionRepository implements PromotionRepositoryInterface
{
    /**
     * Cached instances
     *
     * @var array
     */
    protected $instances = [];

    /**
     * Promotion resource model
     *
     * @var PromotionResourceModel
     */
    protected $resource;

    /**
     * Promotion collection factory
     *
     * @var PromotionCollectionFactory
     */
    protected $promotionCollectionFactory;

    /**
     * Promotion interface factory
     *
     * @var PromotionInterfaceFactory
     */
    protected $promotionInterfaceFactory;

    /**
     * Data Object Helper
     *
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Search result factory
     *
     * @var PromotionSearchResultInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * constructor
     *
     * @param PromotionResourceModel $resource
     * @param PromotionCollectionFactory $promotionCollectionFactory
     * @param PromotionInterfaceFactory $promotionInterfaceFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param PromotionSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        PromotionResourceModel $resource,
        PromotionCollectionFactory $promotionCollectionFactory,
        PromotionInterfaceFactory $promotionInterfaceFactory,
        DataObjectHelper $dataObjectHelper,
        PromotionSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->resource                   = $resource;
        $this->promotionCollectionFactory = $promotionCollectionFactory;
        $this->promotionInterfaceFactory  = $promotionInterfaceFactory;
        $this->dataObjectHelper           = $dataObjectHelper;
        $this->searchResultsFactory       = $searchResultsFactory;
    }

    /**
     * Save Promotion.
     *
     * @param \MMS\Promotions\Api\Data\PromotionInterface $promotion
     * @return \MMS\Promotions\Api\Data\PromotionInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(PromotionInterface $promotion)
    {
        /** @var PromotionInterface|\Magento\Framework\Model\AbstractModel $promotion */
        try {
            $this->resource->save($promotion);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Promotion: %1',
                $exception->getMessage()
            ));
        }
        return $promotion;
    }

    /**
     * Retrieve Promotion.
     *
     * @param int $promotionId
     * @return \MMS\Promotions\Api\Data\PromotionInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($promotionId)
    {
        if (!isset($this->instances[$promotionId])) {
            /** @var PromotionInterface|\Magento\Framework\Model\AbstractModel $promotion */
            $promotion = $this->promotionInterfaceFactory->create();
            $this->resource->load($promotion, $promotionId);
            if (!$promotion->getId()) {
                throw new NoSuchEntityException(__('Requested Promotion doesn\'t exist'));
            }
            $this->instances[$promotionId] = $promotion;
        }
        return $this->instances[$promotionId];
    }

    /**
     * Retrieve Promotions matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \MMS\Promotions\Api\Data\PromotionSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \MMS\Promotions\Api\Data\PromotionSearchResultInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \MMS\Promotions\Model\ResourceModel\Promotion\Collection $collection */
        $collection = $this->promotionCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'promotion_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var PromotionInterface[] $promotions */
        $promotions = [];
        /** @var \MMS\Promotions\Model\Promotion $promotion */
        foreach ($collection as $promotion) {
            /** @var PromotionInterface $promotionDataObject */
            $promotionDataObject = $this->promotionInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $promotionDataObject,
                $promotion->getData(),
                PromotionInterface::class
            );
            $promotions[] = $promotionDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($promotions);
    }

    /**
     * Delete Promotion.
     *
     * @param PromotionInterface $promotion
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(PromotionInterface $promotion)
    {
        /** @var PromotionInterface|\Magento\Framework\Model\AbstractModel $promotion */
        $id = $promotion->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($promotion);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to remove Promotion %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete Promotion by ID.
     *
     * @param int $promotionId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($promotionId)
    {
        $promotion = $this->getById($promotionId);
        return $this->delete($promotion);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(
        FilterGroup $filterGroup,
        Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
