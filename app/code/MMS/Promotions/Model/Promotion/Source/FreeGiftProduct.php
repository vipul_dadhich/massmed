<?php
namespace MMS\Promotions\Model\Promotion\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class FreeGiftProduct
 * @package MMS\Promotions\Model\Promotion\Source
 */
class FreeGiftProduct implements ArrayInterface
{
    /** @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory */
    protected $collectionFactory;

    /**
     * FreeGiftProduct constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        $productCollection = $this->collectionFactory->create();
        /** Apply filters here */
        $productCollection->addAttributeToSelect('sku');
        foreach ($productCollection as $product){
             $options[] = array('value' => $product->getSku(), 'label' => $product->getSku());
        }
        return $options;
    }
}
