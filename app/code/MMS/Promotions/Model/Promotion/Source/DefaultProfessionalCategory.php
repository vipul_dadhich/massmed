<?php
namespace MMS\Promotions\Model\Promotion\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class DefaultProfessionalCategory
 * @package MMS\Promotions\Model\Promotion\Source
 */
class DefaultProfessionalCategory implements ArrayInterface
{
    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    /**
     * DefaultProfessionalCategory constructor.
     * @param \Magento\Eav\Model\Config $eavConfig
     */
    public function __construct(
        \Magento\Eav\Model\Config $eavConfig
    ){
        $this->eavConfig = $eavConfig;
    }

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        $attribute = $this->eavConfig->getAttribute('customer', 'professional_category');
        $attributeOptions = $attribute->getSource()->getAllOptions();
        if (!empty($attributeOptions)) {
            foreach($attributeOptions as $option) {
                $options[] = array('value' => $option['value'], 'label' => $option['label']);
            }
        }
        return $options;
    }
}
