<?php
namespace MMS\Promotions\Model\Promotion\Source;
use Magento\Framework\Option\ArrayInterface;
/**
 * Class BindedTemplate
 * @package MMS\Promotions\Model\Promotion\Source
 */
class BindedTemplate implements ArrayInterface
{
    /**
     * @var \Magento\Cms\Model\ResourceModel\Page\CollectionFactory
     */
    protected $pageCollectionFactory;

    /**
     * BindedTemplate constructor.
     * @param \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $pageCollectionFactory
     */
    public function __construct(
        \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $pageCollectionFactory
    ){
        $this->pageCollectionFactory = $pageCollectionFactory;
    }

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        $pageCollection = $this->pageCollectionFactory->create();
        $pageCollection->addFieldToFilter('is_active' , \Magento\Cms\Model\Page::STATUS_ENABLED);
        $pageCollection->addFieldToFilter('is_promotional' , 1);
        $options[] = array('value' => '', 'label' => 'Please Select');
        foreach($pageCollection as $page){
            $options[] = array('value' => $page->getIdentifier(), 'label' => $page->getTitle()." - ".$page->getIdentifier());
        }
        return $options;
    }
}
