<?php
namespace MMS\Promotions\Model\Promotion;

use Magento\Framework\Filesystem;
use Magento\Framework\UrlInterface;
use MMS\Promotions\Model\ResourceModel\Promotion\CollectionFactory as PromotionCollectionFactory;
use MMS\Promotions\Model\ResourceModel\PcTerms\CollectionFactory as PcTermsCollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class DataProvider
 * @package MMS\Promotions\Model\Promotion
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * Loaded data cache
     *
     * @var array
     */
    protected $loadedData;

    /**
     * Data persistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Store Manager
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Filesystem\Directory\ReadInterface
     */
    protected $_mediaDirectory;

    /**
     * @var PcTermsCollectionFactory
     */
    protected $pcTermscollection;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param PromotionCollectionFactory $collectionFactory
     * @param PcTermsCollectionFactory $pcTermscollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param StoreManagerInterface $storeManager
     * @param Filesystem $fileSystem
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        PromotionCollectionFactory $collectionFactory,
        PcTermsCollectionFactory $pcTermscollectionFactory,
        DataPersistorInterface $dataPersistor,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem $fileSystem,
        array $meta = [],
        array $data = []
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->storeManager = $storeManager;
        $this->_mediaDirectory = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->pcTermscollection = $pcTermscollectionFactory;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $promotion) {
            $this->loadedData[$promotion->getId()] = $promotion->getData();
            $fullData = $this->loadedData;
            $imageData = array();
            if ($promotion->getCartanchorone()) {
                $imageData['cartanchorone'][0]['name'] = $promotion->getCartanchorone();
                $imageData['cartanchorone'][0]['url'] = $this->getMediaUrl().$promotion->getCartanchorone();
            }
            if ($promotion->getCartanchortwo()) {
                $imageData['cartanchortwo'][0]['name'] = $promotion->getCartanchortwo();
                $imageData['cartanchortwo'][0]['url'] = $this->getMediaUrl().$promotion->getCartanchortwo();
            }
            if ($promotion->getCheckoutanchorone()) {
                $imageData['checkoutanchorone'][0]['name'] = $promotion->getCheckoutanchorone();
                $imageData['checkoutanchorone'][0]['url'] = $this->getMediaUrl().$promotion->getCheckoutanchorone();
            }
            if ($promotion->getCheckoutanchortwo()) {
                $imageData['checkoutanchortwo'][0]['name'] = $promotion->getCheckoutanchortwo();
                $imageData['checkoutanchortwo'][0]['url'] = $this->getMediaUrl().$promotion->getCheckoutanchortwo();
            }
            if ($promotion->getConfirmationanchorone()) {
                $imageData['confirmationanchorone'][0]['name'] = $promotion->getConfirmationanchorone();
                $imageData['confirmationanchorone'][0]['url'] = $this->getMediaUrl().$promotion->getConfirmationanchorone();
            }
            if ($promotion->getConfirmationanchortwo()) {
                $imageData['confirmationanchortwo'][0]['name'] = $promotion->getConfirmationanchortwo();
                $imageData['confirmationanchortwo'][0]['url'] = $this->getMediaUrl().$promotion->getConfirmationanchortwo();
            }
            if ($promotion->getLandinganchorone()) {
                $imageData['landinganchorone'][0]['name'] = $promotion->getLandinganchorone();
                $imageData['landinganchorone'][0]['url'] = $this->getMediaUrl().$promotion->getLandinganchorone();
            }
            if ($promotion->getLandinganchortwo()) {
                $imageData['landinganchortwo'][0]['name'] = $promotion->getLandinganchortwo();
                $imageData['landinganchortwo'][0]['url'] = $this->getMediaUrl().$promotion->getLandinganchortwo();
            }
            if ($promotion->getPriceconfigfile()) {
                $imageData['priceconfigfile'][0]['name'] = $promotion->getPriceconfigfile();
                $imageData['priceconfigfile'][0]['size'] = $this->getFileSize($promotion->getPriceconfigfile());
                $imageData['priceconfigfile'][0]['url'] = $this->getMediaUrl().$promotion->getPriceconfigfile();
            }

            $this->loadedData[$promotion->getId()] = array_merge($fullData[$promotion->getId()], $imageData);
            $collection = $this->pcTermscollection->create()->addFieldToFilter('promo_code', $promotion->getPromoCode());
            $items      = $collection->getItems();
            if (count($items) > 0) {
                foreach ($items as $item) {
                    $this->loadedData[$promotion->getId()]['professional_category_and_terms'][] = $item->getData();
                }
            }
        }
        $data = $this->dataPersistor->get('mms_promotions_promotion');
        if (!empty($data)) {
            $promotion = $this->collection->getNewEmptyItem();
            $promotion->setData($data);
            $this->loadedData[$promotion->getId()] = $promotion->getData();
            $this->dataPersistor->clear('mms_promotions_promotion');
        }
        return $this->loadedData;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMediaUrl()
    {
        $mediaUrl = $this->storeManager->getStore()
            ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA).'promotions';
        return $mediaUrl;
    }

    /**
     * @param $filename
     * @return int|mixed
     */
    public function getFileSize($filename){
        $fullFileName = sprintf('promotions%s', $filename);
        $statResults = $this->_mediaDirectory->stat($fullFileName);
        return is_array($statResults) ? $statResults['size'] : -1;
    }
}
