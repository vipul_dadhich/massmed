<?php
namespace MMS\Promotions\Model\ResourceModel;

use Magento\Framework\Event\ManagerInterface as EventManagerInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Promotion
 * @package MMS\Promotions\Model\ResourceModel
 */
class PcTerms extends AbstractDb
{
    /**
     * Event Manager
     *
     * @var EventManagerInterface
     */
    protected $eventManager;

    /**
     * constructor
     *
     * @param Context $context
     * @param EventManagerInterface $eventManager
     * @param mixed $connectionName
     */
    public function __construct(
        Context $context,
        EventManagerInterface $eventManager,
        $connectionName = null
    ) {
        $this->eventManager = $eventManager;
        parent::__construct($context, $connectionName);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('mms_promotions_pc_terms_relation', 'entity_id');
    }

    /**
     * @param AbstractModel $object
     * @param array $attribute
     * @return $this
     * @throws \Exception
     */
    public function saveAttribute(AbstractModel $object, $attribute)
    {
        if (is_string($attribute)) {
            $attributes = [$attribute];
        } else {
            $attributes = $attribute;
        }
        if (is_array($attributes) && !empty($attributes)) {
            $this->getConnection()->beginTransaction();
            $data = array_intersect_key($object->getData(), array_flip($attributes));
            try {
                $this->beforeSaveAttribute($object, $attributes);
                if ($object->getId() && !empty($data)) {
                    $this->getConnection()->update(
                        $object->getResource()->getMainTable(),
                        $data,
                        [$object->getResource()->getIdFieldName() . '= ?' => (int)$object->getId()]
                    );
                    $object->addData($data);
                }
                $this->afterSaveAttribute($object, $attributes);
                $this->getConnection()->commit();
            } catch (\Exception $e) {
                $this->getConnection()->rollBack();
                throw $e;
            }
        }
        return $this;
    }

    /**
     * @param AbstractModel $object
     * @param array $attribute
     * @return $this
     */
    protected function beforeSaveAttribute(AbstractModel $object, $attribute)
    {
        if ($object->getEventObject() && $object->getEventPrefix()) {
            $this->eventManager->dispatch(
                $object->getEventPrefix() . '_save_attribute_before',
                [
                    $object->getEventObject() => $this,
                    'object' => $object,
                    'attribute' => $attribute
                ]
            );
        }
        return $this;
    }

    /**
     * After save object attribute
     *
     * @param AbstractModel $object
     * @param string $attribute
     * @return $this
     */
    protected function afterSaveAttribute(AbstractModel $object, $attribute)
    {
        if ($object->getEventObject() && $object->getEventPrefix()) {
            $this->eventManager->dispatch(
                $object->getEventPrefix() . '_save_attribute_after',
                [
                    $object->getEventObject() => $this,
                    'object' => $object,
                    'attribute' => $attribute
                ]
            );
        }
        return $this;
    }

    /**
     * before save callback
     *
     * @param AbstractModel|\MMS\Promotions\Model\PcTerms $object
     * @return $this
     */
    protected function _beforeSave(AbstractModel $object)
    {
        return parent::_beforeSave($object);
    }

    /**
     * @param AbstractModel $object
     * @return bool
     * @throws LocalizedException
     */
    public function checkIsPcTerms($promocode, $pcategory, $term, $magentoSku)
    {
        $collection = $this->getConnection();
        $select = $collection->select()
            ->from($this->getMainTable())
            ->where('promo_code = ?', $promocode)
            ->where('professional_category = ?', $pcategory)
            ->where('term = ?', $term)
            ->where('sku = ?', $magentoSku);
        $pcTerms = $collection->fetchAll($select);
            if (!empty($pcTerms)) {
                foreach($pcTerms as $p){
                    if ($p['entity_id']) {
                        return true;
                    }
                }
            }
        return false;
    }

    /**
     * @param AbstractModel $object
     * @return bool
     * @throws LocalizedException
     */
    public function deleteExisting($promoCode, $savedIds)
    {
        $connection = $this->getConnection();
        $tableName = $connection->getTableName('mms_promotions_pc_terms_relation');
        $where[] =  $connection->quoteInto('promo_code = ?', $promoCode);
        if ($promoCode) {
            if (!empty($savedIds)) {
                $query = "entity_id NOT IN (" .
                    implode(',', array_fill(0, count($savedIds), "?")). ")";
                foreach($savedIds as $v) {
                    $where[] = $this->getConnection()->quoteInto($query,$v);
                }
            }
            return $connection->delete($tableName, $where);
        }
    }

    /**
     * @param $id
     * @param $promocode
     * @param $pcategory
     * @param $term
     * @param $magentoSku
     * @return bool
     * @throws LocalizedException
     */
    public function checkIsPcTermsEditCheck($id, $promocode, $pcategory, $term, $magentoSku){
        $collection = $this->getConnection();
        $select = $collection->select()
            ->from($this->getMainTable())
            ->where('promo_code = ?', $promocode)
            ->where('professional_category = ?', $pcategory)
            ->where('term = ?', $term)
            ->where('entity_id != ?', $id)
            ->where('sku = ?', $magentoSku);
        $pcTerms = $collection->fetchAll($select);
        if (!empty($pcTerms)) {
            foreach($pcTerms as $p){
                if ($p['entity_id']) {
                    return true;
                }
            }
        }
        return false;
    }
}
