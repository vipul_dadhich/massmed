<?php
namespace MMS\Promotions\Model;

use MMS\Promotions\Api\Data\PromotionInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Promotion
 * @package MMS\Promotions\Model
 */
class Promotion extends AbstractModel implements PromotionInterface
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'mms_promotions_promotion';

    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'mms_promotions_promotion';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'promotion';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\MMS\Promotions\Model\ResourceModel\Promotion::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Promotion id
     *
     * @return array
     */
    public function getPromotionId()
    {
        return $this->getData(PromotionInterface::PROMOTION_ID);
    }

    /**
     * set Promotion id
     *
     * @param int $promotionId
     * @return PromotionInterface
     */
    public function setPromotionId($promotionId)
    {
        return $this->setData(PromotionInterface::PROMOTION_ID, $promotionId);
    }

    /**
     * set Campaign Code
     *
     * @param mixed $campaignCode
     * @return PromotionInterface
     */
    public function setCampaignCode($campaignCode)
    {
        return $this->setData(PromotionInterface::CAMPAIGN_CODE, $campaignCode);
    }

    /**
     * get Campaign Code
     *
     * @return string
     */
    public function getCampaignCode()
    {
        return $this->getData(PromotionInterface::CAMPAIGN_CODE);
    }

    /**
     * set Promotion Code
     *
     * @param mixed $promoCode
     * @return PromotionInterface
     */
    public function setPromoCode($promoCode)
    {
        return $this->setData(PromotionInterface::PROMO_CODE, $promoCode);
    }

    /**
     * get Promotion Code
     *
     * @return string
     */
    public function getPromoCode()
    {
        return $this->getData(PromotionInterface::PROMO_CODE);
    }

    /**
     * set Promotion Description
     *
     * @param mixed $promoDescription
     * @return PromotionInterface
     */
    public function setPromoDescription($promoDescription)
    {
        return $this->setData(PromotionInterface::PROMO_DESCRIPTION, $promoDescription);
    }

    /**
     * get Promotion Description
     *
     * @return string
     */
    public function getPromoDescription()
    {
        return $this->getData(PromotionInterface::PROMO_DESCRIPTION);
    }

    /**
     * set Cart Anchor One
     *
     * @param mixed $cartanchorone
     * @return PromotionInterface
     */
    public function setCartanchorone($cartanchorone)
    {
        return $this->setData(PromotionInterface::CART_ANCHOR_ONE, $cartanchorone);
    }

    /**
     * get Cart Anchor One
     *
     * @return string
     */
    public function getCartanchorone()
    {
        return $this->getData(PromotionInterface::CART_ANCHOR_ONE);
    }

    /**
     * set Cart Anchor Two
     *
     * @param mixed $cartanchortwo
     * @return PromotionInterface
     */
    public function setCartanchortwo($cartanchortwo)
    {
        return $this->setData(PromotionInterface::CART_ANCHOR_TWO, $cartanchortwo);
    }

    /**
     * get Cart Anchor Two
     *
     * @return string
     */
    public function getCartanchortwo()
    {
        return $this->getData(PromotionInterface::CART_ANCHOR_TWO);
    }

    /**
     * set Checkout Anchor One
     *
     * @param mixed $checkoutanchorone
     * @return PromotionInterface
     */
    public function setCheckoutanchorone($checkoutanchorone)
    {
        return $this->setData(PromotionInterface::CHECKOUT_ANCHOR_ONE, $checkoutanchorone);
    }

    /**
     * get Checkout Anchor One
     *
     * @return string
     */
    public function getCheckoutanchorone()
    {
        return $this->getData(PromotionInterface::CHECKOUT_ANCHOR_ONE);
    }

    /**
     * set Checkout Anchor Two
     *
     * @param mixed $checkoutanchortwo
     * @return PromotionInterface
     */
    public function setCheckoutanchortwo($checkoutanchortwo)
    {
        return $this->setData(PromotionInterface::CHECKOUT_ANCHOR_TWO, $checkoutanchortwo);
    }

    /**
     * get Checkout Anchor Two
     *
     * @return string
     */
    public function getCheckoutanchortwo()
    {
        return $this->getData(PromotionInterface::CHECKOUT_ANCHOR_TWO);
    }

    /**
     * set Confirmation Anchor One
     *
     * @param mixed $confirmationanchorone
     * @return PromotionInterface
     */
    public function setConfirmationanchorone($confirmationanchorone)
    {
        return $this->setData(PromotionInterface::CONFIRMATION_ANCHOR_ONE, $confirmationanchorone);
    }

    /**
     * get Confirmation Anchor One
     *
     * @return string
     */
    public function getConfirmationanchorone()
    {
        return $this->getData(PromotionInterface::CONFIRMATION_ANCHOR_ONE);
    }

    /**
     * set Confirmation Anchor Two
     *
     * @param mixed $confirmationanchortwo
     * @return PromotionInterface
     */
    public function setConfirmationanchortwo($confirmationanchortwo)
    {
        return $this->setData(PromotionInterface::CONFIRMATION_ANCHOR_TWO, $confirmationanchortwo);
    }

    /**
     * get Confirmation Anchor Two
     *
     * @return string
     */
    public function getConfirmationanchortwo()
    {
        return $this->getData(PromotionInterface::CONFIRMATION_ANCHOR_TWO);
    }

    /**
     * set Landing Page Anchor one
     *
     * @param mixed $landingPageanchorone
     * @return PromotionInterface
     */
    public function setLandinganchorone($landingPageanchorone)
    {
        return $this->setData(PromotionInterface::LANDING_ANCHOR_ONE, $landingPageanchorone);
    }

    /**
     * get Landing Page Anchor One
     *
     * @return string
     */
    public function getLandinganchorone()
    {
        return $this->getData(PromotionInterface::LANDING_ANCHOR_ONE);
    }

    /**
     * set Landing Page Anchor Two
     *
     * @param mixed $landingPageanchortwo
     * @return PromotionInterface
     */
    public function setLandinganchortwo($landingPageanchortwo)
    {
        return $this->setData(PromotionInterface::LANDING_ANCHOR_TWO, $landingPageanchortwo);
    }

    /**
     * get Landing Page Anchor Two
     *
     * @return string
     */
    public function getLandinganchortwo()
    {
        return $this->getData(PromotionInterface::LANDING_ANCHOR_TWO);
    }

    /**
     * set Price Config File
     *
     * @param mixed $priceconfigfile
     * @return PromotionInterface
     */
    public function setPriceconfigfile($priceconfigfile)
    {
        return $this->setData(PromotionInterface::PRICE_CONFIG_FILE, $priceconfigfile);
    }

    /**
     * get Price Config File
     *
     * @return string
     */
    public function getPriceconfigfile()
    {
        return $this->getData(PromotionInterface::PRICE_CONFIG_FILE);
    }

    /**
     * set Free Gift
     *
     * @param mixed $freeGiftSku
     * @return PromotionInterface
     */
    public function setFreeGiftSku($freeGiftSku)
    {
        return $this->setData(PromotionInterface::FREE_GIFT_SKU, $freeGiftSku);
    }

    /**
     * get Free Gift
     *
     * @return string
     */
    public function getFreeGiftSku()
    {
        return $this->getData(PromotionInterface::FREE_GIFT_SKU);
    }

    /**
     * set Free Gift Eligible Skus
     *
     * @param mixed $freeGiftEligibleSkus
     * @return PromotionInterface
     */
    public function setFreeGiftEligibleSkus($freeGiftEligibleSkus)
    {
        return $this->setData(PromotionInterface::FREE_GIFT_ELIGIBLE_SKUS, $freeGiftEligibleSkus);
    }

    /**
     * get Free Gift Eligible Skus
     *
     * @return string
     */
    public function getFreeGiftEligibleSkus()
    {
        return $this->getData(PromotionInterface::FREE_GIFT_ELIGIBLE_SKUS);
    }

    /**
     * set Professional Category
     *
     * @param mixed $professionalCategory
     * @return PromotionInterface
     */
    public function setProfessionalCategory($professionalCategory)
    {
        return $this->setData(PromotionInterface::DEFAULT_PROFESSIONAL_CATEGORY, $professionalCategory);
    }

    /**
     * get Professional Category
     *
     * @return string
     */
    public function getProfessionalCategory()
    {
        return $this->getData(PromotionInterface::DEFAULT_PROFESSIONAL_CATEGORY);
    }

    /**
     * set Allowed Payment
     *
     * @param mixed $allowedPayments
     * @return PromotionInterface
     */
    public function setAllowedPayments($allowedPayments)
    {
        return $this->setData(PromotionInterface::ALLOWED_PAYMENT, $allowedPayments);
    }

    /**
     * get Allowed Payment
     *
     * @return string
     */
    public function getAllowedPayments()
    {
        return $this->getData(PromotionInterface::ALLOWED_PAYMENT);
    }

    /**
     * set Bind To Template
     *
     * @param mixed $bindedTemplate
     * @return PromotionInterface
     */
    public function setBindedTemplate($bindedTemplate)
    {
        return $this->setData(PromotionInterface::BINDED_TEMPLATE, $bindedTemplate);
    }

    /**
     * get Bind To Template
     *
     * @return string
     */
    public function getBindedTemplate()
    {
        return $this->getData(PromotionInterface::BINDED_TEMPLATE);
    }

    /**
     * set Live Date
     *
     * @param mixed $liveDate
     * @return PromotionInterface
     */
    public function setLiveDate($liveDate)
    {
        return $this->setData(PromotionInterface::LIVE_DATE, $liveDate);
    }

    /**
     * get Live Date
     *
     * @return string
     */
    public function getLiveDate()
    {
        return $this->getData(PromotionInterface::LIVE_DATE);
    }

    /**
     * set End Date
     *
     * @param mixed $endDate
     * @return PromotionInterface
     */
    public function setEndDate($endDate)
    {
        return $this->setData(PromotionInterface::END_DATE, $endDate);
    }

    /**
     * get End Date
     *
     * @return string
     */
    public function getEndDate()
    {
        return $this->getData(PromotionInterface::END_DATE);
    }
}
