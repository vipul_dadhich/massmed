<?php
namespace MMS\Promotions\Model;

use MMS\Promotions\Api\Data\PcTermsInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Promotion
 * @package MMS\Promotions\Model
 */
class PcTerms extends AbstractModel implements PcTermsInterface
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'mms_promotions_pc_terms_relation';

    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'mms_promotions_pc_terms_relation';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'pcterms';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\MMS\Promotions\Model\ResourceModel\PcTerms::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get id
     *
     * @return array
     */
    public function getEntityId()
    {
        return $this->getData(PcTermsInterface::ID);
    }

    /**
     * set id
     *
     * @param int $Id
     * @return PcTermsInterface
     */
    public function setPromotionId($id)
    {
        return $this->setData(PcTermsInterface::ID, $id);
    }

    /**
     * set Campaign Code
     *
     * @param mixed $campaignCode
     * @return PcTermsInterface
     */
    public function setCampaignCode($campaignCode)
    {
        return $this->setData(PcTermsInterface::CAMPAIGN_CODE, $campaignCode);
    }

    /**
     * get Campaign Code
     *
     * @return string
     */
    public function getCampaignCode()
    {
        return $this->getData(PcTermsInterface::CAMPAIGN_CODE);
    }

    /**
     * set Promotion Code
     *
     * @param mixed $promoCode
     * @return PcTermsInterface
     */
    public function setPromoCode($promoCode)
    {
        return $this->setData(PcTermsInterface::PROMO_CODE, $promoCode);
    }

    /**
     * get Promotion Code
     *
     * @return string
     */
    public function getPromoCode()
    {
        return $this->getData(PcTermsInterface::PROMO_CODE);
    }

    /**
     * set Professional Category
     *
     * @param mixed $professionalCategory
     * @return PcTermsInterface
     */
    public function setProfessionalCategory($professionalCategory)
    {
        return $this->setData(PcTermsInterface::PROFESSIONAL_CATEGORY, $professionalCategory);
    }

    /**
     * get Professional Category
     *
     * @return string
     */
    public function getProfessionalCategory()
    {
        return $this->getData(PcTermsInterface::PROFESSIONAL_CATEGORY);
    }

    /**
     * set Term
     *
     * @param mixed $term
     * @return PcTermsInterface
     */
    public function setTerm($term)
    {
        return $this->setData(PcTermsInterface::TERM, $term);
    }

    /**
     * get Term
     *
     * @return string
     */
    public function getTerm()
    {
        return $this->getData(PcTermsInterface::TERM);
    }

    /**
     * set Sku
     *
     * @param mixed $sku
     * @return PcTermsInterface
     */
    public function setSku($sku)
    {
        return $this->setData(PcTermsInterface::SKU, $sku);
    }

    /**
     * get Sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->getData(PcTermsInterface::SKU);
    }
}
