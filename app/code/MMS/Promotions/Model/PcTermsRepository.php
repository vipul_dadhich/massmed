<?php
namespace MMS\Promotions\Model;

use Magento\Framework\Model\AbstractModel;
use MMS\Promotions\Api\Data\PcTermsInterface;
use MMS\Promotions\Api\Data\PcTermsInterfaceFactory;
use MMS\Promotions\Api\Data\PcTermsSearchResultInterfaceFactory;
use MMS\Promotions\Api\PcTermsRepositoryInterface;
use MMS\Promotions\Model\ResourceModel\PcTerms as PcTermsResourceModel;
use MMS\Promotions\Model\ResourceModel\PcTerms\Collection;
use MMS\Promotions\Model\ResourceModel\PcTerms\CollectionFactory as PcTermsCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;

/**
 * Class PcTermsRepository
 * @package MMS\Promotions\Model
 */
class PcTermsRepository implements PcTermsRepositoryInterface
{
    /**
     * Cached instances
     *
     * @var array
     */
    protected $instances = [];

    /**
     * PcTerms resource model
     *
     * @var PcTermsResourceModel
     */
    protected $resource;

    /**
     * PcTerms collection factory
     *
     * @var PcTermsCollectionFactory
     */
    protected $pcTermsCollectionFactory;

    /**
     * Promotion interface factory
     *
     * @var PcTermsInterfaceFactory
     */
    protected $pcTermsInterfaceFactory;

    /**
     * Data Object Helper
     *
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Search result factory
     *
     * @var PcTermsSearchResultInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * constructor
     *
     * @param PcTermsResourceModel $resource
     * @param PcTermsCollectionFactory $pcTermsCollectionFactory
     * @param PcTermsInterfaceFactory $pcTermsInterfaceFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param PcTermsSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        PcTermsResourceModel $resource,
        PcTermsCollectionFactory $pcTermsCollectionFactory,
        PcTermsInterfaceFactory $pcTermsInterfaceFactory,
        DataObjectHelper $dataObjectHelper,
        PcTermsSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->resource                   = $resource;
        $this->pcTermsCollectionFactory = $pcTermsCollectionFactory;
        $this->pcTermsInterfaceFactory  = $pcTermsInterfaceFactory;
        $this->dataObjectHelper           = $dataObjectHelper;
        $this->searchResultsFactory       = $searchResultsFactory;
    }

    /**
     * Save Promotion.
     *
     * @param \MMS\Promotions\Api\Data\PcTermsInterface $pcTerms
     * @return \MMS\Promotions\Api\Data\PcTermsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(PcTermsInterface $pcTerms)
    {
        /** @var PromotionInterface|AbstractModel $pcTerms */
        try {
            $this->resource->save($pcTerms);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Professional Category and Term: %1',
                $exception->getMessage()
            ));
        }
        return $pcTerms;
    }

    /**
     * Retrieve PcTerms.
     *
     * @param int $id
     */
    public function getById($id)
    {
        if (!isset($this->instances[$id])) {
            /** @var PcTermsInterfaceFactory|AbstractModel $pcTerms */
            $pcTerms = $this->pcTermsInterfaceFactory->create();
            $this->resource->load($pcTerms, $id);
            $this->instances[$id] = $pcTerms;
        }
        return $this->instances[$id];
    }

    /**
     * Retrieve PcTerms matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \MMS\Promotions\Api\Data\PcTermsSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \MMS\Promotions\Api\Data\PromotionSearchResultInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \MMS\Promotions\Model\ResourceModel\Promotion\Collection $collection */
        $collection = $this->promotionCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'promotion_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var PcTermsInterface[] $pcTerms */
        $promotions = [];
        /** @var \MMS\Promotions\Model\PcTerms $pcTerm */
        foreach ($collection as $pcTerm) {
            /** @var PromotionInterface $promotionDataObject */
            $pcTermsDataObject = $this->pcTermsInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $promotionDataObject,
                $pcTerm->getData(),
                PcTermsInterface::class
            );
            $pcTerms[] = $pcTermsDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($pcTerms);
    }

    /**
     * Delete PcTerms.
     *
     * @param PcTermsInterface $pcTerms
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(PcTermsInterface $pcTerms)
    {
        /** @var PcTermsInterface|AbstractModel $pcTerms */
        $id = $pcTerms->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($pcTerms);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to remove PC and Terms %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete PcTerms by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id)
    {
        $pcTerms = $this->getById($id);
        return $this->delete($pcTerms);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(
        FilterGroup $filterGroup,
        Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
