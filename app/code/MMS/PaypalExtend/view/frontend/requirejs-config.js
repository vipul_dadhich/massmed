var config = {
    map: {
        '*': {
            'Magento_Paypal/template/payment/paypal-express-in-context.html':
                'MMS_PaypalExtend/template/paypal/paypal-express-in-context.html',
            'Magento_Paypal/js/view/payment/method-renderer/in-context/checkout-express':
                'MMS_PaypalExtend/js/view/payment/method-renderer/in-context/checkout-express',
            'Magento_Paypal/js/in-context/express-checkout-smart-buttons':
                'MMS_PaypalExtend/js/in-context/express-checkout-smart-buttons',
        }
    }
};
