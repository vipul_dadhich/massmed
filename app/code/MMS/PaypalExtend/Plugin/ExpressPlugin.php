<?php

namespace MMS\PaypalExtend\Plugin;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Paypal\Model\Express;
use MMS\PaypalExtend\Helper\Data;

/**
 * Class ExpressPlugin
 * @package MMS\PaypalExtend\Plugin
 */
class ExpressPlugin
{
    /**
     * @var Data
     */
    protected $_helper;
    /**
     * @var string[]
     */
    protected $autoRenewalLanguageStatus = ['SUSPENDED', 'BILLED'];

    public function __construct(
        Data $helper
    ) {
        $this->_helper = $helper;
    }

    /**
     * @param Express $subject
     * @param $result
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function afterGetTitle(Express $subject, $result) {
        if ($this->_helper->isPaybillEnabled()
            && $this->_helper->getCheckoutSession()->getQuote()->getData('is_paybill')
        ) {
            $result = __('PayPal');
            if (!in_array($this->_helper->getCheckoutSession()->getQuote()->getData('acs_order_status'), $this->autoRenewalLanguageStatus)) {
                $result = __('PayPal with Automatic Renewal');
            }
        }

        return $result;
    }
}
