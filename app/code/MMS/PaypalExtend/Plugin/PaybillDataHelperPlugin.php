<?php

namespace MMS\PaypalExtend\Plugin;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use MMS\PaypalExtend\Helper\Data;

/**
 * Class MuleSoftDataHelperPlugin
 * @package MMS\PaypalExtend\Plugin
 */
class PaybillDataHelperPlugin
{
    const PAYMENT_CODE = 'paypal_express';

    /**
     * @var Data
     */
    protected $_dataHelper;
    /**
     * @var TimezoneInterface
     */
    protected $_timezone;

    /**
     * MuleSoftDataHelperPlugin constructor.
     * @param Data $dataHelper
     * @param TimezoneInterface $timezone
     */
    public function __construct(
        Data $dataHelper,
        TimezoneInterface $timezone
    ) {
        $this->_dataHelper = $dataHelper;
        $this->_timezone = $timezone;
    }

    /**
     * @param \MMS\Paybill\Helper\Data $subject
     * @param \Closure $proceed
     * @param $order
     * @return mixed
     */
    public function aroundGetPaymentPayload(
        \MMS\Paybill\Helper\Data $subject,
        \Closure $proceed,
        $order
    ) {
        $result = $proceed($order);
        if ($order->getPayment()->getMethodInstance()->getCode() == self::PAYMENT_CODE) {
            $digitalWalletInfo = $this->getPaymentDigitalWalletInfo($order);
            $result['digitalWalletInfo'] = $digitalWalletInfo;
            if (isset($result['creditCardInfo'])) {
            	unset($result['creditCardInfo']);
            }
        }
        return $result;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    protected function getPaymentDigitalWalletInfo(\Magento\Sales\Model\Order $order)
    {
        $billingAgreementId = $this->_dataHelper->getLastBillingAgreementReferenceId();
        if (!$billingAgreementId) {
            $billingAgreementId = $this->_dataHelper->getBillingAgreementIdsByOrder($order);
        }

        $status = "";
        $orderDate = $order->getCreatedAt();
        $additional = $order->getPayment()->getAdditionalInformation();
        if (isset($additional['paypal_pending_reason'])
            && $additional['paypal_pending_reason'] == 'authorization') {
            $status = "Authorized";
        }
        $transaction = $order->getPayment()->getAuthorizationTransaction();
        if ($transaction) {
            $orderDate = $transaction->getCreatedAt();
            $status = "Authorized";
        }

        $digitalWalletInfo = [];
        $digitalWalletInfo['billingAgreementId'] = $billingAgreementId;
        $digitalWalletInfo['transactionId'] = $order->getPayment()->getLastTransId();
        $digitalWalletInfo['paymentDate'] = $this->getFormattedDate($orderDate);
        $digitalWalletInfo['status'] = $status;
        $digitalWalletInfo['walletType'] = 'PAYPAL';

        return $digitalWalletInfo;
    }

    /**
     * @param $date
     * @param string $format
     * @return string
     */
    public function getFormattedDate($date, $format = 'Y-m-d\TH:i:s\Z')
    {
        return $this->_timezone->date(
            new \DateTime($date)
        )->format($format);
    }
}
