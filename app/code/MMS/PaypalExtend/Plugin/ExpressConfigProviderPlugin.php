<?php

namespace MMS\PaypalExtend\Plugin;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Paypal\Model\ExpressConfigProvider;
use MMS\PaypalExtend\Helper\Data;

/**
 * Class ExpressConfigProviderPlugin
 * @package MMS\PaypalExtend\Plugin
 */
class ExpressConfigProviderPlugin
{
    /**
     * @var Data
     */
    protected $_helper;
    /**
     * @var string[]
     */
    protected $autoRenewalLanguageStatus = ['SUSPENDED', 'BILLED'];

    public function __construct(
        Data $helper
    ) {
        $this->_helper = $helper;
    }

    /**
     * @param ExpressConfigProvider $subject
     * @param array $result
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function afterGetConfig(ExpressConfigProvider $subject, array $result)
    {
        if ($this->_helper->isPaybillEnabled()
            && $this->_helper->getCheckoutSession()->getQuote()->getData('is_paybill')
        ) {
            $result['payment']['paypalExpress']['title'] = __('PayPal');
            if (!in_array($this->_helper->getCheckoutSession()->getQuote()->getData('acs_order_status'), $this->autoRenewalLanguageStatus)) {
                $result['payment']['paypalExpress']['title'] = __('PayPal with Automatic Renewal');
            }
        }
        return $result;
    }

}
