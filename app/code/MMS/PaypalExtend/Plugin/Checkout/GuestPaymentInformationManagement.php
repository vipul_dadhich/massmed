<?php

namespace MMS\PaypalExtend\Plugin\Checkout;

/**
 * Class GuestPaymentInformationManagement
 * @package MMS\PaypalExtend\Plugin\Checkout
 */
class GuestPaymentInformationManagement
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * @var \Magento\Framework\Webapi\Rest\Request
     */
    protected $request;

    /**
     * GuestPaymentInformationManagement constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Webapi\Rest\Request $request
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Webapi\Rest\Request $request
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->request = $request;
    }
    public function beforeSavePaymentInformation(
        \Magento\Checkout\Model\GuestPaymentInformationManagement $subject,
        $cartId,
        $email,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        $bodyParams = $this->request->getBodyParams();
        if (isset($bodyParams['paymentMethod']['method']) && $bodyParams['paymentMethod']['method'] == 'paypal_express') {
            if (isset($bodyParams['billingAddress']['extension_attributes'])) {
                $this->checkoutSession->setPiInformationSession($bodyParams['billingAddress']['extension_attributes']);
            } else if (isset($bodyParams['billingAddress']['customAttributes'])) {
                $customAttributes = [];
                foreach ($bodyParams['billingAddress']['customAttributes'] as $attribute) {
                    $customAttributes[$attribute['attribute_code']] = $attribute['value'];
                }
                $this->checkoutSession->setPiInformationSession($customAttributes);
            }
        }
    }
}
