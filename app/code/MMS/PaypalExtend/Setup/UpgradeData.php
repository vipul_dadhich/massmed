<?php

namespace MMS\PaypalExtend\Setup;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Cms\Model\BlockFactory;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Store\Model\Store;

/**
 * Class UpgradeData
 * @package MMS\PaypalExtend\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var BlockFactory
     */
    private $blockFactory;
    /**
     * @var ConfigInterface
     */
    protected $resourceConfig;

    /**
     * UpgradeData constructor.
     * @param BlockFactory $blockFactory
     * @param ConfigInterface $resourceConfig
     */
    public function __construct(
        BlockFactory $blockFactory,
        ConfigInterface $resourceConfig
    ) {
        $this->blockFactory = $blockFactory;
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {

        $setup->startSetup();

        if (version_compare($context->getVersion(), '2.1.0') < 0) {
            try{
                $staticBlockInfo = [
                    'title' => 'AutoRenewal Paypal Express',
                    'identifier' => 'paypal_express',
                    'stores' => ['0'],
                    'is_active' => 1,
                    'content' => $this->getCmsBlockContent()
                ];
                $this->blockFactory->create()->setData($staticBlockInfo)->save();
            }catch (\Exception $e){

            }
        }

        if (version_compare($context->getVersion(), '2.1.1') < 0) {
            $this->resourceConfig->saveConfig(
                'payment/paypal_express/title',
                'PayPal with Automatic Renewal',
                ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                Store::DEFAULT_STORE_ID
            );
        }

        $setup->endSetup();
    }

    protected function getCmsBlockContent() {
        $html = '<div style="background-color:#F9F3D9">';
        $html .= '<p><b>AUTOMATIC RENEWAL CONVENIENCE</b></p>';
        $html .= '<p>By accepting this offer, you are agreeing to a <b>continuous subscription</b>';
        $html .= 'to <i>NEJM Catalyst Innovations in Care Delivery</i> without interruption.  ';
        $html .= 'At the end of your initial term and before the start of each new term, ';
        $html .= 'we\'ll simply charge your current payment method at the prevailing rate. ';
        $html .= 'A notice will be sent to you reminding you of your impending renewal. ';
        $html .= 'If at any point, you wish to be removed from the program, simply contact customer service: ';
        $html .= '<br>U.S. & Canada 800-843-6356 (8am - 4pm ET Mon-Fri) or <a href="mailto:nejmcust@mms.org">nejmcust@mms.org</a> ';
        $html .= '<br>Outside U.S. & Canada +1-781-434-7888 (8am - 4pm ET Mon-Fri) or ';
        $html .= '<a href="nejmintlcust@mms.org">nejmintlcust@mms.org</a>';
        $html .= '</p>';
        $html .= '<p>{{block class="Magento\Framework\View\Element\Template" methodcode="paypal_express" title="I agree to the Automatic Renewal Convenience terms and conditions." template="Nejm_Custom::checkbox.phtml"}}</p>';
        $html .= '<div>';

        return $html;
    }
}
