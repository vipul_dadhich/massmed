<?php

namespace MMS\PaypalExtend\Model\Api;

use Magento\Framework\DataObject;

/**
 * Class Nvp
 * @package MMS\PaypalExtend\Model\Api
 */
class Nvp extends \Magento\Paypal\Model\Api\Nvp
{
    /**
     * Adopt specified address object to be compatible with Magento
     *
     * @param DataObject $address
     * @return void
     */
    protected function _applyStreetAndRegionWorkarounds(DataObject $address)
    {
        // merge street addresses into 1
        if ($address->getData('street2') !== null) {
            $address->setStreet(implode("\n", [$address->getData('street'), $address->getData('street2')]));
            $address->unsetData('street2');
        }
        // attempt to fetch region_id from directory
        if ($address->getCountryId() && $address->getRegion()) {
            $regions = $this->_countryFactory->create()->loadByCode(
                $address->getCountryId()
            )->getRegionCollection()->addRegionCodeOrNameFilter(
                $address->getRegion()
            )->setPageSize(
                1
            );

            if (count($regions) > 0) {
                foreach ($regions as $region) {
                    $address->setRegionId($region->getId());
                    $address->setExportedKeys(array_merge($address->getExportedKeys(), ['region_id']));
                    break;
                }
            }
        }
    }

    /**
     * Updates shipping address with 'ship to name' data
     *
     * @param DataObject $shippingAddress
     * @param array $data
     * @return void
     */
    private function updateShippingAddressWithShipToName(DataObject $shippingAddress, array $data)
    {
        if (isset($data['SHIPTONAME'])) {
            $nameParts = explode(' ', $data['SHIPTONAME'], 2);
            $shippingAddress->addData(['firstname' => $nameParts[0]]);

            if (isset($nameParts[1])) {
                $shippingAddress->addData(['lastname' => $nameParts[1]]);
            }
        }
    }

    /**
     * Do the API call
     *
     * @param string $methodName
     * @param array $request
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function call($methodName, array $request)
    {
        $request['NOSHIPPING'] = 1;
        return parent::call($methodName, $request);
    }

    /**
     * Prepare request data basing on provided addresses
     *
     * @param array $to
     * @return array
     */
    protected function _importAddresses(array $to)
    {
        $billingAddress = $this->getBillingAddress() ? $this->getBillingAddress() : $this->getAddress();

        $to = \Magento\Framework\DataObject\Mapper::accumulateByMap(
            $billingAddress,
            $to,
            array_merge(array_flip($this->_billingAddressMap), $this->_billingAddressMapRequest)
        );
        $regionCode = $this->_lookupRegionCodeFromAddress($billingAddress);
        if ($regionCode) {
            $to['STATE'] = $regionCode;
        }
        return $to;
    }
}
