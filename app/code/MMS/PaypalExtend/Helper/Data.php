<?php

namespace MMS\PaypalExtend\Helper;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Paypal\Model\ResourceModel\Billing\Agreement\Collection;
use Magento\Paypal\Model\ResourceModel\Billing\Agreement\CollectionFactory;
use MMS\Renew\Helper\Config;
use MMS\Paybill\Helper\Data as PaybillHelper;

/**
 * Class Data
 * @package MMS\PaypalExtend\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var Session
     */
    protected $_checkoutSession;
    /**
     * @var Config
     */
    protected $_renewConfig;
    /**
     * @var PaybillHelper
     */
    protected $_paybillHelper;
    /**
     * @var CollectionFactory
     */
    protected $_agreementCollection;

    /**
     * Data constructor.
     * @param Context $context
     * @param Config $config
     * @param PaybillHelper $paybillHelper
     * @param Session $checkoutSession
     * @param CollectionFactory $agreementCollection
     */
    public function __construct(
        Context $context,
        Config $config,
        PaybillHelper $paybillHelper,
        Session $checkoutSession,
        CollectionFactory $agreementCollection
    ) {
        $this->_renewConfig = $config;
        $this->_paybillHelper = $paybillHelper;
        $this->_checkoutSession = $checkoutSession;
        $this->_agreementCollection = $agreementCollection;
        parent::__construct($context);
    }

    /**
     * @return string
     */
    public function isRenewEnabled() {
        return $this->_renewConfig->isEnabled();
    }

    /**
     * @return string
     */
    public function isPaybillEnabled() {
        return $this->_paybillHelper->isEnabled();
    }

    /**
     * Get frontend checkout session object
     *
     * @return Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    /**
     * @return mixed
     */
    public function getLastBillingAgreementReferenceId()
    {
        return $this->_checkoutSession->getLastBillingAgreementReferenceId();
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return Collection
     */
    public function getBillingAgreementsByOrder(\Magento\Sales\Model\Order $order)
    {
        $collection = $this->_agreementCollection->create()
            ->addFieldToFilter('method_code', 'paypal_billing_agreement')
            ->setOrder('agreement_id', 'desc');

        if ($order->getCustomerId()) {
            $collection->addFieldToFilter('customer_id', $order->getCustomerId());
            $collection->addFieldToFilter('status', 'active');
        } else {
            $collection->getSelect()->joinLeft(
                ['pbao'=> 'paypal_billing_agreement_order'],
                "main_table.agreement_id = pbao.agreement_id",
                ['order_id' => 'pbao.order_id']
            )->where(
                'pbao.order_id = ?',
                $order->getId()
            );
        }

        return $collection;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return string
     */
    public function getBillingAgreementIdsByOrder(\Magento\Sales\Model\Order $order)
    {
        $agreementIds = [];
        $collection = $this->getBillingAgreementsByOrder($order);
        if ($collection->getSize() > 0) {
            foreach($collection as $agreement) {
                $agreementIds[] = $agreement->getReferenceId();
            }
        }

        return implode(",", $agreementIds);
    }
}
