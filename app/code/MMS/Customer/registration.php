<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_Customer extension
 * NOTICE OF LICENSE
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'MMS_Customer',
    __DIR__
);
