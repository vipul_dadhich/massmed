<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_Customer extension
 * NOTICE OF LICENSE
 */
namespace MMS\Customer\Setup;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class UpgradeData
 * @package MMs\Customer\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;
    /**
     * UpgradeData constructor.
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            if (!$customerSetup->getAttributeId(Customer::ENTITY, 'professional_category')) {
                $customerSetup->addAttribute(
                    Customer::ENTITY,
                    'professional_category',
                    [
                        'type' => 'varchar',
                        'label' => 'Professional Category',
                        'input' => 'select',
                        'required' => false,
                        'visible' => true,
                        'source' => 'MMS\Customer\Model\Source\ProfessionalCategory',
                        'backend' => '',
                        'user_defined' => false,
                        'is_user_defined' => false,
                        'sort_order' => 1000,
                        'is_used_in_grid' => false,
                        'is_visible_in_grid' => false,
                        'is_filterable_in_grid' => false,
                        'is_searchable_in_grid' => false,
                        'position' => 1000,
                        'default' => '',
                        'system' => '',
                    ]
                );
                $attribute = $customerSetup->getEavConfig()
                    ->getAttribute(Customer::ENTITY, 'professional_category')
                    ->addData([
                        'used_in_forms' => ['adminhtml_customer'],
                    ]);
                $attribute->save();
            }
        }
        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            if (!$customerSetup->getAttributeId(Customer::ENTITY, 'student_type')) {
                $customerSetup->addAttribute(
                    Customer::ENTITY,
                    'student_type',
                    [
                        'type' => 'varchar',
                        'label' => 'Student Type',
                        'input' => 'select',
                        'required' => false,
                        'visible' => true,
                        'source' => 'MMS\Customer\Model\Source\StudentType',
                        'backend' => '',
                        'user_defined' => false,
                        'is_user_defined' => false,
                        'sort_order' => 1000,
                        'is_used_in_grid' => false,
                        'is_visible_in_grid' => false,
                        'is_filterable_in_grid' => false,
                        'is_searchable_in_grid' => false,
                        'position' => 1000,
                        'default' => '',
                        'system' => '',
                    ]
                );
                $attribute = $customerSetup->getEavConfig()
                    ->getAttribute(Customer::ENTITY, 'student_type')
                    ->addData([
                        'used_in_forms' => ['adminhtml_customer'],
                    ]);
                $attribute->save();
            }
            if (!$customerSetup->getAttributeId(Customer::ENTITY, 'profession')) {
                $customerSetup->addAttribute(
                    Customer::ENTITY,
                    'profession',
                    [
                        'type' => 'varchar',
                        'label' => 'Profession',
                        'input' => 'select',
                        'required' => false,
                        'visible' => true,
                        'source' => 'MMS\Customer\Model\Source\Profession',
                        'backend' => '',
                        'user_defined' => false,
                        'is_user_defined' => false,
                        'sort_order' => 1000,
                        'is_used_in_grid' => false,
                        'is_visible_in_grid' => false,
                        'is_filterable_in_grid' => false,
                        'is_searchable_in_grid' => false,
                        'position' => 1000,
                        'default' => '',
                        'system' => '',
                    ]
                );
                $attribute = $customerSetup->getEavConfig()
                    ->getAttribute(Customer::ENTITY, 'profession')
                    ->addData([
                        'used_in_forms' => ['adminhtml_customer'],
                    ]);
                $attribute->save();
            }
        }
        if (version_compare($context->getVersion(), '1.0.6') < 0) {
            if ($customerSetup->getAttributeId(Customer::ENTITY, 'media_access')) {
                $customerSetup->removeAttribute(Customer::ENTITY, 'media_access');
            }
            if (!$customerSetup->getAttributeId(Customer::ENTITY, 'media_access')) {
                $customerSetup->addAttribute(
                    Customer::ENTITY,
                    'media_access',
                    [
                        'type' => 'varchar',
                        'label' => 'Media Access',
                        'input' => 'select',
                        'required' => false,
                        'visible' => true,
                        'source' => 'MMS\Customer\Model\Source\MediaAccess',
                        'backend' => '',
                        'user_defined' => false,
                        'is_user_defined' => false,
                        'sort_order' => 1000,
                        'is_used_in_grid' => false,
                        'is_visible_in_grid' => false,
                        'is_filterable_in_grid' => false,
                        'is_searchable_in_grid' => false,
                        'position' => 1000,
                        'default' => '',
                        'system' => '',
                    ]
                );
                $attribute = $customerSetup->getEavConfig()
                    ->getAttribute(Customer::ENTITY, 'media_access')
                    ->addData([
                        'used_in_forms' => ['adminhtml_customer'],
                    ]);
                $attribute->save();
            }
        }
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            if (!$customerSetup->getAttributeId(Customer::ENTITY, 'nejmsoi')) {
                $customerSetup->addAttribute(
                    Customer::ENTITY,
                    'nejmsoi',
                    [
                        'type' => 'varchar',
                        'label' => 'NEJM SOI',
                        'input' => 'boolean',
                        'required' => false,
                        'visible' => true,
                        'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                        'backend' => '',
                        'user_defined' => false,
                        'is_user_defined' => false,
                        'sort_order' => 1000,
                        'is_used_in_grid' => false,
                        'is_visible_in_grid' => false,
                        'is_filterable_in_grid' => false,
                        'is_searchable_in_grid' => false,
                        'position' => 1000,
                        'default' => '',
                        'system' => '',
                    ]
                );
                $attribute = $customerSetup->getEavConfig()
                    ->getAttribute(Customer::ENTITY, 'nejmsoi')
                    ->addData([
                        'used_in_forms' => ['adminhtml_customer'],
                    ]);
                $attribute->save();
            }
            if (!$customerSetup->getAttributeId(Customer::ENTITY, 'nejmetoc')) {
                $customerSetup->addAttribute(
                    Customer::ENTITY,
                    'nejmetoc',
                    [
                        'type' => 'varchar',
                        'label' => 'NEJM ETOC',
                        'input' => 'boolean',
                        'required' => false,
                        'visible' => true,
                        'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                        'backend' => '',
                        'user_defined' => false,
                        'is_user_defined' => false,
                        'sort_order' => 1000,
                        'is_used_in_grid' => false,
                        'is_visible_in_grid' => false,
                        'is_filterable_in_grid' => false,
                        'is_searchable_in_grid' => false,
                        'position' => 1000,
                        'default' => '',
                        'system' => '',
                    ]
                );
                $attribute = $customerSetup->getEavConfig()
                    ->getAttribute(Customer::ENTITY, 'nejmetoc')
                    ->addData([
                        'used_in_forms' => ['adminhtml_customer'],
                    ]);
                $attribute->save();
            }
        }
        $setup->endSetup();
    }
}
