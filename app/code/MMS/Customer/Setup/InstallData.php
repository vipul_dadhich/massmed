<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_Customer extension
 * NOTICE OF LICENSE
 */
namespace MMs\Customer\Setup; 
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\InstallDataInterface;

class InstallData implements InstallDataInterface
{
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * @var
     */
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory, CustomerSetupFactory $customerSetupFactory) 
    {
       $this->eavSetupFactory = $eavSetupFactory;
       $this->customerSetupFactory = $customerSetupFactory;
    }

   /**
    * {@inheritdoc}
    * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
    */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();


        $customerSetup->addAttribute(Customer::ENTITY, 'customer_default_country', [
            'type' => 'varchar',
            'label' => 'Country',
            'input' => 'select',
            'required' => false,
            'visible' => true,
            'source' => 'Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Country',
            'backend' => '',
            'user_defined' => false,
            'is_user_defined' => false,
            'sort_order' => 1000,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'is_searchable_in_grid' => false,
            'position' => 1000,
            'default' => 0,
            'system' => 0,
         ]);

         $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'customer_default_country')
            ->addData([
                 'used_in_forms' => ['adminhtml_customer'],
          ]);

         $attribute->save();
	}
}
