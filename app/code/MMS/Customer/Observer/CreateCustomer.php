<?php
/**
 * MMS_Customer
 */
namespace MMS\Customer\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class CreateCustomer
 * @package MMS\Customer\Observer
 */
class CreateCustomer implements ObserverInterface
{
    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;
    /**
     * @var \Ey\MuleSoft\Helper\Data
     */
    protected $helper;
    /**
     * @var \Ey\MuleSoft\Model\Core\Logger\Monolog
     */
    protected $mulesoftLogger;
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;
    /**
     * @var mixed
     */
    protected $isDebugMode;
    /**
     * @var boolean
     */
    protected $isDebugModeType;
    /**
     * @var \Ey\MuleSoft\Model\Catalyst\Api\GetCustomer
     */
    protected $getCustomerApi;
    /**
     * @var \Ey\MuleSoft\Model\Catalyst\Api\CreateCustomer
     */
    protected $createCustomerApi;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * CreateCustomer constructor.
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Ey\MuleSoft\Model\Catalyst\Api\GetCustomer $getCustomerApi
     * @param \Ey\MuleSoft\Model\Catalyst\Api\CreateCustomer $createCustomerApi
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Ey\MuleSoft\Helper\Data $helper
     * @param \Ey\MuleSoft\Model\Core\Logger\Monolog $mulesoftLogger
     */
    public function __construct(
        \Magento\Eav\Model\Config $eavConfig,
        \Ey\MuleSoft\Model\Catalyst\Api\GetCustomer $getCustomerApi,
        \Ey\MuleSoft\Model\Catalyst\Api\CreateCustomer $createCustomerApi,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Ey\MuleSoft\Helper\Data $helper,
        \Ey\MuleSoft\Model\Core\Logger\Monolog $mulesoftLogger
    ) {
        $this->eavConfig = $eavConfig;
        $this->getCustomerApi = $getCustomerApi;
        $this->createCustomerApi = $createCustomerApi;
        $this->_checkoutSession = $checkoutSession;
        $this->jsonHelper = $jsonHelper;
        $this->helper = $helper;
        $this->mulesoftLogger= $mulesoftLogger;
        $this->isDebugMode = $this->helper->getMuleSoftDebug();
        $this->isDebugModeType = $this->helper->getMuleSoftDebugMode();
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->isDebugMode) {
            $this->mulesoftLogger->mulesoftLog("Calling Observer");
        }
        $originalRequestData = $observer->getRequest()->getPostValue('customer');
        $customerId = isset($originalRequestData['entity_id'])
            ? $originalRequestData['entity_id']
            : null;
        $customer = $observer->getEvent()->getCustomer();
        if (!$customerId) {
            $this->callCreateCustomer($customer);
        }
        return $this;
    }

    /**
     * @param $customer
     */
    public function callCreateCustomer($customer)
    {
        try {
            if ($this->isDebugMode) {
                $this->mulesoftLogger->mulesoftLog(" ****************** Get Customer API Call Start create customer action *******************");
            }
            try {
                $customerResponse = $this->getCustomerApi->execute($customer->getEmail());
                if ($customerResponse->getStatusCode() == 200) {
                    $existingCustomerResponse = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                }
            } catch(\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __($e->getMessage())
                );
            }

            if ($this->isDebugMode) {
                $this->mulesoftLogger->mulesoftLog("Get Customer API For admin create customer action Response Code-");
                $this->mulesoftLogger->mulesoftLog($customerResponse->getStatusCode());
            }

            if ($customerResponse->getStatusCode() != 200 || (isset($existingCustomerResponse['audienceType']) && $existingCustomerResponse['audienceType'] == 'LEAD')) {
                try {
                    if ($this->isDebugMode) {
                        $this->mulesoftLogger->mulesoftLog("Create Customer payload request for admin create customer action-");
                    }
                    $customerPayload = $this->createCustomerPayload($customer);
                    if (isset($existingCustomerResponse['audienceType']) && $existingCustomerResponse['audienceType'] == 'LEAD') {
                        $customerPayload['customer']['ucid'] = $existingCustomerResponse['ucid'];
                    }
                    if ($this->isDebugMode && $this->isDebugModeType) {
                        $this->mulesoftLogger->mulesoftLog("Create Customer payload " . json_encode($customerPayload, JSON_PRETTY_PRINT));
                    }
                    $customerResponse = $this->createCustomerApi->execute($customerPayload);
                } catch(\Exception $e) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __($e->getMessage())
                    );
                }
                if ($this->isDebugMode) {
                    $this->mulesoftLogger->mulesoftLog("Response code-" . $customerResponse->getStatusCode());
                }
                if ($customerResponse->getStatusCode() != 201) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __('Customer Create API Error code create customer action: '.$customerResponse->getStatusCode())
                    );
                }

                $response = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                if ($this->isDebugMode && $this->isDebugMode) {
                    $this->mulesoftLogger->mulesoftLog("Response content-" . json_encode($response, JSON_PRETTY_PRINT));
                }
                if (isset($response['ucid'])) {
                    $this->helper->saveUcid($customer->getId(),$response['ucid']);
                }
                if ($this->isDebugMode) {
                    $this->mulesoftLogger->mulesoftLog("Admin Create Customer API End create customer action-");
                }
                return;
            }

            if ($this->isDebugMode) {
                if($this->isDebugModeType){
                    $this->mulesoftLogger->mulesoftLog("Get Customer API Response Content create customer action-");
                    $this->mulesoftLogger->mulesoftLog(print_r($existingCustomerResponse, true));
                }
                $this->mulesoftLogger->mulesoftLog(" ****************** Get Customer API Call End create customer action *******************");
            }
        } catch(\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
            );
        }
    }

    /**
     * @param $customer
     * @return mixed
     */
    public function createCustomerPayload($customer){
        $websiteId = $this->helper->getWebsiteId();
        $customer = $this->helper->customerExists($customer->getEmail(),$websiteId);
        $role = $this->eavConfig->getAttribute('customer', 'role');
        $customersuffix = $this->eavConfig->getAttribute('customer', 'customersuffix');
        $primaryspecialty = $this->eavConfig->getAttribute('customer', 'primaryspecialty');
        $payload['customer'] = array();
        $professionalCat = '';
        $payload['customer']['customerId'] = $customer->getData('entity_id');
        $payload['customer']['firstName'] = $customer->getData('firstname');
        $payload['customer']['lastName'] = $customer->getData('lastname');
        $payload['customer']['email'] = $customer->getData('email');
        $payload['customer']['role'] = $role->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionText($customer->getData('role'));
        $payload['customer']['suffix'] = $customersuffix->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionText($customer->getData('customersuffix'));
        $payload['customer']['password'] = isset($orderData['customer']['password']) ? $orderData['customer']['password'] : 'NA';
        if ($customer->getData('primaryspecialty')) {
            $payload['customer']['primarySpecialty'] = $primaryspecialty->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource()->getOptionText($customer->getData('primaryspecialty'));
        }
        if (isset($payload['customer']['role']) && isset($payload['customer']['suffix'])) {
            $professionalCat = $this->helper->getProfessionCategory($payload['customer']['role'], $payload['customer']['suffix']);
            $payload['customer']['professionalCategory'] = $professionalCat;
            $customer->setProfessionalCategory($professionalCat)->save();
        }
        $payload['customer']['nameOfOrganization'] = $customer->getData('nameoforganization');
        $alerts = [];
        if ($customer->getData('catalystconnect')) {
            $alerts[] = ['alert' => 'catalystConnect'];
        }
        if ($customer->getData('catalystsoi')) {
            $alerts[] = ['alert' => 'catalystSOI'];
        }
        if ($customer->getData('nejmsoi')) {
            $alerts[] = ['alert' => 'nejmSOI'];
        }
        if ($customer->getData('nejmetoc')) {
            $alerts[] = ['alert' => 'nejmTOC'];
        }

        $payload['customer']['emailPreferences'] = $alerts;
        $payload['customer']['country'] = $customer->getData('customer_default_country');;
        $payload['customer']['insightsCouncilMember'] = (bool)$customer->getData('insightscouncilmember');
        return $payload;
    }
}
