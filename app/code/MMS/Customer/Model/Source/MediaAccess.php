<?php
namespace MMS\Customer\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class MediaAccess extends AbstractSource
{
    protected $_options;
    public function getAllOptions()
    {
        $this->_options = [
                ['value' => '', 'label' => __('Please select')],
                ['value' => 'NONE', 'label' => __('NONE')],
                ['value' => 'REGULAR', 'label' => __('REGULAR')],
                ['value' => 'ADVANCE', 'label' => __('ADVANCE')],
                ['value' => 'EXPIRED', 'label' => __('EXPIRED')],
            ];
        return $this->_options;
    }
}
