<?php
namespace MMS\Customer\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class StudentType extends AbstractSource
{
    protected $_options;
    public function getAllOptions()
    {
        $this->_options = [
                ['value' => '', 'label' => __('Please select a student type')],
                ['value' => 'MED', 'label' => __('Medical Student')],
                ['value' => 'PA', 'label' => __('PA Student')],
                ['value' => 'BUS', 'label' => __('Business Student')],
                ['value' => 'NP', 'label' => __('Nurse Practitioner Student')],
                ['value' => 'OTH', 'label' => __('Other Student')],
            ];
        return $this->_options;
    }
}
