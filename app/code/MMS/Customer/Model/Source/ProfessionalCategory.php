<?php
namespace MMS\Customer\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class ProfessionalCategory extends AbstractSource
{
    protected $_options;
    public function getAllOptions()
    {
        $this->_options = [
                ['value' => '', 'label' => __('Please select a Professional category')],
                ['value' => 'PHY', 'label' => __('Physician')],
                ['value' => 'RES', 'label' => __('Resident')],
                ['value' => 'PA', 'label' => __('PA')],
                ['value' => 'NUR', 'label' => __('Nurse')],
                ['value' => 'NP', 'label' => __('Nurse Practitioner')],
                ['value' => 'STU', 'label' => __('Student')],
                ['value' => 'OTH', 'label' => __('Other')]
            ];
        return $this->_options;
    }
}
