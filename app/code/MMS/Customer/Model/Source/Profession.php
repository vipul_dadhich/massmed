<?php
namespace MMS\Customer\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Profession extends AbstractSource
{
    protected $_options;
    public function getAllOptions()
    {
        $this->_options = [
                ['value' => '', 'label' => __('Please select a profession')],
                ['value' => 'CHI', 'label' => __('Chiropractor')],
                ['value' => 'DTL', 'label' => __('Dentist')],
                ['value' => 'PHD', 'label' => __('Doctoral Degree')],
                ['value' => 'EMT', 'label' => __('Emergency Medical Tech')],
                ['value' => 'SRM', 'label' => __('Executive/Senior Manager')],
                ['value' => 'LAW', 'label' => __('Lawyer')],
                ['value' => 'LIB', 'label' => __('Librarian')],
                ['value' => 'MST', 'label' => __('Masters Degree')],
                ['value' => 'COM', 'label' => __('Media Professional')],
                ['value' => 'LAB', 'label' => __('Medical Tech & Lab')],
                ['value' => 'THR', 'label' => __('Medical Therapist')],
                ['value' => 'NMW', 'label' => __('Nurse Midwife')],
                ['value' => 'OPT', 'label' => __('Optometrist')],
                ['value' => 'DRU', 'label' => __('Pharmaceutical Company')],
                ['value' => 'PHA', 'label' => __('Pharmacist')],
                ['value' => 'POD', 'label' => __('Podiatrist')],
                ['value' => 'PCO', 'label' => __('Psychology')],
                ['value' => 'PUB', 'label' => __('Public Health Professional')],
                ['value' => 'US', 'label' => __('Unspecified')],
                ['value' => 'VET', 'label' => __('Veterinary Medicine')]
            ];
        return $this->_options;
    }
}
