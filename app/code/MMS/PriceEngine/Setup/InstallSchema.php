<?php

namespace Ey\PriceEngine\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $installer = $setup;
        $installer->startSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'ucc_pricecode',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Price Code',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'ucc_pricecode',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Price Code',
            ]
        );

        $setup->endSetup();
    }
}
