<?php


namespace MMS\PriceEngine\Setup;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Backend\Price;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * Class UpgradeData
 * @package MMS\PriceEngine\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $_eavSetupFactory;

    /**
     * UpgradeData constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->_eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '2.1.0') < 0) {
            $multiCurrencyAttrs = [
                'default_price_aud' => ['label' => 'Default Price AUD', 'sort_order' => 110],
                'default_price_cad' => ['label' => 'Default Price CAD', 'sort_order' => 120],
                'default_price_eur' => ['label' => 'Default Price EUR', 'sort_order' => 130],
                'default_price_gbp' => ['label' => 'Default Price GBP', 'sort_order' => 140]
            ];

            /** @var EavSetup $eavSetup */
            $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
            foreach ($multiCurrencyAttrs as $currencyAttr => $currencyAttrOption) {
                $eavSetup->addAttribute(
                    Product::ENTITY,
                    $currencyAttr,
                    [
                        'type' => 'decimal',
                        'label' => $currencyAttrOption['label'],
                        'input' => 'price',
                        'backend' => Price::class,
                        'required' => false,
                        'sort_order' => $currencyAttrOption['sort_order'],
                        'global' => ScopedAttributeInterface::SCOPE_WEBSITE,
                        'used_in_product_listing' => true,
                        'apply_to' => '',
                        'group' => 'Multi Currency Prices',
                        'is_used_in_grid' => false,
                        'is_visible_in_grid' => false,
                        'is_filterable_in_grid' => false,
                    ]
                );
            }
        }
        $setup->endSetup();
    }
}
