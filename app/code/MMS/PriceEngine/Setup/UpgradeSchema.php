<?php

namespace MMS\PriceEngine\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 * @package MMS\PriceEngine\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * install tables
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();
        if (version_compare($context->getVersion(), '2.0.0') < 0) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                'quote',
                'ucc_pricecode',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'Price Code From API'
                ]
        );
            $connection->addColumn(
                'sales_order',
                'ucc_pricecode',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'Price Code From API'
                ]
            );
       }

        $setup->endSetup();
    }
}
