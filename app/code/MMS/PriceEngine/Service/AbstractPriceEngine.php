<?php

namespace MMS\PriceEngine\Service;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\App\Cache\StateInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Serialize\Serializer\Serialize;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\StoreManagerInterface;
use MMS\PriceEngine\Api\IpToCountryRepositoryInterface;
use MMS\PriceEngine\Helper\Data as DataHelper;
use Ey\URLParams\Helper\Data as CookieHelper;
use MMS\PriceEngine\Model\Logger as PriceLogger;
use MMS\PriceEngine\Service\Mulesoft\PriceEngineClient;
use Zend\Http\Client;
use MMS\PriceEngine\Model\Cache\Type as PriceEngineCacheType;
use Magento\Framework\App\CacheInterface;

/**
 * Class AbstractPriceEngine
 * @package MMS\PriceEngine\Service
 */
abstract class AbstractPriceEngine
{
    public $service = 'price_engine';
    protected $fullResponse = false;
    protected $fullTermsResponse = false;

    const DEFAULT_CURRENCY_CODE = 'USD';
    protected $_currencyCode = null;

    /** @var bool */
    private $_cacheEnabled;

    /**
     * Array of price engine calls
     *
     * @var array
     */
    protected static $_priceEngineCache = [];

    /**
     * @var Client
     */
    protected $_zendClient;
    /**
     * @var Json
     */
    protected $_json;
    /**
     * @var RequestInterface
     */
    protected $_request;
    /**
     * @var DataHelper
     */
    protected $_dataHelper;
    /**
     * @var CountryFactory
     */
    protected $_countryFactory;
    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * @var CookieHelper
     */
    public $_cookieHelper;

    /**
     * @var PriceLogger
     */
    protected $_priceLogger;
    /**
     * @var PriceEngineClient
     */
    protected $_priceEngineClient;
    /**
     * @var CacheInterface
     */
    protected $_cache;
    /**
     * @var SerializerInterface
     */
    protected $_serializer;
    /**
     * @var IpToCountryRepositoryInterface
     */
    protected $_ipToCountryRepository;
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * AbstractPriceEngine constructor.
     * @param Client $zendClient
     * @param Json $json
     * @param RequestInterface $request
     * @param DataHelper $dataHelper
     * @param CookieHelper $cookieHelper
     * @param PriceLogger $priceLogger
     * @param CountryFactory $countryFactory
     * @param CheckoutSession $checkoutSession
     * @param PriceEngineClient $priceEngineClient
     * @param CacheInterface $cache
     * @param StateInterface $cacheState
     * @param IpToCountryRepositoryInterface $ipToCountryRepository
     */
    public function __construct(
        Client $zendClient,
        Json $json,
        RequestInterface $request,
        DataHelper $dataHelper,
        CookieHelper $cookieHelper,
        PriceLogger $priceLogger,
        CountryFactory $countryFactory,
        CheckoutSession $checkoutSession,
        PriceEngineClient $priceEngineClient,
        CacheInterface $cache,
        StateInterface $cacheState,
        IpToCountryRepositoryInterface $ipToCountryRepository,
        StoreManagerInterface $storeManager
    ) {
        $this->_zendClient = $zendClient;
        $this->_json = $json;
        $this->_request = $request;
        $this->_priceLogger = $priceLogger;
        $this->_dataHelper = $dataHelper;
        $this->_countryFactory = $countryFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_priceEngineClient = $priceEngineClient;
        $this->_cookieHelper = $cookieHelper;
        $this->_cache = $cache;
        $this->_ipToCountryRepository = $ipToCountryRepository;
        $this->_storeManager = $storeManager;
        $this->_cacheEnabled = $cacheState->isEnabled(PriceEngineCacheType::TYPE_IDENTIFIER);
    }

    /**
     * @return mixed
     */
    abstract public function getPrice();

    /**
     * @param $priceCode
     * @return mixed
     */
    abstract public function setPriceCode($priceCode);

    /**
     * @return mixed
     */
    abstract public function getPriceCode();

    /**
     * @param bool $skipCache
     * @return mixed
     */
    abstract public function buildPrice($skipCache = false);

    /**
     * @param $requestPayload
     * @return mixed
     */
    abstract public function setRequest($requestPayload);

    /**
     * @return mixed
     */
    abstract public function getRequest();

    /**
     * @return mixed
     */
    abstract public function buildEndPoint();

    /**
     * @return bool
     */
    public function getFullResponse()
    {
        return $this->fullResponse;
    }

    /**
     * @return bool
     */
    public function getFullTermsResponse()
    {
        return $this->fullTermsResponse;
    }

    /**
     * @param $fullResponse
     * @return void
     */
    public function setFullResponse($fullResponse)
    {
        $this->fullResponse = $fullResponse;
    }

    /**
     * @param $fullTermsResponse
     * @return void
     */
    public function setFullTermsResponse($fullTermsResponse)
    {
        $this->fullTermsResponse = $fullTermsResponse;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->_dataHelper->isEnabled();
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isEnabledFixtureMode($store = null)
    {
        return $this->_dataHelper->isEnabledFixtureMode($store);
    }

    /**
     * @return bool
     */
    public function isExtendFullPageCache(): bool
    {
        return $this->_dataHelper->isExtendFullPageCache();
    }

    /**
     * @param int $productId
     * @param null $isDefaultId
     * @param array $buyRequest
     * @return array
     */
    public function getProductDefaultTerms(int $productId, $isDefaultId = null, $buyRequest = [])
    {
        return $this->_dataHelper->getProductDefaultTerms($productId, $isDefaultId, $buyRequest);
    }

    /**
     * @return string
     */
    public function getApiPointUrl()
    {
        return $this->_dataHelper->getMuleSoftEndPoint();
    }

    /**
     * @return bool
     */
    public function isEnabledCache()
    {
        return $this->_dataHelper->isEnabledCache() && $this->_cacheEnabled;
    }

    /**
     * @return mixed
     */
    public function getCacheLifetime()
    {
        $cacheLifetime = $this->_dataHelper->getCacheLifetime();
        if ($cacheLifetime == null || $cacheLifetime == '') {
            /**
             * Default cache lifetime is 7 days
             */
            $cacheLifetime = 604800;
        }
        return $cacheLifetime;
    }

    /**
     * @return mixed|array|bool|int|float|string|null
     */
    public function sendRequest()
    {
        if ($this->isEnabledFixtureMode()) {
            return $this->sampleResponse();
        }

        try {
            $endPoint = $this->buildEndPoint();
            $response = $this->_priceEngineClient->execute($endPoint);

            if ($response->getStatusCode() == 200) {
                $result = $response->getBody()->getContents();
                $this->getPriceLogger()->debug("*********** Mulesoft PriceEngine API Response Start ***********");
                $this->getPriceLogger()->debug([
                    "event" => "Price Engine API Response",
                    "responseCode" => $response->getStatusCode(),
                    "response" => $result
                ]);
                $this->_priceLogger->debug("*********** Mulesoft PriceEngine API Response End ***********");
                return $this->_json->unserialize($result);
            }
        } catch (\Exception $ex) {
            /** Put MMS Logger here */
        }

        return false;
    }

    /**
     * Returns cache key for some request to price engine service
     *
     * @param string|array $requestParams
     * @return string
     */
    protected function _getQuotesCacheKey($requestParams)
    {
        if (is_array($requestParams)) {
            $requestParams = implode(
                ',',
                array_merge([$this->service], array_keys($requestParams), $requestParams)
            );
        }

        $priceCacheKey = implode(',', $this->getCacheKey());
        $this->getPriceLogger()->debug([
            'event' => "Price engine cache key elements:",
            'data' => $priceCacheKey,
            'generatedKey' => crc32($requestParams) . '_' . crc32($priceCacheKey)
        ]);
        return PriceEngineCacheType::PRICE_ENGINE_CACHE_KEY . '_' . crc32($requestParams) . '_' . crc32($priceCacheKey);
    }

    /**
     * Checks whether some request to price engine have already been done, so we have cache for it
     *
     * Used to reduce number of same requests done to price engine service during one session
     * Returns cached response or null
     *
     * @param string|array $requestParams
     * @return null|string
     */
    protected function _getCachedQuotes($requestParams)
    {
        if ($this->isEnabledCache()) {
            $key = $this->_getQuotesCacheKey($requestParams);
            $this->getPriceLogger()->debug([
                'event' => "Returning cache key:",
                'generatedKey' => $key
            ]);
            $data = $this->_cache->load($key);
            return $data ? $this->getSerializer()->unserialize($data) : null;
        }

        return null;
    }

    /**
     * Sets received price engine calls to cache
     *
     * @param string|array $requestParams
     * @param string|array $response
     * @return $this
     */
    protected function _setCachedQuotes($requestParams, $response)
    {
        if ($this->isEnabledCache()) {
            $key = $this->_getQuotesCacheKey($requestParams);
            $this->getPriceLogger()->debug('************** Start - Setting Cache Tags **************');
            $this->getPriceLogger()->debug([
                'event' => "_setCachedQuotes",
                'requestParams' => $requestParams,
                'CacheKey' => $this->_getQuotesCacheKey($requestParams),
                'StorageKey' => $key,
                'SerializedData' => $this->getSerializer()->serialize($response),
                'CacheTag' =>array(PriceEngineCacheType::CACHE_TAG),
                'TTL' => $this->getCacheLifetime()
            ]);

            $this->_cache->save(
                $this->getSerializer()->serialize($response),
                $key,
                array(PriceEngineCacheType::CACHE_TAG),
                $this->getCacheLifetime()
            );
            $this->getPriceLogger()->debug('************** End - Setting Cache Tags **************');
        } else {
            $this->getPriceLogger()->debug('************** Price Engine Cache Tags Disabled **************');
        }

        return $this;
    }

    /**
     * @return string[]
     */
    public function sampleRequestPayload()
    {
        return [
            'Promo' => 'promo_code_sample',
            'primary_speciality' => 'PR',
            'role'=> 'role',
            'product' => 'sku',
            'sku' => 'sku',
            'term' =>  'term'
        ];
    }

    /**
     * @return array|false
     */
    public function sampleResponse()
    {
        $request = $this->getRequest();
        $fixtureData = $this->_dataHelper->getFixturesData();

        $keyRow = [];
        if (isset($request['promo'])) {
            $keyRow[] = $request['promo'];
        }
        if (isset($request['professionalCategory'])) {
            $keyRow[] = $request['professionalCategory'];
        }
        if (isset($request['country'])) {
            $keyRow[] = $request['country'];
        }
        if (isset($request['sku'])) {
            $keyRow[] = $request['sku'];
        }
        if (isset($request['term'])) {
            $keyRow[] = $request['term'];
        }

        $key = implode('|', $keyRow);
        if (isset($fixtureData[$key])) {
            if ($this->getFullResponse()) {
                return $fixtureData[$key];
            }
            return $fixtureData[$key]['terms'][0]['price'];
        }

        return false;
    }

    /**
     * @param array $buyRequest
     * @return mixed
     */
    public function getPromoCode($buyRequest = [])
    {
        $promoCode = $this->_request->getParam($this->_dataHelper->getPromoCodeUrlParam(), false);
        if (!$promoCode && strval($this->_dataHelper->getCookiePromoCode()) != '') {
            $promoCode = $this->_dataHelper->getCookiePromoCode();
        }

        if ((!$promoCode || $promoCode == '') && $this->getPromoCodeFromQuote()) {
            $promoCode = $this->getPromoCodeFromQuote();
        }

        if ((!$promoCode || $promoCode == '') && isset($buyRequest['promocode']) && $buyRequest['promocode'] != '') {
            $promoCode = $buyRequest['promocode'];
        }

        return $promoCode;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        if ($this->getCustomer()) {
            $defaultCountry = $this->getCustomer()->getData('customer_default_country');
            if (!$defaultCountry && $billingAddress = $this->getCustomer()->getDefaultBillingAddress()) {
                $defaultCountry = $billingAddress->getCountryId();
            }
            if ($defaultCountry) {
                $country = $this->_countryFactory->create()->loadByCode($defaultCountry);
                if ($countryIsoCode = $country->getData('iso3_code')) {
                    return $countryIsoCode;
                }
            }
        }

        if ($countryIsoCode = $this->_ipToCountryRepository->getVisitorCountryCode()) {
            return $countryIsoCode;
        }
        return 'USA';
    }

    /**
     * @return string
     */
    public function getProfessionalCategory()
    {
        if ($this->getCustomer()) {
            $professionalCategory = $this->getCustomer()->getProfessionalCategory();
            if (!$professionalCategory) {
                $role = $this->getCustomer()->getData('role');
                $suffix = $this->getCustomer()->getData('customersuffix');
                try {
                    $roleText = $this->_dataHelper->getEavAttributeCustomer('role', $role);
                    $suffixText = $this->_dataHelper->getEavAttributeCustomer('customersuffix', $suffix);
                    return $this->_dataHelper->getProfessionCategory($roleText, $suffixText);
                } catch (LocalizedException $e) {
                    return "PHY";
                }
            }
            return $professionalCategory ?? "PHY";
        } else if ($this->getCheckoutSession()->getCheckoutPriceUpdateFlag()) {
            $professionalCatText = $this->getCheckoutSession()->getCheckoutProfessionalCatText();
            if ($professionalCatText != '' && $professionalCatText != null) {
                return $professionalCatText;
            }
            $roleText = $this->getCheckoutSession()->getCheckoutRoleText();
            $suffixText = $this->getCheckoutSession()->getCheckoutSuffixText();
            return $this->_dataHelper->getProfessionCategory($roleText, $suffixText);
        }
        return "PHY";
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        if ($this->_currencyCode == null) {
            $this->_currencyCode = self::DEFAULT_CURRENCY_CODE;
        }
        return $this->_currencyCode;
    }

    /**
     * @param $currencyCode
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->_currencyCode = $currencyCode;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return Float|int
     */
    public function getQuoteItemPrice(\Magento\Quote\Model\Quote\Item $item)
    {
        $buyRequest = [];
        $option = $item->getOptionByCode('info_buyRequest');
        if ($option) {
            $buyRequest = \json_decode($option->getValue(), true);
            if (is_null($buyRequest)) {
                $buyRequest = \unserialize($option->getValue());
            }
        }

        $terms = $this->getProductDefaultTerms($item->getProduct()->getId(), null, $buyRequest);
        $payload = [
            'sku' => $item->getProduct()->getSku()
        ];

        $promoCode = $this->getPromoCode($buyRequest);
        if ($promoCode) {
            $payload['promo'] = $promoCode;
        }
        if (!empty($terms)) {
            $payload['term'] = $terms[0];
        }

        $this->setRequest($payload);
        $this->setFullResponse(true);
        $this->buildPrice();
        $result = $this->getPrice();
        $finalPrice = 0;
        $priceCode = null;
        if (isset($terms[0])) {
            if (isset($result['products'][0]['terms']) && count($result['products'][0]['terms']) > 0) {
                foreach($result['products'][0]['terms'] as $termOption) {
                    if (strtolower($termOption['term']) == strtolower($terms[0])) {
                        $finalPrice = $termOption['price'];
                        $priceCode = $termOption['priceCode'];
                        break;
                    }
                }
            }
        }

        if ($finalPrice == 0) {
            $finalPrice = isset($result['products'][0]['terms'][0]['price']) ? $result['products'][0]['terms'][0]['price'] : 0;
            $priceCode = isset($result['products'][0]['terms'][0]['priceCode']) ? $result['products'][0]['terms'][0]['priceCode'] : '';
        }

        if (isset($result['products'][0]['currency'])) {
            $this->setCurrencyCode($result['products'][0]['currency']);
        }

        $this->setPriceCode($priceCode);
        return $finalPrice;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return Float|int
     */
    public function getQuoteItemPriceCode(\Magento\Quote\Model\Quote\Item $item)
    {
        /*$buyRequest = [];
        $option = $item->getOptionByCode('info_buyRequest');
        if ($option) {
            $buyRequest = \json_decode($option->getValue(), true);
            if (is_null($buyRequest)) {
                $buyRequest = \unserialize($option->getValue());
            }
        }

        $terms = $this->getProductDefaultTerms($item->getProduct()->getId(), null, $buyRequest);

        $payload = [
            'sku' => $item->getProduct()->getSku()
        ];
        if (!empty($terms)) {
            $payload['term'] = $terms[0];
        }

        $this->setRequest($payload);
        $this->buildPrice();*/
        return $this->getPriceCode();
    }

    /**
     * @return bool
     */
    public function isAllowedCustomPrice()
    {
        if ($this->getCheckoutSession()->hasQuote()) {
            try {
                if ($this->getCheckoutSession()->getQuote()->getData('is_paybill')) {
                    return false;
                }
            } catch (NoSuchEntityException $e) {
            } catch (LocalizedException $e) {
            }
        }

        return true;
    }

    /**
     * @return false|\Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        return $this->_dataHelper->getCustomer();
    }

    /**
     * @return PriceLogger
     */
    public function getPriceLogger()
    {
        return $this->_priceLogger;
    }

    /**
     * @return CheckoutSession
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    /**
     * Get promo code from quote
     *
     * @return string
     */
    public function getPromoCodeFromQuote()
    {
        if ($this->getCheckoutSession()->hasQuote()) {
            try {
                return $this->getCheckoutSession()->getQuote()->getData('ucc_promocode');
            } catch (NoSuchEntityException $e) {
            } catch (LocalizedException $e) {
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getCacheKey(): array
    {
        $_keys = [];
        $_keys['customerType'] = $this->getCustomer() ? 'Customer' : 'Guest';
        $_keys['customerId'] = $this->getCustomer()
            ? $this->getCustomer()->getId() : '0';
        if ($this->getCustomer()) {
            $_keys['customer_ucid'] = $this->getCustomer()->getData('ucid');
        }
        if ($this->getPromoCode()) {
            $_keys['promoCode'] = $this->getPromoCode();
        }
        if ($this->getProductSku()) {
            $_keys['sku'] = $this->getProductSku();
        }
        if ($this->getProductTerms()) {
            $_keys['term'] = $this->getProductTerms();
        }
        $_keys['professionalCategory'] = $this->getProfessionalCategory();
        $_keys['country'] = $this->getCountry();
        $_keys['current_currency_code'] = $this->getCurrentCurrencyCode();

        return $_keys;
    }

    /**
     * @return mixed
     */
    public function getProductSku()
    {
        $productSku = '';
        if ($this->getProductSkuFromQuote()) {
            $productSku = $this->getProductSkuFromQuote();
        }
        return $productSku;
    }

    /**
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getProductTerms()
    {
        $productTerms= [];
        if ($this->getCheckoutSession()->getQuote()->hasItems()) {
            try {
                $items = $this->getCheckoutSession()->getQuote()->getAllItems();
                foreach($items as $item){
                    $buyRequest = [];
                    $option = $item->getOptionByCode('info_buyRequest');
                    if ($option) {
                        $buyRequest = \json_decode($option->getValue(), true);
                        if (is_null($buyRequest)) {
                            $buyRequest = \unserialize($option->getValue());
                        }
                    }
                    $terms = $this->getProductDefaultTerms($item->getProduct()->getId(), null, $buyRequest);
                    if (!empty($terms)) {
                        $productTerms[] = $terms[0];
                    }

                }
                if (!empty($productTerms)) {
                    return implode('_', $productTerms);
                }
                return false;
            } catch (NoSuchEntityException $e) {
            } catch (LocalizedException $e) {
            }
        }
    }
    /**
     * Get promo code from quote
     *
     * @return string
     */
    public function getProductSkuFromQuote()
    {
        $skus= [];
        if ($this->getCheckoutSession()->getQuote()->hasItems()) {
            try {
                $items = $this->getCheckoutSession()->getQuote()->getAllItems();
                foreach($items as $item){
                    $skus[] = $item->getSku();
                }
                return implode('_', $skus);
            } catch (NoSuchEntityException $e) {
            } catch (LocalizedException $e) {
            }
        }

        return false;
    }
    /**
     * Get serializer
     *
     * @return SerializerInterface
     * @deprecated 101.0.0
     */
    private function getSerializer()
    {
        if (null === $this->_serializer) {
            $this->_serializer = \Magento\Framework\App\ObjectManager::getInstance()->get(Serialize::class);
        }
        return $this->_serializer;
    }

    /**
     * Gets customer's current currency
     *
     * @param null $store
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCurrentCurrencyCode($store = null)
    {
        return $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
    }
}
