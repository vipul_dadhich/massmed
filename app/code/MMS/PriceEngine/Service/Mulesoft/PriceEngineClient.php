<?php

namespace MMS\PriceEngine\Service\Mulesoft;

use Ey\MuleSoft\Helper\Data as MuleSoftHelper;
use GuzzleHttp\Client;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\ResponseFactory;
use MMS\PriceEngine\Model\Logger as PriceLogger;


class PriceEngineClient
{
    /**
     * @var ClientFactory
     */
    protected $_clientFactory;
    /**
     * @var ResponseFactory
     */
    protected $_responseFactory;
    /**
     * @var MuleSoftHelper
     */
    protected $_muleSoftHelper;

    /**
     * @var PriceLogger
     */
    protected $_priceLogger;

    /**
     * FinalPrice constructor.
     * @param PriceLogger $priceLogger
     */
    public function __construct(
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory,
        MuleSoftHelper $muleSoftHelper,
        PriceLogger $priceLogger
    ) {
        $this->_clientFactory = $clientFactory;
        $this->_responseFactory = $responseFactory;
        $this->_muleSoftHelper = $muleSoftHelper;
        $this->_priceLogger = $priceLogger;
    }

    /**
     * @param $uriEndpoint
     * @param array $headers
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function execute($uriEndpoint, $headers = [])
    {
        $headers = $this->prepareHeader($headers);
        $client = $this->_clientFactory->create(['config' => [
            'base_uri' => $this->_muleSoftHelper->getMuleSoftURI(),
            'headers' => $headers
        ]]);

        try {
            $response = $client->request(
                'GET',
                $uriEndpoint
            );
        } catch (GuzzleException $exception) {
            $response = $this->_responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);
        }
        $this->_priceLogger->debug("*********** Mulesoft PriceEngine API ***********");
        $this->_priceLogger->debug([
            "event" => "Price Engine API calling",
            'Request Headers' => ['config' => [
                'base_uri' => $this->_muleSoftHelper->getMuleSoftURI(),
                'headers' => $headers
            ]],
            "method" => 'GET',
            "UriEndPoint" => $uriEndpoint,
            "responseCode" => $response->getStatusCode(),
            //"response" => $loggerResponse->getBody()->getContents()
        ]);
        $this->_priceLogger->debug("*********** End - Mulesoft PriceEngine API Calls ***********");

        return $response;
    }

    /**
     * prepare header for request
     * @param $headers
     * @return mixed
     */
    private function prepareHeader($headers){
        $headers['client_id'] = $this->_muleSoftHelper->getMuleSoftClientId();
        $headers['client_secret'] = $this->_muleSoftHelper->getMuleSoftClientSecret();
        return $headers;
    }
}
