<?php

namespace MMS\PriceEngine\Service;

/**
 * Class PriceEngine
 * @package MMS\PriceEngine\Service
 */
class PriceEngine extends \MMS\PriceEngine\Service\AbstractPriceEngine
{
    /**
     * @var array|mixed
     */
    protected $requestPayload;
    /**
     * @var Float
     */
    protected $_price = 0;

    /**
     * @var Float
     */
    protected $_priceCode = 'NoCodeAssoc';

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->_price;
    }

    /**
     * @param $priceCode
     * @return mixed
     */
    public function setPriceCode($priceCode)
    {
        return $this->_priceCode = $priceCode;
    }

    /**
     * @return Float|mixed|string
     */
    public function getPriceCode()
    {
        return $this->_priceCode;
    }

    /**
     * @param array $requestPayload
     * @return mixed|void
     */
    public function setRequest($requestPayload = [])
    {
        if ($this->getPromoCode() && !isset($requestPayload['promo'])) {
            $requestPayload['promo'] = $this->getPromoCode();
        }
        if ($this->getCustomer() && $this->getCustomer()->getData('ucid')) {
            $requestPayload['ucid'] = $this->getCustomer()->getData('ucid');
        }
        $requestPayload['professionalCategory'] = $this->getProfessionalCategory();
        $requestPayload['country'] = $this->getCountry();

        if (isset($requestPayload['term']) && $this->isEnabledFixtureMode()) {
            if (is_array($requestPayload['term'])) {
                $requestPayload['term'] = implode('|', $requestPayload['term']);
            }
        }

        if (!$this->isEnabledFixtureMode()) {
            /*$requestPayload['currency'] = $this->getCurrency();*/
            $product = [];
            if (isset($requestPayload['sku']) && $requestPayload['sku'] != '') {
                $product['sku'] = $requestPayload['sku'];
            }

            if (isset($requestPayload['term']) && $requestPayload['term'] != '') {
                $product['terms'] = $requestPayload['term'];
            }
            $requestPayload['products'] = $this->_json->serialize($product);
            unset($requestPayload['term']);
            unset($requestPayload['sku']);
        }

        $this->requestPayload = $requestPayload;
    }

    /**
     * @return array|mixed
     */
    public function getRequest()
    {
        return $this->requestPayload;
    }

    /**
     * @return mixed|string
     */
    public function buildEndPoint()
    {
        $apiEndPoint = $this->getApiPointUrl();
        $request = $this->getRequest();
        return $apiEndPoint . '?' . http_build_query($request);
    }

    /**
     * @param bool $skipCache
     * @return mixed|void
     */
    public function buildPrice($skipCache = false)
    {
        $this->_price = 0;
        $response = ($skipCache === true) ? null : $this->_getCachedQuotes($this->getRequest());
        $this->getPriceLogger()->debug("*********** Mulesoft PriceEngine API Response Start ***********");
        $this->getPriceLogger()->debug([
            "event" => "buildPrice Entry",
            "response" => $response
        ]);
        $this->_priceLogger->debug("*********** Mulesoft PriceEngine API Response End ***********");
        if ($response === null || $response === false) {
            $response = $this->sendRequest();
            $this->getPriceLogger()->debug("*********** Mulesoft PriceEngine API Response Start ***********");
            $this->getPriceLogger()->debug([
                "event" => "buildPrice response false",
                "response" => $response
            ]);
            $this->_priceLogger->debug("*********** Mulesoft PriceEngine API Response End ***********");
            if (!$skipCache) {
                $this->_setCachedQuotes($this->getRequest(), $response);
            }
        }

        if ($response) {
            if (isset($response['products'][0]['currency'])) {
                $this->_currencyCode = $response['products'][0]['currency'];
            }

            if (isset($response['products'][0]['terms'][0]['priceCode'])) {
                $this->_priceCode = $response['products'][0]['terms'][0]['priceCode'];
            }

            $_response = $this->getResponseByTerm($this->getRequest(), $response);
            if ($_response) {
                if (isset($_response['products'][0]['terms'][0]['priceCode'])) {
                    $this->_priceCode = $_response['products'][0]['terms'][0]['priceCode'];
                }
            }
            $this->getPriceLogger()->debug("*********** Mulesoft PriceEngine API Response Start ***********");
            $this->getPriceLogger()->debug([
                "event" => "buildPrice _response after priceCode",
                "response" => $_response
            ]);
            $this->_priceLogger->debug("*********** Mulesoft PriceEngine API Response End ***********");

            /* Add logging of these two booleans, since the second doesn't seem to be returned properly */
            $this->getPriceLogger()->debug([
                "event" => "buildPrice getFullTermsResponse",
                "response" => $this->getFullTermsResponse()
            ]);

            $this->getPriceLogger()->debug([
                "event" => "buildPrice getFullResponse",
                "response" => $this->getFullResponse()
            ]);

            if ($this->getFullTermsResponse()) {
                $response = $response['products'][0];
                $this->getPriceLogger()->debug("*********** Mulesoft PriceEngine API Response Start ***********");
                $this->getPriceLogger()->debug([
                    "event" => "buildPrice term response",
                    "response" => $response
                ]);
                $this->_priceLogger->debug("*********** Mulesoft PriceEngine API Response End ***********");
            } elseif (!$this->getFullResponse()) {
                $this->getPriceLogger()->debug("*********** Mulesoft PriceEngine API Response Start ***********");
                $this->getPriceLogger()->debug([
                    "event" => "buildPrice full response",
                    "response" => $_response
                ]);
                $this->_priceLogger->debug("*********** Mulesoft PriceEngine API Response End ***********");
                if ($_response) {
                    $response = $_response['products'][0]['terms'][0]['price'];
                } else {
                    $response = $response['products'][0]['terms'][0]['price'];
                }
            }
            $this->getPriceLogger()->debug("*********** Mulesoft PriceEngine API Response Start ***********");
                $this->getPriceLogger()->debug([
                    "event" => "buildPrice final price",
                    "response" => $response
                ]);
                $this->_priceLogger->debug("*********** Mulesoft PriceEngine API Response End ***********");
            $this->_price = $response;
        }
    }

    /**
     * @param $request
     * @param $response
     * @return false
     */
    protected function getResponseByTerm($request, $response)
    {
        if (isset($request['products'])) {
            $request = $this->_json->unserialize($request['products']);
            if (isset($request['terms'])) {
                if (isset($response['products']) && !empty($response['products'])) {
                    foreach ($response['products'] as &$product) {
                        if (isset($product['terms']) && !empty($product['terms']) && isset($request['terms'])) {
                            $terms = '';
                            if (is_array($request['terms']) && count($request['terms']) == 1) {
                                if (isset($request['terms'][0]['term'])) {
                                    $terms = $request['terms'][0]['term'];
                                } else if (isset($request['terms'][0])) {
                                    $terms = $request['terms'][0];
                                }
                            } else if (is_string($request['terms'])) {
                                $terms = $request['terms'];
                            }
                            foreach ($product['terms'] as &$term) {
                                if (strtolower($term['term']) == strtolower( $terms )) {
                                    unset($product['terms']);
                                    $product['terms'][] = $term;
                                    return $response;
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     */
    public function sendRequest()
    {
        return parent::sendRequest();
    }

    /**
     * @return array
     */
    public function getCacheKey(): array
    {
        return parent::getCacheKey();
    }
}
