<?php

namespace MMS\PriceEngine\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use MMS\PriceEngine\Service\PriceEngine;

/**
 * Class CustomPriceInCart
 * @package MMS\PriceEngine\Observer
 */
class CustomPriceInCart implements ObserverInterface
{
    /**
     * @var PriceEngine
     */
    protected $_priceEngineService;

    /**
     * CustomPriceInCart constructor.
     * @param PriceEngine $priceEngineService
     */
    public function __construct(
        PriceEngine $priceEngineService
    ) {
        $this->_priceEngineService = $priceEngineService;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->_priceEngineService->isEnabled() && $this->_priceEngineService->isAllowedCustomPrice()) {
            /** @var \Magento\Quote\Model\Quote\Item $item */
            $item = $observer->getEvent()->getData('quote_item');
            $item = ($item->getParentItem() ? $item->getParentItem() : $item);

            $customPrice = $this->_priceEngineService->getQuoteItemPrice($item);
            if ($customPrice) {
                $priceCode = $this->_priceEngineService->getQuoteItemPriceCode($item);

                if($priceCode){
                    $item->getQuote()->setData('ucc_pricecode',$priceCode);
                    /**Here we going to change the logic to store in item when we go for the NEJM Pricing"**/
                }
                $item->setCustomPrice($customPrice);
                $item->setOriginalCustomPrice($customPrice);
                $item->getProduct()->setIsSuperMode(true);
            }
        }
    }
}
