<?php

namespace MMS\PriceEngine\Observer;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use MMS\PriceEngine\Service\PriceEngine;

/**
 * Class QuoteItemPriceManager
 * @package MMS\PriceEngine\Observer
 */
class QuoteItemPriceManager
{
    /**
     * @var PriceEngine
     */
    protected $_priceEngineService;
    /**
     * @var RequestInterface
     */
    protected $_request;
    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * CustomPriceInCart constructor.
     * @param PriceEngine $priceEngineService
     * @param RequestInterface $request
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        PriceEngine $priceEngineService,
        RequestInterface $request,
        CheckoutSession $checkoutSession
    ) {
        $this->_priceEngineService = $priceEngineService;
        $this->_request = $request;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return Float|int
     */
    public function getQuoteItemPrice(\Magento\Quote\Model\Quote\Item $item)
    {
        $buyRequest = [];
        $option = $item->getOptionByCode('info_buyRequest');
        if ($option) {
            $buyRequest = \json_decode($option->getValue(), true);
            if (is_null($buyRequest)) {
                $buyRequest = \unserialize($option->getValue());
            }
        }

        $terms = $this->_priceEngineService->getProductDefaultTerms($item->getProduct()->getId(), null, $buyRequest);

        $payload = [
            'sku' => $item->getProduct()->getSku()
        ];
        if (!empty($terms)) {
            $payload['term'] = $terms[0];
        }

        $this->_priceEngineService->setRequest($payload);
        $this->_priceEngineService->buildPrice();
        return $this->_priceEngineService->getPrice();
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->_priceEngineService->isEnabled();
    }

    /**
     * Get frontend checkout session object
     *
     * @return CheckoutSession
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    /**
     * @return bool
     */
    public function isAllowedCustomPrice()
    {
        try {
            if ($this->getCheckoutSession()->getQuote()->getData('is_paybill')) {
                return false;
            }
        } catch (NoSuchEntityException $e) {
        } catch (LocalizedException $e) {
        }

        return true;
    }
}
