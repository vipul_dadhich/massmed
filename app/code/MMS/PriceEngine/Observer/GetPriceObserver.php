<?php

namespace MMS\PriceEngine\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use MMS\PriceEngine\Service\PriceEngine;

/**
 * Class GetPriceObserver
 * @package MMS\PriceEngine\Observer
 */
class GetPriceObserver implements ObserverInterface
{
    /**
     * @var PriceEngine
     */
    protected $_priceEngineService;

    /**
     * GetPriceObserver constructor.
     * @param PriceEngine $_priceEngineService
     */
    public function __construct(
        PriceEngine $_priceEngineService
    ) {
        $this->_priceEngineService = $_priceEngineService;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->_priceEngineService->isEnabled() && $this->_priceEngineService->isAllowedCustomPrice()) {
            $product = $observer->getEvent()->getData('product');
            $request = $observer->getEvent()->getData('request');
            $this->_priceEngineService->setRequest($request);
            $this->_priceEngineService->buildPrice();
            $customPrice = $this->_priceEngineService->getPrice();
            if ($customPrice) {
                $product->setCustomPrice($customPrice);
            }
        }
    }
}
