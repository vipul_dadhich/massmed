<?php

namespace MMS\PriceEngine\Observer;

class SavePriceCodeOrder implements \Magento\Framework\Event\ObserverInterface
{
	protected $_request;

	public function __construct(
		\Magento\Framework\App\RequestInterface $request
	) {
		$this->_request = $request;
	}

	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		$quote = $observer->getQuote();
       	$order = $observer->getOrder();
       	$code = '';
		if ($quote->getData('ucc_pricecode') === NULL) {
			if (isset($this->_request->getPost()['order']['ucc_pricecode'])) {
				$code = $this->_request->getPost()['order']['ucc_pricecode'];
			}
		} else {
			$code = $quote->getData('ucc_pricecode');
		}
		if ($code) {
			$order->setData('ucc_pricecode', $code);
		}
		return $this;
	}
}
