<?php
namespace MMS\PriceEngine\Observer;


class SavePriceCodeQuote implements \Magento\Framework\Event\ObserverInterface{
	public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Ey\PromoCode\Helper\Data $helper
    ){
        $this->checkoutSession = $checkoutSession;
        $this->helper = $helper;
    }

	public function execute(\Magento\Framework\Event\Observer $observer){
		$quote = $this->checkoutSession->getQuote();

		$priceCode = $this->helper->getCustomCookie('PriceCode');
		if(strval($priceCode) != ""){
			if($quote->getId()){
				$quote->setData('ucc_pricecode',$priceCode);
                try {
                    $quote->save();
                } catch (\Exception $e) {
                }
                $this->helper->deleteCustomCookie('PriceCode');
			}
		}

		return $this;
	}
}
