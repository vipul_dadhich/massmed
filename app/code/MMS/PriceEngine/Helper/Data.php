<?php

namespace MMS\PriceEngine\Helper;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Module\Dir;
use Magento\Store\Model\ScopeInterface;
use Ey\PromoCode\Helper\Data as PromoCodeHelperData;

/**
 * Class Data
 * @package MMS\PriceEngine\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_ENABLED = 'price_engine/general/enable';
    const XML_LOG_PATH_ACTIVE = 'price_engine/general/logs_active';
    const XML_PATH_FIXTURE_MODE = 'price_engine/general/fixture_mode';
    const XML_PATH_CACHE_ENABLE = 'price_engine/general/cache_enable';
    const XML_PATH_CACHE_LIFETIME = 'price_engine/general/cache_lifetime';
    const XML_PATH_EXTEND_FULL_PAGE_CACHE = 'price_engine/general/extend_fpc';
    const XML_PATH_API_ENDPOINT = 'mulesoft/api/price_engine_endpoint';
    const XML_PATH_GEO_ENABLED = 'price_engine/geo/enabled';
    const XML_PATH_GEO_WS_LICENSE = 'price_engine/geo/ws_license';
    const XML_PATH_GEO_SIMULATE_IP = 'price_engine/geo/simulate_ip';

    /**
     * @var \Ey\PromoCode\Helper\Data
     */
    protected $_promoHelper;
    /**
     * @var \Ey\MuleSoft\Helper\Data
     */
    protected $_muleSoftHelper;
    /**
     * @var File
     */
    protected $_file;
    /**
     * @var Filesystem
     */
    protected $_filesystem;
    /**
     * @var Dir
     */
    protected $_directory;
    /**
     * @var \Magento\Customer\Model\SessionFactory
     */
    protected $_customerSessionFactory;
    /**
     * @var \Magento\Eav\Model\ConfigFactory
     */
    protected $_eavConfig;
    /**
     * @var ProductRepository
     */
    protected $_productRepository;
    /**
     * @var PromoCodeHelperData
     */
    protected $_promoCodeHelper;

    /**
     * Data constructor.
     * @param Context $context
     * @param \Ey\PromoCode\Helper\Data $promoHelper
     * @param \Ey\MuleSoft\Helper\Data $muleSoftHelper
     * @param File $file
     * @param Filesystem $filesystem
     * @param Dir $directory
     * @param \Magento\Customer\Model\SessionFactory $customerSessionFactory
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param ProductRepository $productRepository
     * @param PromoCodeHelperData $promoCodeHelper
     */
    public function __construct(
        Context $context,
        \Ey\PromoCode\Helper\Data $promoHelper,
        \Ey\MuleSoft\Helper\Data $muleSoftHelper,
        File $file,
        Filesystem $filesystem,
        Dir $directory,
        \Magento\Customer\Model\SessionFactory $customerSessionFactory,
        \Magento\Eav\Model\Config $eavConfig,
        ProductRepository $productRepository,
        PromoCodeHelperData $promoCodeHelper
    ) {
        $this->_promoHelper = $promoHelper;
        $this->_muleSoftHelper = $muleSoftHelper;
        $this->_file = $file;
        $this->_filesystem = $filesystem;
        $this->_directory = $directory;
        $this->_customerSessionFactory = $customerSessionFactory;
        $this->_eavConfig = $eavConfig;
        $this->_productRepository = $productRepository;
        $this->_promoCodeHelper = $promoCodeHelper;
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getPromoCodeUrlParam()
    {
        return $this->_promoHelper->getPromoCodeUrlParam();
    }

    /**
     * @return array|mixed
     */
    public function getFixturesData()
    {
        $filePath = $this->getFixtureFilePath();
        try {
            $string = $this->_file->fileGetContents($filePath);
            return \json_decode($string, true);
        } catch (FileSystemException $e) {
            return ["error"=>$e->getMessage()];
        }
    }

    /**
     * @return string
     */
    public function getFixtureFilePath()
    {
        $dirPath = $this->getModuleDataDir();
        return $dirPath . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'fixtures.json';
    }

    /**
     * @return string
     */
    public function getModuleDataDir()
    {
        return $this->_directory->getDir(
            'MMS_PriceEngine', \Magento\Framework\Module\Dir::MODULE_ETC_DIR
        );
    }

    /**
     * @return false|\Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        if ($this->_customerSessionFactory->create()->isLoggedIn()) {
            return $this->_customerSessionFactory->create()->getCustomer();
        }

        return false;
    }

    /**
     * @param $role
     * @param $suffix
     * @return string
     */
    public function getProfessionCategory($role, $suffix)
    {
        return $this->_muleSoftHelper->getProfessionCategory($role, $suffix);
    }

    /**
     * @param $code
     * @param $value
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getEavAttributeCustomer($code, $value)
    {
        $attribute = $this->_eavConfig->getAttribute('customer', $code);
        $source = $attribute->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID)->getSource();
        return $source->getOptionText($value);
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isEnabled($store = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isLogEnable($store = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_LOG_PATH_ACTIVE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isEnabledFixtureMode($store = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_FIXTURE_MODE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isEnabledCache($store = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_CACHE_ENABLE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getCacheLifetime($store = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_CACHE_LIFETIME,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isExtendFullPageCache($store = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_EXTEND_FULL_PAGE_CACHE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getMuleSoftEndPoint($store = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_API_ENDPOINT,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function isEnabledGeo($store = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_GEO_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getGeoLocationWSLicense($store = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_GEO_WS_LICENSE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getGeoLocationSimulateIP($store = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_GEO_SIMULATE_IP,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param int $productId
     * @param null $isDefaultId
     * @param array $buyRequest
     * @return array
     */
    public function getProductDefaultTerms(int $productId, $isDefaultId = null, $buyRequest = [])
    {
        $terms = [];
        try {
            $product = $this->_productRepository->getById($productId);
            $options = $product->getOptions();
            if (!empty($options) && count($options) > 0) {
                foreach ($options as $option) {
                    $optionData = $option->getValues();
                    foreach ($optionData as $data) {
                        $optionId = $data->getData('option_id');
                        $optionTypeId = $data->getData('option_type_id');
                        if (!empty($buyRequest)) {
                            if (isset($buyRequest['options'][$optionId]) && $buyRequest['options'][$optionId] == $optionTypeId) {
                                $terms[] = $data->getData('default_title');
                            }
                        } elseif ($data->getData('is_default')) {
                            $terms[] = $data->getData('default_title');
                        }
                    }
                }
            }
        } catch (NoSuchEntityException $e) {
            $terms = [];
        }
        return $terms;
    }

    /**
     * @return string|null
     */
    public function getCookiePromoCode()
    {
        $promoCode = $this->_promoCodeHelper->getCustomCookie('PromoCode');
        if (!$promoCode && isset($_COOKIE['PromoCode']) && $_COOKIE['PromoCode'] != '') {
            $promoCode = $_COOKIE['PromoCode'];
        }
        return $promoCode;
    }

    /**
     * @param $cookiename
     * @param $value
     * @param int $duration
     */
    public function setCustomCookie($cookiename,$value, $duration = 86400)
    {
        $metadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration($duration)
            ->setPath($this->sessionManager->getCookiePath())
            ->setDomain($this->sessionManager->getCookieDomain())
            ->setHttpOnly(true)
            ->setSecure(true);

        $this->cookieManager->setPublicCookie(
            $cookiename,
            $value,
            $metadata
        );
    }

    public function getCustomCookie($cookiename)
    {
        return $this->cookieManager->getCookie($cookiename);
    }

    public function deleteCustomCookie($cookiename)
    {
        return $this->cookieManager->deleteCookie($cookiename);
    }
}
