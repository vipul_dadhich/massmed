<?php

namespace MMS\PriceEngine\Console;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateFixture
 * @package MMS\PriceEngine\Console
 */
class GenerateFixture extends Command
{
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;
    /**
     * @var \Magento\Framework\Module\Dir
     */
    protected $_directory;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $_json;

    /**
     * GenerateFixture constructor.
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Module\Dir $directory
     * @param \Magento\Framework\Serialize\Serializer\Json $json
     */
    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Module\Dir $directory,
        \Magento\Framework\Serialize\Serializer\Json $json
    ) {
        $this->_filesystem = $filesystem;
        $this->_directory = $directory;
        $this->_json = $json;
        parent::__construct();
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('price-engine:generate:fixture');
        $this->setDescription('Generate test request and response json data');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dirPath = $this->getModuleDataDir();
        $filePath = $dirPath . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'fixtures.json';
        $content = $this->generateSampleFixtures();
        try {
            $this->getDirectoryWrite()->writeFile($filePath, $content);
            $output->writeln("Fixture data is generated.");
        } catch (FileSystemException $e) {
            $output->writeln("Fixture data is failed.");
        }
    }

    /**
     * @return false|string
     */
    protected function generateSampleFixtures()
    {
        $output = [];

        $output['USA|CAT-ANL-DFT|1-year'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Year", "price" => 159.0000, "priceCode" => "PC159"]
            ],
        ];
        $output['USA|CAT-ANL-DFT|2-year'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "2-Year", "price" => 229.0000, "priceCode" => "PC229"]
            ],
        ];
        $output['PHY|USA|CAT-ANL-DFT|1-year'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Year", "price" => 169.0000, "priceCode" => "PC169"]
            ],
        ];
        $output['RES|USA|CAT-ANL-DFT|1-year'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Year", "price" => 169.0000, "priceCode" => "PC169"],
            ],
        ];
        $output['PHY|USA|CAT-ANL-DFT|2-year'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "2-Year", "price" => 239.0000, "priceCode" => "PC239"]
            ],
        ];
        $output['PHY|USA|CAT-ANL-DFT|1-year|2-year'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1 Year", "price" => 169.0000, "priceCode" => "PC169"],
                ["term" => "2 Year", "price" => 239.0000, "priceCode" => "PC239"],
            ],
        ];
        $output['OCI4SMY3|USA|CAT-ANL-DFT|1-year'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Year", "price" => 139.0000, "priceCode" => "PC149"]
            ],
        ];
        $output['OCI4SMY3|USA|CAT-ANL-DFT|2-year'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "2-Year", "price" => 209.0000, "priceCode" => "PC219"]
            ],
        ];
        $output['OCI4SMY3|PHY|USA|CAT-ANL-DFT|1-year'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Year", "price" => 149.0000, "priceCode" => "PC149"]
            ],
        ];
        $output['OCI4SMY3|PHY|USA|CAT-ANL-DFT|2-year'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "2-Year", "price" => 219.0000, "priceCode" => "PC219"]
            ],
        ];
        $output['OCI4SMY3|PHY|USA|CAT-ANL-DFT|1-year|2-year'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1 Year", "price" => 149.0000, "priceCode" => "PC149"],
                ["term" => "2 Year", "price" => 219.0000, "priceCode" => "PC219"],
            ],
        ];
        $output['USA|CAT-MTH|1 month'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Month", "price" => 14.2000, "priceCode" => "REG1M"]
            ],
        ];
        $output['PHY|USA|CAT-MTH|1 month'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Month", "price" => 15.3000, "priceCode" => "REG1M"]
            ],
        ];
        $output['OCI4SMY3|PHY|USA|CAT-MTH|1 month'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Month", "price" => 16.4000, "priceCode" => "REG1M"]
            ],
        ];

        $output['OCI4SMY1|PHY|USA|CAT-ANL-DFT'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Year", "price" => 189.0000, "priceCode" => "PC189"],
                ["term" => "2-Year", "price" => 329.0000, "priceCode" => "PC329"],
            ],
        ];
        $output['OCI4SMY1|PHY|USA|CAT-MTH'] = [
            "sku" => "CAT-MTH",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Month", "price" => 25.0000, "priceCode" => "REG1M"]
            ],
        ];
        $output['OCI4SMY1|USA|CAT-ANL-DFT'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Month", "price" => 25.0000, "priceCode" => "REG1M"]
            ],
        ];
        $output['OCI4SMY2|PHY|USA|CAT-ANL-DFT'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Year", "price" => 179.0000, "priceCode" => "PC179"],
                ["term" => "2-Year", "price" => 319.0000, "priceCode" => "PC319"],
            ],
        ];
        $output['PHY|USA|CAT-ANL-DFT'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Year", "price" => 179.0000, "priceCode" => "PC179"],
            ],
        ];
        $output['OCI4SMY2|PHY|USA|CAT-MTH'] = [
            "sku" => "CAT-MTH",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Month", "price" => 23.0000, "priceCode" => "REG1M"]
            ],
        ];
        $output['OCI4SMY1|RES|USA|CAT-ANL-DFT'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Year", "price" => 169.0000, "priceCode" => "PC169"],
                ["term" => "2-Year", "price" => 309.0000, "priceCode" => "PC309"],
            ],
        ];
        $output['OCI4SMY1|RES|USA|CAT-MTH'] = [
            "sku" => "CAT-MTH",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Month", "price" => 24.0000, "priceCode" => "REG1M"]
            ],
        ];
        $output['OCI4SMY2|RES|USA|CAT-ANL-DFT'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Year", "price" => 159.0000, "priceCode" => "PC159"],
                ["term" => "2-Year", "price" => 299.0000, "priceCode" => "PC299"],
            ],
        ];
        $output['OCI4SMY2|RES|USA|CAT-MTH'] = [
            "sku" => "CAT-MTH",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Month", "price" => 21.0000, "priceCode" => "REG1M"]
            ],
        ];
        $output['USA|CAT-ANL-DFT'] = [
            "sku" => "CAT-ANL-DFT",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Year", "price" => 199.0000, "priceCode" => "PC199"],
                ["term" => "2-Year", "price" => 339.0000, "priceCode" => "PC339"],
            ],
        ];
        $output['USA|CAT-MTH'] = [
            "sku" => "CAT-MTH",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Month", "price" => 35.0000, "priceCode" => "REG1M"]
            ],
        ];
        $output['PHY|USA|CAT-MTH'] = [
            "sku" => "CAT-MTH",
            "currency" => "USD",
            "terms" => [
                ["term" => "1-Month", "price" => 30.0000, "priceCode" => "REG1M"]
            ],
        ];

        return \json_encode($output, JSON_PRETTY_PRINT);
    }

    /**
     * @return string
     */
    public function getModuleDataDir()
    {
        return $this->_directory->getDir(
            'MMS_PriceEngine', \Magento\Framework\Module\Dir::MODULE_ETC_DIR
        );
    }

    /**
     * Returns filesystem directory instance for write operations
     *
     * @return \Magento\Framework\Filesystem\Directory\WriteInterface
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    protected function getDirectoryWrite()
    {
        return $this->_filesystem->getDirectoryWrite(DirectoryList::ROOT);
    }
}
