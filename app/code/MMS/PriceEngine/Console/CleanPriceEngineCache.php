<?php

namespace MMS\PriceEngine\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CleanPriceEngineCache
 * @package MMS\PriceEngine\Console
 */
class CleanPriceEngineCache extends Command
{
    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_typeList;

    /**
     * CleanPriceEngineCache constructor.
     * @param \Magento\Framework\App\Cache\TypeListInterface $typeList
     */
    public function __construct(
        \Magento\Framework\App\Cache\TypeListInterface $typeList
    ) {
        $this->_typeList = $typeList;
        parent::__construct();
    }
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('price-engine:cache:clean');
        $this->setDescription('Clean and flush the price engine cache');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_typeList->invalidate(\MMS\PriceEngine\Model\Cache\Type::PRICE_ENGINE_CACHE_KEY);
        $this->_typeList->cleanType(\MMS\PriceEngine\Model\Cache\Type::PRICE_ENGINE_CACHE_KEY);
    }
}
