<?php

namespace MMS\PriceEngine\Model;

use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use MMS\PriceEngine\Api\IpToCountryRepositoryInterface;
use MMS\PriceEngine\Helper\Data as DataHelper;
use MMS\PriceEngine\Library\IP2LocationService;

/**
 * Class IpToCountryRepository
 * @package MMS\PriceEngine\Model
 */
class IpToCountryRepository implements IpToCountryRepositoryInterface
{
    /**
     * @var array
     */
    protected $ipToCountry = [];

    /**
     * @var DataHelper
     */
    protected $_dataHelper;
    /**
     * @var RemoteAddress
     */
    protected $_remoteAddress;
    /**
     * @var IP2LocationService
     */
    protected $_IP2LocationService;

    /**
     * IpToCountryRepository constructor.
     * @param DataHelper $dataHelper
     * @param RemoteAddress $remoteAddress
     * @param IP2LocationService $IP2LocationService
     */
    public function __construct(
        DataHelper $dataHelper,
        RemoteAddress $remoteAddress,
        IP2LocationService $IP2LocationService
    ) {
        $this->_dataHelper = $dataHelper;
        $this->_remoteAddress = $remoteAddress;
        $this->_IP2LocationService = $IP2LocationService;
    }

    /**
     * @inheritDoc
     */
    public function getCountryCode($ip)
    {
        if (!$this->_dataHelper->isEnabledGeo()) {
            return false;
        }

        if (!isset($this->ipToCountry[$ip])) {
            $this->ipToCountry[$ip] = false;

            if (!$this->ipToCountry[$ip]) {
                if (function_exists('geoip_country_code_by_name')) {
                    $rf = new \ReflectionFunction('geoip_country_code_by_name');
                    $params = $rf->getParameters();
                    if (!$params || !is_array($params) || count($params) < 2) { /* Fix for custom geoip php libraries, so 0 or 1 params */
                        try {
                            $this->ipToCountry[$ip] = geoip_country_code_by_name($ip);
                        } catch (\Exception $e) {
                        }
                    }
                }
            }

            if (!$this->ipToCountry[$ip]) {
                try {
                    $countryCode = $this->_IP2LocationService->getCountryCode($ip);
                    if ($countryCode) {
                        $this->ipToCountry[$ip] = $countryCode;
                    }
                } catch (\Exception $e) {
                }
            }
        }

        return $this->ipToCountry[$ip];
    }

    /**
     * @inheritDoc
     */
    public function getVisitorCountryCode()
    {
        return $this->getCountryCode($this->getRemoteAddress());
    }

    /**
     * @inheritDoc
     */
    public function getRemoteAddress()
    {
        if ($simulateIP = $this->_dataHelper->getGeoLocationSimulateIP()) {
            return $simulateIP;
        }
        return $this->_remoteAddress->getRemoteAddress();
    }
}
