<?php

namespace MMS\PriceEngine\Model\Cache;
use Magento\Framework\App\Cache\Type\FrontendPool;

/**
 * Class Type
 * @package MMS\PriceEngine\Model\Cache
 */
class Type extends \Magento\Framework\Cache\Frontend\Decorator\TagScope
{
    /**
     * Cache type code unique among all cache types
     */
    const TYPE_IDENTIFIER = 'mms_price_engine_cache';

    /**
     * Cache tag used to distinguish the cache type from all other cache
     */
    const CACHE_TAG = 'MMS_PRICE_ENGINE_CACHE';

    const PRICE_ENGINE_CACHE_KEY = 'mms_price_engine_cache';

    /**
     * @param FrontendPool $cacheFrontendPool
     */
    public function __construct(
        FrontendPool $cacheFrontendPool
    ) {
        parent::__construct($cacheFrontendPool->get(self::TYPE_IDENTIFIER), self::CACHE_TAG);
    }
}
