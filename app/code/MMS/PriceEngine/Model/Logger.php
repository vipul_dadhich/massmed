<?php
/**
 * @package     MMS_PriceEngine
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\PriceEngine\Model;

use MMS\Logger\LoggerInterface;
use MMS\PriceEngine\Helper\Data as PriceDataHelper;
use Magento\Framework\App\RequestInterface;

/**
 * MMS Logger implementation
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Logger extends \MMS\Logger\Model\Logger implements LoggerInterface
{
    /**
     * @var  PriceDataHelper
     */
    public $_priceEngineHelper;

    /**
     * @var  RequestInterface
     */
    public $_request;

    /**
     * Logger constructor.
     * @param \MMS\Logger\Helper\Data $dataHelper
     * @param PriceDataHelper $priceEngineHelper
     * @param RequestInterface $request
     * @param array $handlers
     * @param array $processors
     */
    public function __construct(
        \MMS\Logger\Helper\Data $dataHelper,
        PriceDataHelper $priceEngineHelper,
        RequestInterface $request,
        array $handlers = [],
        array $processors = []
    ) {
        $this->_priceEngineHelper = $priceEngineHelper;
        $this->_dataHelper = $dataHelper;
        $this->_request = $request;
        parent::__construct($dataHelper, $handlers, $processors);
    }

    /**
     * Adds a log record at the DEBUG level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param  string  $message The log message
     * @param  array   $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function debug($message, array $context = [])
    {
        if (!$this->_priceEngineHelper->isLogEnable()) {
            return false;
        }

        return parent::debug($message, $context);
    }

    /**
     * Adds a log record at the INFO level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param  string  $message The log message
     * @param  array   $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function info($message, array $context = [])
    {
        if (!$this->_priceEngineHelper->isLogEnable()) {
            return false;
        }

        return parent::info($message, $context);
    }

    /**
     * Adds a log record at the WARNING level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param  string  $message The log message
     * @param  array   $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function warning($message, array $context = [])
    {
        if (!$this->_priceEngineHelper->isLogEnable()) {
            return false;
        }

        return parent::warning($message, $context);
    }

    /**
     * Adds a log record at the CRITICAL level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param  string  $message The log message
     * @param  array   $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function critical($message, array $context = [])
    {
        if (!$this->_priceEngineHelper->isLogEnable()) {
            return false;
        }
        return parent::critical($message, $context);
    }

    /**
     * @return \MMS\Logger\Helper\Data
     */
    public function getLoggerHelper()
    {
        return $this->_dataHelper;
    }

    /**
     * @return RequestInterface
     */
    public function getRequest()
    {
        return $this->_request;
    }
}
