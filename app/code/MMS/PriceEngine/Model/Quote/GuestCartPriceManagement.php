<?php

namespace MMS\PriceEngine\Model\Quote;

use Exception;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Framework\DataObject;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Quote\Model\ResourceModel\Quote;
use MMS\Logger\LoggerInterface;
use MMS\PriceEngine\Api\Quote\GuestCartPriceManagementInterface;
use MMS\PriceEngine\Service\PriceEngine as PriceEngineService;
use Magento\Framework\Message\ManagerInterface;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;

/**
 * Class GuestCartPriceManagement
 * @package MMS\PriceEngine\Model\Quote
 */
class GuestCartPriceManagement implements GuestCartPriceManagementInterface
{
    /**
     * @var CustomerCart
     */
    protected $_cart;
    /**
     * @var PriceEngineService
     */
    protected $_priceEngineService;
    /**
     * @var ManagerInterface
     */
    protected $_messageManager;
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
    /**
     * @var Quote
     */
    protected $_quoteResource;
    /**
     * @var SessionCurrencyEngine
     */
    protected $_session;
    /**
     * @var LoggerInterface
     */
    protected $_mmsLogger;

    /**
     * GuestCartPriceManagement constructor.
     * @param ObjectManagerInterface $objectManager
     * @param CustomerCart $cart
     * @param PriceEngineService $priceEngine
     * @param ManagerInterface $messageManager
     * @param Quote $quoteResource
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        CustomerCart $cart,
        PriceEngineService $priceEngine,
        ManagerInterface $messageManager,
        Quote $quoteResource,
        SessionCurrencyEngine $session,
        LoggerInterface $mmsLogger
    ) {
        $this->_objectManager = $objectManager;
        $this->_cart = $cart;
        $this->_priceEngineService = $priceEngine;
        $this->_messageManager = $messageManager;
        $this->_quoteResource = $quoteResource;
        $this->_session = $session;
        $this->_mmsLogger = $mmsLogger;
    }

    /**
     * @inheritDoc
     */
    public function updateCartItem(string $cartId, ?string $suffix, ?string $role, ?string $profCat)
    {
        try {
            $this->_priceEngineService->getCheckoutSession()->setCheckoutPriceUpdateFlag(true);
            $this->_priceEngineService->getCheckoutSession()->setCheckoutSuffixText($suffix);
            $this->_priceEngineService->getCheckoutSession()->setCheckoutRoleText($role);
            $this->_priceEngineService->getCheckoutSession()->setCheckoutProfessionalCatText($profCat);
            $this->_session->addProfessionalCategory($profCat);
            $quote = $this->_priceEngineService->getCheckoutSession()->getQuote();
            $items = $quote->getAllVisibleItems();
            foreach($items as $item) {
                $item = ($item->getParentItem() ? $item->getParentItem() : $item);
                $customPrice = $this->_priceEngineService->getQuoteItemPrice($item);
                if ($customPrice) {
                    $item->setCustomPrice($customPrice);
                    $item->setOriginalCustomPrice($customPrice);
                    $item->getProduct()->setIsSuperMode(true);
                }
            }

            $quote->setTotalsCollectedFlag(false);
            $quote->collectTotals();
            $this->_quoteResource->save($quote);
        } catch (LocalizedException $e) {
            $messages = array_unique(explode("\n", $e->getMessage()));
            foreach ($messages as $message) {
                $this->_mmsLogger->info($message);
            }
        } catch (Exception $e) {
            $this->_mmsLogger->info($e->getMessage());
            $this->_mmsLogger->info(__('We can\'t update the item right now.'));
        }
    }
}
