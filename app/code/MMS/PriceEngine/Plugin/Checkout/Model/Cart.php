<?php

namespace MMS\PriceEngine\Plugin\Checkout\Model;

use Magento\Framework\Exception\StateException;
use MMS\PriceEngine\Service\PriceEngine;

/**
 * Class Cart
 * @package MMS\PriceEngine\Plugin\Checkout\Model
 */
class Cart
{
    /**
     * @var PriceEngine
     */
    protected $_priceEngineService;

    /**
     * FinalPrice constructor.
     * @param PriceEngine $priceEngineService
     */
    public function __construct(
        PriceEngine $priceEngineService
    ) {
        $this->_priceEngineService = $priceEngineService;
    }

    /**
     * @param \Magento\Checkout\Model\Cart $subject
     * @param $productInfo
     * @param null $requestInfo
     * @return array
     * @throws StateException
     */
    public function beforeAddProduct(
        \Magento\Checkout\Model\Cart $subject,
        $productInfo,
        $requestInfo = null
    ) {
        if (!$this->validate($productInfo, $requestInfo)) {
            $error = 'Something went wrong, please try to subscribe now again.';
            throw new StateException(__($error));
        }

        $this->_priceEngineService->getPriceLogger()->debug("*********** Before Adding Product to Cart Price Plugin ***********");
        $this->_priceEngineService->getPriceLogger()->debug([
            "action" => "beforeAddProduct",
            'sku' => $productInfo->getSku(),
            "customer" => $this->_priceEngineService->getCustomer() ? $this->_priceEngineService->getCustomer()->getId() : null,
            //"terms" => $terms,
            //"custom_price" => $result,
            "promocode" => $this->_priceEngineService->getPromoCode()
        ]);


        return [$productInfo, $requestInfo];
    }

    /**
     * @param $productInfo
     * @param $requestInfo
     * @return bool
     */
    protected function validate($productInfo, $requestInfo)
    {
        return true;
    }
}

