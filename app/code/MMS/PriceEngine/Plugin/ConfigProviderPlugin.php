<?php

namespace MMS\PriceEngine\Plugin;

use MMS\PriceEngine\Service\PriceEngine;

/**
 * Class ConfigProviderPlugin
 * @package MMS\PriceEngine\Plugin
 */
class ConfigProviderPlugin
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
    /**
     * @var \MMS\Renew\Helper\Config
     */
    protected $_configRenew;
    /**
     * @var PriceEngine
     */
    protected $_priceEngineService;

    /**
     * ConfigProviderPlugin constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \MMS\Renew\Helper\Config $configRenew
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \MMS\Renew\Helper\Config $configRenew,
        PriceEngine $priceEngineService
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_configRenew = $configRenew;
        $this->_priceEngineService = $priceEngineService;
    }

    /**
     * @param \Magento\Checkout\Model\DefaultConfigProvider $subject
     * @param array $result
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, array $result)
    {
        if ($this->_priceEngineService->isEnabled()) {
            /** @var $quote \Magento\Quote\Model\Quote */
            $quote = $this->getCheckoutSession()->getQuote();
            if ($this->_configRenew->isEnabled()
                && $quote->getData('is_quote_renew')
            ) {
                if (isset($result['renewalRates']) && !empty($result['renewalRates'])) {
                    $sku = [];
                    $terms = [];
                    if (isset($result['renewalRates'][0]['options'])) {
                        foreach ($result['renewalRates'][0]['options'] as $option) {
                            $quoteItem = $quote->getItemById($option['quote_item_id']);
                            if ($quoteItem && $quoteItem->getId()) {
                                $sku = $quoteItem->getSku();
                            }
                            $term = $option['default_title'];
                            if ($this->_priceEngineService->isEnabledFixtureMode()) {
                                $term = str_replace(" ", "-", $term);
                            }
                            $terms[] = $term;
                        }
                    }
                    $payload['sku'] = $sku;
                    if (!empty($terms)) {
                        $payload['term'] = $terms;
                    }

                    $this->_priceEngineService->setFullTermsResponse(true);

                    $this->_priceEngineService->setRequest($payload);
                    $this->_priceEngineService->buildPrice();
                    $response = $this->_priceEngineService->getPrice();
                    if (isset($response['terms']) && !empty($response['terms'])) {
                        foreach ($response['terms'] as $term) {
                            foreach ($result['renewalRates'][0]['options'] as $key => &$option) {
                                if (strtolower($option['default_title']) == strtolower($term['term'])) {
                                    $option['default_custom_price'] = $term['price'];
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}
