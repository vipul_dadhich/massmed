<?php

namespace MMS\PriceEngine\Plugin\FullPageCache;

use Magento\Framework\App\Http\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\PageCache\Model\Config;
use MMS\PriceEngine\Service\PriceEngine;

/**
 * Class Identifier
 * @package MMS\PriceEngine\Plugin\FullPageCache
 */
class Identifier
{
    public $allowedIdentifier = ['catalyst'];

    /**
     * @var Http
     */
    protected $_request;
    /**
     * @var Context
     */
    protected $_context;
    /**
     * @var Json
     */
    protected $_serializer;
    /**
     * @var Config
     */
    protected $_configPageCache;
    /**
     * @var PriceEngine
     */
    protected $_priceEngineService;

    /**
     * Identifier constructor.
     * @param Http $request
     * @param Context $context
     * @param Json $serializer
     * @param Config $configPageCache
     * @param PriceEngine $priceEngineService
     */
    public function __construct(
        Http $request,
        Context $context,
        Json $serializer,
        Config $configPageCache,
        PriceEngine $priceEngineService
    ) {
        $this->_request = $request;
        $this->_context = $context;
        $this->_serializer = $serializer;
        $this->_configPageCache = $configPageCache;
        $this->_priceEngineService = $priceEngineService;
    }

    /**
     * @param \Magento\Framework\App\PageCache\Identifier $identifier
     * @param $result
     * @return string
     */
    public function afterGetValue(\Magento\Framework\App\PageCache\Identifier $identifier, $result)
    {
        if ($this->_configPageCache->isEnabled() && $this->_priceEngineService->isEnabled()) {
            $urlString = $this->_request->getUriString();
            $extendFpc = $this->_priceEngineService->isExtendFullPageCache();
            if ($this->strPosA($urlString, $this->allowedIdentifier, 1) || $extendFpc) {
                $data = [
                    $this->_request->isSecure(),
                    $this->_request->getUriString(),
                    $this->_request->get(\Magento\Framework\App\Response\Http::COOKIE_VARY_STRING)
                        ?: $this->_context->getVaryString(),
                    sha1($this->_serializer->serialize($this->_priceEngineService->getCacheKey()))
                ];

                return sha1($this->_serializer->serialize($data));
            }
        }

        return $result;
    }

    /**
     * @param $haystack
     * @param array $needles
     * @param int $offset
     * @return false|mixed
     */
    public function strPosA($haystack, $needles = [], $offset = 0) {
        $chr = [];
        foreach($needles as $needle) {
            $res = strpos($haystack, $needle, $offset);
            if ($res !== false) $chr[$needle] = $res;
        }
        if(empty($chr)) return false;
        return min($chr);
    }
}
