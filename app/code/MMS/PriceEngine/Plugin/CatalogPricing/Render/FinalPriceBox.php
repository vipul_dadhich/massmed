<?php

namespace MMS\PriceEngine\Plugin\CatalogPricing\Render;

use Magento\Framework\Serialize\Serializer\Json;
use MMS\PriceEngine\Service\PriceEngine;

/**
 * Class FinalPriceBox
 * @package MMS\PriceEngine\Plugin\CatalogPricing\Render
 */
class FinalPriceBox
{
    /**
     * @var PriceEngine
     */
    protected $_priceEngineService;
    /**
     * @var Json
     */
    protected $_serializer;

    /**
     * FinalPriceBox constructor.
     * @param PriceEngine $priceEngineService
     * @param Json $serializer
     */
    public function __construct(
        PriceEngine $priceEngineService,
        Json $serializer
    ) {
        $this->_priceEngineService = $priceEngineService;
        $this->_serializer = $serializer;
    }

    /**
     * Get Key for caching block content
     *
     * @param \Magento\Catalog\Pricing\Render\FinalPriceBox $subject
     * @param $cacheKey
     * @return string
     */
    public function afterGetCacheKey(\Magento\Catalog\Pricing\Render\FinalPriceBox $subject, $cacheKey)
    {
        if ($this->_priceEngineService->isEnabled()) {
            $cacheKey .= sha1($this->_serializer->serialize($this->_priceEngineService->getCacheKey()));
        }

        return $cacheKey;
    }

    /**
     * Get cache key informative items
     *
     * @param \Magento\Catalog\Pricing\Render\FinalPriceBox $subject
     * @param $cacheKeys
     * @return mixed
     */
    public function afterGetCacheKeyInfo(\Magento\Catalog\Pricing\Render\FinalPriceBox $subject, $cacheKeys)
    {
        if ($this->_priceEngineService->isEnabled()) {
            $cacheKeys = array_merge($cacheKeys, $this->_priceEngineService->getCacheKey());
        }

        return $cacheKeys;
    }
}
