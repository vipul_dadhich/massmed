<?php

namespace MMS\PriceEngine\Plugin;

use MMS\PriceEngine\Service\PriceEngine;

/**
 * Class FinalPrice
 * @package MMS\PriceEngine\Plugin
 */
class FinalPrice
{
    /**
     * @var PriceEngine
     */
    protected $_priceEngineService;

    /**
     * FinalPrice constructor.
     * @param PriceEngine $priceEngineService
     */
    public function __construct(
        PriceEngine $priceEngineService
    ) {
        $this->_priceEngineService = $priceEngineService;
    }

    /**
     * @param \Magento\Catalog\Pricing\Price\FinalPrice $subject
     * @param float $result
     * @return float
     */
    public function afterGetValue(\Magento\Catalog\Pricing\Price\FinalPrice $subject, $result)
    {
        if ($this->_priceEngineService->isEnabled()) {
            /*$payload = $this->_priceEngineService->sampleRequestPayload();*/
            $payload = [
                'sku' => $subject->getProduct()->getSku()
            ];

            $terms = $this->_priceEngineService->getProductDefaultTerms($subject->getProduct()->getId());
            if (!empty($terms)) {
                $term = $terms[0];
                if ($this->_priceEngineService->isEnabledFixtureMode()) {
                    $term = str_replace(" ", "-", $term);
                }
                $payload['term'] = $term;
            }

            $this->_priceEngineService->setRequest($payload);
            $this->_priceEngineService->buildPrice();
            $customApiPrice = $this->_priceEngineService->getPrice();
            if ($customApiPrice) {
                $result = $customApiPrice;
            }

            $this->_priceEngineService->getPriceLogger()->debug("*********** Init Final Price Plugin ***********");
            $this->_priceEngineService->getPriceLogger()->debug([
                "action" => "afterGetValue",
                'sku' => $subject->getProduct()->getSku(),
                "customer" => $this->_priceEngineService->getCustomer() ? $this->_priceEngineService->getCustomer()->getId() : null,
                "terms" => $terms,
                "payload" => $payload,
                "custom_price" => $result,
                "promocode" => $this->_priceEngineService->getPromoCode('promo')
            ]);
            $this->_priceEngineService->getPriceLogger()->debug("*********** End FinalPrice Plugin ***********");
        }

        return $result;
    }
}
