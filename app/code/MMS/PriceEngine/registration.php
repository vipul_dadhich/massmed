<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_PriceEngine extension
 * NOTICE OF LICENSE
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'MMS_PriceEngine',
    __DIR__
);
