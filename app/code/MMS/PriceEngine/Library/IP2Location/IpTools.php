<?php

namespace MMS\PriceEngine\Library\IP2Location;

/**
 * Class IpTools
 * @package MMS\PriceEngine\Library
 */
class IpTools
{
    /**
     * @param $ip
     * @return bool
     */
    public function isIpv4($ip)
    {
        return (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) ? true : false;
    }

    /**
     * @param $ip
     * @return bool
     */
    public function isIpv6($ip)
    {
        return (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) ? true : false;
    }

    /**
     * @param $ip
     * @return string|void
     */
    public function ipv4ToDecimal($ip)
    {
        if (!$this->isIpv4($ip)) {
            return;
        }

        return sprintf('%u', ip2long($ip));
    }

    /**
     * @param $ipv6
     * @return string|void
     */
    public function ipv6ToDecimal($ipv6)
    {
        if (!$this->isIpv6($ipv6)) {
            return;
        }

        return (string) gmp_import(inet_pton($ipv6));
    }

    /**
     * @param $number
     * @return string|void
     */
    public function decimalToIpv4($number)
    {
        if (!preg_match('/^\d+$/', $number)) {
            return;
        }

        if ($number > 4294967295) {
            return;
        }

        return long2ip($number);
    }

    /**
     * @param $number
     * @return false|string|void
     */
    public function decimalToIpv6($number)
    {
        if (!preg_match('/^\d+$/', $number)) {
            return;
        }

        if ($number <= 4294967295) {
            return;
        }

        return inet_ntop(str_pad(gmp_export($number), 16, "\0", STR_PAD_LEFT));
    }
}
