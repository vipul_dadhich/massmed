<?php

namespace MMS\PriceEngine\Api\Quote;
/**
 * Interface GuestCartPriceManagementInterface
 * @package MMS\PriceEngine\Api\Quote
 */
interface GuestCartPriceManagementInterface
{
    /**
     * Handle custom price to professional category
     *
     * @param string $cartId The shopping cart ID.
     * @param string|null $suffix
     * @param string|null $role
     * @param string|null $profCat
     * @return void
     */
    public function updateCartItem(string $cartId, ?string $suffix, ?string $role, ?string $profCat);
}
