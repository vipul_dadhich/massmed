<?php

namespace MMS\PriceEngine\Api;

/**
 * Interface IpToCountryRepositoryInterface
 * @package MMS\PriceEngine\Api
 */
interface IpToCountryRepositoryInterface
{
    /**
     * @param $ip
     * @return string|bool
     */
    public function getCountryCode($ip);

    /**
     * Retrieve current visitor country code by IP
     *
     * @return string|bool
     */
    public function getVisitorCountryCode();

    /**
     * Retrieve current IP
     *
     * @return string
     */
    public function getRemoteAddress();
}
