define([
    'jquery',
    'MMS_PriceEngine/js/price_engine'
], function (
    $,
    priceEngineJS
) {
    'use strict';

    return function (Component) {
        return Component.extend({
            /**
             * @returns {self}
             */
            initialize: function () {
                this._super();
                priceEngineJS._init();
            }
        });
    };
});
