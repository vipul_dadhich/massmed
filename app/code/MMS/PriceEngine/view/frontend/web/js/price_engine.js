define([
    'jquery',
    'Magento_Checkout/js/model/url-builder',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/action/get-totals',
    'mage/storage',
    'Magento_Ui/js/lib/view/utils/async'
], function (
    $,
    urlBuilder,
    customer,
    customerData,
    quote,
    fullScreenLoader,
    getTotalsAction,
    storage
) {
    'use strict';

    var priceEngineJS,
        piSuffixSelector = '#pi_suffix',
        piRoleSelector = '#pi_role',
        piProfCatSelector = '#pi_professional_category';

    return {
        _init: function () {
            if (!customer.isLoggedIn()) {
                this._listen();
            }
        },
        _listen: function() {
            var self = this;
            $.async(piSuffixSelector, function(element) {
                $(element).on('change', function() {
                    self.updateCartItemPrice();
                });
            });

            $.async(piProfCatSelector, function(element) {
                $(element).on('change', function() {
                    self.updateCartItemPrice(true);
                });
            });

            $.async(piRoleSelector, function(element) {
                $(element).on('change', function() {
                    self.updateCartItemPrice();
                });
            });
        },
        updateCartItemPrice: function () {
            let suffixVal = $(piSuffixSelector).val(),
                roleVal = $(piRoleSelector).val(),
                profCat = $(piProfCatSelector).val(),
                url = '/guest-carts/:cartId/update-item-price',
                urlParams = {
                    cartId: quote.getQuoteId()
                },
                serviceUrl;


            if ((suffixVal !== '' && roleVal !== '') || profCat !== '') {
                var payload = {
                    cartId: quote.getQuoteId(),
                    suffix: suffixVal,
                    role: roleVal,
                    profCat: profCat
                }

                serviceUrl = urlBuilder.createUrl(url, urlParams);

                fullScreenLoader.startLoader();
                return storage.post(
                    serviceUrl,
                    JSON.stringify(payload)
                ).done(
                    function (response) {
                        customerData.reload(['cart'], true);
                        var deferred = $.Deferred();
                        getTotalsAction([], deferred);
                        fullScreenLoader.stopLoader();
                    }
                ).fail(
                    function (response) {
                        fullScreenLoader.stopLoader();
                    }
                );
            }
        }
    };
});
