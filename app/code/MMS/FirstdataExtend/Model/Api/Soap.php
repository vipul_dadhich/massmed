<?php

namespace MMS\FirstdataExtend\Model\Api;

use Magedelight\Firstdata\Model\Config;
use Magento\Directory\Model\CountryFactory;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManager;

/**
 * Class Soap
 * @package MMS\FirstdataExtend\Model\Api
 */
class Soap extends \Magedelight\Firstdata\Model\Api\Soap
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * Soap constructor.
     * @param Config $configModel
     * @param RegionFactory $regionFactory
     * @param CountryFactory $countryFactory
     * @param StoreManager $storeManager
     * @param RequestInterface $httpRequest
     * @param ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        Config $configModel,
        RegionFactory $regionFactory,
        CountryFactory $countryFactory,
        StoreManager $storeManager,
        RequestInterface $httpRequest,
        ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($configModel, $regionFactory, $countryFactory, $storeManager, $httpRequest, $data);
    }

    /**
     * @param array $req
     *
     * @return array
     */
    protected function _buildRequest($req)
    {
        $request = parent::_buildRequest($req);
        if ($this->isCurrencyEnabled() && isset($request['Currency']) && $request['Currency'] === '') {
            $request['Currency'] = $this->getCurrentCurrencyCode();
        }
        return $request;
    }

    /**
     * @return bool
     */
    protected function isCurrencyEnabled()
    {
        return $this->_scopeConfig->isSetFlag('payment/md_firstdata/currency_enabled');
    }

    /**
     * Gets customer's current currency
     *
     * @param null $store
     * @return string
     */
    protected function getCurrentCurrencyCode($store = null)
    {
        try {
            return $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        } catch (NoSuchEntityException | LocalizedException $e) {
            return '';
        }
    }
}
