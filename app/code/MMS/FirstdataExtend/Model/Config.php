<?php

namespace MMS\FirstdataExtend\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use MMS\FirstdataExtend\Helper\Data as ExtendDataHelper;

/**
 * Class Config
 * @package MMS\FirstdataExtend\Model
 */
class Config extends \Magedelight\Firstdata\Model\Config
{
    /**
     * @var ExtendDataHelper
     */
    protected $_extendDataHelper;
    /**
     * Customer current currency code
     * @var mixed
     */
    private $currentCurrentCode;

    /**
     * Config constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Model\Session\Quote $quoteSession
     * @param \Magento\Backend\Model\Session $adminsession
     * @param \Magento\Framework\Encryption\Encryptor $encryptor
     * @param \Magento\Customer\Model\Config\Share $customerScope
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param ScopeConfigInterface $scopeConfig
     * @param ExtendDataHelper $extendDataHelper
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Model\Session\Quote $quoteSession,
        \Magento\Backend\Model\Session $adminsession,
        \Magento\Framework\Encryption\Encryptor $encryptor,
        \Magento\Customer\Model\Config\Share $customerScope,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Sales\Api\Data\OrderInterface $order,
        ScopeConfigInterface $scopeConfig,
        ExtendDataHelper $extendDataHelper
    ) {
        parent::__construct(
            $storeManager,
            $registry,
            $quoteSession,
            $adminsession,
            $encryptor,
            $customerScope,
            $request,
            $order,
            $scopeConfig
        );

        $this->_extendDataHelper = $extendDataHelper;
        $this->currentCurrentCode = strtolower($this->getCurrentCurrencyCode());
    }

    /**
     * This method will return gateway id set by admin in configuration.
     *
     * @return false|mixed|string
     */
    public function getGatewayId()
    {
        if ($this->_extendDataHelper->getGatewayId($this->currentCurrentCode)) {
            return $this->_extendDataHelper->getGatewayId($this->currentCurrentCode);
        }
        return parent::getGatewayId();
    }

    /**
     * This method will return gateway password set by admin in configuration.
     *
     * @return false|mixed|string
     */
    public function getGatewayPass()
    {
        if ($this->_extendDataHelper->getGatewayPass($this->currentCurrentCode)) {
            return $this->_extendDataHelper->getGatewayPass($this->currentCurrentCode);
        }
        return parent::getGatewayPass();
    }

    /**
     * This method will return gateway password set by admin in configuration.
     *
     * @return false|mixed|string
     */
    public function getKeyId()
    {
        if ($this->_extendDataHelper->getKeyId($this->currentCurrentCode)) {
            return $this->_extendDataHelper->getKeyId($this->currentCurrentCode);
        }
        return parent::getKeyId();
    }

    /**
     * This method will return gateway password set by admin in configuration.
     *
     * @return false|mixed|string
     */
    public function getHmacKey()
    {
        if ($this->_extendDataHelper->getHmacKey($this->currentCurrentCode)) {
            return $this->_extendDataHelper->getHmacKey($this->currentCurrentCode);
        }
        return parent::getHmacKey();
    }

    /**
     * Gets customer's current currency
     *
     * @param null $store
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCurrentCurrencyCode($store = null)
    {
        return $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
    }
}
