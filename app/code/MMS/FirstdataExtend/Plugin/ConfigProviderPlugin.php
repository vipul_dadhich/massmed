<?php

namespace MMS\FirstdataExtend\Plugin;
/**
 * Class ConfigProviderPlugin
 * @package MMS\FirstdataExtend\Plugin
 */
class ConfigProviderPlugin
{
    /**
     * Scope config.
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * ConfigProviderPlugin constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param \Magedelight\Firstdata\Model\ConfigProvider $subject
     * @param array $result
     * @return array
     */
    public function afterGetConfig(
        \Magedelight\Firstdata\Model\ConfigProvider $subject,
        array $result
    ) {
        if ($this->isHideSavedCards()) {
            $result['payment']['md_firstdata']['canSaveCard'] = true;
            $result['payment']['md_firstdata']['storedCards'] = [];
            $result['payment']['md_firstdata']['storedCards']['new'] = 'Use other card';
        }

        return $result;
    }

    /**
     * @return bool
     */
    protected function isHideSavedCards()
    {
        return (bool) $this->_scopeConfig->isSetFlag('payment/md_firstdata/hide_saved_card');
    }
}
