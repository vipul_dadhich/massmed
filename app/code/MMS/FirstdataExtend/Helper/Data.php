<?php

namespace MMS\FirstdataExtend\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Encryption\Encryptor;

/**
 * Class Data
 * @package MMS\FirstdataExtend\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Encryptor
     */
    protected $_encryptor;

    /**
     * Data constructor.
     * @param Context $context
     * @param Encryptor $encryptor
     */
    public function __construct(
        Context $context,
        Encryptor $encryptor
    ) {
        $this->_encryptor = $encryptor;
        parent::__construct($context);
    }

    /**
     * @param $currentCode
     * @return array
     */
    public function getTerminalAccessByCurrencyCode($currentCode)
    {
        return [
            'gateway_id' => $this->getGatewayId($currentCode),
            'gateway_pass' => $this->getGatewayPass($currentCode),
            'key_id' => $this->getKeyId($currentCode),
            'hmac_key' => $this->getHmacKey($currentCode),
        ];
    }

    /**
     * @param $currentCode
     * @param null $scopeCode
     * @return mixed
     */
    public function getGatewayId($currentCode, $scopeCode = null)
    {
        try {
            return trim($this->_encryptor->decrypt(
                $this->scopeConfig->getValue(
                    'md_firstdata/' . $currentCode . '_terminal/gateway_id',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $scopeCode
                )
            ));
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $currentCode
     * @param null $scopeCode
     * @return mixed
     */
    public function getGatewayPass($currentCode, $scopeCode = null)
    {
        try {
            return trim($this->_encryptor->decrypt(
                $this->scopeConfig->getValue(
                    'md_firstdata/' . $currentCode . '_terminal/gateway_pass',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $scopeCode
                )
            ));
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $currentCode
     * @param null $scopeCode
     * @return mixed
     */
    public function getKeyId($currentCode, $scopeCode = null)
    {
        try {
            return trim($this->_encryptor->decrypt(
                $this->scopeConfig->getValue(
                    'md_firstdata/' . $currentCode . '_terminal/key_id',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $scopeCode
                )
            ));
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $currentCode
     * @param null $scopeCode
     * @return mixed
     */
    public function getHmacKey($currentCode, $scopeCode = null)
    {
        try {
            return trim($this->_encryptor->decrypt(
                $this->scopeConfig->getValue(
                    'md_firstdata/' . $currentCode . '_terminal/hmac_key',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $scopeCode
                )
            ));
        } catch (\Exception $e) {
            return false;
        }
    }
}
