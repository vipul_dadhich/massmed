<?php

namespace MMS\CurrencyEngine\App\Action\Plugin;

use Magento\Framework\App\Action\AbstractAction;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\NotFoundException;
use MMS\CurrencyEngine\Model\Session;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Api\StoreCookieManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use MMS\PriceEngine\Service\PriceEngine;

/**
 * Class Context
 * @package MMS\CurrencyEngine\App\Action\Plugin
 */
class Context
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var HttpContext
     */
    protected $httpContext;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var StoreCookieManagerInterface
     */
    protected $storeCookieManager;

    /**
     * @var PriceEngine
     */
    protected $_priceEngine;

    /**
     * @param Session $session
     * @param HttpContext $httpContext
     * @param StoreManagerInterface $storeManager
     * @param StoreCookieManagerInterface $storeCookieManager
     */
    public function __construct(
        Session $session,
        HttpContext $httpContext,
        StoreManagerInterface $storeManager,
        StoreCookieManagerInterface $storeCookieManager,
        PriceEngine $_priceEngine
    ) {
        $this->session      = $session;
        $this->httpContext  = $httpContext;
        $this->storeManager = $storeManager;
        $this->storeCookieManager = $storeCookieManager;
        $this->_priceEngine = $_priceEngine;
    }

    /**
     * @param AbstractAction $subject
     * @param RequestInterface $request
     * @throws NotFoundException
     */
    public function beforeDispatch(
        AbstractAction $subject,
        RequestInterface $request
    ) {
        /** @var string|array|null $storeCode */
        $storeCode = $request->getParam(
            StoreManagerInterface::PARAM_NAME,
            $this->storeCookieManager->getStoreCodeFromCookie()
        );
        if (is_array($storeCode)) {
            if (!isset($storeCode['_data']['code'])) {
                $this->processInvalidStoreRequested($request);
            }
            $storeCode = $storeCode['_data']['code'];
        }
        if ($storeCode === '') {
            //Empty code - is an invalid code and it was given explicitly
            //(the value would be null if the code wasn't found).
            $this->processInvalidStoreRequested($request);
        }
        try {
            $currentStore = $this->storeManager->getStore($storeCode);
            $this->updateContext($request, $currentStore);
        } catch (NoSuchEntityException $exception) {
            $this->processInvalidStoreRequested($request, $exception);
        }
    }

    /**
     * Take action in case of invalid store requested.
     *
     * @param RequestInterface $request
     * @param NoSuchEntityException|null $previousException
     * @return void
     * @throws NotFoundException
     */
    private function processInvalidStoreRequested(
        RequestInterface $request,
        NoSuchEntityException $previousException = null
    ) {
        $store = $this->storeManager->getStore();
        $this->updateContext($request, $store);

        throw new NotFoundException(
            $previousException
                ? __($previousException->getMessage())
                : __('Invalid store requested.'),
            $previousException
        );
    }

    /**
     * Update context accordingly to the store found.
     *
     * @param RequestInterface $request
     * @param StoreInterface $store
     * @return void
     */
    private function updateContext(RequestInterface $request, StoreInterface $store)
    {
        $currencyKey = \MMS\CurrencyEngine\Model\Context::CONTEXT_CURRENCY;
        $countryKey = \MMS\CurrencyEngine\Model\Context::CONTEXT_COUNTRY;
        $profCatKey = \MMS\CurrencyEngine\Model\Context::CONTEXT_PROFESSIONAL_CATEGORY;

        if ($this->httpContext->getValue($currencyKey) === null) {
            $this->httpContext->setValue(
                $currencyKey,
                $this->session->getCurrency(),
                $this->_priceEngine->getCurrencyCode()
            );
        }
        if ($this->httpContext->getValue($countryKey) === null) {
            $this->httpContext->setValue(
                $countryKey,
                $this->session->getCountry(),
                'USA'
            );
        }

        if ($this->httpContext->getValue($profCatKey) === null) {
            /** Manage professional category widget full page cache */
            $this->httpContext->setValue(
                $profCatKey,
                $this->_priceEngine->getProfessionalCategory(),
                ''
            );
        }
    }
}
