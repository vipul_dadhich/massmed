<?php

namespace MMS\CurrencyEngine\Api\Quote;

use MMS\CurrencyEngine\Api\Quote\Data\UpdateCountryDataInterface;

/**
 * Interface CartUpdateCountryManagementInterface
 * @package MMS\CurrencyEngine\Api\Quote
 */
interface CartUpdateCountryManagementInterface
{
    /**
     * Handle update country on quote.
     *
     * @param int $cartId
     * @param string $country
     * @return UpdateCountryDataInterface
     */
    public function updateCountry(int $cartId, string $country): UpdateCountryDataInterface;
}
