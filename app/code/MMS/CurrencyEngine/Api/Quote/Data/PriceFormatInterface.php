<?php

namespace MMS\CurrencyEngine\Api\Quote\Data;

/**
 * Interface PriceFormatInterface
 * @package MMS\CurrencyEngine\Api\Quote\Data
 */
interface PriceFormatInterface
{
    const PATTERN = 'pattern';
    const PRECISION = 'precision';
    const REQUIRED_PRECISION = 'requiredPrecision';
    const DECIMAL_SYMBOL = 'decimalSymbol';
    const GROUP_SYMBOL = 'groupSymbol';
    const GROUP_LENGTH = 'groupLength';
    const INTEGER_REQUIRED = 'integerRequired';

    /**
     * @return string
     */
    public function getPattern();

    /**
     * @param string $pattern
     * @return void
     */
    public function setPattern($pattern);

    /**
     * @return int
     */
    public function getPrecision();

    /**
     * @param int $precision
     * @return void
     */
    public function setPrecision($precision);

    /**
     * @return int
     */
    public function getRequiredPrecision();

    /**
     * @param int $requiredPrecision
     * @return void
     */
    public function setRequiredPrecision($requiredPrecision);

    /**
     * @return string
     */
    public function getDecimalSymbol();

    /**
     * @param string $decimalSymbol
     * @return void
     */
    public function setDecimalSymbol($decimalSymbol);

    /**
     * @return string
     */
    public function getGroupSymbol();

    /**
     * @param string $groupSymbol
     * @return void
     */
    public function setGroupSymbol($groupSymbol);

    /**
     * @return int
     */
    public function getGroupLength();

    /**
     * @param int $groupLength
     * @return void
     */
    public function setGroupLength($groupLength);

    /**
     * @return bool mixed
     */
    public function getIntegerRequired();

    /**
     * @param bool mixed $integerRequired
     * @return void
     */
    public function setIntegerRequired($integerRequired);
}
