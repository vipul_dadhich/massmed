<?php

namespace MMS\CurrencyEngine\Api\Quote\Data;

/**
 * Interface UpdateCountryDataInterface
 * @package MMS\CurrencyEngine\Api\Quote\Data
 */
interface UpdateCountryDataInterface
{
    const PRICE_FORMAT = 'priceFormat';
    const BASE_PRICE_FORMAT = 'basePriceFormat';
    const STATUS = 'status';

    /**
     * @return \MMS\CurrencyEngine\Api\Quote\Data\PriceFormatInterface
     */
    public function getPriceFormat();

    /**
     * @param \MMS\CurrencyEngine\Api\Quote\Data\PriceFormatInterface $priceFormat
     * @return void
     */
    public function setPriceFormat(\MMS\CurrencyEngine\Api\Quote\Data\PriceFormatInterface $priceFormat);

    /**
     * @return \MMS\CurrencyEngine\Api\Quote\Data\PriceFormatInterface
     */
    public function getBasePriceFormat();

    /**
     * @param \MMS\CurrencyEngine\Api\Quote\Data\PriceFormatInterface $basePriceFormat
     * @return void
     */
    public function setBasePriceFormat(\MMS\CurrencyEngine\Api\Quote\Data\PriceFormatInterface $basePriceFormat);

    /**
     * @return int mixed
     */
    public function getStatus();

    /**
     * @param int $status
     * @return void
     */
    public function setStatus($status);
}
