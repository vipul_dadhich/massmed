<?php

namespace MMS\CurrencyEngine\Api\Quote;

use MMS\CurrencyEngine\Api\Quote\Data\UpdateCountryDataInterface;

/**
 * Interface GuestCartUpdateCountryManagementInterface
 * @package MMS\CurrencyEngine\Api\Quote
 */
interface GuestCartUpdateCountryManagementInterface
{
    /**
     * Handle update country on quote.
     *
     * @param string $cartId
     * @param string $country
     * @return UpdateCountryDataInterface
     */
    public function updateCountry(string $cartId, string $country): UpdateCountryDataInterface;
}
