<?php

namespace MMS\CurrencyEngine\Api;

/**
 * Interface LoggerInterface
 * @package MMS\CurrencyEngine\Api
 */
interface LoggerInterface extends \MMS\Logger\LoggerInterface
{

}
