<?php

namespace MMS\CurrencyEngine\Model;

/**
 * Class Context
 * @package MMS\CurrencyEngine\Model
 */
class Context
{
    /**
     * Currency cache context
     */
    const CONTEXT_CURRENCY = 'currency_engine_currency';

    /**
     * Country cache context
     */
    const CONTEXT_COUNTRY = 'currency_engine_country';

    /**
     * Professional Category cache context
     */
    const CONTEXT_PROFESSIONAL_CATEGORY = 'currency_engine_professional_category';

    /**
     * Professional Category promo flag cache context
     */
    const CONTEXT_PROFESSIONAL_CATEGORY_PROMO_FLAG = 'currency_engine_professional_category_promo_flag';
}
