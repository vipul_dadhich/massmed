<?php

namespace MMS\CurrencyEngine\Model\Quote;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote;
use MMS\CurrencyEngine\Api\Quote\Data\UpdateCountryDataInterface;
use MMS\CurrencyEngine\Api\Quote\GuestCartUpdateCountryManagementInterface;

/**
 * Class GuestCartUpdateCountryManagement
 * @package MMS\CurrencyEngine\Model\Quote
 */
class GuestCartUpdateCountryManagement extends UpdateCountryManager implements  GuestCartUpdateCountryManagementInterface
{
    /**
     * @inheritDoc
     */
    public function updateCountry(string $cartId, string $country): UpdateCountryDataInterface
    {
        $response = ['status' => 0];
        if ($this->_currencyEngineService->isEnabled() && $this->updateCountryToSession($country)) {
            try {
                /** @var Quote $quote */
                $quote = $this->getQuote($cartId, true);
                try {
                    $this->updateQuoteItems($quote);
                    $this->setForcedCurrencyQuote($quote);
                    $quote->setTotalsCollectedFlag(false);
                    $quote->collectTotals();

                    $this->_quoteResource->save($quote);
                    $response = $this->outputResponse($quote);
                } catch (AlreadyExistsException | \Exception $e) {
                    $this->_logger->debug($e->getMessage());
                }
            } catch (NoSuchEntityException $e) {
                $this->_logger->debug($e->getMessage());
            }
        }

        return $this->_updateCountryData->setData($response);
    }
}
