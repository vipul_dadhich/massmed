<?php

namespace MMS\CurrencyEngine\Model\Quote;

use Magento\Directory\Model\CountryFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Locale\FormatInterface as LocaleFormat;
use Magento\Framework\ObjectManagerInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Quote\Model\ResourceModel\Quote as QuoteResource;
use MMS\CurrencyEngine\Api\LoggerInterface;
use MMS\CurrencyEngine\Api\Quote\Data\PriceFormatInterfaceFactory;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;
use MMS\CurrencyEngine\Service\CurrencyEngine as CurrencyEngineService;
use Magento\Directory\Model\CurrencyFactory as CurrencyModelFactory;
use Magento\Store\Model\StoreManager;
use MMS\CurrencyEngine\Api\Quote\Data\UpdateCountryDataInterface;

/**
 * Class UpdateCountryManager
 * @package MMS\CurrencyEngine\Model\Quote
 */
class UpdateCountryManager
{
    public $_currentCurrencyCode;
    /**
     * @var SessionCurrencyEngine
     */
    protected $_session;
    /**
     * @var CurrencyEngineService
     */
    protected $_currencyEngineService;
    /**
     * @var QuoteIdMaskFactory
     */
    protected $_quoteIdMaskFactory;
    /**
     * Quote repository.
     *
     * @var CartRepositoryInterface
     */
    protected $_cartRepository;
    /**
     * @var CountryFactory
     */
    protected $_countryFactory;
    /**
     * @var QuoteResource
     */
    protected $_quoteResource;
    /**
     * @var CurrencyModelFactory
     */
    protected $_currencyFactory;
    /**
     * @var StoreManager
     */
    protected $_storeManager;
    /**
     * @var LocaleFormat
     */
    protected $_localeFormat;
    /**
     * @var UpdateCountryDataInterface
     */
    protected $_updateCountryData;
    /**
     * @var PriceFormatInterfaceFactory
     */
    protected $_priceFormatFactory;
    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * UpdateCountryManager constructor.
     * @param SessionCurrencyEngine $session
     * @param CurrencyEngineService $currencyEngineService
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param CartRepositoryInterface $cartRepository
     * @param CountryFactory $countryFactory
     * @param QuoteResource $quoteResource
     * @param CurrencyModelFactory $currencyFactory
     * @param StoreManager $storeManager
     * @param LocaleFormat $localeFormat
     * @param UpdateCountryDataInterface $updateCountryData
     * @param PriceFormatInterfaceFactory $priceFormatFactory
     * @param LoggerInterface $logger
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        SessionCurrencyEngine $session,
        CurrencyEngineService $currencyEngineService,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        CartRepositoryInterface $cartRepository,
        CountryFactory $countryFactory,
        QuoteResource $quoteResource,
        CurrencyModelFactory $currencyFactory,
        StoreManager $storeManager,
        LocaleFormat $localeFormat,
        UpdateCountryDataInterface $updateCountryData,
        PriceFormatInterfaceFactory $priceFormatFactory,
        LoggerInterface $logger,
        ObjectManagerInterface $objectManager
    ) {
        $this->_session = $session;
        $this->_currencyEngineService = $currencyEngineService;
        $this->_quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->_cartRepository = $cartRepository;
        $this->_countryFactory = $countryFactory;
        $this->_quoteResource = $quoteResource;
        $this->_currencyFactory = $currencyFactory;
        $this->_storeManager = $storeManager;
        $this->_localeFormat = $localeFormat;
        $this->_updateCountryData = $updateCountryData;
        $this->_priceFormatFactory = $priceFormatFactory;
        $this->_logger = $logger;
        $this->_objectManager = $objectManager;
    }

    /**
     * Update country to session and call the price engine
     *
     * @param $countryId
     * @return bool
     */
    protected function updateCountryToSession($countryId)
    {
        $_country = $this->getCountryCode($countryId);
        if ($_country->getData('iso3_code')) {
            $country = $_country->getData('iso3_code');
            $this->_session->addCountry($country);
            return true;
        }

        return false;
    }

    /**
     * @param $cartId
     * @param bool $isGuest
     * @return CartInterface
     * @throws NoSuchEntityException
     */
    protected function getQuote($cartId, $isGuest = false)
    {
        if ($isGuest) {
            $quoteIdMask = $this->_quoteIdMaskFactory->create()->load($cartId, 'masked_id');
            $cartId = $quoteIdMask->getQuoteId();
        }

        return $this->_cartRepository->get($cartId);
    }

    /**
     * @param $countryId
     * @return \Magento\Directory\Model\Country
     */
    protected function getCountryCode($countryId)
    {
        return $this->_countryFactory->create()->loadByCode($countryId);
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     */
    protected function updateQuoteItems(\Magento\Quote\Model\Quote $quote)
    {
        if ($quote->hasItems()) {
            $currency = false;
            foreach ($quote->getAllVisibleItems() as $item) {
                $item = ($item->getParentItem() ? $item->getParentItem() : $item);
                $customPrice = $this->_currencyEngineService->getPriceEngineService()->getQuoteItemPrice($item);
                if (!$currency) {
                    $currency = $this->_currencyEngineService->getPriceEngineService()->getCurrencyCode() ?? 'USD';
                }
                if (!$customPrice) {
                    $customPrice = $item->getProduct()->getFinalPrice();
                }

                $item->setCustomPrice($customPrice);
                $item->setOriginalCustomPrice($customPrice);
                $item->getProduct()->setIsSuperMode(true);
            }

            $this->setCurrentCurrencyCode($currency);
        }
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return void
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function setForcedCurrencyQuote(
        \Magento\Quote\Model\Quote $quote
    ) {
        $currencyCode = $this->getCurrentCurrencyCode();

        /** @var \Magento\Store\Model\StoreManagerInterface $storeManager */
        $storeManager = $this->_objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
        $storeManager->getStore()->setCurrentCurrencyCode($currencyCode);

        $this->_session->addCurrency($currencyCode);

        $quote->setQuoteCurrencyCode($currencyCode);
        $quoteCurrency = $this->_currencyFactory->create()->load($currencyCode);
        $quote->setForcedCurrency($quoteCurrency);


        /*$quote->setBaseCurrencyCode($quoteCurrency->getCode());
        $store = $this->getStore($quote->getStoreId());
        $store->setCurrentCurrencyCode($quoteCurrency->getCode());
        $store->setData('base_currency', $quoteCurrency);*/
    }

    /**
     * @param $storeId
     * @return \Magento\Store\Api\Data\StoreInterface
     * @throws NoSuchEntityException
     */
    protected function getStore($storeId)
    {
        return $this->_storeManager->getStore($storeId);
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    protected function outputResponse(\Magento\Quote\Model\Quote $quote = null)
    {
        $output = [];
        if ($quote) {
            $priceFormat = $this->_localeFormat->getPriceFormat(
                null,
                $quote->getQuoteCurrencyCode()
            );
            $basePriceFormat = $this->_localeFormat->getPriceFormat(
                null,
                $quote->getBaseCurrencyCode()
            );

            $output['priceFormat'] = $this->_priceFormatFactory->create()->setData($priceFormat);
            $output['basePriceFormat'] = $this->_priceFormatFactory->create()->setData($basePriceFormat);
            $output['status'] = 1;
        } else {
            $output['status'] = 0;
        }

        return $output;
    }

    /**
     * @return mixed
     */
    public function getCurrentCurrencyCode()
    {
        return $this->_currentCurrencyCode;
    }

    /**
     * @param $currentCurrencyCode
     */
    public function setCurrentCurrencyCode($currentCurrencyCode)
    {
        $this->_currentCurrencyCode = $currentCurrencyCode;
    }
}
