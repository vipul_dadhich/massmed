<?php

namespace MMS\CurrencyEngine\Model\Quote\Data;

use Magento\Framework\Model\AbstractModel;
use MMS\CurrencyEngine\Api\Quote\Data\PriceFormatInterface;
use MMS\CurrencyEngine\Api\Quote\Data\UpdateCountryDataInterface;

/**
 * Class UpdateCountryData
 * @package MMS\CurrencyEngine\Model\Quote\Data
 */
class UpdateCountryData extends AbstractModel implements UpdateCountryDataInterface
{
    /**
     * @inheritDoc
     */
    public function getPriceFormat()
    {
        return $this->getData(self::PRICE_FORMAT);
    }

    /**
     * {@inheritdoc}
     */
    public function setPriceFormat(PriceFormatInterface $priceFormat)
    {
        $this->setData(self::PRICE_FORMAT, $priceFormat);
    }

    /**
     * @inheritDoc
     */
    public function getBasePriceFormat()
    {
        return $this->getData(self::BASE_PRICE_FORMAT);
    }

    /**
     * {@inheritdoc}
     */
    public function setBasePriceFormat(PriceFormatInterface $basePriceFormat)
    {
        $this->setData(self::BASE_PRICE_FORMAT, $basePriceFormat);
    }

    /**
     * @inheritDoc
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        $this->setData(self::STATUS, $status);
    }
}
