<?php

namespace MMS\CurrencyEngine\Model\Quote\Data;

use Magento\Framework\Model\AbstractModel;
use MMS\CurrencyEngine\Api\Quote\Data\PriceFormatInterface;

/**
 * Class PriceFormat
 * @package MMS\CurrencyEngine\Model\Quote\Data
 */
class PriceFormat extends AbstractModel implements PriceFormatInterface
{
    /**
     * @inheritDoc
     */
    public function getPattern()
    {
        return $this->getData(self::PATTERN);
    }

    /**
     * @inheritDoc
     */
    public function setPattern($pattern)
    {
        $this->setData(self::PATTERN, $pattern);
    }

    /**
     * @inheritDoc
     */
    public function getPrecision()
    {
        return $this->getData(self::PRECISION);
    }

    /**
     * @inheritDoc
     */
    public function setPrecision($precision)
    {
        $this->setData(self::PRECISION, $precision);
    }

    /**
     * @inheritDoc
     */
    public function getRequiredPrecision()
    {
        return $this->getData(self::REQUIRED_PRECISION);
    }

    /**
     * @inheritDoc
     */
    public function setRequiredPrecision($requiredPrecision)
    {
        $this->setData(self::REQUIRED_PRECISION, $requiredPrecision);
    }

    /**
     * @inheritDoc
     */
    public function getDecimalSymbol()
    {
        return $this->getData(self::DECIMAL_SYMBOL);
    }

    /**
     * @inheritDoc
     */
    public function setDecimalSymbol($decimalSymbol)
    {
        $this->setData(self::DECIMAL_SYMBOL, $decimalSymbol);
    }

    /**
     * @inheritDoc
     */
    public function getGroupSymbol()
    {
        return $this->getData(self::GROUP_SYMBOL);
    }

    /**
     * @inheritDoc
     */
    public function setGroupSymbol($groupSymbol)
    {
        $this->setData(self::GROUP_SYMBOL, $groupSymbol);
    }

    /**
     * @inheritDoc
     */
    public function getGroupLength()
    {
        return $this->getData(self::GROUP_LENGTH);
    }

    /**
     * @inheritDoc
     */
    public function setGroupLength($groupLength)
    {
        $this->setData(self::GROUP_LENGTH, $groupLength);
    }

    /**
     * @inheritDoc
     */
    public function getIntegerRequired()
    {
        return $this->getData(self::INTEGER_REQUIRED);
    }

    /**
     * @inheritDoc
     */
    public function setIntegerRequired($integerRequired)
    {
        $this->setData(self::INTEGER_REQUIRED, $integerRequired);
    }
}
