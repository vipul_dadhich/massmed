<?php

namespace MMS\CurrencyEngine\Model;

use MMS\CurrencyEngine\Api\LoggerInterface;
use MMS\Logger\Helper\Data as LoggerDataHelper;
use MMS\CurrencyEngine\Helper\Data as CurrencyDataHelper;

/**
 * Class Logger
 * @package MMS\CurrencyEngine\Model
 */
class Logger extends \MMS\Logger\Model\Logger implements LoggerInterface
{
    /**
     * @var CurrencyDataHelper
     */
    protected $_currencyEngineHelper;

    /**
     * Logger constructor.
     * @param LoggerDataHelper $dataHelper
     * @param CurrencyDataHelper $currencyEngineHelper
     * @param array $handlers
     * @param array $processors
     */
    public function __construct(
        LoggerDataHelper $dataHelper,
        CurrencyDataHelper $currencyEngineHelper,
        array $handlers = [],
        array $processors = []
    ) {
        $this->_currencyEngineHelper = $currencyEngineHelper;
        parent::__construct($dataHelper, $handlers, $processors);
    }

    /**
     * @param string $message
     * @param array $context
     * @return mixed|bool
     */
    public function debug($message, array $context = [])
    {
        if (!$this->_currencyEngineHelper->isLogEnable()) {
            return false;
        }

        return parent::debug($message, $context);
    }

    /**
     * @param string $message
     * @param array $context
     * @return mixed|bool
     */
    public function info($message, array $context = [])
    {
        if (!$this->_currencyEngineHelper->isLogEnable()) {
            return false;
        }

        return parent::info($message, $context);
    }

    /**
     * @param string $message
     * @param array $context
     * @return mixed|bool
     */
    public function warning($message, array $context = [])
    {
        if (!$this->_currencyEngineHelper->isLogEnable()) {
            return false;
        }

        return parent::warning($message, $context);
    }

    /**
     * @param string $message
     * @param array $context
     * @return mixed|bool
     */
    public function critical($message, array $context = [])
    {
        if (!$this->_currencyEngineHelper->isLogEnable()) {
            return false;
        }

        return parent::critical($message, $context);
    }
}
