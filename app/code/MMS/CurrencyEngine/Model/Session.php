<?php

namespace MMS\CurrencyEngine\Model;

use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\State;
use Magento\Framework\Exception\SessionException;
use Magento\Framework\Session\Config\ConfigInterface;
use Magento\Framework\Session\SaveHandlerInterface;
use Magento\Framework\Session\SessionStartChecker;
use Magento\Framework\Session\SidResolverInterface;
use Magento\Framework\Session\StorageInterface;
use Magento\Framework\Session\ValidatorInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use MMS\PriceEngine\Service\PriceEngine as PriceEngineService;

/**
 * Class Session
 * @package MMS\CurrencyEngine\Model
 */
class Session extends \Magento\Framework\Session\SessionManager
{
    /**
     * @var HttpContext
     */
    protected $httpContext;
    /**
     * @var PriceEngineService
     */
    protected $priceEngineService;

    /**
     * Session constructor.
     * @param Http $request
     * @param SidResolverInterface $sidResolver
     * @param ConfigInterface $sessionConfig
     * @param SaveHandlerInterface $saveHandler
     * @param ValidatorInterface $validator
     * @param StorageInterface $storage
     * @param CookieManagerInterface $cookieManager
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param State $appState
     * @param HttpContext $httpContext
     * @param PriceEngineService $priceEngineService
     * @param SessionStartChecker|null $sessionStartChecker
     * @throws SessionException
     */
    public function __construct(
        Http $request,
        SidResolverInterface $sidResolver,
        ConfigInterface $sessionConfig,
        SaveHandlerInterface $saveHandler,
        ValidatorInterface $validator,
        StorageInterface $storage,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        State $appState,
        HttpContext $httpContext,
        PriceEngineService $priceEngineService,
        SessionStartChecker $sessionStartChecker = null
    ) {
        $this->httpContext = $httpContext;
        $this->priceEngineService = $priceEngineService;
        parent::__construct(
            $request,
            $sidResolver,
            $sessionConfig,
            $saveHandler,
            $validator,
            $storage,
            $cookieManager,
            $cookieMetadataFactory,
            $appState,
            $sessionStartChecker
        );
    }

    /**
     * Set country code to session
     *
     * @param string $countryCode
     * @return $this
     */
    public function addCountry(string $countryCode): Session
    {
        $this->setCurrencyEngineCountry($countryCode);
        $this->httpContext->setValue(
            Context::CONTEXT_COUNTRY,
            $countryCode,
            'USA'
        );
        return $this;
    }

    /**
     * Get country code
     *
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->getCurrencyEngineCountry();
    }

    /**
     * Set currency code to session
     *
     * @param string $currencyCode
     * @return $this
     */
    public function addCurrency(string $currencyCode): Session
    {
        $this->setCurrencyEngineCurrencyCode($currencyCode);
        $this->httpContext->setValue(
            Context::CONTEXT_CURRENCY,
            $currencyCode,
            $this->priceEngineService->getCurrencyCode()
        );
        return $this;
    }

    /**
     * Get currency code
     *
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->getCurrencyEngineCurrencyCode();
    }

    /**
     * Set professional category to session
     *
     * @param string $professionalCatCode
     * @return $this
     */
    public function addProfessionalCategory(string $professionalCatCode): Session
    {
        $this->setCurrencyEngineProfessionalCategory($professionalCatCode);
        $this->httpContext->setValue(
            Context::CONTEXT_PROFESSIONAL_CATEGORY,
            $professionalCatCode,
            ''
        );
        return $this;
    }

    /**
     * Get professional category
     *
     * @return string|null
     */
    public function getProfessionalCategory(): ?string
    {
        return $this->getCurrencyEngineProfessionalCategory();
    }

    /**
     * Clear professional category from session manager
     *
     * @return string|null
     */
    public function unsetProfessionalCategory(): ?string
    {
        return $this->unsetCurrencyEngineProfessionalCategory();
    }

    /**
     * Set professional category promo flag to session
     *
     * @param string $professionalCatCode
     * @return $this
     */
    public function addPromoProfessionalCategory(string $professionalCatCode): Session
    {
        $this->setCurrencEngineProfessionalCategoryPromoFlag($professionalCatCode);
        $this->httpContext->setValue(
            Context::CONTEXT_PROFESSIONAL_CATEGORY_PROMO_FLAG,
            $professionalCatCode,
            ''
        );
        return $this;
    }

    /**
     * Get category promo flag from session
     *
     * @return string|null
     */
    public function getPromoProfessionalCategory(): ?string
    {
        return $this->getCurrencEngineProfessionalCategoryPromoFlag();
    }
}
