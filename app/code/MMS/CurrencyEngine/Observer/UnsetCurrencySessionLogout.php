<?php

namespace MMS\CurrencyEngine\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;
use MMS\CurrencyEngine\Service\CurrencyEngine as CurrencyEngineService;

/**
 * Class UnsetCurrencySessionLogout
 * @package MMS\CurrencyEngine\Observer
 */
class UnsetCurrencySessionLogout implements ObserverInterface
{
    /**
     * @var SessionCurrencyEngine
     */
    protected $_session;
    /**
     * @var CurrencyEngineService
     */
    protected $_currencyEngineService;

    /**
     * UnsetCurrencySessionLogout constructor.
     * @param SessionCurrencyEngine $session
     * @param CurrencyEngineService $currencyEngineService
     */
    public function __construct(
        SessionCurrencyEngine $session,
        CurrencyEngineService $currencyEngineService
    ) {
        $this->_session = $session;
        $this->_currencyEngineService = $currencyEngineService;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->_currencyEngineService->isEnabled()) {
            $this->_session->destroy();
        }
    }
}
