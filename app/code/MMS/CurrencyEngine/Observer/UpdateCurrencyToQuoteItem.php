<?php

namespace MMS\CurrencyEngine\Observer;

use Magento\Directory\Model\CurrencyFactory as CurrencyModelFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Quote\Model\ResourceModel\Quote;
use Magento\Store\Model\StoreManagerInterface;
use MMS\CurrencyEngine\Model\Logger;
use MMS\PriceEngine\Service\PriceEngine as PriceEngineService;

/**
 * Class UpdateCurrencyToQuoteItem
 * @package MMS\CurrencyEngine\Observer
 */
class UpdateCurrencyToQuoteItem implements ObserverInterface
{
    /**
     * @var PriceEngineService
     */
    protected $_priceEngineService;
    /**
     * @var Quote
     */
    protected $_quoteResource;
    /**
     * @var Logger
     */
    protected $_logger;
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
    /**
     * @var CurrencyModelFactory
     */
    protected $_currencyFactory;

    /**
     * UpdateCurrencyToQuoteItem constructor.
     * @param PriceEngineService $priceEngine
     * @param Quote $quoteResource
     * @param Logger $logger
     * @param ObjectManagerInterface $objectManager
     * @param CurrencyModelFactory $currencyFactory
     */
    public function __construct(
        PriceEngineService $priceEngine,
        Quote $quoteResource,
        Logger $logger,
        ObjectManagerInterface $objectManager,
        CurrencyModelFactory $currencyFactory
    ) {
        $this->_priceEngineService = $priceEngine;
        $this->_quoteResource = $quoteResource;
        $this->_logger = $logger;
        $this->_objectManager = $objectManager;
        $this->_currencyFactory = $currencyFactory;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        try {
            $quote = $this->_priceEngineService->getCheckoutSession()->getQuote();
            if ($quote->getIsActive() && $quote->hasItems()) {
                $items = $quote->getAllVisibleItems();
                $currency = false;
                foreach($items as $item) {
                    $item = ($item->getParentItem() ? $item->getParentItem() : $item);
                    $customPrice = $this->_priceEngineService->getQuoteItemPrice($item);
                    if (!$currency) {
                        $currency = $this->_priceEngineService->getCurrencyCode() ?? 'USD';
                    }
                    if (!$customPrice) {
                        $customPrice = $item->getProduct()->getFinalPrice();
                    }
                    $item->setCustomPrice($customPrice);
                    $item->setOriginalCustomPrice($customPrice);
                    $item->getProduct()->setIsSuperMode(true);
                }
                if ($currency) {
                    /** @var StoreManagerInterface $storeManager */
                    $storeManager = $this->_objectManager->get(StoreManagerInterface::class);
                    $storeManager->getStore()->setCurrentCurrencyCode($currency);

                    $quote->setQuoteCurrencyCode($currency);
                    $quoteCurrency = $this->_currencyFactory->create()->load($currency);
                    $quote->setForcedCurrency($quoteCurrency);
                }
                $quote->setTotalsCollectedFlag(false);
                $quote->collectTotals();
                $this->_quoteResource->save($quote);
            }
        } catch (LocalizedException | \Exception $e) {
            $this->_logger->debug('Error on UpdateCurrencyToQuoteItem Observer');
            $this->_logger->debug($e->getMessage());
        }
    }
}
