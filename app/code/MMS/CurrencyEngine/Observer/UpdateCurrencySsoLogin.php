<?php

namespace MMS\CurrencyEngine\Observer;

use Magento\Customer\Model\Session;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;
use MMS\CurrencyEngine\Service\CurrencyEngine as CurrencyEngineService;

/**
 * Class UpdateCurrencySsoLogin
 * @package MMS\CurrencyEngine\Observer
 */
class UpdateCurrencySsoLogin implements ObserverInterface
{
    /**
     * @var CurrencyEngineService
     */
    protected $_currencyEngineService;

    /**
     * @var SessionCurrencyEngine
     */
    protected $_sessionCurrencyEngine;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var ManagerInterface
     */
    protected $_eventManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * UpdateCurrencySsoLogin constructor.
     * @param CurrencyEngineService $currencyEngineService
     * @param SessionCurrencyEngine $sessionCurrencyEngine
     * @param Session $customerSession
     * @param CountryFactory $countryFactory
     * @param ManagerInterface $eventManager
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        CurrencyEngineService $currencyEngineService,
        SessionCurrencyEngine $sessionCurrencyEngine,
        Session $customerSession,
        CountryFactory $countryFactory,
        ManagerInterface $eventManager,
        ObjectManagerInterface $objectManager
    ) {
        $this->_currencyEngineService = $currencyEngineService;
        $this->_sessionCurrencyEngine = $sessionCurrencyEngine;
        $this->_customerSession = $customerSession;
        $this->_countryFactory = $countryFactory;
        $this->_eventManager = $eventManager;
        $this->_objectManager = $objectManager;
    }

    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        if ($this->_currencyEngineService->isEnabled()) {
            $countryIsoCode = false;
            $customer = $this->_customerSession->getCustomer();
            $defaultCountry = $customer->getData('customer_default_country');
            if (!$defaultCountry && $billingAddress = $customer->getDefaultBillingAddress()) {
                $defaultCountry = $billingAddress->getCountryId();
            }
            if ($defaultCountry) {
                $country = $this->_countryFactory->create()->loadByCode($defaultCountry);
                if ($country->getData('iso3_code')) {
                    $countryIsoCode = $country->getData('iso3_code');
                }
            }

            if ($countryIsoCode && $countryIsoCode != $this->_sessionCurrencyEngine->getCountry()) {
                $storeManager = $this->_objectManager->get(StoreManagerInterface::class);
                $this->_sessionCurrencyEngine->addCountry($countryIsoCode);
                $response = $this->_currencyEngineService->callPriceEngine();
                $currency = $response['products'][0]['currency'] ?? 'USD';
                $allowedCurrencies = $storeManager->getStore()->getAvailableCurrencyCodes();
                $currency = strtoupper($currency);
                if (in_array($currency, $allowedCurrencies)) {
                    $this->_sessionCurrencyEngine->addCurrency($currency);
                    $storeManager->getStore()->setCurrentCurrencyCode($currency);
                    $this->_eventManager->dispatch('currency_engine_update_currency', [
                        'country' => $countryIsoCode,
                        'currency' => $currency,
                        'professional_category' => $this->_sessionCurrencyEngine->getProfessionalCategory()
                    ]);
                }
            }
        }

        return $this;
    }
}
