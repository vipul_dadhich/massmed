<?php

namespace MMS\CurrencyEngine\Plugin;

use Magento\Framework\App\Http\Context;
use MMS\CurrencyEngine\Model\Session;
use Magento\Framework\App\RequestInterface;

/**
 * Class CacheContextPlugin
 * @package MMS\CurrencyEngine\Plugin
 */
class CacheContextPlugin
{
    /**
     * @var Session
     */
    private $_currencyEngine;

    /**
     * @var RequestInterface
     */
    private $_request;


    /**
     * @param Session $currencyEngine
     */
    public function __construct(
        Session $currencyEngine,
        RequestInterface $request
    ) {
        $this->_currencyEngine = $currencyEngine;
        $this->_request = $request;
    }

    /**
     * Sets appropriate header if customer session is persistent.
     *
     * @param Context $subject
     * @return mixed
     */
    public function beforeGetVaryString(Context $subject)
    {
        $currencyKey = \MMS\CurrencyEngine\Model\Context::CONTEXT_CURRENCY;
        $countryKey = \MMS\CurrencyEngine\Model\Context::CONTEXT_COUNTRY;
        $profCatKey = \MMS\CurrencyEngine\Model\Context::CONTEXT_PROFESSIONAL_CATEGORY;
        $profCat = $this->_currencyEngine->getProfessionalCategory();
        $country = $this->_currencyEngine->getCountry();
        $currency = $this->_currencyEngine->getCurrency();
        $promo = $this->_request->getParam('promo', false);
        if ($promo) {
            $subject->setValue('PROMO', $promo, '');
        }
        $subject->setValue($currencyKey, $currency, 'USD');
        $subject->setValue($countryKey, $country, 'USA');
        $subject->setValue($profCatKey, $profCat, 'PHY');
        return [];
    }
}
