<?php

namespace MMS\CurrencyEngine\Plugin;

use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;
use MMS\CurrencyEngine\Service\CurrencyEngine as CurrencyEngineService;
use MMS\Paybill\Model\Service\CreateQuote;

/**
 * Class PaybillSetSession
 * @package MMS\CurrencyEngine\Plugin
 */
class PaybillSetSession
{
    /**
     * @var SessionCurrencyEngine
     */
    protected $_session;
    /**
     * @var CurrencyEngineService
     */
    protected $_currencyEngineService;

    /**
     * PriceEngineSetCountry constructor.
     * @param SessionCurrencyEngine $session
     * @param CurrencyEngineService $currencyEngineService
     */
    public function __construct(
        SessionCurrencyEngine $session,
        CurrencyEngineService $currencyEngineService
    ) {
        $this->_session = $session;
        $this->_currencyEngineService = $currencyEngineService;
    }

    /**
     * @param CreateQuote $subject
     * @param $currencyCode
     * @return array|SessionCurrencyEngine
     */
    public function beforeUpdateCurrencyToSession(
        CreateQuote $subject,
        $currencyCode
    ) {
        if ($this->_currencyEngineService->isEnabled()) {
             $this->_session->addCurrency($currencyCode);
        }

        return [$currencyCode];
    }
}
