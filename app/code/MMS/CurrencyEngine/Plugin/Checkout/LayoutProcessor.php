<?php

namespace MMS\CurrencyEngine\Plugin\Checkout;

use MMS\PriceEngine\Service\PriceEngine as PriceEngineService;

/**
 * Class LayoutProcessor
 * @package MMS\CurrencyEngine\Plugin\Checkout
 */
class LayoutProcessor
{
    /**
     * @var PriceEngineService
     */
    protected $_priceEngine;

    public function __construct(
        PriceEngineService $priceEngine
    ) {
        $this->_priceEngine = $priceEngine;
    }

    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {
        $professionalCat = $this->_priceEngine->getProfessionalCategory();
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
        ['customer-email']['children']['additional-login-form-fields']['children']['pi_professional_category'])) {
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_professional_category']['value'] = $professionalCat;
        }
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_professional_category'])) {
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']
            ['customer-email']['children']['additional-login-form-fields']['children']['pi_professional_category']['value'] = $professionalCat;
        }
        return $jsLayout;
    }
}
