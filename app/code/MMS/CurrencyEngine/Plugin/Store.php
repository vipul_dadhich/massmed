<?php

namespace MMS\CurrencyEngine\Plugin;

use Closure;
use Magento\Directory\Model\Currency;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\Store as MagentoStore;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;
use MMS\CurrencyEngine\Service\CurrencyEngine as CurrencyEngineService;

/**
 * Class Store
 * @package MMS\CurrencyEngine\Plugin
 */
class Store
{
    /**
     * @var CurrencyEngineService
     */
    protected $_currencyEngineService;
    /**
     * @var Currency
     */
    protected $_currenciesModel;
    /**
     * @var SessionCurrencyEngine
     */
    protected $_session;
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
    /**
     * @var ManagerInterface
     */
    protected $_eventManager;

    /**
     * Store constructor.
     * @param CurrencyEngineService $currencyEngineService
     * @param Currency $currencyModel
     * @param SessionCurrencyEngine $session
     * @param ObjectManagerInterface $objectManager
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        CurrencyEngineService $currencyEngineService,
        Currency $currencyModel,
        SessionCurrencyEngine $session,
        ObjectManagerInterface $objectManager,
        ManagerInterface $eventManager
    ) {
        $this->_currencyEngineService = $currencyEngineService;
        $this->_currenciesModel = $currencyModel;
        $this->_session = $session;
        $this->_objectManager = $objectManager;
        $this->_eventManager = $eventManager;
    }

    /**
     * @param MagentoStore $subject
     * @param $result
     * @return mixed|string
     */
    public function afterGetDefaultCurrencyCode(
        MagentoStore $subject,
        $result
    ): string
    {
        if ($this->_currencyEngineService->isEnabled()) {
            if (!$this->validateCurrencyEngine()) {
                $this->callPriceEngine();
            }

            $defaultCurrency = $this->_currenciesModel->getConfigDefaultCurrencies();
            $currencyCode = $this->_currencyEngineService->getCurrencyCode();
            $allowedCurrencies = $this->_currenciesModel->getConfigAllowCurrencies();
            if (!in_array($currencyCode, $allowedCurrencies)) {
                return $defaultCurrency[0];
            } else {
                return $currencyCode;
            }
        }

        return $result;
    }

    /**
     * @return bool
     */
    protected function validateCurrencyEngine(): bool
    {
        $countryCodeSession = $this->_currencyEngineService->getCountryCodeSession();
        $countryCodeEngine = $this->_currencyEngineService->getCountryCodePriceEngine();
        if (!$countryCodeSession || $countryCodeSession !== $countryCodeEngine) {
            return false;
        }
        return true;
    }

    protected function callPriceEngine()
    {
        $country = $this->_currencyEngineService->getCountryCodePriceEngine();
        $response = $this->_currencyEngineService->callPriceEngine(true);
        $currency = isset($response['products'][0]['currency']) ? $response['products'][0]['currency'] : 'USD';
        $this->_session->addCurrency($currency);
        $this->_session->addCountry($country);

        /** @var \Magento\Store\Model\StoreManagerInterface $storeManager */
        $storeManager = $this->_objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
        $storeManager->getStore()->setCurrentCurrencyCode($currency);
    }
}
