<?php

namespace MMS\CurrencyEngine\Plugin;

use Magento\Checkout\Model\DefaultConfigProvider;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\Session as CheckoutSession;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;
use MMS\CurrencyEngine\Service\CurrencyEngine as CurrencyEngineService;
use MMS\PriceEngine\Service\PriceEngine as PriceEngine;

/**
 * Class ConfigProviderPlugin
 * @package MMS\CurrencyEngine\Plugin
 */
class ConfigProviderPlugin
{
    /**
     * @var CurrencyEngineService
     */
    protected $_currencyEngineService;

    /**
     * @var SessionCurrencyEngine
     */
    protected $_session;

    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * @var PriceEngine
     */
    protected $_priceEngine;

    /**
     * ConfigProviderPlugin constructor.
     * @param CurrencyEngineService $currencyEngineService
     * @param SessionCurrencyEngine $session
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param Session $checkoutSession
     * @param PriceEngine $_priceEngine
     */
    public function __construct(
        CurrencyEngineService $currencyEngineService,
        SessionCurrencyEngine $session,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        CheckoutSession $checkoutSession,
        PriceEngine $_priceEngine
    ) {
        $this->_currencyEngineService = $currencyEngineService;
        $this->_session = $session;
        $this->_countryFactory = $countryFactory;
        $this->checkoutSession = $checkoutSession;
        $this->_priceEngine = $_priceEngine;
    }

    /**
     * @param DefaultConfigProvider $subject
     * @param array $result
     * @return array
     */
    public function afterGetConfig(
        DefaultConfigProvider $subject,
        array $result
    ): array
    {

        $quote = $this->checkoutSession->getQuote();
        $currency = $quote->getQuoteCurrencyCode();
        $countryCode = $this->_session->getCountry() ?? $this->_priceEngine->getCountry();
        $country = $this->_countryFactory->create()->loadByCode($countryCode);

        $result['currencyEngineEnabled'] = $this->_currencyEngineService->isEnabled();
        $result['currencyEngineCountry'] = $countryCode;
        $result['currencyEngineCountryId'] = $country->getCountryId();
        if ($country->getCountryId()) {
            $result['customerData']['customer_default_country'] = $country->getCountryId();
        }
        $result['currencyEngineCurrency'] = $currency;

        return $result;
    }
}
