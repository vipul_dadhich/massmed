<?php

namespace MMS\CurrencyEngine\Plugin;

use Magento\Directory\Model\Currency;
use Magento\Store\Model\StoreManagerInterface;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;
use MMS\CurrencyEngine\Service\CurrencyEngine as CurrencyEngineService;
use MMS\PriceEngine\Service\PriceEngine;
use MMS\Logger\LoggerInterface;

/**
 * Class PriceEngineSetCountry
 * @package MMS\CurrencyEngine\Plugin
 */
class PriceEngineSetSession
{
    /**
     * @var SessionCurrencyEngine
     */
    protected $_session;
    /**
     * @var CurrencyEngineService
     */
    protected $_currencyEngineService;
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var Currency
     */
    protected $_currenciesModel;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * PriceEngineSetCountry constructor.
     * @param SessionCurrencyEngine $session
     * @param CurrencyEngineService $currencyEngineService
     * @param StoreManagerInterface $storeManager
     * @param Currency $currencyModel
     * @param LoggerInterface $logger
     */
    public function __construct(
        SessionCurrencyEngine $session,
        CurrencyEngineService $currencyEngineService,
        StoreManagerInterface $storeManager,
        Currency $currencyModel,
        LoggerInterface $logger
    ) {
        $this->_session = $session;
        $this->_currencyEngineService = $currencyEngineService;
        $this->_storeManager = $storeManager;
        $this->_currenciesModel = $currencyModel;
        $this->_logger = $logger;
    }

    /**
     * @param PriceEngine $subject
     * @param \Closure $proceed
     * @return mixed
     */
    public function aroundGetCountry(
        PriceEngine $subject,
        \Closure $proceed
    ) {
        if ($this->_currencyEngineService->isEnabled() && $this->_session->getCountry()) {
            return $this->_session->getCountry();
        }

        return $proceed();
    }

    /**
     * @param PriceEngine $subject
     * @param $result
     * @return array
     */
    public function afterGetCacheKey(
        PriceEngine $subject,
        array $result
    ): array
    {
        $result['mms_country_code'] = $this->_session->getCountry();
        $result['mms_currency_code'] = $this->_session->getCurrency();

        return $result;
    }

    /**
     * @param PriceEngine $subject
     * @param \Closure $proceed
     * @return mixed
     */
    public function aroundGetProfessionalCategory(
        PriceEngine $subject,
        \Closure $proceed
    ) {
        if ($this->_currencyEngineService->isEnabled() && $this->_session->getProfessionalCategory()) {
            $this->_logger->debug("Returning From Price Engine Session:-". $this->_session->getProfessionalCategory());
            return $this->_session->getProfessionalCategory();
        }

        return $proceed();
    }
}
