define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/quote',
    'MMS_CurrencyEngine/js/model/currency-engine',
    'MMS_CurrencyEngine/js/action/currency-engine'
], function ($, wrapper, customer, quote, currencyEngineModel, currencyEngineAction) {
    'use strict';
    return function (setBillingAddressAction) {
        return wrapper.wrap(setBillingAddressAction, function (originalAction, messageContainer) {
            if (window.checkoutConfig.currencyEngineEnabled && customer.isLoggedIn()
                && window.checkoutConfig.quoteData.is_paybill !== undefined
                && window.checkoutConfig.quoteData.is_paybill === "0" && currencyEngineModel.triggerBillingEvents()) {
                let billingAddress = quote.billingAddress();
                if (billingAddress && currencyEngineModel.lastCountryId() !== billingAddress.countryId) {
                    currencyEngineAction(billingAddress.countryId);
                    currencyEngineModel.lastCountryId(billingAddress.countryId);
                    currencyEngineModel.isAddressSelected = true;
                }
            }
            return originalAction(messageContainer);
        });
    };
});
