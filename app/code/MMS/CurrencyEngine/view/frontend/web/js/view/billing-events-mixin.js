define([
	'jquery',
    'mage/translate',
	'MMS_CurrencyEngine/js/model/currency-engine',
	'MMS_CurrencyEngine/js/action/currency-engine',
    'Magento_Ui/js/lib/view/utils/async'
], function(
	$,
	$t,
	currencyEngineModel,
	currencyEngineAction
) {
    'use strict';

    let countryIdSelector = '.billing-address-form select[name="country_id"]',
        isDefaultLastCountry = true,
        newAddressOption = {
            /**
             * Get new address label
             * @returns {String}
             */
            getAddressInline: function () {
                return $t('New Address');
            },
            customerAddressId: null
        };

    return function(Component) {
        return Component.extend({
            /**
             * @returns {self}
             */
            initialize: function() {
                this._super();

                if (this.isCurrencyEngineEnabled()) {
                    this.selectedAddress.subscribe(function (address) {
                        if (address.getAddressInline() === newAddressOption.getAddressInline()
                            && address.customerAddressId === newAddressOption.customerAddressId) {
                            currencyEngineModel.isAddressSelected = false;
                        }
                    }, this);

                    this._listenCurrencyEngine();
                }
            },

            _listenCurrencyEngine: function() {
                $.async(countryIdSelector, function(element) {
                    if (window.checkoutConfig.quoteData.is_paybill !== undefined &&
                        window.checkoutConfig.quoteData.is_paybill === "1") {
                        let countryVal = $(countryIdSelector).val();
						currencyEngineModel.lastCountryId(countryVal);
						isDefaultLastCountry = false;
						$(element).on('change', function() {
						    let countryVal = $(countryIdSelector).val();
						    if (countryVal) {
						        if (isDefaultLastCountry || currencyEngineModel.lastCountryId() !== countryVal) {
						            currencyEngineAction(countryVal);
						            currencyEngineModel.lastCountryId(countryVal);
						            isDefaultLastCountry = false;
						        }
						    }
						});
                    }
                });

                $.async(countryIdSelector, function(element) {
                    $(element).on('change', function() {
                        if (!currencyEngineModel.isAddressSelected && currencyEngineModel.triggerBillingEvents()) {
                            let countryVal = $(this).val();
                            if (countryVal && currencyEngineModel.lastCountryId() !== countryVal) {
                                currencyEngineAction(countryVal);
                                currencyEngineModel.lastCountryId(countryVal);
                            }
                        }
                    });
                });
            },

            /**
             * @return {|Boolean}
             */
            isCurrencyEngineEnabled: function() {
                return !!window.checkoutConfig.currencyEngineEnabled;
            }
        });
    };
});
