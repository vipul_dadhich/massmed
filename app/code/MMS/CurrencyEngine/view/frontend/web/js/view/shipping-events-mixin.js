define([
    'jquery',
    'ko',
    'mage/translate',
    'uiRegistry',
    'MMS_CurrencyEngine/js/model/currency-engine',
    'MMS_CurrencyEngine/js/action/currency-engine',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/quote',
    'Magento_Ui/js/lib/view/utils/async'
], function (
    $,
    ko,
    $t,
    uiRegistry,
    currencyEngineModel,
    currencyEngineAction,
    addressList,
    checkoutData,
    quote
) {
    'use strict';

    var billingCountryId = null,
        shippingCountryId = null,
        vatCountries = ["AT","BE","DK","FR","DE","GR","IE","IT","NL","PT","ES","SE","GB"],
        fieldsetName = 'checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset',
        shippingAddressVatIdSelector = '.checkout-index-index .field[name="shippingAddress\\.vat_id"]',
        shippingAddressVatIdInputSelector = 'div[name="shippingAddress\\.vat_id"] input',
        billingAddressSharedIdSelector = '.checkout-index-index .field[name="billingAddressshared\\.vat_id"]',
        billingAddressSharedVatIdSelector = 'div[name="billingAddressshared\\.vat_id"] input',
        billingCountrySelector = 'div[name="billingAddressshared\\.country_id"] select[name="country_id"]',
        shippingCountrySelector = 'div[name="shippingAddress\\.country_id"] select[name="country_id"]';

    return function(Component) {
        if (typeof (window.checkoutConfig.mmsCheckout) == 'undefined') {
            return Component.extend({});
        }

        return Component.extend({
            callCurrencyEngineTimeout: 0,
            validateDelay: 1000,
            triggerShippingEvents: ko.observable(false),

            /**
             * @returns {self}
             */
            initialize: function() {
                this._super();
                var self = this;

                self.isAddressSameAsBillingByEngine(true);
                if (window.checkoutConfig.currencyEngineCountryId !== undefined) {
                    currencyEngineModel.lastCountryId(window.checkoutConfig.currencyEngineCountryId);
                }

                $.async(shippingCountrySelector, function (element) {
                    $(element).on('change', function () {
                        if (self.triggerShippingEvents()) {
                            var val = $(this).val();
                            if ($.inArray(val, vatCountries) >= 0) {
                                $(shippingAddressVatIdSelector).show();
                            } else {
                                $(shippingAddressVatIdSelector).hide();
                                $(shippingAddressVatIdInputSelector).val('');
                            }
                        }
                    });
                });

                $.async(billingCountrySelector, function (element) {
                    if (currencyEngineModel.triggerBillingEvents()) {
                        if ($(element).val() && $.inArray($(element).val(), vatCountries) >= 0) {
                            $(billingAddressSharedIdSelector).show();
                        } else {
                            $(billingAddressSharedIdSelector).hide();
                            $(billingAddressSharedVatIdSelector).val('');
                        }
                    }
                });

                this.isAddressSameAsBilling.subscribe(function (value) {
                    if (value) {
                        self.triggerShippingEvents(false);
                        billingCountryId = $(billingCountrySelector).val();
                        let billingAddress = quote.billingAddress();
                        if (!billingAddress && addressList().length > 0) {
                            billingAddress = addressList.some(function (addressFromList) {
                                if (checkoutData.getSelectedBillingAddress() == addressFromList.getKey()) {
                                    return addressFromList;
                                }
                            });

                            if (!billingAddress && addressList().length === 1) {
                                billingAddress = addressList()[0];
                            }
                        }

                        if (addressList().length > 0 && billingAddress && billingAddress.countryId) {
                            billingCountryId = billingAddress.countryId;
                        }

                        $(shippingAddressVatIdSelector).hide();
                        if (billingCountryId && $.inArray(billingCountryId, vatCountries) >= 0) {
                            $(billingAddressSharedIdSelector).show();
                        } else {
                            $(billingAddressSharedIdSelector).hide();
                            $(billingAddressSharedVatIdSelector).val('');
                        }

                        if (self.isCurrencyEngineEnabled()) {
                            currencyEngineModel.isAddressSameAsBilling(true);
                            currencyEngineModel.triggerBillingEvents(true);
                            currencyEngineModel.triggerShippingEvents(false);

                            if (billingCountryId && currencyEngineModel.lastCountryId() !== billingCountryId) {
                                currencyEngineAction(billingCountryId).always(function() {
                                    self.useBillingForShipping();
                                });
                                currencyEngineModel.lastCountryId(billingCountryId);
                                return;
                            }
                        }

                        self.useBillingForShipping();
                    } else {
                        self.triggerShippingEvents(true);
                        shippingCountryId = $(shippingCountrySelector).val();
                        let shippingAddress = quote.shippingAddress();
                        if (!shippingAddress && addressList().length > 0) {
                            shippingAddress = addressList.some(function (addressFromList) {
                                if (checkoutData.getSelectedShippingAddress() == addressFromList.getKey()) {
                                    return addressFromList;
                                }
                            });

                            if (!shippingAddress && addressList().length === 1) {
                                shippingAddress = addressList()[0];
                            }
                        }

                        if (addressList().length > 0 && shippingAddress && shippingAddress.countryId) {
                            shippingCountryId = shippingAddress.countryId;
                        }

                        $(billingAddressSharedIdSelector).hide();
                        if (shippingCountryId && $.inArray(shippingCountryId, vatCountries) >= 0) {
                            $(shippingAddressVatIdSelector).show();
                        } else {
                            $(shippingAddressVatIdSelector).hide();
                            $(shippingAddressVatIdInputSelector).val('');
                        }

                        if (self.isCurrencyEngineEnabled()) {
                            currencyEngineModel.isAddressSameAsBilling(false);
                            currencyEngineModel.triggerBillingEvents(false);
                            currencyEngineModel.triggerShippingEvents(true);

                            if (shippingCountryId && currencyEngineModel.lastCountryId() !== shippingCountryId) {
                                currencyEngineAction(shippingCountryId).always(function() {
                                    self.useShippingForShipping();
                                });
                                currencyEngineModel.lastCountryId(shippingCountryId);
                                return;
                            }
                        }

                        self.useShippingForShipping();
                    }
                });

                this.isFormPopUpVisible.subscribe(function (value) {
                    if (value) {
                        let shippingCountryId = $(shippingCountrySelector).val();
                        $(billingAddressSharedIdSelector).hide();
                        if (shippingCountryId && $.inArray(shippingCountryId, vatCountries) >= 0) {
                            $(shippingAddressVatIdSelector).show();
                        } else {
                            $(shippingAddressVatIdSelector).hide();
                            $(shippingAddressVatIdInputSelector).val('');
                        }
                    }
                });

                if (quote.billingAddress() && quote.billingAddress().countryId) {
                    currencyEngineAction(quote.billingAddress().countryId).always(function() {
                        self.useBillingForShipping();
                    });
                }

                if (this.isCurrencyEngineEnabled()) {
                    uiRegistry.async(fieldsetName + '.country_id')(self.bindHandler.bind(self));
                }
            },

            bindHandler: function (element, delay) {
                var self = this;
                delay = typeof delay === 'undefined' ? self.validateDelay : delay;

                element.on('value', function () {
                    clearTimeout(self.callCurrencyEngineTimeout);
                    self.callCurrencyEngineTimeout = setTimeout(function () {
                        self.callCurrencyEngine(element);
                    }, delay);
                });
            },

            callCurrencyEngine: function (elem) {
                let countryVal = elem.value();
                if (countryVal === null || countryVal === '') {
                    return true;
                }
                if (this.isFormPopUpVisible() === false
                    && currencyEngineModel.triggerShippingEvents()
                    && currencyEngineModel.lastCountryId() !== countryVal) {
                    currencyEngineAction(countryVal);
                    currencyEngineModel.lastCountryId(countryVal);
                }
            },

            /**
             * @return {|Boolean}
             */
            isCurrencyEngineEnabled: function() {
                return !!window.checkoutConfig.currencyEngineEnabled;
            }
        });
    };
});
