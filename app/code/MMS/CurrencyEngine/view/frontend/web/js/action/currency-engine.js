define([
    'jquery',
    'Magento_Checkout/js/model/url-builder',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/action/get-totals',
    'MMS_Renew/js/action/renewal-rates',
    'mage/storage',
    'Magento_Ui/js/lib/view/utils/async'
], function (
    $,
    urlBuilder,
    customer,
    customerData,
    quote,
    fullScreenLoader,
    getTotalsAction,
    renewalRatesAction,
    storage
) {
    'use strict';

    return function (countryVal) {
        if (countryVal === '') {
            countryVal = 'US';
        }

        let url, urlParams, serviceUrl, payload;
        if (customer.isLoggedIn()) {
            url = '/carts/mine/update-country';
            urlParams = {};
            payload = {
                cartId: quote.getQuoteId(),
                country: countryVal
            };
        } else {
            url = '/guest-carts/:cartId/update-country';
            urlParams = {
                cartId: quote.getQuoteId()
            };
            payload = {
                country: countryVal
            };
        }

        serviceUrl = urlBuilder.createUrl(url, urlParams);
        fullScreenLoader.startLoader();
        return storage.post(
            serviceUrl,
            JSON.stringify(payload)
        ).done(
            function (response) {
                if (response.status === 1) {
                    window.checkoutConfig.priceFormat = response.price_format;
                }
                customerData.reload(['cart'], true);
                if (window.checkoutConfig.quoteData.is_quote_renew !== undefined
                    && window.checkoutConfig.quoteData.is_quote_renew === "1") {
                    renewalRatesAction();
                }
                var deferred = $.Deferred();
                getTotalsAction([], deferred);
                fullScreenLoader.stopLoader();
            }
        ).fail(
            function (response) {
                fullScreenLoader.stopLoader();
            }
        );
    };
});
