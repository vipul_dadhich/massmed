define([], function(){
    'use strict';

    return function(targetModule) {
        targetModule.getPriceFormat = function () {
            return window.checkoutConfig.priceFormat;
        };
        targetModule.getBasePriceFormat = function () {
            return window.checkoutConfig.basePriceFormat;
        };

        return targetModule;
    };
});
