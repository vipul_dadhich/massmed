define([], function(){
    'use strict';

    var mixin = {
        isBaseGrandTotalDisplayNeeded: function () {
            return false;
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
