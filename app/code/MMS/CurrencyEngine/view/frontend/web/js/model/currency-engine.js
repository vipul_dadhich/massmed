/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * @api
 */
define([
    'ko',
    'underscore',
    'domReady!'
], function (ko, _) {
    'use strict';

    var defaultCountryId = ko.observable(window.checkoutConfig.defaultCountryId);

    return {
        defaultCountryId: defaultCountryId,
        lastCountryId: ko.observable(null),
        isAddressSelected: null,
        isAddressSameAsBilling: ko.observable(true),
        triggerBillingEvents: ko.observable(true),
        triggerShippingEvents: ko.observable(false),

        /**
         *
         * @return {*}
         */
        getDefaultCountryId: function () {
            return defaultCountryId;
        },

        /**
         * @param {Object} data
         */
        setDefaultCountryId: function (data) {
            defaultCountryId(data);
        }
    };
});
