var config = {
    config: {
        mixins: {
            'Ey_Checkout/js/view/billing-address': {
                'MMS_CurrencyEngine/js/view/billing-events-mixin': true
            },
            'Magento_Checkout/js/view/shipping': {
                'MMS_CurrencyEngine/js/view/shipping-events-mixin': true
            },
            'Magento_Checkout/js/model/quote': {
                'MMS_CurrencyEngine/js/model/quote-mixin': true
            },
            'Magento_Tax/js/view/checkout/summary/grand-total': {
                'MMS_CurrencyEngine/js/view/checkout/summary/grand-total-mixin': true
            },
            'Magento_Checkout/js/action/set-billing-address': {
                'MMS_CurrencyEngine/js/action/set-billing-address-mixin': true
            }
        }
    }
};
