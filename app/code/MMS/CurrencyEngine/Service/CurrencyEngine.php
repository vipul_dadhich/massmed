<?php

namespace MMS\CurrencyEngine\Service;
use MMS\CurrencyEngine\Api\LoggerInterface as CurrencyEngineLogger;
use MMS\PriceEngine\Service\PriceEngine as PriceEngineService;

/**
 * Class CurrencyEngine
 * @package MMS\CurrencyEngine\Service
 */
class CurrencyEngine extends AbstractCurrencyEngine
{
    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->_dataHelper->isEnabled();
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        if ($this->_currencyCode == null) {
            $this->_currencyCode = $this->_dataHelper->getCurrencyCode()?? $this->getPriceEngineService()->getCurrencyCode();
        }
        return $this->_currencyCode;
    }

    /**
     * @param $currencyCode
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->_currencyCode = $currencyCode;
    }

    /**
     * @return string|null
     */
    public function getCountryCodeSession()
    {
        return $this->_dataHelper->getCountryCode();
    }

    /**
     * @return string|null
     */
    public function getCountryCodePriceEngine()
    {
        return $this->getPriceEngineService()->getCountry();
    }
}
