<?php

namespace MMS\CurrencyEngine\Service;

use MMS\CurrencyEngine\Helper\Data as DataHelper;
use MMS\CurrencyEngine\Api\LoggerInterface as CurrencyEngineLogger;
use MMS\PriceEngine\Service\PriceEngine as PriceEngineService;

/**
 * Class AbstractCurrencyEngine
 * @package MMS\CurrencyEngine\Service
 */
abstract class AbstractCurrencyEngine
{
    public $service = 'currency_engine';
    protected $_currencyCode = null;

    /**
     * @var DataHelper
     */
    protected $_dataHelper;
    /**
     * @var CurrencyEngineLogger
     */
    protected $_currencyLogger;
    /**
     * @var PriceEngineService
     */
    protected $_priceEngineService;

    /**
     * AbstractCurrencyEngine constructor.
     * @param DataHelper $dataHelper
     * @param CurrencyEngineLogger $currencyLogger
     * @param PriceEngineService $priceEngineService
     */
    public function __construct(
        DataHelper $dataHelper,
        CurrencyEngineLogger $currencyLogger,
        PriceEngineService $priceEngineService
    ) {
        $this->_dataHelper = $dataHelper;
        $this->_currencyLogger = $currencyLogger;
        $this->_priceEngineService = $priceEngineService;
    }

    /**
     * @return bool
     */
    abstract public function isEnabled(): bool;

    /**
     * @return string
     */
    abstract public function getCurrencyCode(): string;

    /**
     * @param $currencyCode
     */
    abstract public function setCurrencyCode($currencyCode);

    /**
     * @return CurrencyEngineLogger
     */
    public function getCurrencyEngineLogger(): CurrencyEngineLogger
    {
        return $this->_currencyLogger;
    }

    /**
     * @return PriceEngineService
     */
    public function getPriceEngineService(): PriceEngineService
    {
        return $this->_priceEngineService;
    }

    /**
     * Recall the price engine service for updating the store currency
     *
     * @param bool $skipCache
     * @return Float|int
     */
    public function callPriceEngine($skipCache = false)
    {
        $request = ['sku' => 'CAT-ANL-DFT'];
        $this->getPriceEngineService()->setFullResponse(true);
        $this->getPriceEngineService()->setRequest($request);
        $this->getPriceEngineService()->buildPrice($skipCache);
        return $this->getPriceEngineService()->getPrice();
    }
}
