<?php

namespace MMS\CurrencyEngine\Controller\Action;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Encryption\UrlCoder;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;
use MMS\CurrencyEngine\Service\CurrencyEngine as CurrencyEngineService;

/**
 * Class Professionalcategory
 * @package MMS\CurrencyEngine\Controller\Action
 */
class Professionalcategory extends Action
{
    /**
     * @var JsonFactory
     */
    protected $_jsonResultFactory;

    /**
     * @var SessionCurrencyEngine
     */
    protected $_session;

    /**
     * @var CurrencyEngineService
     */
    protected $_currencyEngineService;
    /**
     * @var UrlCoder
     */
    protected $_urlCoder;

    /**
     * Country constructor.
     * @param Context $context
     * @param JsonFactory $jsonResultFactory
     * @param SessionCurrencyEngine $session
     * @param CurrencyEngineService $currencyEngineService
     * @param UrlCoder $urlCoder
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonResultFactory,
        SessionCurrencyEngine $session,
        CurrencyEngineService $currencyEngineService,
        UrlCoder $urlCoder
    ) {
        $this->_jsonResultFactory = $jsonResultFactory;
        $this->_session = $session;
        $this->_currencyEngineService = $currencyEngineService;
        $this->_urlCoder = $urlCoder;
        parent::__construct($context);
    }

    /**
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Store\Model\StoreManagerInterface $storeManager */
        $storeManager = $this->_objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);

        if ($this->_currencyEngineService->isEnabled()) {
            $professionalCategory = $this->getRequest()->getParam('professionalcategory', false);
            if ($professionalCategory) {
                $this->_session->addProfessionalCategory($professionalCategory);
                $this->_currencyEngineService->callPriceEngine();
                $this->_eventManager->dispatch('currency_engine_update_currency', [
                    'country' => $this->_session->getCountry(),
                    'currency' => $this->_session->getCurrency(),
                    'professional_category' => $this->_session->getProfessionalCategory(),
                ]);
            }
        }

        $encodedUrl = $this->getRequest()->getParam(ActionInterface::PARAM_NAME_URL_ENCODED)
            ?: $this->getRequest()->getParam(ActionInterface::PARAM_NAME_BASE64_URL);
        $refererUrl = $this->getRequest()->getServer('HTTP_REFERER');
        if ($encodedUrl) {
            $refererUrl = $this->_urlCoder->decode($encodedUrl);
        }

        if (!$refererUrl) {
            $storeUrl = $storeManager->getStore()->getBaseUrl();
            $refererUrl = $this->_redirect->getRedirectUrl($storeUrl);
        }

        if($professionalCategory) {
            $url_data = parse_url($refererUrl);
            if (isset($url_data['query'])) {
                //we have some parameters
                parse_str($url_data['query'], $params);
                //if pupdate exists, update it
                if(isset($params['pupdate'])) {
                    $concat_character = count($params) > 1 ? '&':'?';
                    $needle = count($params) > 1 ? '&pupdate':'?pupdate';
                    $refererUrl = strstr($refererUrl, $needle, true);
                    $refererUrl .= $concat_character . 'pupdate=' . $professionalCategory;
                    $this->getResponse()->setRedirect($refererUrl);
                }
                else {
                    //pupdate doesn't exist so just add it to the end
                    $refererUrl .= '&pupdate=' . $professionalCategory;
                    $this->getResponse()->setRedirect($refererUrl);
                }
            }
            else {
                //no existing params but professionalCategory was changed
                $refererUrl .= '?pupdate=' . $professionalCategory;
                $this->getResponse()->setRedirect($refererUrl);
            }
        }
        else {
            //method was called but professionalCategory not set
            $this->getResponse()->setRedirect($refererUrl);
        }
    }
}
