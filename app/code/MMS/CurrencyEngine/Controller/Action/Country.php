<?php

namespace MMS\CurrencyEngine\Controller\Action;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Encryption\UrlCoder;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;
use MMS\CurrencyEngine\Service\CurrencyEngine as CurrencyEngineService;

/**
 * Class Country
 * @package MMS\CurrencyEngine\Controller\Action
 */
class Country extends Action
{
    /**
     * @var JsonFactory
     */
    protected $_jsonResultFactory;

    /**
     * @var SessionCurrencyEngine
     */
    protected $_session;
    /**
     * @var CurrencyEngineService
     */
    protected $_currencyEngineService;
    /**
     * @var UrlCoder
     */
    protected $_urlCoder;

    /**
     * Country constructor.
     * @param Context $context
     * @param JsonFactory $jsonResultFactory
     * @param SessionCurrencyEngine $session
     * @param CurrencyEngineService $currencyEngineService
     * @param UrlCoder $urlCoder
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonResultFactory,
        SessionCurrencyEngine $session,
        CurrencyEngineService $currencyEngineService,
        UrlCoder $urlCoder
    ) {
        $this->_jsonResultFactory = $jsonResultFactory;
        $this->_session = $session;
        $this->_currencyEngineService = $currencyEngineService;
        $this->_urlCoder = $urlCoder;
        parent::__construct($context);
    }

    /**
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Store\Model\StoreManagerInterface $storeManager */
        $storeManager = $this->_objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);

        if ($this->_currencyEngineService->isEnabled()) {
            $country = $this->getRequest()->getParam('country', false);
            if ($country) {
                $this->_session->addCountry($country);
                $response = $this->_currencyEngineService->callPriceEngine();
                $currency = isset($response['products'][0]['currency']) ? $response['products'][0]['currency'] : 'USD';
                $allowedCurrencies = $storeManager->getStore()->getAvailableCurrencyCodes();
                $currency = strtoupper($currency);
                if (in_array($currency, $allowedCurrencies)) {
                    $this->_session->addCurrency($currency);
                    $storeManager->getStore()->setCurrentCurrencyCode($currency);
                    $this->_eventManager->dispatch('currency_engine_update_currency', [
                        'country' => $country,
                        'currency' => $currency,
                        'professional_category' => $this->_session->getProfessionalCategory()
                    ]);
                }
            }
        }

        $encodedUrl = $this->getRequest()->getParam(ActionInterface::PARAM_NAME_URL_ENCODED)
            ?: $this->getRequest()->getParam(ActionInterface::PARAM_NAME_BASE64_URL);
        $refererUrl = $this->getRequest()->getServer('HTTP_REFERER');
        if ($encodedUrl) {
            $refererUrl = $this->_urlCoder->decode($encodedUrl);
        }

        if (!$refererUrl) {
            $storeUrl = $storeManager->getStore()->getBaseUrl();
            $refererUrl = $this->_redirect->getRedirectUrl($storeUrl);
        }

        $this->getResponse()->setRedirect($refererUrl);
    }
}
