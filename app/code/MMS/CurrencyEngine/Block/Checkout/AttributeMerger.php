<?php

namespace MMS\CurrencyEngine\Block\Checkout;

use Magento\Customer\Api\CustomerRepositoryInterface as CustomerRepository;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Helper\Address as AddressHelper;
use Magento\Customer\Model\Session;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;
use MMS\CurrencyEngine\Service\CurrencyEngine as CurrencyEngineService;

/**
 * Class AttributeMerger
 * @package MMS\CurrencyEngine\Block\Checkout
 */
class AttributeMerger extends \Magento\Checkout\Block\Checkout\AttributeMerger
{
    /**
     * @var AddressHelper
     */
    private $addressHelper;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var CustomerInterface
     */
    private $customer;

    /**
     * @var \Magento\Directory\Helper\Data
     */
    private $directoryHelper;

    /**
     * List of codes of countries that must be shown on the top of country list
     *
     * @var array
     */
    private $topCountryCodes;

    /**
     * @var CurrencyEngineService
     */
    protected $_currencyEngineService;
    /**
     * @var SessionCurrencyEngine
     */
    protected $_sessionCurrencyEngine;
    /**
     * @var CountryFactory
     */
    protected $_countryFactory;

    /**
     * AttributeMerger constructor.
     * @param AddressHelper $addressHelper
     * @param Session $customerSession
     * @param CustomerRepository $customerRepository
     * @param DirectoryHelper $directoryHelper
     * @param CurrencyEngineService $currencyEngineService
     * @param SessionCurrencyEngine $sessionCurrencyEngine
     * @param CountryFactory $countryFactory
     */
    public function __construct(
        AddressHelper $addressHelper,
        Session $customerSession,
        CustomerRepository $customerRepository,
        DirectoryHelper $directoryHelper,
        CurrencyEngineService $currencyEngineService,
        SessionCurrencyEngine $sessionCurrencyEngine,
        CountryFactory $countryFactory
    ) {
        $this->addressHelper = $addressHelper;
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->directoryHelper = $directoryHelper;
        $this->topCountryCodes = $directoryHelper->getTopCountryCodes();

        $this->_currencyEngineService = $currencyEngineService;
        $this->_sessionCurrencyEngine = $sessionCurrencyEngine;
        $this->_countryFactory = $countryFactory;

        parent::__construct(
            $addressHelper,
            $customerSession,
            $customerRepository,
            $directoryHelper
        );
    }

    /**
     * Returns default attribute value.
     *
     * @param string $attributeCode
     * @throws NoSuchEntityException
     * @throws LocalizedException
     * @return null|string
     */
    protected function getDefaultValue($attributeCode): ?string
    {
        if ($attributeCode === 'country_id') {
            if ($this->_currencyEngineService->isEnabled() && $this->_sessionCurrencyEngine->getCountry()) {
                $countryCode = $this->_sessionCurrencyEngine->getCountry();
                try {
                    return $this->_countryFactory->create()->loadByCode($countryCode)->getCountryId();
                } catch (\Exception $ex) {
                }
            }

            return $this->directoryHelper->getDefaultCountry();
        }

        return parent::getDefaultValue($attributeCode);
    }
}
