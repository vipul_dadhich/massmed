<?php
namespace MMS\CurrencyEngine\Block\Widget;

use Magento\Framework\Url\Helper\Data as FrameworkUrlHelper;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Widget\Block\BlockInterface;
use MMS\PriceEngine\Service\PriceEngine as PriceEngineService;

/**
 * Class ProfessionalCategory
 * @package MMS\CurrencyEngine\Block\Widget
 */
class ProfessionalCategory extends Template implements BlockInterface
{
    /**
     * @var string
     */
    protected $_template = "widget/professional_category.phtml";

    /**
     * @var PriceEngineService
     */
    protected $_priceEngineService;
    /**
     * @var \MMS\Customer\Model\Source\ProfessionalCategory
     */
    protected $professionalCategory;
    /**
     * @var FrameworkUrlHelper
     */
    protected $_urlHelper;

    /**
     * ProfessionalCategory constructor.
     * @param Context $context
     * @param PriceEngineService $priceEngine
     * @param \MMS\Customer\Model\Source\ProfessionalCategory $professionalCategory
     * @param FrameworkUrlHelper $urlHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        PriceEngineService $priceEngine,
        \MMS\Customer\Model\Source\ProfessionalCategory $professionalCategory,
        FrameworkUrlHelper $urlHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_priceEngineService = $priceEngine;
        $this->professionalCategory = $professionalCategory;
        $this->_urlHelper = $urlHelper;
    }

    /**
     * @return string
     */
    public function getDefaultProfessionalCategory()
    {
        return $this->_priceEngineService->getProfessionalCategory();
    }

    /**
     * @return array|array[]
     */
    public function getProfessionalCategoryList()
    {
        return $this->professionalCategory->getAllOptions();
    }

    /**
     * @return false|\Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        return $this->_priceEngineService->getCustomer();
    }

    /**
     * @return string
     */
    public function getSwitchUrl(): string
    {
        $encodedUrl = $this->_urlHelper->getEncodedUrl();
        return $this->getUrl(
            'currencyengine/action/professionalcategory',
            [
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED => $encodedUrl
            ]
        );
    }

    /**
     * @return array
     */
    public function getFilteredProfessionalCategory(): array
    {
        $profCatOptions = [];
        $professionalCategoryList = $this->getProfessionalCategoryList();
        if (count($professionalCategoryList) > 0) {
            foreach ($professionalCategoryList as $professionalCategory) {
                if (strlen($professionalCategory['value']) > 0) {
                    if ($this->applyPhysicianCatRule($professionalCategory)) {
                        continue;
                    }

                    if ($this->applyOtherCatRule($professionalCategory)) {
                        continue;
                    }

                    if ($this->applyResidentCatRule($professionalCategory)) {
                        continue;
                    }

                    $profCatOptions[] = $professionalCategory;
                }
            }
        }

        return $profCatOptions;
    }

    /**
     * @param $professionalCategory
     * @return bool
     */
    private function applyPhysicianCatRule($professionalCategory): bool
    {
        if ($this->getCustomer() && $this->getCustomer()->getProfessionalCategory() == 'PHY'
            && ($professionalCategory['value'] == 'RES'
                || $professionalCategory['value'] == 'PA'
                || $professionalCategory['value'] == 'NUR'
                || $professionalCategory['value'] == 'NP'
                || $professionalCategory['value'] == 'STU'
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param $professionalCategory
     * @return bool
     */
    private function applyResidentCatRule($professionalCategory): bool
    {
        if ($this->getCustomer() && $this->getCustomer()->getProfessionalCategory() == 'RES'
            && ($professionalCategory['value'] == 'STU')
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param $professionalCategory
     * @return bool
     */
    private function applyOtherCatRule($professionalCategory): bool
    {
        if ($this->getCustomer() && $this->getCustomer()->getProfessionalCategory() == 'OTH'
            && ($professionalCategory['value'] == 'RES'
                || $professionalCategory['value'] == 'PA'
                || $professionalCategory['value'] == 'NUR'
                || $professionalCategory['value'] == 'NP'
                || $professionalCategory['value'] == 'STU'
            )
        ) {
            return true;
        }

        return false;
    }
}
