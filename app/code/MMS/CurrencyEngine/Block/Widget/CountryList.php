<?php
namespace MMS\CurrencyEngine\Block\Widget;

use Magento\Directory\Model\ResourceModel\Country\Collection;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory;
use Magento\Framework\Url\Helper\Data as FrameworkUrlHelper;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use MMS\PriceEngine\Service\PriceEngine as PriceEngineService;

/**
 * Class CountryList
 * @package MMS\CurrencyEngine\Block\Widget
 */
class CountryList extends Template implements BlockInterface
{

    protected $_template = "widget/country_list.phtml";

    /**
     * @var CollectionFactory
     */
    protected $_countryCollectionFactory;

    /**
     * @var PriceEngineService
     */
    protected $_priceEngineService;

    /**
     * @var FrameworkUrlHelper
     */
    protected $_urlHelper;

    /**
     * CountryList constructor.
     * @param Template\Context $context
     * @param CollectionFactory $countryCollectionFactory
     * @param PriceEngineService $priceEngine
     * @param FrameworkUrlHelper $urlHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        CollectionFactory $countryCollectionFactory,
        PriceEngineService $priceEngine,
        FrameworkUrlHelper $urlHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_countryCollectionFactory = $countryCollectionFactory;
        $this->_priceEngineService = $priceEngine;
        $this->_urlHelper = $urlHelper;
    }

    /**
     * @return Collection
     */
    public function getCountryCollection()
    {
        return $this->_countryCollectionFactory->create()->loadByStore();
    }

    /**
     * @return string
     */
    public function getDefaultCountry()
    {
        return $this->_priceEngineService->getCountry();
    }

    /**
     * @param $countryList
     * @return mixed
     */
    public function getCountryISO3Code($countryList)
    {
        $countryCollection = $this->_countryCollectionFactory->create()->loadByStore();

        foreach ($countryList as &$country) {
            if(strlen($country['value']) > 0){
                foreach ($countryCollection as $countryData) {
                    if($country['value'] === $countryData->getData('country_id')){
                        $country['iso3_code'] = $countryData->getData('iso3_code');
                    }
                }
            }
        }
        return $countryList;
    }

    /**
     * @return string
     */
    public function getSwitchUrl(): string
    {
        $encodedUrl = $this->_urlHelper->getEncodedUrl();
        return $this->getUrl(
            'currencyengine/action/country',
            [
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED => $encodedUrl
            ]
        );
    }

    /**
     * @return bool
     */
    public function isExclusiveAgent()
    {
        return (bool) $this->getData('exclusive_agent');
    }
}
