<?php

namespace MMS\CurrencyEngine\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use MMS\CurrencyEngine\Model\Session as SessionCurrencyEngine;

/**
 * Class Data
 * @package MMS\CurrencyEngine\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_ENABLED = 'currency_engine/general/enable';
    const XML_LOG_PATH_ACTIVE = 'currency_engine/general/logs_active';
    /**
     * @var SessionCurrencyEngine
     */
    protected $_session;

    /**
     * Data constructor.
     * @param Context $context
     * @param SessionCurrencyEngine $session
     */
    public function __construct(
        Context $context,
        SessionCurrencyEngine $session
    ) {
        $this->_session = $session;
        parent::__construct($context);
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isEnabled($store = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isLogEnable($store = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_LOG_PATH_ACTIVE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->_session->getCurrency();
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->_session->getCountry();
    }
}
