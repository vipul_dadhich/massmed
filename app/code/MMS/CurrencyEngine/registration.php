<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_CurrencyEngine extension
 * NOTICE OF LICENSE
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'MMS_CurrencyEngine',
    __DIR__
);
