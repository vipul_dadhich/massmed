/**
 * Copyright © 2018 MassMed. All rights reserved.
 * See LICENSE file for license details.
 */
define([
    'jquery',
    'mage/url'
], function ($) {
    'use strict';

    return {
        checkVat: function(vatId, countryId){
            var vatexp = new Array ();
            vatexp.push (/^(AT)U(\d{8})$/);                           //** Austria
            vatexp.push (/^(BE)(0?\d{9})$/);                          //** Belgium 
            vatexp.push (/^(BE)([0-1]\d{9})$/);                       //** Belgium - since 01/01/2020
            vatexp.push (/^(DE)([1-9]\d{8})$/);                       //** Germany 
            vatexp.push (/^(DK)(\d{8})$/);                            //** Denmark 
            //vatexp.push (/^(ES)([A-Z]\d{8})$/);                       //** Spain (National juridical entities)
            vatexp.push (/^(ES)([A-Z0-9]{9})$/);              //** Spain (Other juridical entities)
            //vatexp.push (/^(ES)([0-9YZ]\d{7}[A-Z])$/);                //** Spain (Personal entities type 1)
           // vatexp.push (/^(ES)([KLMX]\d{7}[A-Z])$/);                 //** Spain (Personal entities type 2)
            vatexp.push (/^(FR)(\d{11})$/);                           //** France (1)
            vatexp.push (/^(FR)([A-HJ-NP-Z]\d{10})$/);                // France (2)
            vatexp.push (/^(FR)(\d[A-HJ-NP-Z]\d{9})$/);               // France (3)
            vatexp.push (/^(FR)([A-HJ-NP-Z]{2}\d{9})$/);              // France (4)
            vatexp.push (/^(GB)(\d{9})$/);                           //** UK (Standard)
            vatexp.push (/^(GB)(\d{13})$/);                          //** UK (Branches)
            vatexp.push (/^(GB)(GD\d{4})$/);                         //** UK (Government)
            vatexp.push (/^(GB)(HA\d{4})$/);                         //** UK (Health authority)
            vatexp.push (/^(IE)(\d{7}[A-W])$/);                       //** Ireland (1)
            vatexp.push (/^(IE)([7-9][A-Z\*\+)]\d{5}[A-W])$/);        //** Ireland (2)
            vatexp.push (/^(IE)(\d{7}[A-W][AH])$/);                   //** Ireland (3)
            vatexp.push (/^(IT)(\d{11})$/);                           //** Italy 
            vatexp.push (/^(NL)(\d{9}B\d{3})$/);                      //** Netherlands
            vatexp.push (/^(PT)(\d{9})$/);                            //** Portugal
            vatexp.push (/^(SE)(\d{12})$/);                         //** Sweden
            vatexp.push (/^(EL)(0?\d{9})$/);                          //** Greece

            var VATNumber = vatId.toUpperCase();
            // Remove spaces etc. from the VAT number to help validation
            VATNumber = VATNumber.replace (/(\s|-|\.)+/g, '');
            // Assume we're not going to find a valid VAT number
            var valid = false;                        
            // Check the string against the regular expressions for all types of VAT numbers
            for (var i=0; i<vatexp.length; i++) {
                var rExp = vatexp[i];
                if (rExp.toString().indexOf(countryId) > 0 || (rExp.toString().indexOf('EL')>0 && countryId === 'GR')) {
                    if (vatexp[i].test(VATNumber)) {
                        valid = true;
                        break;
                    }
                }
            }
            return valid;
        }
    }
});
