require([
    'jquery',
    'MMS_Tax/js/vat-validation'
], function ($, vatTax) {
    'use strict';

    var bodySelector = 'body',
        vatIdErrorSelector = '.vat-id-error',
        zipCodeErrorSelector = '.zip-code-error',
        billingAddressPostcodeSelector = '.billing-address-form input[name="postcode"]',
        billingAddressSharedCountryIdSelector = 'div[name="billingAddressshared\\.country_id"] select',
        billingAddressVatIdSelector = '.billing-address-form input[name="vat_id"]',
        shippingVatIdErrorSelector = '.vat-id-error-shipping',
        shippingAddressCountryIdSelector = 'div[name="shippingAddress\\.country_id"] select',
        shippingAddressVatIdSelector = '.form-shipping-address input[name="vat_id"]';

    $(bodySelector).delegate(billingAddressPostcodeSelector,"keyup",function() {
        $(zipCodeErrorSelector).remove();
    });

    $(bodySelector).delegate(billingAddressVatIdSelector,"blur",function() {
        if ($(this).val() !== "") {
            var isValid = true;
            var country_id = $(billingAddressSharedCountryIdSelector).val();
            if (!vatTax.checkVat($(this).val(), country_id)) {
                isValid = false;
            }

            if (!isValid && !$(vatIdErrorSelector).length > 0) {
                $(this).after("<div class='field-error vat-id-error' generated='true'>" +
                    "<span>Please enter a valid VAT Number.</span>" +
                    "</div>"
                );
            }
        }
    });

    $(bodySelector).delegate(billingAddressVatIdSelector,"keyup",function() {
        $(vatIdErrorSelector).remove();
    });

    $(bodySelector).delegate(shippingAddressVatIdSelector, "blur",function() {
        if ($(this).val() !== "") {
            var isValid = true;
            var country_id = $(shippingAddressCountryIdSelector).val();
            if (!vatTax.checkVat($(this).val(), country_id)) {
                isValid = false;
            }

            if (!isValid && !$(shippingVatIdErrorSelector).length > 0) {
                $(this).after("<div class='field-error vat-id-error vat-id-error-shipping' generated='true'>" +
                    "<span>Please enter a valid VAT Number.</span>" +
                    "</div>"
                );
            }
        }
    });

    $(bodySelector).delegate(shippingAddressVatIdSelector,"keyup",function() {
        $(shippingVatIdErrorSelector).remove();
    });
});
