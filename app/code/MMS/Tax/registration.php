<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_Tax extension
 * NOTICE OF LICENSE
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'MMS_Tax',
    __DIR__
);
