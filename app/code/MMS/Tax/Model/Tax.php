<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_Tax extension
 * NOTICE OF LICENSE
 */
namespace MMS\Tax\Model;

use DSTax\OSITD\OSITD;
use Magento\Customer\Api\Data\AddressInterfaceFactory as CustomerAddressFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory as CustomerAddressRegionFactory;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;

/**
 * Tax totals calculation model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Tax extends \DSTax\TaxConnect\Model\Sales\Total\Quote\Tax
{
    /**
     * @var \MMS\Logger\LoggerInterface
     */
    protected $_mmsLogger;
    protected $loggingEnabled;
    protected $logDirectory;

    /**
     * @var \Magento\Directory\Helper\Data
     */
    protected $_directoryHelper;

    /**
     * Tax constructor.
     * @param \Magento\Tax\Model\Config $taxConfig
     * @param \Magento\Tax\Api\TaxCalculationInterface $taxCalculationService
     * @param \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsDataObjectFactory
     * @param \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemDataObjectFactory
     * @param \Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory $taxClassKeyDataObjectFactory
     * @param CustomerAddressFactory $customerAddressFactory
     * @param CustomerAddressRegionFactory $customerAddressRegionFactory
     * @param \Magento\Tax\Helper\Data $taxData
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Tax\Api\Data\QuoteDetailsItemExtensionFactory $extensionFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \MMS\Logger\LoggerInterface $mmsLogger
     * @param \Magento\Directory\Helper\Data $directoryHelper
     */
    public function __construct(
        \Magento\Tax\Model\Config $taxConfig,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculationService,
        \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsDataObjectFactory,
        \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemDataObjectFactory,
        \Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory $taxClassKeyDataObjectFactory,
        CustomerAddressFactory $customerAddressFactory,
        CustomerAddressRegionFactory $customerAddressRegionFactory,
        \Magento\Tax\Helper\Data $taxData,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Tax\Api\Data\QuoteDetailsItemExtensionFactory $extensionFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \MMS\Logger\LoggerInterface $mmsLogger,
        \Magento\Directory\Helper\Data $directoryHelper
    ) {
        $this->_mmsLogger = $mmsLogger;
        $this->_directoryHelper = $directoryHelper;
        parent::__construct(
            $taxConfig,
            $taxCalculationService,
            $quoteDetailsDataObjectFactory,
            $quoteDetailsItemDataObjectFactory,
            $taxClassKeyDataObjectFactory,
            $customerAddressFactory,
            $customerAddressRegionFactory,
            $taxData,
            $priceCurrency,
            $extensionFactory,
            $logger,
            $scopeConfig
        );
    }

    /**
     * Get tax from OneSource
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @param $headerQuote
     * @return string
     */
    public function getTaxFromOneSource(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total,
        $headerQuote
    ) {
        $url = basename($_SERVER['REQUEST_URI']);
        $address = $shippingAssignment->getShipping()->getAddress();
        $addressPostcode = $address->getPostcode();
        $passRequestPostcode = true;
        if (!$addressPostcode && $this->_directoryHelper->isZipCodeOptional($address->getCountryId())) {
            $passRequestPostcode = false;
        }

        if (!$addressPostcode && $passRequestPostcode) {
            return;
        }

        // Do not make a tax call if the cart is empty
        if (!count($address->getAllItems())) {
            return;
        }

        $this->_mmsLogger->info('URL:' . $url);
        $this->_mmsLogger->info('Tx_SOAP_WSDL:' . $this->getConfigValue('SOAP_WSDL'));
        $this->_mmsLogger->info('Tx_UserName:' . $this->getConfigValue('UserName'));


        // Call ONESOURCE for a tax quote
        $ositd = new OSITD();
        $ositd->useTaxCalculationService();
        // if ($this->getConfigValue('TaxService') == 'Simple Tax Service') {
        //     $ositd->useSimpleTaxService();
        // } else {
        //     $ositd->useTaxCalculationService();
        // }
        $ositd->set('wsdl', $this->getConfigValue('SOAP_WSDL'));
        $ositd->set(
            'configurables',
            array(
                'CallingClient' => $this->getConfigValue('CallingClient'),
                'CallingSource' => $this->getConfigValue('CallingSource'),
                'CallingSystemNumber' => $this
                    ->getConfigValue('CallingSystemNumber'),
                'CallingUser' => $this->getConfigValue('CallingUser'),
                'CompanyRole' => $this->getConfigValue('CompanyRole'),
                'CurrencyCode' => $this->getConfigValue('CurrencyCode'),
                'DbVersion' => "dummy db info",
                'ERPVersion' => $this->getConfigValue('ERPVersion'),
                'ExternalCompanyId' => $this->getConfigValue('ExternalCompanyId'),
                'HostSystemNumber' => $this->getConfigValue('HostSystemNumber'),
                'Incoterms' => $this->getConfigValue('Incoterms'),
                'IntegrationVersion' => $this->getConfigValue('IntegrationVersion'),
                'Password' => $this->getConfigValue('Password'),
                'PointOfTitleTransfer' => $this
                    ->getConfigValue('PointOfTitleTransfer'),
                'PostToAudit' => $this->getConfigValue('PostToAudit'),
                'ProductCode_Discount' => $this
                    ->getConfigValue('ProductCode_Discount'),
                'ProductCode_Shipping' => $this
                    ->getConfigValue('ProductCode_Shipping'),
                'SDKVersion' => $this->getConfigValue('SDKVersion'),
                'ShipFromAddress' => null,
                'ShipFromCity' => $this->getConfigValue('ShipFromCity'),
                'ShipFromCountry' => $this->getConfigValue('ShipFromCountry'),
                'ShipFromGeoCode' => $this->getConfigValue('ShipFromGeoCode'),
                'ShipFromPostalCode' => $this->getConfigValue('ShipFromPostalCode'),
                'ShipFromRegion' => $this->getConfigValue('ShipFromState'),
                'UOM' => $this->getConfigValue('UOM'),
                'Username' => $this->getConfigValue('UserName')
            )
        );
        /*
         * Create an associative array for the bill to address.
         */
        $BillToAddress = array();
        $billingAddress = $quote->getBillingAddress();
        if ($billingAddress->getCountry() != null
            AND $billingAddress->getCity() != null
            AND $billingAddress->getPostcode() != null
        ) {
            $BillToAddress = array(
                'Country' => $billingAddress->getCountry(),
                'Region' => $billingAddress->getRegionCode(),
                'City' => $billingAddress->getCity(),
                'PostalCode' => $this->getZipcode(
                    $billingAddress->getCountry(), $billingAddress->getPostcode()
                ),
                'GeoCode' => $this->getZipExtension(
                    $billingAddress->getCountry(), $billingAddress->getPostcode()
                ),
            );

            if ($billingAddress->getCountry() === 'CA') {
                $BillToAddress['Province'] = $BillToAddress['Region'];
                unset($BillToAddress['Region']);
            }
        }
        /*
         * Create an associative array for the ship to address.
         */
        $ShipToAddress = [
            'Address1' => $address->getStreetFull(),
            'City' => $address->getCity(),
            'Country' => $address->getCountry(),
            'GeoCode' => $this
                ->getZipExtension($address->getCountry(), $addressPostcode),
            'PartnerName' => $headerQuote['customerName'],
            'PartnerNumber' => $headerQuote['customerID'],
            'PostalCode' => $this
                ->getZipcode($address->getCountry(), $addressPostcode),
            'Region' => $address->getRegionCode(),
        ];

        if ($address->getCountry() === 'CA') {
            $ShipToAddress['Province'] = $ShipToAddress['Region'];
            unset($ShipToAddress['Region']);
        }

        /*
         * Generate a mapping from tax_class to ProductCode.
         */
        $product_codes = array();
        foreach ($headerQuote['items'] as $item) {
            $product_code = substr($this->getProductCode($item['tax_class']), 0, 4);
            $product_codes[$item['tax_class']] = $product_code;
        }
        /*
         * Get carrier code and carrier method.
         */
        $carrierCode = null;
        $carrierMethod = null;
        if (strpos($address->getShippingMethod(), '_') !== false) {
            $carrierInfo = explode("_", $address->getShippingMethod());
            $carrierCode = $carrierInfo[0];
            $carrierMethod = $carrierInfo[1];
        }
        try {
            $customerVat = null;
            if ($billingAddress) {
                $validateResult = $this->validateTaxResult($billingAddress);
                if ($validateResult) {
                    $customerVat = $billingAddress->getData('vat_id');
                }
            }

            if (!$customerVat) {
                $validateResult = $this->validateTaxResult($address);
                if ($validateResult) {
                    $customerVat = $address->getData('vat_id');
                }
            }

            $requestTax = [
                'addresses' => [
                    'BillToAddress' => $BillToAddress,
                    'ShipToAddress' => $ShipToAddress,
                ],
                'carrier_code' => $carrierCode,
                'carrier_method' => $carrierMethod,
                'customer_vat' => $customerVat,
                'items' => $headerQuote['items'],
                'order_number' => $headerQuote['orderNumber'],
                'product_codes' => $product_codes,
                'total_shipping' => $total->getTotalAmount('shipping'),
                'total_subtotal' => $total->getTotalAmount('subtotal'),
            ];

            $response = $ositd->getTax($requestTax);
            if (\DSTax\TaxConnect\Model\Sales\Total\Quote\Tax::AVS_ENABLED
                AND strpos($url, 'shipping-information') !== false
                AND ($address->getCountry() == "USA"
                    || $address->getCountry() == "US")
            ) {
                $validatedAddress = $this->getValidatedAddress($address);
                //Override saved address with the OneSource Cleansed address
                $shippingAssignment->getShipping()->getAddress()->setCity(
                    $validatedAddress[0]['city']
                );
                $shippingAssignment->getShipping()->getAddress()->setStreet(
                    $validatedAddress[0]['address1']
                );
                $shippingAssignment->getShipping()->getAddress()->setPostcode(
                    $validatedAddress[0]['postalCode']
                );
            };

            if ($this->loggingEnabled) {
                $file = $this->logDirectory . 'taxConnect.log';
                $logs = str_replace(
                    $this->getConfigValue('Password'),
                    'password redacted',
                    $ositd->get('xml_request')
                );
                $logs .= "\n\n";
                $logs .= $ositd->get('xml_response');
                $logs .= "\n\n";
                file_put_contents($file, $logs, FILE_APPEND);
            }

            $this->_mmsLogger->debug("*********** Call DsTax Api ***********", [
                'action' => "DsTaxApiCall",
                'request' => $requestTax,
                'response' => $response
            ]);

        } catch (\SoapFault $exception) {
            $this->_mmsLogger->debug("*********** Call DsTax Api Error ***********", [
                'action' => "DsTaxApiCallError",
                'response_code' => $exception->getCode(),
                'response_error' => $exception->getMessage()
            ]);

            $errorFile = $this->logDirectory . 'taxConnectError.log';
            $logs = str_replace(
                $this->getConfigValue('Password'),
                'password redacted',
                $ositd->get('xml_request')
            );
            $logs .= "\n\n";
            $logs .= $exception->getMessage() . "\n";
            $logs .= "\n\n";
            file_put_contents($errorFile, $logs, FILE_APPEND);
            return $this;
        }

        if ($this->getConfigValue('TaxService') == 'Simple Tax Service') {
            if (isset($response->TaxDocuments->TaxDocument->TotalTaxAmount)) {
                $taxDocument = $response->TaxDocuments->TaxDocument;
                $totalTaxAmount = floatval($taxDocument->TotalTaxAmount);
                $total->setTotalAmount('tax', $totalTaxAmount);
                $total->setBaseTotalAmount('tax', $totalTaxAmount);
                $subtotalInclTax = $total->getSubtotal() + $total->getTotalAmount('tax');
                $baseSubtotalInclTax = $total->getBaseSubtotal() +
                    $total->getBaseTotalAmount('tax');
                $total->setSubtotalInclTax($subtotalInclTax);
                $total->setBaseSubtotalTotalInclTax($baseSubtotalInclTax);
                $total->setBaseSubtotalInclTax($baseSubtotalInclTax);
                $shippingAssignment->getShipping()->getAddress()
                    ->setBaseSubtotalTotalInclTax($baseSubtotalInclTax);
                $shippingAssignment->getShipping()->getAddress()
                    ->setTaxAmount($totalTaxAmount);
                $shippingAssignment->getShipping()->getAddress()
                    ->setBaseTaxAmount($totalTaxAmount);

                //Transfer line by line tax data to $shippingAssignment
                $items = $shippingAssignment->getItems();
                $taxLine = $taxDocument->TaxLines->TaxLine;
                for ($i=0; $i < count($items); $i++) {
                    $item = $items[$i];
                    if (is_array($taxLine)) {
                        // multiple lines
                        $line = $taxLine[$i];
                    } else {
                        // only one line, $taxLine is a stdClass
                        $line = $taxLine;
                    }
                    if (isset($line->TaxSummary->EffectiveTaxRate)) {
                        $taxRate = 100 * floatval($line->TaxSummary->EffectiveTaxRate);
                        $item->setTaxPercent(floatval($taxRate));
                    }
                    if (isset($line->TaxSummary->CalculatedTaxAmount)) {
                        $taxAmount = floatval($line->TaxSummary->CalculatedTaxAmount);
                        $item->setTaxAmount($taxAmount);
                    }
                    if ($address->getCountry() == "CA"
                        || $address->getCountry() == "CANADA"
                    ) {
                        // Break out federal and provincial taxes
                        if (!isset($authorities)) {
                            $authorities = array();
                        }
                        for ($j=0; $j < count($line->TaxDetails->TaxDetail); $j++) {
                            $taxDetail = $line->TaxDetails->TaxDetail;
                            if (is_array($taxDetail)) {
                                $taxDetail = $line->TaxDetails->TaxDetail[$j];
                            }
                            // build a key-value structure of authorities and tax amounts
                            $tax = $taxDetail->CalculatedTaxAmount;
                            if (abs($tax) > 0) {
                                $authority = $taxDetail->AuthorityType;
                                if (isset($authorities[$authority])) {
                                    $authorities[$authority] += $tax;
                                } else {
                                    $authorities[$authority] = $tax;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (array_key_exists('TotalTaxAmount', $response)) {
                $totalTaxAmount = floatval($response['TotalTaxAmount']);
                $total->setTotalAmount('tax', $totalTaxAmount);
                $total->setBaseTotalAmount('tax', $totalTaxAmount);
                $subtotalInclTax = $total->getSubtotal() + $total->getTotalAmount('tax');
                $baseSubtotalInclTax = $total->getBaseSubtotal() +
                    $total->getBaseTotalAmount('tax');
                $total->setSubtotalInclTax($subtotalInclTax);
                $total->setBaseSubtotalTotalInclTax($baseSubtotalInclTax);
                $total->setBaseSubtotalInclTax($baseSubtotalInclTax);
                $shippingAssignment->getShipping()->getAddress()
                    ->setBaseSubtotalTotalInclTax($baseSubtotalInclTax);
                $shippingAssignment->getShipping()->getAddress()
                    ->setTaxAmount($totalTaxAmount);
                $shippingAssignment->getShipping()->getAddress()
                    ->setBaseTaxAmount($totalTaxAmount);
            }

            if (array_key_exists('TaxDetails', $response)) {
                //Transfer line by line tax data to $shippingAssignment
                $items = $shippingAssignment->getItems();
                $taxLine = $response['TaxDetails'];
                for ($i=0; $i < count($items); $i++) {
                    $item = $items[$i];
                    if (is_array($taxLine)) {
                        // multiple lines
                        $line = $taxLine[$i]?? [];
                    } else {
                        // only one line, $taxLine is a stdClass
                        $line = $taxLine;
                    }
                    if (isset($line['TaxRate'])) {
                        $taxRate = floatval($line['TaxRate']);
                        $item->setTaxPercent(floatval(100 * $taxRate));
                    }
                    if (isset($line['TaxAmount'])) {
                        $taxAmount = floatval($line['TaxAmount']);
                        $item->setTaxAmount($taxAmount);
                    }
                }
            }

        }
        return $this;
    }

    /**
     * For the USA, return the first 5 digits in the zipcode.
     * For everyone else, return the entire postalcode.
     *
     * @param string $country    name of country
     * @param string $postalcode postal code
     *
     * @return string
     */
    private function getZipcode($country, $postalcode) {
        if ($country == "USA" || $country == "US") {
            if ($postalcode && strlen($postalcode) > 5) {
                return substr($postalcode, 0, 5);
            }
        }
        return $postalcode;
    }

    /**
     * For the USA, return the last 4 digits of the zipcode.
     * There may or may not be a dash in the code.
     * For everyone else, the zip extension is not used.
     *
     * @param string $country    name of country
     * @param string $postalcode postal code
     *
     * @return string
     */
    private function getZipExtension($country, $postalcode) {
        if ($country == "USA" || $country == "US") {
            if ($postalcode && (strlen($postalcode) == 9 || strlen($postalcode) == 10)) {
                return substr($postalcode, -4);
            }
        }
        return null;
    }

    /**
     * @param $billingAddress
     * @return false
     */
    private function validateTaxResult($billingAddress)
    {
        $validationResult = false;
        $vatCountries = array("AT","BE","DK","FR","DE","GR","IE","IT","NL","PT","ES","SE","GB");
        if ($billingAddress->getData('vat_id') && in_array($billingAddress->getCountryId(), $vatCountries)) {
            $validationResult = $this->checkVatNumber(
                $billingAddress->getCountryId(),
                $billingAddress->getData('vat_id')
            );
        }

        return $validationResult;
    }

    /**
     * Get tax from OneSource
     *
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote  $quote
     *
     * @return array
     */
    protected function getItemDetails($shippingAssignment, $quote, $shippingQtyCosts)
    {
        $taxClassId = 0;
        $orderNumber = $quote->reserveOrderId()->getReservedOrderId();
        $customerID = $quote->getCustomerId();
        $firstName = $quote->getCustomerFirstname();
        $lastName = $quote->getCustomerLastname();

        $lineItems = [];
        $items     = $shippingAssignment->getItems();
        $lineNumber = 0;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productLoader = $objectManager->create(\Magento\Catalog\Model\ProductFactory::class);
        if (count($items) > 0) {
            foreach ($items as $item) {
                $sku = $item->getSku();
                $product = $productLoader->create()->load($item->getProductId());
                if ($product->getData('use_acs_product_code') && $product->getData('acs_product_code') != '') {
                    $sku = $product->getData('acs_product_code');
                }
                $lineNumber = $lineNumber + 10;

                $quantity            = $item->getQty();
                $unitPrice           = (float) $item->getPrice();
                $discount            = (float) $item->getDiscountAmount();
                // $unitShipping        = (float) $shippingQtyCosts[0]['unit_price'] * $quantity;
                $unitShipping        = (float) $shippingQtyCosts[0]['unit_price'] / $quantity;
                $taxClassId          = $item->getTaxClassId();
                $productCode         = $this->getProductCode($taxClassId);
                // Don't double tax dynamically priced bundles
                if ($item->getParentItem() != null) {
                    $unitPrice = (float) 0;
                }

                array_push(
                    $lineItems,
                    [
                        'line'             => $lineNumber,
                        'sku'              => $sku,
                        'quantity'         => $quantity,
                        'unit_price'       => $unitPrice,
                        'discount'         => $discount,
                        'unit_shipping'    => $unitShipping,
                        'product_code'     => $productCode,
                        'tax_class'        => $taxClassId,
                    ]
                );
            }
        }


        $headerQuote['orderNumber'] = $orderNumber;
        $headerQuote['customerID'] = $customerID;
        if ($firstName == null and $lastName == null) {
            $headerQuote['customerName'] = "GUEST";
        } else {
            $headerQuote['customerName'] = $firstName.' '.$lastName;
        }
        $headerQuote['items'] = $lineItems;

        return $headerQuote;
    }

    /**
     * Check vat number
     *
     * @access private
     * @param string $vatNumber | $countryId
     * @return bool
     */
    private function checkVatNumber($countryId, $vatNumber){
        $patterns = array(
            'AT' => 'ATU[A-Z\d]{8}',
            'BE' => 'BE[0|1]{1}\d{9}',
            'DE' => 'DE\d{9}',
            'DK' => 'DK(\d{2} ?){3}\d{2}',
            'GR' => 'EL\d{9}',
            'ES' => 'ES[A-Z0-9]{9}',
            'FR' => 'FR[A-Z]{2}\d{9}',
            'GB' => 'GB\d{9}|GB\d{13}|GB(GD)\d{4}|GB(HA)\d{4}',
            'IE' => 'IE[A-Z\d]{8}|[A-Z\d]{9}',
            'IT' => 'IT\d{11}',
            'NL' => 'NL\d{9}B\d{3}',
            'PT' => 'PT\d{9}',
            'SE' => 'SE\d{12}',
        );
        if (0 === preg_match('/^(?:'.$patterns[$countryId].')$/', str_replace(' ', '', $vatNumber))){ return false; }
        else{ return true;}
    }
}
