<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_Admin extension
 * NOTICE OF LICENSE
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'MMS_Admin',
    __DIR__
);
