<?php

namespace MMS\FrameworkExtend\Webapi\Rest;
/**
 * Class Response
 * @package MMS\FrameworkExtend\Webapi\Rest
 */
class Response extends \Magento\Framework\Webapi\Rest\Response
{
    /**
     * @var \Magento\Framework\Webapi\ErrorProcessor
     */
    protected $_errorProcessor;

    /**
     * @var \Magento\Framework\Webapi\Rest\Response\RendererInterface
     */
    protected $_renderer;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_appState;

    /**
     * Exception stack
     * @var \Exception
     */
    protected $exceptions = [];

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * Response constructor.
     * @param \Magento\Framework\Webapi\Rest\Response\RendererFactory $rendererFactory
     * @param \Magento\Framework\Webapi\ErrorProcessor $errorProcessor
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Customer\Model\Session $customerSession
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function __construct(
        \Magento\Framework\Webapi\Rest\Response\RendererFactory $rendererFactory,
        \Magento\Framework\Webapi\ErrorProcessor $errorProcessor,
        \Magento\Framework\App\State $appState,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_renderer = $rendererFactory->get();
        $this->_errorProcessor = $errorProcessor;
        $this->_appState = $appState;
        $this->_customerSession = $customerSession;

        parent::__construct($rendererFactory, $errorProcessor, $appState);
    }

    /**
     * Generate and set HTTP response code, error messages to Response object.
     *
     * @return \Magento\Framework\Webapi\Rest\Response
     */
    protected function _renderMessages()
    {
        $responseHttpCode = null;
        $messageData = [];
        /** @var \Exception $exception */
        foreach ($this->getException() as $exception) {
            $maskedException = $this->_errorProcessor->maskException($exception);
            $messageData = [
                'message' => $maskedException->getMessage(),
            ];
            if ($maskedException->getErrors()) {
                $messageData['errors'] = [];
                foreach ($maskedException->getErrors() as $errorMessage) {
                    $errorData['message'] = $errorMessage->getRawMessage();
                    $errorData['parameters'] = $errorMessage->getParameters();
                    $messageData['errors'][] = $errorData;
                }
            }
            if ($maskedException->getCode()) {
                $messageData['code'] = $maskedException->getCode();
            }
            if ($maskedException->getDetails()) {
                $messageData['parameters'] = $maskedException->getDetails();
            }
            if ($this->_appState->getMode() == \Magento\Framework\App\State::MODE_DEVELOPER) {
                $messageData['trace'] = $exception instanceof \Magento\Framework\Webapi\Exception
                    ? $exception->getStackTrace()
                    : $exception->getTraceAsString();
            }
            $responseHttpCode = $maskedException->getHttpCode();
        }

        if ($this->_customerSession->getWebApiCustomError()) {
            $messageData['custom_error_message'] = 'test';
            $messageData['custom_error_code'] = 500;
            $this->_customerSession->setWebApiCustomError(false);
        }
        if ($this->_customerSession->getCountryAllowedError()) {
            $messageData['country_redirect_url'] = $this->_customerSession->getCustomRedirectUrl();
            $messageData['custom_error_code'] = 500;
            $this->_customerSession->setCountryAllowedError(false);
            $this->_customerSession->setCustomRedirectUrl('');
        }

        // set HTTP code of the last error, Content-Type, and all rendered error messages to body
        $this->setHttpResponseCode($responseHttpCode);
        $this->setMimeType($this->_renderer->getMimeType());
        $this->setBody($this->_renderer->render($messageData));
        return $this;
    }
}
