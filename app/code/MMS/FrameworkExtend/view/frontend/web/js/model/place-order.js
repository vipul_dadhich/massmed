/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * @api
 */
define(
    [
        'mage/storage',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Customer/js/customer-data',
        'mage/url'
    ],
    function (
        storage,
        errorProcessor,
        fullScreenLoader,
        customerData,
        urlBuilder
    ) {
        'use strict';

        return function (serviceUrl, payload, messageContainer) {
            fullScreenLoader.startLoader();

            return storage.post(
                serviceUrl, JSON.stringify(payload)
            ).fail(
                function (response) {
                    try {
                        const error = JSON.parse(response.responseText);
                        var redirectUrl = response.responseJSON.country_redirect_url;
                        if (redirectUrl != '' && redirectUrl != 'undefined' && redirectUrl != undefined){
                            window.location.replace(urlBuilder.build(redirectUrl));
                            return false;
                        }
                        window.location.replace(urlBuilder.build('apologies'));
                        return false;
                    } catch (exception) {
                    }
                    errorProcessor.process(response, messageContainer);
                }
            ).success(
                function (response) {
                    var clearData = {
                        'selectedShippingAddress': null,
                        'shippingAddressFromData': null,
                        'newCustomerShippingAddress': null,
                        'selectedShippingRate': null,
                        'selectedPaymentMethod': null,
                        'selectedBillingAddress': null,
                        'billingAddressFromData': null,
                        'newCustomerBillingAddress': null
                    };

                    if (response.responseType !== 'error') {
                        customerData.set('checkout-data', clearData);
                    }
                }
            ).always(
                function () {
                    fullScreenLoader.stopLoader();
                }
            );
        };
    }
);
