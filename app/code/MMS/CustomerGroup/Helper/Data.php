<?php
/**
 * @package     MMS_CustomerGroup
 * @author      Dinesh Kaswan
 * @copyright   Copyright (c) 2019 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */

namespace MMS\CustomerGroup\Helper;
use Magento\Framework\App\Helper\Context;
class Data extends \Magento\Framework\App\Helper\AbstractHelper{

public function __construct(Context $context)
{
    parent::__construct($context);
}
    /**
     * @return string
     */

    public function getDefaultTaxClassForCustomer(){
        return $this->scopeConfig->getValue(
            'tax/classes/default_customer_tax_class',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
    }
}
