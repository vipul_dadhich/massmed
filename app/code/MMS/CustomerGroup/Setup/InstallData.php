<?php
namespace MMS\CustomerGroup\Setup;

use Magento\Framework\Module\Setup\Migration;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Model\GroupFactory;
use MMS\CustomerGroup\Helper\Data;

class InstallData implements InstallDataInterface
{
    protected $groupFactory;

    public function __construct(GroupFactory $groupFactory, Data $helper) {
        $this->groupFactory = $groupFactory;
        $this->helper = $helper;
    }

    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        $group = $this->groupFactory->create();
        $customerGroup = $group->load('LEAD', 'customer_group_code');
        if(!$customerGroup->getId()){
            $txClass = $this->helper->getDefaultTaxClassForCustomer();
            $group
                ->setCode('LEAD')
                ->setTaxClassId($txClass) 
                ->save();
        }
        $setup->endSetup();
    }
    
}