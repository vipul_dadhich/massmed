<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_ProductAttributes extension
 * NOTICE OF LICENSE
 */
namespace MMS\ProductAttributes\Setup; 

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    /**
     * @var EavSetupFactory
     */
    private $_eavSetupFactory;

    /**
     * UpgradeData constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->_eavSetupFactory = $eavSetupFactory;
    }
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.0') < 0) {
            
            /** @var EavSetup $eavSetup */
            $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'use_acs_product_code',
                    [
                        'type' => 'int',
                        'label' => 'Use ACS Product Code for Tax',
                        'input' => 'boolean',
                        'required' => false,
                        'sort_order' => 160,
                        'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'used_in_product_listing' => true,
                        'apply_to' => '',
                        'is_used_in_grid' => false,
                        'is_visible_in_grid' => false,
                        'is_filterable_in_grid' => false,
                    ]
                );
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'acs_product_code',
                    [
                        'type' => 'varchar',
                        'label' => 'ACS Product Code',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 170,
                        'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                        'used_in_product_listing' => true,
                        'apply_to' => '',
                        'is_used_in_grid' => false,
                        'is_visible_in_grid' => false,
                        'is_filterable_in_grid' => false,
                    ]
                );
        }
        $setup->endSetup();
    }
}