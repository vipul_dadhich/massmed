<?php

namespace MMS\ExclusiveAgent\ConfigProvider;

use Magento\Checkout\Model\ConfigProviderInterface;
use MMS\ExclusiveAgent\Helper\Data;

/**
 * Class CheckoutConfigProvider
 * @package MMS\ExclusiveAgent\ConfigProvider
 */
class CheckoutConfigProvider implements ConfigProviderInterface
{
    /**
     * @var dataHelper
     */
    protected $dataHelper;

    /**
     * CheckoutConfigProvider constructor.
     * @param Data $dataHelper
     */
    public function __construct(
        Data $dataHelper
    ) {
        $this->dataHelper = $dataHelper;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return [
            'exclusiveAgent' => [
                'is_agent' => $this->dataHelper->getIsExclusiveAgent(),
                'exceptionData' => $this->dataHelper->getExceptionData()
            ]
        ];
    }


}
