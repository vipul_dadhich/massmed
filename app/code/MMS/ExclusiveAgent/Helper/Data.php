<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_ExclusiveAgent extension
 * NOTICE OF LICENSE
 */
namespace MMS\ExclusiveAgent\Helper;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 * @package MMS\ExclusiveAgent\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper{

    const JAPAN_REDIRECT_URL = 'exclusiveagent/general/jp_url';
    const SOUTH_KOREA_REDIRECT_URL = 'exclusiveagent/general/kr_url';
    /**
     * @var null
     */
    public $quote = null;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var _productloader
     */
    protected $_productloader;

    /**
     * @var scopeConfig
     */
    protected $scopeConfig;

    /**
     * CheckoutConfigProvider constructor.
     * @param CheckoutSession $checkoutSession
     * @param \Magento\Catalog\Model\ProductFactory $_productloader
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->_productloader = $_productloader;
        $this->scopeConfig = $scopeConfig;
    }


    public function getExceptionData($quote = ''){
        if (!$quote) {
            $quote = $this->getQuote();
        }
        if ($quote->hasItems()) {
            $items = $quote->getAllItems();
            foreach($items as $item){
                $product = $this->getProductById($item->getProductId());
                if ($product->getData('country_exceptions')) {
                    $data = json_decode($product->getData('country_exceptions'));
                    return $data->countryExceptions;
                }
            }
        }
        return false;
    }
    public function getIsExclusiveAgent($quote = ''){
        $isAgent = false;
        if (!$quote) {
            $quote = $this->getQuote();
        }
        if ($quote->hasItems()) {
            $items = $quote->getAllItems();
            foreach($items as $item){
                $product = $this->getProductById($item->getProductId());
                if ($product->getData('exclusive_agent_flag')) {
                    $isAgent = true;
                    break;
                }
            }
        }
        return $isAgent;
    }

    /**
     * japan url
     *
     * @param null $storeId
     * @return string
     */
    public function getJapanRedirectUrl($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::JAPAN_REDIRECT_URL,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * south korea url
     *
     * @param null $storeId
     * @return string
     */
    public function getSouthKoreaRedirectUrl($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::SOUTH_KOREA_REDIRECT_URL,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get Quote
     *
     * @return \Magento\Quote\Model\Quote|null
     */
    public function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }

        return $this->quote;
    }
    public function getProductById($id){
        return $this->_productloader->create()->load($id);
    }

    public function getOrderCountryJapanOrKoreaUrl($order){
        $billingAddress = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress();
        $exceptionData = $this->getExceptionData($order);
        foreach($exceptionData as $eData){
            if ($billingAddress->getData('country_id') == $eData->country || $shippingAddress->getData('country_id') == $eData->country) {
                return $eData->url;
            }
        }
        return '';
    }
}
