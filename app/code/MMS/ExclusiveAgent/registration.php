<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_ExclusiveAgent extension
 * NOTICE OF LICENSE
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'MMS_ExclusiveAgent',
    __DIR__
);
