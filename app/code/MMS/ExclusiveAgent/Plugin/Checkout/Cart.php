<?php

namespace MMS\ExclusiveAgent\Plugin\Checkout;

use Magento\Catalog\Model\Product;
use Magento\Checkout\Model\Cart as CartModel;
use MMS\PriceEngine\Service\PriceEngine;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\UrlInterface;
use MMS\ExclusiveAgent\Helper\Data;
use Magento\Framework\App\ActionFlag;
use Magento\Framework\App\Action\Action;

class Cart
{
    public $notAllowedCountries = ['JPN', 'KOR'];
    /**
     * @var PriceEngine
     */
    protected $_priceEngine;

    /**
     * @var ResponseFactory
     */
    protected $responseFactory;

    /**
     * @var RedirectInterface
     */
    protected $redirect;

    /**
     * @var UrlInterface
     */
    protected $_url;

    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @var ActionFlag
     */
    protected $_actionFlag;

    /**
     * Cart constructor.
     * @param PriceEngine $priceEngine
     * @param ResponseFactory $responseFactory
     * @param RedirectInterface $redirect
     * @param UrlInterface $url
     * @param Data $helper
     * @param ActionFlag $actionFlag
     */
    public function __construct(
        PriceEngine $priceEngine,
        ResponseFactory $responseFactory,
        RedirectInterface $redirect,
        UrlInterface $url,
        Data $helper,
        ActionFlag $actionFlag
    ) {
        $this->_priceEngine = $priceEngine;
        $this->responseFactory = $responseFactory;
        $this->redirect             = $redirect;
        $this->_url = $url;
        $this->_helper = $helper;
        $this->_actionFlag = $actionFlag;
    }

    /**
     * @param CartModel $subject
     * @param $productInfo
     * @param null $requestInfo
     * @return array
     */
    public function beforeAddProduct(
        CartModel $subject,
        $productInfo,
        $requestInfo = null
        ) {
            $product = null;
            if ($productInfo instanceof Product) {
                $product = $productInfo;
            }
            elseif (is_int($productInfo)  || is_string($productInfo)) {
                $product = $this->_helper->getProductById($productInfo);
            }
            if ($product) {
                $ExclusiveAgentFlag = $product->getData('exclusive_agent_flag');
                if ($ExclusiveAgentFlag) {
                    $country = $this->_priceEngine->getCountry();
                    if ($country && in_array($country, $this->notAllowedCountries)) {
                        $redirectUrl = $this->getRedirectUrl($country);
                        $this->_actionFlag->set('', Action::FLAG_NO_DISPATCH, true);
                        $this->responseFactory->create()->setRedirect($redirectUrl)->sendResponse();
                        return $this;
                    }
                }
            }
            return [$productInfo, $requestInfo];
        }

    /**
     * @param $country
     * @return string
     */
    public function getRedirectUrl($country): ?string
    {
        if ($country == 'JPN') {
            return $this->_url->getUrl($this->_helper->getJapanRedirectUrl());
        } else {
            return $this->_url->getUrl($this->_helper->getSouthKoreaRedirectUrl());
        }
    }
}
