require([
   'jquery',
   'mage/url',
   'Magento_Checkout/js/model/quote',
   'Magento_Ui/js/lib/view/utils/async'
 ], function ($, urlBuilder, quote) {
 'use strict';
 var billingCountryIdSelector = '.billing-address-form select[name=country_id]';
 var shippingCountryIdSelector = '.form-shipping-address select[name=country_id]';
 var shipHereButton = '.action-select-shipping-item, .action-save-address';
 var agentJs;
 let isAgent = window.checkoutConfig.exclusiveAgent.is_agent;
 agentJs = {
		redirectForAgent: function (country) {
			var redirectLink = '';
			if (isAgent) {
			    for(var i = 0; i < window.checkoutConfig.exclusiveAgent.exceptionData.length; i++){
			        if (window.checkoutConfig.exclusiveAgent.exceptionData[i].country == country) {
                        redirectLink = window.checkoutConfig.exclusiveAgent.exceptionData[i].url;
                    }
                }
				if (redirectLink) {
					window.location.href = redirectLink;
				}
			}
		}
	};
 	$.async(billingCountryIdSelector, function(element) {
		$(element).on('change', function () {
			agentJs.redirectForAgent($(element).val());
		});
	});
 	$.async(billingCountryIdSelector, function(element) {
		agentJs.redirectForAgent($(element).val());
	});
 	$.async(shipHereButton, function(element) {
		$(element).on('click', function () {
			var address = quote.shippingAddress();
			agentJs.redirectForAgent(address.countryId);
		});
	});
 	$.async(shipHereButton, function(element) {
	 	var address = quote.shippingAddress();
		agentJs.redirectForAgent(address.countryId);
	});

 	$.async(shippingCountryIdSelector, function(element) {
		$(element).on('change', function () {
			agentJs.redirectForAgent($(element).val());
		});
	});
});
