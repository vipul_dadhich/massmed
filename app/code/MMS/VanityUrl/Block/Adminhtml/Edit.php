<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MMS\VanityUrl\Block\Adminhtml;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Helper\Data;
use Magento\Framework\App\RequestInterface;
use Magento\UrlRewrite\Model\UrlRewriteFactory;

/**
 * Block for URL rewrites edit page
 */
class Edit extends \Magento\UrlRewrite\Block\Edit
{
    /**
     * @var \Magento\UrlRewrite\Block\Selector
     */
    private $_selectorBlock;

    /**
     * Part for building some blocks names
     *
     * @var string
     */
    protected $_controller = 'url_rewrite';

    /**
     * Generated buttons html cache
     *
     * @var string
     */
    protected $_buttonsHtml;

    /**
     * Adminhtml data
     *
     * @var Data
     */
    protected $_adminhtmlData = null;

    /**
     * @var UrlRewriteFactory
     */
    protected $_rewriteFactory;

    /**
     * @param Context $context
     * @param UrlRewriteFactory $rewriteFactory
     * @param Data $adminhtmlData
     * @param Data $adminhtmlData
     * @param RequestInterface $request
     * @param array $data
     */
    public function __construct(
        Context $context,
        UrlRewriteFactory $rewriteFactory,
        Data $adminhtmlData,
        RequestInterface $request,
        array $data = []
    ) {
        $this->_rewriteFactory = $rewriteFactory;
        $this->_adminhtmlData = $adminhtmlData;
        $this->request = $request;
        parent::__construct($context, $rewriteFactory, $adminhtmlData, $data);
    }

    /**
     * Add back button
     *
     * @return void
     */
    protected function _addBackButton()
    {
        $backUrl = $this->_adminhtmlData->getUrl('adminhtml/*/');
        if ($this->request->getParam('vanity')) {
            $backUrl = $this->_adminhtmlData->getUrl('mms_vanityurl/vanityurl');
        }
        $this->addButton(
            'back',
            [
                'label' => __('Back'),
                'onclick' => 'setLocation(\'' . $backUrl . '\')',
                'class' => 'back',
                'level' => -1
            ]
        );
    }

    /**
     * Get selector block
     *
     * @return \Magento\UrlRewrite\Block\Selector
     */
    private function _getSelectorBlock()
    {
        if (!$this->_selectorBlock) {
            $this->_selectorBlock = $this->getLayout()->createBlock(\Magento\UrlRewrite\Block\Selector::class);
        }
        return $this->_selectorBlock;
    }
}
