<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MMS\VanityUrl\Block\Adminhtml\UrlRewrite\Edit;

/**
 * URL rewrites edit form
 *
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class Form extends \Magento\UrlRewrite\Block\Edit\Form
{
    /**
     * @return $this|Form
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $this->_formValues['is_vanity'] = $this->_getModel()->getIsVanity();
        $form = $this->getForm();
        $fieldset = $form->getElement('base_fieldset');
        $fieldset->addField(
            'is_vanity',
            'select',
            [
                'label' => __('Is Vanity URL'),
                'title' => __('Is Vanity URL'),
                'name' => 'is_vanity',
                'options' => [0 => __('No'), 1 => __('Yes')],
                'value' => $this->_formValues['is_vanity']
            ]
        );
        return $this;
    }

    /**
     * Get stores list restricted by entity stores.
     * Stores should be filtered only if custom entity is specified.
     * If we use custom rewrite, all stores are accepted.
     *
     * @param array $entityStores
     * @return array
     */
    private function _getStoresListRestrictedByEntityStores(array $entityStores)
    {
        $stores = $this->_getAllStores();
        if ($this->_requireStoresFilter) {
            foreach ($stores as $i => $store) {
                if (isset($store['value']) && $store['value']) {
                    $found = false;
                    foreach ($store['value'] as $k => $v) {
                        echo $v['value'];
                        if (isset($v['value']) && in_array($v['value'], $entityStores)) {
                            $found = true;
                        } else {
                            unset($stores[$i]['value'][$k]);
                        }
                    }
                    if (!$found) {
                        unset($stores[$i]);
                    }
                }
            }
        }
        return $stores;
    }
}
