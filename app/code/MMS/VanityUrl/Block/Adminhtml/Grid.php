<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 */
namespace MMS\VanityUrl\Block\Adminhtml;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\UrlRewrite\Model\UrlRewriteFactory
     */
    protected $_vanityFactory;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data            $backendHelper
     * @param \Magento\UrlRewrite\Model\UrlRewriteFactory $vanityFactory
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $vanityFactory,
        array $data = []
    ) {
        $this->_vanityFactory = $vanityFactory;
        parent::__construct($context, $backendHelper, $data);
    }
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setUrlRewriteId('gridGrid');
        $this->setDefaultSort('url_rewrite_id');
        $this->setDefaultDir('DESC');
    }
    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_vanityFactory->create()->getCollection()
        ->addFieldToFilter('is_vanity', 1);
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }
    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'url_rewrite_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'url_rewrite_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'request_path',
            [
                'header' => __('Request Path'),
                'index' => 'request_path',
            ]
        );
        $this->addColumn(
            'target_path',
            [
                'header' => __('Target Path'),
                'index' => 'target_path',
            ]
        );
        $this->addColumn(
            'redirect_type',
            [
                'header' => __('Redirect Type'),
                'index' => 'redirect_type',
            ]
        );

        $this->addColumn(
            'edit',
            [
            'header' => __('Edit'),
            'type' => 'action',
            'getter' => 'getId',
            'actions' => [
            [
            'caption' => __('Edit'),
            'url' => [
            'base' => 'adminhtml/url_rewrite/edit',
                'params' => ['vanity' => 1]
            ],
            'field' => 'id',
            ],
            ],
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'header_css_class' => 'col-action',
            'column_css_class' => 'col-action',
            ]
            );


        return parent::_prepareColumns();
    }
    public function getSearchButtonHtml()
    {
        return '';
    }
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        return $this;
    }
    /**
     * @return string
     */
    public function getGridUrl()
    {
        return '';
    }
    /**
     * @return string
     */
    public function getRowUrl($row)
    {
        return '';
    }
}
