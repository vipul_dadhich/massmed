<?php

namespace MMS\VanityUrl\Plugin;

use Magento\Backend\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\UrlInterface;

class DeleteUrlPlugin
{
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var UrlInterface
     */
    protected $_url;

    /**
     * @var Http
     */
    protected $_response;

    /**
     * Initialize dependencies.
     *
     * @param RequestInterface $request
     * @param UrlInterface $url
     * @param Http $response
     */
    public function __construct(
         RequestInterface $request,
         UrlInterface $url,
         Http $response,
         Session $session
    ) {
        $this->_request = $request;
        $this->_response = $response;
        $this->_url = $url;
        $this->_session = $session;
    }

    /**
     * @param \MMS\Promotions\Controller\Adminhtml\Promotion\Save $subject
     * @param array $result
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterExecute(\Magento\UrlRewrite\Controller\Adminhtml\Url\Rewrite\Delete $subject, $result)
    {
        $vanity = $this->_session->getVanity();
        $this->_session->setVanity(null);
        if ($vanity) {
            $vanityPageUrl = $this->_url->getUrl('mms_vanityurl/vanityurl');
            $this->_response->setRedirect($vanityPageUrl)->sendResponse();
        }
        return $result;
    }



}
