<?php

namespace MMS\VanityUrl\Plugin;

use Magento\Backend\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\UrlInterface;
use Magento\UrlRewrite\Model\UrlRewrite;

class SaveUrlPlugin extends \Magento\UrlRewrite\Controller\Adminhtml\Url\Rewrite\Save
{
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var UrlInterface
     */
    protected $_url;

    /**
     * @var UrlRewrite
     */
    protected $_urlRewrite;

    /**
     * @var Http
     */
    protected $_response;

    /**
     * Initialize dependencies.
     *
     * @param RequestInterface $request
     * @param UrlInterface $url
     * @param UrlRewrite $urlRewrite
     * @param Http $response
     */
    public function __construct(
         RequestInterface $request,
         UrlInterface $url,
         UrlRewrite $urlRewrite,
         Http $response,
         Session $session
    ) {
        $this->_request = $request;
        $this->_response = $response;
        $this->_url = $url;
        $this->_urlRewrite = $urlRewrite;
        $this->_session = $session;
    }

    /**
     * @param \MMS\Promotions\Controller\Adminhtml\Promotion\Save $subject
     * @param array $result
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterExecute(\Magento\UrlRewrite\Controller\Adminhtml\Url\Rewrite\Save $subject, $result)
    {
        $requestPath = $this->_request->getParam('request_path');
        $isVanity = $this->_request->getParam('is_vanity');
        $urlRewriteExistingModel = $this->_urlRewrite->getCollection()->addFieldToFilter('request_path', $requestPath)->getFirstItem();
        $urlRewriteExistingModel->setIsVanity($isVanity)->save();
        $vanity = $this->_session->getVanity();
        $this->_session->setVanity(null);
        if ($vanity) {
            $vanityPageUrl = $this->_url->getUrl('mms_vanityurl/vanityurl');
            $this->_response->setRedirect($vanityPageUrl)->sendResponse();
        }
        return $result;
    }
}
