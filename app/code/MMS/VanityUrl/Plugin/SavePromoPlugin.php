<?php

namespace MMS\VanityUrl\Plugin;

use Magento\Store\Model\StoreResolver;
use Magento\UrlRewrite\Model\UrlRewrite;
use Magento\UrlRewrite\Model\UrlRewriteFactory;

class SavePromoPlugin
{
    /**
     * @var UrlRewriteFactory
     */
    protected $_urlRewriteFactory;

    /**
     * @var UrlRewrite
     */
    protected $_urlRewrite;

    /**
     * @var StoreResolver
     */
    protected $storeResolver;

    /**
     * Initialize dependencies.
     *
     * @param UrlRewriteFactory $urlRewriteFactory
     * @param UrlRewrite $urlRewrite
     * @param StoreResolver $storeResolver
     */
    public function __construct(
        UrlRewriteFactory $urlRewriteFactory,
        UrlRewrite $urlRewrite,
        StoreResolver $storeResolver
    ) {
        $this->_urlRewriteFactory = $urlRewriteFactory;
        $this->_urlRewrite = $urlRewrite;
        $this->storeResolver = $storeResolver;
    }

    /**
     * @param \MMS\Promotions\Controller\Adminhtml\Promotion\Save $subject
     * @param array $result
     * @param array $data
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterExecuteSave(\MMS\Promotions\Controller\Adminhtml\Promotion\Save $subject, $result, $data)
    {
        if ($result) {
            if (isset($data['binded_template']) && isset($data['vanity_data'])) {
                    $this->importVanityUrl($data);
            }
        }
        return $result;
    }

    /**
     * @param $data
     * @return string
     * @throws \Exception
     */
    public function importVanityUrl($data){
        $targetUrl = $this->getTargetUrl($data);
        if ($targetUrl) {
            $vanityData = (array)json_decode($data['vanity_data']);
            if (isset($vanityData[$data['promo_code']])) {
                $vanityUrl = $vanityData[$data['promo_code']];
                $urlRewriteExistingModel = $this->_urlRewrite->getCollection()->addFieldToFilter('request_path', $vanityUrl)->getFirstItem();
                if ($urlRewriteExistingModel && $urlRewriteExistingModel->getUrlRewriteId()) {
                    $urlRewriteExistingModel->setTargetPath($targetUrl);
                    $urlRewriteExistingModel->setIsVanity(1);
                    $urlRewriteExistingModel->save();
                } else {
                    $storeId = $this->storeResolver->getCurrentStoreId();
                    $urlRewriteModel = $this->_urlRewriteFactory->create();
                    $urlRewriteModel->setStoreId($storeId);
                    $urlRewriteModel->setIsSystem(0);
                    $urlRewriteModel->setIdPath(rand(1, 100000));
                    $urlRewriteModel->setTargetPath($targetUrl);
                    $urlRewriteModel->setIsVanity(1);
                    $urlRewriteModel->setRequestPath($vanityUrl);
                    $urlRewriteModel->setRedirectType('301');
                    $urlRewriteModel->save();
                }
            }
        }
        return '';
    }

    /**
     * @param $data
     * @return string
     */
    public function getTargetUrl($data){
        if ($data['binded_template']) {
            return $data['binded_template'].'?promo='.$data['promo_code'];
        }
        return '';
    }

}
