<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_VanityUrl extension
 * NOTICE OF LICENSE
 */
namespace MMS\VanityUrl\Setup; 
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $installer->getConnection()->addColumn(
            $installer->getTable('url_rewrite'),
            'is_vanity',
            [
                'type' => 'boolean',
                'nullable' => true,
                'comment' => 'Is Vanity Url',
            ]
        );
        $setup->endSetup();
    }
}