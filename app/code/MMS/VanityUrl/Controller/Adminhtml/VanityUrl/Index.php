<?php
namespace MMS\VanityUrl\Controller\Adminhtml\VanityUrl;

use MMS\VanityUrl\Controller\Adminhtml\VanityUrl as VanityUrlController;

/**
 * Class Index
 * @package MMS\VanityUrl\Controller\Adminhtml\VanityUrl
 */
class Index extends VanityUrlController
{
    /**
     * Promotions list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('MMS_VanityUrl::vanityurl');
        $resultPage->getConfig()->getTitle()->prepend(__('Vanity Urls'));
        $resultPage->addBreadcrumb(__('Vanity Url'), __('Vanity Urls'));
        $resultPage->addBreadcrumb(__('Vanity Url'), __('Vanity Urls'));
        return $resultPage;
    }
}
