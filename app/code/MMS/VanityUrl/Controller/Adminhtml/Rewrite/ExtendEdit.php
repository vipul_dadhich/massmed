<?php
namespace MMS\VanityUrl\Controller\Adminhtml\Rewrite;

use Magento\Backend\Model\Session;

/**
 * Class VanityUrl
 * @package MMS\VanityUrl\Controller\Adminhtml
 */
class ExtendEdit extends \Magento\UrlRewrite\Controller\Adminhtml\Url\Rewrite\Edit
{
    protected function _isAllowed()
    {
        $vanity = $this->getRequest()->getparam('vanity');
        $this->_session->setVanity($vanity);
        if ($vanity) {
            return $this->_authorization->isAllowed('MMS_VanityUrl::vanityurl_vanityurl');
        }
        return parent::_isAllowed();
    }
}
