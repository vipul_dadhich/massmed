<?php
namespace MMS\VanityUrl\Controller\Adminhtml\Rewrite;

/**
 * Class VanityUrl
 * @package MMS\VanityUrl\Controller\Adminhtml
 */
class ExtendSave extends \Magento\UrlRewrite\Controller\Adminhtml\Url\Rewrite\Save
{
    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        $vanity = $this->_getSession()->getVanity();
        if ($vanity) {
            return $this->_authorization->isAllowed('MMS_VanityUrl::vanityurl_save');
        }
        return parent::_isAllowed();
    }
}
