<?php
namespace MMS\VanityUrl\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class VanityUrl
 * @package MMS\VanityUrl\Controller\Adminhtml
 */
abstract class VanityUrl extends Action
{
    

    /**
     * constructor
     *
     * @param Context $context
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory   = $resultPageFactory;
    }

    
}
