<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Model\Source;

use MMS\Logger\Model\Config as Config;

/**
 * Class Generic
 * @package MMS\Logger\Model\Source
 */
class Generic implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var Config
     */
    protected $loggerConfig;

    /**
     * Carrier code
     *
     * @var string
     */
    protected $code = '';

    /**
     * @param Config $loggerConfig
     */
    public function __construct(Config $loggerConfig)
    {
        $this->loggerConfig = $loggerConfig;
    }

    /**
     * Returns array to be used in multiselect on back-end
     *
     * @return array
     */
    public function toOptionArray()
    {
        $configData = $this->loggerConfig->getCode($this->code);
        $arr = [];
        foreach ($configData as $code => $title) {
            $arr[] = ['value' => $code, 'label' => $title];
        }
        return $arr;
    }
}
