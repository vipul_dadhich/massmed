<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Model\Source;

use MMS\Logger\Model\Source\Generic as Generic;

/**
 * Class LogLevel
 * @package MMS\Logger\Model\Source
 */
class LogLevel extends Generic
{
    /**
     * Carrier code
     *
     * @var string
     */
    protected $code = 'logLevel';
}
