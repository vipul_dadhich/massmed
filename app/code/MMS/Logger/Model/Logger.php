<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Model;

use MMS\Logger\LoggerInterface;
use MMS\Logger\Helper\Data as DataHelper;

/**
 * MMS Logger implementation
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Logger extends \Monolog\Logger implements LoggerInterface
{
    const DEBUG_KEYS_MASK = '****';

    /**
     * @var  DataHelper
     */
    private $_dataHelper;

    /**
     * Logger constructor.
     * @param DataHelper $dataHelper
     * @param $name
     * @param array $handlers
     * @param array $processors
     */
    public function __construct(
        DataHelper $dataHelper,
        array $handlers = [],
        array $processors = []
    ) {
        $this->_dataHelper = $dataHelper;
        parent::__construct('default', $handlers, $processors);
    }

    /**
     * Adds a log record at the DEBUG level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param  string  $message The log message
     * @param  array   $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function debug($message, array $context = [])
    {
        if (!$this->_dataHelper->isEnable()) {
            return false;
        }

        return $this->logMessage($message, self::SEVERITY_NOTICE, $context);
    }

    /**
     * Adds a log record at the INFO level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param  string  $message The log message
     * @param  array   $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function info($message, array $context = [])
    {
        if (!$this->_dataHelper->isEnable()) {
            return false;
        }

        return $this->logMessage($message, self::SEVERITY_MINOR, $context);
    }

    /**
     * Adds a log record at the WARNING level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param  string  $message The log message
     * @param  array   $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function warning($message, array $context = [])
    {
        if (!$this->_dataHelper->isEnable()) {
            return false;
        }

        return $this->logMessage($message, self::SEVERITY_MAJOR, $context);
    }

    /**
     * Adds a log record at the CRITICAL level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param  string  $message The log message
     * @param  array   $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function critical($message, array $context = [])
    {
        if (!$this->_dataHelper->isEnable()) {
            return false;
        }
        return $this->logMessage($message, self::SEVERITY_CRITICAL, $context);
    }

    /**
     * @param $message
     * @param array $context
     * @param $severity
     * @return bool
     */
    protected function logMessage($message, $severity, array $context = [])
    {
        $systemLogLevel = $this->_dataHelper->getSystemLevel();
        if ($systemLogLevel > 0 && $systemLogLevel >= $severity) {
            $message = is_string($message) ? $message : var_export($message, true);
            switch ($severity) {
                case self::SEVERITY_NOTICE:
                    parent::debug($message, $context);
                    break;
                case self::SEVERITY_MINOR:
                    parent::notice($message, $context);
                    break;
                case self::SEVERITY_MAJOR:
                    parent::warning($message, $context);
                    break;
                case self::SEVERITY_CRITICAL:
                    parent::critical($message, $context);
                    break;
            }
        }

        return true;
    }

    /**
     * Recursive filter data by private conventions
     *
     * @param array $debugData
     * @param array $debugReplacePrivateDataKeys
     * @return array
     */
    public function filterDebugData(array $debugData, array $debugReplacePrivateDataKeys)
    {
        $debugReplacePrivateDataKeys = array_map('strtolower', $debugReplacePrivateDataKeys);

        foreach (array_keys($debugData) as $key) {
            if (in_array(strtolower($key), $debugReplacePrivateDataKeys)) {
                $debugData[$key] = self::DEBUG_KEYS_MASK;
            } elseif (is_array($debugData[$key])) {
                $debugData[$key] = $this->filterDebugData($debugData[$key], $debugReplacePrivateDataKeys);
            }
        }
        return $debugData;
    }
}
