<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Model\Logger;

use Monolog\Logger;

/**
 * Class Critical
 * @package MMS\Logger\Model\Logger
 */
class Critical extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::CRITICAL;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/MMS-Magento.log';
}
