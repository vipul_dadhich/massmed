<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use MMS\Logger\Helper\Data as DataHelper;
use MMS\Logger\LoggerInterface;

/**
 * Class CheckoutQuoteInit
 * @package MMS\Logger\Observer
 */
class CheckoutQuoteInit implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @var DataHelper
     */
    protected $_dataHelper;

    /**
     * CheckoutQuoteInit constructor.
     * @param LoggerInterface $logger
     * @param DataHelper $dataHelper
     */
    public function __construct(
        LoggerInterface $logger,
        DataHelper $dataHelper
    ) {
        $this->_logger = $logger;
        $this->_dataHelper = $dataHelper;
    }

    /**
     * @param Observer $observer
     * @return false|void
     */
    public function execute(Observer $observer)
    {
        if (!$this->_dataHelper->isEnable()) {
            return false;
        }

        /** @var Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        if ($quote && $quote->getId()) {
            $quoteItems = [];
            foreach ($quote->getAllVisibleItems() as $item) {
                $quoteItems[] = [
                    [
                        "sku" => $item->getSku(),
                        "qty" => (int) $item->getQty(),
                    ]
                ];
            }

            $this->_logger->debug("*********** Init Checkout Quote ***********");
            $this->_logger->debug([
                "action" => "checkoutQuoteInit",
                "quote_id" => $quote->getId(),
                "paymentMethod" => $quote->getPayment()->getMethod(),
                "quote_items" => $quoteItems,
                "total" => $quote->getGrandTotal(),
                "customer_type" => $this->_dataHelper->getCustomerType(),
                "customer_email" => $this->_dataHelper->getCustomerEmail()
            ]);
        }
    }
}
