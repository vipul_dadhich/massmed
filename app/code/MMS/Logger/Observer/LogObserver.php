<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use MMS\Logger\Helper\Data as DataHelper;
use MMS\Logger\LoggerInterface;

/**
 * Class LogObserver
 * @package MMS\Logger\Observer
 */
class LogObserver implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @var DataHelper
     */
    protected $_dataHelper;

    /**
     * LogObserver constructor.
     * @param LoggerInterface $logger
     * @param DataHelper $dataHelper
     */
    public function __construct(
        LoggerInterface $logger,
        DataHelper $dataHelper
    ) {
        $this->_logger = $logger;
        $this->_dataHelper = $dataHelper;
    }

    /**
     * @param Observer $observer
     * @return false
     */
    public function execute(Observer $observer)
    {
        if (!$this->_dataHelper->isEnable()) {
            return false;
        }

        $severity = $observer->getEvent()->getSeverity();
        $action = $observer->getEvent()->getAction();
        $message = $observer->getEvent()->getMessage();
        $context = $observer->getEvent()->getContext();
        if (!is_array($context)) {
            $context = [$context];
        }

        $systemLogLevel = $this->_dataHelper->getSystemLevel();

        if ($systemLogLevel > 0 && $systemLogLevel >= $severity) {
            switch ($severity) {
                case LoggerInterface::SEVERITY_NOTICE:
                    if (!is_null($message) && $message != '') {
                        if ($action) {
                            $this->_logger->debug($message, []);
                        }
                        $this->_logger->debug($message, $context);
                    }
                    break;
                case LoggerInterface::SEVERITY_MINOR:
                    if (!is_null($message) && $message != '') {
                        if ($action) {
                            $this->_logger->info($message, []);
                        }
                        $this->_logger->info($message, $context);
                    }
                    break;
                case LoggerInterface::SEVERITY_MAJOR:
                    if (!is_null($message) && $message != '') {
                        if ($action) {
                            $this->_logger->warning($message, []);
                        }
                        $this->_logger->warning($message, $context);
                    }
                    break;
                case LoggerInterface::SEVERITY_CRITICAL:
                    if (!is_null($message) && $message != '') {
                        if ($action) {
                            $this->_logger->critical($message, []);
                        }
                        $this->_logger->critical($message, $context);
                    }
                    break;
            }
        }
    }
}
