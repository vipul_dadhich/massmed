<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Observer;

use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote\Item;
use MMS\Logger\Helper\Data as DataHelper;
use MMS\Logger\LoggerInterface;

class CheckoutCartProductAddAfter implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @var DataHelper
     */
    protected $_dataHelper;

    /**
     * CheckoutCartProductAddAfter constructor.
     * @param LoggerInterface $logger
     * @param DataHelper $dataHelper
     */
    public function __construct(
        LoggerInterface $logger,
        DataHelper $dataHelper
    ) {
        $this->_logger = $logger;
        $this->_dataHelper = $dataHelper;
    }

    /**
     * @param Observer $observer
     * @return false|void
     */
    public function execute(Observer $observer)
    {
        if (!$this->_dataHelper->isEnable()) {
            return false;
        }

        /** @var Product $product */
        $product = $observer->getEvent()->getProduct();

        /** @var Item $quoteItem */
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $quoteItem = $quoteItem->getParentItem() ? $quoteItem->getParentItem() : $quoteItem;

        $this->_logger->debug("*********** After Add Product To Checkout Cart ***********");
        $this->_logger->debug([
            "action" => "checkoutCartProductAddAfter",
            "quote_id" => $quoteItem->getQuoteId(),
            "quote_item" => $quoteItem->getItemId(),
            "quote_item_id" => $quoteItem->getId(),
            "quote_item_price" => $quoteItem->getPrice(),
            "quote_item_sku" => $product->getSku(),
            "product_id" => $product->getId(),
            "product_option" => $quoteItem->getProductOption(),
            "customer_type" => $this->_dataHelper->getCustomerType(),
            "customer_email" => $this->_dataHelper->getCustomerEmail()
        ]);
    }
}
