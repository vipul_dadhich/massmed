<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger;

/**
 * Interface LoggerInterface
 * @package MMS\Logger
 */
interface LoggerInterface
{
    const SEVERITY_CRITICAL = 1;
    const SEVERITY_MAJOR    = 2;
    const SEVERITY_MINOR    = 3;
    const SEVERITY_NOTICE   = 4;
    const SEVERITY_NONE     = -1;

    /**
     * @param $message
     * @param array $context
     * @return mixed
     */
    public function debug($message, array $context = []);

    /**
     * @param $message
     * @param array $context
     * @return mixed
     */
    public function info($message, array $context = []);

    /**
     * @param $message
     * @param array $context
     * @return mixed
     */
    public function warning($message, array $context = []);

    /**
     * @param $message
     * @param array $context
     * @return mixed
     */
    public function critical($message, array $context = []);

    /**
     * Recursive filter data by private conventions
     *
     * @param array $debugData
     * @param array $debugReplacePrivateDataKeys
     * @return mixed
     */
    public function filterDebugData(array $debugData, array $debugReplacePrivateDataKeys);
}
