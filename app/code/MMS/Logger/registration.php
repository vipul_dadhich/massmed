<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'MMS_Logger',
    __DIR__
);
