<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */

namespace MMS\Logger\Helper;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 * @package MMS\Logger\Helper
 */
class Data extends AbstractHelper
{
    const XML_PATH_ACTIVE = 'mms/logger/active';
    const XML_PATH_SYSTEM_LEVEL = 'mms/logger/system_level';

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * Data constructor.
     * @param Context $context
     * @param CustomerSession $customerSession
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession
    ) {
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isEnable($store = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ACTIVE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getSystemLevel($store = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SYSTEM_LEVEL,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @return CustomerSession
     */
    public function getCustomerSession()
    {
        return $this->_customerSession;
    }

    /**
     * @return bool
     */
    public function isCustomerLogin()
    {
        return $this->_customerSession->isLoggedIn();
    }

    /**
     * @return string
     */
    public function getCustomerType()
    {
        if ($this->isCustomerLogin()) {
            return 'Registered';
        }
        return 'Guest';
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        if ($this->isCustomerLogin()) {
            return $this->_customerSession->getCustomer()->getEmail();
        }

        return "";
    }
}
