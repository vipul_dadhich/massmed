<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Plugin\PaybillCheckout;

use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use MMS\Logger\Helper\Data as DataHelper;
use MMS\Logger\LoggerInterface;

/**
 * Class Index
 * @package MMS\Logger\Plugin\PaybillCheckout
 */
class Index
{
    /**
     * @var Session
     */
    protected $_session;
    /**
     * @var ProductRepository
     */
    protected $_productRepository;
    /**
     * @var RequestInterface
     */
    protected $_request;
    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @var DataHelper
     */
    protected $_dataHelper;

    /**
     * Index constructor.
     * @param Session $session
     * @param ProductRepository $productRepository
     * @param RequestInterface $request
     * @param LoggerInterface $logger
     * @param DataHelper $dataHelper
     */
    public function __construct(
        Session $session,
        ProductRepository $productRepository,
        RequestInterface $request,
        LoggerInterface $logger,
        DataHelper $dataHelper
    ) {
        $this->_session = $session;
        $this->_productRepository = $productRepository;
        $this->_request = $request;
        $this->_logger = $logger;
        $this->_dataHelper = $dataHelper;
    }

    /**
     * @throws LocalizedException
     * @throws  NoSuchEntityException
     */
    public function beforeExecute()
    {
        $items = $this->_session->getQuote()->getAllVisibleItems();
        $qId = (int) $this->_session->getQuoteId();

        $response = [];
        foreach ($items as $item) {
            $response[] = [
                "sku" => $item->getSku(),
                "qty" => (int) $item->getQty(),
                "price" => (int) $item->getPrice()
            ];
        }

        $this->_logger->debug("*********** Visit On Paybill Checkout Page ***********");
        $this->_logger->debug([
            "action" => "paybillCheckoutVisit",
            "quote-id" => $qId,
            "cart_items" => $response,
            "customer_type" => $this->_dataHelper->getCustomerType(),
            "customer_email" => $this->_dataHelper->getCustomerEmail()
        ]);
    }
}
