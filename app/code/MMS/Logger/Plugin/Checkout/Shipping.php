<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Plugin\Checkout;

use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\ShippingInformationManagement;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use MMS\Logger\LoggerInterface;

/**
 * Class Shipping
 * @package MMS\Logger\Plugin\Checkout
 */
class Shipping
{
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var ProductRepository
     */
    protected $_productRepository;
    /**
     * @var Session
     */
    protected $_session;
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * Shipping constructor.
     * @param RequestInterface $request
     * @param ProductRepository $productRepository
     * @param Session $session
     * @param LoggerInterface $logger
     */
    public function __construct(
        RequestInterface $request,
        ProductRepository $productRepository,
        Session $session,
        LoggerInterface $logger
    ) {
        $this->request = $request;
        $this->_productRepository = $productRepository;
        $this->_session = $session;
        $this->_logger = $logger;
    }

    /**
     * @param ShippingInformationManagement $shipping
     * @param $result
     * @return mixed
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function afterSaveAddressInformation(
        ShippingInformationManagement $shipping,
        $result
    ) {
        $address = $this->_session->getQuote()->getShippingAddress();
        $qId = (int)$this->_session->getQuoteId();

        $this->_logger->debug("*********** Shipping Checkout ***********");
        $this->_logger->debug([
            "action" => "checkoutShipping",
            "quote-id" => $qId,
            "address" => [
                "id" => $address->getCustomerId(),
                "email" => $address->getEmail(),
                "state" => $address->getRegionCode(),
                "zip" => $address->getPostcode()
            ]
        ]);

        return $result;
    }

}
