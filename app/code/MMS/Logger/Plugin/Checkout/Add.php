<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Plugin\Checkout;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use MMS\Logger\Helper\Data as DataHelper;
use MMS\Logger\LoggerInterface;

/**
 * Class Add
 * @package MMS\Logger\Plugin\Checkout
 */
class Add
{
    /**
     * @var ProductRepository
     */
    protected $_productRepository;
    /**
     * @var RequestInterface
     */
    protected $_request;
    /**
     * @var LoggerInterface
     */
    protected $_logger;
    /**
     * @var DataHelper
     */
    protected $_dataHelper;

    /**
     * Add constructor.
     * @param RequestInterface $request
     * @param ProductRepository $productRepository
     * @param LoggerInterface $logger
     * @param DataHelper $dataHelper
     */
    public function __construct(
        RequestInterface $request,
        ProductRepository $productRepository,
        LoggerInterface $logger,
        DataHelper $dataHelper
    ) {
        $this->_productRepository = $productRepository;
        $this->_request = $request;
        $this->_logger = $logger;
        $this->_dataHelper = $dataHelper;
    }

    /**
     * @param \Magento\Checkout\Controller\Cart\Add $subject
     * @param $result
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function afterExecute(
        \Magento\Checkout\Controller\Cart\Add $subject,
        $result
    )
    {
        $params = $this->_request->getPostValue();
        $id = $sku = $qty = -1;
        if (!empty($params['product'])) {
            $product = $this->_productRepository->getById($params['product']);
            $id = (int) $product->getId();
            $sku = $product->getSku();
            $qty = (int) isset($params['qty']) ? $params['qty'] : 1;
        }

        $this->_logger->debug("*********** Add To Cart Execute ***********");
        $this->_logger->debug([
            "action" => "AddToCart",
            "productId" => $id,
            "sku" => $sku,
            "qty" => $qty,
            "post_request" => $params,
            "customer_type" => $this->_dataHelper->getCustomerType(),
            "customer_email" => $this->_dataHelper->getCustomerEmail()
        ]);

        return $result;
    }

}
