<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Plugin\Checkout;

use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Api\GuestPaymentInformationManagementInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\PaymentInterface;
use MMS\Logger\Helper\Data as DataHelper;
use MMS\Logger\LoggerInterface;

/**
 * Class GuestPayment
 * @package MMS\Logger\Plugin\Checkout
 */
class GuestPayment
{
    /**
     * @var Session
     */
    protected $_session;
    /**
     * @var ProductRepository
     */
    protected $_productRepository;
    /**
     * @var RequestInterface
     */
    protected $_request;
    /**
     * @var \MMS\Logger\Model\Logger
     */
    protected $_logger;
    /**
     * @var DataHelper
     */
    protected $_dataHelper;

    /**
     * GuestPayment constructor.
     * @param Session $session
     * @param ProductRepository $productRepository
     * @param RequestInterface $request
     * @param LoggerInterface $logger
     * @param DataHelper $dataHelper
     */
    public function __construct(
        Session $session,
        ProductRepository $productRepository,
        RequestInterface $request,
        LoggerInterface $logger,
        DataHelper $dataHelper
    ) {
        $this->_session = $session;
        $this->_productRepository = $productRepository;
        $this->_request = $request;
        $this->_logger = $logger;
        $this->_dataHelper = $dataHelper;
    }

    /**
     * @param GuestPaymentInformationManagementInterface $payment
     * @param callable $proceed
     * @param $cartId
     * @param $email
     * @param PaymentInterface $paymentMethod
     * @param AddressInterface|null $billingAddress
     * @return mixed
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function aroundSavePaymentInformationAndPlaceOrder(
        GuestPaymentInformationManagementInterface $payment,
        callable $proceed,
        $cartId,
        $email,
        PaymentInterface $paymentMethod,
        AddressInterface $billingAddress = null
    ) {
        $qId = (int) $this->_session->getQuoteId();
        $items = $this->_session->getQuote()->getAllVisibleItems();

        $cart = [];
        foreach ($items as $item) {
            $cart[] = [
                "sku" => $item->getSku(),
                "qty" => (int) $item->getQty(),
                "price" => (int) $item->getPrice(),
                "custom_price" => (int) $item->getCustomPrice()
            ];
        }

        $additionalData = !empty($paymentMethod->getAdditionalData()) ? $paymentMethod->getAdditionalData() : [];
        $additional = $this->_logger->filterDebugData(
            $additionalData,
            ['cc_cid', 'cc_number', 'expiration', 'expiration_yr']
        );

        $this->_logger->debug("*********** checkout guest payment before placed order ***********");
        $this->_logger->debug([
            "action" => "checkoutGuestPayment",
            "cart-id" => $cartId,
            "quote-id" => $qId,
            "guest_email" => $email,
            "order-id" => null,
            "address" => [
                'id' => $billingAddress->getId(),
                'street' => $billingAddress->getStreet(),
                'city' => $billingAddress->getCity(),
                'region' => $billingAddress->getRegion(),
                'region_id' => $billingAddress->getRegionId(),
                'region_code' => $billingAddress->getRegionCode(),
                'country' => $billingAddress->getCountryId(),
                'customer_id' => $billingAddress->getCustomerId(),
                'customer_address_id' => $billingAddress->getCustomerAddressId(),
                'firstname' => $billingAddress->getFirstname(),
                'lastname' => $billingAddress->getLastname(),
                'company' => $billingAddress->getCompany(),
                'postcode' => $billingAddress->getPostcode(),
                'vat_id' => $billingAddress->getVatId(),
                'telephone' => $billingAddress->getTelephone(),
            ],
            "payment" => [
                'method' => $paymentMethod->getMethod(),
                'additional_data' => $additional
            ],
            "ordered_items" => $cart,
            "total" => $this->_session->getQuote()->getGrandTotal(),
            "customer_type" => $this->_dataHelper->getCustomerType(),
            "customer_email" => $this->_dataHelper->getCustomerEmail()
        ]);

        $result = $proceed($cartId, $email, $paymentMethod, $billingAddress);

        $this->_logger->debug("*********** checkout guest payment after placed order ***********");
        $this->_logger->debug([
            "action" => "checkoutPaymentGuest",
            "cart-id" => $cartId,
            "quote-id" => $qId,
            "guest_email" => $email,
            "order-id" => !empty($result) ? $result : null,
            "address" => [
                'id' => $billingAddress->getId(),
                'street' => $billingAddress->getStreet(),
                'city' => $billingAddress->getCity(),
                'region' => $billingAddress->getRegion(),
                'region_id' => $billingAddress->getRegionId(),
                'region_code' => $billingAddress->getRegionCode(),
                'country' => $billingAddress->getCountryId(),
                'customer_id' => $billingAddress->getCustomerId(),
                'customer_address_id' => $billingAddress->getCustomerAddressId(),
                'firstname' => $billingAddress->getFirstname(),
                'lastname' => $billingAddress->getLastname(),
                'company' => $billingAddress->getCompany(),
                'postcode' => $billingAddress->getPostcode(),
                'vat_id' => $billingAddress->getVatId(),
                'telephone' => $billingAddress->getTelephone(),
            ],
            "payment" => [
                'method' => $paymentMethod->getMethod(),
                'additional_data' => $additional
            ],
            "ordered_items" => $cart,
            "total" => !empty($result) ? $this->_session->getQuote()->getGrandTotal() : 0,
            "success" => !empty($result),
            "customer_type" => $this->_dataHelper->getCustomerType(),
            "customer_email" => $this->_dataHelper->getCustomerEmail()
        ]);


        return $result;
    }
}
