<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Plugin\Payment\Method;

use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\RequestInterface;
use MMS\Logger\LoggerInterface;

/**
 * Class Logger
 * @package MMS\Logger\Plugin\Payment\Method
 */
class Logger
{
    /**
     * @var Session
     */
    protected $_session;
    /**
     * @var ProductRepository
     */
    protected $_productRepository;
    /**
     * @var RequestInterface
     */
    protected $_request;
    /**
     * @var \MMS\Logger\Model\Logger
     */
    protected $_logger;

    /**
     * Logger constructor.
     * @param Session $session
     * @param ProductRepository $productRepository
     * @param RequestInterface $request
     * @param LoggerInterface $logger
     */
    public function __construct(
        Session $session,
        ProductRepository $productRepository,
        RequestInterface $request,
        LoggerInterface $logger
    ) {
        $this->_session = $session;
        $this->_productRepository = $productRepository;
        $this->_request = $request;
        $this->_logger = $logger;
    }

    /**
     * @param \Magento\Payment\Model\Method\Logger $subject
     * @param \Closure $proceed
     * @param array $data
     * @param array|null $maskKeys
     * @param null $forceDebug
     */
    public function aroundDebug(
        \Magento\Payment\Model\Method\Logger $subject,
        \Closure $proceed,
        array $data,
        array $maskKeys = null,
        $forceDebug = null
    ) {
        $data = $this->_logger->filterDebugData(
            $data,
            $maskKeys
        );
        $this->_logger->info("*********** payment methods logger debug ***********");
        $this->_logger->info([
            "action" => "PaymentMethodsDebugOnRequestResponse",
            "request" => $data
        ]);

        $proceed($data, $maskKeys, $forceDebug);
    }
}
