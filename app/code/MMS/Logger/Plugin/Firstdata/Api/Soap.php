<?php
/**
 * @package     MMS_Logger
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
namespace MMS\Logger\Plugin\Firstdata\Api;

use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\RequestInterface;
use MMS\Logger\LoggerInterface;

/**
 * Class Soap
 * @package MMS\Logger\Plugin\Firstdata\Api
 */
class Soap
{
    /**
     * @var Session
     */
    protected $_session;
    /**
     * @var ProductRepository
     */
    protected $_productRepository;
    /**
     * @var RequestInterface
     */
    protected $_request;
    /**
     * @var \MMS\Logger\Model\Logger
     */
    protected $_logger;

    /**
     * Soap constructor.
     * @param Session $session
     * @param ProductRepository $productRepository
     * @param RequestInterface $request
     * @param LoggerInterface $logger
     */
    public function __construct(
        Session $session,
        ProductRepository $productRepository,
        RequestInterface $request,
        LoggerInterface $logger
    ) {
        $this->_session = $session;
        $this->_productRepository = $productRepository;
        $this->_request = $request;
        $this->_logger = $logger;
    }

    /**
     * @param \Magedelight\Firstdata\Model\Api\Soap $subject
     * @param $result
     */
    public function afterPrepareRefundResponse(
        \Magedelight\Firstdata\Model\Api\Soap $subject,
        $result
    ) {
        $this->_logger->info("*********** md firstdata payment prepare refund response ***********");
        $this->_logger->info([
            "action" => "MdFirstDataPrepareRefundResponse",
            "response" => $result
        ]);

        return $result;
    }

    /**
     * @param \Magedelight\Firstdata\Model\Api\Soap $subject
     * @param $result
     */
    public function afterPrepareVoidResponse(
        \Magedelight\Firstdata\Model\Api\Soap $subject,
        $result
    ) {
        $this->_logger->info("*********** md firstdata payment prepare void response ***********");
        $this->_logger->info([
            "action" => "MdFirstDataPrepareVoidResponse",
            "response" => $result
        ]);

        return $result;
    }

    /**
     * @param \Magedelight\Firstdata\Model\Api\Soap $subject
     * @param $result
     */
    public function afterPrepareAuthorizeRequest(
        \Magedelight\Firstdata\Model\Api\Soap $subject,
        $result
    ) {
        $this->_logger->info("*********** md firstdata payment prepare authorize request ***********");
        $loggingData = $this->prepareDataForLog($result);
        $this->_logger->info([
            "action" => "MdFirstDataPrepareAuthorizeRequest",
            "request" => $loggingData
        ]);

        return $result;
    }

    public function prepareDataForLog($result){
        if (isset($result['keyId'])) {
            $result['keyId'] = 'XXXX';
        }
        if (isset($result['hmacKey'])) {
            $result['hmacKey'] = 'XXXX';
        }
        if (isset($result['gatewayPass'])) {
            $result['gatewayPass'] = 'XXXX';
        }
        if (isset($result['cardnumber'])) {
            $value = substr($result['cardnumber'], -4, 4);
            $value = 'XXXX-'.$value;
            $result['cardnumber'] = $value;
        }
        if (isset($result['cardexpmonth'])) {
            $result['cardexpmonth'] = 'XX';
        }
        if (isset($result['cardexpyear'])) {
            $result['cardexpyear'] = 'XX';
        }
        if (isset($result['cvmvalue'])) {
            $result['cvmvalue'] = 'XXX';
        }
        if (isset($result['cvv2value'])) {
            $result['cvv2value'] = 'XXX';
        }
        return $result;
    }

    /**
     * @param \Magedelight\Firstdata\Model\Api\Soap $subject
     * @param $result
     */
    public function afterPrepareAuthorizeResponse(
        \Magedelight\Firstdata\Model\Api\Soap $subject,
        $result
    ) {
        $this->_logger->info("*********** md firstdata payment prepare authorize response ***********");
        $this->_logger->info([
            "action" => "MdFirstDataPrepareAuthorizeResponse",
            "response" => $result
        ]);

        return $result;
    }

    /**
     * @param \Magedelight\Firstdata\Model\Api\Soap $subject
     * @param $result
     */
    public function afterPrepareAuthorizeCaptureRequest(
        \Magedelight\Firstdata\Model\Api\Soap $subject,
        $result
    ) {
        $this->_logger->info("*********** md firstdata payment prepare authorize capture request ***********");
        $this->_logger->info([
            "action" => "MdFirstDataPrepareAuthorizeCaptureRequest",
            "request" => $result
        ]);

        return $result;
    }

    /**
     * @param \Magedelight\Firstdata\Model\Api\Soap $subject
     * @param $result
     */
    public function afterPrepareAuthorizeCaptureResponse(
        \Magedelight\Firstdata\Model\Api\Soap $subject,
        $result
    ) {
        $this->_logger->info("*********** md firstdata payment prepare authorize capture response ***********");
        $this->_logger->info([
            "action" => "MdFirstDataPrepareAuthorizeCaptureResponse",
            "response" => $result
        ]);

        return $result;
    }

    /**
     * @param \Magedelight\Firstdata\Model\Api\Soap $subject
     * @param $result
     */
    public function afterPrepareCaptureRequest(
        \Magedelight\Firstdata\Model\Api\Soap $subject,
        $result
    ) {
        $this->_logger->info("*********** md firstdata payment prepare capture request ***********");
        $this->_logger->info([
            "action" => "MdFirstDataPrepareCaptureRequest",
            "request" => $result
        ]);

        return $result;
    }

    /**
     * @param \Magedelight\Firstdata\Model\Api\Soap $subject
     * @param $result
     */
    public function afterPrepareCaptureResponse(
        \Magedelight\Firstdata\Model\Api\Soap $subject,
        $result
    ) {
        $this->_logger->info("*********** md firstdata payment prepare capture response ***********");
        $this->_logger->info([
            "action" => "MdFirstDataPrepareCaptureResponse",
            "response" => $result
        ]);

        return $result;
    }

    /**
     * @param \Magedelight\Firstdata\Model\Api\Soap $subject
     * @param $result
     */
    public function afterUpdateCustomerProfileRequest(
        \Magedelight\Firstdata\Model\Api\Soap $subject,
        $result
    ) {
        $this->_logger->info("*********** md firstdata payment update customer profile request ***********");
        $this->_logger->info([
            "action" => "MdFirstDataUpdateCustomerProfileRequest",
            "request" => $result
        ]);

        return $result;
    }

    /**
     * @param \Magedelight\Firstdata\Model\Api\Soap $subject
     * @param $result
     */
    public function afterUpdateCustomerProfile(
        \Magedelight\Firstdata\Model\Api\Soap $subject,
        $result
    ) {
        $this->_logger->info("*********** md firstdata payment update customer profile response ***********");
        $this->_logger->info([
            "action" => "MdFirstDataUpdateCustomerProfile",
            "response" => $result
        ]);

        return $result;
    }

    /**
     * @param \Magedelight\Firstdata\Model\Api\Soap $subject
     * @param $result
     */
    public function afterCreateCustomerProfile(
      \Magedelight\Firstdata\Model\Api\Soap $subject,
      $result
    ) {
        $this->_logger->info("*********** md firstdata payment create customer profile response ***********");
        $this->_logger->info([
            "action" => "MdFirstDataCreateCustomerProfile",
            "response" => $result
        ]);

        return $result;
    }
}
