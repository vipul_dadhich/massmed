<?php

namespace MMS\UpdateCountryState\Setup;

use \Magento\Framework\Setup\InstallDataInterface;
use \Magento\Framework\Setup\ModuleDataSetupInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\DB\Ddl\Table;



class InstallData implements InstallDataInterface{

    public function __construct(
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory
    ){
        $this->_countryFactory = $countryFactory;
        $this->regionFactory = $regionFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context){

        $countryToDelete = ["AS","FM","GU","MH","MP","PW","PR","UM","VI"];

        $num = count($countryToDelete);
        for ($c=0; $c < $num; $c++) {
            $country = $this->_countryFactory->create()->loadByCode($countryToDelete[$c]);
            if($country){
                $country->delete();
            }
        }

        $setup->startSetup();
        
        $region = $this->regionFactory->create()->loadByCode("UM", "US");

        if(count($region->getData())==0){
            $setup->getConnection()->Insert(
                $setup->getTable('directory_country_region'),
                [
                    'country_id' => 'US',
                    'code' => 'UM',
                    'default_name' => 'United States Minor Outlying Islands'
                ]
            );
        }

        $setup->endSetup();
    }

}