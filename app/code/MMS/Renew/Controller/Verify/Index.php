<?php
/**
 * See COPYING.txt for license details.
 *
 * Renew extension
 * NOTICE OF LICENSE
 */
namespace MMS\Renew\Controller\Verify;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use MMS\Renew\Helper\Config;
use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;

/**
 * Tax totals calculation model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Index extends Action implements HttpGetActionInterface
{

    /**
     * @var Config
     */
    protected $dataHelper;

    /**
     * @var PageFactory
     */
    protected $pageFactory;

    public function __construct(
        Config $dataHelper,
        Context $context,
        PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->dataHelper = $dataHelper;
        $this->pageFactory = $pageFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return ResultInterface|ResponseInterface
     * @throws NotFoundException
     */
    public function execute()
    {
        if ($this->dataHelper->customerIsLogin()) {
            $page = $this->pageFactory->create();
            $page->getConfig()->getTitle()->set(__('Renew'));
            return $page;
        }

        return $this->_redirect('renew');
    }
}
