<?php


namespace MMS\Renew\Controller\Index;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\PageFactory;
use MMS\Renew\Api\CreateQuoteServiceInterface;
use MMS\Renew\Helper\Config;
use MMS\Renew\Model\Catalyst\Api\GetEligibility;

/**
 * Tax totals calculation model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Test extends Action implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;
    /**
     * @var Config
     */
    protected $dataHelper;
    /**
     * @var GetEligibility
     */
    protected $_eligibilityApi;
    /**
     * @var CreateQuoteServiceInterface
     */
    protected $_createQuoteService;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        GetEligibility $eligibilityApi,
        Config $dataHelper,
        CreateQuoteServiceInterface $createQuoteService
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->_eligibilityApi = $eligibilityApi;
        $this->dataHelper = $dataHelper;
        $this->_createQuoteService = $createQuoteService;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return ResultInterface|ResponseInterface
     * @throws NotFoundException
     */
    public function execute()
    {
        if ($this->getRequest()->getParam('debug', false) == true) {
            $reNewStatus = 'NOT-ELIGIBLE';
            $eligibilityInfo = $this->sampleData();
            $eligibilityInfo = $this->dataHelper->filterRenewResponse($eligibilityInfo);

            if (isset($eligibilityInfo['products']) && !empty($eligibilityInfo['products'])
                && isset($eligibilityInfo['products'][0]['renew'])) {
                if (!$eligibilityInfo['products'][0]['renew']['isEligible']) {
                    $reNewStatus = $eligibilityInfo['products'][0]['renew']['status'];
                } else {
                    try {
                        $this->_createQuoteService->setRequestData($eligibilityInfo);
                        $this->_createQuoteService->execute();
                        $this->messageManager->addSuccessMessage(__('You added items for renew to your shopping cart.'));
                        return $this->_redirect('checkout/cart');
                    } catch (LocalizedException $e) {
                        $this->messageManager->addNoticeMessage(
                            $this->_objectManager->get(Escaper::class)->escapeHtml($e->getMessage())
                        );
                    } catch (Exception $e) {
                        $this->messageManager->addExceptionMessage(
                            $e,
                            __('We can\'t add this item to your shopping cart right now.')
                        );
                    }
                    return $this->_redirect('/');
                }
            }
            $this->dataHelper->setRenewStatusToCustomerSession($reNewStatus);
            return $this->_redirect('renew-not-eligible');
        } else {
            return $this->_redirect('no-route');
        }
    }

    public function sampleData()
    {
        return json_decode('{
	"products": [{
			"productCode": "ONJ",
			"offerCode": "NEJ-ONJ",
			"expirationDate": "2022-03-24",
			"purchase": {
				"isEligible": false,
				"status": "RENEW_ELIGIBLE"
			},
			"renew": {
				"isEligible": true,
				"status": ""
			}
		},
		{
			"productCode": "NEJ",
			"offerCode": "NEJ-NEJ",
			"expirationDate": "2021-04-22",
			"purchase": {
				"isEligible": false,
				"status": ""
			},
			"renew": {
				"isEligible": false,
				"status": ""
			}
		}
	]
}', true);
    }
}
