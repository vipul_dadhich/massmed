<?php
/**
 * See COPYING.txt for license details.
 *
 * Renew extension
 * NOTICE OF LICENSE
 */
namespace MMS\Renew\Controller\Index;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\PageFactory;
use MMS\Renew\Api\CreateQuoteServiceInterface;
use MMS\Renew\Helper\Config;
use MMS\Renew\Model\Catalyst\Api\GetEligibility;
use Ey\MuleSoft\Helper\Data;
use MMS\Logger\LoggerInterface;

/**
 * Tax totals calculation model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;
    /**
     * @var Config
     */
    protected $dataHelper;
    /**
     * @var Data
     */
    protected $mulesoftHelper;
    /**
     * @var GetEligibility
     */
    protected $_eligibilityApi;
    /**
     * @var CreateQuoteServiceInterface
     */
    protected $_createQuoteService;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Magento\Framework\App\RequestInterface $request,
        GetEligibility $eligibilityApi,
        Config $dataHelper,
        CreateQuoteServiceInterface $createQuoteService,
        Data $mulesoftHelper,
        \Ey\PromoCode\Helper\Data $promoHelper,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->request = $request;
        $this->_eligibilityApi = $eligibilityApi;
        $this->dataHelper = $dataHelper;
        $this->_createQuoteService = $createQuoteService;
        $this->mulesoftHelper = $mulesoftHelper;
        $this->isDebugMode = $this->mulesoftHelper->getMuleSoftDebug();
        $this->isDebugModeType = $this->mulesoftHelper->getMuleSoftDebugMode();
        $this->promoHelper = $promoHelper;
        $this->_logger = $logger;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return ResultInterface|ResponseInterface
     * @throws NotFoundException
     */
    public function execute()
    {
        if ($this->dataHelper->customerIsLogin()) {
            $reNewStatus = 'NOT-ELIGIBLE';
            $customer = $this->dataHelper->getCustomerData();
            if ($customer->getData('ucid')) {
                $promoCodeParam = $this->promoHelper->getPromoCodeUrlParam();
                if($promoCodeParam){
                    $promoCode = $this->request->getParam($promoCodeParam);
                    if($promoCode){
                        $this->promoHelper->setCustomCookie('PromoCode', $promoCode);
                    }
                }

                $this->_logger->debug("****************** Renew - GetEligibility API Log Start *******************");

                //$this->mulesoftLoggerinfo->info("Customer Session data-". json_encode($customer->getData(), JSON_PRETTY_PRINT), 'debug_authtoken.log');
                $eligibilityInfo = $this->_eligibilityApi->execute($customer->getData('ucid'));

                if($this->isDebugModeType) {
                    $this->_logger->debug("Renew GetEligibility Response- ".
                        json_encode($eligibilityInfo, JSON_PRETTY_PRINT));
                }
                $this->_logger->debug("****************** Renew - GetEligibility API Log End *******************");

                if ($eligibilityInfo) {
                    $this->dataHelper->setRenewResponseData($eligibilityInfo);

                    $eligibilityInfo = $this->dataHelper->filterRenewResponse($eligibilityInfo);
                    if (isset($eligibilityInfo['products']) && !empty($eligibilityInfo['products'])
                        && isset($eligibilityInfo['products'][0]['renew'])) {
                        if (!$eligibilityInfo['products'][0]['renew']['isEligible']) {
                            $reNewStatus = $eligibilityInfo['products'][0]['renew']['status'];
                        } else {
                            try {
                                if($eligibilityInfo['products'][0]['offerCode'] == NULL || $eligibilityInfo['products'][0]['offerCode'] == "")
                                {
                                    $eligibilityInfo['products'][0]['offerCode'] = "CAT-ANL-DFT";
                                    //TODO - Default to the Annual Logic -Make id dynamic
                                }
                                if (isset($eligibilityInfo['products'][0]['purchase']['status'])) {
                                    $agencyVerified = $this->request->getParam('is_agency');
                                    $reNewPurchaseStatus = $eligibilityInfo['products'][0]['purchase']['status'];
                                    if ($reNewPurchaseStatus == 'AGENT' && !$agencyVerified) {
                                        return $this->_redirect('renew/verify');
                                    }
                                }
                                $this->_createQuoteService->setRequestData($eligibilityInfo);
                                $this->_createQuoteService->execute();
                                return $this->_redirect('renew/checkout');
                            } catch (LocalizedException $e) {
                                $this->messageManager->addNoticeMessage(
                                    $this->_objectManager->get(Escaper::class)->escapeHtml($e->getMessage())
                                );
                            } catch (Exception $e) {
                                $this->messageManager->addExceptionMessage(
                                    $e,
                                    __('We can\'t add this item to your shopping cart right now.')
                                );
                            }
                            return $this->_redirect('/');
                        }
                    }
                } else {
                    return $this->_redirect('apologies');
                }
            }
            $this->dataHelper->setRenewStatusToCustomerSession($reNewStatus);
            return $this->_redirect('renew-not-eligible');
        }
        $page = $this->pageFactory->create();
        $page->getConfig()->getTitle()->set(__('Renew'));
        return $page;
    }
}
