<?php

namespace MMS\Renew\Plugin\Checkout\Model;

use Exception;
use Magento\Checkout\Model\Session;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Cart
 * @package MMS\Renew\Plugin\Checkout\Model
 */
class Cart
{
    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * Cart constructor.
     *
     * @param Session $checkoutSession
     */
    public function __construct(
        Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Checkout\Model\Cart $subject
     * @param $productInfo
     * @param null $requestInfo
     * @return mixed
     * @throws Exception
     */
    public function beforeAddProduct(
        \Magento\Checkout\Model\Cart $subject,
        $productInfo,
        $requestInfo = null
    ) {
        try {
            $quote = $this->checkoutSession->getQuote();
            if ($quote && $quote->getData('is_quote_renew') && !$quote->hasItems()) {
                $quote->setData('is_quote_renew', 0)->save();
            }
        } catch (NoSuchEntityException $e) {
        } catch (LocalizedException $e) {
        }

        return array($productInfo, $requestInfo);
    }
}