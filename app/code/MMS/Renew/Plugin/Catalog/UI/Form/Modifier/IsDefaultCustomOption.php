<?php

namespace MMS\Renew\Plugin\Catalog\UI\Form\Modifier;

use MMS\Renew\Helper\Config;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\CustomOptions;
use Magento\Ui\Component\Form\Element\Checkbox;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Field;

class IsDefaultCustomOption
{
    const FIELD_IS_DEFAULT = 'is_default';
    const DEFAULT_SORT_ORDER = 70;
    const FIELD_IS_VISIBLE_NAME = 'is_hide_frontend';

    /**
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function afterModifyMeta(CustomOptions $subject, array $meta): array
    {
        if (!$this->config->isDefaultValueActive()) {
            return $meta;
        }

        return array_replace_recursive($meta, [
            CustomOptions::GROUP_CUSTOM_OPTIONS_NAME => [
                'children' => [
                    CustomOptions::GRID_OPTIONS_NAME => [
                        'children' => [
                            'record' => [
                                'children' => [
                                    CustomOptions::CONTAINER_OPTION => [
                                        'children' => [
                                            CustomOptions::CONTAINER_COMMON_NAME => [
                                                'children' => [
                                                    static::FIELD_IS_VISIBLE_NAME => $this->getIsHideFieldConfig(self::DEFAULT_SORT_ORDER)
                                                ],
                                            ],
                                            CustomOptions::GRID_TYPE_SELECT_NAME => [
                                                'children' => [
                                                    'record' => [
                                                        'children' => [
                                                            static::FIELD_IS_DEFAULT => $this->getIsDefaultFieldConfig(self::DEFAULT_SORT_ORDER),
                                                        ],
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }

    protected function getIsDefaultFieldConfig($sortOrder): array
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Default Value'),
                        'componentType' => Field::NAME,
                        'formElement' => Checkbox::NAME,
                        'dataScope' => static::FIELD_IS_DEFAULT,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'value' => '0',
                        'valueMap' => [
                            'true' => '1',
                            'false' => '0',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Get config for "Required" field
     *
     * @param int $sortOrder
     * @return array
     * @since 101.0.0
     */
    protected function getIsHideFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Is Hide For Frontend'),
                        'componentType' => Field::NAME,
                        'formElement' => Checkbox::NAME,
                        'dataScope' => static::FIELD_IS_VISIBLE_NAME,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'value' => '1',
                        'valueMap' => [
                            'true' => '1',
                            'false' => '0'
                        ],
                    ],
                ],
            ],
        ];
    }
}
