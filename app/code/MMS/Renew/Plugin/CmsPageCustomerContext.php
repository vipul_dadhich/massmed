<?php

namespace MMS\Renew\Plugin;

use MMS\Renew\Helper\Config;
use Magento\Framework\App\Http\Context;
use Magento\Customer\Model\Session;

class CmsPageCustomerContext
{
    /**
     * @var Config
     */
    private $config;
    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @param Config $config
     * @param Session $customerSession
     */
    public function __construct(
        Config $config,
        Session $customerSession
    ) {
        $this->config = $config;
        $this->_customerSession = $customerSession;
    }

    /**
     * Sets appropriate header if customer session is persistent.
     *
     * @param Context $subject
     * @return mixed
     */
    public function beforeGetVaryString(Context $subject)
    {
        $renewStatus = $this->config->getRenewStatusFromCustomerSession();
        if (!$renewStatus) {
            $renewStatus = 'NOT_A_SUBSCRIBER';
        }
        $subject->setValue('RENEW_STATUS', $renewStatus, '0');
    }
}