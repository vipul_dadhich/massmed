<?php

namespace MMS\Renew\Plugin;
use Magento\Framework\View\LayoutInterface;

class CompositeConfigProviderPlugin
{

    protected $_checkoutSession;
    /**
     * @var \Magento\Catalog\Model\Product
     */

    /**
     * Initialize dependencies.
     *
     * @param \MMS\Renew\Helper\Config $config
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Catalog\Model\Product $product
     */
    public function __construct(
        \MMS\Renew\Helper\Config $config,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_config = $config;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Checkout\Model\CompositeConfigProvider $subject
     * @param array $result
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetConfig(\Magento\Checkout\Model\CompositeConfigProvider $subject, array $result)
    {
        if ($this->_config->isEnabled()
            && $this->getCheckoutSession()->getQuote()->getData('is_quote_renew')
        ) {
            $result['payment']['md_firstdata']['storedCards'] = array();
            $result['payment']['md_firstdata']['storedCards']['new'] = 'Use other card';
            $result['payment']['md_firstdata']['canSaveCard'] = false;
        }

        return $result;
    }


    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}


