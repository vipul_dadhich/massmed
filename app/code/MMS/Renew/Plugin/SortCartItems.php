<?php

namespace MMS\Renew\Plugin;

class SortCartItems
{
    public function afterGetSectionData(
        \Magento\Checkout\CustomerData\Cart $subject,
        $result
    ) {
        $itemSorted = [];
        $items = $result['items'];
        if (count($items) > 1) {
            foreach($items as $item) {
                $itemSorted[$item['item_id']] = $item;
            }

            ksort($itemSorted);
            $data = ['items' => array_values($itemSorted)];
            $result = array_merge($result, $data);
        }

        return $result;
    }
}