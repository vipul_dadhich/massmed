<?php

namespace MMS\Renew\Plugin;

class ConfigProviderPlugin
{
    protected $professionalCategory = ['RES', 'STU'];

    /**
     * Url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * Renew checkout helper
     *
     * @var \MMS\Renew\Helper\Config
     */
    protected $_config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    /**
     * @var \MMS\Customer\Model\Source\ProfessionalCategory
     */
    protected $_professionalCategorySource;

    /**
     * Initialize dependencies.
     *
     * @param \MMS\Renew\Helper\Config $config
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Catalog\Model\Product $product
     * @param \MMS\Customer\Model\Source\ProfessionalCategory $professionalCategorySource
     */
    public function __construct(
        \MMS\Renew\Helper\Config $config,
        \Magento\Framework\UrlInterface $url,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\Product $product,
        \MMS\Customer\Model\Source\ProfessionalCategory $professionalCategorySource
    ) {
        $this->_config = $config;
        $this->_checkoutSession = $checkoutSession;
        $this->url = $url;
        $this->product = $product;
        $this->_customerSession = $customerSession;
        $this->_professionalCategorySource = $professionalCategorySource;
    }

    /**
     * @param \Magento\Checkout\Model\DefaultConfigProvider $subject
     * @param array $result
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, array $result)
    {
        if ($this->_config->isEnabled()
            && $this->getCheckoutSession()->getQuote()->getData('is_quote_renew')
        ) {
            $result['checkoutUrl'] = $this->url->getUrl('renew/checkout');
            if ($this->_config->isEnabledRenewConfirmation()) {
                $result['defaultSuccessPageUrl'] = $this->url->getUrl('renew/checkout/success');
            }
            $items = $result['quoteItemData'];
            $renewalRates = [];
            $freeItemCount = 0;
            $freeGiftEnabled = $this->_config->isFreeGiftEnabled();
            $freeGiftSku = $this->_config->getFreeGiftProductSku();
            $isProfessionalCatValid = false;
            $isProfessionalCatValue = '';
            $renewalRatesLimit = $this->_config->getRenewOptionLimit();

            if (count($items) > 0) {
                /** @var $quote \Magento\Quote\Model\Quote */
                $quote = $this->getCheckoutSession()->getQuote();
                $customerProfessionalCategory = $this->getProfessionalCategory();
                $professionalCatOptions = $this->_professionalCategorySource->getAllOptions();;
                foreach($items as $item) {
                    $buyRequest = [];
                    if ($this->_config->isBrandNejm($item['sku']) && in_array($customerProfessionalCategory, $this->professionalCategory)) {
                        $isProfessionalCatValid = true;
                        $isProfessionalCatValue = $customerProfessionalCategory;
                        $quoteItem = $quote->getItemById($item['item_id']);
                        $option = $quoteItem->getOptionByCode('info_buyRequest');
                        if ($option) {
                            $buyRequest = \json_decode($option->getValue(), true);
                            if (is_null($buyRequest)) {
                                $buyRequest = \unserialize($option->getValue());
                            }
                        }
                    }

                    $product = $this->product->load($item['product_id']);
                    if ($product->getOptions()) {
                        $isProfessionalValid = false;
                        foreach($product->getOptions() as $option) {
                            $optionData = [];
                            foreach ($option->getValues() as $value) {
                                $profCatLabel = '';
                                $profCatValue = '';
                                if ($isProfessionalCatValid && !empty($buyRequest)
                                    && isset($buyRequest['options'][$option->getOptionId()])
                                    && $buyRequest['options'][$option->getOptionId()] == $value->getData('option_type_id')) {
                                    foreach ($professionalCatOptions as $proCat) {
                                        if ($proCat['value'] == $customerProfessionalCategory) {
                                            $profCatLabel = (string)$proCat['label'];
                                            $profCatValue = $proCat['value'];
                                        }
                                    }
                                    $isProfessionalValid = true;
                                }
                                $optionData[] = array_merge(
                                    $value->getData(),
                                    ['prof_cat_label' => $profCatLabel],
                                    ['prof_cat_value' => $profCatValue],
                                    ['quote_item_id' => $item['item_id']],
                                    ['quote_options' => $item['options'] ?? []],
                                    ['quote_item_price' => $product->getFinalPrice()],
                                    ['quote_item_base_price' => $product->getPrice()],
                                    ['quote_item_custom_price' => $product->getCustomPrice()]
                                );
                                if ($isProfessionalValid) {
                                    $clonedArray = $optionData[0];
                                    $profCatLabel = ($customerProfessionalCategory == 'STU') ? 'Resident' : 'Physician';
                                    $profCatValue = ($customerProfessionalCategory == 'STU') ? 'RES' : 'PHY';
                                    $clonedArray['prof_cat_label'] = $profCatLabel;
                                    $clonedArray['prof_cat_value'] = $profCatValue;
                                    $optionData[] = $clonedArray;
                                    break;
                                }
                            }
                            $this->resetDefaultOption($optionData);
                            $renewalRates[] = [
                                'option_id' => $option->getOptionId(),
                                'options' => $optionData
                            ];
                            if ($isProfessionalValid) {
                                break;
                            }
                        }
                    }

                    if ($freeGiftEnabled && $product->getSku() == $freeGiftSku) {
                        $freeItemCount++;
                    }
                }
            }

            if (isset($renewalRates[0]['options']) && count($renewalRates[0]['options']) > $renewalRatesLimit) {
                $renewalRates[0]['options'] = array_slice($renewalRates[0]['options'], 0, $renewalRatesLimit);
            }

            $result['renewalCMSBlock']['top']['enable'] = $this->_config->isShowTopPositionBlock();
            $result['renewalCMSBlock']['top']['html'] = $this->_config->getTopPositionBlockHtml();
            $result['renewalCMSBlock']['bottom']['enable'] = $this->_config->isShowBottomPositionBlock();
            $result['renewalCMSBlock']['bottom']['html'] = $this->_config->getBottomPositionBlockHtml();

            $result['renewalRates'] = $renewalRates;
            $result['renewalRatesProfCatValid'] = $isProfessionalCatValid;
            $result['renewalRatesProfCatValue'] = $isProfessionalCatValue;
            $result['renewalRatesShow'] = (count($items) - $freeItemCount) == 1;
        }
        return $result;
    }

    protected function resetDefaultOption(&$optionData)
    {
        if (count($optionData) > 0) {
            $defaultItemIndex = 0;
            foreach ($optionData as $key => $item) {
                if ($item['is_default'] == 1) {
                    $defaultItemIndex = $key;
                }
                $item['is_default'] = 0;
                if (isset($item['quote_options']) && count($item['quote_options']) > 0) {
                    foreach ($item['quote_options'] as $option) {
                        if ($option['value'] == $item['default_title']) {
                            $defaultItemIndex = $key;
                        }
                    }
                }
            }
            $optionData[$defaultItemIndex]['is_default'] = 1;
        }
    }

    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    /**
     * @return false|string
     */
    private function getProfessionalCategory()
    {
        $customer = $this->_customerSession->getCustomer();
        if ($customer) {
            return $customer->getProfessionalCategory();
        }

        return false;
    }
}
