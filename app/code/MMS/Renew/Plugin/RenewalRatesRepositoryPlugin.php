<?php

namespace MMS\Renew\Plugin;

use Magento\Checkout\Model\Session;
use Magento\Quote\Model\Quote;
use MMS\PriceEngine\Service\PriceEngine;
use MMS\Renew\Helper\Config;

/**
 * Class RenewalRatesRepositoryPlugin
 * @package MMS\Renew\Plugin
 */
class RenewalRatesRepositoryPlugin
{
    /**
     * @var Config
     */
    protected $_configRenew;
    /**
     * @var PriceEngine
     */
    protected $_priceEngineService;
    /**
     * @var Session
     */
    protected $_checkoutSession;

    /**
     * RenewalRatesRepositoryPlugin constructor.
     * @param Config $configRenew
     * @param PriceEngine $priceEngineService
     * @param Session $checkoutSession
     */
    public function __construct(
        Config $configRenew,
        PriceEngine $priceEngineService,
        Session $checkoutSession
    ) {
        $this->_configRenew = $configRenew;
        $this->_priceEngineService = $priceEngineService;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param \MMS\Renew\Model\RenewalRatesRepository $subject
     * @param array $result
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterGetRenewalRatesList(
        \MMS\Renew\Model\RenewalRatesRepository $subject,
        array $result
    ): array
    {
        if ($this->_priceEngineService->isEnabled()) {
            /** @var $quote Quote */
            $quote = $this->_checkoutSession->getQuote();
            if (isset($result[0]) && !empty($result[0])) {
                $sku = [];
                $terms = [];
                if (isset($result[0]['options'])) {
                    foreach ($result[0]['options'] as $option) {
                        $quoteItem = $quote->getItemById($option['quote_item_id']);
                        if ($quoteItem && $quoteItem->getId()) {
                            $sku = $quoteItem->getSku();
                        }
                        $term = $option['default_title'];
                        if ($this->_priceEngineService->isEnabledFixtureMode()) {
                            $term = str_replace(" ", "-", $term);
                        }
                        $terms[] = $term;
                    }
                }
                $payload['sku'] = $sku;
                if (!empty($terms)) {
                    $payload['term'] = $terms;
                }

                $this->_priceEngineService->setFullTermsResponse(true);
                $this->_priceEngineService->setRequest($payload);
                $this->_priceEngineService->buildPrice();
                $response = $this->_priceEngineService->getPrice();
                if (isset($response['terms']) && !empty($response['terms'])) {
                    foreach ($response['terms'] as $term) {
                        foreach ($result[0]['options'] as $key => &$option) {
                            if (strtolower($option['default_title']) == strtolower($term['term'])) {
                                $option['default_custom_price'] = $term['price'];
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }
}
