<?php

namespace MMS\Renew\Plugin;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\InventoryConfigurationApi\Exception\SkuIsNotAssignedToStockException;
use Magento\InventorySalesApi\Api\Data\ProductSalableResultInterface;
use MMS\Renew\Model\IsProductSalableForRequestedQtyCondition\IsCorrectQtyCondition as IsCorrectQtyConditionRenew;
use Magento\InventorySales\Model\IsProductSalableForRequestedQtyCondition\IsCorrectQtyCondition as InventorySalesIsCorrectQtyCondition;

/**
 * Class IsCorrectQtyCondition
 * @package MMS\Renew\Plugin
 */
class IsCorrectQtyCondition
{
    /**
     * @var IsCorrectQtyConditionRenew
     */
    protected $_isCorrectQtyConditionRenew;

    /**
     * IsCorrectQtyCondition constructor.
     * @param IsCorrectQtyConditionRenew $isCorrectQtyConditionRenew
     */
    public function __construct(
        IsCorrectQtyConditionRenew $isCorrectQtyConditionRenew
    ) {
        $this->_isCorrectQtyConditionRenew = $isCorrectQtyConditionRenew;
    }

    /**
     * @param InventorySalesIsCorrectQtyCondition $subject
     * @param \Closure $proceed
     * @param string $sku
     * @param int $stockId
     * @param float $requestedQty
     * @return ProductSalableResultInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws SkuIsNotAssignedToStockException
     */
    public function aroundExecute(
        InventorySalesIsCorrectQtyCondition $subject,
        \Closure $proceed,
        string $sku,
        int $stockId,
        float $requestedQty
    ) {
        $result = $this->_isCorrectQtyConditionRenew->execute($sku, $stockId, $requestedQty);
        if (!$result) {
            $result = $proceed($sku, $stockId, $requestedQty);
        }
        return $result;
    }
}