<?php

namespace MMS\Renew\Plugin;
use MMS\Renew\Helper\Config;

class MuleSoftDataHelperPlugin
{
    /**
     * @var Config
     */
    protected $_configHelper;

    public function __construct(
        Config $configHelper
    ) {
        $this->_configHelper = $configHelper;
    }
    public function aroundGetOrderDataForAdmin(
        \Ey\MuleSoft\Helper\Data $subject,
        \Closure $proceed,
        $order,
        $session
    ) {
        $result = $proceed($order, $session);
        if ($this->_configHelper->isEnabled() && $order->getData('is_quote_renew')) {
            $result['order']['isRenewal'] = true;
        }
        return $result;
    }

    public function aroundGetOrderData(
        \Ey\MuleSoft\Helper\Data $subject,
        \Closure $proceed,
        $order,
        $session
    ) {
        $result = $proceed($order, $session);
        if ($this->_configHelper->isEnabled() && $order->getData('is_quote_renew')) {
            $result['order']['isRenewal'] = true;
        }
        return $result;
    }
}
