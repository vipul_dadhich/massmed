<?php

namespace MMS\Renew\Plugin;

class BlockOnepageLinkPlugin
{
    /**
     * Url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * Renew checkout helper
     *
     * @var \MMS\Renew\Helper\Config
     */
    protected $_config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * Initialize dependencies.
     *
     * @param \MMS\Renew\Helper\Config $config
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \MMS\Renew\Helper\Config $config,
        \Magento\Framework\UrlInterface $url,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_config = $config;
        $this->_checkoutSession = $checkoutSession;
        $this->url = $url;
    }

    /**
     * Modify checkout url
     *
     * @param \Magento\Checkout\Block\Onepage\Link $subject
     * @param string $checkoutUrl
     * @return string
     */
    public function afterGetCheckoutUrl(
        \Magento\Checkout\Block\Onepage\Link $subject,
        $checkoutUrl
    ) {
        if (!$this->_config->isEnabled()) {
            return $checkoutUrl;
        }

        if ($this->getCheckoutSession()->getQuote()->getData('is_quote_renew')) {
            return $this->url->getUrl('renew/checkout');
        }

        return $checkoutUrl;
    }

    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}