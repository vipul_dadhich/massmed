<?php

namespace MMS\Renew\Plugin;

use Closure;
use Magento\Framework\App\RequestInterface;
use MMS\Renew\Helper\Config;
use MMS\Renew\Helper\PromoExtendToRenew;

/**
 * Class ConfigHelperPlugin
 * @package MMS\Renew\Plugin
 */
class ConfigHelperPlugin
{
    /**
     * @var PromoExtendToRenew
     */
    protected $_promoExtendToRenew;
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * ConfigHelperPlugin constructor.
     * @param PromoExtendToRenew $promoExtendToRenew
     * @param RequestInterface $request
     */
    public function __construct(
        PromoExtendToRenew $promoExtendToRenew,
        RequestInterface $request
    ) {
        $this->_promoExtendToRenew = $promoExtendToRenew;
        $this->_request = $request;
    }

    /**
     * @param Config $subject
     * @param Closure $proceed
     * @return bool|mixed|string
     */
    public function aroundGetFreeGiftProductSku(
        Config $subject,
        Closure $proceed
    ) {
        if ($promoSku = $this->getSkuFromPromotion()) {
            return $promoSku;
        }
        return $proceed();
    }

    /**
     * @return bool|string
     */
    private function getSkuFromPromotion()
    {
        if ($promo = $this->getPromo()) {
            return $this->_promoExtendToRenew->getSkuFromPromotion($promo);
        }
        return false;
    }

    /**
     * @return mixed
     */
    private function getPromo()
    {
        return $this->_request->getParam($this->_promoExtendToRenew->getPromoCodeUrlParam());
    }
}
