<?php

namespace MMS\Renew\Plugin\Quote\Model;

use Magento\Framework\Exception\LocalizedException;

class Quote
{
    /**
     * @var \Magento\Framework\DataObject\Factory
     */
    protected $objectFactory;
    /**
     * @var \MMS\Renew\Helper\Config
     */
    protected $_configHelper;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_product;

    /**
     * Quote constructor.
     * @param \Magento\Framework\DataObject\Factory $objectFactory
     * @param \MMS\Renew\Helper\Config $configHelper
     * @param \Magento\Catalog\Model\ProductFactory $product
     */
    public function __construct(
        \Magento\Framework\DataObject\Factory $objectFactory,
        \MMS\Renew\Helper\Config $configHelper,
        \Magento\Catalog\Model\ProductFactory $product
    ) {
        $this->objectFactory = $objectFactory;
        $this->_configHelper = $configHelper;
        $this->_product = $product;
    }

    /**
     * @param \Magento\Quote\Model\Quote $subject
     * @param \Magento\Catalog\Model\Product $product
     * @param null $request
     * @param string $processMode
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeAddProduct(
        \Magento\Quote\Model\Quote $subject,
        \Magento\Catalog\Model\Product $product,
        $request = null,
        $processMode = \Magento\Catalog\Model\Product\Type\AbstractType::PROCESS_MODE_FULL
    ) {
        if ($request === null) {
            $request = 1;
        }
        if (is_numeric($request)) {
            $request = $this->objectFactory->create(['qty' => $request]);
        }
        if (!$request instanceof \Magento\Framework\DataObject) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('We found an invalid request for adding product to quote.')
            );
        }

        if (!$request->getOptions()) {
            $options = [];
            foreach ($product->getOptions() as $o) {
                foreach ($o->getValues() as $value) {
                    if ($value->getData('is_default')) {
                        $options[$value['option_id']] = $value['option_type_id'];
                    }
                }
            }
            if (count($options) > 0) {
                $request->setOptions($options);
            }
        }


        return [$product, $request, $processMode];
    }

    /**
     * Remove Free Gift item from quote when item is removed from quote.
     *
     * @param \Magento\Quote\Model\Quote $subject
     * @param mixed $itemId
     *
     * @return array
     * @throws LocalizedException
     */
    public function beforeRemoveItem(\Magento\Quote\Model\Quote $subject, $itemId): array
    {
        if ($subject->getIsActive() && $subject->hasItems()) {
            $freeItems = [];
            foreach ($subject->getAllVisibleItems() as $quoteItem) {
                if ($quoteItem && $quoteItem->getItemId() != $itemId) {
                    $itemPrice = $quoteItem->getPrice();
                    if ($customPrice = $quoteItem->getCustomPrice()) {
                        $itemPrice = $customPrice;
                    }
                    if ($itemPrice == 0) {
                        $freeItems[] = $quoteItem;
                    }
                }
            }

            if (!empty($freeItems)) {
                foreach ($freeItems as $freeItem) {
                    if ($freeItem && $freeItem->getItemId() != $itemId) {
                        $freeItem->isDeleted(true);
                    }
                }
            }
        }

        /*if ($subject->getData('is_quote_renew')) {
            $item = $this->removeFreeGiftItemToCart($subject);
            if ($item && $item->getItemId() != $itemId) {
                $item->isDeleted(true);
            }
        }*/

        return [$itemId];
    }

    /**
     * remove free gift item to renew quote
     * @param \Magento\Quote\Model\Quote $quote
     * @return bool|\Magento\Quote\Model\Quote\Item
     */
    protected function removeFreeGiftItemToCart(\Magento\Quote\Model\Quote $quote)
    {
        if ($this->_configHelper->isFreeGiftEnabled()) {
            $productSku = $this->_configHelper->getFreeGiftProductSku();
            if ($productSku) {
                $product = $this->_product->create()->loadByAttribute('sku', $productSku);
                if ($product && $product->getId()) {
                    $product = $this->_product->create()->load($product->getId());
                    try {
                        return $quote->getItemByProduct($product);
                    } catch (LocalizedException $e) {
                        return false;
                    }
                }
            }
        }
        return false;
    }
}
