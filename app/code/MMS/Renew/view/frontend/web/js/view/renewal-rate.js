define(
    [
        'jquery',
        'underscore',
        'uiComponent',
        'knockout',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'MMS_Renew/js/model/renewal-rates',
        'MMS_Renew/js/action/update-terms-option'
    ],
    function ($, _, Component, ko, quote, priceUtils, renewalRatesModel, updateTermsAction) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'MMS_Renew/view/renewal-rate-block'
            },
            isVisible: ko.observable(false),
            preSelectedOldValue: null,
            professionalCat: window.checkoutConfig.renewalRatesProfCatValue,
            radioSelectedOptionValue: ko.observable(false),
            renewalRatesGroups: renewalRatesModel.renewalRatesGroups,

            initialize: function() {
                var self = this;
                this._super();
                self.renewalRatesGroups([]);
                if (self.getRenewalRates().length > 0) {
                    _.each(self.getRenewalRates(), function (options, i) {
                        if (options.options) {
                            _.each(options.options, function (item, i) {
                                let profCatCondition = true;
                                if (self.isProfessionalCat()) {
                                    profCatCondition = (self.getProfessionalCat() == item.prof_cat_value);
                                }

                                if (profCatCondition && item.is_default == "1") {
                                    self.preSelectedOldValue = item.option_type_id;
                                    let selectedVal = item.option_type_id;
                                    if (self.isProfessionalCat()) {
                                        selectedVal = self.getProfessionalCat() + '_' + selectedVal;
                                    }
                                    self.radioSelectedOptionValue(selectedVal);
                                }
                                self.renewalRatesGroups.push(item);
                            });
                        }
                    });
                }

                /*self.radioSelectedOptionValue.subscribe(function (value) {
                    updateTermsAction(value);
                });*/
            },

            setRenewalRatesGroups: function () {
                return this.renewalRatesGroups;
            },

            /**
             * @return {*|Boolean}
             */
            isActive: function () {
                if (window.checkoutConfig.renewalRatesShow && this.renewalRatesGroups().length > 0) {
                    this.isVisible(true);
                    return true;
                }
                return false;
            },

            /**
             * @return {*|Boolean}
             */
            isProfessionalCat: function () {
                if (window.checkoutConfig.renewalRatesProfCatValid !== undefined
                    && window.checkoutConfig.renewalRatesProfCatValid != '') {
                    return true;
                }
                return false;
            },

            /**
             * @return {*|String}
             */
            getProfessionalCat: function () {
                if (this.professionalCat !== undefined) {
                    return this.professionalCat;
                }
                return '';
            },

            /**
             * @return {*}
             */
            getRenewalRates: function () {
                return window.checkoutConfig.renewalRates;
            },

            getRenewalRatesItems: function () {
                var self = this;
                if (window.checkoutConfig.renewalRates) {
                    _.each(window.checkoutConfig.renewalRates, function (options, i) {
                        return self.getRenewalRatesItem(options.options);
                    });
                }
            },

            getRenewalRatesItem: function (options) {
                return _.map(options, function (item, id) {
                    return item;
                });
            },

            getId: function (item) {
                return 'option_' + item['option_id'] + '_' + item['option_type_id'];
            },

            getName: function (item) {
                return 'option[' + item['option_id'] + ']';
            },

            getTitle: function (item) {
                return item['default_title'];
            },

            getProfCatTitle: function (item) {
                if (item['prof_cat_label'] === undefined) {
                    return '';
                }
                return item['prof_cat_label'] + ' Rate';
            },

            getValue: function (item) {
                if (this.isProfessionalCat()) {
                    return item.prof_cat_value + '_' + item.option_type_id;
                }
                return item.option_type_id;
            },

            getPrice: function (item) {
                let price = parseFloat(item.quote_item_price) + parseFloat(item.price);
                if (item.default_custom_price !== undefined) {
                    price = parseFloat(item.default_custom_price);
                }
                return this.getFormattedPrice(price);
            },

            /**
             * @param {*} price
             * @return {*|String}
             */
            getFormattedPrice: function (price) {
                //todo add format data
                return priceUtils.formatPrice(price, quote.getPriceFormat());
            },

            setRenewalRates: function (itemData) {
                this.preSelectedOldValue = itemData.option_type_id;
                let selectedVal = itemData.option_type_id;
                if (this.isProfessionalCat()) {
                    selectedVal = this.getProfessionalCat() + '_' + selectedVal;
                }
                this.radioSelectedOptionValue(selectedVal);
                updateTermsAction(itemData);
                return true;
            }

            /*isChecked: function (item) {
                console.log(item.option_type_id);
                this.selectedOptionId(item.option_type_id);
                return item.option_type_id;
            }*/

            /*isChecked: function (item) {
                return true;
                console.log(item['is_default'] == 1);
                return (item['is_default'] == 1) ? true : false;
            }*/
        });
    }
);
