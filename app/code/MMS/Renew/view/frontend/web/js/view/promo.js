define([
    'jquery',
    'underscore',
    'uiComponent',
    'knockout'
], function ($, _, Component, ko) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'MMS_Renew/view/promo',
            viewPosition: null
        },
        isVisible: ko.observable(false),

        initialize: function() {
            this._super();
        },

        isActive: function () {
            if (this.getRenewalBlocks()) {
                if (this.isPositionAvailable() && this.getPromoHtml() !== '' && this.getPromoHtml() !== null) {
                    this.isVisible(true);
                    return true;
                }
            }
            return false;
        },

        getRenewalBlocks: function () {
            if (window.checkoutConfig.renewalCMSBlock !== undefined) {
                return window.checkoutConfig.renewalCMSBlock;
            }
            return false;
        },

        getPosition: function () {
            return this.viewPosition;
        },

        getPromoHtml: function () {
            let self = this;
            let promoHtml = _.filter(this.getRenewalBlocks(), function (item, position) {
                if (self.getPosition() === position && _.has(item, 'enable') && _.has(item, 'html')) {
                    return item.enable === true;
                }
                return false;
            }).pop();
            return _.isEmpty(promoHtml) ? '' : promoHtml.html;
        },

        isPositionAvailable: function () {
            let self = this;
            let config = _.filter(this.getRenewalBlocks(), function (item, position) {
                if (self.getPosition() === position && _.has(item, 'enable') && _.has(item, 'html')) {
                    return item.enable === true;
                }
                return false;
            });
            return !_.isEmpty(config);
        }
    });
});
