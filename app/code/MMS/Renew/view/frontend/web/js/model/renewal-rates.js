define([
    'ko'
], function (ko) {
    'use strict';

    return {
        renewalRatesGroups: ko.observableArray([])
    };
});
