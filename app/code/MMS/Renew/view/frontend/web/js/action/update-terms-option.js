define([
    'jquery',
    'underscore',
    'Magento_Checkout/js/model/url-builder',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/customer-data',
    'mage/storage',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/action/get-totals'
], function (
    $,
    _,
    urlBuilder,
    customer,
    customerData,
    storage,
    quote,
    fullScreenLoader,
    getTotalsAction
) {
    'use strict';
    return function (value) {
        var url, urlParams, serviceUrl;
        if (customer.isLoggedIn()) {
            url = '/carts/mine/update-terms-option';
            urlParams = {};
        } else {
            url = '/guest-carts/:cartId/update-terms-option';
            urlParams = {
                cartId: quote.getQuoteId()
            };
        }

        var payload = {
            cartId: quote.getQuoteId(),
            quoteItemId: value.quote_item_id,
            optionId: value.option_id,
            selectedOption: value.option_type_id,
            selectedProfessionalCat: value.prof_cat_value
        };
        serviceUrl = urlBuilder.createUrl(url, urlParams);

        fullScreenLoader.startLoader();
        return storage.post(
            serviceUrl,
            JSON.stringify(payload)
        ).done(
            function (response) {
                customerData.reload(['cart'], true);
                var deferred = $.Deferred();
                getTotalsAction([], deferred);
                fullScreenLoader.stopLoader();
            }
        ).fail(
            function (response) {
                fullScreenLoader.stopLoader();
            }
        );
    };
});