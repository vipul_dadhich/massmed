require([
    'jquery',
    'Magento_Ui/js/lib/view/utils/async',
    'domReady!'
], function ($) {
    'use strict';

    $.async('.opc-progress-bar li:first-child span', function (element) {
        $(element).html("Review & Make Payment");
    });

    $.async('#md_firstdata_save_card', function (element) {
        $(element).prop('checked', false);
    });

    $.async('#billing-save-in-address-book-shared', function (element) {
        $(element).prop('checked', false);
    });
});
