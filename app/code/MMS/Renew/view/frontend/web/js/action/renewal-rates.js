define([
    'jquery',
    'Magento_Checkout/js/model/url-builder',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/full-screen-loader',
    'mage/storage',
    'MMS_Renew/js/model/renewal-rates'
], function (
    $,
    urlBuilder,
    customer,
    fullScreenLoader,
    storage,
    renewalRatesModel
) {
    'use strict';

    return function () {
        let url, urlParams, serviceUrl;
        if (customer.isLoggedIn()) {
            url = '/carts/mine/get-renewal-terms';
            urlParams = {};
            serviceUrl = urlBuilder.createUrl(url, urlParams);
            fullScreenLoader.startLoader();
            return storage.get(
                serviceUrl,
                false
            ).done(
                function (response) {
                    if (response) {
                        renewalRatesModel.renewalRatesGroups([])
                        if (response.renewal_rates.length > 0) {
                            _.each(response.renewal_rates, function (options, i) {
                                if (options.options) {
                                    _.each(options.options, function (item, i) {
                                        renewalRatesModel.renewalRatesGroups.push(item);
                                    });
                                }
                            });
                        }
                    }
                    fullScreenLoader.stopLoader();
                }
            ).fail(
                function (response) {
                    fullScreenLoader.stopLoader();
                }
            );
        }
    };
});
