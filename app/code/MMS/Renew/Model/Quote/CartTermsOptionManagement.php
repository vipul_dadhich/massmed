<?php

namespace MMS\Renew\Model\Quote;

use Exception;
use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Framework\DataObject;
use Magento\Framework\Escaper;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Framework\ObjectManagerInterface;
use MMS\Renew\Api\Data\RenewalRatesInterface;
use MMS\Renew\Api\Quote\CartTermsOptionManagementInterface;
use MMS\Renew\Helper\Config;
use MMS\Renew\Model\RenewalRatesRepository;
use Zend_Filter_LocalizedToNormalized;

/**
 * Class CartTermsOptionManagement
 * @package MMS\Renew\Model\Quote
 */
class CartTermsOptionManagement implements CartTermsOptionManagementInterface
{
    /**
     * @var CustomerCart
     */
    protected $cart;

    /**
     * @var ManagerInterface
     */
    protected $_eventManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;
    /**
     * @var Config
     */
    protected $_config;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;
    /**
     * @var RenewalRatesInterface
     */
    protected $_renewalRates;
    /**
     * @var RenewalRatesRepository
     */
    protected $_renewalRatesRepository;

    /**
     * GuestCartTermsOptionManagement constructor.
     *
     * @param CustomerCart $cart
     * @param ManagerInterface $eventManager
     * @param ObjectManagerInterface $objectManager
     * @param Config $config
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param ProductFactory $productFactory
     * @param RenewalRatesRepository $renewalRatesRepository
     */
    public function __construct(
        CustomerCart $cart,
        ManagerInterface $eventManager,
        ObjectManagerInterface $objectManager,
        Config $config,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        ProductFactory $productFactory,
        RenewalRatesInterface $renewalRates,
        RenewalRatesRepository $renewalRatesRepository,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->cart = $cart;
        $this->_eventManager = $eventManager;
        $this->_objectManager = $objectManager;
        $this->_config = $config;
        $this->messageManager = $messageManager;
        $this->_productFactory = $productFactory;
        $this->_renewalRates = $renewalRates;
        $this->_renewalRatesRepository = $renewalRatesRepository;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * Handle selected delivery option.
     *
     * @param int $cartId
     * @param int $quoteItemId
     * @param int $optionId
     * @param int $selectedOption
     * @param string $selectedProfessionalCat
     * @return void
     */
    public function save($cartId, $quoteItemId, $optionId, $selectedOption, $selectedProfessionalCat)
    {
        try {
            $params = [];
            $params['qty'] = 1;
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get(
                        ResolverInterface::class
                    )->getLocale()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }
            $params['options'] = [$optionId => $selectedOption];

            $freeItemCount = 0;
            $freeGiftEnabled = $this->_config->isFreeGiftEnabled();
            $freeGiftSku = $this->_config->getFreeGiftProductSku();
            $items = $this->cart->getQuote()->getAllVisibleItems();
            foreach($items as $item) {
                if ($freeGiftEnabled && $item->getSku() == $freeGiftSku) {
                    $freeItemCount++;
                } else {
                    $quoteItemId = $item->getItemId();
                }
            }
            if ((count($items) - $freeItemCount) != 1) {
                throw new LocalizedException(
                    __("Request not allowed.")
                );
            }

            $quoteItem = $this->cart->getQuote()->getItemById($quoteItemId);
            if (!$quoteItem) {
                throw new LocalizedException(
                    __("The quote item isn't found. Verify the item and try again.")
                );
            }
            $item = $this->cart->updateItem($quoteItemId, new DataObject($params));
            if (is_string($item)) {
                throw new LocalizedException(__($item));
            }
            if ($item->getHasError()) {
                throw new LocalizedException(__($item->getMessage()));
            }

            $items = $this->cart->getQuote()->getAllVisibleItems();

            if(count($items) == 1){
                $this->addFreeGiftItemToCart();
            }

            if ($selectedProfessionalCat) {
                $this->_checkoutSession->setProfessionalCatagory($selectedProfessionalCat);
            }
            $this->cart->save();

        } catch (LocalizedException $e) {
            
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('We can\'t update the item right now.'));
        }
    }

    /**
     * Add free gift item to renew quote
     *
     * @return bool|\Magento\Quote\Model\Quote\Item|string|null
     * @throws LocalizedException
     */
    protected function addFreeGiftItemToCart()
    {
        $currentQuote = $this->cart->getQuote();

        if ($this->_config->isFreeGiftEnabled()) {
            $productSku = $this->_config->getFreeGiftProductSku();
            if ($productSku) {
                $product = $this->_productFactory->create()->loadByAttribute('sku', $productSku);
                if ($product && $product->getId()) {
                    $product = $this->_productFactory->create()->load($product->getId());
                    try {
                        return $currentQuote->addProduct($product, 1);
                    } catch (LocalizedException $e) {
                        throw new LocalizedException(__($e->getMessage()));
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param int $cartId
     * @return RenewalRatesInterface|null
     */
    public function getRenewalTerms(int $cartId)
    {
        $renewalRates = $this->_renewalRatesRepository->getRenewalRatesList();
        return $this->_renewalRates->setData([
            'renewal_rates' => $renewalRates
        ]);
    }
}
