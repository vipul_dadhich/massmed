<?php
namespace MMS\Renew\Model\Quote;

use Exception;
use Magento\Framework\DataObject;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Locale\ResolverInterface;
use MMS\Renew\Api\Quote\GuestCartTermsOptionManagementInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use MMS\Renew\Helper\Config;
use Zend_Filter_LocalizedToNormalized;

class GuestCartTermsOptionManagement implements GuestCartTermsOptionManagementInterface
{
    /**
     * @var CustomerCart
     */
    protected $cart;

    /**
     * @var ManagerInterface
     */
    protected $_eventManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;
    /**
     * @var Config
     */
    protected $_config;

    /**
     * GuestCartTermsOptionManagement constructor.
     *
     * @param CustomerCart $cart
     * @param ManagerInterface $eventManager
     * @param ObjectManagerInterface $objectManager
     * @param Config $config
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        CustomerCart $cart,
        ManagerInterface $eventManager,
        ObjectManagerInterface $objectManager,
        Config $config,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->cart = $cart;
        $this->_eventManager = $eventManager;
        $this->_objectManager = $objectManager;
        $this->_config = $config;
        $this->messageManager = $messageManager;
    }

    /**
     * Handle selected delivery option.
     *
     * @param int $cartId
     * @param int $quoteItemId
     * @param int $optionId
     * @param int $selectedOption
     * @param string $selectedProfessionalCat
     * @return void
     * @throws LocalizedException
     */
    public function save($cartId, $quoteItemId, $optionId, $selectedOption, $selectedProfessionalCat)
    {
        try {
            $params = [];
            $params['qty'] = 1;
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get(
                        ResolverInterface::class
                    )->getLocale()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }
            $params['options'] = [$optionId => $selectedOption];

            $freeItemCount = 0;
            $freeGiftEnabled = $this->_config->isFreeGiftEnabled();
            $freeGiftSku = $this->_config->getFreeGiftProductSku();
            $items = $this->cart->getQuote()->getAllVisibleItems();
            foreach($items as $item) {
                if ($freeGiftEnabled && $item->getSku() == $freeGiftSku) {
                    $freeItemCount++;
                } else {
                    $quoteItemId = $item->getItemId();
                }
            }
            if ((count($items) - $freeItemCount) != 1) {
                throw new LocalizedException(
                    __("Request not allowed.")
                );
            }

            $quoteItem = $this->cart->getQuote()->getItemById($quoteItemId);
            if (!$quoteItem) {
                throw new LocalizedException(
                    __("The quote item isn't found. Verify the item and try again.")
                );
            }
            $item = $this->cart->updateItem($quoteItemId, new DataObject($params));
            if (is_string($item)) {
                throw new LocalizedException(__($item));
            }
            if ($item->getHasError()) {
                throw new LocalizedException(__($item->getMessage()));
            }
            
            if ($selectedProfessionalCat) {
                $this->cart->getQuote()->setProfessionalCatagory($selectedProfessionalCat);
            }
            $this->cart->save();

        } catch (LocalizedException $e) {
            
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('We can\'t update the item right now.'));
        }
    }
}
