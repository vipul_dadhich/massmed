<?php

namespace MMS\Renew\Model;

use Magento\Catalog\Model\Product;
use Magento\Checkout\Model\Session;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Quote\Api\CartItemRepositoryInterface as QuoteItemRepository;
use MMS\Customer\Model\Source\ProfessionalCategory;
use MMS\Renew\Helper\Config;

/**
 * Class RenewalRatesRepository
 * @package MMS\Renew\Model
 */
class RenewalRatesRepository
{
    protected $professionalCategory = ['RES', 'STU'];

    /**
     * @var Session
     */
    protected $_checkoutSession;
    /**
     * @var QuoteItemRepository
     */
    private $_quoteItemRepository;
    /**
     * @var Product
     */
    protected $_product;
    /**
     * @var CustomerSession
     */
    protected $_customerSession;
    /**
     * @var ProfessionalCategory
     */
    protected $_professionalCategorySource;

    /**
     * @var Config
     */
    protected $_config;

    /**
     * RenewalRatesRepository constructor.
     * @param Session $checkoutSession
     * @param QuoteItemRepository $quoteItemRepository
     * @param Product $product
     * @param CustomerSession $customerSession
     * @param ProfessionalCategory $professionalCategorySource
     */
    public function __construct(
        Session $checkoutSession,
        QuoteItemRepository $quoteItemRepository,
        Product $product,
        CustomerSession $customerSession,
        ProfessionalCategory $professionalCategorySource,
        Config $config
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_quoteItemRepository = $quoteItemRepository;
        $this->_product = $product;
        $this->_customerSession = $customerSession;
        $this->_professionalCategorySource = $professionalCategorySource;
        $this->_config = $config;
    }

    /**
     * @param null $items
     * @return array
     */
    public function getRenewalRatesList($items = null): array
    {
        $renewalRates = [];
        if (!$items) {
            $items = $this->getQuoteItemData();
        }

        if (count($items) > 0) {
            $isProfessionalCatValid = false;
            $customerProfessionalCategory = $this->getProfessionalCategory();
            $professionalCatOptions = $this->_professionalCategorySource->getAllOptions();

            /** @var $quote \Magento\Quote\Model\Quote */
            $quote = $this->_checkoutSession->getQuote();

            foreach($items as $item) {
                $buyRequest = [];
                if ($this->_config->isBrandNejm($item['sku']) && in_array($customerProfessionalCategory, $this->professionalCategory)) {
                    $isProfessionalCatValid = true;
                    $quoteItem = $quote->getItemById($item['item_id']);
                    $option = $quoteItem->getOptionByCode('info_buyRequest');
                    if ($option) {
                        $buyRequest = \json_decode($option->getValue(), true);
                        if (is_null($buyRequest)) {
                            $buyRequest = \unserialize($option->getValue());
                        }
                    }
                }

                $product = $this->_product->load($item['product_id']);
                if ($product->getOptions()) {
                    $isProfessionalValid = false;
                    foreach($product->getOptions() as $option) {
                        $optionData = [];
                        foreach ($option->getValues() as $value) {
                            $profCatLabel = '';
                            $profCatValue = '';
                            if ($isProfessionalCatValid && !empty($buyRequest)
                                && isset($buyRequest['options'][$option->getOptionId()])
                                && $buyRequest['options'][$option->getOptionId()] == $value->getData('option_type_id')) {
                                foreach ($professionalCatOptions as $proCat) {
                                    if ($proCat['value'] == $customerProfessionalCategory) {
                                        $profCatLabel = (string)$proCat['label'];
                                        $profCatValue = $proCat['value'];
                                    }
                                }
                                $isProfessionalValid = true;
                            }
                            $optionData[] = array_merge(
                                $value->getData(),
                                ['prof_cat_label' => $profCatLabel],
                                ['prof_cat_value' => $profCatValue],
                                ['quote_item_id' => $item['item_id']],
                                ['quote_options' => $item['options'] ?? []],
                                ['quote_item_price' => $product->getFinalPrice()],
                                ['quote_item_base_price' => $product->getPrice()],
                                ['quote_item_custom_price' => $product->getCustomPrice()]
                            );
                            if ($isProfessionalValid) {
                                $profCatLabel = ($customerProfessionalCategory == 'STU') ? 'Resident' : 'Physician';
                                $profCatValue = ($customerProfessionalCategory == 'STU') ? 'RES' : 'PHY';
                                $clonedArray = $optionData[0];
                                $clonedArray['prof_cat_label'] = $profCatLabel;
                                $clonedArray['prof_cat_value'] = $profCatValue;
                                $clonedArray['is_default'] = '';
                                $optionData[] = $clonedArray;
                                break;
                            }
                        }
                        $this->resetDefaultOption($optionData);
                        $renewalRates[] = [
                            'option_id' => $option->getOptionId(),
                            'options' => $optionData
                        ];
                        if ($isProfessionalValid) {
                            break;
                        }
                    }
                }
            }
        }

        $renewalRatesLimit = $this->_config->getRenewOptionLimit();
        if (isset($renewalRates[0]['options']) && count($renewalRates[0]['options']) > $renewalRatesLimit) {
            $renewalRates[0]['options'] = array_slice($renewalRates[0]['options'], 0, $renewalRatesLimit);
        }

        return $renewalRates;
    }

    /**
     * @param $optionData
     */
    protected function resetDefaultOption(&$optionData)
    {
        if (count($optionData) > 0) {
            $defaultItemIndex = 0;
            foreach ($optionData as $key => $item) {
                if ($item['is_default'] == 1) {
                    $defaultItemIndex = $key;
                }
                $item['is_default'] = 0;
                if (isset($item['quote_options']) && count($item['quote_options']) > 0) {
                    foreach ($item['quote_options'] as $option) {
                        if ($option['value'] == $item['default_title']) {
                            $defaultItemIndex = $key;
                        }
                    }
                }
            }
            $optionData[$defaultItemIndex]['is_default'] = 1;
        }
    }

    /**
     * Retrieve quote item data
     *
     * @return array
     */
    private function getQuoteItemData()
    {
        $quoteItemData = [];
        $quoteId = $this->_checkoutSession->getQuote()->getId();
        if ($quoteId) {
            $quoteItems = $this->_quoteItemRepository->getList($quoteId);
            foreach ($quoteItems as $index => $quoteItem) {
                $quoteItemData[$index] = $quoteItem->toArray();
            }
        }
        return $quoteItemData;
    }

    /**
     * @return false|string
     */
    private function getProfessionalCategory()
    {
        $customer = $this->_customerSession->getCustomer();
        if ($customer) {
            return $customer->getProfessionalCategory();
        }

        return false;
    }
}
