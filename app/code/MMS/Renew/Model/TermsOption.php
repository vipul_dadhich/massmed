<?php

namespace MMS\Renew\Model;

use MMS\Renew\Api\Data\RenewTermsOptionInterface;

/**
 * Class TermsOption
 * @package MMS\Renew\Model
 */
class TermsOption extends \Magento\Catalog\Model\Product\Option implements RenewTermsOptionInterface
{
    /**#@+
     * Constants
     */
    const KEY_QUOTE_ITEM_ID = 'quote_item_id';
    const KEY_QUOTE_ITEM_PRICE = 'quote_item_price';
    const KEY_QUOTE_ITEM_BASE_PRICE = 'quote_item_base_price';
    const KEY_QUOTE_ITEM_CUSTOM_PRICE = 'quote_item_custom_price';
    const KEY_DEFAULT_CUSTOM_PRICE = 'default_custom_price';

    /**
     * @inheritDoc
     */
    public function getQuoteItemId(): int
    {
        return $this->getData(self::KEY_QUOTE_ITEM_ID);
    }

    /**
     * @inheritDoc
     */
    public function setQuoteItemId(int $quoteItemId): RenewTermsOptionInterface
    {
        $this->setData(self::KEY_QUOTE_ITEM_ID, $quoteItemId);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getQuoteItemPrice(): float
    {
        return $this->getData(self::KEY_QUOTE_ITEM_PRICE);
    }

    /**
     * @inheritDoc
     */
    public function setQuoteItemPrice(float $quoteItemPrice): RenewTermsOptionInterface
    {
        $this->setData(self::KEY_QUOTE_ITEM_PRICE, $quoteItemPrice);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getQuoteItemBasePrice(): float
    {
        return $this->getData(self::KEY_QUOTE_ITEM_BASE_PRICE);
    }

    /**
     * @inheritDoc
     */
    public function setQuoteItemBasePrice($quoteItemBasePrice): RenewTermsOptionInterface
    {
        $this->setData(self::KEY_QUOTE_ITEM_BASE_PRICE, $quoteItemBasePrice);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getQuoteItemCustomPrice(): float
    {
        return $this->getData(self::KEY_QUOTE_ITEM_CUSTOM_PRICE);
    }

    /**
     * @inheritDoc
     */
    public function setQuoteItemCustomPrice($quoteItemCustomPrice): RenewTermsOptionInterface
    {
        $this->setData(self::KEY_QUOTE_ITEM_CUSTOM_PRICE, $quoteItemCustomPrice);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDefaultCustomPrice(): float
    {
        return $this->getData(self::KEY_DEFAULT_CUSTOM_PRICE);
    }

    /**
     * @inheritDoc
     */
    public function setDefaultCustomPrice($defaultCustomPrice): RenewTermsOptionInterface
    {
        $this->setData(self::KEY_DEFAULT_CUSTOM_PRICE, $defaultCustomPrice);
        return $this;
    }
}
