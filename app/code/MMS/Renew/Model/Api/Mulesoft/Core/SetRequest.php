<?php

namespace MMS\Renew\Model\Api\Mulesoft\Core;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Psr7\ResponseFactory;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Framework\Webapi\Rest\Request;
use Ey\MuleSoft\Helper\Data;
use MMS\Logger\LoggerInterface;


Class SetRequest {


    /**
     * API request URL
     */
    const API_REQUEST_URI = '';
    /**
     * SetRequest constructor
     *
     * @param ClientFactory $clientFactory
     * TODO refactor the code to call from MuleSoft API.
     */

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    public function __construct(
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory,
        Data $helper,
        \MMS\Renew\Helper\Config $renewHelper,
        LoggerInterface $logger
    ) {
        $this->clientFactory = $clientFactory;
        $this->responseFactory = $responseFactory;
        $this->helper = $helper;
        $this->renewHelper = $renewHelper;
        $this->isDebugMode = $this->helper->getMuleSoftDebug();
        $this->_logger = $logger;
    }
    /**
     * Api end point
     **/
    private function getCheckEligibilityAPIURI(){
        return $this->renewHelper->getCheckEligibilityAPIURI();
    }
    /**
     * Api client ID
     **/
    private function getMulesoftClientId(){
        return $this->helper->getMuleSoftClientId();
    }
    /**
     * Api secret
     **/
    private function getMulesoftSecret(){
        return $this->helper->getMuleSoftClientSecret();
    }

    /**
     * Akamai Auth Token
     **/
    private function getAkamaiAuthToken(){
        return $this->renewHelper->getAkamaiAuthToken();
    }

    /**
     * Do API request with provided params
     *
     * @param string $apiMethod
     * @param string $requestMethod
     * @param array $params
     *
     * @return Response
     */
    public function makeRequest($apiMethod, $requestMethod, $params = []){
        return $this->doRequest($apiMethod, $params, $requestMethod);
    }
    /**
     * Do API request with provided params
     *
     * @param string $uriEndpoint
     * @param array $params
     * @param string $requestMethod
     *
     * @return Response
     */
    private function doRequest(
        string $uriEndpoint,
        array $params = [],
        string $requestMethod = Request::HTTP_METHOD_GET
    ) {
        /** @var Client $client */
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => $this->getCheckEligibilityAPIURI(),
            'headers' => [
                'client_id' => $this->getMulesoftClientId(),
                'client_secret' => $this->getMulesoftSecret(),
                'akamai-auth-token' => $this->getAkamaiAuthToken()
            ]
        ]]);


        try {
            $response = $client->request(
                $requestMethod,
                $uriEndpoint
            );

            $this->helper->getSendMuleSoftRequest($response,array('config' => array(
                'base_uri' => $this->getCheckEligibilityAPIURI(),
                'headers' => array(
                    'client_id' => $this->getMulesoftClientId(),
                    'client_secret' => $this->getMulesoftSecret(),
                    'akamai-auth-token' => $this->getAkamaiAuthToken()
                ))));

        } catch (GuzzleException $exception) {
            /** @var Response $response */
            $response = $this->responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);

            $this->helper->getSendMuleSoftRequestException($response,array('config' => array(
                'base_uri' => $this->getCheckEligibilityAPIURI(),
                'headers' => array(
                    'client_id' => $this->getMulesoftClientId(),
                    'client_secret' => $this->getMulesoftSecret(),
                    'akamai-auth-token' => $this->getAkamaiAuthToken()
                ))));
        }

        return $response;
    }
}
