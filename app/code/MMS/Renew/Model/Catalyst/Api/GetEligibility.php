<?php

namespace MMS\Renew\Model\Catalyst\Api;

use MMS\Renew\Model\Api\Mulesoft\Core\SetRequest;
use Ey\URLParams\Helper\Data;
use MMS\Renew\Helper\Data as DataHelper;

/**
 * Class setRequest
 */
class GetEligibility
{
    /**
     * API request endpoint
     */
    const API_REQUEST_ENDPOINT = 'v1/api/customers';
    /**
     * API request methodyt
     */
    const API_REQUEST_METHOD = 'GET';

    protected $_productFamily = null;

    /**
     * JSON helper
     *
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * getCustomer constructor
     *
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     * @param \Ey\MuleSoft\Helper\Endpoints $endpoints
     */
    public function __construct(
        SetRequest $request,
        Data $urlHelper,
        DataHelper $dataHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\App\RequestInterface $requestParam,
        \Ey\MuleSoft\Helper\Endpoints $endpoints
    ) {
        $this->request = $request;
        $this->urlHelper = $urlHelper;
        $this->jsonHelper = $jsonHelper;
        $this->dataHelper = $dataHelper;
        $this->requestParam = $requestParam;
        $this->endpoints = $endpoints;
    }

    /**
     * Fetch some data from API
     * @param $params array() or string
     */
    public function execute($ucid)
    {
       $response = $this->request->makeRequest( $this->getEndpointUri($ucid), static::API_REQUEST_METHOD, array() );
       if($response->getStatusCode() == 200){
            return $this->jsonHelper->jsonDecode($response->getBody()->getContents());
        }
        return false;
    }


     /**
      * get endpoint uri
      */
     private function getEndpointUri($ucid){
         return $this->getCustomerEndpoint().'/'.$ucid.'/purchase-eligibility?productFamily='.$this->getProductFamily();
     }
    /**
     * Get Customer API Endpoint
     * @return string
     */
    public function getCustomerEndpoint(){
        if($this->endpoints->getCustomerEndpoint()){
            return $this->endpoints->getCustomerEndpoint();
        }
        return self::API_REQUEST_ENDPOINT;
    }

     /**
     * Prepare the request for an API
     */
    private function prepareRequest($email){
         $params = array("purchaseEligibility" => $email);
         return json_encode($params);

    }

    /**
     * Get Product Family
     *
     * @return mixed|string|null
     */
    public function getProductFamily()
    {
        if ($this->_productFamily != null) {
            return $this->_productFamily;
        }

        if ($this->requestParam->getParam('product')) {
            return $this->requestParam->getParam('product');
        } else if($this->urlHelper->getCustomCookie('product')) {
            return $this->urlHelper->getCustomCookie('product');
        } else if($this->dataHelper->getDefaultProductFamily()) {
            return $this->dataHelper->getDefaultProductFamily();
        } else {
            return 'catalyst';
        }
    }

    /**
     * Set Product Family
     * @param $productFamily
     */
    public function setProductFamily($productFamily)
    {
        $this->_productFamily = $productFamily;
    }

    private function ApiErrorMessages(){
        $error = "Customer doesn't exists";
        //Enhance the Error API error messages.
        //Use the Response setting classes for it.
    }
}

