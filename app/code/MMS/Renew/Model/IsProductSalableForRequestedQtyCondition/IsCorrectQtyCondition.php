<?php

namespace MMS\Renew\Model\IsProductSalableForRequestedQtyCondition;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\InventoryConfigurationApi\Exception\SkuIsNotAssignedToStockException;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteFactory;
use MMS\Renew\Helper\Config;
use Magento\InventoryConfigurationApi\Api\GetStockItemConfigurationInterface;
use Magento\InventoryReservationsApi\Model\GetReservationsQuantityInterface;
use Magento\Framework\Math\Division as MathDivision;
use Magento\InventorySalesApi\Api\Data\ProductSalabilityErrorInterfaceFactory;
use Magento\InventorySalesApi\Api\Data\ProductSalableResultInterfaceFactory;
use Magento\InventorySalesApi\Api\Data\ProductSalableResultInterface;
use Magento\InventoryConfigurationApi\Api\Data\StockItemConfigurationInterface;
use Magento\Framework\Phrase;

/**
 * Class IsCorrectQtyCondition
 * @package MMS\Renew\Model\IsProductSalableForRequestedQtyCondition
 */
class IsCorrectQtyCondition
{
    protected $_quote = null;
    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;
    /**
     * @var Config
     */
    protected $_config;
    /**
     * @var GetStockItemConfigurationInterface
     */
    protected $getStockItemConfiguration;
    /**
     * @var GetReservationsQuantityInterface
     */
    protected $getReservationsQuantity;
    /**
     * @var MathDivision
     */
    protected $mathDivision;
    /**
     * @var ProductSalabilityErrorInterfaceFactory
     */
    protected $productSalabilityErrorFactory;
    /**
     * @var ProductSalableResultInterfaceFactory
     */
    protected $productSalableResultFactory;
    /**
     * @var QuoteFactory
     */
    protected $_quoteFactory;

    /**
     * IsCorrectQtyCondition constructor.
     * @param GetStockItemConfigurationInterface $getStockItemConfiguration
     * @param GetReservationsQuantityInterface $getReservationsQuantity
     * @param MathDivision $mathDivision
     * @param ProductSalabilityErrorInterfaceFactory $productSalabilityErrorFactory
     * @param ProductSalableResultInterfaceFactory $productSalableResultFactory
     * @param Config $config
     * @param CheckoutSession $session
     * @param QuoteFactory $quoteFactory
     */
    public function __construct(
        GetStockItemConfigurationInterface $getStockItemConfiguration,
        GetReservationsQuantityInterface $getReservationsQuantity,
        MathDivision $mathDivision,
        ProductSalabilityErrorInterfaceFactory $productSalabilityErrorFactory,
        ProductSalableResultInterfaceFactory $productSalableResultFactory,
        Config $config,
        CheckoutSession $session,
        QuoteFactory $quoteFactory
    ) {
        $this->getStockItemConfiguration = $getStockItemConfiguration;
        $this->getReservationsQuantity = $getReservationsQuantity;
        $this->mathDivision = $mathDivision;
        $this->productSalabilityErrorFactory = $productSalabilityErrorFactory;
        $this->productSalableResultFactory = $productSalableResultFactory;
        $this->_config = $config;
        $this->_checkoutSession = $session;
        $this->_quoteFactory = $quoteFactory;
    }

    /**
     * @param string $sku
     * @param int $stockId
     * @param float $requestedQty
     * @return bool|ProductSalableResultInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws SkuIsNotAssignedToStockException
     */
    public function execute(string $sku, int $stockId, float $requestedQty)
    {
        if ($this->_config->isEnabled() && $this->getQuote()->getData('is_quote_renew')) {
            $stockItemConfiguration = $this->getStockItemConfiguration->execute($sku, $stockId);
            if ($this->isMinSaleQuantityCheckFailed($stockItemConfiguration, $requestedQty)) {
                return $this->createErrorResult(
                    'is_correct_qty-min_sale_qty',
                    __(
                        'The fewest you may purchase is %1',
                        $stockItemConfiguration->getMinSaleQty()
                    )
                );
            }

            if ($this->isQuantityIncrementCheckFailed($stockItemConfiguration, $requestedQty)) {
                return $this->createErrorResult(
                    'is_correct_qty-qty_increment',
                    __(
                        'You can buy this product only in quantities of %1 at a time.',
                        $stockItemConfiguration->getQtyIncrements()
                    )
                );
            }

            if ($this->isDecimalQtyCheckFailed($stockItemConfiguration, $requestedQty)) {
                return $this->createErrorResult(
                    'is_correct_qty-is_qty_decimal',
                    __('You cannot use decimal quantity for this product.')
                );
            }

            return $this->productSalableResultFactory->create(['errors' => []]);
        }

        return false;
    }

    /**
     * Check if min sale condition is satisfied
     *
     * @param StockItemConfigurationInterface $stockItemConfiguration
     * @param float $requestedQty
     * @return bool
     */
    private function isMinSaleQuantityCheckFailed(
        StockItemConfigurationInterface $stockItemConfiguration,
        float $requestedQty
    ): bool {
        // Minimum Qty Allowed in Shopping Cart
        if ($stockItemConfiguration->getMinSaleQty() && $requestedQty < $stockItemConfiguration->getMinSaleQty()) {
            return true;
        }
        return false;
    }

    /**
     * Check if increment quantity condition is satisfied
     *
     * @param StockItemConfigurationInterface $stockItemConfiguration
     * @param float $requestedQty
     * @return bool
     */
    private function isQuantityIncrementCheckFailed(
        StockItemConfigurationInterface $stockItemConfiguration,
        float $requestedQty
    ): bool {
        // Qty Increments
        $qtyIncrements = $stockItemConfiguration->getQtyIncrements();
        if ($qtyIncrements !== (float)0 && $this->mathDivision->getExactDivision($requestedQty, $qtyIncrements) !== 0) {
            return true;
        }
        return false;
    }

    /**
     * Check if decimal quantity is valid
     *
     * @param StockItemConfigurationInterface $stockItemConfiguration
     * @param float $requestedQty
     * @return bool
     */
    private function isDecimalQtyCheckFailed(
        StockItemConfigurationInterface $stockItemConfiguration,
        float $requestedQty
    ): bool {
        return (!$stockItemConfiguration->isQtyDecimal() && (floor($requestedQty) !== $requestedQty));
    }

    /**
     * Create Error Result Object
     *
     * @param string $code
     * @param Phrase $message
     * @return ProductSalableResultInterface
     */
    private function createErrorResult(string $code, Phrase $message): ProductSalableResultInterface
    {
        $errors = [
            $this->productSalabilityErrorFactory->create([
                'code' => $code,
                'message' => $message
            ])
        ];
        return $this->productSalableResultFactory->create(['errors' => $errors]);
    }

    /**
     * @return CartInterface|Quote
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getQuote()
    {
        if (null === $this->_quote) {
            $this->_quote = $this->_quoteFactory->create()->load($this->getCheckoutSession()->getQuoteId());
        }
        return $this->_quote;
    }

    /**
     * Get frontend checkout session object
     *
     * @return CheckoutSession
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}