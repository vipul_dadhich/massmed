<?php

namespace MMS\Renew\Model;

use Magento\Framework\Model\AbstractExtensibleModel;
use MMS\Renew\Api\Data\ProductRenewOptionInterface;
use MMS\Renew\Api\Data\RenewTermsOptionInterface;

/**
 * Class ProductRenewOption
 * @package MMS\Renew\Model
 */
class ProductRenewOption extends AbstractExtensibleModel implements ProductRenewOptionInterface
{
    /**
     * @inheritDoc
     */
    public function getOptionId(): int
    {
        return $this->getData('option_id');
    }

    /**
     * @inheritDoc
     */
    public function setOptionId(int $optionId)
    {
        $this->setData('option_id', $optionId);
    }

    /**
     * Get all options of product
     *
     * @return RenewTermsOptionInterface[]|null
     */
    public function getOptions(): ?array
    {
        return $this->getData('options');
    }

    /**
     * Set product options
     *
     * @param RenewTermsOptionInterface[] $options
     * @return void
     */
    public function setOptions(array $options = null)
    {
        $this->setData('options', $options);
    }
}
