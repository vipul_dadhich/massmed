<?php

namespace MMS\Renew\Model;

use Magento\Framework\Model\AbstractExtensibleModel;

/**
 * Class RenewalRates
 * @package MMS\Renew\Model
 */
class RenewalRates extends AbstractExtensibleModel implements \MMS\Renew\Api\Data\RenewalRatesInterface
{
    /**
     * @inheritDoc
     */
    public function getRenewalRates(): ?array
    {
        return $this->getData('renewal_rates');
    }

    /**
     * @inheritDoc
     */
    public function setRenewalRates(array $options = null)
    {
        $this->setData('renewal_rates', $options);
    }
}
