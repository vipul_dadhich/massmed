<?php

namespace MMS\Renew\Model\Service;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

class CreateQuote implements \MMS\Renew\Api\CreateQuoteServiceInterface
{
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cartModel;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_productModel;
    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $_formKey;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $_cartRepository;
    /**
     * @var \Magento\Quote\Api\CartManagementInterface
     */
    protected $_cartManagement;
    /**
     * @var \Magento\Directory\Model\CurrencyFactory
     */
    protected $_currencyFactory;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepository;
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $_quoteFactory;
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;
    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    public $_request;

    /**
     * @var \Magento\Quote\Model\Quote
     */
    public $_currentQuote;
    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_product;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;
    /**
     * @var \MMS\Renew\Helper\Config
     */
    protected $_configHelper;

    protected $currentQuoteId = null;

    public function __construct(
        \Magento\Checkout\Model\Cart $cartModel,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepository,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        LoggerInterface $logger,
        \Magento\Catalog\Model\ProductFactory $product,
        ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \MMS\Renew\Helper\Config $configHelper
    ) {
        $this->_cartModel = $cartModel;
        $this->_productModel = $productModel;
        $this->_formKey = $formKey;
        $this->_checkoutSession = $checkoutSession;
        $this->_storeManager = $storeManager;
        $this->_cartRepository = $cartRepository;
        $this->_cartManagement = $cartManagement;
        $this->_currencyFactory = $currencyFactory;
        $this->_customerSession = $customerSession;
        $this->_customerRepository = $customerRepository;
        $this->_quoteFactory = $quoteFactory;
        $this->_jsonHelper = $jsonHelper;
        $this->_countryFactory = $countryFactory;
        $this->_logger = $logger;
        $this->_objectManager = $objectManager;
        $this->_productRepository = $productRepository;
        $this->_productFactory = $productFactory;
        $this->_product = $product;
        $this->_configHelper = $configHelper;
    }

    public function execute()
    {
        $errorMessages = false;
        try {
            $this->_currentQuote = null;

            /*$this->emptyCurrentCart();*/
            $this->initQuote();
            $this->setStoreToQuote();
            $this->setCurrencyToQuote();
            /*$this->setQuoteIsVirtual();*/
            $this->setCustomerNoteNotify();
            $this->addProductsToQuote();

            /*$this->saveCurrentQuote();
            $this->setShippingAddress();
            $this->setBillingAddress();*/

            $this->collectShippingRates();
            $this->cartSave();
            $this->collectTotalsAndSave();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $errorMessages = $e->getMessage();
        } catch (\Exception $ex) {
            $errorMessages = $ex->getMessage();
        }

        if ($errorMessages) {
            $this->_currentQuote = null;
            throw new \Magento\Framework\Exception\LocalizedException(__($errorMessages));
        }
    }

    /**
     * @param $request
     */
    public function setRequestData($request)
    {
        $this->_request = $request;
    }

    /**
     * @return mixed
     */
    public function getRequestData()
    {
        return $this->_request;
    }

    /**
     * @throws \Exception
     */
    protected function saveCurrentQuote()
    {
        $this->_currentQuote->save();
    }

    /**
     * empty existing quote for cart items
     */
    protected function emptyCurrentCart()
    {
        if ($this->_checkoutSession->getQuoteId()) {
            $this->_checkoutSession->resetCheckout();
            $this->_checkoutSession->clearQuote()->clearStorage();
        }
    }

    /**
     * Initialize the quote
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Exception
     */
    protected function initQuote()
    {
        $this->clearAllQuoteToInActive();

        $cart_id = $this->_cartManagement->createEmptyCartForCustomer($this->_customerSession->getCustomerId());
        $this->_currentQuote = $this->_cartRepository->get($cart_id);
        $this->_cartModel->setQuote($this->_currentQuote);

        $this->_currentQuote->setIsActive(1)->setReservedOrderId(null);
        $this->_currentQuote->setData('is_quote_renew', 1);
        $this->_currentQuote->save();
        $this->currentQuoteId = $this->_currentQuote->getId();
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function setStoreToQuote()
    {
        $store = $this->_storeManager->getStore();
        $this->_currentQuote->setStore($store);
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function assignCustomerToQuote()
    {
        $customerId = $this->_customerSession->getCustomer()->getId();
        $customer = $this->_customerRepository->getById($customerId);
        $this->_currentQuote->assignCustomer($customer);
    }

    /**
     * Set current to current quote
     */
    protected function setCurrencyToQuote()
    {
        /*$this->_storeManager->getStore()->getCurrentCurrency();*/
        $this->_currentQuote->setCurrency();
    }

    /**
     * set current Quote isVirtual
     */
    protected function setQuoteIsVirtual()
    {
        $this->_currentQuote->setIsVirtual(true);
    }

    /**
     * Set customer note notify to current quote
     */
    protected function setCustomerNoteNotify()
    {
        $this->_currentQuote->setCustomerNoteNotify(false);
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function addProductsToQuote()
    {
        $errorMessages = false;
        $productAdded = false;
        try {
            $request = $this->getRequestData();
            if (isset($request['products']) && count($request['products'])) {

                foreach ($request['products'] as $item) {
                    $product = $this->_product->create()->loadByAttribute('sku', $item['offerCode']);
                    if ($product && $product->getId()) {
                        $product = $this->_productFactory->create()->load($product->getId());
                        try {
                            $this->_currentQuote->addProduct($product, 1);
                            $productAdded = true;
                        } catch (LocalizedException $e) {
                            throw new LocalizedException(__($e->getMessage()));
                        }
                    }
                }
                if ($productAdded) {
                    $this->addFreeGiftItemToCart();
                }
            }

            $this->_cartModel->save();

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $errorMessages = $e->getMessage();
        } catch (\Exception $exception) {
            $errorMessages = $exception->getMessage();
        }

        if ($errorMessages) {
            throw new \Magento\Framework\Exception\LocalizedException(__($errorMessages));
        }
    }

    /**
     * Add free gift item to renew quote
     *
     * @return bool|\Magento\Quote\Model\Quote\Item|string|null
     * @throws LocalizedException
     */
    protected function addFreeGiftItemToCart()
    {
        if ($this->_configHelper->isFreeGiftEnabled()) {
            $productSku = $this->_configHelper->getFreeGiftProductSku();
            if ($productSku) {
                $product = $this->_product->create()->loadByAttribute('sku', $productSku);
                if ($product && $product->getId()) {
                    $product = $this->_productFactory->create()->load($product->getId());
                    try {
                        return $this->_currentQuote->addProduct($product, 1);
                    } catch (LocalizedException $e) {
                        throw new LocalizedException(__($e->getMessage()));
                    }
                }
            }
        }
        return false;
    }

    /**
     * Set Shipping Address to current quote, if quote is not virtual
     */
    protected function setShippingAddress()
    {
        if (!$this->_currentQuote->isVirtual()) {
            /** here shipping will be set for current quote */
            $shipAddress = [
                'firstname'             => 'firstname',
                'lastname'              => 'lastname',
                'street'                => 'street',
                'city'                  => 'city',
                'country'               => 'country',
                'country_id'            => 'country_id',
                'region'                => 'region',
                'postcode'              => 'postcode',
                'telephone'             => 'telephone',
                'fax'                   => 'fax',
                'company'               => 'company',
                'save_in_address_book'  => 'save_in_address_book',
            ];
            $this->_currentQuote->getShippingAddress()->addData($shipAddress);
        }
    }

    /**
     * Set Billing Address to current quote
     */
    protected function setBillingAddress()
    {
        $billAddress = [
            'firstname'             => 'firstname',
            'lastname'              => 'lastname',
            'street'                => 'street',
            'city'                  => 'city',
            'country'               => 'country',
            'country_id'            => 'country_id',
            'region'                => 'region',
            'postcode'              => 'postcode',
            'telephone'             => 'telephone',
            'fax'                   => 'fax',
            'company'               => 'company',
            'save_in_address_book'  => 'save_in_address_book',
        ];
        $this->_currentQuote->getBillingAddress()->addData($billAddress);
    }

    /**
     * Load shipping rates based on shipping address
     * and assign to current quote
     */
    protected function collectShippingRates()
    {
        if (!$this->_currentQuote->isVirtual()) {
            $shippingAddress = $this->_currentQuote->getShippingAddress();
            $shippingAddress->setCollectShippingRates(true)
                ->collectShippingRates();
        }
    }

    /**
     * Save current Quote to Cart Repository
     */
    protected function cartSave()
    {
        $this->_cartRepository->save($this->_currentQuote);
    }

    /**
     * @throws \Exception
     */
    protected function collectTotalsAndSave()
    {
        $this->_currentQuote->collectTotals();
        $this->saveCurrentQuote();
    }

    /**
     * Get checkout model
     *
     * @return \Magento\Checkout\Model\Session
     */
    protected function _getCheckout()
    {
        return $this->_objectManager->get(\Magento\Checkout\Model\Session::class);
    }

    /**
     * Delete all active quotes from customer quote collection
     */
    protected function deleteAllCustomerQuote()
    {
        $quoteCollection = $this->_quoteFactory->create()->getCollection();
        $quoteCollection->addFieldToFilter('customer_id', $this->_customerSession->getCustomerId());
        $quoteCollection->addFieldToFilter('is_active', 1);
        if ($quoteCollection->getSize() > 0) {
            foreach($quoteCollection as $quote) {
                try {
                    $quote->delete();
                } catch (\Exception $e) {
                }
            }
        }
    }

    /**
     * Clear all active quote from customer quote collection
     */
    protected function clearAllQuoteToInActive()
    {
        $collection = $this->getQuoteCollection();
        if ($collection->getSize() > 0) {
            foreach ($collection as $quote) {
                /** @var $quote \Magento\Quote\Model\Quote */
                try {
                    $quote->setIsActive(0)->save();
                } catch (\Exception $ex) {
                    /** no execution need */
                }
            }
        }
    }

    /**
     * @return \Magento\Framework\Data\Collection\AbstractDb|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection|null
     */
    public function getQuoteCollection()
    {
        $quoteCollection = $this->_quoteFactory->create()->getCollection();
        $quoteCollection->addFieldToFilter('customer_id', $this->_customerSession->getCustomerId());
        $quoteCollection->addFieldToFilter('is_active', 1);
        if ($this->currentQuoteId != null) {
            $quoteCollection->addFieldToFilter(
                'entity_id',
                ['nin' => [$this->currentQuoteId]]
            );
        }
        return $quoteCollection;
    }
}
