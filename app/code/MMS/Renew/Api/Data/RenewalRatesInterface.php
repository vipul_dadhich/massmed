<?php

namespace MMS\Renew\Api\Data;

/**
 * Interface RenewalRatesInterface
 * @package MMS\Renew\Api\Data
 */
interface RenewalRatesInterface
{
    /**
     * Set renewal terms
     *
     * @param \MMS\Renew\Api\Data\ProductRenewOptionInterface[] $options
     * @return void
     */
    public function setRenewalRates(array $options = null);

    /**
     * @return \MMS\Renew\Api\Data\ProductRenewOptionInterface[]|null
     */
    public function getRenewalRates(): ?array;
}
