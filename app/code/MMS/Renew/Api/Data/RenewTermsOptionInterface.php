<?php

namespace MMS\Renew\Api\Data;

/**
 * Interface RenewTermsOptionInterface
 * @package MMS\Renew\Api\Data
 */
interface RenewTermsOptionInterface extends \Magento\Catalog\Api\Data\ProductCustomOptionInterface
{
    /**
     * @return int
     */
    public function getQuoteItemId(): int;

    /**
     * @param int $quoteItemId
     * @return $this
     */
    public function setQuoteItemId(int $quoteItemId): RenewTermsOptionInterface;

    /**
     * @return float
     */
    public function getQuoteItemPrice(): float;

    /**
     * @param float $quoteItemPrice
     * @return $this
     */
    public function setQuoteItemPrice(float $quoteItemPrice): RenewTermsOptionInterface;

    /**
     * @return float
     */
    public function getQuoteItemBasePrice(): float;

    /**
     * @param float $quoteItemBasePrice
     * @return $this
     */
    public function setQuoteItemBasePrice(float $quoteItemBasePrice): RenewTermsOptionInterface;

    /**
     * @return float
     */
    public function getQuoteItemCustomPrice();

    /**
     * @param float $quoteItemCustomPrice
     * @return $this
     */
    public function setQuoteItemCustomPrice(float $quoteItemCustomPrice): RenewTermsOptionInterface;

    /**
     * @return float
     */
    public function getDefaultCustomPrice();

    /**
     * @param float $defaultCustomPrice
     * @return $this
     */
    public function setDefaultCustomPrice(float $defaultCustomPrice): RenewTermsOptionInterface;
}
