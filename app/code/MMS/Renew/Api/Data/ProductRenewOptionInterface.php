<?php

namespace MMS\Renew\Api\Data;


/**
 * Interface ProductRenewOptionInterface
 * @package MMS\Renew\Api\Data
 */
interface ProductRenewOptionInterface
{
    /**
     * @return int
     */
    public function getOptionId(): int;

    /**
     * @param int $optionId
     * @return void
     */
    public function setOptionId(int $optionId);

    /**
     * Get list of product options
     *
     * @return \MMS\Renew\Api\Data\RenewTermsOptionInterface[]|null
     */
    public function getOptions(): ?array;

    /**
     * Set list of product options
     *
     * @param \MMS\Renew\Api\Data\RenewTermsOptionInterface[] $options
     * @return void
     */
    public function setOptions(array $options = null);
}
