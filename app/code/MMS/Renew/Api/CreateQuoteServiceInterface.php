<?php

namespace MMS\Renew\Api;

/**
 * Create Quote by renew api service
 */
interface CreateQuoteServiceInterface
{
    /**
     * @return mixed
     */
    public function execute();

    /**
     * @param $request
     * @return mixed
     */
    public function setRequestData($request);

    /**
     * @return mixed
     */
    public function getRequestData();
}