<?php
namespace MMS\Renew\Api\Quote;

use Magento\Catalog\Api\Data\ProductInterface;

interface CartTermsOptionManagementInterface
{
    /**
     * Handle selected custom option.
     *
     * @param int $cartId The shopping cart ID.
     * @param int $quoteItemId
     * @param int $optionId
     * @param int $selectedOption
     * @param string $selectedProfessionalCat
     * @return void
     */
    public function save($cartId, $quoteItemId, $optionId, $selectedOption, $selectedProfessionalCat);

    /**
     * Get the list of renewal terms options for a quote
     *
     * @param int $cartId
     * @return \MMS\Renew\Api\Data\RenewalRatesInterface|null
     */
    public function getRenewalTerms(int $cartId);
}
