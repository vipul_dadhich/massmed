<?php
namespace MMS\Renew\Api\Quote;

interface GuestCartTermsOptionManagementInterface
{
    /**
     * Handle selected custom option.
     *
     * @param string $cartId The shopping cart ID.
     * @param int $quoteItemId
     * @param int $optionId
     * @param int $selectedOption
     * @param string $selectedProfessionalCat
     * @return void
     */
    public function save($cartId, $quoteItemId, $optionId, $selectedOption, $selectedProfessionalCat);
}
