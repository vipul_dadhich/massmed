<?php

namespace MMS\Renew\Helper;

use Magento\Framework\App\Helper\Context;

/**
 * Class PromoExtendToRenew
 * @package MMS\Renew\Helper
 */
class PromoExtendToRenew extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \MMS\Promotions\Helper\Data
     */
    protected $_promotionHelper;

    /**
     * PromoExtendToRenew constructor.
     * @param Context $context
     * @param \MMS\Promotions\Helper\Data $promotionHelper
     */
    public function __construct(
        Context $context,
        \MMS\Promotions\Helper\Data $promotionHelper
    ) {
        $this->_promotionHelper = $promotionHelper;
        parent::__construct($context);
    }

    /**
     * @param $promoCode
     * @return bool|string
     */
    public function getSkuFromPromotion($promoCode)
    {
        if ($this->_promotionHelper->isModuleEnabled()) {
            $promotionData = $this->_promotionHelper->getPromotionByPromoCode($promoCode);
            if (!empty($promotionData)) {
                if ($promotionData->getFreeGiftSku()) {
                    return $promotionData->getFreeGiftSku();
                }
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getPromoCodeUrlParam()
    {
        return $this->scopeConfig->getValue(
            'promocode/settings/url_parameter',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
