<?php

namespace MMS\Renew\Helper;

use Ey\PromoCode\Helper\Data as PromoHelper;
use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\View\LayoutInterface;
use Magento\Store\Model\ScopeInterface;
use Ey\MuleSoft\Model\Catalyst\Api\GetCustomerData;;
use Ey\URLParams\Helper\Data as urlHelper;
use Magento\Framework\App\RequestInterface;
use MMS\Promotions\Helper\Data as PromotionHelper;


/**
 * Class Config
 * @package MMS\Renew\Helper
 */
class Config extends AbstractHelper
{
    const ENABLE = 'renew/general/enable';
    const XML_PATH_GENERAL_DEFAULT_OPTION = 'renew/general/default_option';
    const XML_PATH_GENERAL_ALLOWED_PAYMENTS = 'renew/general/allow_payments';
    const XML_PATH_GENERAL_ENABLED_TOP_BLOCK = 'renew/general/show_top_block';
    const XML_PATH_GENERAL_TOP_BLOCK = 'renew/general/top_block';
    const XML_PATH_GENERAL_ENABLED_BOTTOM_BLOCK = 'renew/general/show_bottom_block';
    const XML_PATH_GENERAL_BOTTOM_BLOCK = 'renew/general/bottom_block';
    const XML_PATH_GENERAL_IS_RENEW_SUCCESS = 'renew/general/is_renew_success';
    const XML_PATH_GENERAL_OPTION_LIMIT = 'renew/general/option_limit';
    const XML_PATH_GENERAL_SKIP_ELIGIBILTY = 'renew/general/skip_eligibility_skus';
    const XML_PATH_FREE_GIFT_ENABLE = 'renew/free_gift/enable';
    const XML_PATH_FREE_GIFT_SKU = 'renew/free_gift/sku';

    protected $customerAudienceType = null;

    protected $customerMediaAccess = null;

    /**
     * @var \Magento\Customer\Model\SessionFactory
     */
    protected $customerSessionFactory;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;
    /**
     * @var Data
     */
    protected $jsonHelper;
    /**
     * @var GetCustomerData
     */
    protected $getCustomerApi;
    /**
     * @var LayoutInterface
     */
    protected $_layout;

    /**
     * @var Ey\PromoCode\Helper\Data
     */
    protected $_promoHelper;
    /**
     * Config constructor.
     * @param Context $context
     * @param \Magento\Customer\Model\SessionFactory $customerSessionFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param GetCustomerData $getCustomerApi
     * @param Data $jsonHelper
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\SessionFactory $customerSessionFactory,
        \Magento\Customer\Model\Session $customerSession,
        GetCustomerData $getCustomerApi,
        Data $jsonHelper,
        LayoutInterface $layout,
        urlHelper $urlHelper,
        PromotionHelper $promotionHelper,
        Session $checkoutSession,
        ProductFactory $productFactory
    ) {
        $this->customerSessionFactory = $customerSessionFactory;
        $this->customerSession = $customerSession;
        $this->getCustomerApi = $getCustomerApi;
        $this->jsonHelper = $jsonHelper;
        $this->scopeConfig = $context->getScopeConfig();
        $this->_layout = $layout;
        $this->urlHelper = $urlHelper;
        $this->promotionHelper  = $promotionHelper;
        $this->_checkoutSession = $checkoutSession;
        $this->_productFactory = $productFactory;
        parent::__construct($context);
    }

    /**
     * Is module enabled
     *
     * @param null $storeId
     * @return string
     */
    public function isEnabled($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::ENABLE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Check skipped sku's
     *
     * @param null $storeId
     * @return string
     */
    public function getSkipEligibiltySkus($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_GENERAL_SKIP_ELIGIBILTY,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isDefaultValueActive($store = null): bool
    {
        if (!$this->isEnabled()) {
            return false;
        }
        return (bool) $this->scopeConfig->isSetFlag(
            self::XML_PATH_GENERAL_DEFAULT_OPTION,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return array
     */
    public function getRenewAllowedPaymentMethods($store = null): array
    {
        $paymentMethods = $this->scopeConfig->getValue(
            self::XML_PATH_GENERAL_ALLOWED_PAYMENTS,
            ScopeInterface::SCOPE_STORE,
            $store
        );

        return $paymentMethods ? explode(",", $paymentMethods) : [];
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isShowTopPositionBlock($store = null): bool
    {
        if (!$this->isEnabled()) {
            return false;
        }
        return (bool) $this->scopeConfig->isSetFlag(
            self::XML_PATH_GENERAL_ENABLED_TOP_BLOCK,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return string|null
     */
    public function getTopPositionBlock($store = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_GENERAL_TOP_BLOCK,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return string
     */
    public function getTopPositionBlockHtml($store = null)
    {
        $cmsStaticId = $this->getTopPositionBlock($store);
        $block = $this->_layout->createBlock(\Magento\Cms\Block\Block::class);
        if ($block) {
            return $block->setBlockId($cmsStaticId)->toHtml();
        }

        return '';
    }

    /**
     * @param null $store
     * @return string
     */
    public function getBottomPositionBlockHtml($store = null)
    {
        $cmsStaticId = $this->getBottomPositionBlock($store);
        $block = $this->_layout->createBlock(\Magento\Cms\Block\Block::class);
        if ($block) {
            return $block->setBlockId($cmsStaticId)->toHtml();
        }

        return '';
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isShowBottomPositionBlock($store = null): bool
    {
        if (!$this->isEnabled()) {
            return false;
        }
        return (bool) $this->scopeConfig->isSetFlag(
            self::XML_PATH_GENERAL_ENABLED_BOTTOM_BLOCK,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return string|null
     */
    public function getBottomPositionBlock($store = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_GENERAL_BOTTOM_BLOCK,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isFreeGiftEnabled($store = null): bool
    {
        if (!$this->isEnabled()) {
            return false;
        }
        return (bool) $this->scopeConfig->isSetFlag(
            self::XML_PATH_FREE_GIFT_ENABLE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return string|null
     */
    public function getFreeGiftProductSku($store = null)
    {
        $promoCode = $this->_checkoutSession->getQuote()->getData('ucc_promocode');
        if ($promoCode) {
            $promotionData = $this->promotionHelper->getPromotionByPromoCode($promoCode);
            if (!empty($promotionData)) {
                $cartItems = $this->_checkoutSession->getQuote()->getAllItems();
                $freeGiftEligibilty = false;
                $promotionsSkus = explode(',', $promotionData->getFreeGiftEligibleSkus());
                if (!empty($promotionsSkus)) {
                    foreach ($cartItems as $cItem) {
                        if (in_array($cItem->getSku(), $promotionsSkus)) {
                            $freeGiftEligibilty = true;
                        }
                    }
                }
                if ($freeGiftEligibilty && $promotionData->getFreeGiftSku()) {
                    return $promotionData->getFreeGiftSku();
                }
            }
        }
        if($this->_checkoutSession->getQuote()->hasItems()) {
            $items = $this->_checkoutSession->getQuote()->getAllItems();
            foreach($items as $item){
                $product = $this->_productFactory->create()->load($item->getProductId());
                if ($product->getData('renew_free_gift_sku')) {
                    return $product->getData('renew_free_gift_sku');
                }
            }
        }
        return '';
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isEnabledRenewConfirmation($store = null)
    {
        return (bool) $this->scopeConfig->isSetFlag(
            self::XML_PATH_GENERAL_IS_RENEW_SUCCESS,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getRenewOptionLimit($store = null) {
        return $this->scopeConfig->getValue(
            self::XML_PATH_GENERAL_OPTION_LIMIT,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @return mixed
     */
    public function getRenewStatus()
    {
        if ($this->customerIsLogin()) {
            return $this->getRenewStatusFromCustomerSession();
        }
        $this->goToHomePage();
    }

    /**
     * @return bool
     */
    public function customerIsLogin(){
        $sessionCustomer = $this->customerSessionFactory->create();
        return $sessionCustomer->isLoggedIn();
    }

    public function goToHomePage(){
        // invalid code
    }

    /**
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomerData(){
        return $this->customerSessionFactory->create()->getCustomer();
    }

    /**
     * @return mixed
     */
    public function getCheckEligibilityAPIURI()
    {
        return $this->scopeConfig->getValue(
            'mulesoft/api/mulesoft_uri',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getAkamaiAuthToken()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/debug_renew_authtoken.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $logger->info("getAkamaiAuthToken - customerSession - Both Access Tokens". $this->customerSession->getAkamaiAccessToken());
        $logger->info("getAccessToken - customerSession - Both Access Tokens". $this->customerSession->getAccessToken());

        if($this->customerSession->getAkamaiAccessToken()){
            return $this->customerSession->getAkamaiAccessToken();
        }
        return $this->customerSession->getAccessToken();
        //return $this->customerSession->getAkamaiAccessToken();

    }


    /**
     * @return mixed
     */
    public function getRenewStatusFromCustomerSession()
    {
        return $this->customerSessionFactory->create()->getRenewStatus();
    }

    /**
     * @param $renewStatus
     * @return mixed
     */
    public function setRenewStatusToCustomerSession($renewStatus)
    {
        return $this->customerSessionFactory->create()->setRenewStatus($renewStatus);
    }

    /**
     * @return mixed
     */
    public function getCustomerAudienceType(){
        if (is_null($this->customerAudienceType)) {
            if ($this->customerIsLogin()) {
                $customer = $this->getCustomerData();
                $customerResponse = $this->getCustomerApi->execute($customer->getData('ucid'));
                if($customerResponse->getStatusCode() == 200){
                    $response = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                    if (isset($response['Customer']['audienceType'])) {
                        $this->customerAudienceType = $response['Customer']['audienceType'];
                    }
                }
            }
        }
        return $this->customerAudienceType;
    }
    /**
     * @return mixed
     */
    public function getCustomerMediaAccess(){
        if (is_null($this->customerMediaAccess)) {
            if ($this->customerIsLogin()) {
                $customer = $this->getCustomerData();
                $customerResponse = $this->getCustomerApi->execute($customer->getData('ucid'));
                if($customerResponse->getStatusCode() == 200){
                    $response = $this->jsonHelper->jsonDecode($customerResponse->getBody()->getContents());
                    if (isset($response['Customer']['nejm.mediaAccess'])) {
                        $this->customerMediaAccess = $response['Customer']['nejm.mediaAccess'];
                    }
                }
            }
        }
        return $this->customerMediaAccess;
    }

    /**
     * Set Renew Response Config To Customer Session
     * @param $response
     */
    public function setRenewResponseData($response)
    {
        $this->customerSession->setRenewResponseData($response);
    }

    /**
     * Return Renew Response Data From Customer Session
     */
    public function getRenewResponseData()
    {
        return $this->customerSession->getRenewResponseData() ?? false;
    }

    /**
     * Return cookie value
     */
    public function getCurrentBrand()
    {
        return $this->urlHelper->getCustomCookie('product');
    }

    /**
     * @param null $sku
     * @return bool
     */
    public function isBrandNejm($sku = null)
    {
        if ($this->getCurrentBrand() && strtolower($this->getCurrentBrand()) === 'nejm') {
            return true;
        } else if ($sku && strtolower(substr($sku, 0, 3)) === 'nej') {
            return true;
        } else if ($this->getCurrentBrand() && strtolower($this->getCurrentBrand()) === 'onj') {
            return true;
        } else if ($sku && strtolower(substr($sku, 0, 3)) === 'onj') {
            return true;
        }

        return false;
    }

    /**
     * @param $eligibilityInfo
     * @return mixed
     */
    public function filterRenewResponse($eligibilityInfo)
    {
        $_filterEligibleProducts = [];
        $_filterInEligibleProducts = [];
        if (isset($eligibilityInfo['products']) && !empty($eligibilityInfo['products'])) {
            foreach ($eligibilityInfo['products'] as $product) {
                if (isset($product['renew']['isEligible']) && $product['renew']['isEligible']) {
                    $_filterEligibleProducts[] = $product;
                }

                if (isset($product['renew']['isEligible']) && !$product['renew']['isEligible']
                    && $product['renew']['status'] != '') {
                    $_filterInEligibleProducts[] = $product;
                }
            }
        }

        if  (!empty($_filterEligibleProducts)) {
            $eligibilityInfo['products'] = $_filterEligibleProducts;
        } else if  (!empty($_filterInEligibleProducts)) {
            $eligibilityInfo['products'] = $_filterInEligibleProducts;
        }

        return $eligibilityInfo;
    }

    /**
     * @param $eligibilityInfo
     * @return mixed
     */
    public function filterPurchaseResponse($eligibilityInfo)
    {
        $_filterEligibleProducts = [];
        $_filterInEligibleProducts = [];
        if (isset($eligibilityInfo['products']) && !empty($eligibilityInfo['products'])) {
            foreach ($eligibilityInfo['products'] as $product) {
                if (isset($product['purchase']['isEligible']) && $product['purchase']['isEligible']) {
                    $_filterEligibleProducts[] = $product;
                }

                if (isset($product['purchase']['isEligible']) && !$product['purchase']['isEligible']
                    && $product['purchase']['status'] != '') {
                    $_filterInEligibleProducts[] = $product;
                }
            }
        }

        if  (!empty($_filterEligibleProducts)) {
            $eligibilityInfo['products'] = $_filterEligibleProducts;
        } else if  (!empty($_filterInEligibleProducts)) {
            $eligibilityInfo['products'] = $_filterInEligibleProducts;
        }

        return $eligibilityInfo;
    }
}
