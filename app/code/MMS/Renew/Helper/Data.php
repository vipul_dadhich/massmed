<?php

namespace MMS\Renew\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;


/**
 * Class Data
 * @package MMS\Renew\Helper
 */
class Data extends AbstractHelper
{
    const DEFAULT_PRODUCT_FAMILY = 'renew/general/default_product_family';

    /**
     * Config constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
    }

    /**
     * Is module enabled
     *
     * @param null $storeId
     * @return string
     */
    public function getDefaultProductFamily($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::DEFAULT_PRODUCT_FAMILY,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
