<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_Renew extension
 * NOTICE OF LICENSE
 */

namespace MMS\Renew\Block;

class Renew extends \Magento\Framework\View\Element\Template
{
	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\SessionFactory $customerSession
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;

    }
    public function getRenewHeaderText() {
        return "Renew";
    }

    public function isCustomerLoggedIn(){
    	$sessionCustomer = $this->customerSession->create();
    	return $sessionCustomer->isLoggedIn();
    }

}