<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_Renew extension
 * NOTICE OF LICENSE
 */

namespace MMS\Renew\Block;

use Magento\Framework\View\Element\Block\ArgumentInterface;


 class Quote implements ArgumentInterface
 {
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\Product $product         
     ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_product = $product;
         
     }

    /**
     *
     * @return string
     */
    public function getRenewCheckoutBrand()
    {
        $brand = '';
        $quote = $this->_checkoutSession->getQuote();
        if ($quote->hasItems()) {
            foreach($quote->getAllVisibleItems() as $item){
                $productId = $item->getProductId();
                $product = $this->_product->load($productId);
                $brand = strtolower($product->getResource()->getAttribute('brand')->getFrontend()->getValue($product));
                if($brand == 'nejm' || $brand == 'catalyst'){
                    break;
                }
            }
        }

        return $brand;
    }
}
