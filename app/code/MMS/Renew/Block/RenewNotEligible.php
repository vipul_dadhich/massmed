<?php

namespace MMS\Renew\Block;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template\Context;
use MMS\Renew\Helper\Config;

/**
 * Class RenewNotEligible
 * @package MMS\Renew\Block
 */
class RenewNotEligible  extends \Magento\Framework\View\Element\Template
{
    const NOT_A_SUBSCRIBER_BLOCK_ID = 'renew-not-subscriber';
    const CURRENT_BILLED_BLOCK_ID = 'renew-current-billed';
    const FUTURE_BILLED_BLOCK_ID = 'renew-future-billed';
    const COMP_BLOCK_ID = 'renew-comp';
    const AUTO_RENEW_BLOCK_ID = 'renew-autorenew';
    const AGENT_BLOCK_ID = 'renew-agent';
    const PROCESSING_PAYMENT_BLOCK_ID = 'processing-payment';
    const RENEW_JAPAN_BLOCK_ID = 'renew-japan';
    const RENEW_KOREA_BLOCK_ID = 'renew-korea';
    const RENEW_SPONSORED_BLOCK_ID = 'renew-sponsored';
    const RENEW_MMS_MEMBER_BLOCK_ID = 'renew-mms-member';
    const RENEW_TEMP_SUSP_BLOCK_ID = 'renew-temp-susp';
    const RENEW_LIFETIME_BLOCK_ID = 'renew-lifetime';
    const RENEW_MEDIA_ACCESS_BLOCK_ID = 'renew-media';
    const RENEW_OTHER_BLOCK_ID = 'renew-other';
    const NOT_LOGGED_IN = 'not-logged-in';
    const RENEW_EXPIRED = 'renew-expired';

//renew-mms-member
//renew-temp-susp
//

    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * RenewNotEligible constructor.
     * @param Context $context
     * @param Config $configHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $configHelper,
        array $data = []
    ) {
        $this->configHelper = $configHelper;
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
    }

    /**
     * @return mixed
     */
    public function getRenewStatus()
    {
        return $this->configHelper->getRenewStatus();
    }

    /**
     * @return string
     */
    public function getBlockIdByRenewStatus()
    {
        if (!$this->configHelper->customerIsLogin()) {
            return self::NOT_LOGGED_IN;
        }
        $renewStatus = $this->getRenewStatus();
        $mediaAccess = $this->configHelper->getCustomerMediaAccess();
        $customerSession = $this->configHelper->getCustomerData();

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/renew-status.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info("Renew Status From Customer Session -> ". $renewStatus);


        $blockId = self::NOT_A_SUBSCRIBER_BLOCK_ID;
        if ($mediaAccess && ($mediaAccess == 'REGULAR' || $mediaAccess == 'ADVANCE')) {
            $blockId = self::RENEW_MEDIA_ACCESS_BLOCK_ID;
        } else if ($renewStatus == 'AUTO_RENEW') {
            $blockId = self::AUTO_RENEW_BLOCK_ID;
        } else if($renewStatus == 'CURRENT_BILLED') {
            $blockId = self::CURRENT_BILLED_BLOCK_ID;
        } else if($renewStatus == 'FUTURE_BILLED') {
            $blockId = self::FUTURE_BILLED_BLOCK_ID;
        } else if($renewStatus == 'COMP') {
            $blockId = self::COMP_BLOCK_ID;
        } else if($renewStatus == 'AGENT' && $customerSession->getData('customer_default_country') == 'JP' && strtolower($this->configHelper->getCurrentBrand()) == 'nejm' ) {
            $blockId = self::RENEW_JAPAN_BLOCK_ID;
        } else if($renewStatus == 'AGENT' && ($customerSession->getData('customer_default_country') == 'KP' || $customerSession->getData('customer_default_country') == 'KR') && strtolower($this->configHelper->getCurrentBrand()) == 'nejm' ) {
            $blockId = self::RENEW_KOREA_BLOCK_ID;
        } else if($renewStatus == 'AGENT') {
            $blockId = self::AGENT_BLOCK_ID;
        } else if($renewStatus == 'PROCESSING_PAYMENT' || $renewStatus == 'PROCESSING_SUBSCRIPTION' || $renewStatus == 'PROCESSING_ORDER') {
            $blockId = self::PROCESSING_PAYMENT_BLOCK_ID;
        } else if($renewStatus == 'NOT-ELIGIBLE') {
            $blockId = self::NOT_A_SUBSCRIBER_BLOCK_ID;
        } else if($renewStatus == 'SPONSOR') {
            $blockId = self::RENEW_SPONSORED_BLOCK_ID;
        } else if($renewStatus == 'MEMBER') {
            $blockId = self::RENEW_MMS_MEMBER_BLOCK_ID;
        } else if($renewStatus == 'TEMPORARY_SUSPENDED') {
            $blockId = self::RENEW_TEMP_SUSP_BLOCK_ID;
        } else if($renewStatus == 'LIFETIME') {
            $blockId = self::RENEW_LIFETIME_BLOCK_ID;
        } else if($renewStatus == 'RENEW_EXPIRED') {
            $blockId = self::RENEW_EXPIRED;
        } else {
            $blockId = self::RENEW_OTHER_BLOCK_ID;
        }


        $logger->info("Renew Status Block ID -> ". $blockId);

        return $blockId;
    }

    /**
     * @return bool
     */
    public function getCmsBlockHtml()
    {
        try {
            return $this->getLayout()->createBlock(
                \Magento\Cms\Block\Block::class
            )->setBlockId(
                $this->getBlockIdByRenewStatus()
            )->toHtml();
        } catch (LocalizedException $e) {
            return false;
        }
    }

    /**
     * Get block cache life time
     *
     * @return int|bool|null
     */
    protected function getCacheLifetime()
    {
        return null;
    }
}
