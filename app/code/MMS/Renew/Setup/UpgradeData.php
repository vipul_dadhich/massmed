<?php

namespace MMS\Renew\Setup;

use Magento\Catalog\Model\Product;
use Magento\Cms\Model\BlockFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Store\Model\Store;
use MMS\Renew\Helper\Config;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * Class UpgradeData
 * @package MMS\Renew\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    const PAYPAL_EXPRESS_CODE = 'paypal_express';

    /**
     * @var Config
     */
    protected $configHelper;
    /**
     * @var ConfigInterface
     */
    protected $resourceConfig;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
    /**
     * UpgradeData constructor.
     * @param Config $configHelper
     * @param ConfigInterface $resourceConfig
     */
    public function __construct(
        Config $configHelper,
        ConfigInterface $resourceConfig,
        EavSetupFactory $eavSetupFactory,
        BlockFactory $blockFactory
    ) {
        $this->configHelper = $configHelper;
        $this->resourceConfig = $resourceConfig;
        $this->eavSetupFactory		= $eavSetupFactory;
        $this->_blockFactory = $blockFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.2.0') < 0) {
            $allowedPayments = $this->configHelper->getRenewAllowedPaymentMethods();
            if (!in_array(self::PAYPAL_EXPRESS_CODE, $allowedPayments)) {
                $allowedPayments[] = self::PAYPAL_EXPRESS_CODE;
            }
            $this->resourceConfig->saveConfig(
                Config::XML_PATH_GENERAL_ALLOWED_PAYMENTS,
                implode(',', $allowedPayments),
                ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                Store::DEFAULT_STORE_ID
            );
        }
        if (version_compare($context->getVersion(), '1.2.1') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            if (!$eavSetup->getAttributeId(Product::ENTITY, 'renew_free_gift_sku')) {
                $eavSetup->addAttribute(
                    Product::ENTITY,
                    'renew_free_gift_sku',
                    [
                        'type' => 'text',
                        'label' => 'Renew Free Gift Sku',
                        'input' => 'text',
                        'backend' => '',
                        'required' => false,
                        'sort_order' => 160,
                        'global' => ScopedAttributeInterface::SCOPE_WEBSITE,
                        'used_in_product_listing' => true,
                        'apply_to' => '',
                        'is_used_in_grid' => false,
                        'is_visible_in_grid' => false,
                        'is_filterable_in_grid' => false,
                    ]
                );
            }
        }
        if (version_compare($context->getVersion(), '1.2.2') < 0) {
            $customHtmlblockNoPaymentDue = '<p>You are Logged Out</p>';
            $staticBlockInfo = [
                'title' => 'Not Logged In',
                'identifier' => 'not-logged-in',
                'stores' => ['0'],
                'is_active' => 1,
                'content' => $customHtmlblockNoPaymentDue
            ];
            $this->_blockFactory->create()->setData($staticBlockInfo)->save();
        }

        $setup->endSetup();
    }
}
