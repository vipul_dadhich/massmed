<?php


namespace MMS\Renew\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.1.0', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable("sales_order"),
                'is_quote_renew',
                [
                    "type" => Table::TYPE_SMALLINT,
                    'length' => '1',
                    'default' => 0,
                    'nullable' => true,
                    "comment" => "Is Order Renew"
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable("sales_order_grid"),
                'is_quote_renew',
                [
                    "type" => Table::TYPE_SMALLINT,
                    'length' => '1',
                    'default' => 0,
                    'nullable' => true,
                    "comment" => "Is Order Renew"
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable("quote"),
                'is_quote_renew',
                [
                    "type" => Table::TYPE_SMALLINT,
                    'length' => '1',
                    'default' => 0,
                    'nullable' => true,
                    "comment" => "Is Quote Renew"
                ]
            );
        }

        $setup->endSetup();
    }
}