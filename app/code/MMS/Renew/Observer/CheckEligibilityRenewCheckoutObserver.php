<?php

namespace MMS\Renew\Observer;

use Ey\MuleSoft\Model\Core\Logger\Monolog;
use Ey\URLParams\Helper\Data as URLParamsDataHelper;
use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ActionFlag;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote;
use MMS\Renew\Helper\Config;
use MMS\Renew\Model\Catalyst\Api\GetEligibility;

/**
 * Class CheckEligibilityRenewCheckoutObserver
 * @package MMS\Renew\Observer
 */
class CheckEligibilityRenewCheckoutObserver implements ObserverInterface
{
    protected $eligibilityInfo = null;
    protected $quote = null;
    protected $quoteItems = null;

    /**
     * @var RedirectInterface
     */
    protected $redirect;

    /**
     * @var Config
     */
    protected $_config;

    /**
     * @var Session
     */
    protected $_checkoutSession;

    /**
     * @var Monolog
     */
    protected $mulesoftLogger;

    /**
     * @var GetEligibility
     */
    protected $_eligibilityApi;

    /**
     * @var Cart
     */
    protected $_cart;

    /**
     * @var ActionFlag
     */
    protected $_actionFlag;
    /**
     * @var URLParamsDataHelper
     */
    protected $_urlParamsDataHelper;
    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * CheckEligibilityRenewCheckoutObserver constructor.
     * @param RedirectInterface $redirect
     * @param Config $config
     * @param Session $checkoutSession
     * @param Monolog $mulesoftLogger
     * @param GetEligibility $eligibilityApi
     * @param Cart $cart
     * @param ActionFlag $actionFlag
     * @param URLParamsDataHelper $urlParamsDataHelper
     * @param ProductFactory $productFactory
     */
    public function __construct(
        RedirectInterface $redirect,
        Config $config,
        Session $checkoutSession,
        Monolog $mulesoftLogger,
        GetEligibility $eligibilityApi,
        Cart $cart,
        ActionFlag $actionFlag,
        URLParamsDataHelper $urlParamsDataHelper,
        ProductFactory $productFactory
    ) {
        $this->redirect = $redirect;
        $this->_config = $config;
        $this->_checkoutSession = $checkoutSession;
        $this->mulesoftLogger = $mulesoftLogger;
        $this->_eligibilityApi = $eligibilityApi;
        $this->_cart = $cart;
        $this->_actionFlag = $actionFlag;
        $this->_urlParamsDataHelper = $urlParamsDataHelper;
        $this->_productFactory = $productFactory;
    }

    public function execute(Observer $observer): CheckEligibilityRenewCheckoutObserver
    {
        try {
            if ($this->_config->isEnabled() && $this->getQuote()->getData('is_quote_renew')) {
                $result = $this->checkCustomerPurchaseEligibility();
                if (!$result) {
                    if (isset($this->eligibilityInfo['products'][0]['renew'])) {
                        $reNewStatus = $this->eligibilityInfo['products'][0]['renew']['status'];
                        $this->_config->setRenewStatusToCustomerSession($reNewStatus);
                    }
                    try {
                        $this->_cart->truncate()->save();
                    } catch (LocalizedException $exception) {
                        $this->mulesoftLogger->log($exception->getMessage());
                    } catch (\Exception $exception) {
                        $this->mulesoftLogger->log('We can\'t update the shopping cart.');
                    }

                    /** @var Action $controller */
                    $controller = $observer->getControllerAction();
                    $this->_actionFlag->set('', Action::FLAG_NO_DISPATCH, true);
                    $this->redirect->redirect($controller->getResponse(), 'renew-not-eligible');
                }
            }
        } catch (NoSuchEntityException | LocalizedException $e) {
            $this->mulesoftLogger->log($e->getMessage());
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerPurchaseEligibility()
    {
        if ($this->_config->customerIsLogin()) {
            $customer = $this->_config->getCustomerData();
            if ($customer->getData('ucid')) {
                $quoteItems = $this->getQuoteItems();
                if (!empty($quoteItems)) {
                    foreach ($quoteItems as $quoteItem) {
                        if ($brand = $this->getProductBrand($quoteItem)) {
                            $this->_eligibilityApi->setProductFamily($brand);
                            break;
                        }
                    }
                }
                $eligibilityInfo = $this->_eligibilityApi->execute($customer->getData('ucid'));
                $this->_eligibilityApi->setProductFamily(null);
                if (isset($eligibilityInfo['products']) && !empty($eligibilityInfo['products'])) {
                    $this->eligibilityInfo = $eligibilityInfo;
                    return $eligibilityInfo;
                }
            }
        }
        return false;
    }

    /**
     * check cart item eligible for renew
     *
     * @return boolean
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @codeCoverageIgnore
     */
    private function checkCustomerPurchaseEligibility(): bool
    {
        $eligibilityInfo = $this->getCustomerPurchaseEligibility();

        $this->mulesoftLogger->log("Renew Data-");
        $this->mulesoftLogger->log(print_r($eligibilityInfo, true));

        $eligibilityInfo = $this->_config->filterRenewResponse($eligibilityInfo);

        $this->mulesoftLogger->log("Renew Filtered Data-");
        $this->mulesoftLogger->log(print_r($eligibilityInfo, true));

        $eligibleSku = $eligibilityInfo['products'][0]['offerCode'] ?? false;
        if ($eligibleSku) {
            $items = $this->getQuoteItems();
            foreach ($items as $item) {
                if (substr($item->getSku(), 0, 3) == substr($eligibleSku, 0, 3)
                    && isset($eligibilityInfo['products'][0]['renew'])
                    && (!$eligibilityInfo['products'][0]['renew']['isEligible'])) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Get quote object associated with cart. By default it is current customer session quote
     *
     * @return Quote
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function getQuote()
    {
        if (!$this->quote) {
            $this->quote = $this->_checkoutSession->getQuote();
        }
        return $this->quote;
    }

    /**
     * @return Quote\Item[]
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function getQuoteItems(): array
    {
        if ($this->quoteItems == null) {
            $this->quoteItems = $this->getQuote()->getAllVisibleItems();
        }

        return $this->quoteItems;
    }

    /**
     * Get Product Brand
     *
     * @param $quoteItem
     * @return string
     */
    private function getProductBrand($quoteItem): string
    {
        $product = $this->_productFactory->create()->load($quoteItem->getProduct()->getId());
        return strtolower($product->getAttributeText('brand'));
    }
}
