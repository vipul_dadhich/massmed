<?php


namespace MMS\Renew\Observer;


use Magento\Checkout\Block\Cart\Item\Renderer\Actions\Remove;
use Magento\Checkout\Model\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use MMS\Renew\Helper\Config;

class BlockCartItemActionRemove implements ObserverInterface
{
    /**
     * @var Config
     */
    protected $_config;
    /**
     * @var Session
     */
    protected $_checkoutSession;

    /**
     * BlockCartItemActionRemovePlugin constructor.
     * @param Config $config
     * @param Session $checkoutSession
     */
    public function __construct(
        Config $config,
        Session $checkoutSession
    ) {
        $this->_config = $config;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param Observer $observer
     * @return $this|void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer) {
        $block = $observer->getEvent()->getBlock();
        if ($block instanceof Remove) {
            if ($this->_config->isEnabled() && $this->getCheckoutSession()->getQuote()->getData('is_quote_renew')) {
                $transport = $observer->getEvent()->getTransport();
                $transport->setHtml('');
            }
        }
        return $this;
    }

    /**
     * Get frontend checkout session object
     *
     * @return Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}