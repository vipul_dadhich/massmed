<?php

namespace MMS\Renew\Observer;

use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ActionFlag;
use Magento\Framework\Event\ObserverInterface;
use Ey\MuleSoft\Model\Core\Logger\Monolog;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote;
use MMS\Renew\Model\Catalyst\Api\GetEligibility;

/**
 * Class PreventRenewCheckoutObserver
 * @package MMS\Renew\Observer
 */
class PreventRenewCheckoutObserver implements ObserverInterface
{
    protected $brandName = null;
    protected $eligibilityInfo = null;
    protected $quote = null;
    protected $quoteItems = [];

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;
    /**
     * @var \MMS\Renew\Helper\Config
     */
    protected $_config;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
    /**
     * @var GetEligibility
     */
    protected $_eligibilityApi;
    /**
     * @var Monolog
     */
    protected $mulesoftLogger;
    /**
     * @var ProductFactory
     */
    protected $_productFactory;
    /**
     * @var Cart
     */
    protected $_cart;
    /**
     * @var ActionFlag
     */
    protected $_actionFlag;

    /**
     * PreventRenewCheckoutObserver constructor.
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     * @param \MMS\Renew\Helper\Config $config
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param Monolog $mulesoftLogger
     * @param GetEligibility $eligibilityApi
     * @param ProductFactory $productFactory
     * @param Cart $cart
     * @param ActionFlag $actionFlag
     */
    public function __construct(
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \MMS\Renew\Helper\Config $config,
        \Magento\Checkout\Model\Session $checkoutSession,
        Monolog $mulesoftLogger,
        GetEligibility $eligibilityApi,
        ProductFactory $productFactory,
        Cart $cart,
        ActionFlag $actionFlag
    ) {
        $this->redirect = $redirect;
        $this->_config = $config;
        $this->_checkoutSession = $checkoutSession;
        $this->mulesoftLogger = $mulesoftLogger;
        $this->_eligibilityApi = $eligibilityApi;
        $this->_productFactory = $productFactory;
        $this->_cart = $cart;
        $this->_actionFlag = $actionFlag;
    }

    /**
     * Redirect to login page
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->getQuote() && $this->getQuote()->hasItems()) {
            $isRedirectToUrl = false;
            $cleanCartItem = false;
            $this->brandName = false;
            $arguments = [];

            if ($this->_config->isEnabled()) {
                if ($this->isPurchaseEligibilityCheckAvailable($this->getQuote()->getItems())) {
                    if ($this->getQuote() && $this->getQuote()->getData('is_quote_renew')) {
                        $isRedirectToUrl = 'renew/checkout/index';
                    }

                    if (!$isRedirectToUrl) {
                        $this->getCustomerPurchaseEligibility();
                        if ($this->getEligibilityInfo()) {
                            if (!$this->isPurchaseEligible() || $this->isRenewEligible()) {
                                $cleanCartItem = true;
                                $isRedirectToUrl = 'renew';
                                if ($this->getBrand()) {
                                    $arguments = [
                                        '_nosid' => true,
                                        '_query' => ['product' => $this->getBrand()]
                                    ];
                                }
                            }
                        }
                    }
                }
            }

            if ($cleanCartItem) {
                try {
                    $this->_cart->truncate()->save();
                } catch (\Exception $exception) {
                    $message = $exception->getMessage()?? 'We can\'t update the shopping cart.';
                    $this->mulesoftLogger->log($message);
                }
            }

            if ($isRedirectToUrl) {
                /** @var Action $controller */
                $controller = $observer->getControllerAction();
                $this->_actionFlag->set('', Action::FLAG_NO_DISPATCH, true);
                $this->redirect->redirect($controller->getResponse(), $isRedirectToUrl, $arguments);
            }
        }

        return $this;
    }

    /**
     * @param $items
     * @return bool
     */
    public function isPurchaseEligibilityCheckAvailable($items){
        $skippedSkusString = $this->_config->getSkipEligibiltySkus();
        $skippedSkus = [];
        $flag = true;
        if ($skippedSkusString) {
            $skippedSkus = explode(',', $skippedSkusString);
        }
        if (!empty($skippedSkus)) {
            foreach($items as $item){
                if (in_array($item->getSku(), $skippedSkus)) {
                    $flag = false;
                }
            }
        }
        return $flag;
    }
    /**
     * Call purchase eligibility api
     */
    private function getCustomerPurchaseEligibility()
    {
        $this->eligibilityInfo = null;
        if ($this->_config->customerIsLogin()) {
            $customer = $this->_config->getCustomerData();
            if ($customer->getData('ucid')) {
                $quoteItems = $this->getQuoteItems();
                if (!empty($quoteItems)) {
                    foreach ($quoteItems as $quoteItem) {
                        if ($brand = $this->getProductBrand($quoteItem)) {
                            $this->brandName = $brand;
                            $this->_eligibilityApi->setProductFamily($brand);
                            break;
                        }
                    }
                }

                $eligibilityInfo = $this->_eligibilityApi->execute($customer->getData('ucid'));
                $this->_eligibilityApi->setProductFamily(null);
                if (isset($eligibilityInfo['products']) && !empty($eligibilityInfo['products'])) {
                    $this->mulesoftLogger->log("Renew Data-");
                    $this->mulesoftLogger->log(print_r($eligibilityInfo, true));
                    $this->eligibilityInfo = $eligibilityInfo;
                }
            }
        }
    }

    private function getBrand()
    {
        return $this->brandName;
    }

    /**
     * @return null|array
     */
    private function getEligibilityInfo(): ?array
    {
        return $this->eligibilityInfo;
    }

    /**
     * check cart item eligible for renew
     *
     * @return boolean
     * @codeCoverageIgnore
     */
    private function isPurchaseEligible(): bool
    {
        $eligibilityInfo = $this->getEligibilityInfo();
        if ($eligibilityInfo) {
            $eligibilityInfo = $this->_config->filterPurchaseResponse($eligibilityInfo);
        }
        $eligibleSku = $eligibilityInfo['products'][0]['offerCode'] ?? '';
        if ($eligibleSku) {
            $items = $this->getQuoteItems();
            foreach ($items as $item) {
                if (substr($item->getSku(), 0, 3) == substr($eligibleSku, 0, 3)
                    && isset($eligibilityInfo['products'][0]['purchase']['isEligible'])
                    && $eligibilityInfo['products'][0]['purchase']['isEligible'] == true) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * check cart item eligible for renew
     *
     * @return boolean
     * @codeCoverageIgnore
     */
    private function isRenewEligible(): bool
    {
        $eligibilityInfo = $this->getEligibilityInfo();
        if ($eligibilityInfo) {
            $eligibilityInfo = $this->_config->filterRenewResponse($eligibilityInfo);
        }
        $eligibleSku = $eligibilityInfo['products'][0]['offerCode'] ?? '';
        if ($eligibleSku) {
            $items = $this->getQuoteItems();
            foreach ($items as $item) {
                if (substr($item->getSku(), 0, 3) == substr($eligibleSku, 0, 3)
                    && isset($eligibilityInfo['products'][0]['renew']['isEligible'])
                    && $eligibilityInfo['products'][0]['renew']['isEligible'] == true) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Get quote object associated with cart. By default it is current customer session quote
     *
     * @return Quote
     */
    private function getQuote()
    {
        if (!$this->quote) {
            try {
                $this->quote = $this->_checkoutSession->getQuote();
            } catch (NoSuchEntityException | LocalizedException $e) {
                $this->quote = false;
            }
        }
        return $this->quote;
    }

    /**
     * @return Quote\Item[]
     */
    private function getQuoteItems(): array
    {
        if ($this->quoteItems == null) {
            $this->quoteItems = $this->getQuote() ? $this->getQuote()->getAllVisibleItems() : [];
        }

        return $this->quoteItems;
    }

    /**
     * Get Product Brand
     *
     * @param $quoteItem
     * @return string
     */
    private function getProductBrand($quoteItem): string
    {
        $product = $this->_productFactory->create()->load($quoteItem->getProduct()->getId());
        return strtolower($product->getAttributeText('brand'));
    }
}
