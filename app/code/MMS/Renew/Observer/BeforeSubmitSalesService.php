<?php

namespace MMS\Renew\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;

class BeforeSubmitSalesService implements ObserverInterface
{
    /**
     *
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        /* @var Order $order */
        $order = $observer->getEvent()->getData('order');
        /* @var Quote $quote */
        $quote = $observer->getEvent()->getData('quote');

        if ($quote->hasData('is_quote_renew')) {
            $order->setData('is_quote_renew', $quote->getData('is_quote_renew'));
        }

        return $this;
    }
}