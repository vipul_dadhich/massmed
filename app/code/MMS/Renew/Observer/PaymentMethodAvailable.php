<?php

namespace MMS\Renew\Observer;

use Magento\Checkout\Model\Cart;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use MMS\Renew\Helper\Config as ConfigHelper;

/**
 * Class PaymentMethodAvailable
 * @package MMS\Renew\Observer
 */
class PaymentMethodAvailable implements ObserverInterface{

    /**
     * @var ConfigHelper
     */
    protected $_configHelper;
    /**
     * @var Cart
     */
    protected $_cart;

    /**
     * PaymentMethodAvailable constructor.
     * @param ConfigHelper $configHelper
     * @param Cart $cart
     */
    public function __construct(
        ConfigHelper $configHelper,
        Cart $cart
	) {
        $this->_configHelper = $configHelper;
        $this->_cart = $cart;
    }

    /**
     * @param Observer $observer
     */
	public function execute(Observer $observer){
        $result = $observer->getEvent()->getResult();
        $isQuoteRenew = $this->_cart->getQuote()->getData('is_quote_renew');
        $paymentMethod = $observer->getEvent()->getMethodInstance()->getCode();
        if ($isQuoteRenew) {
            if (in_array($paymentMethod, $this->getAllowedPayments())) {
                $result->setData('is_available', true);
            } else {
                $result->setData('is_available', false);
            }
        }
	}

    /**
     * @return array
     */
	protected function getAllowedPayments()
    {
        return $this->_configHelper->getRenewAllowedPaymentMethods() ?? [];
    }
}