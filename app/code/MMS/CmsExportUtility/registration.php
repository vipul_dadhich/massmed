<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_CmsExportUtility extension
 * NOTICE OF LICENSE
 * Developer: Dinesh Kaswan <dkaswan@mms.org>
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'MMS_CmsExportUtility',
    __DIR__
);
