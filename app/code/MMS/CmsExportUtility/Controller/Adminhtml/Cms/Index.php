<?php
/**
 * See COPYING.txt for license details.
 *
 * MMS_CmsExportUtility extension
 * NOTICE OF LICENSE
 * Developer: Dinesh Kaswan <dkaswan@mms.org>
 */
namespace MMS\CmsExportUtility\Controller\Adminhtml\Cms;

use Magento\Backend\App\Action;
use Magento\Cms\Model\ResourceModel\Page\CollectionFactory as PageCollectionFactory;

class Index extends Action
{
    /**
     * @var PageCollectionFactory
     */
    protected $pageCollectionFactory;

    /**
     * ChangeColor constructor.
     * @param Action\Context $context
     * @param PageCollectionFactory $pageCollectionFactory
     */
    public function __construct(
        Action\Context $context,
        PageCollectionFactory $pageCollectionFactory,
        \MMS\CmsExportUtility\Helper\Data $dataHelper
    ) {
        $this->pageCollectionFactory = $pageCollectionFactory;
        $this->dataHelper = $dataHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $request = $this->getRequest();
      
        $pageIds = $request->getPost('selected', []);
        if (empty($pageIds)) {
            $this->getMessageManager()->addErrorMessage(__('No pages found.'));
            return $this->_redirect('cms/page');
        }
        $pageCollection = $this->pageCollectionFactory->create();
        $pageCollection->addFieldToFilter('page_id', ['in' => $pageIds]);
        $count = 0;
        try {
            foreach ($pageCollection as $page) {
                $this->dataHelper->exportPage($page);
                $count++;
            }
            
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->getMessageManager()->addErrorMessage(__($message));
        }
        $this->getMessageManager()->addSuccessMessage(__($count.' pages exported to DEV env.'));
        return $this->_redirect('cms/page');
    }
    
}