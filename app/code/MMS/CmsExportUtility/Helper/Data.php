<?php

namespace MMS\CmsExportUtility\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Store\Model\ScopeInterface;
use MMS\Logger\LoggerInterface;

/**
 * Class Data
 * @package MMS\CmsExportUtility\Helper
 */
class Data extends AbstractHelper
{
    CONST TOKEN_API_ENDPOINT = 'rest/V1/integration/admin/token';
    CONST CMS_PAGE_POST_API_ENDPOINT = 'rest/V1/cmsPage';
    /**
     * Url builder
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var EncryptorInterface
     */
    protected $encriptor;

    /**
     * Data constructor.
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encriptor
     * @param LoggerInterface $logger
     */
	public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encriptor,
        LoggerInterface $logger
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->encriptor = $encriptor;
        $this->_logger = $logger;
        return parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->scopeConfig->getValue('cmsexport/general/username', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->encriptor->decrypt($this->scopeConfig->getValue('cmsexport/general/password', ScopeInterface::SCOPE_STORE));
    }
    /**
     * @return mixed
     */
    public function getEnvironmentBaseUrl()
    {
        return $this->scopeConfig->getValue('cmsexport/general/baseurl', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getEnvironment()
    {
        return $this->scopeConfig->getValue('cmsexport/general/environment', ScopeInterface::SCOPE_STORE);
    }

    public function getUserData(){
        return array("username" => $this->getUsername(), "password" => $this->getPassword());
    }
    public function getAuthToken(){
        $this->_logger->debug("Connection Start-> ");
        $userData = $this->getUserData();
        $this->_logger->debug("USERDATA-> " . json_encode($userData, JSON_PRETTY_PRINT));
        $apiUrl = $this->getEnvironmentBaseUrl().self::TOKEN_API_ENDPOINT;
        $this->_logger->debug("TOKEN URL-> " . $apiUrl);
        $ch = curl_init($apiUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($userData))));
        $token = curl_exec($ch);
        $this->_logger->debug("TOKEN-> " . $token);
        if (curl_errno($ch)) {
            $this->_logger->debug("TOKEN ERROR-> " . curl_error($ch));
        }
        return $token;
    }
    public function exportPage($page){
        $token = $this->getAuthToken();
        $apiUrl = $this->getEnvironmentBaseUrl().self::CMS_PAGE_POST_API_ENDPOINT;
        $this->_logger->debug("PAGE URL-> " . $apiUrl);
        $ch = curl_init($apiUrl);
        $postParams = $this->getPageData($page);
        $this->_logger->debug("PAGEDATA-> " . json_encode($postParams, JSON_PRETTY_PRINT));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postParams));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        $this->_logger->debug("PAGEDATA-> " . json_encode($result, JSON_PRETTY_PRINT));
        if (curl_errno($ch)) {
            $this->_logger->debug("PAGE ERROR-> " . curl_error($ch));
        }
    }
    public function getPageData($page){
        return array(
            "page" => array(
                "identifier" => $page->getIdentifier(),
                "title" => $page->getTitle(),
                "page_layout" => $page->getPageLayout(),
                "meta_keywords" => $page->getMetaKeywords(),
                "meta_description" => $page->getMetaDescription(),
                "content_heading" => $page->getContentHeading(),
                "content" => $page->getContent(),
                "sort_order" => $page->getSortOrder(),
                "active" => $page->getIsActive(),
                "layout_update_xml" => $page->getLayoutUpdateXml(),
                "custom_theme" => $page->getCustomTheme(),
                "custom_root_template" => $page->getCustomRootTemplate(),
                "custom_layout_update_xml" => $page->getCustomLayoutUpdateXml(),
                "custom_theme_from" => $page->getCustomThemeFrom(),
                "custom_theme_to" => $page->getCustomThemeTo(),
                "meta_title" => $page->getMetaTitle(),
                //"layout_update_selected" => $page->getLayoutUpdateSelected(),
                //"is_promotional" => $page->getIsPromotional(),
                //"analytics_value" => $page->getAnalyticsValue(),
                "custom_theme_from" => $page->getCustomThemeFrom(),
            )
        );
    }
}

