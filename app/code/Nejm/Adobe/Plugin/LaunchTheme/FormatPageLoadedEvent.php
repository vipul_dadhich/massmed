<?php

namespace Nejm\Adobe\Plugin\LaunchTheme;

use Closure;
use Magento\Cms\Model\Page;
use MMS\Paybill\Helper\Data;
use Nejm\Adobe\Helper\Data as PromoHelper;
use Magento\Framework\Session\SessionManagerInterface as CoreSession;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\ObjectManagerInterface;
/**
 * Class FormatPageLoadedEvent
 * @package Nejm\Adobe\Plugin\LaunchTheme
 */
class FormatPageLoadedEvent
{
    private $_empCode;
    private $_promoCode;
    private $_pageType;
    private $_pageTypeCode;
    private $_activity;
    private $_viewType = 'desktop';
    private $_activityStart = 'y';
    private $_products = '';
    private $_flowType = 'SingleForm';
    private $_pageModel = '';


    const BRAND = 'Catalyst';
    const SECTION = 'Commerce';
    const LAYOUT = 'fullPage';

    /**
     * @var Data
     */
    protected $_dataHelper;
    /**
     * @var PromoHelper
     */
    protected $_promoHelper;
    /**
     * @var CoreSession
     */
    protected $_coreSession;
    /**
     * @var RequestInterface
     */
    protected $_request;
    /**
     * @var Page
     */
    protected $_page;
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * FormatPageLoadedEvent constructor.
     * @param RequestInterface $_request
     * @param Data $_dataHelper
     * @param CoreSession $_coreSession
     * @param PromoHelper $_promoHelper
     * @param Page $_page
     */
    public function __construct(
        RequestInterface $request,
        Data $dataHelper,
        CoreSession $coreSession,
        PromoHelper $promoHelper,
        ObjectManagerInterface $objectManager,
        Page $page,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ) {
        $this->_request = $request;
        $this->_dataHelper = $dataHelper;
        $this->_coreSession = $coreSession;
        $this->_promoHelper = $promoHelper;
        $this->_page = $page;
        $this->_objectManager = $objectManager;
        $this->_productRepository = $productRepository;
    }

    /**
     * @param \Adobe\LaunchTheme\Model\FormatPageLoadedEvent $subject
     * @param Closure $original
     * @param string $pageTitle
     * @param string $pageType
     * @param array $breadcrumbs
     * @return array[]|mixed
     */
    public function aroundExecute(
        \Adobe\LaunchTheme\Model\FormatPageLoadedEvent $subject,
        Closure $original,
        $pageTitle,
        $pageType,
        array $breadcrumbs = []
    ) {
        $pageId = $this->_request->getParam('page_id', $this->_request->getParam('id', false));
        $this->_pageModel = $this->_page->load($pageId);
        $this->_pageTypeCode = $this->_pageModel->getAnalyticsValue();
        $this->_pageTitle = $this->_pageModel->getTitle();
        $this->_products = $this->_pageModel->getUsedSkus();
        $this->_promoCode = $this->_promoHelper->getPromoCode($this->_promoHelper->getPromoCodeUrlParam());
        $this->_empCode = $this->_promoHelper->getEmpCode($this->_promoHelper->getFirstUrlParamCode());
        $options  = $this->_objectManager->get('Nejm\Adobe\Model\Config\Source\Options')->toOptionArray();
        if ($pageId && $this->_pageModel)
        {
            $this->_activity = 'PURCHASE';
            $pageType = isset($options[$this->_pageModel->getAnalyticsValue()]['value']) ? $options[$this->_pageModel->getAnalyticsValue()]['label'] : '';
            $this->_pageType = $pageType;
            if ($this->_pageModel->getIdentifier() == 'no-payment-due' || $this->_pageModel->getIdentifier() == 'renew-not-eligible') {
                $this->_pageType = 'Not Eligible';
                $this->_activityStart = 'n';
                if ($this->_pageModel->getIdentifier() == 'no-payment-due') {
                    $this->_activity = 'PAYMYBILL';
                }
                if ($this->_pageModel->getIdentifier() == 'renew-not-eligible') {
                    $this->_activity = 'RENEW';
                }
            }
            return $this->processPageLoadEvent();
        }
        else if ($this->_request->getFullActionName() == 'paybill_index_index'
                || $this->_request->getFullActionName() == 'renew_index_index'
        ){
            return $this->processPageLoadEventForPaybillAndRenew();
        }
        return $original($pageTitle, $pageType, $breadcrumbs);
    }

    /**
     * @return array[]
     */
    protected function processPageLoadEvent()
    {
        $this->viewType = $this->_promoHelper->getViewType();
        $this->_activityStart = $this->_promoHelper->getActivityStart();
        if ($this->_pageModel->getIdentifier() == 'no-payment-due' || $this->_pageModel->getIdentifier() == 'renew-not-eligible') {
            $this->_activityStart = 'n';
        }
        $event = 'Page Loaded';
        if ($this->_pageTypeCode == "choice-landing" || $this->_pageTypeCode == "cme-choice-landing") {
            $event = 'Listing Viewed';
        } else if ($this->_pageTypeCode == "product-landing" || $this->_pageTypeCode == "cme-product-landing") {
            $event = 'Product Viewed';
        } else if ($this->_pageTypeCode == "error") {
            $event = 'Error';
            $this->_pageType = 'Error';
            $this->_activity = 'Error';
            $this->_activityStart = 'n';
            $this->_flowType = '';
        }
        else if ($this->_pageTypeCode == "disable") {
            return [];
        }
        $mmsData = [
            'campaign' => [
                'empCode' => $this->_empCode,
                'promoCode' => $this->_promoCode
            ],
            'page' => [
                'layout' => self::LAYOUT,
                'userAgentView' => $this->_viewType,
                'siteSection' => self::SECTION,
                'sessionID' => $this->_promoHelper->getSessionID()
            ],
            'step' => [
                'activity' => $this->_activity,
                'activityStart' => $this->_activityStart,
                'flowStepName' => '',
                'flowType' => $this->_flowType
            ],
            'commerce' => [
                'products' =>   $this->getProductsInfo()
            ]
        ];
        if ($this->_pageType) {
            $mmsData['page']['type'] = $this->_pageType;
        }
        if ($this->_pageTypeCode == "error") {
            $mmsData['step']['activityComplete'] = 'n';
        }
        return [
            'event' => $event,
            'mmsData' => $mmsData
        ];
    }

    /**
     * @return array[]
     */
    protected function processPageLoadEventForPaybillAndRenew()
    {
        $this->viewType = $this->_promoHelper->getViewType();
        $this->_activityStart = 'n';
        $event = 'Page Loaded';
        if ($this->_request->getFullActionName() == 'paybill_index_index') {
            $this->_activity = 'PAYMYBILL';
            $event = 'Paybill Loaded';
        }
        if ($this->_request->getFullActionName() == 'renew_index_index') {
            $this->_activity = 'RENEW';
            $event = 'Renew Loaded';
        }

        $mmsData = [
            'campaign' => [
                'empCode' => $this->_empCode,
                'promoCode' => $this->_promoCode
            ],
            'page' => [
                'layout' => self::LAYOUT,
                'userAgentView' => $this->_viewType,
                'siteSection' => self::SECTION,
                'sessionID' => $this->_promoHelper->getSessionID()
            ],
            'step' => [
                'activity' => $this->_activity,
                'activityStart' => $this->_activityStart,
                'flowStepName' => '',
                'flowType' => $this->_flowType
            ]
        ];
        return [
            'event' => $event,
            'mmsData' => $mmsData
        ];
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductsInfo(){
        $productData = [];
        if ($this->_products) {
            $skus = explode(',', $this->_products);
            if (!empty($skus)) {
                foreach($skus as $sku){
                    $product = $this->_productRepository->get($sku);
                    $productData[] = ['brand' => $product->getAttributeText('brand'), 'sku' => $sku];
                }
            }
        }
        return $productData;
    }
}
