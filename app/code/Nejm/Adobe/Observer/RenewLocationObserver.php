<?php
namespace Nejm\Adobe\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Session\SessionManagerInterface as CoreSession;

/**
 * lastLocation_session Session Observer
 */
class RenewLocationObserver  implements ObserverInterface
{
    protected $request;
    protected $_coreSession;
    protected $redirect;

    public function __construct(
        CoreSession $coreSession,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\App\Response\RedirectInterface $redirect
    ) {
        $this->_coreSession = $coreSession;
        $this->request = $request;
        $this->redirect = $redirect;
    }

    /**
     * lastLocation session
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        
        $fullActionName= $this->request->getFullActionName();

        if(!$this->request->isAjax()){
            if($fullActionName =="catalog_category_view"){
                $this->_coreSession->setLastLocation("catalog_category_view");
            }else if($fullActionName == "catalog_product_view"){
                $this->_coreSession->setLastLocation("catalog_product_view");
            }
            else if(!($fullActionName == "catalog_category_view"
                    || $fullActionName == "checkout_cart_index" 
                    || "checkout_cart_index")){
                $this->_coreSession->unsLastLocation();
            }
    
        }
    }
}
