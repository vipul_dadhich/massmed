<?php

declare(strict_types=1);

namespace Nejm\Adobe\Helper;

use Magento\Framework\App\Request\Http;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\App\Helper\Context;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Data
 * @package Nejm\Adobe\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Http
     */
    protected $_request;
    /**
     * @var Session
     */
    protected $_checkoutSession;
    /**
     * @var SessionManagerInterface
     */
    private $_sessionManager;
    /**
     * @var CookieManagerInterface
     */
    private $_cookieManager;
    /**
     * @var CookieMetadataFactory
     */
    private $_cookieMetadataFactory;
    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @param Session $checkoutSession
     */

    /**
     * Data constructor.
     * @param Session $checkoutSession
     * @param Http $request
     * @param SessionManagerInterface $sessionManager
     * @param CookieManagerInterface $cookieManager
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param Context $context
     * @param \Ey\PromoCode\Helper\Data $promoHelper
     * @param \Magento\Catalog\Model\ProductFactory $_productloader
     * @param \MMS\PriceEngine\Helper\Data $priceEngineHelper
     */

    public function __construct(
        Session $checkoutSession,
        Http $request,
        SessionManagerInterface $sessionManager,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        ScopeConfigInterface $scopeConfig,
        Context $context,
        \Ey\PromoCode\Helper\Data $promoHelper,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        \MMS\PriceEngine\Helper\Data $priceEngineHelper
    ) {
        $this->_request = $request;
        $this->_checkoutSession = $checkoutSession;
        $this->_sessionManager = $sessionManager;
        $this->_cookieManager = $cookieManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_promoCodeHelper = $promoHelper;
        $this->_productloader = $_productloader;
        $this->_priceEngineHelper = $priceEngineHelper;
        parent::__construct($context);
    }

    /**
     * @param $code
     * @return false|mixed|string|null
     */
    public function getPromoCode($code)
    {
        $promoCode = $this->_request->getParam($code, false);

        if (!$promoCode && ($this->getCookiePromoCode()) != '') {
            $promoCode = $this->getCookiePromoCode();
        }

        if ((!$promoCode || $promoCode == '') && $this->getPromoCodeFromQuote()) {
            $promoCode = $this->getPromoCodeFromQuote();
        }

        if ((!$promoCode || $promoCode == '') && isset($buyRequest['promocode']) && $buyRequest['promocode'] != '') {
            $promoCode = $buyRequest['promocode'];
        }
        return $promoCode;
    }

    /**
     * @return mixed|string|null
     */
    public function getCookiePromoCode()
    {
        $promoCode = $this->_promoCodeHelper->getCustomCookie('PromoCode');
        if (!$promoCode && isset($_COOKIE['PromoCode']) && $_COOKIE['PromoCode'] != '') {
            $promoCode = $_COOKIE['PromoCode'];
        }
        return $promoCode;
    }

    /**
     * @param $code
     * @return false|mixed|string
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function getEmpCode($code)
    {
        if ($code) {
            $cookieCode = ucfirst($code);
            $promoCode = $this->_request->getParam($code, false);
            $cookie = $this->getCustomCookie($cookieCode . 'Code');

            if ($promoCode && !$cookie) {
                $this->setCustomCookie($cookieCode, $promoCode);
                return $promoCode;
            }
            if ($cookie && !$promoCode) {
                return $cookie;
            }
            return $promoCode;
        }
        return false;
    }

    /**
     * @param $cookieName
     * @param $value
     * @param int $duration
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function setCustomCookie($cookieName, $value, $duration = 86400)
    {
        $metadata = $this->_cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration($duration)
            ->setPath($this->_sessionManager->getCookiePath())
            ->setDomain($this->_sessionManager->getCookieDomain())
            ->setHttpOnly(true)
            ->setSecure(true);

        $this->_cookieManager->setPublicCookie(
            $cookieName.'Code',
            $value,
            $metadata
        );
    }

    /**
     * @param $cookiename
     * @return string|null
     */
    public function getCustomCookie($cookiename)
    {
        return $this->_cookieManager->getCookie($cookiename);
    }

    /**
     * @return Session
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    /**
     * @return false|mixed|null
     */
    public function getPromoCodeFromQuote()
    {
        try {
            if ($this->getCheckoutSession()->getQuote()->hasItems()) {
                return $this->getCheckoutSession()->getQuote()->getData('ucc_promocode');
            }
        } catch (NoSuchEntityExceptionAlias $e) {
        } catch (LocalizedException $e) {
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getPromoCodeUrlParam()
    {
        return  $this->_scopeConfig
            ->getValue('promocode/settings/url_parameter', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getFirstUrlParamCode()
    {
        return  $this->_scopeConfig
            ->getValue('url_parameters/url_parmeters/url_parameter', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $cartItems
     * @return array
     */
    public function getProductsInfo($cartItems){
        $productData = [];
        if ($cartItems) {
            foreach($cartItems as $item){
                $buyRequest = [];
                $option = $item->getOptionByCode('info_buyRequest');
                $buyRequest = '';
                if ($option) {
                    $buyRequest = \json_decode($option->getValue(), true);
                    if (is_null($buyRequest)) {
                        $buyRequest = \unserialize($option->getValue());
                    }
                }
                $product = $this->_productloader->create()->load($item->getProductId());
                $productId = (int)$item->getProductId();
                $terms = $this->_priceEngineHelper->getProductDefaultTerms($productId, null, $buyRequest);
                $term = (isset($terms[0])) ? $terms[0] : '';
                $brand = '';
                if ($product->getAttributeText('brand')){
                    $brand = $product->getAttributeText('brand');
                }
                $productData[] = [
                    'brand' => $brand,
                    'sku' => $product->getSku(),
                    'term' => $term
                ];
            }
        }
        return $productData;
    }

    public function getItemInfoForOrder($cartItems){
        $productData = [];
        if ($cartItems) {
            foreach($cartItems as $item){
                $buyRequest = [];
                $option = $item->getOptionByCode('info_buyRequest');
                $buyRequest = '';
                if ($option) {
                    $buyRequest = \json_decode($option->getValue(), true);
                    if (is_null($buyRequest)) {
                        $buyRequest = \unserialize($option->getValue());
                    }
                }
                $product = $this->_productloader->create()->load($item->getProductId());
                $productId = (int)$item->getProductId();
                $terms = $this->_priceEngineHelper->getProductDefaultTerms($productId, null, $buyRequest);
                $term = (isset($terms[0])) ? $terms[0] : '';
                $brand = '';
                if ($product->getAttributeText('brand')){
                    $brand = $product->getAttributeText('brand');
                }
                $acsProductCode = '';
                if ($product->getAttributeText('acs_product_code')){
                    $acsProductCode = $product->getAttributeText('acs_product_code');
                } else {
                    $acsProductCode = $product->getSku();
                }
                $productData[] = [
                    'brand' => $brand,
                    'sku' => $product->getSku(),
                    'term' => $term,
                    'price' => $item->getPrice(),
                    'units' => $item->getQtyOrdered(),
                    'ACSproductCode' => $acsProductCode,
                    'autoRenew' => $product->getData('autorenew')
                ];
            }
        }
        return $productData;
    }

    /**
     * @param $cartItems
     * @return mixed
     */
    public function getCartID($cartItems)
    {
        if ($cartItems) {
            foreach($cartItems as $item){
                return $item->getQuote()->getId();
            }
        }
    }

    /**
     * @return string
     */
    public function getViewType(){
        $viewType = 'desktop';
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $_SERVER['HTTP_USER_AGENT'])||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($_SERVER['HTTP_USER_AGENT'], 0, 4))) {
            $viewType ='mobile';
        }
        return $viewType;
    }

    /**
     * @return string
     */
    public function getActivityStart(){
        $activityStart = 'n';
        if (!$this->_sessionManager->getLastLocation()) {
            $activityStart = 'y';
        }
        return $activityStart;
    }

    /**
     * @return mixed
     */
    public function getSessionID()
    {
        return $this->_sessionManager->getSessionId();
    }
}
