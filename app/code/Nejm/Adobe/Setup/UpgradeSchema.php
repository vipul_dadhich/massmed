<?php


namespace Nejm\Adobe\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            if ($setup->getConnection()->tableColumnExists('cms_page', 'analytics_value') === false) {
                $setup->getConnection()->addColumn(
                    $setup->getTable('cms_page'),
                    'analytics_value',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        '255',
                        [],
                        'comment' => 'CMS Page Type Value',
                    ]
                );
            }
        }
        if (version_compare($context->getVersion(), '1.1.0') < 0) {
            if ($setup->getConnection()->tableColumnExists('cms_page', 'used_skus') === false) {
                $setup->getConnection()->addColumn(
                    $setup->getTable('cms_page'),
                    'used_skus',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        '255',
                        [],
                        'comment' => 'Used sku\'s in CMS Page',
                    ]
                );
            }
        }
        $setup->endSetup();
    }
}
