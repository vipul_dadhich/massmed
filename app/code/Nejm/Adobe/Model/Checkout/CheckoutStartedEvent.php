<?php
declare(strict_types=1);

namespace Nejm\Adobe\Model\Checkout;

use Adobe\Launch\Api\AddDatalayerEventInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\LayoutInterface;
use Magento\Checkout\Block\Onepage as CheckoutBlock;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\GuestCartRepositoryInterface;
use \Magento\Framework\Session\SessionManagerInterface as CoreSession;
use Nejm\Adobe\Helper\Data as PromoHelper;

/**
 * Add datalayer events to the Checkout start page.
 */
class CheckoutStartedEvent extends \Adobe\LaunchCheckout\Model\CheckoutStartedEvent
{
    private $_empCode;
    private $_promoCode;

    private $viewType = 'desktop';
    private $activityStart = 'n';
    private $pageName = 'Checkout';
    private $activity = 'PURCHASE';

    protected $_coreSession;
    protected $_productloader;

    const TYPE          = 'Commerce';
    const ACTIVITYTYPE  = 'SingleForm';
    const LAYOUT = 'fullPage';
    const SECTION = 'Commerce';

    /**
     * @var AddDatalayerEventInterface
     */
    private $addDatalayerEvent;

    /**
     * @var FormatCartViewedEvent
     */
    private $formatCartViewedEvent;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var CartRepositoryInterface
     */
    private $guestCartRepository;

    /**
     * @param AddDatalayerEventInterface $addDatalayerEvent
     * @param FormatCartViewedEvent $formatCartViewedEvent
     * @param CartRepositoryInterface $cartRepository
     * @param GuestCartRepositoryInterface $guestCartRepository
     */
    public function __construct(
        AddDatalayerEventInterface $addDatalayerEvent,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        CartRepositoryInterface $cartRepository,
        GuestCartRepositoryInterface $guestCartRepository,
        PromoHelper $promoHelper,
        CoreSession $coreSession
    ) {
        $this->addDatalayerEvent        = $addDatalayerEvent;
        $this->_productloader = $_productloader;
        $this->cartRepository           = $cartRepository;
        $this->guestCartRepository      = $guestCartRepository;
        $this->_coreSession             = $coreSession;
        $this->_promoHelper = $promoHelper;
    }

    /**
     * Add datalayer events to the Checkout page.
     *
     * @param LayoutInterface $layout
     * @return void
     */
    public function execute(LayoutInterface $layout)
    {
        /** @var CheckoutBlock $checkoutBlock */
        $checkoutBlock = $layout->getBlock("checkout.root");
        if (!$checkoutBlock) {
            return;
        }
        $checkoutConfig = $checkoutBlock->getCheckoutConfig();
        $cartId = $checkoutConfig['quoteData']['entity_id'];
        $is_paybill = $checkoutConfig['quoteData']['is_paybill'];
        $is_quote_renew = $checkoutConfig['quoteData']['is_quote_renew'];
        $stepData = $this->getStepData();
        $this->activityStart = $this->_promoHelper->getActivityStart();
        if ($is_quote_renew) {
            $this->pageName = 'Renew Checkout';
            $this->activity = 'RENEW';
            $this->activityStart = 'y';
            $stepData = $this->getStepData($is_quote_renew);
        }
        if ($is_paybill) {
            $this->pageName = 'PayMyBill Checkout';
            $this->activity = 'PAYMYBILL';
            $this->activityStart = 'y';
            $stepData = $this->getStepData($is_paybill);
        }
        $isLoggedIn = $checkoutConfig['isCustomerLoggedIn'];
        try {
            $cart = $isLoggedIn ? $this->cartRepository->get($cartId) : $this->guestCartRepository->get($cartId);
        } catch (NoSuchEntityException $exception) {
            return;
        }
        $cartItems = $cart->getItems();
        $this->_promoCode = $this->_promoHelper->getPromoCode($this->_promoHelper->getPromoCodeUrlParam());
        $this->_empCode = $this->_promoHelper->getEmpCode($this->_promoHelper->getFirstUrlParamCode());
        $this->viewType = $this->_promoHelper->getViewType();

        $mmsData = [
            'campaign' => [
                'empCode' => $this->_empCode,
                'promoCode' => $this->_promoCode
            ],
            'page' => [
                'layout' => self::LAYOUT,
                'userAgentView' => $this->viewType,
                'siteSection' => self::SECTION,
                'type' => $this->pageName,
                'sessionID' => $this->_promoHelper->getSessionID()
            ],
            'step' => $stepData,
            'commerce' => [
                'products' =>   $this->_promoHelper->getProductsInfo($cartItems),
                'cartID'   =>   $this->_promoHelper->getCartID($cartItems)
            ]
        ];
        $eventData = [
            'event' => 'Checkout Started',
            'mmsData' => $mmsData
        ];
        $this->addDatalayerEvent->execute($eventData);
    }
    public function getStepData($isNotRegularCheckout = false){
        $stepData = [
            'activity' => $this->activity,
            'flowStepName' => '',
            'flowType' => self::ACTIVITYTYPE
        ];
        if ($isNotRegularCheckout) {
            $stepData['activityStart'] = $this->activityStart;
        }
        return $stepData;
    }
    /*
        Get session ID
    */
    public function getSessionID()
    {
        return $this->_coreSession->getSessionId();
    }
}
