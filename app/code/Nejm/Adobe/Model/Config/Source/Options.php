<?php
namespace Nejm\Adobe\Model\Config\Source;

class Options implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $options = [];
        $options['choice-landing'] = ['label' => 'Subscribe Choice Landing', 'value' => 'choice-landing'];
        $options['product-landing'] = ['label' => 'Subscribe Product Landing', 'value' => 'product-landing'];
        $options['cme-choice-landing'] = ['label' => 'CME Choice Landing', 'value' => 'cme-choice-landing'];
        $options['cme-product-landing'] = ['label' => 'CME Product Option Landing', 'value' => 'cme-product-landing'];
        $options['cms-page'] = ['label' => 'CMS Page', 'value' => 'cms-page'];
        $options['disable'] = ['label' => 'Disable', 'value' => 'disable'];
        $options['error'] = ['label' => 'Error Page', 'value' => 'error'];
        return $options;
    }
}
