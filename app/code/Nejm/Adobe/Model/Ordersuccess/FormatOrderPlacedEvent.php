<?php
declare(strict_types=1);

namespace Nejm\Adobe\Model\Ordersuccess;
use Nejm\Adobe\Helper\Data as PromoHelper;

use Magento\Customer\Model\Session;

/**
 * Format Order placed datalayer event data.
 */
class FormatOrderPlacedEvent extends \Adobe\LaunchCheckout\Model\FormatOrderPlacedEvent
{

    private $_viewType = 'desktop';
    private $_empCode;
    private $_promoCode;
    private $_pageType = 'Success';
    private $_activity = 'PURCHASE';
    const LAYOUT = 'fullPage';
    const NAME          = 'Commerce Order Confirmation Page';
    const ACTIVITYTYPE  = 'SingleForm';
    const SECTION = 'Commerce';

    /**
     * @var $_promoHelper
     */
    protected $_promoHelper;
    /**
     * @var $session
     */
    protected $session;
    /**
     * FormatOrderPlacedEvent constructor.
     * @param Session $session
     * @param PromoHelper $promoHelper
     */
    public function __construct(
        Session $session,
        PromoHelper $promoHelper
    ) {
        $this->session = $session;
        $this->_promoHelper = $promoHelper;
    }

    /**
     * Order placed event data.
     *
     * @param array $orders
     * @return array
     * @depracated Logic needs to be reviewed.
     */
    public function execute(array $orders): array
    {
        $this->_viewType = $this->_promoHelper->getViewType();
        $result = [];
        foreach ($orders as $order) {
            $newUser = '';
            $customerExist = $this->session->getCustomerExist();
            if ($this->session->getCustomerCreatedByOrder()) {
                $customerExist = $this->session->getCustomerCreatedByOrder();
                $this->session->setCustomerCreatedByOrder('');
            }
            if ($customerExist == 'NO') {
                $newUser = 'y';
            } else if($customerExist == 'YES') {
                $newUser = 'n';
            } else {
                $newUser = $customerExist;
            }
            $methodTitle = '';
            $payment = $order->getPayment();
            if ($payment) {
                $method = $payment->getMethodInstance();
                if ($method->getCode() == 'md_firstdata') {
                    $methodTitle = 'Credit';
                } else if ($method->getCode() == 'paypal_express') {
                    $methodTitle = 'PayPal';
                } else if ($method->getCode() == 'cashondelivery') {
                    $methodTitle = 'BillMe';
                }
            }
            $billingAddress = $order->getBillingAddress();
            $this->_promoCode = $this->_promoHelper->getPromoCode($this->_promoHelper->getPromoCodeUrlParam());
            $this->_empCode = $this->_promoHelper->getEmpCode($this->_promoHelper->getFirstUrlParamCode());
            if ($order->getData('is_paybill')) {
                $this->_pageType = 'PayMyBill Success';
                $this->_activity = 'PAYMYBILL';
            } else if ($order->getData('is_quote_renew')) {
                $this->_pageType = 'Renew Success';
                $this->_activity = 'RENEW';
            }
            $mmsData = [
                'campaign' => [
                    'empCode' => $this->_empCode,
                    'promoCode' => $this->_promoCode
                ],
                'page' => [
                        'layout' => self::LAYOUT,
                        'userAgentView' => $this->_viewType,
                        'siteSection' => self::SECTION,
                        'type' => $this->_pageType,
                        'sessionID' => $this->_promoHelper->getSessionID()
                ],
                'step' => [
                    'activity'          => $this->_activity,
                    'activityComplete'  => 'y',
                    'flowStepName'      => '',
                    'flowType'      => self::ACTIVITYTYPE,
                    'newUser'           => $newUser
                ],
                'emails' => [
                    'optinCodes'          => '',
                    'optoutCodes'  => ''
                ],
                'commerce' => [
                    'orderID'           => $order->getIncrementId(),
                    'currency'          => $order->getOrderCurrencyCode(),
                    'paymentMethod'     => $methodTitle,
                    'state'             => $billingAddress->getRegionCode(),
                    'tax'               => $order->getTaxAmount(),
                    'totalAmount'       => $order->getGrandTotal(),
                    'purchaseID'       => $order->getIncrementId(),
                    'cartID'       => $order->getQuoteId(),
                    'products'     => $this->_promoHelper->getItemInfoForOrder($order->getAllVisibleItems())
                ]
            ];
            $orderObject = [
                'event' => 'Order Placed',
                'mmsData' => $mmsData
            ];
            $result[] = $orderObject;
        }
        return $result;
    }

}
