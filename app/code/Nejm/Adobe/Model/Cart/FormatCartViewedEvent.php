<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nejm\Adobe\Model\Cart;

use Magento\Framework\Session\SessionManagerInterface as CoreSession;
use Magento\Quote\Api\Data\CartItemInterface;
use Nejm\Adobe\Helper\Data as PromoHelper;

/**
 * Format Cart Viewed datalayer event data.
 */
class FormatCartViewedEvent extends \Adobe\LaunchCheckout\Model\FormatCartViewedEvent
{

    private $_empCode;
    private $_promoCode;

    private $viewType = 'desktop';
    private $activityStart = 'n';
    private $activity = 'PURCHASE';
    protected $_coreSession;
    protected $_productloader;


    const BRAND = 'Catalyst';
    const NAME          = 'Cart';
    const TYPE          = 'Commerce';
    const ACTIVITYSTEP = 'cart';
    const ACTIVITYTYPE  = 'SingleForm';
    const LAYOUT = 'fullPage';
    const SECTION = 'Commerce';

    /**
     * @var PromoHelper
     */
    protected $_promoHelper;

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $_productloader,
        PromoHelper $promoHelper,
        CoreSession $coreSession,
        \MMS\PriceEngine\Helper\Data $priceEngineHelper
    ) {
        $this->_productloader = $_productloader;
        $this->_promoHelper = $promoHelper;
        $this->_coreSession = $coreSession;
        $this->_priceEngineHelper = $priceEngineHelper;

    }

    /**
     * Format Cart Viewed datalayer event data.
     *
     * @param CartItemInterface[] $cartItems
     * @return array
     */
    public function execute(array $cartItems): array
    {
        $result = [];
        //Identifying user comming from category page or note

        $this->_promoCode = $this->_promoHelper->getPromoCode($this->_promoHelper->getPromoCodeUrlParam());
        $this->_empCode = $this->_promoHelper->getEmpCode($this->_promoHelper->getFirstUrlParamCode());
        $this->viewType = $this->_promoHelper->getViewType();
        $this->activityStart = $this->_promoHelper->getActivityStart();
        $this->activity = $this->getActivity($cartItems);
        $mmsData = [
            'campaign' => [
                'empCode' => $this->_empCode,
                'promoCode' => $this->_promoCode
            ],
            'page' => [
                'layout' => self::LAYOUT,
                'userAgentView' => $this->viewType,
                'siteSection' => self::SECTION,
                'type' => self::NAME,
                'sessionID' => $this->_promoHelper->getSessionID()
            ],
            'step' => [
                'activity' => $this->activity,
                'activityStart' => 'y',
                'flowStepName' => '',
                'flowType' => self::ACTIVITYTYPE
            ],
            'commerce' => [
                'products' =>   $this->_promoHelper->getProductsInfo($cartItems),
                'cartID'   =>   $this->_promoHelper->getCartID($cartItems)
            ]
        ];
        $result['event'] = 'Cart Viewed';
        $result['mmsData'] = $mmsData;
        return $result;
    }

    public function getActivity($cartItems){
        foreach($cartItems as $item){
            $quote = $item->getQuote();
            if ($quote->getData('is_paybill')) {
                return 'PAYMYBILL';
            } else if ($quote->getData('is_quote_renew')) {
                return 'RENEW';
            } else {
                return 'PURCHASE';
            }
        }
    }
    /*
        Get session ID
    */
    public function getSessionID()
    {
        return $this->_coreSession->getSessionId();
    }

}
