<?php
/**
 * Tag Manager Extension
 *
 * @category    Fts
 * @package     Nejm_Adobe
 * @author      Commerce Team
 * @copyright   Copyright (c) 2019
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Nejm_Adobe',
    __DIR__
);