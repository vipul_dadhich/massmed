define([
    'jquery',
    'ko',
    'uiComponent'
], function ($, ko, Component) {
    'use strict';

    var checkoutCmsCustom = window.checkoutConfig.checkoutCmsCustom;
    return Component.extend({
        defaults: {
            template: 'Nejm_CustomPrivacyPolicySection/checkout/checkout-cms-privacy'
        },
        isVisible: ko.observable(false),
        cmsBlockCustom: checkoutCmsCustom,

        /**
         * build a unique id for the term checkbox
         *
         * @param {Object} context - the ko context
         */
        getCmsBlockIdPrivacy: function (context) {
            var paymentMethodName = '',
                paymentMethodRenderer = context[1];

            // corresponding payment method fetched from parent context
            if (paymentMethodRenderer) {
                // item looks like this: {title: "Check / Money order", method: "checkmo"}
                paymentMethodName = paymentMethodRenderer.item ?
                  paymentMethodRenderer.item.method : '';
            }

            return 'paymentcmsafter_' + paymentMethodName;
        },

        /**
         * build a unique html for the term checkbox
         *
         * @param {Object} context - the ko context
         */
        getCmsBlockContentPrivacy: function (context) {
            var cmsContent = '',
            
                cmsContent = this.cmsBlockCustom['privacy-policy'] ?
                this.cmsBlockCustom['privacy-policy'] : '';
                this.isVisible(true);

            return cmsContent;
        },
        moveElementRendered: function(){
           setTimeout(function(){
               $('.privacy-policy-checkout .payment-methods').insertAfter($('.ey_payment_method_step').find('.actions-toolbar'));
               $('.privacy-policy-checkout').show();
            }, 3000);
        }
    });
});