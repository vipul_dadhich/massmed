require([
    'jquery',
    'Magento_Customer/js/zxcvbn',   
], function ($,zxcvbn) {
    'use strict';
    $( "body" ).delegate( ".billing-address-form input[name='postcode'], .billing-address-form select[name='country_id'], .billing-address-form input[name='street[0]'], .billing-address-form input[name='city'], .billing-address-form select[name='region_id'], .billing-address-form input[id='ba_password'], .billing-address-form .confirmpassword", "blur", function() {
        if($('.billing-address-form select[name="country_id"]').val()
            && $('.billing-address-form input[name="street[0]"]').val()
            && $('.billing-address-form input[name="city"]').val()
            && $('.billing-address-form select[name="region_id"]').val()
            && $('.billing-address-form input[name="postcode"]').val()
            && $('.billing-address-form input[id="ba_password"]').val()
            && $('.billing-address-form .confirmpassword').val()
            ){
            $('#myCustomUpdateButton').click();
        }
        
    });
});
