define([
    'jquery'
], function ($) {
    'use strict';

    return {
        options : {
            methodId: null,
            methodCode: null
        },
        customCheckboxSelector: '.custom-checkbox',
        customCheckboxInputSelector: '.custom-checkbox input',
        paymentMethodRadioSelector: '.payment-method .radio',
        placeOrderSelector : '#checkout-payment-method-load .actions-toolbar button.checkout',
        billingAddrFormRegionIdSelector: '.billing-address-form select[name="region_id"]',
        bindCheckRenewalSelector: '.bind-check-renewal',
        placeOrderClick:1,
        paypalOrderSelector : "#paypal-express-in-context-button",

        _init: function(options) {
            this.options = options;
            this._bindControls();
        },

        _bindControls: function () {
            this._bindRegionIdSelect();
            this._bindPaymentMethodRadio();
            this._bindCheckRenewal();
        },

        _bindRegionIdSelect: function () {
            var self = this;

            $(self.billingAddrFormRegionIdSelector).on('change', function () {
                if ($(this).val() === '178' && $('.payment-method._active').find('.custom-checkbox').length >0) {
                    $(self.customCheckboxSelector).show();

                    if(self.placeOrderClick>0){
                        $(self.customCheckboxInputSelector).prop('checked', false);
                        $(self.placeOrderSelector).attr('disabled', 'disabled');
                        if ($(self.paypalOrderSelector).length > 0) {
                            $(self.paypalOrderSelector).addClass('paypal-disabled');
                        }
                    }

                }else{
                    $(self.placeOrderSelector).removeAttr('disabled');
                    if ($(self.paypalOrderSelector).length > 0) {
                        $(self.paypalOrderSelector).removeClass('paypal-disabled');
                    }
                    $(self.customCheckboxSelector).hide();
                }
            });
        },

        _bindPaymentMethodRadio: function () {
            var self = this;
            $(self.paymentMethodRadioSelector).on('click', function () {
                $(self.placeOrderSelector).removeAttr('disabled');
                self.placeOrderClick = 1;
                if ($(self.billingAddrFormRegionIdSelector).val() === '178') {
                    if ($(this).closest('.payment-method._active').find('.custom-checkbox').length) {
                        var eId = $(this).closest('.payment-method._active').find('.custom-checkbox').find('input').attr('id');
                        $(self.customCheckboxSelector).show();
                        if ($('#'+eId).is(':checked')) {
                            if(eId === 'checkbox_paypal_express' && $(self.paypalOrderSelector).length > 0){
                                $(self.paypalOrderSelector).removeClass('paypal-disabled');
                            }
                            $(self.placeOrderSelector).removeAttr('disabled');
                        } else {
                            if(eId === 'checkbox_paypal_express' && $(self.paypalOrderSelector).length > 0){
                                $(self.paypalOrderSelector).addClass('paypal-disabled');
                            }
                            $(self.placeOrderSelector).attr('disabled', 'disabled');
                        }
                    }
                }
            });
        },

        _bindCheckRenewal: function () {
            var self = this;
            $(self.bindCheckRenewalSelector).on('change', function () {
                let eId = $(this).closest('.payment-method._active').find('.custom-checkbox').find('input').attr('id');

                if ($(this).is(':checked')) {
                    self.placeOrderClick = 0;
                    $(self.placeOrderSelector).removeAttr('disabled');
                    if(eId === 'checkbox_paypal_express' && $(self.paypalOrderSelector).length > 0){
                        $(self.paypalOrderSelector).removeClass('paypal-disabled');
                    }
                } else {
                    $(self.placeOrderSelector).attr('disabled', 'disabled');
                    if(eId === 'checkbox_paypal_express' && $(self.paypalOrderSelector).length > 0){
                        $(self.paypalOrderSelector).addClass('paypal-disabled');
                    }
                }
            });
        }
    };
});
