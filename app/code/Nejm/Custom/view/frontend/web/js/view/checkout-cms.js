define([
    'jquery',
    'ko',
    'uiComponent'
], function ($, ko, Component) {
    'use strict';

    var checkoutCms = window.checkoutConfig.checkoutCms;
    return Component.extend({
        defaults: {
            template: 'Nejm_Custom/checkout/checkout-cms'
        },
        isVisible: ko.observable(false),
        cmsBlock: checkoutCms,

        /**
         * build a unique id for the term checkbox
         *
         * @param {Object} context - the ko context
         */
        getCmsBlockId: function (context) {
            var paymentMethodName = '',
                paymentMethodRenderer = context[1];

            // corresponding payment method fetched from parent context
            if (paymentMethodRenderer) {
                // item looks like this: {title: "Check / Money order", method: "checkmo"}
                paymentMethodName = paymentMethodRenderer.item ?
                  paymentMethodRenderer.item.method : '';
            }

            return 'paymentcms_' + paymentMethodName;
        },

        /**
         * build a unique html for the term checkbox
         *
         * @param {Object} context - the ko context
         */
        getCmsBlockContent: function (context) {
            var paymentMethodName = '',
                cmsContent = '',
                paymentMethodRenderer = context[1];

            // corresponding payment method fetched from parent context
            if (paymentMethodRenderer) {
                // item looks like this: {title: "Check / Money order", method: "checkmo"}
                paymentMethodName = paymentMethodRenderer.item ?
                  paymentMethodRenderer.item.method : '';
            }
            
            if (paymentMethodName) {
                cmsContent = this.cmsBlock[paymentMethodName] ?
                this.cmsBlock[paymentMethodName] : '';
                this.isVisible(true);
            }

            return cmsContent;
        }
    });
});