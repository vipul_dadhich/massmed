<?php
/**
 * Nejm Payment Method CMS Extension
 *
 * @category    Fts
 * @package     Nejm_Custom
 * @author      Nejm Team
 * @copyright   Copyright (c) 2019
 */
namespace Nejm\Custom\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\Checkout\Model\Session as CheckoutSession;


class CmsConfigProvider implements ConfigProviderInterface
{
    /** @var LayoutInterface  */
    protected $_layout;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var \Magento\Quote\Api\PaymentMethodManagementInterface
     */
    protected $paymentMethodManagement;

    public function __construct(
        LayoutInterface $layout,
        \Magento\Quote\Api\PaymentMethodManagementInterface $paymentMethodManagement,
        CheckoutSession $checkoutSession
    ) {
        $this->_layout = $layout;
        $this->checkoutSession = $checkoutSession;
        $this->paymentMethodManagement = $paymentMethodManagement;
    }

    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $cmsBlockArray = [];
        $cmsBlockArray['checkoutCms']=[];
        foreach($this->getPaymentMethods() as $paymentMethod)
        {
            $cmsBlockArray['checkoutCms'][$paymentMethod['code']] = $this->_layout->createBlock('Magento\Cms\Block\Block')->setBlockId($paymentMethod['code'])->toHtml();
        }
        return $cmsBlockArray;
    }

    private function getPaymentMethods()
    {
        $paymentMethods = [];
        $quote = $this->checkoutSession->getQuote();
        if ($quote->getIsActive()) {
            foreach ($this->paymentMethodManagement->getList($quote->getId()) as $paymentMethod) {
                $paymentMethods[] = [
                    'code' => $paymentMethod->getCode(),
                    'title' => $paymentMethod->getTitle()
                ];
            }
        }
        return $paymentMethods;
    }
}
