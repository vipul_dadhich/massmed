<?php
/**
 * Nejm Payment Method CMS Extension
 *
 * @category    Fts
 * @package     Nejm_Custom
 * @author      Nejm Team
 * @copyright   Copyright (c) 2019
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Nejm_Custom',
    __DIR__
);
