define([
    'jquery',
    'mage/translate',
    'underscore',
    'jquery-ui-modules/widget'
], function ($, $t, _) {
    'use strict';

    $.widget('mage.catalogWidgetAddToCart', {
        options: {
            bindSubmit: true,
            addToCartButtonSelector: '.ctaButton',
            addToCartButtonDisabledClass: 'disabled'
        },

        /** @inheritdoc */
        _create: function () {
            if (this.options.bindSubmit) {
                this._bindSubmit();
                this.enableAddToCartButton();
            }
        },

        /**
         * @private
         */
        _bindSubmit: function () {
            var self = this;

            if (this.element.data('catalog-widget-addtocart-initialized')) {
                return;
            }
            this.element.data('catalog-widget-addtocart-initialized', 1);

            $(this.options.addToCartButtonSelector).unbind('click').on('click', function (e) {
                e.preventDefault();
                self.disableAddToCartButton();
                let form = $(this).closest('form');
                $(form).validation().submit();
            });
        },

        disableAddToCartButton: function () {
            let addToCartButton = $(this.options.addToCartButtonSelector);
            addToCartButton.addClass(this.options.addToCartButtonDisabledClass);
        },

        enableAddToCartButton: function () {
            let self = this,
                addToCartButton = $(this.options.addToCartButtonSelector);
            setTimeout(function () {
                addToCartButton.removeClass(self.options.addToCartButtonDisabledClass)
            }, 1000);
        }
    });

    return $.mage.catalogWidgetAddToCart;
});
