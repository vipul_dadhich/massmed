<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Nejm\Cmswidgets\Block\Html;

/**
 * Html page header block
 *
 * @api
 * @since 100.0.2
 */
class Header extends \Magento\Theme\Block\Html\Header
{
    
    public function getWidgetClientID()
    {
        return $this->_scopeConfig->getValue('akamai/api/client_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
