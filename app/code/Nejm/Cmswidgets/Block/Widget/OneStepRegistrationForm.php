<?php 
namespace Nejm\Cmswidgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

 
class OneStepRegistrationForm extends Template implements BlockInterface {

    protected $_template = "widget/onestep_registration_form.phtml";

}