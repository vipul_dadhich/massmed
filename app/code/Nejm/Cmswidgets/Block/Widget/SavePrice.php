<?php 
namespace Nejm\Cmswidgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Nejm\Cmswidgets\Helper\Data;

class SavePrice extends Template implements BlockInterface {

    protected $_template = "widget/save_price.phtml";

    /**
     * @var \Nejm\Cmswidgets\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface 
     */
    protected $priceCurrency;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Data $helper,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_helper = $helper;
        $this->priceCurrency = $priceCurrency;
        $this->_storeManager = $storeManager;
    }

    public function getPriceData(){
        $productData = $this->_helper->getProductData($this->getData('productSku'));
        $offAmount = null;
        $finalPriceAmt = null;
        $regularPrice = null;

        if($productData){

            if($this->_storeManager->getStore()->getCurrentCurrencyCode() == 'AUD'){
                $regularPrice = $productData->getDefaultPriceAud();
            }elseif($this->_storeManager->getStore()->getCurrentCurrencyCode() == 'CAD'){
                $regularPrice = $productData->getDefaultPriceCad();
            }elseif($this->_storeManager->getStore()->getCurrentCurrencyCode() == 'EUR'){
                $regularPrice = $productData->getDefaultPriceEur();
            }elseif($this->_storeManager->getStore()->getCurrentCurrencyCode() == 'GBP'){
                $regularPrice = $productData->getDefaultPriceGbp();
            }else{
                $regularPrice = $productData->getPrice();
            }

            $finalPriceAmt = $productData->getPriceInfo()->getPrice('final_price')->getValue();

            if(!is_null($regularPrice) && !is_null($finalPriceAmt)){
                if(is_numeric($regularPrice) && is_numeric($finalPriceAmt)){
                    $offAmount = $regularPrice - $finalPriceAmt;
                }else if(is_array($regularPrice) || is_array($finalPriceAmt)){
                    $offAmount = null;
                }
                else{
                    $offAmount = number_format($regularPrice, 2) - number_format($finalPriceAmt, 2);
                }
                
    
                if(!is_null($offAmount) && is_numeric( $offAmount ) && floor( $offAmount ) != $offAmount){
                    $offAmount = number_format($offAmount, 2);
                }
            }
            
            
            if(!is_null($offAmount) && $offAmount >= 0){
                if($this->getData('percentage') == '1'){
                    return floor(($offAmount/$regularPrice)*100 );        
                }else{
                    return $offAmount;
                }
            }
        }
        return null;
    }

    public function getCurrencySymbol()
    {
        if($this->getData('percentage') == '1'){
            return '%';
        }else{
            return $this->priceCurrency->getCurrencySymbol();
        }
        
    }


}