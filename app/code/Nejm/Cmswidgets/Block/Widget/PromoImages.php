<?php 
namespace Nejm\Cmswidgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Nejm\Cmswidgets\Helper\Data;

 
class PromoImages extends Template implements BlockInterface {

    protected $_template = "widget/promo_images.phtml";

    /**
     * @var Nejm\Cmswidgets\Helper\Data
     */
    protected $_helper;

    /**
     * @var Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * Url builder
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Data $helper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_helper = $helper;
        $this->storeManager = $storeManager;
    }

    public function getPromotionImage(){
        $promocode = $this->getPromoCode();
        $imageURL = $this->getData('defaultImage');
        $anchorOption = $this->getData('anchorOption') == '1' ? 'Anchor1' : 'Anchor2';
        
        if($promocode){
            $promotionData = $this->_helper->getPromotionData($promocode);
            if($anchorOption == 'Anchor1'){
                if ($promotionData && $promotionData->getLandinganchorone()) {
                    return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'promotions'.$promotionData->getLandinganchorone();
                }
            }else{
                if ($promotionData && $promotionData->getLandinganchortwo()) {
                    return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'promotions'.$promotionData->getLandinganchortwo();
                }
            }
            
        }

        return $imageURL;
    }

    public function getPromoCode(){
        return $this->getRequest()->getParam($this->_helper->getPromoCodeUrlParam(), false);
    }

}