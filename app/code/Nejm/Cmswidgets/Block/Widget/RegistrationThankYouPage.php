<?php 
namespace Nejm\Cmswidgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Nejm\Cmswidgets\Helper\Alerts;
use Magento\Framework\App\Config\ScopeConfigInterface;

 
class RegistrationThankYouPage extends Template implements BlockInterface {

    protected $_template = "widget/registration_thankyou_page.phtml";

    /**
     * @var Nejm\Cmswidgets\Helper\Alerts
     */
    protected $_alerts;

    /**
     * @var Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Alerts $alerts,
        ScopeConfigInterface $scopeConfig,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_alerts = $alerts;
        $this->_scopeConfig = $scopeConfig;
    }


    public function getReturnURL(){
        $returnURLParam = $this->getData('returnURLParam');
        $returnURL = $this->getRequest()->getParam($returnURLParam, false);

        if($returnURL){
            return $returnURL;
        }

        return $this->getData('defaultReturnURL');
    }

    public function getButtonName(){
        $returnURLParam = $this->getData('returnURLParam');
        $returnURL = $this->getRequest()->getParam($returnURLParam, false);

        if($returnURL){
            if(str_contains($returnURL, 'doi') || str_contains($returnURL, '/do/')){
                return 'CONTINUE READING';
            }
        }

        return 'BACK TO SITE';
    }

    public function getAlerts(){
        $alertsParam = $this->getData('alertsParam');

        if($this->getRequest()->getParam($alertsParam, false)){
            $query  = explode('&', $_SERVER['QUERY_STRING']);
            $params = array();
            $alertsData = array();

            foreach( $query as $param )
            {
                if (strpos($param, '=') === false) $param += '=';
                list($name, $value) = explode('=', $param, 2);
                $params[urldecode($name)][] = urldecode($value);   
            }

            foreach($params[$alertsParam] as $alertKey){
                $alertValue = $this->_alerts->getAlertsValue($alertKey);
                array_push($alertsData, $alertValue);
            }
            return $alertsData;
        }

        return false;
        
    }

    public function getMyAccountAlertLink(){
        $manageProfileLink = $this->_scopeConfig->getValue('akamai/page_urls/my_account_page_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $alertLink = $this->getData('alertLink');
        if($alertLink){
            return $manageProfileLink.'/'.$alertLink;
        }

        return $manageProfileLink.'/alerts';
    }
    

}