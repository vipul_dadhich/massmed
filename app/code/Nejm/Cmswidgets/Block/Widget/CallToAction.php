<?php 
namespace Nejm\Cmswidgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Nejm\Cmswidgets\Helper\Data;
use Magento\Catalog\Block\Product\ListProduct;

 
class CallToAction extends Template implements BlockInterface {

    protected $_template = "widget/call_to_action.phtml";

    /**
     * @var Nejm\Cmswidgets\Helper\Data
     */
    protected $_helper;

    /**
     * @var Magento\Catalog\Block\Product\ListProduct
     */
    protected $_catalogBlock;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Data $helper,
        \Magento\Catalog\Block\Product\ListProduct $catalogBlock,
        \Magento\Catalog\Model\Product\Option $optionFactory,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_helper = $helper;
        $this->_catalogBlock = $catalogBlock;
        $this->optionFactory = $optionFactory;
    }

    public function getProductData($sku){
        return $this->_helper-> getProductData($sku);
    }

    public function getAddToCartPostParams($product){
        return $this->_catalogBlock->getAddToCartPostParams($product);
    }

    public function getProductOptions($product){
        $optionValue = [];
        $options = $this->optionFactory->getProductOptionCollection($product) ?: [];
        if (count($options) > 0) {
            foreach ($options as $o) {
                foreach ($o->getValues() as $value) {

                    $terms = $value['default_title'];
                    if ($terms === $this->getData('term')) {
                        $optionValue = $value;
                        break;
                    }else if($value->getData('is_default')){
                        $optionValue = $value;
                    }

                    }
                }
            }
        return $optionValue;
    }

    public function getPromoCode(){
        return $this->getRequest()->getParam($this->_helper->getPromoCodeUrlParam(), false);
    }

}