<?php 
namespace Nejm\Cmswidgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Nejm\Cmswidgets\Helper\Data;

 
class FreeGift extends Template implements BlockInterface {

    protected $_template = "widget/free_gift.phtml";

    /**
     * @var Nejm\Cmswidgets\Helper\Data
     */
    protected $_helper;

    /**
     * @var Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Data $helper,
        \Magento\Catalog\Helper\Image $imageHelper,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_helper = $helper;
        $this->imageHelper = $imageHelper;
    }

    public function getFreeGiftData(){
        $promocode = $this->getPromoCode();
        $freeGiftData = array(
            'productData' => NULL,
            'class' => 'fg_hide',
        );
        
        if($promocode){
            $productData = $this->_helper->getFreeGiftProductData($promocode);

            if($productData){          
                $freeGiftData = array(
                    'productData' => array(
                        'subtitle' => $productData->getPdpTitle(),
                        'imageURL' => $this->imageHelper->init($productData, 'product_base_image')->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)->resize(350, null)->getUrl()
                    ),
                    'class' => 'fg_show',
                );
            }
        }

        return $freeGiftData;
    }

    public function getPromoCode(){
        return $this->getRequest()->getParam($this->_helper->getPromoCodeUrlParam(), false);
    }

}