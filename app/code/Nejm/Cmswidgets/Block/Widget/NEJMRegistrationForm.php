<?php 
namespace Nejm\Cmswidgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

 
class NEJMRegistrationForm extends Template implements BlockInterface {

    protected $_template = "widget/nejm_registration_form.phtml";

}