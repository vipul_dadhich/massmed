<?php
/**
 * Connect SignUp Widget
 *
 * @package     Nejm_Cmswidgets
 * @author      Himanshu Tongya
 * @copyright   Copyright (c) 2019 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */

namespace Nejm\Cmswidgets\Block\Widget;

/**
 * Magento Widget
 *
 */

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

/**
 * ConnectSignUpForm class 
 * 
 * creating widget in backend to use in cms page.
 */

class ConnectSignUpForm extends Template implements BlockInterface {

    /**
    * @var $_template  for rendering the template 
    *
    */
    protected $_template = "widget/connect_signup_form.phtml";

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Store\Model\ScopeConfigInterface
     */
    protected $_scopeConfig;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,\Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
    }

}