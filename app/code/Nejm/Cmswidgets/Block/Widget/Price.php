<?php
namespace Nejm\Cmswidgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Nejm\Cmswidgets\Helper\Data;
use MMS\PriceEngine\Service\PriceEngine;

/**
 * Class Price
 * @package Nejm\Cmswidgets\Block\Widget
 */
class Price extends Template implements BlockInterface
{
    protected $productData;

    protected $_template = "widget/price.phtml";

    /**
     * @var \Nejm\Cmswidgets\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Catalog\Block\Product\ListProduct
     */
    protected $listProductBlock;

    /**
     * @var PriceEngine
     */
    protected $_priceEngineService;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $pricingHelper;

    /**
     * Price constructor.
     * @param Template\Context $context
     * @param \Magento\Catalog\Block\Product\ListProduct $listProductBlock
     * @param Data $helper
     * @param PriceEngine $priceEngineService
     * @param \Magento\Framework\Pricing\Helper\Data $pricingHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Block\Product\ListProduct $listProductBlock,
        Data $helper,
        PriceEngine $priceEngineService,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_helper = $helper;
        $this->listProductBlock = $listProductBlock;
        $this->_priceEngineService = $priceEngineService;
        $this->pricingHelper = $pricingHelper;
        $this->_storeManager = $storeManager;
    }

    /**
     * @return false|\Magento\Catalog\Api\Data\ProductInterface
     */
    public function getProduct()
    {
        if (is_null($this->productData)) {
            $this->productData = $this->_helper->getProductData($this->getData('productSku'));
        }

        return $this->productData;
    }

    /**
     * @param bool $format
     * @return float|string|null
     */
    public function getPriceData($format = true)
    {
        $productData = $this->getProduct();
        $term = $this->getData('term');
        if ($productData && $productData->getId()) {
            if ($this->_priceEngineService->isEnabled()) {
                $payload = [
                    'sku' => $productData->getSku()
                ];
                if ($term) {
                    $payload['term'] = $term;
                }
                $this->_priceEngineService->setRequest($payload);
                /* UCC-3104 have to specifically set this to get correct response format */
                $this->_priceEngineService->setFullResponse(false);
                $this->_priceEngineService->buildPrice();
                $finalPrice = $this->_priceEngineService->getPrice();

                if (!$finalPrice) {
                    $finalPrice = $productData->getPrice();
                }

                if(is_numeric($finalPrice) && floor($finalPrice ) != $finalPrice){
                    $finalPrice = number_format($finalPrice, 2);
                }

                if ($format) {
                    return $this->pricingHelper->currency($finalPrice,true,true);
                } else {
                    return number_format($finalPrice, 2);
                }
            }
        }

        return null;
    }

    /**
     * @param bool $format
     * @return float|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getRegularPrice($format = true)
    {
        $productData = $this->getProduct();
        $term = $this->getData('term');
        if($productData && $productData->getId()){
            if ($this->_storeManager->getStore()->getCurrentCurrencyCode() == 'AUD') {
                $regularPrice = $productData->getDefaultPriceAud();
            } elseif($this->_storeManager->getStore()->getCurrentCurrencyCode() == 'CAD') {
                $regularPrice = $productData->getDefaultPriceCad();
            } elseif($this->_storeManager->getStore()->getCurrentCurrencyCode() == 'EUR') {
                $regularPrice = $productData->getDefaultPriceEur();
            } elseif($this->_storeManager->getStore()->getCurrentCurrencyCode() == 'GBP') {
                $regularPrice = $productData->getDefaultPriceGbp();
            } else {
                $regularPrice = $productData->getPrice();
            }

            if($term){
                if ($productData->getData('has_options')) {
                    foreach($productData->getOptions() as $o){
                        if(strtolower($o->getTitle()) == 'term' || strtolower($o->getTitle()) == 'terms'){
                            foreach($o->getValues() as $value){
                                if(strtolower($value->getTitle()) == strtolower($term)){
                                    $regularPrice += $value->getPrice();
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        $regularPrice = number_format($regularPrice, 2);
        if ($format) {
            return $this->pricingHelper->currency($regularPrice,true,true);
        }

        return $regularPrice;
    }

    /**
     * @return string
     */
    public function getStrikeThroughClass()
    {
        if ($this->getData('strikeThroughRequired') != "1") {
            return 'strikeThroughDisable';
        }
        return '';
    }
}
