<?php

namespace Nejm\Cmswidgets\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\ResourceModel\Quote;

/**
 * Class AddFreeGiftProduct
 * @package Nejm\Cmswidgets\Observer
 */
class AddFreeGiftProduct implements ObserverInterface
{
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Nejm\Cmswidgets\Helper\Data
     */
    protected $_helper;
    /**
     * @var Quote
     */
    protected $_quoteResource;

    /**
     * AddFreeGiftProduct constructor.
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Nejm\Cmswidgets\Helper\Data $helper
     * @param Quote $quoteResource
     */
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Nejm\Cmswidgets\Helper\Data $helper,
        Quote $quoteResource
    ) {
        $this->_productFactory = $productFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_helper = $helper;
        $this->_quoteResource = $quoteResource;
    }

    /**
     * @param Observer $observer
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        $request = $observer->getEvent()->getRequest();
        $promoCode = $request->getParam('promocode', false);
        $defaultFreeGiftSku = $request->getParam('freegiftsku', false);
        $product = false;

        if($defaultFreeGiftSku){
            $product = $this->_productFactory->create()->loadByAttribute('sku', $defaultFreeGiftSku);
        }
        else if ($promoCode) {
            $productSku = $this->_helper->getSKUFromPromotion($promoCode);
            if ($productSku) {
                $product = $this->_productFactory->create()->loadByAttribute('sku', $productSku); 
            }
        }

        if ($product && $product->getId()) {
            $product = $this->_productFactory->create()->load($product->getId());
            try {
                $quote = $this->getCheckoutSession()->getQuote();
                $quote->addProduct($product, 1);
                $quote->setTotalsCollectedFlag(false);
                $quote->collectTotals();
                $this->_quoteResource->save($quote);

            } catch (\Exception $e) {
                throw new LocalizedException(__($e->getMessage()));
            }
        }
    }

    /**
     * @return \Magento\Checkout\Model\Session
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}
