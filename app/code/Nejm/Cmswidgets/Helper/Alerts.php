<?php
namespace Nejm\Cmswidgets\Helper;

class Alerts extends \Magento\Framework\App\Helper\AbstractHelper{
    public function getAlertsValue($alert){
        $availableMapping = array(
            'nejmAllergyImmunology' => 'Allergy/Immunology',
            'nejmNeurologyNeurosurgery' => 'Neurology/Neurosurgery',
            'nejmPrimaryCareHospitalist' => 'Primary Care/Hospitalist',
            'nejmNephrology' => 'Nephrology',
            'nejmPsychiatry' => 'Psychiatry',
            'nejmPediatrics' => 'Pediatrics',
            'nejmGastroenterology' => 'Gastroenterology',
            'nejmRecentlyPublished' => 'Recently Published',
            'nejmDermatology' => 'Dermatology',
            'nejmObstetricsGynecology' => 'Obstetrics/Gynecology',
            'nejmRheumatology' => 'Rheumatology',
            'nejmHealthPolicy' => 'Health Policy',
            'nejmCardiology' => 'Cardiology',
            'nejmSurgery' => 'Surgery',
            'nejmGeriatricsAging' => 'Geriatrics/Aging',
            'nejmPulmonaryCriticalCare' => 'Pulmonary/Critical Care',
            'nejmCareerCenter' => 'NEJM CareerCenter Alerts',
            'nejmEndocrinology' => 'Endocrinology',
            'nejmGenetics' => 'Genetics',
            'nejmInfectiousDisease' => 'Infectious Disease',
            'nejmHematologyOncology' => 'Hematology/Oncology',
            'nejmEmergencyMedicine' => 'Emergency Medicine',
            'nejmNewNEJM' => 'New@NEJM.org',
            'nejmTOC' => 'Weekly Table of Contents',
            'nejmWeeklyResidentBriefing' => 'Weekly Resident Briefing',
            'nejmGeneralInformation' => 'General Information',
            'catalystEditorsPicks' => 'NEJM Catalyst - Editors Picks',
            'catalystInsightsCouncil' => 'NEJM Catalyst - Insights Council',
            'catalystConnect' => 'NEJM Catalyst - Connect',
            'catalystSOI' => 'NEJM Catalyst - General Information',
        );
		if (isset($availableMapping[$alert])) {
			return $availableMapping[$alert];
		}
		return false;
    }
}
