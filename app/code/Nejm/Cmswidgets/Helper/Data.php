<?php

namespace Nejm\Cmswidgets\Helper;

use Magento\Checkout\Model\Session as CheckoutSession;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	/**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
    */

    /**
     * @var \Magento\Store\Model\ScopeConfigInterface
     */
    protected $_scopeConfig;

	public function __construct(
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \MMS\Promotions\Helper\Data $promotionHelper,
        CheckoutSession $checkoutSession
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_productRepository = $productRepository;
        $this->promotionHelper  = $promotionHelper;
        $this->checkoutSession = $checkoutSession;
        return parent::__construct($context);
    }

    public function getFreeGiftProductData($promocode){
        $sku = $this->getSKUFromPromotion($promocode);
        if($sku){
            try{
                return $this->_productRepository->get($sku);
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function getSKUFromPromotion($promocode){
        $promotionData = $this->promotionHelper->getPromotionByPromoCode($promocode);

        if(!empty($promotionData)){
            $cartItems = $this->checkoutSession->getQuote()->getAllItems();
            $freeGiftEligibilty = false;
            $promotionsSkus = explode(',', $promotionData->getFreeGiftEligibleSkus());
            if (!empty($promotionsSkus)) {
                foreach ($cartItems as $cItem) {
                    if (in_array($cItem->getSku(), $promotionsSkus)) {
                        $freeGiftEligibilty = true;
                    }
                }
            }
            if ($freeGiftEligibilty && $promotionData->getFreeGiftSku()) {
                return $promotionData->getFreeGiftSku();
            }
        }

        return false ;
    }

    public function getPromotionData($promocode){
        return $this->promotionHelper->getPromotionByPromoCode($promocode);
    }

    public function getPromoCodeUrlParam(){
        return  $this->_scopeConfig->getValue('promocode/settings/url_parameter',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getProductData($sku){
        if($sku){
            try{
                return $this->_productRepository->get($sku);
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }


}
