<?php
/**
 * @package     Nejm_ThemeExtend
 * @author      Dinesh Arya
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Nejm_ThemeExtend',
    __DIR__
);
