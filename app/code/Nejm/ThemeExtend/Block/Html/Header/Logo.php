<?php
/**
 * @package     Nejm_ThemeExtend
 * @author      Dinesh Arya
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */

namespace Nejm\ThemeExtend\Block\Html\Header;

use Magento\Framework\View\Asset\Repository;
use Magento\Framework\View\Element\Template\Context;
use Magento\MediaStorage\Helper\File\Storage\Database;

/**
 * Class Logo
 * @package Nejm\ThemeExtend\Block\Html\Header
 */
class Logo extends \Magento\Theme\Block\Html\Header\Logo
{
    /**
     * @var Repository
     */
    protected $_assetRepo;

    /**
     * Logo constructor.
     * @param Context $context
     * @param Database $fileStorageHelper
     * @param Repository $assetRepo
     * @param array $data
     */
    public function __construct(
        Context $context,
        Database $fileStorageHelper,
        Repository $assetRepo,
        array $data = []
    ) {
        $this->_assetRepo = $assetRepo;
        parent::__construct($context, $fileStorageHelper, $data);
    }

    /**
     * Retrieve logo image URL
     *
     * @return string
     */
    protected function _getLogoUrl()
    {
        return $this->_assetRepo->getUrl('Nejm_ThemeExtend::image/nejm-logo.png');
    }

    /**
     * Retrieve logo width
     *
     * @return int
     */
    public function getLogoWidth()
    {
        return 514;
    }

    /**
     * Retrieve logo height
     *
     * @return int
     */
    public function getLogoHeight()
    {
        return 60;
    }
}
