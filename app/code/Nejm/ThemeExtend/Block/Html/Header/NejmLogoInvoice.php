<?php
/**
 * @package     Nejm_ThemeExtend
 * @author      Dinesh Arya
 * @copyright   Copyright (c) 2020 Massachusetts Medical Society. All rights reserved
 * @license     Commercial
 */

namespace Nejm\ThemeExtend\Block\Html\Header;

use Magento\Framework\Phrase;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\View\Element\Template;

/**
 * Class Logo
 * @package Nejm\ThemeExtend\Block\Html\Header
 */
class NejmLogoInvoice extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Repository
     */
    protected $_assetRepo;

    /**
     * NejmLogo constructor.
     * @param Template\Context $context
     * @param Repository $assetRepo
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Repository $assetRepo,
        array $data = []
    ) {
        $this->_assetRepo = $assetRepo;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->_assetRepo->getUrl('Nejm_ThemeExtend::image/logo.png');
    }
	
}
