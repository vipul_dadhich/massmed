<?php

namespace Magedelight\Firstdata\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $installer = $setup;
        $installer->startSetup();
        $tableName = $installer->getTable('md_firstdata');

        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $installer->getConnection()->addColumn(
                    $tableName,
                    'website_id',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'length' => 10,
                        'nullable' => true,
                        'afters' => 'card_id',
                        'comment' => 'Customer Website Id'
                    ]
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.9', '<')) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $installer->getConnection()->modifyColumn(
                    $tableName,
                    'card_id',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'length' => 10,
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'comment' => 'Card Id'
                    ]
                );
            }
        }

        $setup->endSetup();
    }
}
