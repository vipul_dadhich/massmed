<?php
namespace Pitbulk\SAML2\Observer;

use Magento\Authorization\Model\RoleFactory;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface as MessageManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\ConfigurationMismatchException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\User\Model\UserFactory;
use Magento\Security\Model\AdminSessionsManager;

use Pitbulk\SAML2\Helper\Data;

class AdminLoginObserver implements ObserverInterface
{
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var UserFactory
     */
    private $userFactory;
    /**
     * @var MessageManager
     */
    private $messageManager;
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var EventManager
     */
    private $eventManager;
    /**
     * @var AdminSessionsManager
     */
    private $adminSessionsManager;
    /**
     * @var DateTime
     */
    private $dateTime;
    /**
     * @var EncryptorInterface
     */
    private $encryptor;
    /**
     * @var RoleFactory
     */
    private $roleFactory;

    public function __construct(
        Data $helper,
        Session $session,
        RequestInterface $request,
        UserFactory $userFactory,
        MessageManager $messageManager,
        Registry $registry,
        EventManager $eventManager,
        AdminSessionsManager $adminSessionsManager,
        DateTime $dateTime,
        EncryptorInterface $encryptor,
        RoleFactory $roleFactory
    ) {
        $this->helper = $helper;
        $this->session = $session;
        $this->request = $request;
        $this->userFactory = $userFactory;
        $this->messageManager = $messageManager;
        $this->registry = $registry;
        $this->eventManager = $eventManager;
        $this->adminSessionsManager = $adminSessionsManager;
        $this->dateTime = $dateTime;
        $this->encryptor = $encryptor;
        $this->roleFactory = $roleFactory;
    }

    public function execute(Observer $observer)
    {
        if (null === $this->request->getParam("SAMLResponse") ||
            $this->registry->registry('admin_login_observer_fired')) {
            return;
        }

        // Prevent if already logged
        if ($this->session->isLoggedIn()) {
            return;
        }

        $this->registry->register('admin_login_observer_fired', true);

        try {
            $moduleEnabled = $this->helper->checkEnabledModule('backend');
            if (!$moduleEnabled) {
                throw new ConfigurationMismatchException(__('SAML Backend module has disabled status'));
            }

            $auth = $this->helper->getAuth('backend');
            $auth->processResponse();
            $errors = $auth->getErrors();
            if (!empty($errors)) {
                $errorMsg = "Error at the Backend ACS Endpoint.<br>".implode(', ', $errors);
                $debug = $this->helper->getConfig('pitbulk_saml2_admin/advanced/debug');
                $reason = $auth->getLastErrorReason();
                if ($debug && isset($reason) && !empty($reason)) {
                    $errorMsg .= '<br><br>Reason: '.$reason;
                }
                throw new AuthenticationException(__($errorMsg));
            } elseif (!$auth->isAuthenticated()) {
                throw new AuthenticationException(__("Backend ACS Process failed"));
            }

            $this->handleValidSamlResponse($auth);
        } catch (AuthenticationException $e) {
            $this->_processError($e->getMessage());
        } catch (\Exception $e) {
            $this->_processError($e->getMessage());
        }
    }

    private function handleValidSamlResponse($auth)
    {
        $userData = $this->processAttrs($auth);

        $user = $this->tryGetUser($userData);

        if (!empty($user) && $user->getId()) {
            if ($user->getIsActive() != '1') {
                throw new AuthenticationException(
                    __('You did not sign in correctly or your account is temporarily disabled.')
                );
            }
            $user = $this->updateUser($user, $userData);

            if (!$user->hasAssigned2Role($user->getId())) {
                throw new AuthenticationException(__('You need more permissions to access this.'));
            }
        } else {
            $user = $this->provisionUser($userData);
        }

        if (!empty($user)) {
            $this->registerUserSession($auth, $user);
        }
    }

    /**
     * User finder
     *
     */
    private function tryGetUser($userData)
    {
        $user = null;
        $useridfield = $this->helper->getConfig('pitbulk_saml2_admin/options/useridfield');
        if (empty($useridfield)) {
            $useridfield = 'email';
        }

        if (!isset($userData[$useridfield])) {
            $errorMsg = "The field to identify the user is the '".$useridfield."' but IdP is not providing it";
                throw new ConfigurationMismatchException(__($errorMsg));
        }
        
        $user = $this->userFactory->create()->load($userData[$useridfield], $useridfield);

        return $user;
    }

    /**
     * Provision user
     *
     */
    private function provisionUser($userData)
    {
        $autocreate = $this->helper->getConfig('pitbulk_saml2_admin/options/autocreate');
        if ($autocreate) {
            try {
                $user = $this->userFactory->create();

                $user->setUserName($userData['username']);
                $user->setEmail($userData['email']);
                $user->setFirstname($userData['firstname']);
                $user->setLastname($userData['lastname']);
                $password = $this->encryptor->getHash('admin-password', true);
                $user->setPassword($password);
                if (empty($userData['roleid'])) {
                    $requireRoleForJit = $this->helper->getConfig('pitbulk_saml2_admin/options/requireroleforjit');
                    if ($requireRoleForJit) {
                        $errorMsg = 'The login could not be completed, user ' . $userData['email'] .
                        ' does not exist in Magento and the auto-provisioning' .
                        " function can't be executed because role info was not " .
                        "provided and the extension is configured to require it";
                        throw new AuthenticationException(__($errorMsg));
                    }

                    $defaultRole = $this->helper->getConfig('pitbulk_saml2_admin/options/defaultrole');
                    if (empty($defaultRole)) {
                        $defaultRole = 1;
                    }
                    $userData['roleid'] = $defaultRole;
                }

                if (!empty($userData['roleid'])) {
                    $role = $this->roleFactory->create()->load($userData['roleid']);
                    if (!isset($role) || empty($role->getData())) {
                        $errorMsg = 'The login could not be completed, user ' . $userData['email'] .
                        ' does not exist in Magento and the auto-provisioning' .
                        " function can't be executed because role info " .
                        "provided was wrong, contact the administrator and ask for review role mapping settings";
                        throw new AuthenticationException(__($errorMsg));
                    } else {
                        $user->save();
                        $user->setRoleId($userData['roleid']);
                    }
                }

                $user->save();

                $successMsg = __('User registration successful.');
                $this->messageManager->addSuccess($successMsg);
                return $user;
            } catch (Exception $e) {
                throw new CouldNotSaveException(__('The auto-provisioning process failed: ') .$e->getMessage());
            }
        } else {
            $errorMsg = 'The login could not be completed, user ' . $userData['email'] .
                        ' does not exist in Magento and the auto-provisioning' .
                        ' function is disabled';
            throw new AuthenticationException(__($errorMsg));
        }
    }

    /**
     * Update user
     *
     */
    private function updateUser($user, $userData)
    {
        $updateuser = $this->helper->getConfig('pitbulk_saml2_admin/options/updateuser');
        if ($updateuser) {
            if (!empty($userData['firstname'])) {
                $user->setFirstname($userData['firstname']);
            }
            if (!empty($userData['lastname'])) {
                $user->setLastname($userData['lastname']);
            }
            if (!empty($userData['roleid'])) {
                $role = $this->roleFactory->create()->load($userData['roleid']);
                if (isset($role) && !empty($role->getData())) {
                    $user->setRoleId($userData['roleid']);
                }
            }
            $user->save();
        }
        return $user;
    }

    /**
     * Register user session
     *
     */
    private function registerUserSession($auth, $user)
    {
        $this->session->setUser($user);
        $this->adminSessionsManager->getCurrentSession()->load($this->session->getSessionId());
        $sessionInfo = $this->adminSessionsManager->getCurrentSession();
        $sessionInfo->setUpdatedAt($this->dateTime->gmtTimestamp());
        $sessionInfo->setStatus($sessionInfo::LOGGED_IN);
        $sessionInfo->save();
        $this->adminSessionsManager->processLogin();

        $nameId = $auth->getNameId();
        $nameIdFormat = $auth->getNameIdFormat();
        $sessionIndex = $auth->getSessionIndex();

        $user->setSamlLogin(true);
        $user->setSamlNameId($nameId);
        $user->setSamlNameFormat($nameIdFormat);
        $user->setSamlSessionIndex($sessionIndex);

        $this->eventManager->dispatch(
            'backend_auth_user_login_success',
            ['user' => $user]
        );
    }

    /**
     * Process SAML Attributes
     *
     */
    private function processAttrs($auth)
    {
        $userData = [
            'username' => '',
            'email' => '',
            'firstname' => '',
            'lastname' => '',
            'roleid' => ''
        ];

        $attrs = $auth->getAttributes();

        if (empty($attrs)) {
            $userData['username'] = $auth->getNameId();
            $userData['email'] = $auth->getNameId();
        } else {
            $mapping = $this->getAttrMapping();

            foreach (['username', 'email', 'firstname', 'lastname'] as $key) {
                if (!empty($mapping[$key]) && isset($attrs[$mapping[$key]])
                  && !empty($attrs[$mapping[$key]][0])) {
                    $userData[$key] = $attrs[$mapping[$key]][0];
                }
            }

            if (!empty($mapping['role']) && isset($attrs[$mapping['role']])
              && !empty($attrs[$mapping['role']])) {
                $roleMapping = $this->getRoleMapping();
                $roleValues = $attrs[$mapping['role']];
                $roleid = $this->obtainRoleId($roleValues, $roleMapping);

                if ($roleid !== false) {
                    $userData['roleid'] = $roleid;
                }
            }
        }
        return $userData;
    }

    /**
     * Aux method for get role mapping
     *
     */
    private function getRoleMapping()
    {
        $roleMapping = [];
        for ($i=1; $i<51; $i++) {
            $key = 'pitbulk_saml2_admin/role_mapping/role'.$i;
            $maps = $this->helper->getConfig($key);
            $roleMapping[$i] = explode(',', $maps);
        }

        return $roleMapping;
    }

    /**
     * Aux method for get attribute mapping
     *
     */
    private function getAttrMapping()
    {
        $mapping = [];

        $attrMapKey = 'pitbulk_saml2_admin/attr_mapping/';

        $mapping['username'] =  $this->helper->getConfig($attrMapKey.'username');
        $mapping['email'] =  $this->helper->getConfig($attrMapKey.'email');
        $mapping['firstname'] =  $this->helper->getConfig($attrMapKey.'firstname');
        $mapping['lastname'] =  $this->helper->getConfig($attrMapKey.'lastname');
        $mapping['role'] = $this->helper->getConfig($attrMapKey.'role');

        return $mapping;
    }

    /**
     * Aux method for calculating roleid
     *
     */
    private function obtainRoleId($samlRoles, $roleValues)
    {
        foreach ($samlRoles as $samlRole) {
            for ($i=1; $i<51; $i++) {
                if (in_array($samlRole, $roleValues[$i])) {
                    return $i;
                }
            }
        }
        return false;
    }

    private function _processError($errorMsg)
    {
        $this->messageManager->addErrorMessage($errorMsg);
    }
}
