<?php
/**
 * SAML Extension for Magento2.
 *
 * @package     Pitbulk_SAML2
 * @copyright   Copyright (c) 2019 Sixto Martin Garcia (http://saml.info)
 * @license     Commercial
 */

namespace Pitbulk\SAML2\Block\System\Config\Form\Field;

use \Magento\Config\Block\System\Config\Form\Field;
use \Magento\Framework\Data\Form\Element\AbstractElement;
use \Magento\Framework\UrlInterface;
use \Magento\Store\Model\ScopeInterface;

class Metadataurlbackend extends Field
{
    public function _getElementHtml(AbstractElement $element)
    {
        $store = $this->_storeManager->getStore();
        $metadataUrladmin = $store->getBaseUrl(UrlInterface::URL_TYPE_LINK);
        $metadataUrladmin .= "sso/saml2/backendmetadata";

        $license = $this->_scopeConfig->getValue(
            'pitbulk_saml2_admin/status/license',
            ScopeInterface::SCOPE_STORE
        );

        $licenseHtml = "";

        if (!empty($license)) {
            $licenseHtml = '&license='.urlencode($license);
        }

        $html = '<label id="' . $element->getHtmlId() .
                     '" name="' . $element->getName() .
                     '"></label><a target="_blank" 
                    href="'.$metadataUrladmin.'">'.$metadataUrladmin.'</a>';

        $url = 'https://logs-01.loggly.com/inputs/'.
               '30c06b29-45ba-411f-a89c-60a9eb5f8e22.gif'.
               '?source=pixel'.$licenseHtml.'&extension=magento2admin';

        $html .= '<img style="width: 0px;height: 0px;" src="'.$url.'" />';

        return $html;
    }

    /**
     * Render button
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        // Remove scope label
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }
}
