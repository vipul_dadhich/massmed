<?php
/**
 * SAML Extension for Magento2.
 *
 * @package     Pitbulk_SAML2
 * @copyright   Copyright (c) 2019 Sixto Martin Garcia (http://saml.info)
 * @license     Commercial
 */

namespace Pitbulk\SAML2\Controller\Adminhtml\Saml2;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Auth\Session;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\UrlInterface as BackendUrl;

use Pitbulk\SAML2\Helper\Data;

class Logout extends Action
{
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Session
     */
    private $session;

    public function __construct(
        Context $context,
        Session $session,
        Data $helper
    ) {
        $this->_publicActions = ['logout'];
        $this->helper = $helper;
        $this->session = $session;

        parent::__construct($context);

        $this->_whitelistEndpoint();
    }

    public function _whitelistEndpoint()
    {
        // CSRF Magento2.3 compatibility
        if (interface_exists("\Magento\Framework\App\CsrfAwareActionInterface")) {
            $request = $this->getRequest();
            if ($request instanceof HttpRequest && empty($request->getParam(BackendUrl::SECRET_KEY_PARAM_NAME))) {
                $request->setParam(BackendUrl::SECRET_KEY_PARAM_NAME, $this->_backendUrl->getSecretKey());
            }
        }
    }

    public function execute()
    {
        $errorMsg = null;

        $moduleEnabled = $this->helper->checkEnabledModule('backend');
        if ($moduleEnabled) {
            $sloEnabled = $this->helper->getConfig('pitbulk_saml2_admin/options/slo');
            if ($sloEnabled) {
                if ($this->session->isLoggedIn()) {
                    $samlLogin = $this->session->getData('backend_saml_login');

                    if ($samlLogin) {
                        $auth = $this->helper->getAuth('backend');

                        $redirectTo =  $this->_helper->getHomePageUrl();
                        $auth->logout(
                            $redirectTo,
                            [],
                            $this->session->getData('backend_saml_nameid'),
                            $this->session->getData('backend_saml_sessionindex'),
                            false,
                            $this->session->getData('backend_saml_nameid_format')
                        );
                    } else {
                        $errorMsg = "You tried to start a Backend SLO process but you" .
                                    " are not logged via SAML";
                    }
                } else {
                        $errorMsg = "You tried to start a Backend SLO process but you" .
                                    " are not logged";
                }
            } else {
                $errorMsg = "You tried to start a Backend SLO process but this" .
                            " functionality is disabled";
            }
        } else {
            $errorMsg = "You tried to start a Backend SLO process but SAML2 module" .
                        " has disabled status";
        }

        if (isset($errorMsg)) {
            $this->_processError($errorMsg);
        }
    }

    private function _processError($errorMsg)
    {
        $this->messageManager->addErrorMessage($errorMsg);
        return $this->_redirect($this->_backendUrl->getStartupPageUrl());
    }
}
