<?php
namespace Magedelight\Firstdata\Model\ConfigProvider;

/**
 * Interceptor class for @see \Magedelight\Firstdata\Model\ConfigProvider
 */
class Interceptor extends \Magedelight\Firstdata\Model\ConfigProvider implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Payment\Model\CcConfig $ccConfig, \Magento\Payment\Helper\Data $paymentHelper, \Magedelight\Firstdata\Model\Config $config, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Customer\Model\Session $customerSession, \Magento\Payment\Model\Config $paymentConfig, \Magento\Backend\Model\Session\Quote $sessionquote, \Magedelight\Firstdata\Helper\Data $dataHelper, \Magento\Framework\Encryption\Encryptor $encryptor, \Magedelight\Firstdata\Model\CardsFactory $cardFactory, array $methodCodes = [])
    {
        $this->___init();
        parent::__construct($ccConfig, $paymentHelper, $config, $checkoutSession, $customerSession, $paymentConfig, $sessionquote, $dataHelper, $encryptor, $cardFactory, $methodCodes);
    }

    /**
     * {@inheritdoc}
     */
    public function getStoredCards()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getStoredCards');
        if (!$pluginInfo) {
            return parent::getStoredCards();
        } else {
            return $this->___callPlugins('getStoredCards', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function canSaveCard()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'canSaveCard');
        if (!$pluginInfo) {
            return parent::canSaveCard();
        } else {
            return $this->___callPlugins('canSaveCard', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCcMonths()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCcMonths');
        if (!$pluginInfo) {
            return parent::getCcMonths();
        } else {
            return $this->___callPlugins('getCcMonths', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function show3dSecure()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'show3dSecure');
        if (!$pluginInfo) {
            return parent::show3dSecure();
        } else {
            return $this->___callPlugins('show3dSecure', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getConfig');
        if (!$pluginInfo) {
            return parent::getConfig();
        } else {
            return $this->___callPlugins('getConfig', func_get_args(), $pluginInfo);
        }
    }
}
