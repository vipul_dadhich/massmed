<?php
namespace MMS\CurrencyEngine\Api\Quote\Data;

/**
 * Factory class for @see \MMS\CurrencyEngine\Api\Quote\Data\PriceFormatInterface
 */
class PriceFormatInterfaceFactory
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Factory constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\MMS\\CurrencyEngine\\Api\\Quote\\Data\\PriceFormatInterface')
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param array $data
     * @return \MMS\CurrencyEngine\Api\Quote\Data\PriceFormatInterface
     */
    public function create(array $data = [])
    {
        return $this->_objectManager->create($this->_instanceName, $data);
    }
}
