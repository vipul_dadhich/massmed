<?php
namespace MMS\Renew\Helper\Config;

/**
 * Interceptor class for @see \MMS\Renew\Helper\Config
 */
class Interceptor extends \MMS\Renew\Helper\Config implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Helper\Context $context, \Magento\Customer\Model\SessionFactory $customerSessionFactory, \Magento\Customer\Model\Session $customerSession, \Ey\MuleSoft\Model\Catalyst\Api\GetCustomerData $getCustomerApi, \Magento\Framework\Json\Helper\Data $jsonHelper, \Magento\Framework\View\LayoutInterface $layout, \Ey\URLParams\Helper\Data $urlHelper, \MMS\Promotions\Helper\Data $promotionHelper, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Catalog\Model\ProductFactory $productFactory)
    {
        $this->___init();
        parent::__construct($context, $customerSessionFactory, $customerSession, $getCustomerApi, $jsonHelper, $layout, $urlHelper, $promotionHelper, $checkoutSession, $productFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled($storeId = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isEnabled');
        if (!$pluginInfo) {
            return parent::isEnabled($storeId);
        } else {
            return $this->___callPlugins('isEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSkipEligibiltySkus($storeId = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSkipEligibiltySkus');
        if (!$pluginInfo) {
            return parent::getSkipEligibiltySkus($storeId);
        } else {
            return $this->___callPlugins('getSkipEligibiltySkus', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isDefaultValueActive($store = null) : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isDefaultValueActive');
        if (!$pluginInfo) {
            return parent::isDefaultValueActive($store);
        } else {
            return $this->___callPlugins('isDefaultValueActive', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRenewAllowedPaymentMethods($store = null) : array
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRenewAllowedPaymentMethods');
        if (!$pluginInfo) {
            return parent::getRenewAllowedPaymentMethods($store);
        } else {
            return $this->___callPlugins('getRenewAllowedPaymentMethods', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isShowTopPositionBlock($store = null) : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isShowTopPositionBlock');
        if (!$pluginInfo) {
            return parent::isShowTopPositionBlock($store);
        } else {
            return $this->___callPlugins('isShowTopPositionBlock', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTopPositionBlock($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getTopPositionBlock');
        if (!$pluginInfo) {
            return parent::getTopPositionBlock($store);
        } else {
            return $this->___callPlugins('getTopPositionBlock', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTopPositionBlockHtml($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getTopPositionBlockHtml');
        if (!$pluginInfo) {
            return parent::getTopPositionBlockHtml($store);
        } else {
            return $this->___callPlugins('getTopPositionBlockHtml', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBottomPositionBlockHtml($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBottomPositionBlockHtml');
        if (!$pluginInfo) {
            return parent::getBottomPositionBlockHtml($store);
        } else {
            return $this->___callPlugins('getBottomPositionBlockHtml', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isShowBottomPositionBlock($store = null) : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isShowBottomPositionBlock');
        if (!$pluginInfo) {
            return parent::isShowBottomPositionBlock($store);
        } else {
            return $this->___callPlugins('isShowBottomPositionBlock', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBottomPositionBlock($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBottomPositionBlock');
        if (!$pluginInfo) {
            return parent::getBottomPositionBlock($store);
        } else {
            return $this->___callPlugins('getBottomPositionBlock', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isFreeGiftEnabled($store = null) : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isFreeGiftEnabled');
        if (!$pluginInfo) {
            return parent::isFreeGiftEnabled($store);
        } else {
            return $this->___callPlugins('isFreeGiftEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFreeGiftProductSku($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getFreeGiftProductSku');
        if (!$pluginInfo) {
            return parent::getFreeGiftProductSku($store);
        } else {
            return $this->___callPlugins('getFreeGiftProductSku', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabledRenewConfirmation($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isEnabledRenewConfirmation');
        if (!$pluginInfo) {
            return parent::isEnabledRenewConfirmation($store);
        } else {
            return $this->___callPlugins('isEnabledRenewConfirmation', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRenewOptionLimit($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRenewOptionLimit');
        if (!$pluginInfo) {
            return parent::getRenewOptionLimit($store);
        } else {
            return $this->___callPlugins('getRenewOptionLimit', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRenewStatus()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRenewStatus');
        if (!$pluginInfo) {
            return parent::getRenewStatus();
        } else {
            return $this->___callPlugins('getRenewStatus', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function customerIsLogin()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'customerIsLogin');
        if (!$pluginInfo) {
            return parent::customerIsLogin();
        } else {
            return $this->___callPlugins('customerIsLogin', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function goToHomePage()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'goToHomePage');
        if (!$pluginInfo) {
            return parent::goToHomePage();
        } else {
            return $this->___callPlugins('goToHomePage', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerData()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomerData');
        if (!$pluginInfo) {
            return parent::getCustomerData();
        } else {
            return $this->___callPlugins('getCustomerData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCheckEligibilityAPIURI()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCheckEligibilityAPIURI');
        if (!$pluginInfo) {
            return parent::getCheckEligibilityAPIURI();
        } else {
            return $this->___callPlugins('getCheckEligibilityAPIURI', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAkamaiAuthToken()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAkamaiAuthToken');
        if (!$pluginInfo) {
            return parent::getAkamaiAuthToken();
        } else {
            return $this->___callPlugins('getAkamaiAuthToken', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRenewStatusFromCustomerSession()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRenewStatusFromCustomerSession');
        if (!$pluginInfo) {
            return parent::getRenewStatusFromCustomerSession();
        } else {
            return $this->___callPlugins('getRenewStatusFromCustomerSession', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setRenewStatusToCustomerSession($renewStatus)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setRenewStatusToCustomerSession');
        if (!$pluginInfo) {
            return parent::setRenewStatusToCustomerSession($renewStatus);
        } else {
            return $this->___callPlugins('setRenewStatusToCustomerSession', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerAudienceType()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomerAudienceType');
        if (!$pluginInfo) {
            return parent::getCustomerAudienceType();
        } else {
            return $this->___callPlugins('getCustomerAudienceType', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerMediaAccess()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomerMediaAccess');
        if (!$pluginInfo) {
            return parent::getCustomerMediaAccess();
        } else {
            return $this->___callPlugins('getCustomerMediaAccess', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setRenewResponseData($response)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setRenewResponseData');
        if (!$pluginInfo) {
            return parent::setRenewResponseData($response);
        } else {
            return $this->___callPlugins('setRenewResponseData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRenewResponseData()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRenewResponseData');
        if (!$pluginInfo) {
            return parent::getRenewResponseData();
        } else {
            return $this->___callPlugins('getRenewResponseData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrentBrand()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCurrentBrand');
        if (!$pluginInfo) {
            return parent::getCurrentBrand();
        } else {
            return $this->___callPlugins('getCurrentBrand', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isBrandNejm($sku = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isBrandNejm');
        if (!$pluginInfo) {
            return parent::isBrandNejm($sku);
        } else {
            return $this->___callPlugins('isBrandNejm', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function filterRenewResponse($eligibilityInfo)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'filterRenewResponse');
        if (!$pluginInfo) {
            return parent::filterRenewResponse($eligibilityInfo);
        } else {
            return $this->___callPlugins('filterRenewResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function filterPurchaseResponse($eligibilityInfo)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'filterPurchaseResponse');
        if (!$pluginInfo) {
            return parent::filterPurchaseResponse($eligibilityInfo);
        } else {
            return $this->___callPlugins('filterPurchaseResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isModuleOutputEnabled');
        if (!$pluginInfo) {
            return parent::isModuleOutputEnabled($moduleName);
        } else {
            return $this->___callPlugins('isModuleOutputEnabled', func_get_args(), $pluginInfo);
        }
    }
}
