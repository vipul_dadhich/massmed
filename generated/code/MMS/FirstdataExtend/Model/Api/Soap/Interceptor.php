<?php
namespace MMS\FirstdataExtend\Model\Api\Soap;

/**
 * Interceptor class for @see \MMS\FirstdataExtend\Model\Api\Soap
 */
class Interceptor extends \MMS\FirstdataExtend\Model\Api\Soap implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magedelight\Firstdata\Model\Config $configModel, \Magento\Directory\Model\RegionFactory $regionFactory, \Magento\Directory\Model\CountryFactory $countryFactory, \Magento\Store\Model\StoreManager $storeManager, \Magento\Framework\App\RequestInterface $httpRequest, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, array $data = [])
    {
        $this->___init();
        parent::__construct($configModel, $regionFactory, $countryFactory, $storeManager, $httpRequest, $scopeConfig, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function createCustomerProfile()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'createCustomerProfile');
        if (!$pluginInfo) {
            return parent::createCustomerProfile();
        } else {
            return $this->___callPlugins('createCustomerProfile', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateCustomerProfile()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'updateCustomerProfile');
        if (!$pluginInfo) {
            return parent::updateCustomerProfile();
        } else {
            return $this->___callPlugins('updateCustomerProfile', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateCustomerProfileRequest()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'updateCustomerProfileRequest');
        if (!$pluginInfo) {
            return parent::updateCustomerProfileRequest();
        } else {
            return $this->___callPlugins('updateCustomerProfileRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareCaptureResponse(\Magento\Payment\Model\InfoInterface $payment, $amount, $transarmor = false)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'prepareCaptureResponse');
        if (!$pluginInfo) {
            return parent::prepareCaptureResponse($payment, $amount, $transarmor);
        } else {
            return $this->___callPlugins('prepareCaptureResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareCaptureRequest(\Magento\Payment\Model\InfoInterface $payment, $amount, $transarmor)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'prepareCaptureRequest');
        if (!$pluginInfo) {
            return parent::prepareCaptureRequest($payment, $amount, $transarmor);
        } else {
            return $this->___callPlugins('prepareCaptureRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareAuthorizeCaptureResponse(\Magento\Payment\Model\InfoInterface $payment, $amount, $transarmor = false)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'prepareAuthorizeCaptureResponse');
        if (!$pluginInfo) {
            return parent::prepareAuthorizeCaptureResponse($payment, $amount, $transarmor);
        } else {
            return $this->___callPlugins('prepareAuthorizeCaptureResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareAuthorizeCaptureRequest(\Magento\Payment\Model\InfoInterface $payment, $amount, $transarmor)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'prepareAuthorizeCaptureRequest');
        if (!$pluginInfo) {
            return parent::prepareAuthorizeCaptureRequest($payment, $amount, $transarmor);
        } else {
            return $this->___callPlugins('prepareAuthorizeCaptureRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareAuthorizeResponse(\Magento\Payment\Model\InfoInterface $payment, $amount, $transarmor = false)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'prepareAuthorizeResponse');
        if (!$pluginInfo) {
            return parent::prepareAuthorizeResponse($payment, $amount, $transarmor);
        } else {
            return $this->___callPlugins('prepareAuthorizeResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareAuthorizeRequest(\Magento\Payment\Model\InfoInterface $payment, $amount, $transarmor)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'prepareAuthorizeRequest');
        if (!$pluginInfo) {
            return parent::prepareAuthorizeRequest($payment, $amount, $transarmor);
        } else {
            return $this->___callPlugins('prepareAuthorizeRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareVoidResponse(\Magento\Payment\Model\InfoInterface $payment, $card)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'prepareVoidResponse');
        if (!$pluginInfo) {
            return parent::prepareVoidResponse($payment, $card);
        } else {
            return $this->___callPlugins('prepareVoidResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareRefundResponse(\Magento\Payment\Model\InfoInterface $payment, $amount, $realCaptureTransactionId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'prepareRefundResponse');
        if (!$pluginInfo) {
            return parent::prepareRefundResponse($payment, $amount, $realCaptureTransactionId);
        } else {
            return $this->___callPlugins('prepareRefundResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepareSoapForDebug($request, $response)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'prepareSoapForDebug');
        if (!$pluginInfo) {
            return parent::prepareSoapForDebug($request, $response);
        } else {
            return $this->___callPlugins('prepareSoapForDebug', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function array_to_xml($array, &$xml_user_info)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'array_to_xml');
        if (!$pluginInfo) {
            return parent::array_to_xml($array, $xml_user_info);
        } else {
            return $this->___callPlugins('array_to_xml', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setInputData($input = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setInputData');
        if (!$pluginInfo) {
            return parent::setInputData($input);
        } else {
            return $this->___callPlugins('setInputData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getInputData()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getInputData');
        if (!$pluginInfo) {
            return parent::getInputData();
        } else {
            return $this->___callPlugins('getInputData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setResponseData($response = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setResponseData');
        if (!$pluginInfo) {
            return parent::setResponseData($response);
        } else {
            return $this->___callPlugins('setResponseData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseData()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getResponseData');
        if (!$pluginInfo) {
            return parent::getResponseData();
        } else {
            return $this->___callPlugins('getResponseData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigModel()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getConfigModel');
        if (!$pluginInfo) {
            return parent::getConfigModel();
        } else {
            return $this->___callPlugins('getConfigModel', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addData(array $arr)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'addData');
        if (!$pluginInfo) {
            return parent::addData($arr);
        } else {
            return $this->___callPlugins('addData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setData($key, $value = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setData');
        if (!$pluginInfo) {
            return parent::setData($key, $value);
        } else {
            return $this->___callPlugins('setData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function unsetData($key = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'unsetData');
        if (!$pluginInfo) {
            return parent::unsetData($key);
        } else {
            return $this->___callPlugins('unsetData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getData($key = '', $index = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getData');
        if (!$pluginInfo) {
            return parent::getData($key, $index);
        } else {
            return $this->___callPlugins('getData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDataByPath($path)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDataByPath');
        if (!$pluginInfo) {
            return parent::getDataByPath($path);
        } else {
            return $this->___callPlugins('getDataByPath', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDataByKey($key)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDataByKey');
        if (!$pluginInfo) {
            return parent::getDataByKey($key);
        } else {
            return $this->___callPlugins('getDataByKey', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDataUsingMethod($key, $args = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setDataUsingMethod');
        if (!$pluginInfo) {
            return parent::setDataUsingMethod($key, $args);
        } else {
            return $this->___callPlugins('setDataUsingMethod', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDataUsingMethod($key, $args = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDataUsingMethod');
        if (!$pluginInfo) {
            return parent::getDataUsingMethod($key, $args);
        } else {
            return $this->___callPlugins('getDataUsingMethod', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasData($key = '')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'hasData');
        if (!$pluginInfo) {
            return parent::hasData($key);
        } else {
            return $this->___callPlugins('hasData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(array $keys = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'toArray');
        if (!$pluginInfo) {
            return parent::toArray($keys);
        } else {
            return $this->___callPlugins('toArray', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function convertToArray(array $keys = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'convertToArray');
        if (!$pluginInfo) {
            return parent::convertToArray($keys);
        } else {
            return $this->___callPlugins('convertToArray', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function toXml(array $keys = [], $rootName = 'item', $addOpenTag = false, $addCdata = true)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'toXml');
        if (!$pluginInfo) {
            return parent::toXml($keys, $rootName, $addOpenTag, $addCdata);
        } else {
            return $this->___callPlugins('toXml', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function convertToXml(array $arrAttributes = [], $rootName = 'item', $addOpenTag = false, $addCdata = true)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'convertToXml');
        if (!$pluginInfo) {
            return parent::convertToXml($arrAttributes, $rootName, $addOpenTag, $addCdata);
        } else {
            return $this->___callPlugins('convertToXml', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function toJson(array $keys = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'toJson');
        if (!$pluginInfo) {
            return parent::toJson($keys);
        } else {
            return $this->___callPlugins('toJson', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function convertToJson(array $keys = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'convertToJson');
        if (!$pluginInfo) {
            return parent::convertToJson($keys);
        } else {
            return $this->___callPlugins('convertToJson', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function toString($format = '')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'toString');
        if (!$pluginInfo) {
            return parent::toString($format);
        } else {
            return $this->___callPlugins('toString', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function __call($method, $args)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, '__call');
        if (!$pluginInfo) {
            return parent::__call($method, $args);
        } else {
            return $this->___callPlugins('__call', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isEmpty');
        if (!$pluginInfo) {
            return parent::isEmpty();
        } else {
            return $this->___callPlugins('isEmpty', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function serialize($keys = [], $valueSeparator = '=', $fieldSeparator = ' ', $quote = '"')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'serialize');
        if (!$pluginInfo) {
            return parent::serialize($keys, $valueSeparator, $fieldSeparator, $quote);
        } else {
            return $this->___callPlugins('serialize', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function debug($data = null, &$objects = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'debug');
        if (!$pluginInfo) {
            return parent::debug($data, $objects);
        } else {
            return $this->___callPlugins('debug', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'offsetSet');
        if (!$pluginInfo) {
            return parent::offsetSet($offset, $value);
        } else {
            return $this->___callPlugins('offsetSet', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'offsetExists');
        if (!$pluginInfo) {
            return parent::offsetExists($offset);
        } else {
            return $this->___callPlugins('offsetExists', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'offsetUnset');
        if (!$pluginInfo) {
            return parent::offsetUnset($offset);
        } else {
            return $this->___callPlugins('offsetUnset', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'offsetGet');
        if (!$pluginInfo) {
            return parent::offsetGet($offset);
        } else {
            return $this->___callPlugins('offsetGet', func_get_args(), $pluginInfo);
        }
    }
}
