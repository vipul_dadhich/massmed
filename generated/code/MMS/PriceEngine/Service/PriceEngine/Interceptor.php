<?php
namespace MMS\PriceEngine\Service\PriceEngine;

/**
 * Interceptor class for @see \MMS\PriceEngine\Service\PriceEngine
 */
class Interceptor extends \MMS\PriceEngine\Service\PriceEngine implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Zend\Http\Client $zendClient, \Magento\Framework\Serialize\Serializer\Json $json, \Magento\Framework\App\RequestInterface $request, \MMS\PriceEngine\Helper\Data $dataHelper, \Ey\URLParams\Helper\Data $cookieHelper, \MMS\PriceEngine\Model\Logger $priceLogger, \Magento\Directory\Model\CountryFactory $countryFactory, \Magento\Checkout\Model\Session $checkoutSession, \MMS\PriceEngine\Service\Mulesoft\PriceEngineClient $priceEngineClient, \Magento\Framework\App\CacheInterface $cache, \Magento\Framework\App\Cache\StateInterface $cacheState, \MMS\PriceEngine\Api\IpToCountryRepositoryInterface $ipToCountryRepository, \Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->___init();
        parent::__construct($zendClient, $json, $request, $dataHelper, $cookieHelper, $priceLogger, $countryFactory, $checkoutSession, $priceEngineClient, $cache, $cacheState, $ipToCountryRepository, $storeManager);
    }

    /**
     * {@inheritdoc}
     */
    public function getPrice()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPrice');
        if (!$pluginInfo) {
            return parent::getPrice();
        } else {
            return $this->___callPlugins('getPrice', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setPriceCode($priceCode)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setPriceCode');
        if (!$pluginInfo) {
            return parent::setPriceCode($priceCode);
        } else {
            return $this->___callPlugins('setPriceCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPriceCode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPriceCode');
        if (!$pluginInfo) {
            return parent::getPriceCode();
        } else {
            return $this->___callPlugins('getPriceCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setRequest($requestPayload = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setRequest');
        if (!$pluginInfo) {
            return parent::setRequest($requestPayload);
        } else {
            return $this->___callPlugins('setRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRequest');
        if (!$pluginInfo) {
            return parent::getRequest();
        } else {
            return $this->___callPlugins('getRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildEndPoint()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'buildEndPoint');
        if (!$pluginInfo) {
            return parent::buildEndPoint();
        } else {
            return $this->___callPlugins('buildEndPoint', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildPrice($skipCache = false)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'buildPrice');
        if (!$pluginInfo) {
            return parent::buildPrice($skipCache);
        } else {
            return $this->___callPlugins('buildPrice', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function sendRequest()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'sendRequest');
        if (!$pluginInfo) {
            return parent::sendRequest();
        } else {
            return $this->___callPlugins('sendRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheKey() : array
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCacheKey');
        if (!$pluginInfo) {
            return parent::getCacheKey();
        } else {
            return $this->___callPlugins('getCacheKey', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFullResponse()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getFullResponse');
        if (!$pluginInfo) {
            return parent::getFullResponse();
        } else {
            return $this->___callPlugins('getFullResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFullTermsResponse()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getFullTermsResponse');
        if (!$pluginInfo) {
            return parent::getFullTermsResponse();
        } else {
            return $this->___callPlugins('getFullTermsResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setFullResponse($fullResponse)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setFullResponse');
        if (!$pluginInfo) {
            return parent::setFullResponse($fullResponse);
        } else {
            return $this->___callPlugins('setFullResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setFullTermsResponse($fullTermsResponse)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setFullTermsResponse');
        if (!$pluginInfo) {
            return parent::setFullTermsResponse($fullTermsResponse);
        } else {
            return $this->___callPlugins('setFullTermsResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isEnabled');
        if (!$pluginInfo) {
            return parent::isEnabled();
        } else {
            return $this->___callPlugins('isEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabledFixtureMode($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isEnabledFixtureMode');
        if (!$pluginInfo) {
            return parent::isEnabledFixtureMode($store);
        } else {
            return $this->___callPlugins('isEnabledFixtureMode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isExtendFullPageCache() : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isExtendFullPageCache');
        if (!$pluginInfo) {
            return parent::isExtendFullPageCache();
        } else {
            return $this->___callPlugins('isExtendFullPageCache', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProductDefaultTerms(int $productId, $isDefaultId = null, $buyRequest = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProductDefaultTerms');
        if (!$pluginInfo) {
            return parent::getProductDefaultTerms($productId, $isDefaultId, $buyRequest);
        } else {
            return $this->___callPlugins('getProductDefaultTerms', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getApiPointUrl()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getApiPointUrl');
        if (!$pluginInfo) {
            return parent::getApiPointUrl();
        } else {
            return $this->___callPlugins('getApiPointUrl', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabledCache()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isEnabledCache');
        if (!$pluginInfo) {
            return parent::isEnabledCache();
        } else {
            return $this->___callPlugins('isEnabledCache', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheLifetime()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCacheLifetime');
        if (!$pluginInfo) {
            return parent::getCacheLifetime();
        } else {
            return $this->___callPlugins('getCacheLifetime', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function sampleRequestPayload()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'sampleRequestPayload');
        if (!$pluginInfo) {
            return parent::sampleRequestPayload();
        } else {
            return $this->___callPlugins('sampleRequestPayload', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function sampleResponse()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'sampleResponse');
        if (!$pluginInfo) {
            return parent::sampleResponse();
        } else {
            return $this->___callPlugins('sampleResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPromoCode($buyRequest = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPromoCode');
        if (!$pluginInfo) {
            return parent::getPromoCode($buyRequest);
        } else {
            return $this->___callPlugins('getPromoCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCountry()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCountry');
        if (!$pluginInfo) {
            return parent::getCountry();
        } else {
            return $this->___callPlugins('getCountry', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProfessionalCategory()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProfessionalCategory');
        if (!$pluginInfo) {
            return parent::getProfessionalCategory();
        } else {
            return $this->___callPlugins('getProfessionalCategory', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrencyCode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCurrencyCode');
        if (!$pluginInfo) {
            return parent::getCurrencyCode();
        } else {
            return $this->___callPlugins('getCurrencyCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setCurrencyCode($currencyCode)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setCurrencyCode');
        if (!$pluginInfo) {
            return parent::setCurrencyCode($currencyCode);
        } else {
            return $this->___callPlugins('setCurrencyCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getQuoteItemPrice(\Magento\Quote\Model\Quote\Item $item)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getQuoteItemPrice');
        if (!$pluginInfo) {
            return parent::getQuoteItemPrice($item);
        } else {
            return $this->___callPlugins('getQuoteItemPrice', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getQuoteItemPriceCode(\Magento\Quote\Model\Quote\Item $item)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getQuoteItemPriceCode');
        if (!$pluginInfo) {
            return parent::getQuoteItemPriceCode($item);
        } else {
            return $this->___callPlugins('getQuoteItemPriceCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isAllowedCustomPrice()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isAllowedCustomPrice');
        if (!$pluginInfo) {
            return parent::isAllowedCustomPrice();
        } else {
            return $this->___callPlugins('isAllowedCustomPrice', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomer()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomer');
        if (!$pluginInfo) {
            return parent::getCustomer();
        } else {
            return $this->___callPlugins('getCustomer', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPriceLogger()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPriceLogger');
        if (!$pluginInfo) {
            return parent::getPriceLogger();
        } else {
            return $this->___callPlugins('getPriceLogger', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCheckoutSession()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCheckoutSession');
        if (!$pluginInfo) {
            return parent::getCheckoutSession();
        } else {
            return $this->___callPlugins('getCheckoutSession', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPromoCodeFromQuote()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPromoCodeFromQuote');
        if (!$pluginInfo) {
            return parent::getPromoCodeFromQuote();
        } else {
            return $this->___callPlugins('getPromoCodeFromQuote', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProductSku()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProductSku');
        if (!$pluginInfo) {
            return parent::getProductSku();
        } else {
            return $this->___callPlugins('getProductSku', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProductTerms()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProductTerms');
        if (!$pluginInfo) {
            return parent::getProductTerms();
        } else {
            return $this->___callPlugins('getProductTerms', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProductSkuFromQuote()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProductSkuFromQuote');
        if (!$pluginInfo) {
            return parent::getProductSkuFromQuote();
        } else {
            return $this->___callPlugins('getProductSkuFromQuote', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrentCurrencyCode($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCurrentCurrencyCode');
        if (!$pluginInfo) {
            return parent::getCurrentCurrencyCode($store);
        } else {
            return $this->___callPlugins('getCurrentCurrencyCode', func_get_args(), $pluginInfo);
        }
    }
}
