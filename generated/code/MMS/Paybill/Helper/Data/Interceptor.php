<?php
namespace MMS\Paybill\Helper\Data;

/**
 * Interceptor class for @see \MMS\Paybill\Helper\Data
 */
class Interceptor extends \MMS\Paybill\Helper\Data implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Customer\Model\SessionFactory $customerSession, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Json\Helper\Data $jsonHelper, \Ey\MuleSoft\Api\Paybill\HttpClientInterface $sendPayment, \Ey\MuleSoft\Helper\Data $mulesoftHelper, \Magento\Customer\Model\Session $session, \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone, \Ey\URLParams\Helper\Data $urlHelper, \Magento\Sales\Model\OrderFactory $orderFactory, \Magento\Quote\Model\QuoteFactory $quoteFactory, \MMS\Logger\LoggerInterface $logger)
    {
        $this->___init();
        parent::__construct($customerSession, $scopeConfig, $jsonHelper, $sendPayment, $mulesoftHelper, $session, $timezone, $urlHelper, $orderFactory, $quoteFactory, $logger);
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled($storeId = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isEnabled');
        if (!$pluginInfo) {
            return parent::isEnabled($storeId);
        } else {
            return $this->___callPlugins('isEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isPaybillSuccessEnabled($storeId = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isPaybillSuccessEnabled');
        if (!$pluginInfo) {
            return parent::isPaybillSuccessEnabled($storeId);
        } else {
            return $this->___callPlugins('isPaybillSuccessEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getLanguageBlock($storeId = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getLanguageBlock');
        if (!$pluginInfo) {
            return parent::getLanguageBlock($storeId);
        } else {
            return $this->___callPlugins('getLanguageBlock', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPaypalLanguageBlock($storeId = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaypalLanguageBlock');
        if (!$pluginInfo) {
            return parent::getPaypalLanguageBlock($storeId);
        } else {
            return $this->___callPlugins('getPaypalLanguageBlock', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function customerIsLogin()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'customerIsLogin');
        if (!$pluginInfo) {
            return parent::customerIsLogin();
        } else {
            return $this->___callPlugins('customerIsLogin', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function goToHomePage()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'goToHomePage');
        if (!$pluginInfo) {
            return parent::goToHomePage();
        } else {
            return $this->___callPlugins('goToHomePage', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerData()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomerData');
        if (!$pluginInfo) {
            return parent::getCustomerData();
        } else {
            return $this->___callPlugins('getCustomerData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAPIURI()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAPIURI');
        if (!$pluginInfo) {
            return parent::getAPIURI();
        } else {
            return $this->___callPlugins('getAPIURI', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPaybillPaymentEndpoint()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaybillPaymentEndpoint');
        if (!$pluginInfo) {
            return parent::getPaybillPaymentEndpoint();
        } else {
            return $this->___callPlugins('getPaybillPaymentEndpoint', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAkamaiAuthToken()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAkamaiAuthToken');
        if (!$pluginInfo) {
            return parent::getAkamaiAuthToken();
        } else {
            return $this->___callPlugins('getAkamaiAuthToken', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getInvoiceDataFromQuote($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getInvoiceDataFromQuote');
        if (!$pluginInfo) {
            return parent::getInvoiceDataFromQuote($order);
        } else {
            return $this->___callPlugins('getInvoiceDataFromQuote', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMuleSoftDebug()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMuleSoftDebug');
        if (!$pluginInfo) {
            return parent::getMuleSoftDebug();
        } else {
            return $this->___callPlugins('getMuleSoftDebug', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMuleSoftDebugMode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMuleSoftDebugMode');
        if (!$pluginInfo) {
            return parent::getMuleSoftDebugMode();
        } else {
            return $this->___callPlugins('getMuleSoftDebugMode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentPayload($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentPayload');
        if (!$pluginInfo) {
            return parent::getPaymentPayload($order);
        } else {
            return $this->___callPlugins('getPaymentPayload', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderItemData($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOrderItemData');
        if (!$pluginInfo) {
            return parent::getOrderItemData($order);
        } else {
            return $this->___callPlugins('getOrderItemData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAcsOrderId($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAcsOrderId');
        if (!$pluginInfo) {
            return parent::getAcsOrderId($order);
        } else {
            return $this->___callPlugins('getAcsOrderId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAcsExtOrderNumber($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAcsExtOrderNumber');
        if (!$pluginInfo) {
            return parent::getAcsExtOrderNumber($order);
        } else {
            return $this->___callPlugins('getAcsExtOrderNumber', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAcsPromoCode($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAcsPromoCode');
        if (!$pluginInfo) {
            return parent::getAcsPromoCode($order);
        } else {
            return $this->___callPlugins('getAcsPromoCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrencyCode($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCurrencyCode');
        if (!$pluginInfo) {
            return parent::getCurrencyCode($order);
        } else {
            return $this->___callPlugins('getCurrencyCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerNumber($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomerNumber');
        if (!$pluginInfo) {
            return parent::getCustomerNumber($order);
        } else {
            return $this->___callPlugins('getCustomerNumber', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPromotionCode($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPromotionCode');
        if (!$pluginInfo) {
            return parent::getPromotionCode($order);
        } else {
            return $this->___callPlugins('getPromotionCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentInfo($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentInfo');
        if (!$pluginInfo) {
            return parent::getPaymentInfo($order);
        } else {
            return $this->___callPlugins('getPaymentInfo', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function sendPayment($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'sendPayment');
        if (!$pluginInfo) {
            return parent::sendPayment($order);
        } else {
            return $this->___callPlugins('sendPayment', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentEndpoint()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentEndpoint');
        if (!$pluginInfo) {
            return parent::getPaymentEndpoint();
        } else {
            return $this->___callPlugins('getPaymentEndpoint', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentTitle($id)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentTitle');
        if (!$pluginInfo) {
            return parent::getPaymentTitle($id);
        } else {
            return $this->___callPlugins('getPaymentTitle', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getQuoteFromOrder($quoteId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getQuoteFromOrder');
        if (!$pluginInfo) {
            return parent::getQuoteFromOrder($quoteId);
        } else {
            return $this->___callPlugins('getQuoteFromOrder', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder($id)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOrder');
        if (!$pluginInfo) {
            return parent::getOrder($id);
        } else {
            return $this->___callPlugins('getOrder', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setPaybillResponseData($response)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setPaybillResponseData');
        if (!$pluginInfo) {
            return parent::setPaybillResponseData($response);
        } else {
            return $this->___callPlugins('setPaybillResponseData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPaybillResponseData()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaybillResponseData');
        if (!$pluginInfo) {
            return parent::getPaybillResponseData();
        } else {
            return $this->___callPlugins('getPaybillResponseData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isModuleOutputEnabled');
        if (!$pluginInfo) {
            return parent::isModuleOutputEnabled($moduleName);
        } else {
            return $this->___callPlugins('isModuleOutputEnabled', func_get_args(), $pluginInfo);
        }
    }
}
