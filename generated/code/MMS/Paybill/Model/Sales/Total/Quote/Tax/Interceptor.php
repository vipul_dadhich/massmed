<?php
namespace MMS\Paybill\Model\Sales\Total\Quote\Tax;

/**
 * Interceptor class for @see \MMS\Paybill\Model\Sales\Total\Quote\Tax
 */
class Interceptor extends \MMS\Paybill\Model\Sales\Total\Quote\Tax implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Tax\Model\Config $taxConfig, \Magento\Tax\Api\TaxCalculationInterface $taxCalculationService, \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsDataObjectFactory, \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemDataObjectFactory, \Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory $taxClassKeyDataObjectFactory, \Magento\Customer\Api\Data\AddressInterfaceFactory $customerAddressFactory, \Magento\Customer\Api\Data\RegionInterfaceFactory $customerAddressRegionFactory, \Magento\Tax\Helper\Data $taxData, \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency, \Magento\Tax\Api\Data\QuoteDetailsItemExtensionFactory $extensionFactory, \Psr\Log\LoggerInterface $logger, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \MMS\Logger\LoggerInterface $mmsLogger, \Magento\Framework\Json\Helper\Data $jsonHelper, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Directory\Helper\Data $directoryHelper)
    {
        $this->___init();
        parent::__construct($taxConfig, $taxCalculationService, $quoteDetailsDataObjectFactory, $quoteDetailsItemDataObjectFactory, $taxClassKeyDataObjectFactory, $customerAddressFactory, $customerAddressRegionFactory, $taxData, $priceCurrency, $extensionFactory, $logger, $scopeConfig, $mmsLogger, $jsonHelper, $checkoutSession, $directoryHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function collect(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'collect');
        if (!$pluginInfo) {
            return parent::collect($quote, $shippingAssignment, $total);
        } else {
            return $this->___callPlugins('collect', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateCustomSubtotal($total, $quote)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'updateCustomSubtotal');
        if (!$pluginInfo) {
            return parent::updateCustomSubtotal($total, $quote);
        } else {
            return $this->___callPlugins('updateCustomSubtotal', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateCustomGrandTotal($total, $quote)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'updateCustomGrandTotal');
        if (!$pluginInfo) {
            return parent::updateCustomGrandTotal($total, $quote);
        } else {
            return $this->___callPlugins('updateCustomGrandTotal', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateCustomTaxAmount($total, $quote)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'updateCustomTaxAmount');
        if (!$pluginInfo) {
            return parent::updateCustomTaxAmount($total, $quote);
        } else {
            return $this->___callPlugins('updateCustomTaxAmount', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateCustomDiscountAmount($total, $quote)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'updateCustomDiscountAmount');
        if (!$pluginInfo) {
            return parent::updateCustomDiscountAmount($total, $quote);
        } else {
            return $this->___callPlugins('updateCustomDiscountAmount', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getInvoiceDataFromQuote($quote)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getInvoiceDataFromQuote');
        if (!$pluginInfo) {
            return parent::getInvoiceDataFromQuote($quote);
        } else {
            return $this->___callPlugins('getInvoiceDataFromQuote', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTaxFromOneSource(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment, \Magento\Quote\Model\Quote\Address\Total $total, $headerQuote)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getTaxFromOneSource');
        if (!$pluginInfo) {
            return parent::getTaxFromOneSource($quote, $shippingAssignment, $total, $headerQuote);
        } else {
            return $this->___callPlugins('getTaxFromOneSource', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigValue($name)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getConfigValue');
        if (!$pluginInfo) {
            return parent::getConfigValue($name);
        } else {
            return $this->___callPlugins('getConfigValue', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getQuoteTaxDetailsOverride(\Magento\Quote\Model\Quote $quote, \Magento\Tax\Api\Data\QuoteDetailsInterface $taxDetails, $useBaseCurrency)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getQuoteTaxDetailsOverride');
        if (!$pluginInfo) {
            return parent::getQuoteTaxDetailsOverride($quote, $taxDetails, $useBaseCurrency);
        } else {
            return $this->___callPlugins('getQuoteTaxDetailsOverride', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function mapQuoteExtraTaxables(\Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $itemDataObjectFactory, \Magento\Quote\Model\Quote\Address $address, $useBaseCurrency)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'mapQuoteExtraTaxables');
        if (!$pluginInfo) {
            return parent::mapQuoteExtraTaxables($itemDataObjectFactory, $address, $useBaseCurrency);
        } else {
            return $this->___callPlugins('mapQuoteExtraTaxables', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'fetch');
        if (!$pluginInfo) {
            return parent::fetch($quote, $total);
        } else {
            return $this->___callPlugins('fetch', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function processConfigArray($config, $store)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'processConfigArray');
        if (!$pluginInfo) {
            return parent::processConfigArray($config, $store);
        } else {
            return $this->___callPlugins('processConfigArray', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getLabel()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getLabel');
        if (!$pluginInfo) {
            return parent::getLabel();
        } else {
            return $this->___callPlugins('getLabel', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function mapAddress(\Magento\Quote\Model\Quote\Address $address)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'mapAddress');
        if (!$pluginInfo) {
            return parent::mapAddress($address);
        } else {
            return $this->___callPlugins('mapAddress', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function mapItem(\Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $itemDataObjectFactory, \Magento\Quote\Model\Quote\Item\AbstractItem $item, $priceIncludesTax, $useBaseCurrency, $parentCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'mapItem');
        if (!$pluginInfo) {
            return parent::mapItem($itemDataObjectFactory, $item, $priceIncludesTax, $useBaseCurrency, $parentCode);
        } else {
            return $this->___callPlugins('mapItem', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function mapItemExtraTaxables(\Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $itemDataObjectFactory, \Magento\Quote\Model\Quote\Item\AbstractItem $item, $priceIncludesTax, $useBaseCurrency)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'mapItemExtraTaxables');
        if (!$pluginInfo) {
            return parent::mapItemExtraTaxables($itemDataObjectFactory, $item, $priceIncludesTax, $useBaseCurrency);
        } else {
            return $this->___callPlugins('mapItemExtraTaxables', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function mapItems(\Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment, $priceIncludesTax, $useBaseCurrency)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'mapItems');
        if (!$pluginInfo) {
            return parent::mapItems($shippingAssignment, $priceIncludesTax, $useBaseCurrency);
        } else {
            return $this->___callPlugins('mapItems', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function populateAddressData(\Magento\Tax\Api\Data\QuoteDetailsInterface $quoteDetails, \Magento\Quote\Model\Quote\Address $address)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'populateAddressData');
        if (!$pluginInfo) {
            return parent::populateAddressData($quoteDetails, $address);
        } else {
            return $this->___callPlugins('populateAddressData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getShippingDataObject(\Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment, \Magento\Quote\Model\Quote\Address\Total $total, $useBaseCurrency)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getShippingDataObject');
        if (!$pluginInfo) {
            return parent::getShippingDataObject($shippingAssignment, $total, $useBaseCurrency);
        } else {
            return $this->___callPlugins('getShippingDataObject', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateItemTaxInfo($quoteItem, $itemTaxDetails, $baseItemTaxDetails, $store)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'updateItemTaxInfo');
        if (!$pluginInfo) {
            return parent::updateItemTaxInfo($quoteItem, $itemTaxDetails, $baseItemTaxDetails, $store);
        } else {
            return $this->___callPlugins('updateItemTaxInfo', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function convertAppliedTaxes($appliedTaxes, $baseAppliedTaxes, $extraInfo = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'convertAppliedTaxes');
        if (!$pluginInfo) {
            return parent::convertAppliedTaxes($appliedTaxes, $baseAppliedTaxes, $extraInfo);
        } else {
            return $this->___callPlugins('convertAppliedTaxes', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setCode($code)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setCode');
        if (!$pluginInfo) {
            return parent::setCode($code);
        } else {
            return $this->___callPlugins('setCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCode');
        if (!$pluginInfo) {
            return parent::getCode();
        } else {
            return $this->___callPlugins('getCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function _setTotal(\Magento\Quote\Model\Quote\Address\Total $total)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, '_setTotal');
        if (!$pluginInfo) {
            return parent::_setTotal($total);
        } else {
            return $this->___callPlugins('_setTotal', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getItemRowTotal(\Magento\Quote\Model\Quote\Item\AbstractItem $item)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getItemRowTotal');
        if (!$pluginInfo) {
            return parent::getItemRowTotal($item);
        } else {
            return $this->___callPlugins('getItemRowTotal', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getItemBaseRowTotal(\Magento\Quote\Model\Quote\Item\AbstractItem $item)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getItemBaseRowTotal');
        if (!$pluginInfo) {
            return parent::getItemBaseRowTotal($item);
        } else {
            return $this->___callPlugins('getItemBaseRowTotal', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getIsItemRowTotalCompoundable(\Magento\Quote\Model\Quote\Item\AbstractItem $item)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getIsItemRowTotalCompoundable');
        if (!$pluginInfo) {
            return parent::getIsItemRowTotalCompoundable($item);
        } else {
            return $this->___callPlugins('getIsItemRowTotalCompoundable', func_get_args(), $pluginInfo);
        }
    }
}
