<?php
namespace Ey\Akamai\Controller\Ssologin\Index;

/**
 * Interceptor class for @see \Ey\Akamai\Controller\Ssologin\Index
 */
class Interceptor extends \Ey\Akamai\Controller\Ssologin\Index implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Customer\Model\Customer $customerRepo, \Magento\Customer\Model\Session $customerSession, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Store\Model\StoreResolver $storeResolver, \Ey\MuleSoft\Helper\Data $helper, \Ey\MuleSoft\Model\Core\Logger\Monolog $mulesoftLogger, \Ey\MuleSoft\Model\Catalyst\Api\GetCustomerData $getCustomerData, \Magento\Framework\Json\Helper\Data $jsonHelper, \Ey\MuleSoft\Helper\CustomerHelper $customerHelper, \GuzzleHttp\ClientFactory $clientFactory)
    {
        $this->___init();
        parent::__construct($context, $customerRepo, $customerSession, $resultJsonFactory, $storeManager, $storeResolver, $helper, $mulesoftLogger, $getCustomerData, $jsonHelper, $customerHelper, $clientFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        if (!$pluginInfo) {
            return parent::execute();
        } else {
            return $this->___callPlugins('execute', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validateToken($accessToken, $ucid, array $headers = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'validateToken');
        if (!$pluginInfo) {
            return parent::validateToken($accessToken, $ucid, $headers);
        } else {
            return $this->___callPlugins('validateToken', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getActionFlag()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getActionFlag');
        if (!$pluginInfo) {
            return parent::getActionFlag();
        } else {
            return $this->___callPlugins('getActionFlag', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRequest');
        if (!$pluginInfo) {
            return parent::getRequest();
        } else {
            return $this->___callPlugins('getRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getResponse');
        if (!$pluginInfo) {
            return parent::getResponse();
        } else {
            return $this->___callPlugins('getResponse', func_get_args(), $pluginInfo);
        }
    }
}
