<?php
namespace Ey\BillMeLater\Observer\BillMeLaterEnable;

/**
 * Interceptor class for @see \Ey\BillMeLater\Observer\BillMeLaterEnable
 */
class Interceptor extends \Ey\BillMeLater\Observer\BillMeLaterEnable implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Checkout\Model\Cart $cart, \Magento\Catalog\Api\ProductRepositoryInterface $product)
    {
        $this->___init();
        parent::__construct($scopeConfig, $cart, $product);
    }

    /**
     * {@inheritdoc}
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        if (!$pluginInfo) {
            return parent::execute($observer);
        } else {
            return $this->___callPlugins('execute', func_get_args(), $pluginInfo);
        }
    }
}
