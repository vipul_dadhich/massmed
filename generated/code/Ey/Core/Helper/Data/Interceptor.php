<?php
namespace Ey\Core\Helper\Data;

/**
 * Interceptor class for @see \Ey\Core\Helper\Data
 */
class Interceptor extends \Ey\Core\Helper\Data implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $request, \Magento\Sales\Model\OrderFactory $orderFactory, \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable, \Magento\Catalog\Model\ProductRepository $productRepository, \Magento\Sales\Model\Order\Address\Renderer $addressRenderer, \Magento\Customer\Model\Session $customerSession, \Magento\Catalog\Model\Product $product, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory, \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager)
    {
        $this->___init();
        parent::__construct($request, $orderFactory, $configurable, $productRepository, $addressRenderer, $customerSession, $product, $scopeConfig, $coreRegistry, $cookieMetadataFactory, $cookieManager);
    }

    /**
     * {@inheritdoc}
     */
    public function getIsCustomerLoggedIn()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getIsCustomerLoggedIn');
        if (!$pluginInfo) {
            return parent::getIsCustomerLoggedIn();
        } else {
            return $this->___callPlugins('getIsCustomerLoggedIn', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function loadProductById($id)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'loadProductById');
        if (!$pluginInfo) {
            return parent::loadProductById($id);
        } else {
            return $this->___callPlugins('loadProductById', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigValue($id)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getConfigValue');
        if (!$pluginInfo) {
            return parent::getConfigValue($id);
        } else {
            return $this->___callPlugins('getConfigValue', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder($id)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOrder');
        if (!$pluginInfo) {
            return parent::getOrder($id);
        } else {
            return $this->___callPlugins('getOrder', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getIncrementOrder($id)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getIncrementOrder');
        if (!$pluginInfo) {
            return parent::getIncrementOrder($id);
        } else {
            return $this->___callPlugins('getIncrementOrder', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProductInfo($productId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProductInfo');
        if (!$pluginInfo) {
            return parent::getProductInfo($productId);
        } else {
            return $this->___callPlugins('getProductInfo', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderAmount($id)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOrderAmount');
        if (!$pluginInfo) {
            return parent::getOrderAmount($id);
        } else {
            return $this->___callPlugins('getOrderAmount', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBillingInformation($id)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBillingInformation');
        if (!$pluginInfo) {
            return parent::getBillingInformation($id);
        } else {
            return $this->___callPlugins('getBillingInformation', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentTitle($id)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentTitle');
        if (!$pluginInfo) {
            return parent::getPaymentTitle($id);
        } else {
            return $this->___callPlugins('getPaymentTitle', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setCurrentOrder($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setCurrentOrder');
        if (!$pluginInfo) {
            return parent::setCurrentOrder($order);
        } else {
            return $this->___callPlugins('setCurrentOrder', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isModuleOutputEnabled');
        if (!$pluginInfo) {
            return parent::isModuleOutputEnabled($moduleName);
        } else {
            return $this->___callPlugins('isModuleOutputEnabled', func_get_args(), $pluginInfo);
        }
    }
}
