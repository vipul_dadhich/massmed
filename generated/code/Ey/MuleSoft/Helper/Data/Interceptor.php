<?php
namespace Ey\MuleSoft\Helper\Data;

/**
 * Interceptor class for @see \Ey\MuleSoft\Helper\Data
 */
class Interceptor extends \Ey\MuleSoft\Helper\Data implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Helper\Context $context, \Magento\Customer\Model\Customer $customer, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Eav\Model\Config $eavConfig, \Magento\Catalog\Model\Product $product, \Magento\Directory\Api\CountryInformationAcquirerInterface $countryInformationAcquirer, \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone, \Magento\Customer\Model\CustomerFactory $customerFactory, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository, \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerInterfaceFactory, \Magento\Framework\Encryption\EncryptorInterface $encryptorInterface, \Magento\Directory\Model\CountryFactory $countryFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Store\Model\StoreResolver $storeResolver, \Magento\Customer\Model\Session $session, \Magento\Customer\Model\Group $group, \Magento\Customer\Api\AddressRepositoryInterface $addressRepository, \Magento\Customer\Api\Data\AddressInterfaceFactory $addressDataFactory, \Magento\Customer\Api\Data\RegionInterfaceFactory $regionInterfaceFactory, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Framework\App\State $state, \MMS\Logger\LoggerInterface $logger, \Ey\MuleSoft\Helper\Speciality $speciality)
    {
        $this->___init();
        parent::__construct($context, $customer, $scopeConfig, $eavConfig, $product, $countryInformationAcquirer, $timezone, $customerFactory, $customerRepository, $customerInterfaceFactory, $encryptorInterface, $countryFactory, $storeManager, $storeResolver, $session, $group, $addressRepository, $addressDataFactory, $regionInterfaceFactory, $checkoutSession, $state, $logger, $speciality);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderData($order, $session)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOrderData');
        if (!$pluginInfo) {
            return parent::getOrderData($order, $session);
        } else {
            return $this->___callPlugins('getOrderData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderDataForAdmin($order, $session)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOrderDataForAdmin');
        if (!$pluginInfo) {
            return parent::getOrderDataForAdmin($order, $session);
        } else {
            return $this->___callPlugins('getOrderDataForAdmin', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAllStates()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAllStates');
        if (!$pluginInfo) {
            return parent::getAllStates();
        } else {
            return $this->___callPlugins('getAllStates', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function customerExists($email, $websiteId = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'customerExists');
        if (!$pluginInfo) {
            return parent::customerExists($email, $websiteId);
        } else {
            return $this->___callPlugins('customerExists', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createCustomer($orderData)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'createCustomer');
        if (!$pluginInfo) {
            return parent::createCustomer($orderData);
        } else {
            return $this->___callPlugins('createCustomer', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateCustomer($response, $customer)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'updateCustomer');
        if (!$pluginInfo) {
            return parent::updateCustomer($response, $customer);
        } else {
            return $this->___callPlugins('updateCustomer', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function saveUcid($customer_id, $ucid)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'saveUcid');
        if (!$pluginInfo) {
            return parent::saveUcid($customer_id, $ucid);
        } else {
            return $this->___callPlugins('saveUcid', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMuleSoftClientId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMuleSoftClientId');
        if (!$pluginInfo) {
            return parent::getMuleSoftClientId();
        } else {
            return $this->___callPlugins('getMuleSoftClientId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMuleSoftClientSecret()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMuleSoftClientSecret');
        if (!$pluginInfo) {
            return parent::getMuleSoftClientSecret();
        } else {
            return $this->___callPlugins('getMuleSoftClientSecret', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMuleSoftDebug()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMuleSoftDebug');
        if (!$pluginInfo) {
            return parent::getMuleSoftDebug();
        } else {
            return $this->___callPlugins('getMuleSoftDebug', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMuleSoftDebugMode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMuleSoftDebugMode');
        if (!$pluginInfo) {
            return parent::getMuleSoftDebugMode();
        } else {
            return $this->___callPlugins('getMuleSoftDebugMode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMuleSoftCustomerURI()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMuleSoftCustomerURI');
        if (!$pluginInfo) {
            return parent::getMuleSoftCustomerURI();
        } else {
            return $this->___callPlugins('getMuleSoftCustomerURI', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMuleSoftURI()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMuleSoftURI');
        if (!$pluginInfo) {
            return parent::getMuleSoftURI();
        } else {
            return $this->___callPlugins('getMuleSoftURI', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMuleSoftOrderEndpoint($orderData)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMuleSoftOrderEndpoint');
        if (!$pluginInfo) {
            return parent::getMuleSoftOrderEndpoint($orderData);
        } else {
            return $this->___callPlugins('getMuleSoftOrderEndpoint', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMuleSoftTokenValidationURI()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMuleSoftTokenValidationURI');
        if (!$pluginInfo) {
            return parent::getMuleSoftTokenValidationURI();
        } else {
            return $this->___callPlugins('getMuleSoftTokenValidationURI', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTermFromOrderOrProduct(\Magento\Sales\Model\Order\Item $orderItem, \Magento\Catalog\Model\Product $product)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getTermFromOrderOrProduct');
        if (!$pluginInfo) {
            return parent::getTermFromOrderOrProduct($orderItem, $product);
        } else {
            return $this->___callPlugins('getTermFromOrderOrProduct', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function postSendMuleSoftRequest($params, $header)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'postSendMuleSoftRequest');
        if (!$pluginInfo) {
            return parent::postSendMuleSoftRequest($params, $header);
        } else {
            return $this->___callPlugins('postSendMuleSoftRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSendMuleSoftRequest($params, $header)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSendMuleSoftRequest');
        if (!$pluginInfo) {
            return parent::getSendMuleSoftRequest($params, $header);
        } else {
            return $this->___callPlugins('getSendMuleSoftRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSendMuleSoftRequestException($params, $header)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSendMuleSoftRequestException');
        if (!$pluginInfo) {
            return parent::getSendMuleSoftRequestException($params, $header);
        } else {
            return $this->___callPlugins('getSendMuleSoftRequestException', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createCustomerBeforeOrder($order, $postParams)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'createCustomerBeforeOrder');
        if (!$pluginInfo) {
            return parent::createCustomerBeforeOrder($order, $postParams);
        } else {
            return $this->___callPlugins('createCustomerBeforeOrder', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createCustomerLoginTime($customerResponse)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'createCustomerLoginTime');
        if (!$pluginInfo) {
            return parent::createCustomerLoginTime($customerResponse);
        } else {
            return $this->___callPlugins('createCustomerLoginTime', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createAddressForNewCustomer($order, $customer, $postParams)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'createAddressForNewCustomer');
        if (!$pluginInfo) {
            return parent::createAddressForNewCustomer($order, $customer, $postParams);
        } else {
            return $this->___callPlugins('createAddressForNewCustomer', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerGroupIdByName($name)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomerGroupIdByName');
        if (!$pluginInfo) {
            return parent::getCustomerGroupIdByName($name);
        } else {
            return $this->___callPlugins('getCustomerGroupIdByName', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function redirectToErrorPage()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'redirectToErrorPage');
        if (!$pluginInfo) {
            return parent::redirectToErrorPage();
        } else {
            return $this->___callPlugins('redirectToErrorPage', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderItemBrand($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOrderItemBrand');
        if (!$pluginInfo) {
            return parent::getOrderItemBrand($order);
        } else {
            return $this->___callPlugins('getOrderItemBrand', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerInfoFromOrder($order, $postParams)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomerInfoFromOrder');
        if (!$pluginInfo) {
            return parent::getCustomerInfoFromOrder($order, $postParams);
        } else {
            return $this->___callPlugins('getCustomerInfoFromOrder', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createCustomerPayload($customer, $order, $postParams)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'createCustomerPayload');
        if (!$pluginInfo) {
            return parent::createCustomerPayload($customer, $order, $postParams);
        } else {
            return $this->___callPlugins('createCustomerPayload', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createCustomerPayloadForAdmin($customer, $order, $postParams)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'createCustomerPayloadForAdmin');
        if (!$pluginInfo) {
            return parent::createCustomerPayloadForAdmin($customer, $order, $postParams);
        } else {
            return $this->___callPlugins('createCustomerPayloadForAdmin', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProfessionCategory($role, $suffix)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProfessionCategory');
        if (!$pluginInfo) {
            return parent::getProfessionCategory($role, $suffix);
        } else {
            return $this->___callPlugins('getProfessionCategory', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateCustomerGroupAfterSubscribe($customer)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'updateCustomerGroupAfterSubscribe');
        if (!$pluginInfo) {
            return parent::updateCustomerGroupAfterSubscribe($customer);
        } else {
            return $this->___callPlugins('updateCustomerGroupAfterSubscribe', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getWebsiteId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getWebsiteId');
        if (!$pluginInfo) {
            return parent::getWebsiteId();
        } else {
            return $this->___callPlugins('getWebsiteId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProductBrandFromOrder($order)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProductBrandFromOrder');
        if (!$pluginInfo) {
            return parent::getProductBrandFromOrder($order);
        } else {
            return $this->___callPlugins('getProductBrandFromOrder', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isModuleOutputEnabled');
        if (!$pluginInfo) {
            return parent::isModuleOutputEnabled($moduleName);
        } else {
            return $this->___callPlugins('isModuleOutputEnabled', func_get_args(), $pluginInfo);
        }
    }
}
