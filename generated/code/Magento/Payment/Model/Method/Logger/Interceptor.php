<?php
namespace Magento\Payment\Model\Method\Logger;

/**
 * Interceptor class for @see \Magento\Payment\Model\Method\Logger
 */
class Interceptor extends \Magento\Payment\Model\Method\Logger implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Psr\Log\LoggerInterface $logger, ?\Magento\Payment\Gateway\ConfigInterface $config = null)
    {
        $this->___init();
        parent::__construct($logger, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function debug(array $data, ?array $maskKeys = null, $forceDebug = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'debug');
        if (!$pluginInfo) {
            return parent::debug($data, $maskKeys, $forceDebug);
        } else {
            return $this->___callPlugins('debug', func_get_args(), $pluginInfo);
        }
    }
}
