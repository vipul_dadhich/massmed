<?php
namespace Magento\Quote\Api\Data;

/**
 * ExtensionInterface class for @see \Magento\Quote\Api\Data\AddressInterface
 */
interface AddressExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return \Magento\SalesRule\Api\Data\RuleDiscountInterface[]|null
     */
    public function getDiscounts();

    /**
     * @param \Magento\SalesRule\Api\Data\RuleDiscountInterface[] $discounts
     * @return $this
     */
    public function setDiscounts($discounts);

    /**
     * @return int|null
     */
    public function getGiftRegistryId();

    /**
     * @param int $giftRegistryId
     * @return $this
     */
    public function setGiftRegistryId($giftRegistryId);

    /**
     * @return string|null
     */
    public function getBaPassword();

    /**
     * @param string $baPassword
     * @return $this
     */
    public function setBaPassword($baPassword);

    /**
     * @return string|null
     */
    public function getPiSuffix();

    /**
     * @param string $piSuffix
     * @return $this
     */
    public function setPiSuffix($piSuffix);

    /**
     * @return string|null
     */
    public function getPiRole();

    /**
     * @param string $piRole
     * @return $this
     */
    public function setPiRole($piRole);

    /**
     * @return string|null
     */
    public function getPiPrimaryspecialty();

    /**
     * @param string $piPrimaryspecialty
     * @return $this
     */
    public function setPiPrimaryspecialty($piPrimaryspecialty);

    /**
     * @return string|null
     */
    public function getPiProfessionalCategory();

    /**
     * @param string $piProfessionalCategory
     * @return $this
     */
    public function setPiProfessionalCategory($piProfessionalCategory);

    /**
     * @return string|null
     */
    public function getPiStudentType();

    /**
     * @param string $piStudentType
     * @return $this
     */
    public function setPiStudentType($piStudentType);

    /**
     * @return string|null
     */
    public function getPiProfession();

    /**
     * @param string $piProfession
     * @return $this
     */
    public function setPiProfession($piProfession);

    /**
     * @return string|null
     */
    public function getOrganization();

    /**
     * @param string $organization
     * @return $this
     */
    public function setOrganization($organization);

    /**
     * @return string|null
     */
    public function getCustomerCatalystConnect();

    /**
     * @param string $customerCatalystConnect
     * @return $this
     */
    public function setCustomerCatalystConnect($customerCatalystConnect);

    /**
     * @return string|null
     */
    public function getCustomerCatalystSoi();

    /**
     * @param string $customerCatalystSoi
     * @return $this
     */
    public function setCustomerCatalystSoi($customerCatalystSoi);

    /**
     * @return string|null
     */
    public function getNejmSoi();

    /**
     * @param string $nejmSoi
     * @return $this
     */
    public function setNejmSoi($nejmSoi);

    /**
     * @return string|null
     */
    public function getNejmEtoc();

    /**
     * @param string $nejmEtoc
     * @return $this
     */
    public function setNejmEtoc($nejmEtoc);

    /**
     * @return string|null
     */
    public function getAddresscode();

    /**
     * @param string $addresscode
     * @return $this
     */
    public function setAddresscode($addresscode);

    /**
     * @return string|null
     */
    public function getBillingAddressCode();

    /**
     * @param string $billingAddressCode
     * @return $this
     */
    public function setBillingAddressCode($billingAddressCode);

    /**
     * @return string|null
     */
    public function getMailingAddressCode();

    /**
     * @param string $mailingAddressCode
     * @return $this
     */
    public function setMailingAddressCode($mailingAddressCode);

    /**
     * @return \Magento\Framework\Api\AttributeInterface[]|null
     */
    public function getCheckoutFields();

    /**
     * @param \Magento\Framework\Api\AttributeInterface[] $checkoutFields
     * @return $this
     */
    public function setCheckoutFields($checkoutFields);
}
