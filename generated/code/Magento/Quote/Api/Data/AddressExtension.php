<?php
namespace Magento\Quote\Api\Data;

/**
 * Extension class for @see \Magento\Quote\Api\Data\AddressInterface
 */
class AddressExtension extends \Magento\Framework\Api\AbstractSimpleObject implements AddressExtensionInterface
{
    /**
     * @return \Magento\SalesRule\Api\Data\RuleDiscountInterface[]|null
     */
    public function getDiscounts()
    {
        return $this->_get('discounts');
    }

    /**
     * @param \Magento\SalesRule\Api\Data\RuleDiscountInterface[] $discounts
     * @return $this
     */
    public function setDiscounts($discounts)
    {
        $this->setData('discounts', $discounts);
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGiftRegistryId()
    {
        return $this->_get('gift_registry_id');
    }

    /**
     * @param int $giftRegistryId
     * @return $this
     */
    public function setGiftRegistryId($giftRegistryId)
    {
        $this->setData('gift_registry_id', $giftRegistryId);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBaPassword()
    {
        return $this->_get('ba_password');
    }

    /**
     * @param string $baPassword
     * @return $this
     */
    public function setBaPassword($baPassword)
    {
        $this->setData('ba_password', $baPassword);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPiSuffix()
    {
        return $this->_get('pi_suffix');
    }

    /**
     * @param string $piSuffix
     * @return $this
     */
    public function setPiSuffix($piSuffix)
    {
        $this->setData('pi_suffix', $piSuffix);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPiRole()
    {
        return $this->_get('pi_role');
    }

    /**
     * @param string $piRole
     * @return $this
     */
    public function setPiRole($piRole)
    {
        $this->setData('pi_role', $piRole);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPiPrimaryspecialty()
    {
        return $this->_get('pi_primaryspecialty');
    }

    /**
     * @param string $piPrimaryspecialty
     * @return $this
     */
    public function setPiPrimaryspecialty($piPrimaryspecialty)
    {
        $this->setData('pi_primaryspecialty', $piPrimaryspecialty);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPiProfessionalCategory()
    {
        return $this->_get('pi_professional_category');
    }

    /**
     * @param string $piProfessionalCategory
     * @return $this
     */
    public function setPiProfessionalCategory($piProfessionalCategory)
    {
        $this->setData('pi_professional_category', $piProfessionalCategory);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPiStudentType()
    {
        return $this->_get('pi_student_type');
    }

    /**
     * @param string $piStudentType
     * @return $this
     */
    public function setPiStudentType($piStudentType)
    {
        $this->setData('pi_student_type', $piStudentType);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPiProfession()
    {
        return $this->_get('pi_profession');
    }

    /**
     * @param string $piProfession
     * @return $this
     */
    public function setPiProfession($piProfession)
    {
        $this->setData('pi_profession', $piProfession);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrganization()
    {
        return $this->_get('organization');
    }

    /**
     * @param string $organization
     * @return $this
     */
    public function setOrganization($organization)
    {
        $this->setData('organization', $organization);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomerCatalystConnect()
    {
        return $this->_get('customer_catalyst_connect');
    }

    /**
     * @param string $customerCatalystConnect
     * @return $this
     */
    public function setCustomerCatalystConnect($customerCatalystConnect)
    {
        $this->setData('customer_catalyst_connect', $customerCatalystConnect);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomerCatalystSoi()
    {
        return $this->_get('customer_catalyst_soi');
    }

    /**
     * @param string $customerCatalystSoi
     * @return $this
     */
    public function setCustomerCatalystSoi($customerCatalystSoi)
    {
        $this->setData('customer_catalyst_soi', $customerCatalystSoi);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNejmSoi()
    {
        return $this->_get('nejm_soi');
    }

    /**
     * @param string $nejmSoi
     * @return $this
     */
    public function setNejmSoi($nejmSoi)
    {
        $this->setData('nejm_soi', $nejmSoi);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNejmEtoc()
    {
        return $this->_get('nejm_etoc');
    }

    /**
     * @param string $nejmEtoc
     * @return $this
     */
    public function setNejmEtoc($nejmEtoc)
    {
        $this->setData('nejm_etoc', $nejmEtoc);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddresscode()
    {
        return $this->_get('addresscode');
    }

    /**
     * @param string $addresscode
     * @return $this
     */
    public function setAddresscode($addresscode)
    {
        $this->setData('addresscode', $addresscode);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBillingAddressCode()
    {
        return $this->_get('billing_address_code');
    }

    /**
     * @param string $billingAddressCode
     * @return $this
     */
    public function setBillingAddressCode($billingAddressCode)
    {
        $this->setData('billing_address_code', $billingAddressCode);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMailingAddressCode()
    {
        return $this->_get('mailing_address_code');
    }

    /**
     * @param string $mailingAddressCode
     * @return $this
     */
    public function setMailingAddressCode($mailingAddressCode)
    {
        $this->setData('mailing_address_code', $mailingAddressCode);
        return $this;
    }

    /**
     * @return \Magento\Framework\Api\AttributeInterface[]|null
     */
    public function getCheckoutFields()
    {
        return $this->_get('checkout_fields');
    }

    /**
     * @param \Magento\Framework\Api\AttributeInterface[] $checkoutFields
     * @return $this
     */
    public function setCheckoutFields($checkoutFields)
    {
        $this->setData('checkout_fields', $checkoutFields);
        return $this;
    }
}
