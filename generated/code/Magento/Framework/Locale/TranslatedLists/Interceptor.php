<?php
namespace Magento\Framework\Locale\TranslatedLists;

/**
 * Interceptor class for @see \Magento\Framework\Locale\TranslatedLists
 */
class Interceptor extends \Magento\Framework\Locale\TranslatedLists implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Locale\ConfigInterface $config, \Magento\Framework\Locale\ResolverInterface $localeResolver, $locale = null)
    {
        $this->___init();
        parent::__construct($config, $localeResolver, $locale);
    }

    /**
     * {@inheritdoc}
     */
    public function getOptionLocales()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOptionLocales');
        if (!$pluginInfo) {
            return parent::getOptionLocales();
        } else {
            return $this->___callPlugins('getOptionLocales', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTranslatedOptionLocales()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getTranslatedOptionLocales');
        if (!$pluginInfo) {
            return parent::getTranslatedOptionLocales();
        } else {
            return $this->___callPlugins('getTranslatedOptionLocales', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOptionTimezones()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOptionTimezones');
        if (!$pluginInfo) {
            return parent::getOptionTimezones();
        } else {
            return $this->___callPlugins('getOptionTimezones', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOptionWeekdays($preserveCodes = false, $ucFirstCode = false)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOptionWeekdays');
        if (!$pluginInfo) {
            return parent::getOptionWeekdays($preserveCodes, $ucFirstCode);
        } else {
            return $this->___callPlugins('getOptionWeekdays', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOptionCountries()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOptionCountries');
        if (!$pluginInfo) {
            return parent::getOptionCountries();
        } else {
            return $this->___callPlugins('getOptionCountries', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOptionCurrencies()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOptionCurrencies');
        if (!$pluginInfo) {
            return parent::getOptionCurrencies();
        } else {
            return $this->___callPlugins('getOptionCurrencies', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOptionAllCurrencies()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOptionAllCurrencies');
        if (!$pluginInfo) {
            return parent::getOptionAllCurrencies();
        } else {
            return $this->___callPlugins('getOptionAllCurrencies', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCountryTranslation($value, $locale = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCountryTranslation');
        if (!$pluginInfo) {
            return parent::getCountryTranslation($value, $locale);
        } else {
            return $this->___callPlugins('getCountryTranslation', func_get_args(), $pluginInfo);
        }
    }
}
