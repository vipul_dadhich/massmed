<?php
namespace Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\CustomOptions;

/**
 * Interceptor class for @see \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\CustomOptions
 */
class Interceptor extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\CustomOptions implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Catalog\Model\Locator\LocatorInterface $locator, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Catalog\Model\ProductOptions\ConfigInterface $productOptionsConfig, \Magento\Catalog\Model\Config\Source\Product\Options\Price $productOptionsPrice, \Magento\Framework\UrlInterface $urlBuilder, \Magento\Framework\Stdlib\ArrayManager $arrayManager)
    {
        $this->___init();
        parent::__construct($locator, $storeManager, $productOptionsConfig, $productOptionsPrice, $urlBuilder, $arrayManager);
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'modifyData');
        if (!$pluginInfo) {
            return parent::modifyData($data);
        } else {
            return $this->___callPlugins('modifyData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'modifyMeta');
        if (!$pluginInfo) {
            return parent::modifyMeta($meta);
        } else {
            return $this->___callPlugins('modifyMeta', func_get_args(), $pluginInfo);
        }
    }
}
