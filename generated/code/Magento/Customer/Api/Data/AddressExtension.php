<?php
namespace Magento\Customer\Api\Data;

/**
 * Extension class for @see \Magento\Customer\Api\Data\AddressInterface
 */
class AddressExtension extends \Magento\Framework\Api\AbstractSimpleObject implements AddressExtensionInterface
{
    /**
     * @return string|null
     */
    public function getAddresscode()
    {
        return $this->_get('addresscode');
    }

    /**
     * @param string $addresscode
     * @return $this
     */
    public function setAddresscode($addresscode)
    {
        $this->setData('addresscode', $addresscode);
        return $this;
    }
}
