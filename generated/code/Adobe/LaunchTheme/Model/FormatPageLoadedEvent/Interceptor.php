<?php
namespace Adobe\LaunchTheme\Model\FormatPageLoadedEvent;

/**
 * Interceptor class for @see \Adobe\LaunchTheme\Model\FormatPageLoadedEvent
 */
class Interceptor extends \Adobe\LaunchTheme\Model\FormatPageLoadedEvent implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct()
    {
        $this->___init();
    }

    /**
     * {@inheritdoc}
     */
    public function execute(string $pageTitle, string $pageType, array $breadcrumbs = []) : array
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        if (!$pluginInfo) {
            return parent::execute($pageTitle, $pageType, $breadcrumbs);
        } else {
            return $this->___callPlugins('execute', func_get_args(), $pluginInfo);
        }
    }
}
